function(subgine_add_test TEST_MODULE TEST_NAME TEST_SRC)
	if(SUBGINE_TEST)
		if("${TEST_MODULE}" MATCHES "^(.*)::(.*)")
			string(REGEX MATCH "::(.*)" NAME_WITH_COLON ${TEST_MODULE})
			string(SUBSTRING ${NAME_WITH_COLON} 2 -1 target-name)
		else()
			set(target-name ${TEST_MODULE})
		endif()
		
		add_executable(${target-name}-${TEST_NAME} ${TEST_SRC})
		target_link_libraries(${target-name}-${TEST_NAME} PUBLIC subgine::test-common ${TEST_MODULE})
		add_test(NAME ${TEST_NAME} COMMAND ${target-name}-${TEST_NAME})
		set_tests_properties(${TEST_NAME} PROPERTIES
			ENVIRONMENT "TEST_FILE=${TEST_SRC}"
		)
	endif()
endfunction()
