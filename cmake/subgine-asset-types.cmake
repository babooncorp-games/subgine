function(subgine_asset_compile_json COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	set(output ${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}.cbor)
	add_custom_command(
		OUTPUT ${output}
		COMMAND subgine::compile-json
			${output}
			${COMPILATION_SOURCES}
		DEPENDS ${COMPILATION_SOURCES} subgine::compile-json
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set(${COMPILATION_OUTPUT} ${output} PARENT_SCOPE)
endfunction()

function(subgine_asset_compile_file COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	list(LENGTH length "${COMPILATION_SOURCES}")
	
	if(NOT ${length} EQUAL 1)
		message(FATAL_ERROR "Asset compilation failed: ${ASSET_TARGET_NAME} must have exactly one source file.")
	endif()
	
	get_filename_component(extension "${COMPILATION_SOURCES}" LAST_EXT)
	
	set(output ${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}${extension})
	add_custom_command(
		OUTPUT ${output}
		COMMAND ${CMAKE_COMMAND} -E copy
			${COMPILATION_SOURCES}
			${output}
		DEPENDS ${COMPILATION_SOURCES}
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set(${COMPILATION_OUTPUT} ${output} PARENT_SCOPE)
endfunction()

function(subgine_asset_compile_mapping COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	set(output "${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}.cbor")
	cmake_path(RELATIVE_PATH CMAKE_CURRENT_SOURCE_DIR BASE_DIRECTORY "${PROJECT_SOURCE_DIR}" OUTPUT_VARIABLE module-path)
	
	add_custom_command(
		OUTPUT "${output}"
		COMMAND subgine::compile-asset-map
			"${output}"
			"${module-path}"
			"${CMAKE_CURRENT_BINARY_DIR}/asset.json"
			${COMPILATION_SOURCES}
		DEPENDS ${COMPILATION_SOURCES} "${CMAKE_CURRENT_BINARY_DIR}/asset.json" subgine::compile-asset-map
		WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
	)
	set(${COMPILATION_OUTPUT} "${output}" PARENT_SCOPE)
endfunction()

function(subgine_asset_compile_tileset COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	get_filename_component(extension "${COMPILATION_SOURCES}" LAST_EXT)
	
	set(output ${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}${extension})
	add_custom_command(
		OUTPUT ${output}
		COMMAND subgine::compile-tileset
			${OPTIONS}
			${output}
			${COMPILATION_SOURCES}
		DEPENDS ${COMPILATION_SOURCES} subgine::compile-tileset
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set(${COMPILATION_OUTPUT} ${output} PARENT_SCOPE)
endfunction()

function(subgine_asset_compile_spritesheet COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	get_filename_component(extension "${COMPILATION_SOURCES}" LAST_EXT)
	
	set(output ${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}${extension})
	add_custom_command(
		OUTPUT ${output}
		COMMAND subgine::compile-spritesheet
			${OPTIONS}
			${output}
			${COMPILATION_SOURCES}
		DEPENDS ${COMPILATION_SOURCES} subgine::compile-spritesheet
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
	set(${COMPILATION_OUTPUT} ${output} PARENT_SCOPE)
endfunction()

function(subgine_asset_compile_directory COMPILATION_OUTPUT ASSET_TARGET_NAME COMPILATION_SOURCES OPTIONS)
	set(output-dir "")
	set(output "")
	set(commands "")
	foreach(asset-file ${COMPILATION_SOURCES})
		set(asset-output "${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}/${ASSET_TARGET_NAME}/${asset-file}")
		set(asset-input "${OPTIONS}/${asset-file}")
		list(APPEND output "${asset-output}")
		add_custom_command(
			OUTPUT "${asset-output}"
			COMMAND ${CMAKE_COMMAND} -E copy "\"${asset-input}\"" "\"${asset-output}\""
			DEPENDS "${asset-input}"
			WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		)
	endforeach()
	set(${COMPILATION_OUTPUT} "${output}" PARENT_SCOPE)
endfunction()
