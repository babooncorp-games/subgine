set(_SUBGINE_MODULE_CMAKE_CURRENT_DIR "${CMAKE_CURRENT_LIST_DIR}")

function(subgine_config_asset_directory target-name)
	configure_file("${_SUBGINE_MODULE_CMAKE_CURRENT_DIR}/cmake/assetdirectoryconfig.cpp.in" ${CMAKE_CURRENT_BINARY_DIR}/generated/assetdirectoryconfig.cpp @ONLY)
	target_sources(${target-name} PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/generated/assetdirectoryconfig.cpp)
endfunction()

function(subgine_configure_target target-name)
	target_include_directories(${target-name} PUBLIC
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${target-name}/include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	)
	
	add_library(${PROJECT_NAME}::${target-name} ALIAS ${target-name})
	
	install(TARGETS ${target-name} EXPORT ${PROJECT_NAME}-targets
		LIBRARY DESTINATION lib
		ARCHIVE DESTINATION lib
		RUNTIME DESTINATION bin
		INCLUDES DESTINATION include
	)
endfunction()

function(subgine_copy_required_dlls target-name)
	if(WIN32)
		add_custom_command(TARGET ${target-name} POST_BUILD
			COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_RUNTIME_DLLS:${target-name}> $<TARGET_FILE_DIR:${target-name}>
			COMMAND_EXPAND_LISTS
		)
	endif()
endfunction()

function(subgine_configure_asset_command target-name)
	add_executable(${PROJECT_NAME}::${target-name} ALIAS ${target-name})
	target_compile_features(${target-name} PUBLIC
		$<$<CXX_COMPILER_ID:MSVC>:cxx_std_23>
		$<$<NOT:$<CXX_COMPILER_ID:MSVC>>:cxx_std_20>
	)
	set_target_properties(${target-name}
	PROPERTIES
		RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin/"
	)

	install(TARGETS ${target-name} DESTINATION bin)
	install(TARGETS ${target-name} EXPORT ${PROJECT_NAME}-targets
		LIBRARY DESTINATION lib
		ARCHIVE DESTINATION lib
		RUNTIME DESTINATION bin
		INCLUDES DESTINATION include
	)
endfunction()

function(subgine_build_assets target-name)
	if(${ARGC} LESS 2)
		add_custom_target(${target-name})
		add_library(${PROJECT_NAME}::${target-name} INTERFACE IMPORTED GLOBAL)
		add_dependencies(${PROJECT_NAME}::${target-name} ${target-name})
		return()
	endif()
	
	if(NOT ${ARGV1} STREQUAL "ASSET")
		message(FATAL_ERROR "The first argument of the subgine_build_assets function must be ASSET")
	endif()
	
	add_custom_command(
		OUTPUT asset.json
		COMMAND subgine::generate-asset-metadata asset.json ${ARGN}
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
	)
	
	set(final-target-depends "")
	
	function(_subgine_build_assets_parse_args args)
		set(oneValueArgs "ASSET;TYPE")
		set(multiValueArgs "SOURCES;OPTIONS")
		cmake_parse_arguments(ASSET_ARGS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${args})
		cmake_language(CALL "subgine_asset_compile_${ASSET_ARGS_TYPE}" ASSET_OUTPUT "${ASSET_ARGS_ASSET}" "${ASSET_ARGS_SOURCES}" "${ASSET_ARGS_OPTIONS}")
		list(APPEND final-target-depends ${ASSET_OUTPUT})
		set(final-target-depends "${final-target-depends}" PARENT_SCOPE)
	endfunction()
	
	set(current-arg 0)
	set(begin-asset-arg 0)
	foreach(arg-value ${ARGV})
		if(${arg-value} STREQUAL "ASSET")
			set(end-asset-arg ${current-arg})
			math(EXPR before-end-asset-arg "${end-asset-arg} - 1")
			set(asset-args "")
			if(NOT ${begin-asset-arg} EQUAL 0)
				foreach(arg-to-parse RANGE ${begin-asset-arg} ${before-end-asset-arg})
					list(APPEND asset-args "${ARGV${arg-to-parse}}")
				endforeach()
				_subgine_build_assets_parse_args("${asset-args}")
			endif()
			set(begin-asset-arg ${end-asset-arg})
		endif()
		math(EXPR current-arg "${current-arg} + 1")
	endforeach()
	
	if(NOT ${begin-asset-arg} EQUAL 0)
		set(asset-args "")
		foreach(arg-to-parse RANGE ${begin-asset-arg} ${ARGC})
			list(APPEND asset-args "${ARGV${arg-to-parse}}")
		endforeach()
		_subgine_build_assets_parse_args("${asset-args}")
	endif()
	
	add_custom_target(${target-name} DEPENDS ${final-target-depends})
	
	add_library(${PROJECT_NAME}::${target-name} INTERFACE IMPORTED GLOBAL)
	add_dependencies(${PROJECT_NAME}::${target-name} ${target-name})
	
	function(_subgine_build_assets_generate_export asset-dir output-file)
		set(generated-asset-copy "")
		set(generated-target-depends "")
		foreach(file-output ${final-target-depends})
			cmake_path(RELATIVE_PATH file-output BASE_DIRECTORY "${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}" OUTPUT_VARIABLE asset-path)
			set(full-asset-path "${asset-dir}/${asset-path}")
			set(generated-target-output "\${PROJECT_BINARY_DIR}/share/\${PROJECT_NAME}/${asset-path}")
			string(APPEND generated-asset-copy "
add_custom_command(
	OUTPUT \"${generated-target-output}\"
	COMMAND \${CMAKE_COMMAND} -E copy
		\"${full-asset-path}\"
		\"${generated-target-output}\"
	DEPENDS \"${full-asset-path}\"
)
")
			list(APPEND generated-target-depends "\${PROJECT_BINARY_DIR}/share/\${PROJECT_NAME}/${asset-path}")
		endforeach()
		string(APPEND generated-asset-copy "
add_custom_target(${PROJECT_NAME}-${target-name}-copy DEPENDS \"${generated-target-depends}\")
add_library(${PROJECT_NAME}::${target-name} INTERFACE IMPORTED GLOBAL)
add_dependencies(${PROJECT_NAME}::${target-name} ${PROJECT_NAME}-${target-name}-copy)
")
		file(WRITE "${output-file}" "${generated-asset-copy}")
	endfunction()
	
	_subgine_build_assets_generate_export("${PROJECT_BINARY_DIR}/share/${PROJECT_NAME}" "${PROJECT_BINARY_DIR}/assets/${target-name}-export.cmake")
	_subgine_build_assets_generate_export("${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}" "${PROJECT_BINARY_DIR}/lib/cmake/${PROJECT_NAME}/${target-name}-export.cmake")
	install(
		FILES "${PROJECT_BINARY_DIR}/lib/cmake/${PROJECT_NAME}/${target-name}-export.cmake"
		DESTINATION "${CMAKE_INSTALL_DATADIR}/cmake/${PROJECT_NAME}/assets"
	)
	if(NOT EXISTS "${PROJECT_BINARY_DIR}/assets/asset-exports.txt")
		file(TOUCH "${PROJECT_BINARY_DIR}/assets/asset-exports.txt")
		install(
			FILES "${PROJECT_BINARY_DIR}/assets/asset-exports.txt"
			DESTINATION "${CMAKE_INSTALL_DATADIR}/cmake/${PROJECT_NAME}/assets"
		)
	endif()
	file(READ "${PROJECT_BINARY_DIR}/assets/asset-exports.txt" asset-export-list)
	list(APPEND asset-export-list "${target-name}")
	file(WRITE "${PROJECT_BINARY_DIR}/assets/asset-exports.txt" "${asset-export-list}")
endfunction()

if(EXISTS "${PROJECT_BINARY_DIR}/assets/asset-exports.txt")
	file(WRITE "${PROJECT_BINARY_DIR}/assets/asset-exports.txt" "")
endif()
