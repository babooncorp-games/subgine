#pragma once

#include "subgine/vector/vector.h"

#include <string>

namespace sbg {

struct WindowSettings {
	explicit WindowSettings(std::u8string title, Vector2i size, bool fullscreen);

	std::u8string title;
	Vector2i size;
	bool fullscreen = false;
};

} // namespace sbg
