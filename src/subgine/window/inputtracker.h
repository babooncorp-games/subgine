#pragma once

#include "event/keyboardevent.h"
#include "event/mousemoveevent.h"
#include "event/clickevent.h"
#include "event/joystickaxisevent.h"
#include "event/joystickbuttonevent.h"
#include "event/scrollevent.h"

#include <vector>
#include <array>

namespace sbg {

struct InputTrackerService;
struct EventDispatcher;

struct InputTracker {
	InputTracker(EventDispatcher& eventDispatcher);
	
	void handle(KeyboardEvent const& event);
	void handle(MouseMoveEvent const& event);
	void handle(ClickEvent const& event);
	void handle(JoystickAxisEvent const& event);
	void handle(JoystickButtonEvent const& event);
	void handle(ScrollEvent const& event);
	
	bool isKeyDown(KeyboardEvent::KeyCode key);
	bool isMouseDown(ClickEvent::Button button);

private:
	void trackKey(const KeyboardEvent& event) noexcept;
	
	EventDispatcher& _eventDispatcher;
	std::vector<bool> _keystate = std::vector<bool>(349, false);
	std::array<bool, 4> _mouseState = {};
	
	friend auto service_map(InputTracker const&) -> InputTrackerService;
};

} // namespace sbg
