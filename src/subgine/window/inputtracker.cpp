#include "inputtracker.h"

#include "subgine/event.h"
#include "window.h"

namespace sbg {

InputTracker::InputTracker(EventDispatcher& eventDispatcher) : _eventDispatcher{eventDispatcher} {}

void InputTracker::handle(const KeyboardEvent& event) {
	trackKey(event);
	_eventDispatcher.dispatch(event);
}

void InputTracker::handle(MouseMoveEvent const& event) {
	_eventDispatcher.dispatch(event);
}

void InputTracker::handle(ClickEvent const& event) {
	_mouseState[static_cast<std::size_t>(event.button)] = event.pressed;
	_eventDispatcher.dispatch(event);
}

void InputTracker::handle(JoystickAxisEvent const& event) {
	_eventDispatcher.dispatch(event);
}

void InputTracker::handle(JoystickButtonEvent const& event) {
	_eventDispatcher.dispatch(event);
}

void InputTracker::handle(ScrollEvent const& event) {
	_eventDispatcher.dispatch(event);
}

bool InputTracker::isKeyDown(KeyboardEvent::KeyCode key) {
	return _keystate[static_cast<std::size_t>(key)];
}

auto InputTracker::isMouseDown(ClickEvent::Button button) -> bool {
	return _mouseState[static_cast<std::size_t>(button)];
}

void InputTracker::trackKey(const KeyboardEvent& event) noexcept {
	if (event.keyCode != KeyboardEvent::KeyCode::Unknown) {
		_keystate[static_cast<std::size_t>(event.keyCode)] = event.pressed;
	}
}

} // namespace sbg
