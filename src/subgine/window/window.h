#pragma once

#include "subgine/event/eventdispatcher.h"
#include "subgine/graphic/view.h"
#include "windowsettings.h"

#include "joystick.h"

#include <array>
#include <optional>

namespace sbg {

struct WindowService;

struct Window {
	Window() = default;
	Window(Window&&) = default;
	
	void open(WindowSettings const& settings);
	void close();
	void toggleFullscreen();
	void handleEvents();
	void render();
	bool isOpen();
	void flip();
	Vector2i size() const;
	
	void addView(View& view);
	void removeView(View& view);
	
	const EventDispatcher& getEventDispatcher() const;
	EventDispatcher& getEventDispatcher();
	
private:
	struct GLFWWindowDeleter {
		void operator()(void* window);
	};
	
	struct Glfw {
		Glfw();
		Glfw(const Glfw&) = delete;
		Glfw& operator=(const Glfw&) = delete;
		Glfw(Glfw&&) noexcept;
		Glfw& operator=(Glfw&&) noexcept;
		~Glfw();
		
	private:
		bool shouldEnd = true;
	};
	
	EventDispatcher _eventDispatcher;
	std::vector<View*> _views;
	
	static auto initialize_joystick(int joystick_id) -> Joystick;
	auto glfw_window() const noexcept;
	
	Glfw _glfw;
	Vector2i _size;
	Vector2i _windowedSize;
	std::array<std::optional<Joystick>, 16> _joysticks;
	std::unique_ptr<void, GLFWWindowDeleter> _window;
	
	friend auto service_map(Window const&) -> WindowService;
};

} // namespace sbg
