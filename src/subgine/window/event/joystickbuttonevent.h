#pragma once

#include "../joystick.h"

namespace sbg {

struct JoystickButtonEvent {
	Joystick joystick;
	int nth_button;
	bool pressed;
	
};

} // namespace sbg
