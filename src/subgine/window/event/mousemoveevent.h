#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

struct MouseMoveEvent {
	explicit MouseMoveEvent(Vector2d _position, Vector2d _worldPosition);
	
	const Vector2d position;
	const Vector2d worldPosition;
};

} // namespace sbg
