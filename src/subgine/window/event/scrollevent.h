#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

struct ScrollEvent {
	sbg::Vector2d scroll;
};

} // namespace sbg
