#pragma once 

#include "subgine/vector/vector.h"

namespace sbg {

struct ClickEvent {
	enum class Button {
		None, Left, Right, Middle
	};
	
	sbg::Vector2d position;
	bool pressed;
	Button button;
};

} // namespace sbg
