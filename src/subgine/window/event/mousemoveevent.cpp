#include "mousemoveevent.h"

namespace sbg {

MouseMoveEvent::MouseMoveEvent(Vector2d _position, Vector2d _worldPosition) : position{_position}, worldPosition{_worldPosition} {}

} // namespace sbg
