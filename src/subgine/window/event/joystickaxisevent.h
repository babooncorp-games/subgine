#pragma once

#include "../joystick.h"

namespace sbg {

struct JoystickAxisEvent {
	Joystick joystick;
	int nth_axis;
	float value;
	float previous;
};

} // namespace sbg
