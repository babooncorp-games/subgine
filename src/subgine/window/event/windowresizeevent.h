#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

struct Window;

struct WindowResizeEvent {
	sbg::Vector2i size;
	Window& window;
};

} // namespace sbg
