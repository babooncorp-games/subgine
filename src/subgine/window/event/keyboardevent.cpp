#include "keyboardevent.h"

namespace sbg {

KeyboardEvent::KeyboardEvent(KeyCode _keyCode, bool _pressed) : keyCode{_keyCode}, pressed{_pressed} {}

} // namespace sbg
