#include "windowmodule.h"

#include "subgine/common/lookup_table.h"
#include "subgine/resource/utility/optionalvalue.h"

#include "subgine/provider.h"
#include "subgine/scene.h"
#include "subgine/window.h"
#include "subgine/log.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace sbg;
using namespace std::literals;

constexpr auto horizontalAlign = makeLookupTable<std::string_view, std::int32_t>({
	{"left", -1},
	{"center", 0},
	{"right", 1}
});

constexpr auto verticalAlign = makeLookupTable<std::string_view, std::int32_t>({
	{"bottom", -1},
	{"middle", 0},
	{"top", 1},
});

void WindowModule::setupPluggerCreator(PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("window-input", [](Window& window, InputTracker& input, Property) {
		return Plugger{
			[&window, &input] {
				window.getEventDispatcher().subscribe<KeyboardEvent>(input);
				window.getEventDispatcher().subscribe<MouseMoveEvent>(input);
				window.getEventDispatcher().subscribe<ClickEvent>(input);
				window.getEventDispatcher().subscribe<JoystickAxisEvent>(input);
				window.getEventDispatcher().subscribe<JoystickButtonEvent>(input);
				window.getEventDispatcher().subscribe<ScrollEvent>(input);
			},
			[&window, &input] {
				window.getEventDispatcher().unsubscribe(input);
			}
		};
	});
}

void WindowModule::setupProviderCreator(ProviderCreator<glm::mat4>& providerCreator) const {
	providerCreator.add("window-ortho-transform-fixed-height", [](Window& window, Entity entity, Property data) {
		auto const height = float{data["height"]};
		auto const hAlign = horizontalAlign[value_or(data["align"], "center"sv)];
		
		Log::trace(SBG_LOG_INFO, "Ortho projection created with height", height);
		
		return [&window, height, hAlign] {
			auto const size = window.size();
			
			return glm::ortho(
				((1 + hAlign) * size.x * (height / size.y)) / -2, ((1 - hAlign) * size.x * (height / size.y)) / 2,
				height / 2, height / -2,
				-1.f, 100.f
			);
		};
	});
	
	providerCreator.add("window-ortho-transform-fixed-width", [](Window& window, Entity entity, Property data) {
		auto const width = float{data["width"]};
		auto const hAlign = horizontalAlign[value_or(data["align"], "center"sv)];
		
		Log::trace(SBG_LOG_INFO, "Ortho projection created with width", width);
		
		return [&window, width, hAlign] {
			auto const size = window.size();
			
			return glm::ortho(
				(1 + hAlign) * width / -2, (1 - hAlign) * width / 2,
				(size.y * (width / size.x)) / 2, (size.y * (width / size.x)) / -2,
				-1.f, 100.f
			);
		};
	});
	
	providerCreator.add("window-ortho-transform", [](Window& window, Entity entity, Property data) {
		auto const hAlign = horizontalAlign[value_or(data["align"], "center"sv)];
		auto const vAlign = verticalAlign[value_or(data["valign"], "middle"sv)];
		
		return [&window, hAlign, vAlign] {
			auto const size = window.size();
			
			return glm::ortho(
				(1 + hAlign) * size.x / -2.f, (1 - hAlign) * size.x / 2.f,
				(1 + vAlign) * size.y / 2.f, (1 - vAlign) * size.y / -2.f,
				-1.f, 100.f
			);
		};
	});
}
