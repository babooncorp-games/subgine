#pragma once

#include <string_view>
#include <stdexcept>
#include <span>

namespace sbg {

struct Joystick {
	std::span<float const> axes;
	std::span<unsigned char const> buttons;
	std::string_view name;
	int id;
};

} // namespace sbg
