#include "windowsettings.h"

namespace sbg {

WindowSettings::WindowSettings(std::u8string _title, Vector2i _size, bool _fullscreen): title{_title}, size{_size}, fullscreen{_fullscreen} {}

} // namespace sbg
