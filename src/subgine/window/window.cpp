#include "window.h"
#include "windowsettings.h"

#include "event/clickevent.h"
#include "event/mousemoveevent.h"
#include "event/keyboardevent.h"
#include "event/joystickaxisevent.h"
#include "event/joystickbuttonevent.h"
#include "event/windowresizeevent.h"
#include "event/mouseenterevent.h"
#include "event/scrollevent.h"
#include "event/mouseleaveevent.h"

#include "subgine/graphic/view.h"
#include "subgine/event/eventdispatcher.h"
#include "subgine/log.h"

#include "GLFW/glfw3.h"

#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h>

#include <exception>
#include <algorithm>
#include <functional>

using namespace sbg;

auto Window::glfw_window() const noexcept {
	return static_cast<GLFWwindow*>(_window.get());
}

auto Window::initialize_joystick(int joystick_id) -> Joystick {
	int axes_count;
	auto const axes = glfwGetJoystickAxes(joystick_id, &axes_count);
	
	int buttons_count;
	auto const buttons = glfwGetJoystickButtons(joystick_id, &buttons_count);
	
	auto const name = glfwGetJoystickName(joystick_id);
	
	return Joystick{
		std::span{axes, axes + axes_count},
		std::span{buttons, buttons + buttons_count},
		name,
		joystick_id
	};
}

void Window::close() {
	glfwSetWindowShouldClose(glfw_window(), true);
}

void Window::handleEvents() {
	glfwPollEvents();
	for (auto& joystick : _joysticks) {
		if (!joystick) continue;
		
		auto previous_axes = std::vector<float>{};
		auto previous_buttons = std::vector<char>{};
		
		previous_axes.reserve(joystick->axes.size());
		previous_buttons.reserve(joystick->buttons.size());
		
		std::copy(std::begin(joystick->axes), std::end(joystick->axes), std::back_inserter(previous_axes));
		std::copy(std::begin(joystick->buttons), std::end(joystick->buttons), std::back_inserter(previous_buttons));
		
		int axes_count;
		auto const axes = glfwGetJoystickAxes(joystick->id, &axes_count);
		
		int buttons_count;
		auto const buttons = glfwGetJoystickButtons(joystick->id, &buttons_count);
		
		joystick->axes = std::span{axes, axes + axes_count};
		joystick->buttons = std::span{buttons, buttons + buttons_count};
		
		for (auto nth = 0 ; static_cast<std::size_t>(nth) < joystick->axes.size() ; ++nth) {
			if (previous_axes[nth] != joystick->axes[nth]) {
				_eventDispatcher.dispatch(JoystickAxisEvent{*joystick, nth, joystick->axes[nth], previous_axes[nth]});
			}
		}
		
		for (auto nth = 0 ; static_cast<std::size_t>(nth) < joystick->buttons.size() ; ++nth) {
			if (previous_buttons[nth] != joystick->buttons[nth]) {
				_eventDispatcher.dispatch(JoystickButtonEvent{*joystick, nth, static_cast<bool>(joystick->buttons[nth])});
			}
		}
	}
}

void Window::open(WindowSettings const& settings) {
	Log::warning(
		[&]{ return isOpen(); },
		SBG_LOG_INFO, "Opening an already opened window"
	);
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_SRGB_CAPABLE, true);
	
	auto title = std::string{};
	title.resize(settings.title.size());
	std::copy(settings.title.begin(), settings.title.end(), title.begin());
	
	if (settings.fullscreen) {
		const auto primary_monitor = glfwGetPrimaryMonitor();
		const auto mode = glfwGetVideoMode(primary_monitor);
		
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		
		_window = std::unique_ptr<GLFWwindow, GLFWWindowDeleter>(glfwCreateWindow(mode->width, mode->height, title.c_str(), primary_monitor, nullptr));
		_size = {mode->width, mode->height};
		glfwFocusWindow(glfw_window());
	} else {
		_size = settings.size;
		_window = std::unique_ptr<GLFWwindow, GLFWWindowDeleter>(glfwCreateWindow(settings.size.x, settings.size.y, title.c_str(), nullptr, nullptr));
	}
	
	_windowedSize = settings.size;
	
	Log::fatal([&]{ return !_window; }, SBG_LOG_INFO, "GLFW error. Window cannot be created.");
	
	glfwMakeContextCurrent(glfw_window());
	glfwSetWindowUserPointer(glfw_window(), this);
	
	glbinding::initialize(glfwGetProcAddress);
	
	gl::glEnable(gl::GL_FRAMEBUFFER_SRGB);
	
	
	gl::glEnable(gl::GL_BLEND);
	gl::glBlendFunc(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA);
	
	// Default viewport. Expected to be changed by render pipelines.
	gl::glViewport(0,0,settings.size.x, settings.size.y);
	gl::glEnable(gl::GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	gl::glDepthFunc(gl::GL_LEQUAL);
	gl::glDisable(gl::GL_POLYGON_SMOOTH);
	gl::glClearColor(0, 0, 0, 1);
	glfwSwapInterval(1);
	
	glfwSetKeyCallback(glfw_window(), [](GLFWwindow* window, int key, int scancode, int action, int mods){
		Window& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
		if (action != GLFW_REPEAT) {
			self._eventDispatcher.dispatch(KeyboardEvent{static_cast<KeyboardEvent::KeyCode>(key), action == GLFW_PRESS});
		}
	});
	
	glfwSetFramebufferSizeCallback(glfw_window(), [](GLFWwindow* window, int width, int height) {
		Window* self = static_cast<Window*>(glfwGetWindowUserPointer(window));
		self->_size = {width, height};
		self->_windowedSize = (glfwGetWindowMonitor(window) ? self->_windowedSize : self->_size);
		self->_eventDispatcher.dispatch(WindowResizeEvent{{width, height}, *self});
	});
	
	glfwSetCursorPosCallback(glfw_window(), [](GLFWwindow* window, double xpos, double ypos) {
		Window& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
		self._eventDispatcher.dispatch(MouseMoveEvent{{xpos, ypos}, Vector2d{}});
	});
	
	glfwSetCursorEnterCallback(glfw_window(), [](GLFWwindow* window, int entered) {
		auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
		if (entered == 1) {
			self._eventDispatcher.dispatch(MouseEnterEvent{});
		} else {
			self._eventDispatcher.dispatch(MouseLeaveEvent{});
		}
	});
	
	glfwSetMouseButtonCallback(glfw_window(), [](GLFWwindow* window, int button, int action, int mods) {
		auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
		
		auto pressedButton = ClickEvent::Button::None;
		
		Vector2d pos;
		glfwGetCursorPos(window, &pos.x, &pos.y);
		
		switch (button) {
			case GLFW_MOUSE_BUTTON_RIGHT:
				pressedButton = ClickEvent::Button::Right;
				break;
			
			case GLFW_MOUSE_BUTTON_LEFT:
				pressedButton = ClickEvent::Button::Left;
				break;
			
			case GLFW_MOUSE_BUTTON_MIDDLE:
				pressedButton = ClickEvent::Button::Middle;
				break;
				
			default:
				Log::debug(SBG_LOG_INFO, "Unknown mouse button", button);
				break;
		}
		
		self._eventDispatcher.dispatch(ClickEvent{.position = pos, .pressed = action == GLFW_PRESS, .button = pressedButton});
	});
	
	glfwSetScrollCallback(glfw_window(), [](GLFWwindow* window, double x, double y) {
		auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
		self._eventDispatcher.dispatch(ScrollEvent{sbg::Vector2d{x, y}});
	});
	
	for (int i = 0 ; i <= GLFW_JOYSTICK_LAST ; ++i) {
		_joysticks[i] = glfwJoystickPresent(GLFW_JOYSTICK_1 + i) ? initialize_joystick(GLFW_JOYSTICK_1 + i) : std::optional<Joystick>{};
	}
	
	// TODO: Remove in glfw 3.3
	static auto& window_this = *this;
	glfwSetJoystickCallback([](int joystick_id, int event) {
		if (event == GLFW_CONNECTED) {
			window_this._joysticks[joystick_id] = initialize_joystick(joystick_id);
		} else if (event == GLFW_DISCONNECTED) {
			window_this._joysticks[joystick_id] = {};
		}
	});
}

bool Window::isOpen() {
	return _window && !glfwWindowShouldClose(glfw_window());
}

void Window::toggleFullscreen() {
	const auto primary_monitor = glfwGetPrimaryMonitor();
	const auto mode = glfwGetVideoMode(primary_monitor);
	const auto fullscreen = glfwGetWindowMonitor(glfw_window()) != nullptr;
	const auto size = fullscreen ? _windowedSize : sbg::Vector2i{mode->width, mode->height};
	
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
	sbg::Log::debug(SBG_LOG_INFO, "Going", (fullscreen ? "windowed":"fullscreen"), "Size:", size);
	glfwSetWindowMonitor(glfw_window(), fullscreen ? nullptr : primary_monitor, 100, 100, size.x, size.y, fullscreen ? GLFW_DONT_CARE : mode->refreshRate);
}

void Window::render() {
	gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);
	
	for (auto& view : _views) {
		view->render();
	}
}

void Window::flip() {
	glfwSwapBuffers(glfw_window());
}

Vector2i Window::size() const {
	Vector2i size;
	
	glfwGetWindowSize(glfw_window(), &size.x, &size.y);
	
	return size;
}


void Window::addView(View& view) {
	_views.emplace_back(&view);
}

void Window::removeView(View& view) {
	_views.erase(
		std::remove(
			_views.begin(), _views.end(),
			&view
		),
		_views.end()
	);
}

const EventDispatcher& Window::getEventDispatcher() const {
	return _eventDispatcher;
}

EventDispatcher& Window::getEventDispatcher() {
	return _eventDispatcher;
}

void Window::GLFWWindowDeleter::operator()(void* window) {
	glfwDestroyWindow(static_cast<GLFWwindow*>(window));
}

Window::Glfw::Glfw() {
	glfwSetErrorCallback([](int code, const char* error) {
		Log::error(SBG_LOG_INFO, "GLFW error, code", code, ":", error);
	});
	
	if (!glfwInit()) {
		std::abort();
	}
}

auto Window::Glfw::operator=(Glfw&& other) noexcept -> Glfw& {
	std::swap(other.shouldEnd, shouldEnd);
	return *this;
}

Window::Glfw::Glfw(Glfw&& other) noexcept {
	other.shouldEnd = false;
}

Window::Glfw::~Glfw() {
	if (shouldEnd) {
		glfwTerminate();
	}
}
