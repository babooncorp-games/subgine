#pragma once

#include "../windowmodule.h"

#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/provider/service/providercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct WindowModuleService : kgr::single_service<WindowModule>,
	autocall<
		&WindowModule::setupPluggerCreator,
		&WindowModule::setupProviderCreator
	> {};

} // namespace sbg
