#pragma once

#include "../window.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct WindowService : kgr::single_service<Window>{};

} // namespace sbg
