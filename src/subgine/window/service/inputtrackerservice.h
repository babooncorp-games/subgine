#pragma once

#include "../inputtracker.h"

#include "subgine/event/service/eventdispatcherservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct InputTrackerService : kgr::single_service<InputTracker, kgr::autowire> {};

} // namespace sbg
