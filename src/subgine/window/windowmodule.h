#pragma once

#include <glm/glm.hpp>

namespace sbg {

struct WindowModuleService;

struct PluggerCreator;
template<typename>
struct ProviderCreator;

struct WindowModule {
	void setupPluggerCreator(PluggerCreator& pluggerCreator) const;
	void setupProviderCreator(ProviderCreator<glm::mat4>& providerCreator) const;
	
	friend auto service_map(WindowModule const&) -> WindowModuleService;
};

} // namespace sbg
