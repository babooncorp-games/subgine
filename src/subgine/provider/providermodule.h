#pragma once

namespace sbg {

struct ProviderModuleService;

struct ProviderModule {
	friend auto service_map(ProviderModule const&) -> ProviderModuleService;
};

} // namespace sbg
