#pragma once

#include <functional>
#include <variant>
#include <memory>
#include <algorithm>
#include <type_traits>

#include "subgine/common/traits.h"
#include "subgine/common/concepts.h"
#include "subgine/common/define.h"

namespace sbg::detail::provider {

struct ProviderBase {
protected:
	template<typename T>
	static constexpr std::size_t storage_align = std::max(alignof(T), sizeof(void*));
	
	template<typename T>
	static constexpr std::size_t storage_size = std::max(sizeof(T), sizeof(void*) * 2);
	
	template<typename T, typename U>
	static constexpr bool is_embeddable =
		    std::is_trivially_destructible_v<U>
		and std::is_trivially_copyable_v<U>
		and storage_size<T> >= sizeof(U)
		and storage_align<T> >= alignof(U);
	
	template<typename T>
	using call_operator_fptr = T(*)(void*) noexcept;
	
	template<typename T>
	struct LambdaStorage {
		explicit LambdaStorage(T&& l) noexcept : lambda{std::move(l)} {}
		explicit LambdaStorage(T const& l) noexcept(std::is_nothrow_copy_constructible_v<T>) : lambda{l} {}
		
		static auto call_operator(void* storage) noexcept -> std::invoke_result_t<T&> {
			return static_cast<LambdaStorage*>(storage)->lambda();
		}
		
	private:
		T lambda;
	};
	
	template<typename T>
	struct HeapStorage {
		using storage_t = std::unique_ptr<void, void(*)(void*)>;
		
		[[nodiscard]]
		static auto copy(HeapStorage const& other) -> storage_t {
			return other.clone(other.storage.get());
		}
		
		template<typename U, enable_if_i<is_not_self_v<HeapStorage, U>> = 0>
		explicit HeapStorage(U&& function) : storage{nullptr, nullptr} {
			assign(FWD(function));
		}
		
		HeapStorage(HeapStorage&&) = default;
		HeapStorage& operator=(HeapStorage&&) = default;
		
		HeapStorage(HeapStorage const& other) : clone{other.clone}, call{other.call}, storage{copy(other)} {}
		
		HeapStorage& operator=(HeapStorage const& other) {
			storage = copy(other);
			call = other.call;
			
			return *this;
		}
		
		template<typename L>
		static constexpr auto deleter() noexcept -> void(*)(void*) {
			return [](void* ptr) { delete static_cast<L*>(ptr); };
		}
		
		template<typename U, enable_if_i<!std::is_same_v<std::invoke_result_t<U>, T>> = 0>
		void assign(U&& function) {
			assign([f = FWD(function)]() -> T { return static_cast<T>(std::invoke(f)); });
		}
		
		template<typename U, enable_if_i<std::is_same_v<std::invoke_result_t<U>, T>> = 0>
		void assign(U&& function) {
			using L = LambdaStorage<std::decay_t<U>>;
			
			auto copy = [](void const* ptr) {
				auto function = std::make_unique<L>(*static_cast<L const*>(ptr));
				return storage_t{function.release(), deleter<L>()};
			};
			
			auto allocated = std::make_unique<L>(FWD(function));
			
			clone = copy;
			call = &L::call_operator;
			storage = storage_t{allocated.release(), deleter<L>()};
		}
		
		auto operator()() const -> T {
			return call(storage.get());
		}
		
		auto storage_pointer() const noexcept -> void* {
			return storage.get();
		}
		
		storage_t storage;
		storage_t(*clone)(void const*);
		call_operator_fptr<T> call;
	};
	
	template<typename T>
	struct InlineStorage {
		template<typename U, enable_if_i<is_not_self_v<InlineStorage, U>> = 0>
		explicit InlineStorage(U&& function) noexcept(std::is_nothrow_constructible_v<std::decay_t<U>, U>) {
			assign(std::forward<U>(function));
		}
		
		InlineStorage(InlineStorage&&) = default;
		InlineStorage& operator=(InlineStorage&&) = default;
		
		InlineStorage(InlineStorage const& other) = default;
		InlineStorage& operator=(InlineStorage const& other) = default;
		
		template<typename U, typename R = std::invoke_result_t<U>, enable_if_i<!std::is_same_v<R, T> && std::is_constructible_v<T, R>> = 0>
		void assign(U&& function) & noexcept(std::is_nothrow_constructible_v<std::decay_t<U>, U>) {
			assign([f = FWD(function)]() -> T { return static_cast<T>(std::invoke(f)); });
		}
		
		template<typename U, typename L = LambdaStorage<std::decay_t<U>>, enable_if_i<std::is_same_v<std::invoke_result_t<U>, T>> = 0>
		void assign(U&& function) & noexcept(std::is_nothrow_constructible_v<L, U>) {
			static_assert(is_embeddable<T, L>, "The callable object must be embeddable");
			
			call = &L::call_operator;
			new (&storage) L(std::forward<U>(function));
		}
		
		auto operator()() const& noexcept -> T {
			return call(&storage);
		}
		
		auto storage_pointer() const& noexcept -> void* {
			return &storage;
		}
		
		alignas(storage_align<T>) std::byte mutable storage[storage_size<T>];
		call_operator_fptr<T> call;
	};
};

} // namespace sbg::detail::provider

namespace sbg {
	
template<typename T, typename R>
concept provider_lambda = std::invocable<T> and std::is_invocable_r_v<R, T>;

template<typename T, typename U>
concept different = not std::same_as<T, U>;

template<typename T>
struct Provider : private detail::provider::ProviderBase {
	template<typename> friend struct Provider;
	
	Provider() = default;
	
	Provider(Provider const&) = default;
	Provider& operator=(Provider const&) = default;
	Provider(Provider&&) = default;
	Provider& operator=(Provider&&) = default;
	
	~Provider() = default;
	
	Provider(T&& value) : _storage{std::in_place_index<index_for<T>>, std::move(value)} {}
	Provider(T const& value) : _storage{std::in_place_index<index_for<T>>, value} {}
	
	template<typename U> requires different_from<T, Provider> and different<T, std::decay_t<U>> and provider_lambda<U&&, T>
	Provider(U&& lambda) : _storage{std::in_place_index<index_for<std::decay_t<U>>>, FWD(lambda)} {}
	
	template<typename U> requires different_from<T, Provider> and different<T, std::decay_t<U>> and std::is_convertible_v<U, T>
	Provider(U&& value) : _storage{std::in_place_index<index_for<std::decay_t<U>>>, FWD(value)} {}
	
	template<typename U> requires different<U, T>
	explicit Provider(Provider<U> const& other) : _storage{std::visit(from_other<U>(), other._storage)} {}
	
	template<typename U> requires different<U, T>
	explicit Provider(Provider<U>&& other) : _storage{std::visit(from_other<U>(), std::move(other._storage))} {}
	
	auto operator()() const -> T {
		return std::visit([](auto&& f) -> T {
			if constexpr (std::is_same_v<T, std::decay_t<decltype(f)>>) {
				return f;
			} else {
				return std::invoke(f);
			}
		}, _storage);
	}
	
	template<typename F>
	auto visit_storage(F visitor) const& noexcept -> decltype(auto) {
		return std::visit(visitor, _storage);
	}
	
private:
	using storage_t = std::variant<T, InlineStorage<T>, HeapStorage<T>>;
	
	template<typename F, typename U = T>
	static constexpr std::size_t index_for = std::is_convertible_v<F, U> ? 0 : is_embeddable<U, F> ? 1 : 2;
	
	template<typename U>
	static constexpr auto from_other() noexcept {
		return [](auto&& f) {
			using F = std::decay_t<decltype(f)>;
			return storage_t{std::in_place_index<index_for<F, U>>, FWD(f)};
		};
	}
	
	storage_t _storage;
};

} // namespace sbg

#include "subgine/common/undef.h"
