#pragma once

#include "../provider.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

template<typename T>
struct ProviderCreatorService;

template<typename T>
struct ProviderCreator : Creator<DefaultBuilder<Provider<T>>> {
	using Parent = Creator<DefaultBuilder<Provider<T>>>;
	using Parent::create;
	
	friend auto service_map(ProviderCreator const&) -> ProviderCreatorService<T>;
};

template<typename T>
using ProviderFactory = Factory<ProviderCreator<T>>;

} // namespace sbg
