#pragma once

#include "../creator/providercreator.h"
#include "subgine/resource/service/factoryservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

template<typename T>
struct ProviderCreatorService : kgr::single_service<ProviderCreator<T>> {};

template<typename T>
using ProviderFactoryService = FactoryService<ProviderFactory<T>>;

} // namespace sbg
