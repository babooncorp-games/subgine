#pragma once

#include "subgine/common/kangaru.h"
#include "../providermodule.h"

namespace sbg {

struct ProviderModuleService : kgr::single_service<ProviderModule> {};

} // namespace sbg
