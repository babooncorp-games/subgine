#pragma once

#include "../gameloopmodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct GameloopModuleService : kgr::single_service<GameloopModule> {};

auto service_map(GameloopModule const&) -> GameloopModuleService;

} // namespace sbg
