#pragma once

#include "../gameloop.h"

#include "subgine/window/service/windowservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct GameloopService : kgr::single_service<Gameloop, kgr::autowire> {};

auto service_map(Gameloop const&) -> GameloopService;

} // namespace sbg
