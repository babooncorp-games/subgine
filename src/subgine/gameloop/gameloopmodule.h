#pragma once

namespace sbg {

/*
 * This class is the module class for game module.
 * It does nothing for the moment.
 */
struct GameloopModule {};

} // namespace sbg
