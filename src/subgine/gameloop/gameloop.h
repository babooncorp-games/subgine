#pragma once

namespace sbg {

struct Window;
struct MainEngine;

/*
 * This class has th ejob of running the main loop of the game.
 * It tells the window to process events, executes engines and tells th ewindow to draw the game.
 */
struct Gameloop {
	Gameloop(MainEngine& engine, Window& window);
	
	/*
	 * This function starts the main loop.
	 */
	void run();
	
private:
	MainEngine& _engine;
	Window& _window;
};

} // namespace sbg
