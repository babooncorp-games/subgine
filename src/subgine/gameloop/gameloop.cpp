#include "gameloop.h"

#include "subgine/window.h"
#include "subgine/system.h"

namespace sbg {

Gameloop::Gameloop(MainEngine& engine, Window& window): _engine{engine}, _window{window} {}

void Gameloop::run() {
	_engine.reset();
	while (_window.isOpen()) {
		_window.handleEvents();
		
		_engine.update();
		_window.render();
		_window.flip();
	}
}

} // namespace sbg
