#pragma once

#include "../framemodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct FrameModuleService : kgr::single_service<FrameModule> {};

} // namespace sbg

