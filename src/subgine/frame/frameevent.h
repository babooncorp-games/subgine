#pragma once

#include "subgine/system/deferredscheduler.h"
#include "subgine/entity/entity.h"

#include <concepts>

#include "subgine/common/define.h"

namespace sbg {

struct DefaultEvent {
	auto operator()(auto&&...) const noexcept -> void {}
};

template<typename Start, typename Event, typename Finish>
struct FrameEvent {
	FrameEvent(DeferredScheduler& scheduler, Start start, Finish finish) noexcept requires(std::default_initializable<Event>) :
		_scheduler{&scheduler},
		_start{std::move(start)},
		_event{},
		_finish{std::move(finish)} {}
	
	FrameEvent(DeferredScheduler& scheduler, Start start, Event event, Finish finish) noexcept :
		_scheduler{&scheduler},
		_start{std::move(start)},
		_event{std::move(event)},
		_finish{std::move(finish)} {}
	
	FrameEvent(DeferredScheduler& scheduler, Entity owner, Start start, Finish finish) noexcept requires(std::default_initializable<Event>) :
		_scheduler{&scheduler},
		_owner{owner},
		_start{std::move(start)},
		_event{},
		_finish{std::move(finish)} {}
	
	FrameEvent(DeferredScheduler& scheduler, Entity owner, Start start, Event event, Finish finish) noexcept :
		_scheduler{&scheduler},
		_owner{owner},
		_start{std::move(start)},
		_event{std::move(event)},
		_finish{std::move(finish)} {}
	
	template<typename... Args> requires (std::invocable<Start, std::add_lvalue_reference_t<Args>...> and std::invocable<Event, Args...>)
	auto operator()(Args&&... args) -> void {
		if (_state == State::inactive) {
			_start(args...);
		}
		
		_event(FWD(args)...);
		
		_scheduler->defer([this, owner = _owner] {
			if (not owner.definitely_dead() and not owner.alive()) return;
			end_of_frame();
		});
		
		_state = State::event_triggered;
	}

private:
	auto end_of_frame() -> void {
		_state = State::active_event;
		
		_scheduler->defer([this, owner = _owner] {
			if (not owner.definitely_dead() and not owner.alive()) return;
			if (_state == State::active_event) {
				_finish();
				_state = State::inactive;
			}
		});
	}
	
	enum struct State : std::uint8_t { inactive, active_event, event_triggered };
	
	DeferredScheduler* _scheduler;
	
	State _state = State::inactive;
	Entity _owner;
	
	NO_UNIQUE_ADDRESS
	Start _start;
	
	NO_UNIQUE_ADDRESS
	Event _event;
	
	NO_UNIQUE_ADDRESS
	Finish _finish;
};

template<typename Start, typename Finish>
FrameEvent(DeferredScheduler&, Start, Finish) -> FrameEvent<Start, DefaultEvent, Finish>;

template<typename Start, typename Finish>
FrameEvent(DeferredScheduler&, Entity, Start, Finish) -> FrameEvent<Start, DefaultEvent, Finish>;

template<typename Start, typename Event, typename Finish>
FrameEvent(DeferredScheduler&, Start, Event, Finish) -> FrameEvent<Start, Event, Finish>;

template<typename Start, typename Event, typename Finish>
FrameEvent(DeferredScheduler&, Entity, Start, Event, Finish) -> FrameEvent<Start, Event, Finish>;

} // namespace sbg

#include "subgine/common/undef.h"
