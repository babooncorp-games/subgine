#pragma once

namespace sbg {

struct FrameModuleService;

struct FrameModule {
	friend auto service_map(FrameModule const&) -> FrameModuleService;
};

} // namespace sbg
