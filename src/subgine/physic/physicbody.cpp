#include "physicbody.h"

#include <cmath>

namespace sbg {

constexpr double correction = 12;

template<dim_t n>
PhysicBody<n>::PhysicBody(PhysicBody<n>&& other) noexcept :
	PhysicPoint<n>{std::move(other)},
	_pulsesPosition{std::move(other._pulsesPosition)},
	_forcesPosition{std::move(other._forcesPosition)},
	_pulsesPositionAccumulator{std::move(other._pulsesPositionAccumulator)},
	_orientation{other._orientation},
	_angularVelocity{other._angularVelocity},
	_torque{other._torque},
	_pulseAccumulationMutex{}
{}

template<dim_t n>
PhysicBody<n>& PhysicBody<n>::operator=(PhysicBody<n>&& other) noexcept {
	static_cast<PhysicPoint<n>&>(*this) = std::move(other);
	_pulsesPosition = std::move(other._pulsesPosition);
	_forcesPosition = std::move(other._forcesPosition);
	_pulsesPositionAccumulator = std::move(other._pulsesPositionAccumulator);
	_orientation = other._orientation;
	_angularVelocity = other._angularVelocity;
	_torque = other._torque;
	
	return *this;
}

template<dim_t n>
PhysicBody<n>::PhysicBody() = default;

template<dim_t n>
PhysicBody<n>::~PhysicBody() = default;

template<>
void PhysicBody<2>::update(Time time) {
	_orientation = std::fmod(getNextOrientation(time.current), tau);
	_angularVelocity = getNextAngularVelocity(time.current);
	_pulsesPosition.clear();
	_torque = getNextTorque();

	this->_position = getNextPosition(time.current);
	this->_velocity = getNextVelocity(time.current);
	this->_pulses.clear();
	_pulsesPositionAccumulator.clear();
	this->_pulseAccumulators.clear();
	this->_forces = getNextForces();
}

template<>
void PhysicBody<3>::update(Time time) {
	Angle nextOrientation = getNextOrientation(time.current);
	_orientation.x = std::fmod(nextOrientation.x, tau);
	_orientation.y = std::fmod(nextOrientation.y, tau);
	_orientation.z = std::fmod(nextOrientation.z, tau);
	_angularVelocity = getNextAngularVelocity(time.current);
	_pulsesPosition.clear();
	_torque = getNextTorque();

	this->_position = getNextPosition(time.current);
	this->_velocity = getNextVelocity(time.current);
	this->_pulses.clear();
	_pulsesPositionAccumulator.clear();
	this->_pulseAccumulators.clear();
	this->_forces = getNextForces();
}

template<dim_t n>
Vector<n, double> PhysicBody<n>::getNextVelocity(double time) const {
	Vector<n, double> velocity = this->_velocity;

	for (auto i : this->getNextForces()) {
		Vector<n, double> position = getForcePosition(i.first);

		if (!position.null() && !i.second.null()) {
			Angle torque = position.cross(i.second) / correction;
			i.second *= 1 / ((torque.length() * (1.0 / getMomentOfInertia())) + Vector<freedom(n), double>{1});
		}
		
		velocity += (i.second / this->_mass) * time;
	}
	
	for (auto i : this->getNextPulses()) {
		Vector<n, double> pulse = i.second;
		Vector<n, double> position = getPulsePosition(i.first);

		if (!position.null() && !pulse.null()) {
			Angle torque = position.cross(pulse) / correction;
			pulse *= 1 / ((torque.length() / getMomentOfInertia()) + Vector<freedom(n), double>{1});
		}

		velocity += (pulse / this->_mass);
	}

	return velocity;
}

template<dim_t n>
std::map<std::string, Vector<n, double>> PhysicBody<n>::getNextForces() const {
	auto forces = this->_forces;

	for (auto& i : this->_rules) {
		forces[i.first] = i.second->getResult(*this);
	}

	return forces;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getNextOrientation(double time) const {
	return _orientation + (getNextAngularVelocity(time)) * time;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getOrientation() const {
	return _orientation;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getAngularVelocity() const {
	return _angularVelocity;
}

template<dim_t n>
void PhysicBody<n>::setOrientation(Angle orientation) {
	_orientation = orientation;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getNextAngularVelocity(double time) const {
	Angle velocity = Angle();
	Angle torquePulse = Angle();

	velocity = (getNextTorque() * (1/getMomentOfInertia())) * time;
	
	auto pulses = this->getNextPulses();

	for (auto forcePos : getNextPulsesPositions()) {
		Vector<n, double> pulse = pulses[forcePos.first];
		Vector<n, double> position = forcePos.second;

		if (!position.null() && !pulse.null()) {
			Angle torque = position.cross(pulse) / correction;
			torquePulse += torque;
		}
	}

	velocity += torquePulse * (1/ getMomentOfInertia());

	return _angularVelocity + (velocity / 1.001) * tau;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getTorque() const {
	return _torque;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getNextTorque() const {
	Angle torque = Angle();

	for (auto forcePos : _forcesPosition) {
		Vector<n, double> force = this->getForce(forcePos.first);

		if (!forcePos.second.null() && !force.null()) {
			Angle forceTorque = forcePos.second.cross(force) / correction;
			torque += forceTorque;
		}
	}

	return torque;
}

template<dim_t n>
Vector<freedom(n), double> PhysicBody<n>::getMomentOfInertia() const {
	return Vector<freedom(n), double>{this->_mass};
}

template<dim_t n>
void PhysicBody<n>::setPulse(const std::string type, const Vector<n, double> pulse, const Vector<n, double> position) {
	this->_pulses[type] = pulse;
	_pulsesPosition[type] = position;
}

template<dim_t n>
void PhysicBody<n>::setForce(const std::string type, const Vector<n, double> force, const Vector<n, double> position) {
	this->_forces[type] = force;
	_forcesPosition[type] = position;
}

template<dim_t n>
Vector<n, double> PhysicBody<n>::getPulsePosition(std::string type) const {
	auto pulsePosition = getNextPulsesPositions();
	
	auto it = pulsePosition.find(type);

	if (it != pulsePosition.end()) {
		return it->second;
	}

	return {};
}

template<dim_t n>
Vector<n, double> PhysicBody<n>::getForcePosition(std::string type) const {
	auto it = _forcesPosition.find(type);

	if (it != _forcesPosition.end()) {
		return it->second;
	}

	return {};
}

template<dim_t n>
void PhysicBody<n>::accumulatePulse(const std::string type, Vector<n, double> pulse) {
	accumulatePulse(type, pulse, {});
}

template<dim_t n>
void PhysicBody<n>::accumulatePulse(const std::string type, Vector<n, double> pulse, Vector<n, double> position) {
	std::lock_guard<std::mutex> lock{_pulseAccumulationMutex};
	PhysicPoint<n>::accumulatePulse(type, pulse);
	_pulsesPositionAccumulator[type].first += position * pulse.length();
	_pulsesPositionAccumulator[type].second += pulse.length();
	
}

template<dim_t n>
std::map<std::string, Vector<n, double>> PhysicBody<n>::getNextPulsesPositions() const {
	auto pulsePositions = _pulsesPosition;
	
	for (auto accumulator : _pulsesPositionAccumulator) {
		if (accumulator.second.second > 0) {
			pulsePositions[accumulator.first] += accumulator.second.first / accumulator.second.second;
		}
	}
	
	return pulsePositions;
}

template struct PhysicBody<2>;
template struct PhysicBody<3>;

} // namespace sbg
