#include "physicengine.h"

#include "../physicbody.h"
#include "../physicpoint.h"

#include "subgine/graphic.h"
#include "subgine/entity.h"

#include <algorithm>

namespace sbg {

void PhysicEngine::add(Entity object) {
	// TODO: Remove physic
	// TODO: Don't hardcode the type list
	if (object.has<PhysicPoint2D>()) {
		_objects.emplace_back(object, Component<PhysicPoint2D>{object}, Component<Transform>{object});
	} else if (object.has<PhysicBody2D>()) {
		_objects.emplace_back(object, Component<PhysicBody2D>{object}, Component<Transform>{object});
	} else if (object.has<PhysicPoint3D>()) {
		_objects.emplace_back(object, Component<PhysicPoint3D>{object}, Component<Transform>{object});
	} else if (object.has<PhysicBody3D>()) {
		_objects.emplace_back(object, Component<PhysicBody3D>{object}, Component<Transform>{object});
	}
}

void PhysicEngine::remove(Entity object) noexcept {
	_objects.erase(
		std::remove_if(_objects.begin(), _objects.end(),
			[&](auto const& entry) {
				return entry.entity == object;
			}
		), _objects.end()
	);
}

void PhysicEngine::owner(OwnershipToken owner) noexcept {
	_owner = std::move(owner);
}

void PhysicEngine::execute(Time time) {
	_objects.erase(std::remove_if(_objects.begin(), _objects.end(), [](auto const& object) {
		return !object.entity;
	}), _objects.end());
	
	for (auto& object : _objects) {
		std::visit(
			[&]<typename T>(Component<T> const& component) {
				auto& physic = component.get();
				physic.update(time);
				if (auto const transform = object.transform) {
					if constexpr (std::is_same_v<T, PhysicPoint2D>) {
						*transform = transform->position2d(physic.getPosition());
					} else if constexpr (std::is_same_v<T, PhysicBody2D>) {
						*transform = transform->position2d(physic.getPosition());
						*transform = transform->orientation2d(physic.getOrientation());
					} else if constexpr (std::is_same_v<T, PhysicPoint3D>) {
						*transform = transform->position3d(physic.getPosition());
					} else if constexpr (std::is_same_v<T, PhysicBody3D>) {
						*transform = transform->position3d(physic.getPosition());
						// *transform = transform->orientation3d(p.getOrientation()); // lol
					}
				}
			},
			object.physic
		);
	}
}

} // namespace sbg
