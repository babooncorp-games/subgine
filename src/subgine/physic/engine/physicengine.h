#pragma once

#include <vector>
#include <memory>

#include "subgine/entity/entity.h"
#include "subgine/entity/component.h"
#include "subgine/system/time.h"
#include "subgine/common/types.h"
#include "subgine/common/ownershiptoken.h"

#include <variant>

namespace sbg {

struct PhysicEngineService;

struct AbstractPhysicPoint;

template<dim_t n>
struct PhysicPoint;

template<dim_t n>
struct PhysicBody;

struct Transform;

struct PhysicEngine {
	void add(Entity entity);
	void remove(Entity entity) noexcept;
	
	void execute(Time time);
	void owner(OwnershipToken owner) noexcept;
	
private:
	struct Entry {
		Entity entity;
		std::variant<
			Component<PhysicPoint<2>>,
			Component<PhysicPoint<3>>,
			Component<PhysicBody<2>>,
			Component<PhysicBody<3>>
		> physic;
		Component<Transform> transform;
	};
	
	OwnershipToken _owner;
	std::vector<Entry> _objects;
	
	friend auto service_map(PhysicEngine const&) -> PhysicEngineService;
};

} // namespace sbg
