#pragma once

#include "../engine/physicengine.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct PhysicEngineService : kgr::single_service<PhysicEngine> {};

} // namespace sbg
