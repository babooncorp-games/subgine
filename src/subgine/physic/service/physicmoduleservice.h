#pragma once

#include "../physicmodule.h"

#include "physicengineservice.h"
#include "physicpointcreatorservice.h"

#include "subgine/system/service/mainengineservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct PhysicModuleService : kgr::single_service<PhysicModule>,
	autocall<
		&PhysicModule::setupMainEngine,
		&PhysicModule::setupEntityBindingCreator,
		&PhysicModule::setupComponentCreator,
		&PhysicModule::setupPhysicPointCreator<2>,
		&PhysicModule::setupPhysicPointCreator<3>,
		&PhysicModule::setupRuleCreator<2>,
		&PhysicModule::setupRuleCreator<3>
	> {};

} // namespace sbg
