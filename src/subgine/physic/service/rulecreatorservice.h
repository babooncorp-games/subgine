#pragma once

#include "../creator/rulecreator.h"

#include "subgine/common/kangaru.h"
#include "subgine/resource/service/factoryservice.h"

namespace sbg {

template<dim_t n>
struct RuleCreatorService : kgr::single_service<RuleCreator<n>> {};

using Rule2DCreatorService = RuleCreatorService<2>;
using Rule3DCreatorService = RuleCreatorService<3>;

template<dim_t n>
using RuleFactoryService = FactoryService<RuleFactory<n>>;

using Rule2DFactoryService = RuleFactoryService<2>;
using Rule3DFactoryService = RuleFactoryService<3>;

} // namespace sbg
