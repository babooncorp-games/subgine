#pragma once

#include "../creator/physicpointcreator.h"
#include "rulecreatorservice.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

template<dim_t n>
struct PhysicPointCreatorService : kgr::single_service<PhysicPointCreator<n>, kgr::autowire> {};

using PhysicPoint2DCreatorService = PhysicPointCreatorService<2>;
using PhysicPoint3DCreatorService = PhysicPointCreatorService<3>;

template<dim_t n>
using PhysicPointFactoryService = FactoryService<PhysicPointFactory<n>>;

using PhysicPoint2DFactoryService = PhysicPointFactoryService<2>;
using PhysicPoint3DFactoryService = PhysicPointFactoryService<3>;

} // namespace sbg
