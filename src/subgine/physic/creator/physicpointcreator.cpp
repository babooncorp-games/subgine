#include "physicpointcreator.h"

#include "rulecreator.h"
#include "../physicpoint.h"

#include "subgine/resource.h"

namespace sbg {

template<int n>
PhysicPointCreator<n>::PhysicPointCreator(RuleCreator<n>& ruleCreator) :
	Parent{PhysicPointApply<n>{ruleCreator}} {}

template<int n>
void PhysicPointCreator<n>::create(kgr::invoker invoker, Entity entity, Property data) {
	build(invoker, std::string_view{data["type"]}, entity, data);
}

template struct PhysicPointCreator<2>;
template struct PhysicPointCreator<3>;

} // namespace sbg
