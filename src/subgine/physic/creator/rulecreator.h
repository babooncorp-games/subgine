#pragma once

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

template<dim_t>
struct Rule;

template<dim_t n>
struct RuleCreatorService;

template<dim_t n>
struct RuleCreator : Creator<SimpleBuilder<std::unique_ptr<Rule<n>>>> {
	using Parent = Creator<SimpleBuilder<std::unique_ptr<Rule<n>>>>;
	using Parent::create;
	
	friend auto service_map(RuleCreator const&) -> RuleCreatorService<n>;
};

template<dim_t n>
using RuleFactory = Factory<RuleCreator<n>>;

using Rule2DFactory = RuleFactory<2>;
using Rule3DFactory = RuleFactory<3>;

extern template struct RuleCreator<2>;
extern template struct RuleCreator<3>;

using Rule2DCreator = RuleCreator<2>;
using Rule3DCreator = RuleCreator<3>;

} // namespace sbg
