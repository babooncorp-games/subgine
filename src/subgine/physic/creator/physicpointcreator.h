#pragma once

#include "../detail/physicpointapply.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/entity/entity.h"

namespace sbg {

template<dim_t>
struct RuleCreator;

template<dim_t>
struct PhysicPoint;

template<dim_t n>
struct PhysicPointCreatorService;

template<dim_t n>
struct PhysicPointCreator : Creator<VoidBuilder, PhysicPointApply<n>> {
	PhysicPointCreator(RuleCreator<n>& ruleCreator);
	
	void create(kgr::invoker invoker, Entity entity, Property data);
	
private:
	using Parent = Creator<VoidBuilder, PhysicPointApply<n>>;
	using Creator<VoidBuilder, PhysicPointApply<n>>::build;
	
	friend auto service_map(PhysicPointCreator const&) -> PhysicPointCreatorService<n>;
};

template<dim_t n>
using PhysicPointFactory = Factory<PhysicPointCreator<n>>;

using PhysicPoint2DFactory = PhysicPointFactory<2>;
using PhysicPoint3DFactory = PhysicPointFactory<3>;

extern template struct PhysicPointCreator<2>;
extern template struct PhysicPointCreator<3>;

using PhysicPoint2DCreator = PhysicPointCreator<2>;
using PhysicPoint3DCreator = PhysicPointCreator<3>;

} // namespace sbg
