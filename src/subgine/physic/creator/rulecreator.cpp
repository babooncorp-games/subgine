#include "rulecreator.h"

#include "../rule/rule.h"

namespace sbg {

template struct RuleCreator<2>;
template struct RuleCreator<3>;

} // namespace sbg
