#pragma once

#include <memory>

#include "physicpoint.h"
#include "subgine/common/types.h"
#include "subgine/system/time.h"

namespace sbg {

template<dim_t n>
struct PhysicBody : PhysicPoint<n> {
	using Angle = Vector<freedom(n), double>;
	
	PhysicBody();
	PhysicBody(const PhysicBody&) = delete;
	PhysicBody& operator=(const PhysicBody&) = delete;
	PhysicBody(PhysicBody&&) noexcept;
	PhysicBody& operator=(PhysicBody&&) noexcept;
	~PhysicBody();

	void update(Time time);
	
	Vector<n, double> getNextVelocity(double time) const override;
	std::map<std::string, Vector<n, double>> getNextForces() const override;

	Angle getTorque() const;
	Angle getOrientation() const;
	Angle getAngularVelocity() const;
	
	Angle getNextOrientation(double time) const;
	Angle getNextAngularVelocity(double time) const;
	Angle getNextTorque() const;

	void setOrientation(Angle orientation);
	
	Angle getMomentOfInertia() const;

	void accumulatePulse(const std::string type, Vector<n, double> pulse) override;
	void accumulatePulse(const std::string type, Vector<n, double> pulse, Vector<n, double> position = Vector<n, double>());
	void setPulse(const std::string type, const Vector<n, double> pulse, const Vector<n, double> position = Vector<n, double>());
	void setForce(const std::string type, const Vector<n, double> force, const Vector<n, double> position = Vector<n, double>());
	Vector<n, double> getPulsePosition(std::string type) const;
	Vector<n, double> getForcePosition(std::string type) const;

protected:
	std::map<std::string, Vector<n, double>> getNextPulsesPositions() const;
	std::map<std::string, Vector<n, double>> _pulsesPosition;
	std::map<std::string, Vector<n, double>> _forcesPosition;
	std::map<std::string, std::pair<Vector<n, double>, double>> _pulsesPositionAccumulator;
	std::mutex _pulseAccumulationMutex;

	Angle _orientation;
	Angle _angularVelocity;
	Angle _torque;
};

template <> void PhysicBody<2>::update(Time time);
template <> void PhysicBody<3>::update(Time time);

extern template struct PhysicBody<2>;
extern template struct PhysicBody<3>;

using PhysicBody2D = PhysicBody<2>;
using PhysicBody3D = PhysicBody<3>;

} // namespace sbg
