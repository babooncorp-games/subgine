#include "abstractphysicpoint.h"

namespace sbg {

double AbstractPhysicPoint::getMass() const {
	return _mass;
}

void AbstractPhysicPoint::setMass(const double mass) {
	_mass = mass;
}

} // namespace sbg
