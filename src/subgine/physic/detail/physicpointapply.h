#pragma once

#include "../creator/rulecreator.h"

#include "subgine/entity/entity.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"
#include "subgine/resource/property.h"
#include "subgine/common/traits.h"
#include "subgine/common/dropping_invoke.h"
#include "subgine/vector/vector.h"

namespace sbg {

template<dim_t n>
struct PhysicPointApply {
	explicit PhysicPointApply(RuleCreator<n>& ruleCreator) noexcept : _ruleCreator{ruleCreator} {}
	
	template<typename I, typename B, enable_if_i<!std::is_void_v<dropping_invoke_result_t<I, B&, Entity, Property>>> = 0>
	void operator()(I invoker, B& builder, Entity entity, Property data) const {
		auto physic = dropping_invoke(invoker, builder, entity, data);
		setup(invoker, physic, data);
		entity.assign(std::move(physic));
	}
	
private:
	template<typename I, typename T>
	void setup(I invoker, T& physic, Property data) const {
		physic.setMass(double{data["mass"]});
		
		if (!data["velocity"].isNull()) {
			physic.setVelocity(Vector<n, double>{data["velocity"]});
		}
		
		if (!data["position"].isNull()) {
			physic.setPosition(Vector<n, double>{data["position"]});
		}
		
		for (auto const rule : data["rules"]) {
			physic.setRule(std::string{rule["name"]}, _ruleCreator.create(invoker, std::string{rule["type"]}, rule["data"]));
		}
	}
	
	RuleCreator<n>& _ruleCreator;
};

} // namespace sbg
