#pragma once

#include "subgine/vector/vectorfwd.h"

namespace sbg {

struct PhysicModuleService;

template<dim_t>
struct PhysicPointCreator;
template<dim_t>
struct RuleCreator;
struct EntityBindingCreator;
struct ComponentCreator;
struct MainEngine;
struct PhysicEngine;

struct PhysicModule {
	void setupMainEngine(MainEngine& mainEngine, PhysicEngine& physicEngine) const;
	void setupEntityBindingCreator(EntityBindingCreator& entityBindingCreator) const;
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	
	template<dim_t n>
	void setupPhysicPointCreator(PhysicPointCreator<n>& ppf) const;
	
	template<dim_t n>
	void setupRuleCreator(RuleCreator<n>& rf) const;
	
	friend auto service_map(PhysicModule const&) -> PhysicModuleService;
};

} // namespace sbg
