#pragma once

#include "physic/service.h"

#include "../../creator/physicpointcreator.h"
#include "../../creator/rulecreator.h"

#include "../../engine/physicengine.h"

#include "../../rule/attraction.h"
#include "../../rule/gravity.h"
#include "../../rule/resistance.h"
#include "../../rule/rubber.h"
#include "../../rule/rule.h"
#include "../../rule/spring.h"

#include "../../abstractphysicpoint.h"
#include "../../physicbody.h"
#include "../../physicpoint.h"
#include "../../pulseaccumulator.h"
