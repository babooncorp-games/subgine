#include "physicmodule.h"

#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/provider.h"
#include "subgine/entity.h"
#include "subgine/system.h"
#include "subgine/tiled.h"

namespace sbg {

void PhysicModule::setupMainEngine(MainEngine& mainEngine, PhysicEngine& physicEngine) const {
	mainEngine.add(physicEngine);
}

void PhysicModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("physicpoint", [](PhysicPoint2DFactory ppf, Entity entity, Property data){
		ppf.create(entity, data);
	});
}

void PhysicModule::setupEntityBindingCreator(EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("physic", [](PhysicEngine& engine, Entity entity) {
		engine.add(entity);
	});
}

template<dim_t n>
void PhysicModule::setupPhysicPointCreator(PhysicPointCreator<n>& ppf) const {
	ppf.add("physicPoint", [](Entity entity) {
		auto physicPoint = PhysicPoint<n>{};
		
		if constexpr (n == 2) {
			if (auto const tiledData = Component<tiled::Data>{entity}) {
				physicPoint.setPosition(tiledData->objectLayer->offset + tiledData->object->position);
			}
		}
		
		return physicPoint;
	});
	
	ppf.add("physicBody", [](Entity, Property data) {
		PhysicBody<n> physicBody;
		
		// physicBody.setShapeInfo(sif.create(std::string_view{data["shape"]["type"]}, data["shape"]["data"]));
		
		return physicBody;
	});
}

template<dim_t n>
void PhysicModule::setupRuleCreator(RuleCreator<n>& rf) const {
	rf.add("gravity", [](Property data){
		return std::make_unique<Gravity<n>>(Vector<n, double>{data["value"]});
	});
	
	rf.add("resistance", [](Property data){
		return std::make_unique<Resistance<n>>(double{data["value"]});
	});
	
	rf.add("spring", [](ProviderFactory<sbg::Vector<n, double>> providers, Property data){
		auto spring = std::make_unique<Spring<n>>(double{data["strength"]}, double{data["length"]});
		auto position = data["position"];
		
		if (position.isObject()) {
			spring->setPositionProvider(providers.create(position["type"], sbg::Entity{}, position["data"]));
		} else if (position.isArray()) {
			spring->setPositionProvider(sbg::Vector<n, double>{position});
		}
		
		return spring;
	});
	
	rf.add("rubber", [](ProviderFactory<sbg::Vector<n, double>> providers, Property data){
		auto rubber =  std::make_unique<Rubber<n>>(double{data["strength"]}, double{data["length"]});
		auto position = data["position"];
		
		if (position.isObject()) {
			rubber->setPositionProvider(providers.create(position["type"], sbg::Entity{}, position["data"]));
		} else if (position.isArray()) {
			rubber->setPositionProvider(sbg::Vector<n, double>{position});
		}
		
		return rubber;
	});
}

template void PhysicModule::setupPhysicPointCreator<2>(PhysicPointCreator<2>&) const;
template void PhysicModule::setupPhysicPointCreator<3>(PhysicPointCreator<3>&) const;
template void PhysicModule::setupRuleCreator<2>(RuleCreator<2>&) const;
template void PhysicModule::setupRuleCreator<3>(RuleCreator<3>&) const;

} // namespace sbg
