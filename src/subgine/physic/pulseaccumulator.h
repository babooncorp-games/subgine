#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

template<dim_t n>
struct PulseAccumulator {
	PulseAccumulator();
	
	void operator+=(Vector<n, double> value);
	Vector<n, double> value() const;
	
	void clear();
	bool empty() const;
	int count() const;
	
private:
	Vector<n, double> _total;
	int _count;
};

template <> void PulseAccumulator<2>::operator+=(Vector2d value);
template <> void PulseAccumulator<3>::operator+=(Vector3d value);

extern template struct PulseAccumulator<2>;
extern template struct PulseAccumulator<3>;

typedef PulseAccumulator<2> PulseAccumulator2D;
typedef PulseAccumulator<3> PulseAccumulator3D;

} // namespace sbg
