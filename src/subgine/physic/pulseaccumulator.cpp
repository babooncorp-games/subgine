#include "pulseaccumulator.h"

namespace sbg {

template<dim_t n>
PulseAccumulator<n>::PulseAccumulator() : _total(), _count(0) {}

template<>
void PulseAccumulator<2>::operator+=(Vector2d value) {
	_total.x = std::abs(_total.x) > std::abs(value.x) ? _total.x : value.x;
	_total.y = std::abs(_total.y) > std::abs(value.y) ? _total.y : value.y;
	_count++;
}

template<>
void PulseAccumulator<3>::operator+=(Vector3d value) {
	_total.x = std::abs(_total.x) > std::abs(value.x) ? _total.x : value.x;
	_total.y = std::abs(_total.y) > std::abs(value.y) ? _total.y : value.y;
	_total.z = std::abs(_total.z) > std::abs(value.z) ? _total.z : value.z;
	_count++;
}

template<dim_t n>
Vector<n, double> PulseAccumulator<n>::value() const {
	if (_count > 0) {
		return _total;
	} else {
		return Vector<n, double>();
	}
}

template<dim_t n>
void PulseAccumulator<n>::clear() {
	_total = Vector<n, double>();
	_count = 0;
}

template<dim_t n>
bool PulseAccumulator<n>::empty() const {
	return _count == 0;
}

template<dim_t n>
int PulseAccumulator<n>::count() const {
	return _count;
}

template struct PulseAccumulator<2>;
template struct PulseAccumulator<3>;

} // namespace sbg
