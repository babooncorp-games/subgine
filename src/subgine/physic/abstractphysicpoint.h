#pragma once

namespace sbg {

struct AbstractPhysicPoint {
	void setMass(const double mass);
	double getMass() const;

protected:
	double _mass = 1;
};

} // namespace sbg
