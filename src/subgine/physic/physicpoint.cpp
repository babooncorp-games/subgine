#include "physicpoint.h"

#include "subgine/common.h"

namespace sbg {

template<dim_t n>
PhysicPoint<n>::PhysicPoint() = default;

template<dim_t n>
PhysicPoint<n>::~PhysicPoint() = default;

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getVelocity() const {
	return _velocity;
}

template<dim_t n>
void PhysicPoint<n>::setVelocity(const Vector<n, double> velocity) {
	_velocity = velocity;
}

template<dim_t n>
void PhysicPoint<n>::setPosition(const Vector<n, double> posision) {
	this->_position = posision;
}

template<dim_t n>
Rule<n>* PhysicPoint<n>::getRule(const std::string tag) {
	auto it = _rules.find(tag);
	
	if (it != _rules.end()) {
		return it->second.get();
	}
	
	return nullptr;
}

template<dim_t n>
const Rule<n>* PhysicPoint<n>::getRule(const std::string tag) const {
	auto it = _rules.find(tag);
	
	if (it != _rules.end()) {
		return it->second.get();
	}
	
	return nullptr;
}

template<dim_t n>
void PhysicPoint<n>::removeRule(std::string tag) {
	_rules.erase(tag);
	_forces.erase(tag);
}

template<dim_t n>
bool PhysicPoint<n>::hasRule(std::string tag) const {
	return _rules.find(tag) != _rules.end();
}

template<dim_t n>
void PhysicPoint<n>::setForce(const std::string type, const Vector<n, double> force) {
	_forces[type] = force;
}

template<dim_t n>
void PhysicPoint<n>::update(Time time) {
	this->_position = getNextPosition(time.current);
	_velocity = getNextVelocity(time.current);
	_corrections = {};
	_correctionAmount = {};
	_pulses.clear();
	_pulseAccumulators.clear();
	_forces = getNextForces();
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getNextVelocity(double time) const {
	Vector<n, double> velocity = _velocity;
	
	for (auto i : getNextForces()) {
		velocity += (i.second / _mass) * time;
	}
	
	for (auto i : getNextPulses()) {
		velocity += (i.second / _mass);
	}
	
	return velocity;
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getNextPosition(double time) const {
	Vector<n, double> position = this->_position + (_velocity + getNextVelocity(time)) * time / 2;
	
	if (_correctionAmount > 0) {
		position += _corrections / _correctionAmount;
	}
	
	return position;
}

template<dim_t n>
std::map<std::string, Vector<n, double>> PhysicPoint<n>::getNextForces() const {
	auto forces = _forces;
	
	for (auto& i : _rules) {
		forces[i.first] = i.second->getResult(*this);
	}
	
	return forces;
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getPulse(const std::string type) const {
	auto it = _pulses.find(type);
	
	if (it == _pulses.end()) {
		return Vector<n, double>();
	}
	
	return it->second;
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getNextPulse(const std::string type) const {
	auto pulses = getNextPulses();
	auto it = pulses.find(type);

	if (it == pulses.end()) {
		return Vector<n, double>();
	}
	
	return it->second;
}

template<dim_t n>
void PhysicPoint<n>::setPulse(const std::string type, const Vector<n, double> pulse) {
	_pulses[type] = pulse;
}

template<dim_t n>
const std::map<std::string, Vector<n, double>>& PhysicPoint<n>::getForce() const {
	return _forces;
}

template<dim_t n>
std::map<std::string, Vector<n, double>>& PhysicPoint<n>::getForce() {
	return _forces;
}

template<dim_t n>
const std::map<std::string, Vector<n, double>>& PhysicPoint<n>::getPulse() const {
	return _pulses;
}

template<dim_t n>
std::map<std::string, Vector<n, double>>& PhysicPoint<n>::getPulse() {
	return _pulses;
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getForce(const std::string type) const {
	auto force = _forces.find(type);

	if (force == _forces.end()) {
		return Vector<n, double>();
	}
	
	return force->second;
}

template<dim_t n>
std::map<std::string, std::unique_ptr<Rule<n>>>& PhysicPoint<n>::getRule() {
	return _rules;
}

template<dim_t n>
const std::map<std::string, std::unique_ptr<Rule<n>>>& PhysicPoint<n>::getRule() const {
	return _rules;
}

template<dim_t n>
void PhysicPoint<n>::setRule(const std::string tag, std::unique_ptr<Rule<n>> rule) {
	_rules[tag] = std::move(rule);
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::momentum() const {
	return _velocity * _mass;
}

template<dim_t n>
Vector<n, double> PhysicPoint<n>::getPosition() const {
	return this->_position;
}

template<dim_t n>
void PhysicPoint<n>::correctPosition(const std::string profile, const Vector<n, double> amount) {
	_corrections += amount;
	++_correctionAmount;
}

template<dim_t n>
void PhysicPoint<n>::accumulatePulse(const std::string type, Vector<n, double> pulse) {
	_pulseAccumulators[type] += pulse;
	
}

template<dim_t n>
std::map<std::string, Vector<n, double>> PhysicPoint<n>::getNextPulses() const {
	auto pulses = _pulses;
	
	for (auto& accumulator : _pulseAccumulators) {
		pulses[accumulator.first] += accumulator.second.value();
	}
	
	return pulses;
}

template<dim_t n>
auto PhysicPoint<n>::getMass() const -> double {
	return _mass;
}

template<dim_t n>
auto PhysicPoint<n>::setMass(double mass) -> void {
	_mass = mass;
}

template struct PhysicPoint<2>;
template struct PhysicPoint<3>;

} // namespace sbg
