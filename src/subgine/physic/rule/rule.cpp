#include "rule.h"

namespace sbg {

template<int n>
Rule<n>::~Rule() {}

template struct Rule<2>;
template struct Rule<3>;

} // namespace sbg
