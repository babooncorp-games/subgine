#pragma once

#include <functional>

#include "rule.h"
#include "../physicpoint.h"

#include "subgine/provider/provider.h"

namespace sbg {

template<int n>
struct Spring : Rule<n> {
	Spring();
	Spring(const double strength, const double length);
	Spring(const double strength, const double length, Provider<Vector<n, double>> provider);
	Spring(const Spring& other) = default;
	Spring(Spring&& other) noexcept = default;
	
	Vector<n, double> getPosition() const;
	void setPositionProvider(const Provider<Vector<n, double>>& provider);
	void setPositionProvider(Provider<Vector<n, double>>&& provider);
	
	double getLength() const;
	void setLength(const double length);
	
	double getStrength() const;
	void setStrength(const double strength);
	
	Vector<n, double> getResult(const PhysicPoint<n>& object) const override;

private:
	double _strength;
	double _length;
	Provider<Vector<n, double>> _provider;
};

extern template struct Spring<2>;
extern template struct Spring<3>;

using Spring2D = Spring<2>;
using Spring3D = Spring<3>;

} // namespace sbg
