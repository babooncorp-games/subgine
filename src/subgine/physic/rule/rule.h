#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

template<int>
struct PhysicPoint;

template<int n>
struct Rule {
	virtual ~Rule();
	virtual Vector<n, double> getResult(const PhysicPoint<n>& object) const = 0;
};

using Rule2D = Rule<2>;
using Rule3D = Rule<3>;

extern template struct Rule<2>;
extern template struct Rule<3>;

} // namespace sbg
