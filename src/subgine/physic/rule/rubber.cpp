#include "rubber.h"

#include "../physicpoint.h"

namespace sbg {

template<int n>
Rubber<n>::Rubber() : _strength{0}, _length{0} {
	
}

template<int n>
Rubber<n>::Rubber(const double strength, const double length, Provider<Vector<n, double>> provider) : _strength{strength}, _length{length}, _provider{provider} {
	
}

template<int n>
Rubber<n>::Rubber(const double strength, const double length) : _strength{strength}, _length{length} {
	
}

template<int n>
Vector<n, double> Rubber<n>::getPosition() const {
	return _provider();
}

template<int n>
void Rubber<n>::setPositionProvider(const Provider<Vector<n, double>>& provider) {
	_provider = provider;
}

template<int n>
void Rubber<n>::setPositionProvider(Provider<Vector<n, double>>&& provider) {
	std::swap(_provider, provider);
}

template<int n>
double Rubber<n>::getLength() const {
	return _length;
}

template<int n>
void Rubber<n>::setLength(const double size) {
	_length = size;
}

template<int n>
Vector<n, double> Rubber<n>::getResult(const PhysicPoint<n>& object) const {
	auto relative = getPosition() - object.getPosition();
	
	if (relative.length() > getLength()) {
		return relative.unit() * getStrength() * (relative.length() - getLength());
	}
	
	return Vector<n, double>{0};
}

template<int n>
double Rubber<n>::getStrength() const {
	return _strength;
}

template<int n>
void Rubber<n>::setStrength(const double strength) {
	_strength = strength;
}

template struct Rubber<2>;
template struct Rubber<3>;

} // namespace sbg
