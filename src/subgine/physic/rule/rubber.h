#pragma once

#include "subgine/physic/rule/rule.h"
#include "subgine/provider/provider.h"

namespace sbg {

template<int n>
struct Rubber : Rule<n> {
	Rubber();
	Rubber(const double strength, const double length);
	Rubber(const double strength, const double length, Provider<Vector<n, double>> provider);
	Rubber(const Rubber& other) = default;
	Rubber(Rubber&& other) = default;
	
	Vector<n, double> getPosition() const;
	void setPositionProvider(const Provider<Vector<n, double>>& provider);
	void setPositionProvider(Provider<Vector<n, double>>&& provider);
	
	double getLength() const;
	void setLength(const double length);
	
	double getStrength() const;
	void setStrength(const double strength);
	
	Vector<n, double> getResult(const PhysicPoint<n>& object) const override;

private:
	double _strength;
	double _length;
	Provider<Vector<n, double>> _provider;
};

extern template struct Rubber<2>;
extern template struct Rubber<3>;

using Rubber2D = Rubber<2>;
using Rubber3D = Rubber<3>;

} // namespace sbg
