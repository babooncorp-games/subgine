#include "spring.h"

namespace sbg {

template<int n>
Spring<n>::Spring() : _strength{0}, _length{0} {}

template<int n>
Spring<n>::Spring(const double strength, const double length, Provider<Vector<n, double>> provider) : _strength{strength}, _length{length}, _provider{provider} {}

template<int n>
Spring<n>::Spring(const double strength, const double length) : _strength{strength}, _length{length} {}

template<int n>
Vector<n, double> Spring<n>::getPosition() const {
	return _provider();
}

template<int n>
void Spring<n>::setPositionProvider(const Provider<Vector<n, double>>& provider) {
	_provider = provider;
}

template<int n>
void Spring<n>::setPositionProvider(Provider<Vector<n, double>>&& provider) {
	std::swap(_provider, provider);
}

template<int n>
double Spring<n>::getLength() const {
	return _length;
}

template<int n>
void Spring<n>::setLength(const double size) {
	_length = size;
}

template<int n>
Vector<n, double> Spring<n>::getResult(const PhysicPoint<n>& object) const {
	auto relative = getPosition() - object.getPosition();
	return relative.unit() * getStrength() * (relative.length() - getLength());
}

template<int n>
double Spring<n>::getStrength() const {
	return _strength;
}

template<int n>
void Spring<n>::setStrength(const double strength) {
	_strength = strength;
}

template struct Spring<2>;
template struct Spring<3>;

} // namespace sbg
