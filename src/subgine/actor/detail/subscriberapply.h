#pragma once

#include "../component/actor.h"
#include "subgine/common/traits.h"
#include "subgine/common/dropping_invoke.h"

namespace sbg {

struct SubscriberApply {
	template<typename I, typename B, typename... Args>
	auto operator()(I invoker, B& builder, Actor& actor, Args&&... args) const
		-> std::void_t<decltype(actor.subscribe(dropping_invoke(invoker, builder, std::declval<Args>()...)))>
	{
		actor.subscribe(dropping_invoke(invoker, builder, std::forward<Args>(args)...));
	}
};

} // namespace sbg
