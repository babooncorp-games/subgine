#pragma once

namespace sbg {

struct ActorModuleService;

struct ComponentCreator;

struct ActorModule {
	void setupComponentCreator(ComponentCreator& ef) const;
	
	friend auto service_map(ActorModule const&) -> ActorModuleService;
};

} // namespace sbg
