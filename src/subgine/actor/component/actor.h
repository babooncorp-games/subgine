#pragma once

#include "../eventdispatcher.h"
#include "subgine/common/types.h"

#include <string>
#include <map>
#include <algorithm>

namespace sbg {

/**
 * Actor is a component that is an event dipatcher but also an handler.
 * 
 * This class has a private inheritance because calling direclty function of the parent may cause errors.
 */
struct Actor : EventDispatcher {
	template<typename A>
	void act(A action) {
		dispatch(std::move(action));
	}
};

} // namespace sbg
