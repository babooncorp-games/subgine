#pragma once

#include "../detail/subscriberapply.h"

#include "subgine/entity/entity.h"
#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

struct ActorCreatorService;

/**
 * TODO: This factory should be named ActionHandlerCreator, since this is what its builders construct
 * TODO: The create function should simply build and return one action handler
 * 
 * This class is a creator for actors.
 * 
 * Builder signature:
 * \code
 * <action handler>(Entity, Property)
 * \endcode
 * 
 * Action handlers are lambdas that receive a particular action or a class that have a single handle function.
 */
struct ActorCreator : Creator<AppliedBuilder<Actor>, SubscriberApply> {
	auto create(kgr::invoker invoker, Actor& actor, Entity entity, Property data) const -> void;
	
	friend auto service_map(ActorCreator const&) -> ActorCreatorService;
};

using ActorFactory = Factory<ActorCreator>;

} // namespace sbg
