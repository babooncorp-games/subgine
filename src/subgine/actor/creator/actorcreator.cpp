#include "actorcreator.h"

#include "../component/actor.h"
#include "subgine/log.h"

namespace sbg {

auto ActorCreator::create(kgr::invoker invoker, Actor& actor, Entity entity, Property data) const -> void {
	if (!data.empty()) {
		for (auto const jsonHandler : data["handlers"]) {
			auto const type = std::string_view{jsonHandler["type"]};
			auto const l = sbg::Log::trace(SBG_LOG_INFO, "Creating an action handler of type", log::enquote(type));
			build(invoker, type, actor, entity, jsonHandler["data"]);
		}
	} else {
		sbg::Log::warning(SBG_LOG_INFO, "No data for actor creation");
	}
}

} // namespace sbg
