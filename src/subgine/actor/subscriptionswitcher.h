#pragma once

#include "subgine/common/types.h"
#include "subgine/entity/entity.h"

namespace sbg {

struct SubscriptionSwitcherService;
struct EventDispatcher;

/**
 * This class is used to manage a unique subcription shared between multiple entities.
 */
struct SubscriptionSwitcher {
	SubscriptionSwitcher(EventDispatcher& eventDispatcher);
	
	/**
	 * This function unsubscibe the previous subscriber and subscribe the new one.
	 */
	void switchTo(Entity subscriber);
	
private:
	Entity _subscriber;
	EventDispatcher& _eventDispatcher;
	
	friend auto service_map(SubscriptionSwitcher const&) -> SubscriptionSwitcherService;
};

} // namespace sbg
