#include "subscriptionswitcher.h"

#include "component/actor.h"
#include "subgine/event/eventdispatcher.h"

namespace sbg {

SubscriptionSwitcher::SubscriptionSwitcher(EventDispatcher& eventDispatcher) : _eventDispatcher{eventDispatcher} {}

void SubscriptionSwitcher::switchTo(Entity subscriber) {
	if (subscriber.has<Actor>()) {
		if (_subscriber) {
			_eventDispatcher.unsubscribe(_subscriber.component<Actor>());
		}
		
		_eventDispatcher.subscribe(subscriber.component<Actor>());
		_subscriber = subscriber;
	}
}

} // namespace sbg
