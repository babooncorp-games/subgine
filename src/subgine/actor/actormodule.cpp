#include "actormodule.h"

#include "subgine/actor.h"
#include "subgine/entity.h"

using namespace sbg;

void ActorModule::setupComponentCreator(ComponentCreator& ef) const {
	ef.add("actor", [](ActorFactory af, Entity entity, Property data){
		auto actor = Actor{};
		af.create(actor, entity, data);
		return actor;
	});
}
