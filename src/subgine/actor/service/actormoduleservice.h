#pragma once

#include "../actormodule.h"

#include "subgine/entity/service/componentcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ActorModuleService : kgr::single_service<ActorModule>,
	autocall<
		&ActorModule::setupComponentCreator
	> {};

} // namespace sbg

