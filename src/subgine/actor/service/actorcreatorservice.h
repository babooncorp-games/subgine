#pragma once

#include "../creator/actorcreator.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ActorCreatorService : kgr::single_service<ActorCreator> {};
using ActorFactoryService = FactoryService<ActorFactory>;

} // namespace sbg
