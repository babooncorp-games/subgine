#pragma once

#include "../subscriptionswitcher.h"

#include "subgine/event/service/eventdispatcherservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SubscriptionSwitcherService : kgr::service<SubscriptionSwitcher, kgr::autowire>{};

} // namespace sbg
