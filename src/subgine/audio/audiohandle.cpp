#include "audiohandle.h"

#include "audiosystem.h"

#include <SFML/Audio.hpp>

using namespace sbg;

template<typename T>
auto AudioHandle::visit(T visitor) const -> decltype(auto) {
	return std::visit(
		[&](auto const& audio) -> decltype(auto) {
			return visitor(*audio.handle);
		},
		*_audio->_audio_objects[_id].audio
	);
}

void AudioHandle::pitch(float pitch) const {
	visit([&](auto& audio) {
		audio.setPitch(pitch);
	});
}

void AudioHandle::volume(float volume) const {
	visit([&](auto& audio) {
		audio.setVolume(volume * 100);
	});
}

void AudioHandle::loop(bool looping) const {
	visit([&](auto& audio) {
		audio.setLoop(looping);
	});
}

void AudioHandle::pause() const {
	visit([&](auto& audio) {
		audio.pause();
	});
}

void AudioHandle::play() const {
	visit([&](auto& audio) {
		audio.play();
	});
}

void AudioHandle::stop() const {
	visit([&](auto& audio) {
		audio.stop();
	});
}

auto AudioHandle::valid() const noexcept -> bool {
	return _audio && _audio->_audio_objects[_id].audio && _audio->_audio_objects[_id].generation == _generation;
}

auto AudioHandle::status() const -> Status {
	return visit([&](auto const& audio) {
		switch (audio.getStatus()) {
			case sf::SoundSource::Status::Playing: return Status::Playing;
			case sf::SoundSource::Status::Paused: return Status::Paused;
			default: return Status::Stopped;
		}
	});
}
