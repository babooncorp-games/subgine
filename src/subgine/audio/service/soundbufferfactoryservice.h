 #pragma once

#include "../factory/soundbufferfactory.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"
#include "subgine/resource/service/resourcefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SoundBufferLoaderService : kgr::service<SoundBufferLoader>{};

using SoundBufferFactoryService = ResourceFactoryService<SoundBufferFactory>;
using SoundBufferManagerService = ResourceManagerService<SoundBufferManager>;

} // namespace sbg
