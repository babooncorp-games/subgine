 #pragma once
 
#include "../audiomodule.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct AudioModuleService : kgr::single_service<AudioModule> {};

} // namespace sbg
