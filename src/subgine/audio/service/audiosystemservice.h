#pragma once

#include "../audiosystem.h"

#include "soundbufferfactoryservice.h"
#include "subgine/resource/service/gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct AudioSystemService : kgr::single_service<AudioSystem, kgr::autowire> {};

} // namespace sbg
