#pragma once

namespace sbg {

struct AudioModuleService;

struct AudioModule {
	friend auto service_map(AudioModule const&) -> AudioModuleService;
};

} // namespace sbg
