#pragma once

#include <utility>
#include <cstdint>

namespace sbg {

struct AudioSystem;

struct AudioHandle {
	AudioHandle() = default;
	
	explicit AudioHandle(AudioSystem& audio, std::size_t id, std::size_t generation) noexcept :
		_audio{&audio}, _id{id}, _generation{generation} {}
	
	enum struct Status : std::uint8_t { Stopped, Paused, Playing };
	
	void volume(float volume) const;
	void pitch(float pitch) const;
	void play() const;
	void pause() const;
	void stop() const;
	void loop(bool looping) const;
	auto valid() const noexcept -> bool;
	auto status() const -> Status;
	
private:
	template<typename T>
	auto visit(T visitor) const -> decltype(auto);
	
	friend AudioSystem;
	
	AudioSystem* _audio = nullptr;
	std::size_t _id = 0;
	std::size_t _generation = 0;
};

} // namespace sbg
