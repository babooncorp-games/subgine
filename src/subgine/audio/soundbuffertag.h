#pragma once

#include "subgine/resource/resourcetag.h"

namespace sbg {
	using SoundBufferTag = ResourceTag<struct SoundBufferTagType>;
}
