#include "soundbufferfactory.h"

#include "subgine/resource.h"

#include <SFML/Audio.hpp>

namespace sbg {

auto SoundBufferLoader::create(Property data) const -> SoundBuffer {
	auto buffer = std::unique_ptr<sf::SoundBuffer, SoundBufferDeleter>(new sf::SoundBuffer);
	
	buffer->loadFromFile((asset_directory() / std::filesystem::path{data["path"]}).string());
	
	return SoundBuffer{std::move(buffer)};
}

void SoundBufferDeleter::operator()(sf::SoundBuffer* ptr) noexcept {
	delete ptr;
}

void SoundBuffer::free() noexcept {
	delete _buffer;
}

} // namespace sbg
