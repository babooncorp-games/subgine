#pragma once

#include "subgine/resource/property.h"
#include "subgine/resource/resourcemanager.h"
#include "subgine/resource/resourcefactory.h"

namespace sf {
	class SoundBuffer;
}

namespace sbg {

struct SoundBufferLoaderService;

struct SoundBufferDeleter {
	void operator()(sf::SoundBuffer* ptr) noexcept;
};

// This class must be used with OwnedResource, otherwise this structure leak
struct SoundBuffer {
	SoundBuffer() = default;
	
	explicit SoundBuffer(std::unique_ptr<sf::SoundBuffer, SoundBufferDeleter> buffer) noexcept : _buffer{buffer.release()} {}
	
	void free() noexcept;
	
	auto buffer() const& -> sf::SoundBuffer const& {
		return *_buffer;
	}

private:
	sf::SoundBuffer* _buffer = nullptr;
};

struct SoundBufferLoader {
	auto create(Property data) const -> SoundBuffer;
	
	static constexpr auto path = "sound";
	
	friend auto service_map(SoundBufferLoader const&) -> SoundBufferLoaderService;
};

using SoundBufferFactory = ResourceFactory<SoundBufferLoader>;
using SoundBufferManager = ResourceManager<SoundBufferFactory>;

} // namespace sbg
