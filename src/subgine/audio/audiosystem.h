#pragma once

#include "factory/soundbufferfactory.h"
#include "audiohandle.h"

#include <vector>
#include <variant>
#include <optional>

namespace sf {
	class SoundBuffer;
	class Sound;
	class Music;
}

namespace sbg {

struct AudioSystemService;

struct AudioHandle;
struct GameDatabase;

struct AudioSystem {
	explicit AudioSystem(SoundBufferManager buffers, GameDatabase& database) noexcept : _buffers{buffers}, _database{&database} {}
	
	auto create_sound(std::string_view sound) -> AudioHandle;
	auto create_music(std::string_view sound) -> AudioHandle;
	
	auto transfer(AudioHandle handle, AudioSystem& other) -> AudioHandle;
	
	void pause_all() noexcept;
	void unpause_all() noexcept;
	
private:
	friend AudioHandle;
	
	template<typename H, typename T>
	static auto visit(H&& handle, T visitor) noexcept -> decltype(auto);
	
	struct Sound {
		explicit Sound(std::unique_ptr<sf::Sound> h) noexcept;
		Sound(Sound&&) noexcept;
		Sound& operator=(Sound&&) noexcept;
		~Sound();
		
		std::unique_ptr<sf::Sound> handle;
	};
	
	struct Music {
		explicit Music(std::unique_ptr<sf::Music> h) noexcept;
		Music(Music&&) noexcept;
		Music& operator=(Music&&) noexcept;
		~Music();
		
		std::unique_ptr<sf::Music> handle;
	};
	
	struct Audio {
		std::optional<std::variant<Sound, Music>> audio;
		std::size_t generation = 0;
		bool paused_by_system = false;
	};
	
	auto create_audio(std::variant<Sound, Music> audio) -> std::pair<std::size_t, std::size_t>;
	
	GameDatabase* _database;
	SoundBufferManager _buffers;
	std::vector<Audio> _audio_objects;
	
	friend auto service_map(AudioSystem const&) -> AudioSystemService;
};

} // namespace sbg
