#include "audiosystem.h"

#include "audiohandle.h"

#include "subgine/resource.h"

#include <SFML/Audio.hpp>
#include <algorithm>

using namespace sbg;

AudioSystem::Music::Music(std::unique_ptr<sf::Music> h) noexcept : handle{std::move(h)} {}
AudioSystem::Sound::Sound(std::unique_ptr<sf::Sound> h) noexcept : handle{std::move(h)} {}

auto AudioSystem::Music::operator=(AudioSystem::Music&& other) noexcept -> AudioSystem::Music& {
	std::swap(other.handle, handle);
	return *this;
}

auto AudioSystem::Sound::operator=(AudioSystem::Sound&& other) noexcept -> AudioSystem::Sound& {
	std::swap(other.handle, handle);
	return *this;
}

AudioSystem::Music::Music(AudioSystem::Music&& other) noexcept : handle{std::move(other.handle)} {}
AudioSystem::Sound::Sound(AudioSystem::Sound&& other) noexcept : handle{std::move(other.handle)} {}

AudioSystem::Music::~Music() = default;
AudioSystem::Sound::~Sound() = default;

template<typename H, typename T>
auto AudioSystem::visit(H&& handle, T visitor) noexcept -> decltype(auto) {
	return std::visit(
		[&](auto const& audio) -> decltype(auto) {
			return visitor(*audio.handle);
		},
		*std::forward<H>(handle).audio
	);
}

auto AudioSystem::create_audio(std::variant<Sound, Music> audio) -> std::pair<std::size_t, std::size_t> {
	auto stopped = [](auto const& audio) {
		return audio.getStatus() == sf::SoundSource::Status::Stopped;
	};
	
	auto found = std::find_if(_audio_objects.begin(), _audio_objects.end(), [&](auto const& audio) {
		return visit(audio, stopped);
	});
	
	if (found != _audio_objects.end()) {
		found->audio = std::move(audio);
		return {static_cast<std::size_t>(std::distance(_audio_objects.begin(), found)), ++found->generation};
	} else {
		auto id = _audio_objects.size();
		_audio_objects.emplace_back(std::move(audio));
		return {id, 0};
	}
}

auto AudioSystem::create_music(std::string_view sound) -> AudioHandle {
	auto data = _database->get("sound", sound);
	auto music = std::make_unique<sf::Music>();
	music->openFromFile((asset_directory() / std::filesystem::path{data["path"]}).string());
	
	auto [id, generation] = create_audio(Music{std::move(music)});
	return AudioHandle{*this, id, generation};
}

auto AudioSystem::create_sound(std::string_view sound) -> AudioHandle {
	auto audio = std::make_unique<sf::Sound>(_buffers.get(sound)->buffer());
	
	auto [id, generation] = create_audio(Sound{std::move(audio)});
	return AudioHandle{*this, id, generation};
}

auto AudioSystem::transfer(AudioHandle handle, AudioSystem& other) -> AudioHandle {
	auto [id, generation] = create_audio(*std::move(other._audio_objects[handle._id].audio));
	other._audio_objects[handle._id].audio = {};
	return AudioHandle{*this, id, generation};
}

void AudioSystem::pause_all() noexcept {
	for (auto& sound : _audio_objects) {
		auto pause = [](auto& audio) {
			audio.pause();
		};
		
		auto playing = [](auto const& audio) {
			return audio.getStatus() == sf::SoundSource::Status::Playing;
		};
		
		if (sound.audio && visit(sound, playing)) {
			visit(sound, pause);
			sound.paused_by_system = true;
		}
	}
}

void AudioSystem::unpause_all() noexcept {
	for (auto& sound : _audio_objects) {
		auto play = [](auto& audio) {
			audio.pause();
		};
		
		auto paused = [](auto const& audio) {
			return audio.getStatus() == sf::SoundSource::Status::Paused;
		};
		
		if (sound.audio && sound.paused_by_system && visit(sound, paused)) {
			visit(sound, play);
		}
		
		sound.paused_by_system = false;
	}
}
