#pragma once

#include "../statemodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct StateModuleService : kgr::single_service<StateModule> {};

} // namespace sbg
