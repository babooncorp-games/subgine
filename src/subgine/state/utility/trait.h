#pragma once

#include "subgine/common/traits.h"

#include <optional>

namespace sbg::detail {

struct Transit;

template<typename T>
using from_states_t = typename T::FromStates;

template<typename T>
using from_any_state_t = decltype(std::declval<T>().transit());

template<typename T, typename S>
using from_state_t = decltype(std::declval<T>().transit(std::declval<const S&>()));

template<typename T>
inline constexpr auto has_from_states = is_detected_v<from_states_t, T>;

template<typename T>
inline constexpr auto has_from_any = is_detected_v<from_any_state_t, T>;

template<typename T>
inline constexpr auto is_transition = has_from_states<T> || has_from_any<T>;

template<typename, typename = void, typename = void>
inline constexpr auto is_conditional_transition = false;

template<typename T, typename S>
inline constexpr auto is_conditional_transition<T, S, std::enable_if_t<is_specialisation_of<std::optional, decltype(std::declval<T>().transit(std::declval<S&>()))>::value>> = true;

template<typename T>
inline constexpr auto is_conditional_transition<T, void, std::enable_if_t<is_specialisation_of<std::optional, decltype(std::declval<T>().transit())>::value>> = true;

template<typename, typename = void, typename = void>
inline constexpr auto is_void_transition = false;

template<typename T>
inline constexpr auto is_void_transition<T, void, std::enable_if_t<std::is_same<void, decltype(std::declval<T>().transit())>::value>> = true;

template<typename T, typename S>
inline constexpr auto is_void_transition<T, S, std::enable_if_t<std::is_same<void, decltype(std::declval<T>().transit(std::declval<S&>()))>::value>> = true;

template<typename, typename = void, typename = void>
inline constexpr auto is_void_polymorphic_transition = false;

template<typename T>
inline constexpr auto is_void_polymorphic_transition<T, void, std::enable_if_t<std::is_same<void, std::invoke_result_t<decltype(std::declval<T>().transit()), Transit>>::value>> = true;

template<typename T, typename S>
inline constexpr auto is_void_polymorphic_transition<T, S, std::enable_if_t<std::is_same<void, std::invoke_result_t<decltype(std::declval<T>().transit(std::declval<S&>())), Transit>>::value>> = true;

template<typename, typename = void, typename = void>
inline constexpr auto is_polymorph_transition = false;

template<typename T, typename S>
inline constexpr auto is_polymorph_transition<T, S, std::enable_if_t<std::is_invocable<decltype(std::declval<T>().transit(std::declval<S&>())), Transit>::value>> = true;

template<typename T>
inline constexpr auto is_polymorph_transition<T, void, std::enable_if_t<std::is_invocable<decltype(std::declval<T>().transit()), Transit>::value>> = true;

template<typename... F>
inline constexpr auto is_lambda_transition = ((has_call_operator_v<F> && !has_from_states<F>) && ...);

} // namespace sbg::detail
