#pragma once

#include <tuple>

namespace sbg {

template<typename... Ts>
struct From {
	using FromStates = std::tuple<Ts...>;
};

} // namespace sbg
