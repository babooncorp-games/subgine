#pragma once

#include "utility/trait.h"

namespace sbg {
	struct StateMachine;
}

namespace sbg::detail {

struct Transit {
	friend StateMachine;
	
	Transit(Transit&&) = default;
	Transit(const Transit&) = default;
	Transit& operator=(Transit&&) = default;
	Transit& operator=(const Transit&) = default;
	
	template<typename T, enable_if_i<is_transition<T>> = 0>
	bool operator()(T&& transition);
	
	template<typename... F, enable_if_i<is_lambda_transition<F...>> = 0>
	bool operator()(F&&... functions);
	
private:
	Transit(StateMachine& machine);
	
	StateMachine* _machine;
};

} // namespace sbg::detail
