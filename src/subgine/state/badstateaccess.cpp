#include "badstateaccess.h"

namespace sbg {

const char* BadStateAccess::what() const noexcept {
	return "The State Machine is not at the requested state";
}


} // namespace sbg
