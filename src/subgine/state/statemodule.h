#pragma once

namespace sbg {

struct StateModuleService;

struct StateModule {
	friend auto service_map(StateModule const&) -> StateModuleService;
};

} // namespace sbg
