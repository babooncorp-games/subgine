#pragma once

#include "subgine/common/function_traits.h"
#include "utility/trait.h"

#include <tuple>

namespace sbg::detail {

template<typename L, bool = function_traits<L>::arity == 0>
struct BaseTransition : L {
	using FromState = std::tuple<>;
	
	constexpr explicit BaseTransition(L transition) noexcept : L{std::move(transition)} {}
	
	constexpr auto transit() -> decltype(auto) {
		return L::operator()();
	}
};

template<typename L>
struct BaseTransition<L, false> : L {
	using FromState = std::tuple<std::decay_t<function_argument_t<0, L>>>;
	
	constexpr explicit BaseTransition(L transition) noexcept : L{std::move(transition)} {}
	
	constexpr auto transit(function_argument_t<0, L> state) -> decltype(auto) {
		return L::operator()(std::forward<function_argument_t<0, L>>(state));
	}
};

} // namespace sbg::detail

namespace sbg {

template<typename First, typename... Lambdas>
struct Transition : private detail::BaseTransition<First>, private Transition<Lambdas...> {
	using detail::BaseTransition<First>::transit;
	using Transition<Lambdas...>::transit;

	using FromStates = tuple_cat_t<typename detail::BaseTransition<First>::FromState, typename Transition<Lambdas...>::FromStates>;
	void operator()() const = delete;

	constexpr Transition(First first, Lambdas... lambdas) noexcept : detail::BaseTransition<First>{std::move(first)}, Transition<Lambdas...>{std::move(lambdas)...} {}
};

template<typename Lambda>
struct Transition<Lambda> : private detail::BaseTransition<Lambda> {
	using detail::BaseTransition<Lambda>::transit;

	using FromStates = typename detail::BaseTransition<Lambda>::FromState;
	void operator()() const = delete;
	
	constexpr Transition(Lambda lambda) noexcept : detail::BaseTransition<Lambda>{std::move(lambda)} {}
};

} // namespace sbg
