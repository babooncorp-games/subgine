#pragma once

#include <exception>

namespace sbg {

struct BadStateAccess : std::exception {
	const char * what() const noexcept override;
};

} // namespace sbg
