#include "../statemachine.h"
#include "../transition.h"

#include "catch2/catch.hpp"

#include <optional>

using namespace sbg;

TEST_CASE("InitialState", "[statemachine]") {
	struct Initial {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("ChangeStateAction", "[statemachine]") {
	struct State1 {};
	struct Initial {};
	struct GotoState1Action : From<Initial> {
		auto transit(Initial) {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoState1Action{}));
	REQUIRE(machine.at<State1>());
}

TEST_CASE("VoidTransition", "[statemachine]") {
	struct State1 {};
	struct Initial {};
	
	static bool executed = false;
	
	struct GotoState1Action : From<Initial> {
		void transit(Initial) { executed = true; }
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(GotoState1Action{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
	REQUIRE(executed);
}

TEST_CASE("VoidFromAnyTransition", "[statemachine]") {
	struct State1 {};
	struct Initial {};
	
	static bool executed = false;
	
	struct GotoState1Action {
		void transit() { executed = true; }
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(GotoState1Action{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
	REQUIRE(executed);
}

TEST_CASE("ChangeStateFromAny", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct ToState1 {
		auto transit() {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(ToState1{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("ChangeStateFromAnyOptionalTrue", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct GotoState1Action {
		std::optional<State1> transit() {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoState1Action{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("ChangeStateFromAnyOptionalFalse", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct GotoState1Action {
		std::optional<State1> transit() {
			return std::nullopt;
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(GotoState1Action{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("ChangeStateActionOptionalTrue", "[statemachine]") {
	struct Initial{};
	struct State1 {};
	struct GotoState1Action  : From<Initial> {
		std::optional<State1> transit(Initial) {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoState1Action{}));
	REQUIRE(machine.at<State1>());
}

TEST_CASE("ChangeStateActionOptionalFalse", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct GotoState1Action : From<Initial> {
		std::optional<State1> transit(Initial) {
			return std::nullopt;
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(GotoState1Action{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("ChangeStateFromAnyVariant1", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct GotoStateAction {
		auto transit() -> std::variant<State1, State2> {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoStateAction{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("ChangeStateFromAnyVariant2", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct GotoStateAction {
		auto transit() -> std::variant<State1, State2> {
			return State2{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoStateAction{}));
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
}

TEST_CASE("ChangeStateVariant1", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct GotoStateAction : From<Initial> {
		auto transit(Initial) -> std::variant<State1, State2> {
			return State1{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoStateAction{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("ChangeStateVariant2", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct GotoStateAction : From<Initial> {
		auto transit(Initial) -> std::variant<State1, State2> {
			return State2{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GotoStateAction{}));
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
}

TEST_CASE("ChangeStateActionBack", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	struct GoAndBackAction : From<Initial, State1> {
		auto transit(Initial) {
			return State1{};
		}
		
		auto transit(State1) {
			return Initial{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(GoAndBackAction{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(machine.transit(GoAndBackAction{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("ChangeStateInvalid", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct None {};
	
	struct GotoState1Action : From<None> {
		auto transit(None) {
			return None{};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(GotoState1Action{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("PolymorphicTransitionAlt1", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	static bool toState1 = true;
	
	(void) toState1;
	
	struct ToState1 : From<Initial> {
		auto transit(Initial) { return State1{}; }
	};
	
	struct ToState2 : From<Initial> {
		auto transit(Initial) { return State2{}; }
	};
	
	struct Transition : From<Initial> {
		auto transit(Initial) {
			return [](auto transit) {
				if (toState1) {
					return transit(ToState1{});
				} else {
					return transit(ToState2{});
				}
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(Transition{}));
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
}

TEST_CASE("PolymorphicTransitionAlt2", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	static bool toState1 = false;
	
	(void) toState1;
	
	struct ToState1 : From<Initial> {
		auto transit(Initial) { return State1{}; }
	};
	
	struct ToState2 : From<Initial> {
		auto transit(Initial) { return State2{}; }
	};
	
	struct Transition : From<Initial> {
		auto transit(Initial) {
			return [](auto transit) {
				if (toState1) {
					return transit(ToState1{});
				} else {
					return transit(ToState2{});
				}
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(Transition{}));
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<State1>());
}

TEST_CASE("PolymorphicTransitionFail", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	static bool toState1 = false;
	
	(void) toState1;
	
	struct ToState1 : From<Initial> {
		auto transit(Initial) { return State1{}; }
	};
	
	struct ToState2 : From<Initial> {
		std::optional<State2> transit(Initial) { return std::nullopt; }
	};
	
	struct Transition : From<Initial> {
		auto transit(Initial) {
			return [](auto transit) {
				if (toState1) {
					return transit(ToState1{});
				} else {
					return transit(ToState2{});
				}
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.transit(Transition{}));
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
}

TEST_CASE("PolymorphicTransitionStaySuccess", "[statemachine]") {
	struct Initial {};
	
	struct Transition : From<Initial> {
		auto transit(Initial) {
			return [](auto) {
				return true;
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	REQUIRE(machine.transit(Transition{}));
	REQUIRE(machine.at<Initial>());
}
