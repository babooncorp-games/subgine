#include "../statemachine.h"
#include "../transition.h"

#include "catch2/catch.hpp"

#include <optional>

using namespace sbg;

TEST_CASE("LambdaTransitionSingleSuccess", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { return State1{}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleFailure", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE_FALSE(machine.transit(
		[](Initial) { return std::optional<State1>{}; }
	));
	
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleEither1", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { return std::variant<State1, State2>{State1{}}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleEither2", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { return std::variant<State1, State2>{State2{}}; }
	));
	
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleVoid", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE_FALSE(machine.transit(
		[](Initial) {}
	));
	
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleFromAnyFail", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE_FALSE(machine.transit(
		[]{}
	));
	
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionSingleFromAnySuccess", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[]{ return State1{}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionManySuccess", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { return State1{}; },
		[](State2) { return Initial{}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
}

TEST_CASE("LambdaTransitionManyWhich", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{Initial{}};
	
	static bool executed = false;
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { executed = true; return State1{}; },
		[](State2) { return Initial{}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(executed);
}

TEST_CASE("LambdaTransitionManyVoid", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{Initial{}};
	
	static bool executed = false;
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(
		[](Initial) { executed = true; return State1{}; },
		[] { },
		[](State2) { return Initial{}; }
	));
	
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(executed);
}

TEST_CASE("LambdaTransitionManyVoid2", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	StateMachine machine{State1{}};
	
	REQUIRE(machine.at<State1>());
	
	REQUIRE(machine.transit(
		[](Initial) { return State1{}; },
		[] { return Initial{}; },
		[](State2) { return Initial{}; }
	));
	
	REQUIRE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
}

TEST_CASE("MakeTransitionFromLambda", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	
	auto transition = Transition{
		[](Initial) { return State1{}; },
		[](State1) { return State2{}; },
		[](State2) { return State1{}; }
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<State2>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
}

TEST_CASE("LambdaPolymorphicTransition", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct State3 {};
	
	static bool state2 = true;
	
	(void) state2;
	
	auto transition = Transition{
		[](Initial) { return State1{}; },
		[](State1) {
			return [](StateMachine::Transit transit) {
				if (state2) {
					return transit([](State1) { return State2{}; });
				} else {
					return transit([](State1) { return State3{}; });
				}
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<State3>());
	
	REQUIRE_FALSE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE(machine.at<State2>());
	REQUIRE_FALSE(machine.at<State3>());
}

TEST_CASE("LambdaPolymorphicTransitionAlt", "[statemachine]") {
	struct Initial {};
	struct State1 {};
	struct State2 {};
	struct State3 {};
	
	static bool state2 = false;
	
	(void) state2;
	
	auto transition = Transition{
		[](Initial) { return State1{}; },
		[](State1) {
			return [](StateMachine::Transit transit) {
				if (state2) {
					return transit([](State1) { return State2{}; });
				} else {
					return transit([](State1) { return State3{}; });
				}
			};
		}
	};
	
	StateMachine machine{Initial{}};
	
	REQUIRE(machine.at<Initial>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	
	REQUIRE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	REQUIRE(machine.at<State3>());
	
	REQUIRE_FALSE(machine.transit(transition));
	
	REQUIRE_FALSE(machine.at<Initial>());
	REQUIRE_FALSE(machine.at<State1>());
	REQUIRE_FALSE(machine.at<State2>());
	REQUIRE(machine.at<State3>());
}
