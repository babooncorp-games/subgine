#pragma once

#include "utility/trait.h"
#include "transit.h"
#include "badstateaccess.h"
#include "transition.h"
#include "utility/from.h"

#include "subgine/common/concepts.h"
#include "subgine/common/erasedtype.h"

#include <optional>
#include <tuple>
#include <variant>

namespace sbg {

template<typename T>
concept state_transition = requires { typename T::FromStates; } or requires(T t) { t.transit(); };

template<typename T>
concept state = object<T> and different_from<T, struct StateMachine>;

struct StateMachine {
	using Transit = detail::Transit;
	
	/**
	 * Construct the state machine with an initial state.
	 */
	StateMachine(state auto const& initial) : _state{initial} {}
	
	/**
	 * Construct the state machine with an initial state.
	 */
	StateMachine(state auto&& initial) : _state{std::move(initial)} {}
	
	/**
	 * Returns if T is the current state
	 */
	template<state T>
	bool at() const {
		return _state.is<T>();
	}
	
	/**
	 * @brief Executes the transition.
	 * 
	 * Iterate over all types in FromStates and calls possible transition path or try to call the from any transition if it exists.
	 */
	bool transit(state_transition auto transition) {
		using FromStates = detected_or<std::tuple<>, detail::from_states_t, decltype(transition)>;
		
		// Try a transition, return false if it can't handle the current state type.
		auto do_transit = [&](auto i) {
			using From = std::tuple_element_t<i, FromStates>;
			
			if (at<From>()) {
				return fromState<From>(transition);
			}
			
			return false;
		};
		
		// Try all transition, and try fromAny if available. else return false.
		return std::apply([&](auto... s) {
			return (do_transit(s) || ...) || this->fromState(transition);
		}, tuple_sequence<FromStates>);
	}
	
	/**
	 * Executes the transition from lambdas.
	 * This overload is called when lambdas are sent to the transit function.
	 * It will build a transition type and call transit.
	 */
	template<typename... F, enable_if_i<sizeof...(F) && detail::is_lambda_transition<std::decay_t<F>...>> = 0>
	bool transit(F&&... transitions) {
		return transit(Transition{std::forward<F>(transitions)...});
	}
	
	/**
	 * Returns the state T if T is the current state
	 * 
	 * @throw BadStateAccess if T is not the current state
	 */
	template<typename T>
	T& inspect() {
		if (not at<T>()) {
			throw BadStateAccess{};
		}
		
		return _state.as<T>();
	}
	
	/**
	 * Returns the state T if T is the current state
	 * 
	 * @throw BadStateAccess if T is not the current state
	 */
	template<typename T>
	const T& inspect() const {
		if (not at<T>()) {
			throw BadStateAccess{};
		}
		
		return _state.as<T>();
	}
	
private:
	/**
	 * Execute the transition and change the state.
	 * This overload is called when the transition is from a particular state.
	 */
	template<typename F, typename T>
	bool fromState(T& transition) {
		auto const& state = _state.as<F>();
		
		static_assert(!detail::is_void_polymorphic_transition<T, F>, "Polymorphic transitions must return a boolean");
		
		if constexpr (detail::is_conditional_transition<T, F>)
		{
			if (auto result = transition.transit(state)) {
				return changeState(std::move(*result));
			}
		}
		else if constexpr (detail::is_polymorph_transition<T, F>)
		{
			return transition.transit(state)(Transit{*this});
		}
		else if constexpr (detail::is_void_transition<T, F>)
		{
			return transition.transit(state), false;
		}
		else
		{
			return changeState(transition.transit(state));
		}
		
		return false;
	}
	
	/**
	 * Execute the transition and change the state.
	 * This overload is called when the transition is from any state.
	 */
	template<typename T>
	bool fromState(T& transition) {
		static_assert(!detail::is_void_polymorphic_transition<T>, "Polymorphic transitions must return a boolean");
		
		if constexpr (detail::is_conditional_transition<T>)
		{
			if (auto result = transition.transit()) {
				return changeState(std::move(*result));
			}
		}
		else if constexpr (detail::is_polymorph_transition<T>)
		{
			return transition.transit()(Transit{*this});
		}
		else if constexpr (detail::is_void_transition<T>)
		{
			return transition.transit(), false;
		}
		else if constexpr (detail::has_from_any<T>)
		{
			return changeState(transition.transit());
		}
		
		return false;
	}
	
	/**
	 * Changes the state.
	 */
	template<typename T>
	bool changeState(T state) {
		_state = ErasedType{std::move(state)};
		return true;
	}
	
	/**
	 * Does not change the state, empty variant.
	 */
	bool changeState(std::monostate) {
		return false;
	}
	
	/**
	 * Does not change the state, empty variant.
	 */
	template<typename... Ts>
	bool changeState(std::variant<Ts...> state) {
		return std::visit(
			[this](auto&& state) {
				return this->changeState(std::move(state));
			},
			std::move(state)
		);
	}
	
	ErasedType _state;
};

namespace detail {

template<typename T, enable_if_i<is_transition<T>>>
inline bool Transit::operator()(T&& transition) {
	return _machine->transit(std::forward<T>(transition));
}

template<typename... F, enable_if_i<is_lambda_transition<F...>>>
inline bool Transit::operator()(F&&... functions) {
	return _machine->transit(std::forward<F>(functions)...);
}

}

} // namespace sbg
