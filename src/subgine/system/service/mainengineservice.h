#pragma once

#include "../mainengine.h"

#include "deferredschedulerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct MainEngineService : kgr::single_service<MainEngine>, autocall<&MainEngine::setupContainer> {};

} // namespace sbg
