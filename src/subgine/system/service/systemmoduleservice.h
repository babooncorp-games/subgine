#pragma once

#include "../systemmodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SystemModuleService : kgr::single_service<SystemModule> {};

} // namespace sbg
