#pragma once

#include "../mainengine.h"
#include "../time.h"
#include "mainengineservice.h"

#include "subgine/provider/provider.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct MainEngineTimeProviderService : kgr::service<MainEngine*> {
	inline static auto construct(kgr::inject_t<MainEngineService> mainEngine) {
		return kgr::inject(&mainEngine.forward());
	}
	
	inline Provider<Time> forward() {
		return [&mainEngine = *instance()]{ return mainEngine.time(); };
	}
};

} // namespace sbg
