#pragma once

#include "../deferredscheduler.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct DeferredSchedulerService : kgr::extern_service<DeferredScheduler> {};

} // namespace sbg
