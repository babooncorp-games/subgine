#include "../deferredscheduler.h"
#include "catch2/catch.hpp"

using namespace sbg;
using namespace std::literals;

void deferred_update(DeferredScheduler& scheduler, Time const& time = {}) {
	scheduler.execute(time);
}

TEST_CASE("Scheduling", "[deferred]") {
	DeferredScheduler scheduler;
	
	static bool called;
	called = false;
	
	SECTION("Basic Scheduling") {
		scheduler.defer([]{
			called = true;
		});
		
		deferred_update(scheduler);
		
		REQUIRE(called);
	}
	
	SECTION("Timed Scheduling") {
		scheduler.defer(1s, []{
			called = true;
		});
		
		deferred_update(scheduler, Time{1});
		
		REQUIRE(called);
	}
	
	SECTION("Done Scheduling") {
		auto task = scheduler.defer([]{
			called = true;
		});
		
		CHECK(!task.done());
		
		deferred_update(scheduler);
		
		
		CHECK(task.done());
		REQUIRE(called);
	}
	
	SECTION("Multiple steps") {
		scheduler.defer(1s, []{
			called = true;
		});
		
		deferred_update(scheduler, Time{0.5});
		
		CHECK(!called);
		
		deferred_update(scheduler, Time{0.5});
		
		REQUIRE(called);
	}
	
	SECTION("Cancel steps") {
		auto task = scheduler.defer(1s, []{
			called = true;
		});
		
		deferred_update(scheduler, Time{0.5});
		
		CHECK(!called);
		task.cancel();
		CHECK(task.done());
		
		deferred_update(scheduler, Time{0.5});
		
		REQUIRE(!called);
	}
	
	SECTION("Multiple tasks") {
		static bool called2;
		called2 = false;
		
		auto task1 = scheduler.defer([]{
			called = true;
		});
		
		auto task2 = scheduler.defer([]{
			called2 = true;
		});
		
		CHECK(!called);
		CHECK(!called2);
		
		CHECK(!task1.done());
		CHECK(!task2.done());
		
		deferred_update(scheduler);
		
		CHECK(task1.done());
		REQUIRE(called);
		CHECK(task2.done());
		REQUIRE(called2);
	}
	
	SECTION("Multiple tasks cancel1") {
		static bool called2;
		called2 = false;
		
		auto task1 = scheduler.defer([]{
			called = true;
		});
		
		auto task2 = scheduler.defer([]{
			called2 = true;
		});
		
		CHECK(!called);
		CHECK(!called2);
		
		CHECK(!task1.done());
		CHECK(!task2.done());
		
		task1.cancel();
		
		CHECK(!task2.done());
		CHECK(task1.done());
		
		deferred_update(scheduler);
		
		CHECK(task1.done());
		REQUIRE(!called);
		CHECK(task2.done());
		REQUIRE(called2);
	}
	
	SECTION("Multiple tasks cancel2") {
		static bool called2;
		called2 = false;
		
		auto task1 = scheduler.defer([]{
			called = true;
		});
		
		auto task2 = scheduler.defer([]{
			called2 = true;
		});
		
		CHECK(!called);
		CHECK(!called2);
		
		CHECK(!task1.done());
		CHECK(!task2.done());
		
		task2.cancel();
		
		CHECK(!task1.done());
		CHECK(task2.done());
		
		deferred_update(scheduler);
		
		CHECK(task1.done());
		REQUIRE(called);
		CHECK(task2.done());
		REQUIRE(!called2);
	}
	
	SECTION("Multiple tasks cancel both") {
		static bool called2;
		called2 = false;
		
		auto task1 = scheduler.defer([]{
			called = true;
		});
		
		auto task2 = scheduler.defer([]{
			called2 = true;
		});
		
		CHECK(!called);
		CHECK(!called2);
		
		CHECK(!task1.done());
		CHECK(!task2.done());
		
		task1.cancel();
		task2.cancel();
		
		CHECK(task1.done());
		CHECK(task2.done());
		
		deferred_update(scheduler);
		
		CHECK(task1.done());
		REQUIRE(!called);
		CHECK(task2.done());
		REQUIRE(!called2);
	}
	
	SECTION("Cancel steps check later") {
		auto task = scheduler.defer(1s, []{
			called = true;
		});
		
		deferred_update(scheduler, Time{0.5});
		
		CHECK(!called);
		task.cancel();
		
		deferred_update(scheduler, Time{0.2});
		
		CHECK(task.done());
		REQUIRE(!called);
		
		deferred_update(scheduler, Time{0.3});
	}
}
