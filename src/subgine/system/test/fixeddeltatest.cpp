#include "../fixeddelta.h"
#include "catch2/catch.hpp"
#include <array>

using namespace sbg;
using namespace std::literals;

TEST_CASE("Fixed Delta", "[system]") {
	auto fd = FixedDelta{};
	
	SECTION("Can accumulate time") {
		fd.accumulate(FixedDelta::tick);
		
		SECTION("Can iterate on deltas") {
			auto n = std::size_t{};
			for (auto dt : fd.deltas()) {
				REQUIRE(dt == FixedDelta::tick);
				++n;
			}
			REQUIRE(n == 1);
		}
		
		SECTION("Can iterate on fraction of delta") {
			fd.accumulate(FixedDelta::tick / 2);
			auto n = std::size_t{};
			auto const value = std::array{FixedDelta::tick, FixedDelta::tick / 2};
			
			for (auto dt : fd.deltas()) {
				REQUIRE(dt == value[n]);
				++n;
			}
			
			REQUIRE(n == 2);
			
			SECTION("Will iterate two full delta when possible") {
				fd.accumulate(FixedDelta::tick / 2);
				auto n = std::size_t{};
				auto const value = std::array{FixedDelta::tick, FixedDelta::tick};
				
				for (auto dt : fd.deltas()) {
					REQUIRE(dt == value[n]);
					++n;
				}
				
				REQUIRE(n == 2);
			}
		}
		
		SECTION("Can consume time") {
			fd.consume(FixedDelta::tick);
			
			auto n = std::size_t{};
			for ([[maybe_unused]] auto dt : fd.deltas()) {
				++n;
				break;
			}
			REQUIRE(n == 0);
		}
	}
	
	SECTION("Will try to fill in fast frames") {
		fd.accumulate(FixedDelta::tick / 8);
		auto n = std::size_t{};
		for (auto dt : fd.deltas()) {
			REQUIRE(dt == FixedDelta::tick / 8);
		}
		REQUIRE(n == 0);
	}
	
	SECTION("Will ignore very small missing time budget") {
		fd.accumulate(FixedDelta::tick - 1ns);
		auto n = std::size_t{};
		auto const values = std::array{FixedDelta::tick, -1ns};
		for (auto dt : fd.deltas()) {
			REQUIRE(dt != -1ns);
			REQUIRE(dt == values[n]);
			++n;
			fd.consume(dt);
		}
		REQUIRE(n == 1);
	}
	
	SECTION("Will not do excessive iteration even in the worst case") {
		fd.accumulate(FixedDelta::tick * 2 - (FixedDelta::tick / 8 + 2ns));
		auto n = std::size_t{};
		for ([[maybe_unused]] auto dt : fd.deltas()) {
			++n;
		}
		REQUIRE(n == 3);
	}
	
	SECTION("Can tell if an update is necessary") {
		REQUIRE(not fd.needUpdate());
		fd.accumulate(1ns);
		REQUIRE(fd.needUpdate());
		fd.accumulate(-2ns);
		REQUIRE(not fd.needUpdate());
	}
}
