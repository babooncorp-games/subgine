#include "deferredscheduler.h"

#include <algorithm>

using namespace sbg;

void DeferredScheduler::execute(Time time) {
	auto const l = std::lock_guard{_mutex};
	{
		auto const l = std::lock_guard{_addMutex};
		_tasks.reserve(_tasks.size() + _addedTasks.size());
		std::move(_addedTasks.begin(), _addedTasks.end(), std::back_inserter(_tasks));
		_addedTasks.clear();
	}
	
	{
		auto const l = std::lock_guard{_removedMutex};
		
		auto begin = _tasks.begin();
		auto end = _tasks.end();
		for (std::size_t i = 0; i < _removedTasks.size(); ++i) {
			while (begin != end && begin->id != _removedTasks[i]) ++begin;
			
			if (begin == end) {
				break;
			} else {
				std::swap(*begin, *--end);
			}
		}
		
		_tasks.erase(end, _tasks.end());
		_removedTasks.clear();
	}
	
	{
		_tasks.erase(std::remove_if(_tasks.begin(), _tasks.end(), [&](auto& task) {
			task.until -= std::chrono::duration<double>{time.current};
			
			if (task.until <= std::chrono::duration<double>{0}) {
				task.execute();
				return true;
			}
			
			return false;
		}), _tasks.end());
	}
}

void DeferredScheduler::TaskHandle::cancel() {
	if (!_scheduler || _id == nullid) return;
	
	auto const l = std::lock_guard{_scheduler->_removedMutex};
	_scheduler->_removedTasks.emplace_back(_id);
	_id = nullid;
}

bool DeferredScheduler::TaskHandle::done() const noexcept {
	if (!_scheduler || _id == nullid) return true;
	
	auto const l1 = std::lock_guard{_scheduler->_mutex};
	auto const l2 = std::lock_guard{_scheduler->_addMutex};
	auto const l3 = std::lock_guard{_scheduler->_removedMutex};
	
	auto const& scheduler = *_scheduler;
	
	auto const current_task = [&](auto const& task) { return task.id == _id; };
	return std::find(scheduler._removedTasks.begin(), scheduler._removedTasks.end(), _id) != scheduler._removedTasks.end() || !(
		std::find_if(scheduler._tasks.begin(), scheduler._tasks.end(), current_task) != scheduler._tasks.end() ||
		std::find_if(scheduler._addedTasks.begin(), scheduler._addedTasks.end(), current_task) != scheduler._addedTasks.end()
	);
}
