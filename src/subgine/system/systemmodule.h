#pragma once

namespace sbg {

struct SystemModuleService;

struct SystemModule {
	friend auto service_map(SystemModule const&) -> SystemModuleService;
};

} // namespace sbg
