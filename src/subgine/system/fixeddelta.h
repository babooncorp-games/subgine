#pragma once

#include <chrono>
#include <algorithm>

namespace sbg {

struct FixedDelta {
	// Power of 2 closest to 60 PFS
	static constexpr auto tick = std::chrono::nanoseconds{16'777'216};
	static constexpr auto threshold = 8;
	
	struct DeltaIterator;
	
	struct DeltaSentinel {
		explicit constexpr DeltaSentinel(std::chrono::nanoseconds frameBudget) noexcept : frameBudget{frameBudget} {}
		
	private:
		friend DeltaIterator;
		std::chrono::nanoseconds frameBudget;
	};
	
	struct DeltaIterator {
		explicit constexpr DeltaIterator(std::chrono::nanoseconds remaining, std::chrono::nanoseconds partialStep) noexcept :
			remaining{remaining}, partialStep{partialStep}, delta{nextDelta(remaining, partialStep)} {}
		
		auto operator!=(DeltaSentinel sentinel) const noexcept {
			return remaining > std::min(sentinel.frameBudget / threshold, tick / threshold);
		}
		
		constexpr auto operator++() noexcept -> DeltaIterator& {
			remaining -= delta;
			partialStep = (partialStep + delta) % tick;
			delta = nextDelta(remaining, partialStep);
			
			return *this;
		}
		
		constexpr auto operator*() const noexcept -> std::chrono::nanoseconds const& {
			return delta;
		}
		
		constexpr auto operator->() const noexcept -> std::chrono::nanoseconds const* {
			return &delta;
		}
		
	private:
		std::chrono::nanoseconds remaining;
		std::chrono::nanoseconds partialStep;
		std::chrono::nanoseconds delta;
	};
	
	struct DeltaRange {
		explicit constexpr DeltaRange(
			std::chrono::nanoseconds frameBudget,
			std::chrono::nanoseconds partialStep
		) noexcept :
			frameBudget{frameBudget},
			partialStep{partialStep} {}
		
		constexpr auto begin() const noexcept -> DeltaIterator {
			return DeltaIterator{frameBudget, partialStep};
		}
		
		constexpr auto end() const noexcept -> DeltaSentinel {
			return DeltaSentinel{frameBudget};
		}
		
	private:
		std::chrono::nanoseconds frameBudget;
		std::chrono::nanoseconds partialStep;
	};
	
	constexpr auto nextDelta(std::chrono::nanoseconds additionalBudget = {}) const noexcept -> std::chrono::nanoseconds {
		return nextDelta(frameBudget + additionalBudget, partialStep);
	}
	
	constexpr void accumulate(std::chrono::nanoseconds delta) {
		frameBudget += delta;
	}
	
	constexpr void consume(std::chrono::nanoseconds const delta) {
		using namespace std::literals;
		
		frameBudget -= delta;
		partialStep = (delta + partialStep) % tick;
	}
	
	constexpr auto deltas() -> DeltaRange {
		return DeltaRange{frameBudget, partialStep};
	}
	
	[[nodiscard]]
	constexpr auto needUpdate() const noexcept -> bool {
		return frameBudget > std::chrono::nanoseconds{0};
	}
	
private:
	static constexpr auto closestFraction(std::chrono::nanoseconds budget) noexcept -> std::chrono::nanoseconds {
		using namespace std::literals;
		
		auto fraction = std::int32_t{0};
		
		if (budget <= 0ns) {
			return 1ns;
		}
		
		while (budget < (tick / (1 << fraction))) {
			++fraction;
		}
		
		return (tick / (1 << fraction));
	}
	
	static constexpr auto nextDelta(std::chrono::nanoseconds budget, std::chrono::nanoseconds partialStep) -> std::chrono::nanoseconds {
		if (budget > (tick - partialStep) - tick / threshold) {
			return tick - partialStep;
		} else {
			return closestFraction(budget);
		}
	}
	
	std::chrono::nanoseconds partialStep = {};
	std::chrono::nanoseconds frameBudget = {};
};

} // namespace sbg
