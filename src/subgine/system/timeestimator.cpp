#include "timeestimator.h"

#include "fixeddelta.h"

using namespace sbg;
using namespace std::literals;

void TimeEstimator::reset(std::chrono::high_resolution_clock::time_point time) noexcept {
	_last = time;
	_next = FixedDelta::tick;
	_current = FixedDelta::tick;
}

// TODO: Revise logic here
void TimeEstimator::update(std::chrono::high_resolution_clock::time_point time) noexcept {
	auto const lastNext = _next;
	auto const delta = time - _last;
	
	_next = (delta + lastNext, _current); // ? What was the intention here?
	_current = delta;
	_last = time;
}

auto TimeEstimator::current() const noexcept -> std::chrono::nanoseconds {
	return _current;
}

auto TimeEstimator::next() const noexcept -> std::chrono::nanoseconds {
	return _next;
}

auto TimeEstimator::lastUpdate() const noexcept -> std::chrono::high_resolution_clock::time_point {
	return _last;
}
