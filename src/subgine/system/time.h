#pragma once

#include <chrono>

namespace sbg {

struct Time {
	double current = 0;
	double next = 0;
	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
};

} // namespace sbg
