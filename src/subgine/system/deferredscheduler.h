#pragma once

#include "time.h"

#include "subgine/common/traits.h"
#include "subgine/common/kangaru.h"

#include <vector>
#include <mutex>
#include <tuple>
#include <chrono>
#include <functional>
#include <limits>
#include <type_traits>

#include "subgine/common/define.h"

namespace sbg {

struct DeferredSchedulerService;

struct DeferredScheduler {
	struct TaskHandle {
		constexpr TaskHandle() = default;
		explicit constexpr TaskHandle(DeferredScheduler& scheduler, std::size_t id) noexcept : _scheduler{&scheduler}, _id{id} {}
		
		void cancel();
		bool done() const noexcept;
		
	private:
		static constexpr auto nullid = std::size_t{std::numeric_limits<std::size_t>::max()};
		DeferredScheduler* _scheduler = nullptr;
		std::size_t _id = nullid;
	};
	
	template<typename... Args>
	auto defer(std::invocable<Args...> auto&& task, Args&&... args) {
		return defer({}, FWD(task), FWD(args)...);
	}
	
	template<typename... Args>
	auto defer(std::chrono::duration<double> until, std::invocable<Args...> auto&& task, Args&&... args) -> TaskHandle {
		std::lock_guard l{_addMutex};
		auto id = _nextId++;
		
		_addedTasks.emplace_back(DeferredTask{until, [task = FWD(task), ...args = FWD(args)]() mutable {
			std::invoke(FWD(task), FWD(args)...);
		}, id});
		
		return TaskHandle{*this, id};
	}
	
	void execute(Time time);
	
private:
	
	struct DeferredTask {
		std::chrono::duration<double> until;
		std::function<void()> execute;
		std::size_t id;
	};
	
	std::mutex _mutex;
	std::mutex _addMutex;
	std::mutex _removedMutex;
	std::vector<DeferredTask> _tasks;
	std::vector<DeferredTask> _addedTasks;
	std::vector<std::size_t> _removedTasks;
	std::size_t _nextId = 0;
	
	friend auto service_map(DeferredScheduler const&) -> DeferredSchedulerService;
};

} // namespace sbg

#include "subgine/common/undef.h"
