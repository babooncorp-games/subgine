#pragma once

#include <chrono>

namespace sbg {

struct TimeEstimator {
	void reset(std::chrono::high_resolution_clock::time_point time) noexcept;
	void update(std::chrono::high_resolution_clock::time_point time) noexcept;
	
	auto current() const noexcept -> std::chrono::nanoseconds;
	auto next() const noexcept -> std::chrono::nanoseconds;
	auto lastUpdate() const noexcept -> std::chrono::high_resolution_clock::time_point;
	
private:
	std::chrono::nanoseconds _next = {};
	std::chrono::nanoseconds _current = {};
	std::chrono::high_resolution_clock::time_point _last = {};
};

} // namespace sbg
