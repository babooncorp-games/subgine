#pragma once

#include "time.h"
#include "timeestimator.h"
#include "fixeddelta.h"
#include "deferredscheduler.h"

#include "subgine/common/ownershiptoken.h"
#include "subgine/common/traits.h"
#include "subgine/common/types.h"
#include "subgine/common/kangaru.h"

#include <thread>
#include <mutex>
#include <functional>
#include <vector>
#include <optional>
#include <concepts>

namespace sbg {

struct MainEngineService;

struct MainEngine {
private:
	template<typename T>
	using execute_t = decltype(std::declval<T>().execute(std::declval<Time const&>()));
	
	template<typename T>
	using owner_t = decltype(std::declval<T>().owner(std::declval<OwnershipToken const&>()));
	
	template<typename T>
	static constexpr bool is_self_owned = is_detected_v<owner_t, T>;
	
public:
	// TODO: To remove when GCC12 is fixed
	MainEngine() = default;
	MainEngine(MainEngine&&) noexcept;
	auto operator=(MainEngine&&) noexcept -> MainEngine&;

	template<typename T> requires(std::invocable<remove_rvalue_reference_t<T>, Time const&> or requires(T&& engine, sbg::Time const& time) { engine.execute(time); })
	auto add(T&& engine) -> OwnershipToken {
		using U = remove_rvalue_reference_t<T>;
		using EngineType = std::conditional_t<is_detected_v<execute_t, U>, ErasedEngine<U>, LambdaEngine<U>>;
		
		auto owneship = std::make_shared<char>();
		auto e = std::make_unique<EngineType>(std::forward<T>(engine));
		
		if constexpr (is_self_owned<U>) {
			e->_engine.owner(OwnershipToken{owneship});
		}
		
		_enginesToAdd.emplace_back(std::move(e), owneship);
		
		return OwnershipToken{owneship};
	}
	
	void reset();
	void update();
	
	auto time() const noexcept -> Time const&;
	
	void setupContainer(kgr::container& container);
	
private:
	using clock_type = std::chrono::high_resolution_clock;
	using time_point_type = clock_type::time_point;
	
	DeferredScheduler _scheduler;
	TimeEstimator _estimator;
	FixedDelta _fixedDelta;
	Time _time;
	std::optional<std::chrono::nanoseconds> _lastNextTime;
	
	struct Engine {
		virtual ~Engine() = default;
		virtual void execute(Time const& time) = 0;
	};
	
	template<typename T>
	struct ErasedEngine : Engine {
		explicit ErasedEngine(T engine) noexcept : _engine{std::forward<T>(engine)} {}
		
		void execute(Time const& time) override {
			_engine.execute(time);
		}
		
		T _engine;
	};
	
	template<typename T>
	struct LambdaEngine : Engine {
		explicit LambdaEngine(T engine) noexcept : _engine{std::forward<T>(engine)} {}
		
		void execute(Time const& time) override {
			_engine(time);
		}
		
		T _engine;
	};
	
	std::vector<std::pair<std::unique_ptr<Engine>, std::weak_ptr<void const>>> _engines;
	std::vector<std::pair<std::unique_ptr<Engine>, std::weak_ptr<void const>>> _enginesToAdd;
	
	friend auto service_map(MainEngine const&) -> MainEngineService;
};

} // namespace sbg
