#include "mainengine.h"

#include <ratio>
#include <chrono>
#include <algorithm>

#include "service/deferredschedulerservice.h"

using namespace std::literals;

namespace sbg {

constexpr auto maxFrameTime = FixedDelta::tick * 4;

template<typename T, typename P>
static constexpr auto to_seconds(std::chrono::duration<T, P> duration) {
	return std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(duration).count();
}

void MainEngine::reset() {
	_estimator.reset(std::chrono::high_resolution_clock::now());
}

void MainEngine::update() {
	// Update engines
	
	_engines.erase(std::remove_if(std::begin(_engines), std::end(_engines), [](auto const& engine) {
		return engine.second.expired();
	}), _engines.end());
	
	if (!_enginesToAdd.empty()) {
		_engines.reserve(_engines.size() + _enginesToAdd.size());
		std::move(_enginesToAdd.begin(), _enginesToAdd.end(), std::back_inserter(_engines));
		_enginesToAdd.clear();
	}
	
	auto const do_update = [&](std::chrono::nanoseconds current, std::chrono::nanoseconds next, time_point_type now) {
		auto const time = sbg::Time{to_seconds(current), to_seconds(next), now};
		
		_time = time;
		
		for (auto const& [engine, ref] : _engines) {
			if (auto const r = ref.lock()) {
				engine->execute(time);
			}
		}
		
		_scheduler.execute(time);
		_fixedDelta.consume(current);
	};
	
	// Compute deltas
	auto const lastUpdate = _estimator.lastUpdate();
	auto const lastNext = _estimator.next();
	auto const first = _fixedDelta.nextDelta(lastNext);
	_estimator.update(std::chrono::high_resolution_clock::now());
	
	auto const current = _estimator.current();
	_fixedDelta.accumulate(std::min(current, maxFrameTime));
	
	if (_fixedDelta.needUpdate()) {
		auto const range = _fixedDelta.deltas();
		auto begin = range.begin();
		auto const end = range.end();
		
		auto current = _lastNextTime.value_or(*begin);
		auto next = *++begin;
		auto currentNow = lastUpdate;
		
		for (; begin != end ; ++begin) {
			next = *begin;
			currentNow += current;
			
			do_update(current, next, currentNow);
			
			current = next;
		}
		
		currentNow += current;
		next = _fixedDelta.nextDelta(lastNext);
		_lastNextTime = next;
		
		do_update(current, next, currentNow);
	}
}

auto MainEngine::time() const noexcept -> Time const& {
	return _time;
}

void MainEngine::setupContainer(kgr::container& container) {
	container.emplace<DeferredSchedulerService>(_scheduler);
}

} // namespace sbg
