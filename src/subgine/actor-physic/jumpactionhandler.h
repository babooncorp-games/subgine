#pragma once

#include "jumpaction.h"

#include "subgine/entity/component.h"

namespace sbg {

template<int>
struct PhysicPoint;

template<int n>
struct JumpActionHandler {
	JumpActionHandler(sbg::Component<PhysicPoint<n>> object, double strenght);
	
	void handle(JumpAction action);
	
private:
	sbg::Component<PhysicPoint<n>> _object;
	double _strenght;
};

using JumpActionHandler2D = JumpActionHandler<2>;
using JumpActionHandler3D = JumpActionHandler<3>;

extern template struct JumpActionHandler<2>;
extern template struct JumpActionHandler<3>;

} // namespace sbg
