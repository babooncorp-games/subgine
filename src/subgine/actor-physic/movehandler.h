#pragma once

#include "moveaction.h"

#include "subgine/entity/component.h"

namespace sbg {

template<int>
struct PhysicPoint;

template<int n>
struct MoveHandler {
	MoveHandler(sbg::Component<PhysicPoint<n>> object, double speed = 0);
	void handle(MoveAction<n> action);
	
private:
	sbg::Component<PhysicPoint<n>> _object;
	double _speed;
};

using Move2DHandler = MoveHandler<2>;
using Move3DHandler = MoveHandler<3>;

extern template struct MoveHandler<2>;
extern template struct MoveHandler<3>;

} // namespace sbg
