#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

template<int n>
struct MoveAction {
	explicit MoveAction(Vector<n, double> _movement);

	const Vector<n, double> movement;
};

using Move2DAction = MoveAction<2>;
using Move3DAction = MoveAction<3>;

extern template struct MoveAction<2>;
extern template struct MoveAction<3>;

} // namespace sbg
