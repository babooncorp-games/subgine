#pragma once

#include "../actorphysicmodule.h"

#include "subgine/actor/service/actorcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ActorPhysicModuleService : kgr::single_service<ActorPhysicModule>,
	autocall<
		&ActorPhysicModule::setupActorCreator
	> {};

} // namespace sbg

