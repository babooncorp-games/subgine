#include "actorphysicmodule.h"

#include "subgine/actor-physic.h"

#include "subgine/entity.h"
#include "subgine/actor.h"
#include "subgine/physic.h"

using namespace sbg;

namespace {
	template<int n>
	auto jumpHandlerBuilder(Entity entity, Property data) -> JumpActionHandler<n> {
		return JumpActionHandler<n>{sbg::Component<PhysicPoint<n>>{entity}, double{data["strength"]}};
	}

	template<int n>
	auto rollHandlerBuilder(Entity entity, Property data) -> RollActionHandler<n> {
		return RollActionHandler<n>{sbg::Component<PhysicBody<n>>{entity}, double{data["strength"]}};
	}

	template<int n>
	auto moveHandlerBuilder(Entity entity, Property data) -> MoveHandler<n> {
		return MoveHandler<n>{sbg::Component<PhysicPoint<n>>{entity}, double{data["speed"]}};
	}
} // namespace

void ActorPhysicModule::setupActorCreator(ActorCreator& af) const {
	af.add("jump2D", jumpHandlerBuilder<2>);
	af.add("jump3D", jumpHandlerBuilder<3>);
	af.add("roll2D", rollHandlerBuilder<2>);
	af.add("move2D", moveHandlerBuilder<2>);
	af.add("move3D", moveHandlerBuilder<3>);
}
