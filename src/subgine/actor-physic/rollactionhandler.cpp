#include "rollactionhandler.h"

#include "subgine/physic.h"

namespace sbg {

template<int n>
RollActionHandler<n>::RollActionHandler(sbg::Component<PhysicBody<n>> object, double speed) : _object{object}, _speed{speed} {}

template<int n>
void RollActionHandler<n>::handle(MoveAction<n> action) {
	_object->setForce("move", Vector2d{_speed * action.movement.x / 2, 0}, Vector2d{0, -1});
	_object->setForce("move2", Vector2d{-_speed * action.movement.x / 2, 0}, Vector2d{0, 1});
}

template struct RollActionHandler<2>;
// template struct RollActionHandler<3>;

} // namespace sbg
