#pragma once

namespace sbg {

struct JumpAction {
	JumpAction(double _ratio = 1);
	
	const double ratio;
};

} // namespace sbg
