#pragma once

#include "moveaction.h"

#include "subgine/entity/component.h"

#include <memory>

namespace sbg {

template<int>
struct PhysicBody;

template<int n>
struct RollActionHandler {
	RollActionHandler(sbg::Component<PhysicBody<n>> object, double speed);
	void handle(MoveAction<n> action);
	
private:
	sbg::Component<PhysicBody<n>> _object;
	double _speed;
};

extern template struct RollActionHandler<2>;
extern template struct RollActionHandler<3>;

using Roll2DActionHandler = RollActionHandler<2>;
using Roll3DActionHandler = RollActionHandler<3>;

} // namespace sbg
