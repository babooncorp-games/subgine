#include "moveaction.h"

namespace sbg {

template<int n>
MoveAction<n>::MoveAction(Vector<n, double> _movement) : movement{_movement} {}

template struct MoveAction<2>;
template struct MoveAction<3>;

} // namespace sbg
