#include "jumpactionhandler.h"

#include "subgine/physic.h"

namespace sbg {

template<int n>
JumpActionHandler<n>::JumpActionHandler(sbg::Component<PhysicPoint<n>> object, double strenght) : _object{object}, _strenght{strenght} {}

template<int n>
void JumpActionHandler<n>::handle(JumpAction action) {
	Vector<n, double> impulse;
	impulse.y = -_strenght * action.ratio;
	_object->accumulatePulse("jump", impulse);
}

template struct JumpActionHandler<2>;
template struct JumpActionHandler<3>;

} // namespace sbg
