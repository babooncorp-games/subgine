#pragma once

namespace sbg {

struct ActorPhysicModuleService;
struct ActorCreator;

struct ActorPhysicModule {
	void setupActorCreator(ActorCreator& af) const;

	friend auto service_map(ActorPhysicModule const&) -> ActorPhysicModuleService;
};

} // namespace sbg
