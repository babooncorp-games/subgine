#include "movehandler.h"

#include "subgine/physic.h"

namespace sbg {

template<int n>
MoveHandler<n>::MoveHandler(sbg::Component<PhysicPoint<n>> object, double speed) : _object{object}, _speed{speed} {}

template<int n>
void MoveHandler<n>::handle(MoveAction<n> action) {
	_object->setForce("move", action.movement * _speed);
}

template struct MoveHandler<2>;
template struct MoveHandler<3>;

} // namespace sbg
