#pragma once

namespace sbg {

struct ComponentCreator;
struct GraphModuleService;
struct GraphEngine;
struct MainEngine;
struct EntityBindingCreator;

struct GraphModule {
	// GraphEngine is a cross-scene service, we need to instantiate it for the whole module
	void setupMainEngine(MainEngine& mainEngine, GraphEngine& graphEngine) const;
	void setupEntityBindingCreator(EntityBindingCreator& entityBindingCreator) const;
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	friend auto service_map(GraphModule const&) -> GraphModuleService;
};

} // namespace sbg
