#include "graphengine.h"

#include "subgine/entity/component.h"
#include "subgine/log.h"

#include <ranges>
#include <algorithm>
#include <numeric>

using namespace sbg;

GraphEngine::GraphEngine(EntityManager& entityManager) noexcept : _entityManager{&entityManager} {}

auto GraphEngine::unused_or_new_node(Level& level, Node node) -> std::pair<Node const&, std::uint32_t> {
	if (level.free == no_parent.index) {
		auto const index = static_cast<std::uint32_t>(level.nodes.size());
		auto const& new_node = level.nodes.emplace_back(std::move(node));
		return std::pair<Node const&, std::uint32_t>{new_node, index};
	} else {
		auto& unused = level.nodes[level.free];
		auto const index = level.free;
		auto const generation = unused.generation;
		
		// This pops one element in the free list
		auto const next_unused = unused.parent_index;
		level.free = next_unused;
		
		unused = std::move(node);
		unused.generation = generation;
		
		return std::pair<Node const&, std::uint32_t>{unused, index};
	}
}

auto GraphEngine::invalidate(Level& level, std::size_t index) -> void{
	auto& node = level.nodes[index];
	node.owner = {};
	node.matrix = {};
	++node.generation;
	
	// This push a new element in the freelist
	node.parent_index = level.free;
	level.free = index;
}

auto GraphEngine::add(Entity entity, Transform const* transform, GraphKey parent, Transform const& origin) -> GraphAddResult {
	if (parent.level == no_parent.level) {
		if (_graph.empty()) {
			_graph.resize(1);
		}
		auto& level = _graph.front();
		auto const [inserted, index] = unused_or_new_node(level, Node{
			.transform = transform,
			.matrix = std::make_unique<glm::mat4>(1.f),
			.origin = origin,
			.owner = entity,
		});
		
		return GraphAddResult{
			.matrix = inserted.matrix.get(),
			.key = GraphKey{
				.index = index,
				.level = 0,
				.generation = inserted.generation,
			},
		};
	} else {
		auto& parent_node = _graph[parent.level].nodes[parent.index];
		
		if (parent_node.generation != parent.generation) {
			sbg::Log::error(SBG_LOG_INFO,
				"Trying to add a child to a dead node level",
				log::enquote(parent.level),
				"with index",
				log::enquote(parent.index),
				"generation",
				log::enquote(parent.generation)
			);
			
			return GraphAddResult{nullptr, no_parent};
		}
		
		if (_graph.size() <= parent.level + 1u) {
			_graph.resize(parent.level + 2);
		}
		
		auto& level = _graph[parent.level + 1];
		auto const [inserted, index] = unused_or_new_node(level, Node{
			.transform = transform,
			.matrix = std::make_unique<glm::mat4>(1.f),
			.origin = origin,
			.parent_index = parent.index,
			.owner = entity,
			.parent_generation = parent.generation,
		});
		
		return GraphAddResult{
			.matrix = inserted.matrix.get(),
			.key = GraphKey{
				.index = index,
				.level = static_cast<std::uint16_t>(parent.level + 1),
				.generation = inserted.generation,
			},
		};
	}
}

auto GraphEngine::walk_bottom_up(GraphKey key, auto function) -> void {
	auto& node = _graph[key.level].nodes[key.index];
	if (node.generation != key.generation) return;
	
	auto current = _graph.begin() + key.level + 1;
	auto const end = _graph.end();
	for (; current != end; ++current) {
		std::size_t index = 0;
		for (auto& node : current->nodes) {
			if (node.parent_index == key.index and node.parent_generation == key.generation and _entityManager->alive(node.owner)) {
				walk_bottom_up(GraphKey{
					.index = static_cast<std::uint32_t>(index),
					.level = static_cast<std::uint16_t>(current - _graph.begin()),
					.generation = node.generation,
				}, function);
			}
			++index;
		}
	}
	
	function(key, node);
}

auto GraphEngine::walk_top_down(GraphKey key, auto function) -> void {
	auto& node = _graph[key.level].nodes[key.index];
	if (node.generation != key.generation) return;
	
	function(key, node);
	
	auto current = _graph.begin() + key.level + 1;
	auto const end = _graph.end();
	for (; current != end; ++current) {
		std::size_t index = 0;
		for (auto& node : current->nodes) {
			if (node.parent_index == key.index and node.parent_generation == key.generation and _entityManager->alive(node.owner)) {
				walk_top_down(GraphKey{
					.index = static_cast<std::uint32_t>(index),
					.level = static_cast<std::uint16_t>(current - _graph.begin()),
					.generation = node.generation,
				}, function);
			}
			++index;
		}
	}
}

auto GraphEngine::parent(GraphKey key) const -> GraphKey {
	if (key.level == 0) return no_parent;
	auto& node = _graph[key.level].nodes[key.index];
	if (node.generation != key.generation or node.parent_index == no_parent.index) return no_parent;
	auto& parent = _graph[key.level - 1].nodes[node.parent_index];
	if (parent.generation != node.parent_generation) return no_parent;
	
	return GraphKey{
		.index = node.parent_index,
		.level = static_cast<std::uint16_t>(key.level - 1),
		.generation = node.parent_generation,
	};
}

auto GraphEngine::matrix(GraphKey key) const -> glm::mat4 const* {
	auto& node = _graph[key.level].nodes[key.index];
	if (node.generation != key.generation) return nullptr;
	return node.matrix.get();
}

auto GraphEngine::valid(GraphKey key) const -> bool {
	auto& node = _graph[key.level].nodes[key.index];
	return node.generation == key.generation;
}

auto GraphEngine::detach_tree(GraphKey key) -> void {
	walk_bottom_up(key, [&](GraphKey key, Node& node) {
		invalidate(_graph[key.level], key.index);
	});
}

auto GraphEngine::destroy_tree(GraphKey key) -> void {
	walk_bottom_up(key, [&](GraphKey key, Node& node) {
		if (_entityManager->alive(node.owner)) {
			_entityManager->destroy(node.owner);
		}
		
		invalidate(_graph[key.level], key.index);
	});
}

auto GraphEngine::move_tree(GraphKey key, GraphKey parent) -> GraphKey {
	assert(valid(key));
	assert(valid(parent));
	auto& node = _graph[key.level].nodes[key.index];
	auto const& parent_node = _graph[parent.level].nodes[parent.index];
	
	if (node.generation == key.generation and node.matrix and _entityManager->alive(node.owner)) {
		if (_graph.size() <= parent.level + 1u) {
			_graph.resize(parent.level + 2);
		}
		
		// We check if we want to move the node to a different level
		if (key.level != parent.level + 1) {
			auto& level = _graph[parent.level + 1];
			auto const [inserted, index] = unused_or_new_node(level, Node{
				.transform = node.transform,
				.matrix = std::move(node.matrix),
				.origin = node.origin,
				.parent_index = parent.index,
				.owner = node.owner,
				.parent_generation = parent_node.generation
			});
			
			auto const new_key = GraphKey{
				.index = index,
				.level = static_cast<std::uint16_t>(parent.level + 1),
				.generation = inserted.generation,
			};
			
			for (auto const child : children(key)) {
				// We call move_tree recursively until we walk the whole branch
				move_tree(child, new_key);
			}
			
			// We invalidate the old node
			invalidate(_graph[key.level], key.index);
		
			return new_key;
		} else {
			node.parent_index = parent.index;
			node.parent_generation = parent_node.generation;
			return key;
		}
	} else {
		Log::error(SBG_LOG_INFO,
			"Key at level",
			log::enquote(key.level),
			"index",
			log::enquote(key.index),
			"with generation",
			log::enquote(key.generation),
			"not found in graph while setting parent"
		);
		
		return key; // not affected, nothing done
	}
}

auto GraphEngine::update_origin(GraphKey key, Transform const& origin) -> void {
	auto& node = _graph[key.level].nodes[key.index];
	
	if (node.generation == key.generation and node.matrix) {
		node.origin = origin;
	} else {
		Log::error(SBG_LOG_INFO,
			"Key at level",
			log::enquote(key.level),
			"index",
			log::enquote(key.index),
			"with generation",
			log::enquote(key.generation),
			"not found in graph while setting origin"
		);
	}
}

auto GraphEngine::execute(Time time) -> void {
	// TODO: Add optimizations when using parent node
	auto prev = _graph.begin();
	auto current = std::next(prev);
	auto const end = _graph.end();
	
	// On the first level, we only need to update the matrix to the origin's value
	// TODO: Can we just remove this and update when inserting instead?
	for (auto& node : prev->nodes) {
		if (not node.matrix) continue;
		*node.matrix = node.origin.matrix();
	}
	
	// On other levels, we change the matrix to be the parent's transform multiplied by origin
	for (; current != end; ++current, ++prev) {
		// TODO: C++23 use enumerate
		auto index = std::size_t{0};
		for (auto& node : current->nodes) {
			if (not node.matrix) {
				++index;
				continue;
			}
			
			auto const& parent = prev->nodes[node.parent_index];
			
			if (parent.generation == node.parent_generation and _entityManager->alive(parent.owner) and _entityManager->alive(node.owner)) {
				*node.matrix = parent.transform->matrix() * node.origin.matrix() * (*parent.matrix);
			} else {
				_entityManager->destroy(node.owner);
				invalidate(*current, index);
			}
			
			++index;
		}
	}
}

void GraphEngine::owner(OwnershipToken owner) {
	_owner = owner;
}
