#pragma once

#include "subgine/graphic/component/transform.h"
#include "subgine/entity/basicentity.h"
#include "subgine/entity/entity.h"
#include "subgine/entity/entitymanager.h"
#include "subgine/system/mainengine.h"

#include <glm/glm.hpp>

#include <cstdint>
#include <vector>
#include <memory>
#include <limits>
#include <ranges>

namespace sbg {

struct GraphEngineService;

struct GraphKey {
	std::uint32_t index;
	std::uint16_t level;
	std::uint8_t generation;
	
	friend auto operator<=>(GraphKey const& lhs, GraphKey const& rhs) noexcept = default;
	friend auto operator==(GraphKey const& lhs, GraphKey const& rhs) noexcept -> bool = default;
};

struct GraphEngine {
	/**
	 * @brief Construct a new Graph Engine, the constructor must be called using injection
	 * 
	 * @param entityManager The injected entity manager
	 */
	explicit GraphEngine(EntityManager& entityManager) noexcept;
	
	static constexpr auto no_parent = GraphKey{
		.index = std::numeric_limits<std::uint32_t>::max(),
		.level = std::numeric_limits<std::uint16_t>::max(),
		.generation = 0,
	};
	
	struct GraphAddResult {
		glm::mat4 const* matrix;
		GraphKey key;
	};
	
	/////////////////////
	// Node operations //
	/////////////////////
	/**
	 * @brief Adds a new node into the graph. It will return a child matrix from the parent object
	 * 
	 * @param entity The owner of the node. This entity will also become owned by the parent node
	 * @param transform The transform to use as this node's position. Usually it's the entity's transform component
	 * @param parent The node that will become parent of the one we will create.
	 * @param origin Sets a different origin from the parent's matrix.
	 * @return GraphAddResult Both a matrix that represents the origin in which the entity's transform component will apply on and a key in the graph
	 */
	auto add(Entity entity, Transform const* transform, GraphKey parent = no_parent, Transform const& origin = {}) -> GraphAddResult;
	
	/**
	 * @brief Changes the node's origin in relation to its parent
	 * 
	 * @param key The key of the node to change the origin
	 * @param origin The new origin to apply
	 */
	void update_origin(GraphKey key, Transform const& origin);
	
	//////////////////////
	// Query operations //
	//////////////////////
	/**
	 * @brief Fetch the key of the parent
	 * 
	 * @param key The key of the node to get the parent from
	 * @return GraphKey The parent's key
	 */
	auto parent(GraphKey key) const -> GraphKey;
	
	/**
	 * @brief Gets the matrix of a particular node
	 * 
	 * @param key The key of the node to get the matrix
	 * @return glm::mat4 const* The node's matrix
	 */
	auto matrix(GraphKey key) const -> glm::mat4 const*;
	
	/**
	 * @brief Returns if a key is considered valid (alive)
	 * 
	 * @param key The key to check if the node still exists in the graph
	 * @return true When the key points to a valid node
	 * @return false When the key points to no node or a dead node
	 */
	auto valid(GraphKey key) const -> bool;
	
	/**
	 * @brief Returns the child nodes of a node
	 * 
	 * @param key The node from which to get the children
	 * @return std::ranges::view A view of the children. Each element of the range is a GraphKey
	 */
	inline auto children(GraphKey key) const -> std::ranges::view auto {
		auto& node = _graph[key.level].nodes[key.index];
		
		// The first level will match no children, since all nodes on that level
		// has no_parent as their parent index and no_parent as their generation.
		// So we use that as the lower level to match none
		auto& lower_level = key.level + 1u == _graph.size() or node.generation != key.generation
			? _graph.front()
			: _graph[key.level + 1];
		
		return std::ranges::views::iota(0u, lower_level.nodes.size())
			| std::ranges::views::filter([key, &lower_level](auto index) {
				if (index >= lower_level.nodes.size()) return false;
				auto& child = lower_level.nodes[index];
				return child.matrix and child.parent_index == key.index and child.parent_generation == key.generation;
			})
			| std::ranges::views::transform([key, &lower_level](auto index) {
				auto& child = lower_level.nodes[index];
				return GraphKey{
					.index = index,
					.level = static_cast<std::uint16_t>(key.level + 1u),
					.generation = child.generation,
				};
			});
	}
	
	//////////////////////////
	// Tree-wise operations //
	//////////////////////////
	/**
	 * @brief Delete all nodes from a graph branch without destroying owner entities.
	 * 
	 * Useful when you're in a situation you need to delete a prent entity without deleting the child entities.
	 * 
	 * Use at your own risk since this will invalidate the matrix pointer the child entity might still be using.
	 * 
	 * @param key The root to detach nodes down from
	 */
	auto detach_tree(GraphKey key) -> void;
	
	/**
	 * @brief Destroys a branch from a tree
	 *
	 * Deletes all owner entities assigned to the nodes in the branch
	 * 
	 * @param key The root to destroy down from
	 */
	auto destroy_tree(GraphKey key) -> void;
	
	/**
	 * @brief Moves a whole branch to a different parent node
	 * 
	 * This will invalidate all child keys, but won't invalidate the matrix pointer those child entities are using.
	 * 
	 * @param key The root node to move
	 * @param parent The new parent to reattach the branch to
	 * @return GraphKey The new graph key generated from the reattaching operation on the node referenced by key
	 */
	auto move_tree(GraphKey key, GraphKey parent) -> GraphKey;
	
	//////////////////////
	// Engine interface //
	//////////////////////
	
	/**
	 * @brief Updates the graph engine. This will update the matrices of all nodes.
	 * 
	 * @param time Delta time since the last update
	 */
	auto execute(Time time) -> void;
	
	/**
	 * @brief Make the engine owner of itself. When it is deleted, the main engine will remove it from the list of engines.
	 * 
	 * @param owner A token that keeps the engine alive. When the token is destroyed, the engine will be removed from the list of engines.
	 */
	auto owner(OwnershipToken owner) -> void;
	
private:
	struct Node {
		Transform const* transform;
		std::unique_ptr<glm::mat4> matrix;
		Transform origin;
		std::uint32_t parent_index = no_parent.index;
		BasicEntity owner;
		std::uint8_t generation = 0;
		std::uint8_t parent_generation = no_parent.generation;
	};
	
	struct Level {
		std::vector<Node> nodes;
		std::uint32_t free = no_parent.index;
	};
	
	auto walk_bottom_up(GraphKey key, auto function) -> void;
	auto walk_top_down(GraphKey key, auto function) -> void;
	auto unused_or_new_node(Level&, Node) -> std::pair<Node const&, std::uint32_t>;
	auto invalidate(Level& level, std::size_t index) -> void;
	
	OwnershipToken _owner;
	EntityManager* _entityManager;
	std::vector<Level> _graph;
	
	friend auto service_map(GraphEngine const&) -> GraphEngineService;
};

} // namespace sbg
