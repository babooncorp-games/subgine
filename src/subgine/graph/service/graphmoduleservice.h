#pragma once

#include "../graphmodule.h"

#include "graphengineservice.h"

#include "subgine/system/service/mainengineservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct GraphModuleService : kgr::single_service<GraphModule>,
	autocall<
		&GraphModule::setupMainEngine,
		&GraphModule::setupEntityBindingCreator,
		&GraphModule::setupComponentCreator
	> {};

} // namespace sbg

