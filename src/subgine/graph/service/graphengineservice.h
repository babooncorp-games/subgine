#pragma once

#include "../engine/graphengine.h"

#include "subgine/entity/service/entitymanagerservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct GraphEngineService : kgr::single_service<GraphEngine, kgr::autowire> {};

} // namespace sbg
