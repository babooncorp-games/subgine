#include "graphmodule.h"

#include "subgine/graph.h"
#include "subgine/entity.h"

using namespace sbg;

void GraphModule::setupMainEngine(MainEngine& mainEngine, GraphEngine& graphEngine) const {
	mainEngine.add(graphEngine);
}

void GraphModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("graph-from-dynamic-parent", [](GraphEngine& graphEngine, sbg::Entity entity, sbg::Property data) {
		auto const entity_graph_node = Component<EntityGraphNode>{entity};
		auto const parent = Component<DynamicParent>{entity};
		auto const [matrix, key] = graphEngine.add(entity, Component<Transform>{entity}.unchecked_ptr(), parent->key, parent->origin);
		entity_graph_node->key = key;
		entity_graph_node->matrix = matrix;
	});
	
	entityBindingCreator.add("graph-no-parent", [](GraphEngine& graphEngine, sbg::Entity entity, sbg::Property data) {
		auto const entity_graph_node = Component<EntityGraphNode>{entity};
		auto const [matrix, key] = graphEngine.add(entity, Component<Transform>{entity}.unchecked_ptr());
		entity_graph_node->key = key;
		entity_graph_node->matrix = matrix;
	});
}

void GraphModule::setupComponentCreator(sbg::ComponentCreator& componentCreator) const {
	componentCreator.add("entity-graph-node", [] {
		return EntityGraphNode{};
	});
}
