#include "graphtransform.h"

using namespace sbg;

auto GraphTransform::matrix() const noexcept -> glm::mat4 {
	if (parent) {
		return transform->matrix() * origin.matrix() * *parent;
	} else {
		return transform->matrix() * origin.matrix();
	}
}
