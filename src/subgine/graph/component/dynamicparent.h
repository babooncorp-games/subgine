#pragma once

#include "subgine/entity/entity.h"
#include "subgine/graphic/component/transform.h"
#include "../engine/graphengine.h"

namespace sbg {

struct EntityGraphNode {
	GraphKey key;
	glm::mat4 const* matrix;
};

struct DynamicParent {
	GraphKey key;
	sbg::Transform origin;
};

} // namespace sbg
