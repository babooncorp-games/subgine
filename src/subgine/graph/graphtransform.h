#pragma once

#include "engine/graphengine.h"

namespace sbg {

struct GraphTransform {
	glm::mat4 const* parent;
	Transform const* transform; // this is usually the component
	Transform origin;
	
	auto matrix() const noexcept -> glm::mat4;
};

} // namespace sbg
