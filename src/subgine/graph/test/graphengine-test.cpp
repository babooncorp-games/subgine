#include "catch2/catch.hpp"

#include "../engine/graphengine.h"
#include "../service/graphengineservice.h"

#include "subgine/entity.h"
#include "subgine/vector.h"

#include "subgine/graphic/component/transform.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <ranges>

using namespace sbg;

auto constexpr identity = glm::mat4{1.f};
auto const translation_vector1 = sbg::Vector3f{2, 3, 4};
auto const translated1 = glm::translate(glm::vec3{2.f, 3.f, 4.f});
auto const translated2 = translated1 * glm::translate(glm::vec3{2.f, 3.f, 4.f});

TEST_CASE("Can declare root entites", "[graph]") {
	kgr::container{}.invoke([](EntityManager& entities, GraphEngine& graph) {
		auto const run = [&]{ graph.execute(sbg::Time{}); };
		
		auto const entity1 = entities.create();
		auto const entity2 = entities.create();
		
		entity1.assign(sbg::Transform{});
		entity2.assign(sbg::Transform{});
		
		auto const transform1 = sbg::Component<sbg::Transform>{entity1};
		auto const [mat1, key1] = graph.add(entity1, transform1.ptr());
		
		CHECK(graph.matrix(key1) == mat1);
		
		SECTION("The root entity will have a specific key and will recycle space") {
			CHECK(key1.generation == 0);
			CHECK(key1.index == 0);
			CHECK(key1.level == 0);
			
			graph.detach_tree(key1);
			auto const [mat1_2, key1_2] = graph.add(entity1, transform1.ptr());

			CHECK(key1_2.generation == 1);
			CHECK(key1_2.index == 0);
			CHECK(key1_2.level == 0);
			
			auto const [mat1_3, key1_3] = graph.add(entity1, transform1.ptr());
			auto const [mat1_4, key1_4] = graph.add(entity1, transform1.ptr());
			auto const [mat1_5, key1_5] = graph.add(entity1, transform1.ptr());
			auto const [mat1_6, key1_6] = graph.add(entity1, transform1.ptr());
			
			CHECK(key1_3.generation == 0);
			CHECK(key1_3.index == 1);
			CHECK(key1_3.level == 0);
			CHECK(key1_4.generation == 0);
			CHECK(key1_4.index == 2);
			CHECK(key1_4.level == 0);
			CHECK(key1_5.generation == 0);
			CHECK(key1_5.index == 3);
			CHECK(key1_5.level == 0);
			CHECK(key1_6.generation == 0);
			CHECK(key1_6.index == 4);
			CHECK(key1_6.level == 0);
			
			graph.detach_tree(key1_3);
			graph.detach_tree(key1_4);
			graph.detach_tree(key1_5);
			graph.detach_tree(key1_6);
			
			auto const [mat1_3_2, key1_3_2] = graph.add(entity1, transform1.ptr());
			auto const [mat1_4_2, key1_4_2] = graph.add(entity1, transform1.ptr());
			auto const [mat1_5_2, key1_5_2] = graph.add(entity1, transform1.ptr());
			auto const [mat1_6_2, key1_6_2] = graph.add(entity1, transform1.ptr());
			
			CHECK(key1_3_2.generation == 1);
			CHECK(key1_3_2.index == 4);
			CHECK(key1_3_2.level == 0);
			CHECK(key1_4_2.generation == 1);
			CHECK(key1_4_2.index == 3);
			CHECK(key1_4_2.level == 0);
			CHECK(key1_5_2.generation == 1);
			CHECK(key1_5_2.index == 2);
			CHECK(key1_5_2.level == 0);
			CHECK(key1_6_2.generation == 1);
			CHECK(key1_6_2.index == 1);
			CHECK(key1_6_2.level == 0);
		}
		
		SECTION("Add a child entity") {
			auto const entity2 = entities.create();
			entity2.assign(sbg::Transform{});
			auto const transform2 = sbg::Component<sbg::Transform>{entity2};
			
			auto const [mat2, key2] = graph.add(entity2, transform2.ptr(), key1);
			
			SECTION("Run with initial matrix values") {
				run();
				
				CHECK(*mat1 == identity);
				CHECK(*mat2 == identity);
				
				SECTION("Set origin of the parent should affect the parent and the child matrix") {
					graph.update_origin(key1, sbg::Transform{}.position3d(translation_vector1));
					run();
					CHECK(*mat1 == translated1);
					CHECK(*mat2 == translated1);
				}
			}
			
			SECTION("Add a second child") {
				auto const entity3 = entities.create();
				auto const transform3 = sbg::Component<sbg::Transform>{entity3};
				entity3.assign(sbg::Transform{});
				
				auto const [mat3, key3] = graph.add(entity3, transform3.ptr(), key1);
				
				SECTION("Run with initial matrix values") {
					run();
					
					CHECK(*mat1 == identity);
					CHECK(*mat2 == identity);
					CHECK(*mat3 == identity);
				}
				
				SECTION("Can iterate on children") {
					auto children = graph.children(key1);
					auto begin = std::ranges::begin(children);
					auto const end = std::ranges::end(children);
					CHECK(*begin++ == key2);
					CHECK(*begin++ == key3);
					CHECK(begin == end);
				}
				
				SECTION("All children have the correct parent") {
					auto children = graph.children(key1);
					auto begin = std::ranges::begin(children);
					auto const end = std::ranges::end(children);
					CHECK(graph.parent(*begin++) == key1);
					CHECK(graph.parent(*begin++) == key1);
					CHECK(begin == end);
				}
				
				SECTION("Leaf node have no children") {
					auto children = graph.children(key3);
					auto const begin = std::ranges::begin(children);
					auto const end = std::ranges::end(children);
					CHECK(begin == end);
				}
			}
			
			SECTION("Run with translation on child transform") {
				*transform2 = transform2->position3d(translation_vector1);
				run();
				
				CHECK(*mat1 == identity);
				CHECK(*mat2 == identity);
				
				SECTION("Child of translated child is translated") {
					auto const entity3 = entities.create();
					auto const transform3 = sbg::Component<sbg::Transform>{entity3};
					entity3.assign(sbg::Transform{});
					auto const [mat3, key3] = graph.add(entity3, transform3.ptr(), key2);
					
					run();
					CHECK(*mat2 == identity);
					CHECK(*mat3 == translated1);
					
					SECTION("Can move one leaf to another root") {
						auto const moved_key3 = graph.move_tree(key3, key1);
						CHECK(not graph.valid(key3));
						CHECK(graph.parent(moved_key3) == key1);
						auto key1_children = graph.children(key1);
						CHECK(std::ranges::find(key1_children, moved_key3) != std::ranges::end(key1_children));
						
						run();
						CHECK(*mat3 == identity);
						
						*transform1 = transform1->position3d(translation_vector1);
						
						run();
						CHECK(*mat3 == translated1);
						CHECK(*mat2 == translated1);
					}
					
					SECTION("Can move a whole branch to a new root on the same level") {
						auto const entity4 = entities.create();
						auto const transform4 = sbg::Component<sbg::Transform>{entity4};
						entity4.assign(sbg::Transform{});
						auto const [mat4, key4] = graph.add(entity4, transform4.ptr());
						
						auto const new_key2 = graph.move_tree(key2, key4);
						
						REQUIRE(new_key2 == key2);
						REQUIRE(graph.valid(new_key2));
						
						// We moved the node on the same level as before, so the key stays the same.
						CHECK(graph.valid(key2));
						CHECK(graph.valid(key3));
						
						run();
						CHECK(*mat2 == identity);
						CHECK(*mat3 == translated1);
						
						*transform4 = transform4->position3d(translation_vector1);
						
						run();
						CHECK(*mat2 == translated1);
						CHECK(*mat3 == translated2);
					}
					
					SECTION("Can move a whole branch to a new root on a different level") {
						auto const entity4 = entities.create();
						auto const transform4 = sbg::Component<sbg::Transform>{entity4};
						entity4.assign(sbg::Transform{});
						auto const [mat4, key4] = graph.add(entity4, transform4.ptr(), key1);
						
						auto const new_key2 = graph.move_tree(key2, key4);
						
						REQUIRE(new_key2 != key2);
						REQUIRE(graph.valid(new_key2));
						
						// We moved the node on a different level, so the key changes.
						CHECK(not graph.valid(key2));
						CHECK(not graph.valid(key3));
						
						run();
						CHECK(*mat2 == identity);
						CHECK(*mat3 == translated1);
						
						*transform4 = transform4->position3d(translation_vector1);
						
						run();
						CHECK(*mat2 == translated1);
						CHECK(*mat3 == translated2);
					}
					
					SECTION("Translation to the root is transitive") {
						*transform1 = transform1->position3d(translation_vector1);
						run();
						
						CHECK(*mat1 == identity);
						CHECK(*mat2 == translated1);
						CHECK(*mat3 == translated2);
						
						SECTION("Can change parent node") {
							graph.move_tree(key3, key1);
							run();
						
							CHECK(*mat1 == identity);
							CHECK(*mat2 == translated1);
							CHECK(*mat3 == translated1);
						}
					}
					
					SECTION("Can invalidate all nodes from a tree") {
						graph.detach_tree(key1);
						run();
						
						CHECK(not graph.valid(key1));
						CHECK(not graph.valid(key2));
						CHECK(not graph.valid(key3));
						
						CHECK(entity1.alive());
						CHECK(entity2.alive());
						CHECK(entity3.alive());
					}
					
					SECTION("Can invalidate all nodes from a child tree") {
						graph.detach_tree(key2);
						run();
						
						CHECK(graph.valid(key1));
						CHECK(not graph.valid(key2));
						CHECK(not graph.valid(key3));
						
						CHECK(entity1.alive());
						CHECK(entity2.alive());
						CHECK(entity3.alive());
						
						SECTION("Root entity has no children") {
							auto children = graph.children(key1);
							auto const begin = std::ranges::begin(children);
							auto const end = std::ranges::end(children);
							CHECK(begin == end);
						}
					}
					
					SECTION("Can destroy all entities part of a tree") {
						graph.destroy_tree(key1);
						run();
						
						CHECK(not entity1);
						CHECK(not entity2);
						CHECK(not entity3);
						
						CHECK(not graph.valid(key1));
						CHECK(not graph.valid(key2));
						CHECK(not graph.valid(key3));
					}
					
					SECTION("Can destroy all entities part of a child tree") {
						graph.destroy_tree(key2);
						run();
						
						CHECK(entity1);
						CHECK(not entity2);
						CHECK(not entity3);
						
						CHECK(graph.valid(key1));
						CHECK(not graph.valid(key2));
						CHECK(not graph.valid(key3));
					}

					SECTION("Destroy a parent will destroy children") {
						entity2.destroy();
						run();
						CHECK(not entity3.alive());
					}
				}
			}
			
			SECTION("Run with translation on parent transform") {
				*transform1 = transform1->position3d(translation_vector1);
				run();
				
				CHECK(*mat1 == identity);
				CHECK(*mat2 == translated1);
			}
		}
	});
}
