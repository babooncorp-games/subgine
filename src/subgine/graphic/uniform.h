#pragma once

#include "dynstructbuffer.h"
#include "modelhandle.h"
#include "subgine/graphic/detail/updatinguniformdata.h"
#include "uniformdata.h"

#include "subgine/entity/entity.h"
#include "subgine/common/hash.h"
#include "subgine/common/map.h"
#include "subgine/resource/resourcetag.h"

#include <span>
#include <vector>
#include <string_view>
#include <cstddef>

#include "subgine/common/define.h"

namespace sbg {

struct UniformSet {
	using uniforms_t = hash_map<hash_t, UniformData, key_is_hash>;
	uniforms_t uniforms;
	
	UniformSet() = default;
	explicit UniformSet(uniforms_t uniforms) noexcept : uniforms{std::move(uniforms)} {}
	
	void emplace(hash_t hash, UniformData data) &;
	void emplace(std::string_view name, UniformData uniform) &;
	auto find(hash_t name) const& noexcept -> uniforms_t::const_iterator;
	void clear() & noexcept;
	
	template<typename T>
	auto optional(hash_t name) const& noexcept -> std::optional<T> {
		auto const it = find(name);
		
		if (it != uniforms.end()) {
			if constexpr (std::same_as<T, GenericResource>) {
				if (auto const* value = std::get_if<GenericResource>(&it->second)) {
					return *value;
				}
			} else if constexpr (resource_tag<T>) {
				if (auto const* value = std::get_if<GenericResource>(&it->second); value and value->is<T>()) {
					return T{value->hash};
				}
			} else {
				if constexpr (!std::is_same_v<T, DynStructBuffer> && !std::is_same_v<T, GenericResource>) {
					if (auto const* uniform = std::get_if<T const*>(&it->second)) {
						return **uniform;
					}
				}
				
				if (auto const* value = std::get_if<T>(&it->second)) {
					return *value;
				}
			}
		}
		
		return {};
	}
	
	template<typename T>
	auto required(hash_t name) const& noexcept -> std::conditional_t<resource_tag<T>, T, T const&> {
		auto const it = find(name);
		
		if (it != uniforms.end()) {
			if constexpr (std::same_as<T, GenericResource>) {
				if (auto const* value = std::get_if<GenericResource>(&it->second)) {
					return *value;
				}
			} else if constexpr (resource_tag<T>) {
				if (auto const* value = std::get_if<GenericResource>(&it->second); value and value->is<T>()) {
					return T{value->hash};
				}
			} else {
				if constexpr (!std::is_same_v<T, DynStructBuffer> && !std::is_same_v<T, GenericResource>) {
					if (auto const* uniform = std::get_if<T const*>(&it->second)) {
						return **uniform;
					}
				}
				
				if (auto const* value = std::get_if<T>(&it->second)) {
					return *value;
				}
			}
		}
		
		fatal_not_found(name);
	}
	
private:
	[[noreturn]]
	static void fatal_not_found(std::string_view name) noexcept;
	
	[[noreturn]]
	static void fatal_not_found(hash_t name) noexcept;
};

struct ModelInfo {
	ModelHandle handle;
	Entity entity;
	UniformSet uniforms;
};

struct ModelUniformContext {
	UniformSet const& model;
	UniformSet const& environment;
};

struct UpdatingInfo {
	UpdatingUniformDataStorage uniform;
	ModelHandle model;
	BasicEntity entity;
};

struct ComputedUpdatingInfo {
	UpdatingUniformDataStorage uniform;
	UniformSet uniforms;
	BasicEntity entity;
};

enum struct uniform_kind : std::uint8_t {
	value, updating_pointer, updating_function, resource
};

template<typename T, uniform_kind k>
struct UniformArgument {
	static constexpr auto kind = k;
	hash_t hash;
	T uniform;
};

template<typename T>
concept uniform_metadata = requires(T const& t) {
	   t.kind == uniform_kind::value
	or t.kind == uniform_kind::updating_pointer
	or t.kind == uniform_kind::resource;
	
	{ t.uniform };
	{ t.hash } -> std::same_as<hash_t const&>;
};

template<typename T>
concept uniform_metadata_function =
	    uniform_metadata<T>
	and requires(T const& t) {
		t.kind == uniform_kind::updating_function;
		requires is_updating_callback<decltype(t.uniform)>;
	};

namespace uniform {
	auto value(hash_t hash, auto uniform) -> uniform_metadata auto {
		return UniformArgument<decltype(uniform), uniform_kind::value>{hash, std::move(uniform)};
	}
	
	template<typename T> requires (not std::is_pointer_v<T> and not is_updating_callback<T>) 
	auto updating(hash_t hash, T const& uniform) -> uniform_metadata auto {
		return UniformArgument<T const*, uniform_kind::updating_pointer>{hash, &uniform};
	}
	
	auto resource(hash_t hash, auto uniform) -> uniform_metadata auto {
		return UniformArgument<GenericResource, uniform_kind::resource>{hash, uniform};
	}
	
	template<typename T> requires(is_updating_callback<T>)
	auto updating(hash_t hash, T const& uniform) -> uniform_metadata_function auto {
		return UniformArgument<T, uniform_kind::updating_function>{hash, uniform};
	}
}

struct UniformEncoderBase {
protected:
	friend auto operator<<(std::derived_from<UniformEncoderBase> auto& self, uniform_metadata auto&& uniform) -> auto& {
		self.add_uniform(FWD(uniform));
		return self;
	}
};

struct ModelUniformEncoder : UniformEncoderBase {
	using uniforms_t = hash_map<hash_t, ModelUniformData, key_is_hash>;
	uniforms_t uniforms;
	
	auto add_uniform(uniform_metadata auto&& arg) {
		if constexpr (arg.kind == uniform_kind::value or arg.kind == uniform_kind::updating_pointer) {
			uniforms.emplace(arg.hash, ModelUniformData{UniformData{std::in_place_type<decltype(arg.uniform)>, arg.uniform}});
		} else if constexpr (arg.kind == uniform_kind::updating_function) {
			uniforms.emplace(arg.hash, make_updating_uniform(FWD(arg).uniform));
		} else if constexpr (arg.kind == uniform_kind::resource) {
			uniforms.emplace(arg.hash, ModelUniformData{UniformData{std::in_place_type<GenericResource>, FWD(arg).uniform}});
		}
	}
	
private:
	template<typename T> requires is_updating_callback<T> [[nodiscard]]
	static auto make_updating_uniform(T function) noexcept -> ModelUniformData {
		using UniformType = function_result_t<T>;
		return ModelUniformData{
			std::in_place_type<TaggedModelUpdatingFunction>,
			type_id<UniformType>,
			&ModelUniformEncoder::batch_update<T>
		};
	}
	
	template<typename T>
	static auto batch_update(EntityManager const* entities, std::span<UpdatingInfo> data) noexcept {
		using Model = model_type_t<T>;
		using UniformType = function_result_t<T>;
		
		constexpr auto function = T{};
		for (auto& info : data) {
			if (not info.entity.definitely_dead() and entities->alive(info.entity)) {
				std::get<UniformType>(info.uniform) = function(*static_cast<Model const*>(info.model.pointer));
			}
		}
	}
};

struct ComputedUniformEncoder : UniformEncoderBase {
	using uniforms_t = hash_map<hash_t, ModelUniformData, key_is_hash>;
	uniforms_t uniforms;
	
	auto add_uniform(uniform_metadata auto&& arg) {
		if constexpr (arg.kind == uniform_kind::value or arg.kind == uniform_kind::updating_pointer) {
			uniforms.emplace(arg.hash, ModelUniformData{UniformData{std::in_place_type<decltype(arg.uniform)>, arg.uniform}});
		} else if constexpr (arg.kind == uniform_kind::updating_function) {
			uniforms.emplace(arg.hash, make_updating_uniform(FWD(arg).uniform));
		} else if constexpr (arg.kind == uniform_kind::resource) {
			uniforms.emplace(arg.hash, ModelUniformData{UniformData{std::in_place_type<GenericResource>, FWD(arg).uniform}});
		}
	}
	
private:
	template<typename T> requires(is_updating_callback<T> and std::same_as<model_type_t<T>, ModelUniformContext>) [[nodiscard]]
	static auto make_updating_uniform(T function) noexcept -> ModelUniformData {
		using UniformType = function_result_t<T>;
		return ModelUniformData{
			std::in_place_type<TaggedComputedUpdatingFunction>,
			type_id<UniformType>,
			&ComputedUniformEncoder::batch_update<T>
		};
	}
	
	template<typename T>
	static auto batch_update(EntityManager const* entities, std::span<ComputedUpdatingInfo> data, UniformSet const& environment) noexcept {
		using UniformType = function_result_t<T>;
		
		constexpr auto function = T{};
		for (auto& info : data) {
			if (not info.entity.definitely_dead() and entities->alive(info.entity)) {
				std::get<UniformType>(info.uniform) = function(ModelUniformContext{info.uniforms, environment});
			}
		}
	}
};

struct EnvironmentUniformEncoder : UniformEncoderBase {
	using uniforms_t = hash_map<hash_t, UniformData, key_is_hash>;
	uniforms_t uniforms;
	
	auto add_uniform(uniform_metadata auto&& arg) {
		if constexpr (arg.kind == uniform_kind::value or arg.kind == uniform_kind::updating_pointer) {
			uniforms.emplace(arg.hash, UniformData{std::in_place_type<decltype(arg.uniform)>, arg.uniform});
		} else if constexpr (arg.kind == uniform_kind::updating_function) {
			// TODO: Implement updating function with arbitrary context
			static_assert(not std::same_as<decltype(arg), decltype(arg)>, "Updating function unimplemented");
		} else if constexpr (arg.kind == uniform_kind::resource) {
			uniforms.emplace(arg.hash, UniformData{std::in_place_type<GenericResource>, FWD(arg).uniform});
		}
	}
};

} // namespace sbg

#include "subgine/common/undef.h"
