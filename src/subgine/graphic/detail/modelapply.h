#pragma once

#include "../component/visual.h"

#include "subgine/entity/entity.h"
#include "subgine/resource/property.h"
#include "subgine/common/traits.h"
#include "subgine/common/kangaru.h"
#include "subgine/common/dropping_invoke.h"

#include <vector>
#include <string>
#include <string_view>
#include <type_traits>

namespace sbg {

struct ModelApply {
	template<typename B, typename R = dropping_invoke_result_t<kgr::invoker, B&, Entity, Property>> requires(serializable_model<R> or std::same_as<Model, R>)
	void operator()(kgr::invoker invoker, B& builder, std::string_view name, Visual& visual, Entity entity, Property data) const {
		if constexpr (std::same_as<Model, R>) {
			visual.add(
				std::string{name},
				dropping_invoke(invoker, builder, entity, std::move(data))
			);
		} else {
			auto const painter = data["painter"];
			visual.add(
				std::string{name},
				Model{
					dropping_invoke(invoker, builder, entity, std::move(data)),
					painter.isNull()
						? std::vector<std::string>{data["painters"]}
						: std::vector<std::string>{std::string{painter}}
				}
			);
		}
	}
};

} // namespace sbg
