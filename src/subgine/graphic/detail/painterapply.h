#pragma once

#include "../creator/computeduniformcreator.h"
#include "../painter.h"

#include "subgine/common/dropping_invoke.h"
#include "subgine/common/traits.h"
#include "subgine/resource/property.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct PainterApply {
	constexpr explicit PainterApply(
		ComputedUniformCreator const& computedUniformCreator
	) noexcept :
		_computedUniformCreator{&computedUniformCreator} {}
	
	template<typename B, typename R = dropping_invoke_result_t<kgr::invoker, B&, Property>, enable_if_i<!std::is_void_v<R> && std::is_constructible_v<Painter, R, ComputedUniforms&&>> = 0>
	auto operator()(kgr::invoker invoker, B& builder, Property data) const -> Painter {
		auto computedUniforms = ComputedUniforms{};
		
		for (auto const uniform : data["uniforms"]) {
			auto const uniformSource = _computedUniformCreator->create(invoker, uniform["type"], uniform["data"]);
			computedUniforms.uniform_source(uniformSource);
		}
		
		return Painter{dropping_invoke(invoker, builder, data), std::move(computedUniforms)};
	}
	
private:
	ComputedUniformCreator const* _computedUniformCreator;
};

} // namespace sbg
