#pragma once

#include "../modelhandle.h"

#include "subgine/common/detection.h"
#include "subgine/common/type_id.h"
#include "subgine/common/function_traits.h"
#include "subgine/entity/basicentity.h"
#include "subgine/vector/vector.h"

#include <glm/glm.hpp>
#include <memory>
#include <span>
#include <variant>
#include <any>

namespace sbg {

template<typename T>
using model_type_t = std::decay_t<function_argument_t<0, T>>;

template<typename T,
	typename UniformType = detected_t<function_result_t, T>,
	typename Model = std::decay_t<kgr::detail::meta_list_element_t<0, detected_or<kgr::detail::meta_list<nonesuch>, function_arguments_t, T>>>
>
inline constexpr auto is_updating_callback =
	    is_function_v<T>
	and not std::is_same_v<Model, nonesuch>
	and not std::is_same_v<UniformType, nonesuch>
	and not std::is_reference_v<UniformType>
	and std::is_default_constructible_v<T>
	and requires { function_traits<T>::arity == 1; }
	and requires { requires function_traits<T>::is_noexcept; };

using UpdatingUniformDataStorage = std::variant<
	bool,
	float,
	double,
	std::int32_t,
	std::uint32_t,
	std::uint64_t,
	Vector2f,
	Vector3f,
	Vector4f,
	Vector2i,
	Vector3i,
	Vector4i,
	Vector2d,
	Vector3d,
	Vector4d,
	glm::mat2,
	glm::mat3,
	glm::mat4,
	std::any
>;

struct EntityManager;
struct UpdatingInfo;
struct ComputedUpdatingInfo;
struct UniformSet;

using ComputedUpdatingFunction = auto(*)(EntityManager const*, std::span<ComputedUpdatingInfo>, UniformSet const&) noexcept -> void;
using ModelUpdatingFunction = auto(*)(EntityManager const*, std::span<UpdatingInfo>) noexcept -> void;

struct TaggedModelUpdatingFunction {
	type_id_t type;
	ModelUpdatingFunction function;
};

struct TaggedComputedUpdatingFunction {
	type_id_t type;
	ComputedUpdatingFunction function;
};

} // namespace sbg
