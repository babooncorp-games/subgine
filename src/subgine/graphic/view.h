#pragma once

#include "component/visual.h"
#include "painter.h"
#include "environmentuniforms.h"
#include "uniform.h"
#include "modelslotmap.h"

#include "subgine/common/detection.h"
#include "subgine/common/concepts.h"
#include "subgine/common/map.h"
#include "subgine/common/stable_vector.h"
#include "subgine/common/hash.h"
#include "subgine/entity/component.h"
#include "subgine/entity/componentlist.h"

#include <span>
#include <vector>
#include <string>
#include <string_view>

namespace sbg {

struct ViewService;
struct PaintJob;

struct PainterModelSystem {
	struct RenderStep {
		std::string step;
		PainterRef painter;
	};
	
	hash_map<ComputedUpdatingFunction, stable_vector<ComputedUpdatingInfo>> update_computed;
	hash_map<ModelUpdatingFunction, stable_vector<UpdatingInfo>> update_model;
	dictionary<Painter> painters;
	std::vector<RenderStep> steps;
	ModelSlotMap models;
	UniformSet environment;
};

/**
 * @brief Represent the whole scene rendering scheme.
 * 
 * Contains every objects to be render in the scene.
 * Contains every painter to be used by the scene, and this view's rendering pipeline.
 * Contained as a single service, per Scene.
 */
struct View {
	using Pipeline = std::function<void(PaintJob const&, UniformSet const&)>;
	
	explicit View(EntityManager const& entities, Pipeline pipeline, EnvironmentUniforms environment) noexcept;
	
	/**
	 * @brief Renders the scene.
	 * 
	 * Will call the rending pipeline.
	 * The rendering pipeline will call painter's prepare and draw function to render the scene.
	 * 
	 * Called one time per frame
	 */
	void render();
	
	/**
	 * @brief Adds an Entity to the view.
	 * @param entity The Entity to add. It must have a Visual component.
	 */
	void add(Entity entity);
	
	/**
	 * @brief Adds a Painter to the view.
	 * 
	 * If there is already a Painter that draws the same models,
	 * and draws during the same step, it is replaced by the given Painter.
	 * 
	 * @param name The name of the painter.
	 * @param painter The painter to add.
	 * @param steps In which steps the painter should draw.
	 */
	void addPainter(std::string name, Painter painter, std::span<std::string const> steps);
	
	/**
	 * @brief Updates Model value inside the Painter.
	 * 
	 * @param name The Model's unique identifier.
	 * @param entity The Entity that has the Visual component.
	 * 
	 * TODO: We should only receive the visual, not the entity
	 */
	void reserialize(std::string_view name, Entity entity);
	
	/**
	 * @breif Update the environment uniforms.
	 */
	void refresh();
	
private:
	EnvironmentUniforms _environment;
	EntityManager const* _entityManager;
	Pipeline _pipeline;
	PainterModelSystem _renderSystem;
	bool _needsRefresh = true;
	
	friend auto service_map(View const&) -> ViewService;
};

} // namespace sbg
