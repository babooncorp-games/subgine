#include "view.h"

#include "paintjob.h"
#include "uniformdata.h"
#include "detail/updatinguniformdata.h"

#include "subgine/log.h"

#include <algorithm>
#include <ranges>

// Must be the last header
#include "subgine/common/define.h"

using namespace sbg;

template<typename T>
	requires std::invocable<T, TaggedModelUpdatingFunction> and std::invocable<T, TaggedComputedUpdatingFunction>
static auto transform_model_uniforms(T const& convert) {
	return std::views::transform([convert](auto&& model_uniform) {
		return std::pair{
			FWD(model_uniform).first,
			std::visit([&]<typename U>(U&& uniform) -> UniformData {
				using type = std::decay_t<U>;
				if constexpr (std::same_as<type, UniformData>) {
					return FWD(uniform);
				} else if constexpr (std::same_as<type, TaggedModelUpdatingFunction>) {
					return convert(FWD(uniform));
				} else if constexpr (std::same_as<type, TaggedComputedUpdatingFunction>) {
					return convert(FWD(uniform));
				}
			}, FWD(model_uniform).second)
		};
	});
}

static auto storage_for(PainterModelSystem& system, std::size_t index) {
	return [index, &system]<typename T>(T const tagged_function) -> auto& {
		auto const [it, inserted] = [&]{
			if constexpr (std::same_as<T, TaggedModelUpdatingFunction>) {
				return system.update_model.emplace(
					tagged_function.function,
					stable_vector_size(Entity::Id::Limits::index)
				);
			} else if constexpr (std::same_as<T, TaggedComputedUpdatingFunction>) {
				return system.update_computed.emplace(
					tagged_function.function,
					stable_vector_size(Entity::Id::Limits::index)
				);
			}
		}();
		
		auto& uniforms_storage = it->second;
		
		if (uniforms_storage.size() <= index) {
			uniforms_storage.resize(index + 1);
		}
		
		return uniforms_storage[index];
	};
}

static auto uniform_with_storage(auto const storage_for, ModelInfo const& info) {
	return [storage_for, &info]<typename T>(T const tagged_function) {
		return visit_type_updating_data(tagged_function.type, [&]<typename type>() -> UniformData {
			auto& storage = [&]() -> auto& {
				if constexpr (std::same_as<T, TaggedModelUpdatingFunction>) {
					return storage_for(tagged_function) = UpdatingInfo{
						.uniform = UpdatingUniformDataStorage{std::in_place_type<type>, type{}},
						.model = info.handle,
						.entity = info.entity
					};
				} else if constexpr (std::same_as<T, TaggedComputedUpdatingFunction>) {
					return storage_for(tagged_function) = ComputedUpdatingInfo{
						.uniform = UpdatingUniformDataStorage{std::in_place_type<type>, type{}},
						.uniforms = info.uniforms,
						.entity = info.entity
					};
				}
			}();
			
			return UniformData{
				std::in_place_type<type const*>,
				std::get_if<type>(&storage.uniform)
			};
		});
	};
}

View::View(EntityManager const& entities, Pipeline pipeline, EnvironmentUniforms environment) noexcept :
	_pipeline{std::move(pipeline)}, _entityManager{&entities}, _environment{std::move(environment)} {}

void View::render() {
	_renderSystem.models.cleanup();
	for (auto& [update, updating_infos] : _renderSystem.update_model) {
		update(_entityManager, updating_infos);
	}
	
	if (_needsRefresh) {
		_renderSystem.environment = _environment.uniforms();
		for (auto& [name, painter] : _renderSystem.painters) {
			painter.refresh(_renderSystem.environment);
		}
		_needsRefresh = false;
	}
	
	_pipeline(PaintJob{*_entityManager, _renderSystem}, _renderSystem.environment);
}

void View::add(Entity entity) {
	Log::fatal([&]{ return !entity.has<Visual>(); },
		SBG_LOG_INFO, "The entity need a visual component to be added to the view"
	);
	
	auto& visual = entity.component<Visual>();
	
	for (auto& [name, model] : visual._models) {
		auto [info, index] = _renderSystem.models.insert(ModelInfo{model.handle(), entity, {}});
		
		auto uniforms = model.reserialize();
		std::ranges::move(
			uniforms | transform_model_uniforms(uniform_with_storage(storage_for(_renderSystem, index), info)),
			std::inserter(info.uniforms.uniforms, info.uniforms.uniforms.end())
		);
		
		for (auto const& painterName : model.painters) {
			auto const it = _renderSystem.painters.find(painterName);
			if (it != _renderSystem.painters.end()) {
				auto painterInfo = info;
				auto& [_, painter] = *it;
				
				auto computed = painter.uniforms_for(info.uniforms, _renderSystem.environment);
				std::ranges::move(
					computed | transform_model_uniforms(uniform_with_storage(storage_for(_renderSystem, index), painterInfo)),
					std::inserter(painterInfo.uniforms.uniforms, painterInfo.uniforms.uniforms.end())
				);
				
				painter.add(std::move(painterInfo), _renderSystem.environment);
			} else {
				Log::warning(SBG_LOG_INFO,
					"No painter named", log::enquote(painterName), "has been found while adding model", log::enquote(name)
				);
			}
		}
	}
}

void View::addPainter(std::string name, Painter painter, std::span<std::string const> steps) {
	for (auto const& step : steps) {
		_renderSystem.steps.emplace_back(step, painter);
	}
	
	_renderSystem.painters.emplace(std::move(name), std::move(painter));
}

void View::refresh() {
	_needsRefresh = true;
}

void View::reserialize(std::string_view name, Entity entity) {
	auto& visual = entity.component<Visual>();
	
	if (auto const it = visual.find(name); it != visual._models.end()) {
		auto& [name, model] = *it;
		
		auto index = _renderSystem.models.find(model.handle());
		
		if (index == _renderSystem.models.size()) {
			Log::error(SBG_LOG_INFO, "Model", log::enquote(name), "for entity", entity.id().index, "don't exist in view");
			return;
		}
		
		auto& info = _renderSystem.models[index];
		info.uniforms.clear();
		
		auto uniforms = model.reserialize();
		std::ranges::move(
			uniforms | transform_model_uniforms(uniform_with_storage(storage_for(_renderSystem, index), info)),
			std::inserter(info.uniforms.uniforms, info.uniforms.uniforms.end())
		);
		
		for (auto const& painter : model.painters) {
			if (auto const it = _renderSystem.painters.find(painter); it != _renderSystem.painters.end()) {
				auto painterInfo = info;
				auto& [_, painter] = *it;
				
				auto computed = painter.uniforms_for(info.uniforms, _renderSystem.environment);
				std::ranges::move(
					computed | transform_model_uniforms(uniform_with_storage(storage_for(_renderSystem, index), painterInfo)),
					std::inserter(painterInfo.uniforms.uniforms, painterInfo.uniforms.uniforms.end())
				);
				
				painter.update(std::move(painterInfo), _renderSystem.environment);
			}
		}
	} else {
		Log::error(SBG_LOG_INFO, "Visual component has no model named", name);
	}
}
