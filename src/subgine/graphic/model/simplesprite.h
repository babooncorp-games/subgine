#pragma once

#include "subgine/graphic/component/transform.h"
#include "subgine/graphic/texturetag.h"
#include "subgine/graphic/utility/uniformencoderforward.h"
#include "subgine/entity/component.h"

namespace sbg {

struct SimpleSprite {
	Component<Transform> transform;
	Transform origin;
	glm::mat4 const* parent = nullptr;
	TextureTag texture;
	bool visible = true;
	
	static void serialize(ModelUniformEncoder& uniforms, SimpleSprite const& sprite);
};

} // namespace sbg
