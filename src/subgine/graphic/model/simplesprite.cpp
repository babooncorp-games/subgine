#include "simplesprite.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/utility/transformmodelmatrix.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace sbg;
using namespace sbg::literals;

void SimpleSprite::serialize(ModelUniformEncoder& uniforms, SimpleSprite const& sprite) {
	uniforms
		<< uniform::resource("tex"_h, sprite.texture)
		<< uniform::updating("model"_h, TransformModelMatrix<SimpleSprite>{})
		<< uniform::value("visible"_h, sprite.visible);
}
