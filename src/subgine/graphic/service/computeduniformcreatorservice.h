#pragma once

#include "../creator/computeduniformcreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ComputedUniformCreatorService : kgr::single_service<ComputedUniformCreator, kgr::autowire> {};
using ComputedUniformFactoryService = FactoryService<ComputedUniformFactory>;

} // namespace sbg
