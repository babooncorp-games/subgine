#pragma once

#include "../camera.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct CameraService : kgr::single_service<Camera> {};

} // namespace sbg
