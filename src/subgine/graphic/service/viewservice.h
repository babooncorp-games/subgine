#pragma once

#include "../view.h"

#include "subgine/common/kangaru.h"

#include <exception>

namespace sbg {

struct ViewService : kgr::single_service<View>, kgr::supplied {};

} // namespace sbg
