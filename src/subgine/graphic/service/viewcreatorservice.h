#pragma once

#include "../creator/viewcreator.h"

#include "paintercreatorservice.h"
#include "environmentuniformcreatorservice.h"

#include "subgine/resource/service/factoryservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct ViewCreatorService : kgr::single_service<ViewCreator, kgr::autowire> {};
using ViewFactoryService = FactoryService<ViewFactory>;

} // namespace sbg
