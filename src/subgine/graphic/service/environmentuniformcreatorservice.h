#pragma once

#include "../creator/environmentuniformcreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EnvironmentUniformCreatorService : kgr::single_service<EnvironmentUniformCreator, kgr::autowire> {};
using EnvironmentUniformFactoryService = FactoryService<EnvironmentUniformFactory>;

} // namespace sbg
