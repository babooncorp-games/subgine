#pragma once

#include "../creator/modelcreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ModelCreatorService : kgr::single_service<ModelCreator> {};
using ModelFactoryService = FactoryService<ModelFactory>;

} // namespace sbg
