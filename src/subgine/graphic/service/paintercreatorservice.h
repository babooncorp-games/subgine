#pragma once

#include "../creator/paintercreator.h"

#include "computeduniformcreatorservice.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct PainterCreatorService : kgr::single_service<PainterCreator, kgr::autowire> {};
using PainterFactoryService = FactoryService<PainterFactory>;

} // namespace sbg
