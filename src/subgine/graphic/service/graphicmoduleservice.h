#pragma once

#include "../graphicmodule.h"

#include "modelcreatorservice.h"
#include "viewcreatorservice.h"
#include "computeduniformcreatorservice.h"

#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/provider/service/providercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct GraphicModuleService : kgr::single_service<GraphicModule>,
	autocall<
		&GraphicModule::setupModelCreator,
		&GraphicModule::setupViewCreator,
		&GraphicModule::setupComponentCreator,
		&GraphicModule::setupEntityBindingCreator,
		&GraphicModule::setupPluggerCreator,
		&GraphicModule::setupProviderCreator,
		&GraphicModule::setupComputedUniformCreator
	> {};

} // namespace sbg
