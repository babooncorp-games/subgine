#include "transform.h"

#include <glm/ext.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace sbg;

auto Transform::matrix() const noexcept -> glm::mat4 {
	return
		  glm::translate(glm::mat4{1.f}, glm::vec3{_position.x, _position.y, _position.z})
		* glm::toMat4(_orientation)
		* glm::scale(glm::mat4{1.f}, glm::vec3{_scale.x, _scale.y, _scale.z});
}

auto Transform::position2d() const noexcept -> Vector2f {
	return Vector2f{_position.x, _position.y};
}

auto Transform::zvalue() const noexcept -> float {
	return _position.z;
}

auto Transform::scale2d() const noexcept -> Vector2f {
	return Vector2f{_scale.x, _scale.y};
}

auto Transform::orientation2d() const noexcept -> float {
	return _orientation.z;
}

auto Transform::position2d(Vector2f position) const noexcept -> Transform {
	auto result = Transform{*this};
	result._position = {position.x, position.y, _position.z};
	return result;
}

auto Transform::position2d(Vector2f position, float zvalue) const noexcept -> Transform {
	auto result = Transform{*this};
	result._position = {position.x, position.y, zvalue};
	return result;
}

auto Transform::zvalue(float zvalue) const noexcept -> Transform {
	auto result = Transform{*this};
	result._position.z = zvalue;
	return result;
}

auto Transform::scale2d(Vector2f scale) const noexcept -> Transform {
	auto result = Transform{*this};
	result._scale = {scale.x, scale.y, 1.f};
	return result;
}

auto Transform::orientation2d(float radian) const noexcept -> Transform {
	auto result = Transform{*this};
	result._orientation = glm::angleAxis(radian, glm::vec3{0, 0, 1});
	return result;
}

auto Transform::position3d() const noexcept -> Vector3f {
	return _position;
}

auto Transform::scale3d() const noexcept -> Vector3f {
	return _scale;
}

auto Transform::orientation3d() const noexcept -> glm::quat {
	return _orientation;
}

auto Transform::position3d(Vector3f _position) const noexcept -> Transform {
	auto result = Transform{*this};
	result._position = _position;
	return result;
}

auto Transform::scale3d(Vector3f _scale) const noexcept -> Transform {
	auto result = Transform{*this};
	result._scale = _scale;
	return result;
}

auto Transform::orientation3d(glm::quat orientation) const noexcept -> Transform {
	auto result = Transform{*this};
	result._orientation = orientation;
	return result;
}

namespace sbg {

auto lerp(Transform const& start, Transform const& end, double const percent) -> Transform {
	using std::lerp;
	return Transform{}
		.position3d(lerp(start._position, end._position, percent))
		.scale3d(lerp(start._scale, end._scale, percent))
		.orientation3d(glm::slerp(start._orientation, end._orientation, static_cast<float>(percent)));
}

}
