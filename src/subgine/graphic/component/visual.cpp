#include "visual.h"

using namespace sbg;

auto Visual::add(std::string name, Model model) -> Model& {
	return _models.emplace_back(name, std::move(model)).second;
}

void Visual::remove(std::string_view name) {
	_models.erase(find(name));
}

auto Visual::model(std::string_view name) const noexcept -> Model const* {
	auto const it = find(name);
	
	if (it != _models.end()) {
		return &it->second;
	} else {
		return nullptr;
	}
}

auto Visual::model(std::string_view name) noexcept -> Model* {
	auto const it = find(name);
	
	if (it != _models.end()) {
		return &it->second;
	} else {
		return nullptr;
	}
}

auto Visual::find(std::string_view name) const noexcept -> model_container::const_iterator {
	return std::find_if(
		_models.begin(), _models.end(),
		[&](auto const& model) {
			return model.first == name;
		}
	);
}

auto Visual::find(std::string_view name) noexcept -> model_container::iterator {
	return std::find_if(
		_models.begin(), _models.end(),
		[&](auto const& model) {
			return model.first == name;
		}
	);
}
