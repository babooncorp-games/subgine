#pragma once

#include "subgine/common/math.h"
#include "subgine/vector/vector.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace sbg {

struct Transform {
	[[nodiscard]]
	auto matrix() const noexcept -> glm::mat4;
	
	[[nodiscard]]
	auto position2d() const noexcept -> Vector2f;
	
	[[nodiscard]]
	auto zvalue() const noexcept -> float;
	
	[[nodiscard]]
	auto scale2d() const noexcept -> Vector2f;
	
	[[nodiscard]]
	auto orientation2d() const noexcept -> float;
	
	[[nodiscard]]
	auto position2d(Vector2f position) const noexcept -> Transform;
	
	[[nodiscard]]
	auto position2d(Vector2f position, float zvalue) const noexcept -> Transform;
	
	[[nodiscard]]
	auto zvalue(float zvalue) const noexcept -> Transform;
	
	[[nodiscard]]
	auto scale2d(Vector2f scale) const noexcept -> Transform;
	
	[[nodiscard]]
	auto orientation2d(float radian) const noexcept -> Transform;
	
	[[nodiscard]]
	auto position3d() const noexcept -> Vector3f;
	
	[[nodiscard]]
	auto scale3d() const noexcept -> Vector3f;
	
	[[nodiscard]]
	auto orientation3d() const noexcept -> glm::quat;
	
	[[nodiscard]]
	auto position3d(Vector3f position) const noexcept -> Transform;
	
	[[nodiscard]]
	auto scale3d(Vector3f scale) const noexcept -> Transform;
	
	[[nodiscard]]
	auto orientation3d(glm::quat orientation) const noexcept -> Transform;
	
	friend auto lerp(Transform const& start, Transform const& end, double const percent) -> Transform;
	
private:
	Vector3f _position = {};
	Vector3f _scale = {1, 1, 1};
	glm::quat _orientation = {};
};

} // namespace sbg
