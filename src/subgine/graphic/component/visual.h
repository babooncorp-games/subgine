#pragma once

#include "../model.h"
#include "../utility/trait.h"

#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"
#include "subgine/log/logdecoration.h"

#include <string_view>
#include <exception>
#include <unordered_map>
#include <list>

namespace sbg {

struct View;

/**
 * @brief Component used for the drawing process.
 * 
 * A Visual component tells the engine how to draw the Entity that contains it.
 * A "Model" is in fact any given struct. Ideally, the struct should have an associated Painter.
 * 
 * @see Painter
 */
struct Visual {
	friend View;
	
	/**
	 * @brief Adds a model with the given name to the component.
	 * 
	 * @param name The identifier for the model (must me unique for the component).
	 * @param model Any struct that can be considered a "Model" (has at least one compatible Painter).
	 * @param painter Which painter with the tag the model should be added into.
	 * @param steps in which steps the model is rendered.
	 */
	template<typename T> inline
	void add(std::string name, T model, std::string painter) {
		add(name, Model{std::move(model), std::move(painter)});
	}
	
	/**
	 * @brief Adds a model with the given name to the component.
	 * 
	 * @param name The identifier for the model (must me unique for the component).
	 * @param model The model to insert into the visual component.
	 */
	auto add(std::string name, Model model) -> Model&;
	
	/**
	 * @brief Removes the model with the given name (if it is present).
	 * 
	 * @param name The identifier of the model that must be removed.
	 */
	void remove(std::string_view name);
	
	/**
	 * @brief Get a model with a name with a const visual.
	 * 
	 * @param name The name of the model to search for
	 * @return A raw pointer to model constant. If not found, the pointer is null.
	 */
	auto model(std::string_view name) const noexcept -> Model const*;
	
	/**
	 * @brief Get a model with a name
	 * 
	 * @param name The name of the model to search for
	 * @return A raw pointer to model. If not found, the pointer is null.
	 */
	auto model(std::string_view name) noexcept -> Model*;
	
	/**
	 * @brief Runs the provided function with the desired Model as its parameter.
	 * 
	 * @param name The identifier of the desired model.
	 * @param function A function (anything that defines the '()' operator) that recieves the desired model as its parameter.
	 * @tparam T Deduced. The type of the visitor.
	 * 
	 * @return true if the function has been called, false otherwise
	 */
	template<typename T>
	bool visit(std::string_view name, T function) {
		if (auto const model = find(name); model != _models.end() && model->second.model.is<model_visitor_t<T>>()) {
			function(model->second.model.as<model_visitor_t<T>>());
			return true;
		}
		
		return false;
	}

	/**
	 * @brief Runs the provided function with the desired Model as its parameter.
	 * 
	 * @param name The identifier of the desired model.
	 * @param function A function (anything that defines the '()' operator) that recieves the desired model as its parameter.
	 * @tparam T Deduced. The type of the visitor.
	 * 
	 * @return true if the function has been called, false otherwise
	 */
	template<typename T>
	bool visit(std::string_view name, T function) const {
		if (auto const model = find(name); model != _models.end() && model->second.model.is<model_visitor_t<T>>()) {
			function(model->second.model.as<const model_visitor_t<T>>());
			return true;
		}
		
		return false;
	}
	

	/**
	 * @brief Get a model of type T in the visual component.
	 * 
	 * @param name The identifier of the desired model.
	 * @tparam T the type of the model to get
	 * 
	 * @return a reference to the model instance
	 */
	template<typename T>
	auto inspect(std::string_view name) -> T& {
		if (auto const model = find(name); model != _models.end() && model->second.model.is<T>()) {
			return model->second.model.as<T>();
		}
		
		Log::fatal(SBG_LOG_INFO, "No model named", log::enquote(name), "that has the requested type");
	}
	
	/**
	 * @brief Get a model of type T in the visual component (const version).
	 * 
	 * @param name The identifier of the desired model.
	 * @tparam T the type of the model to get
	 * 
	 * @return a const reference to the model instance
	 */
	template<typename T>
	auto inspect(std::string_view name) const -> T const& {
		if (auto const model = find(name); model != _models.end() && model->second.model.is<T>()) {
			return model->second.model.as<const T>();
		}
		
		Log::fatal(SBG_LOG_INFO, "No model named", log::enquote(name), "that has the requested type");
	}
	
	/**
	 * @brief Get a model of type T in the visual component (const version).
	 * 
	 * @param name The identifier of the desired model.
	 * @tparam T the type of the model to get
	 * 
	 * @return a const reference to the model instance
	 */
	template<typename T>
	bool has(std::string_view name) const {
		return visit(name, [](const T&){});
	}
	
private:
	using model_container = std::vector<std::pair<std::string, Model>>;
	
	auto find(std::string_view name) const noexcept -> model_container::const_iterator;
	auto find(std::string_view name) noexcept -> model_container::iterator;
	
	model_container _models;
};

} // namespace sbg
