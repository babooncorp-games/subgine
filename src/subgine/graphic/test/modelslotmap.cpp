#include "catch2/catch.hpp"

#include "subgine/graphic.h"
#include "subgine/entity.h"

#include <tuple>

using namespace sbg::literals;

struct Model {
	int a;
	
	static void serialize(sbg::ModelUniformEncoder& uniforms, Model const& model) {
		uniforms.add_uniform(sbg::uniform::value("a"_h, model.a));
	}
};

auto make_entity(sbg::EntityManager& entityManager, int a) -> sbg::ModelInfo {
	auto entity = entityManager.create();
	auto visual = sbg::Visual{};
	auto& model = visual.add("model", sbg::Model{Model{a}, "any"});
	auto const uniforms = model.reserialize();
	entity.assign(std::move(visual));
	auto modelUniforms = sbg::UniformSet{};
	for (auto const& [name, uniform] : uniforms) {
		std::visit([&]<typename T>(T const& uniform) {
			if constexpr (not std::is_pointer_v<T>) {
				modelUniforms.emplace(name, uniform);
			}
		}, uniform);
	}
	return sbg::ModelInfo{model.handle(), entity, modelUniforms};
}

auto get_uniform_a(sbg::ModelInfo const& model, sbg::ModelSlotMap const& modelmap) -> int {
	return modelmap[modelmap.find(model.handle)].uniforms.required<int>("a"_h);
}

TEST_CASE("ModelSlotMap maps models to slots", "[graphic]") {
	auto map = sbg::ModelSlotMap{};
	auto entityManager = sbg::EntityManager{};
	auto generic_model1 = make_entity(entityManager, 1);
	auto generic_model2 = make_entity(entityManager, 2);
	auto generic_model3 = make_entity(entityManager, 3);
	auto generic_model4 = make_entity(entityManager, 4);
	
	auto [_1, index1] = map.insert(generic_model1);
	auto [_2, index2] = map.insert(generic_model2);
	
	SECTION("Index are the right one when inserted") {
		REQUIRE(index1 == 0);
		REQUIRE(index2 == 1);
	}
	
	SECTION("The found entities have the right index") {
		REQUIRE(index1 == map.find(generic_model1.handle));
		REQUIRE(index2 == map.find(generic_model2.handle));
	}
	
	SECTION("Can remove them") {
		map.erase(generic_model1);
		map.erase(generic_model2);
		
		CHECK(map.size() == 2);
		
		REQUIRE(map.size() == map.find(generic_model1.handle));
		REQUIRE(map.size() == map.find(generic_model2.handle));
		REQUIRE(!map.alive(index1));
		REQUIRE(!map.alive(index2));
		REQUIRE(!map[index1].entity);
		REQUIRE(!map[index2].entity);
		
		SECTION("Can add them back") {
			auto [_1, index1] = map.insert(generic_model1);
			auto [_2, index2] = map.insert(generic_model2);
			
			REQUIRE(index1 == 1);
			REQUIRE(index2 == 0);
			REQUIRE(index1 == map.find(generic_model1.handle));
			REQUIRE(index2 == map.find(generic_model2.handle));
			REQUIRE(get_uniform_a(generic_model1, map) == 1);
			REQUIRE(get_uniform_a(generic_model2, map) == 2);
			REQUIRE(map.alive(index1));
			REQUIRE(map.alive(index2));
			REQUIRE(map[index1].entity);
			REQUIRE(map[index2].entity);
		}
		
		SECTION("Can add other models") {
			auto [_3, index3] = map.insert(generic_model3);
			auto [_4, index4] = map.insert(generic_model4);
			
			REQUIRE(index3 == 1);
			REQUIRE(index4 == 0);
			REQUIRE(index3 == map.find(generic_model3.handle));
			REQUIRE(index4 == map.find(generic_model4.handle));
			REQUIRE(get_uniform_a(generic_model3, map) == 3);
			REQUIRE(get_uniform_a(generic_model4, map) == 4);
			REQUIRE(map.alive(index3));
			REQUIRE(map.alive(index4));
			REQUIRE(map[index3].entity);
			REQUIRE(map[index4].entity);
		
			SECTION("Can add the first ones back") {
				auto [_1, index1] = map.insert(generic_model1);
				auto [_2, index2] = map.insert(generic_model2);
				
				REQUIRE(index1 == 2);
				REQUIRE(index2 == 3);
				REQUIRE(index1 == map.find(generic_model1.handle));
				REQUIRE(index2 == map.find(generic_model2.handle));
				REQUIRE(get_uniform_a(generic_model1, map) == 1);
				REQUIRE(get_uniform_a(generic_model2, map) == 2);
				REQUIRE(map.alive(index1));
				REQUIRE(map.alive(index2));
				REQUIRE(map[index1].entity);
				REQUIRE(map[index2].entity);
			}
			
			REQUIRE(index3 == 1);
			REQUIRE(index4 == 0);
			REQUIRE(index3 == map.find(generic_model3.handle));
			REQUIRE(index4 == map.find(generic_model4.handle));
			REQUIRE(get_uniform_a(generic_model3, map) == 3);
			REQUIRE(get_uniform_a(generic_model4, map) == 4);
			REQUIRE(map.alive(index3));
			REQUIRE(map.alive(index4));
			REQUIRE(map[index3].entity);
			REQUIRE(map[index4].entity);
		}
	}
}
