#include "catch2/catch.hpp"

#include "subgine/graphic.h"
#include "subgine/graphic/uniform.h"

#include <cstring>

using namespace sbg::literals;

auto operator"" _b(unsigned long long value) -> std::byte {
	return static_cast<std::byte>(value);
}

TEST_CASE("DynStructBuffer forms dynamic objects", "[graphic]") {
	auto buffer = sbg::DynStructBuffer{};
	
	SECTION("Can safely push back models") {
		auto uniforms = sbg::UniformSet{};
		constexpr auto model_to_visit = int{10};
		
		buffer = sbg::DynStructBuffer{
			sbg::DynStructBuffer::Member{0, sbg::type_id<int>, "a"},
			sbg::DynStructBuffer::Member{2, sbg::type_id<double>, "b"},
			sbg::DynStructBuffer::Member{1, sbg::type_id<float>, "c"},
		};
		
		REQUIRE(buffer.type().total_size() == 24);
	
		uniforms.emplace("a", 10);
		uniforms.emplace("b", 2.5);
		uniforms.emplace("c", 1.5f);
		
		for (int i = 0 ; i < model_to_visit ; ++i) {
			buffer.grow(1);
			sbg::write_object_uniforms(buffer[i], uniforms);
		}
		
		REQUIRE(buffer.size() == model_to_visit);
		REQUIRE(buffer.capacity() >= buffer.size());
		
		SECTION("Can visit inserted objects") {
			auto amount_visited = std::size_t{};
			
			for (auto i = std::size_t{0} ; i < buffer.size() ; ++i) {
				sbg::read_object_uniforms(buffer[i], [&](unsigned int id, auto data) {
					++amount_visited;
					
					REQUIRE((
						(id == 0 && std::is_same_v<decltype(data), int>) ||
						(id == 1 && std::is_same_v<decltype(data), float>) ||
						(id == 2 && std::is_same_v<decltype(data), double>)
					));
					
					if constexpr (std::is_same_v<decltype(data), int>) {
						REQUIRE(data == 10);
					} else if constexpr (std::is_same_v<decltype(data), double>) {
						REQUIRE(data == 2.5);
					} else if constexpr (std::is_same_v<decltype(data), float>) {
						REQUIRE(data == 1.5f);
					}
				});
			}
			
			REQUIRE(amount_visited == model_to_visit * 3);
		}
		
		SECTION("Can retreive the uniform names") {
			for (auto const& object : buffer) {
				auto const type = buffer.type();
				sbg::read_object_uniforms(object, [&](unsigned int id, auto data) {
					if (id == 0) {
						REQUIRE(type.name_of(id) == "a");
					} else if (id == 1) {
						REQUIRE(type.name_of(id) == "c");
					} else if (id == 2) {
						REQUIRE(type.name_of(id) == "b");
					}
				});
			}
		}
		
		SECTION("Can retreive the buffer and give expected value") {
			auto const expected = std::array{
				0x00_b, 0x00_b, 0x00_b, 0x0a_b, // int: 10
				0xde_b, 0xad_b, 0xbe_b, 0xef_b, // deadbeef
				0x40_b, 0x04_b, 0x00_b, 0x00_b, // double: 2.5
				0x00_b, 0x00_b, 0x00_b, 0x00_b, //
				0x3f_b, 0xC0_b, 0x00_b, 0x00_b, // float: 1.5f
				0xde_b, 0xad_b, 0xbe_b, 0xef_b, // deadbeef
			};
			
			auto const data = buffer.buffer().data();
			
			for (int i = 0 ; i < model_to_visit ; ++i) {
				auto const start = i * expected.size();
				
				REQUIRE(std::memcmp(data + start, expected.data(), 4));
				REQUIRE(std::memcmp(data + start + 8, expected.data() + 8, 8));
				REQUIRE(std::memcmp(data + start + 16, expected.data() + 16, 4));
			}
		}
	}
		
	SECTION("Can push updating model") {
		struct Model { int a = 42; double b = 23.25; sbg::Vector2f c = {1, 2}; } model;
		auto encoder = sbg::ModelUniformEncoder{};
		auto uniforms = sbg::UniformSet{};
		
		uniforms.emplace("a"_h, sbg::UniformData{&model.a});
		uniforms.emplace("b"_h, sbg::UniformData{model.b});
		uniforms.emplace("c"_h, sbg::UniformData{&model.c});
		
		buffer = sbg::DynStructBuffer{
			sbg::DynStructBuffer::Member{0, sbg::type_id<int>, "a"},
			sbg::DynStructBuffer::Member{2, sbg::type_id<double>, "b"},
			sbg::DynStructBuffer::Member{1, sbg::type_id<sbg::Vector2f>, "c"},
		};
		
		buffer.grow(1);
		sbg::write_object_uniforms(buffer[0], uniforms);
		
		for (auto const& object : buffer) {
			sbg::read_object_uniforms(object, [&](unsigned int id, auto data) {
				REQUIRE((
					(id == 0 && std::is_same_v<decltype(data), int>) ||
					(id == 2 && std::is_same_v<decltype(data), double>) ||
					(id == 1 && std::is_same_v<decltype(data), sbg::Vector2f>)
				));
				
				if constexpr (std::is_same_v<decltype(data), int>) {
					REQUIRE(data == 42);
				} else if constexpr (std::is_same_v<decltype(data), double>) {
					REQUIRE(data == 23.25);
				} else if constexpr (std::is_same_v<decltype(data), sbg::Vector2f>) {
					REQUIRE(data == sbg::Vector2f{1, 2});
				}
			});
		}
		
		model.a = 12;
		model.b = 15.5;
		model.c = {4.5f, 2.5f};
		
		sbg::write_object_uniforms(buffer[0], uniforms);
		
		for (auto const& object : buffer) {
			sbg::read_object_uniforms(object, [&](unsigned int id, auto data) {
				REQUIRE((
					(id == 0 && std::is_same_v<decltype(data), int>) ||
					(id == 2 && std::is_same_v<decltype(data), double>) ||
					(id == 1 && std::is_same_v<decltype(data), sbg::Vector2f>)
				));
				
				if constexpr (std::is_same_v<decltype(data), int>) {
					REQUIRE(data == 12);
				} else if constexpr (std::is_same_v<decltype(data), double>) {
					REQUIRE(data == 23.25);
				} else if constexpr (std::is_same_v<decltype(data), sbg::Vector2f>) {
					REQUIRE(data == sbg::Vector2f{4.5f, 2.5f});
				}
			});
		}
	}
}
