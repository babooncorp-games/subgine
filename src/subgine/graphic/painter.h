#pragma once

#include "environmentuniforms.h"
#include "uniform.h"
#include "computeduniforms.h"

#include "subgine/common/erasedtype.h"
#include "subgine/common/traits.h"
#include "subgine/common/concepts.h"

#include <memory>
#include <concepts>

namespace sbg {

struct PainterRef;

template<typename T>
concept painter =
	    object<T>
	and std::move_constructible<T>
	and requires(T& painter, ModelInfo info, UniformSet const& uniforms) {
		{ painter.add(ModelInfo{info}, uniforms) } -> std::same_as<void>;
		{ std::as_const(painter).draw() } -> std::same_as<void>;
		{ painter.prepare(uniforms) } -> std::same_as<void>;
		{ painter.remove(std::as_const(info)) } -> std::same_as<void>;
		{ painter.update(ModelInfo{info}, uniforms) } -> std::same_as<void>;
		{ painter.refresh(uniforms) } -> std::same_as<void>;
	};

struct Painter {
	template<painter T>
	explicit Painter(T painter, ComputedUniforms computedUniforms) noexcept :
		_painter{std::make_unique<ConcretePainter<T>>(std::move(painter))},
		_computedUniforms{std::move(computedUniforms)} {}
	
	void add(ModelInfo model, UniformSet const& environment);
	void draw() const;
	void prepare(UniformSet const& context);
	void update(ModelInfo model, UniformSet const& environment);
	void refresh(UniformSet const& environment);
	void remove(ModelInfo const& model);
	auto uniforms_for(UniformSet const& model, UniformSet const& environment) -> ComputedUniformEncoder::uniforms_t;
	
private:
	friend PainterRef;
	struct AbstractPainter {
		virtual void add(ModelInfo model, UniformSet const& environment) = 0;
		virtual void draw() const = 0;
		virtual void prepare(UniformSet const& environment) = 0;
		virtual void update(ModelInfo model, UniformSet const& environment) = 0;
		virtual void refresh(UniformSet const& environment) = 0;
		virtual void remove(ModelInfo const& model) = 0;
		virtual ~AbstractPainter() = default;
	};
	
	template<painter T>
	struct ConcretePainter : AbstractPainter {
		explicit ConcretePainter(T const& painter) : _painter{painter} {}
		explicit ConcretePainter(T&& painter) noexcept : _painter{std::move(painter)} {}
		
		void add(ModelInfo model, UniformSet const& environment) override {
			_painter.add(std::move(model), environment);
		}
		
		void draw() const override {
			_painter.draw();
		}
		
		void prepare(UniformSet const& environment) override {
			_painter.prepare(environment);
		}
		
		void update(ModelInfo model, UniformSet const& environment) override {
			_painter.update(std::move(model), environment);
		}
		
		void refresh(UniformSet const& environment) override {
			_painter.refresh(environment);
		}
		
		void remove(ModelInfo const& model) override {
			_painter.remove(model);
		}
		
		~ConcretePainter() override = default;
		
	private:
		T _painter;
	};
	
	std::unique_ptr<AbstractPainter> _painter;
	ComputedUniforms _computedUniforms;
};

struct PainterRef {
	PainterRef(Painter const& painter) noexcept :
		_painter{painter._painter.get()} {}
	
	void add(ModelInfo model, UniformSet const& environment) {
		_painter->add(model, environment);
	}
	
	void draw() const {
		_painter->draw();
	}
	
	void prepare(UniformSet const& context) {
		_painter->prepare(context);
	}
	
	void update(ModelInfo model, UniformSet const& environment) {
		_painter->update(std::move(model), environment);
	}
	
	void refresh(UniformSet const& environment) {
		_painter->refresh(environment);
	}

	void remove(ModelInfo const& model) {
		_painter->remove(model);
	}
	
private:
	Painter::AbstractPainter* _painter;
};

} // namespace sbg
