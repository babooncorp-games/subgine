#pragma once

#include "uniform.h"
#include "subgine/entity/entity.h"

#include <stdexcept>
#include <span>
#include <unordered_map>
#include <vector>

namespace sbg {

struct ModelSlotMap {
	auto insert(ModelInfo model) -> std::pair<ModelInfo&, std::size_t>;
	void erase(ModelInfo const& model) noexcept;
	auto find(ModelHandle handle) const& noexcept -> std::size_t;
	
	[[nodiscard]]
	auto alive(std::size_t const index) const& noexcept -> bool;
	
	auto models() & noexcept -> std::span<ModelInfo>;
	auto models() const& noexcept -> std::span<ModelInfo const>;
	
	[[nodiscard]]
	auto empty() const& noexcept -> bool;
	
	void cleanup() & noexcept;
	
	auto size() -> std::size_t;
	
	auto operator[](std::size_t const index) const& noexcept -> ModelInfo const&;
	auto operator[](std::size_t const index) & noexcept -> ModelInfo&;
	
private:
	auto insert_impl(ModelInfo&& model) -> ModelInfo&;
	void remove_impl(ModelInfo const& model) noexcept;
	
	std::vector<ModelInfo> _models;
	std::unordered_map<ModelHandle, std::size_t> _entities;
	std::optional<std::uint32_t> _free;
	std::vector<bool> _dead;
};

} // namespace sbg
