#include "modelslotmap.h"

using namespace sbg;

auto ModelSlotMap::insert(sbg::ModelInfo model) -> std::pair<ModelInfo&, std::size_t> {
	auto it = _entities.find(model.handle);
	
	if (it != _entities.end()) {
		_entities.erase(it);
	}
	
	auto const index = _free.value_or(_models.size());
	_entities.insert_or_assign(model.handle, index);
	return {insert_impl(std::move(model)), index};
}

void ModelSlotMap::erase(ModelInfo const& model) noexcept {
	remove_impl(model);
}

auto ModelSlotMap::find(sbg::ModelHandle handle) const& noexcept -> std::size_t {
	if (auto const it = _entities.find(handle); it != _entities.end() && !_dead[it->second]) {
		return it->second;
	} else {
		return _models.size();
	}
}

auto ModelSlotMap::alive(std::size_t const index) const& noexcept -> bool {
	return !_dead[index];
}

auto ModelSlotMap::empty() const& noexcept -> bool {
	return _models.empty();
}

auto ModelSlotMap::models() & noexcept -> std::span<ModelInfo> {
	return _models;
}

auto ModelSlotMap::models() const& noexcept -> std::span<ModelInfo const> {
	return _models;
}

void ModelSlotMap::cleanup() & noexcept {
	auto index = std::uint32_t{};
	for (auto& model : _models) {
		if (!_dead[index] && !model.entity) {
			model.entity = Entity{nullptr, Entity::Id{Entity::Id::Limits::generation, _free.value_or(Entity::Id::Limits::index)}};
			_free = index;
			_dead[index] = true;
		}
		
		++index;
	}
}

auto ModelSlotMap::size() -> std::size_t {
	return _models.size();
}

auto ModelSlotMap::operator[](std::size_t const index) const& noexcept -> ModelInfo const& {
	return _models[index];
}

auto ModelSlotMap::operator[](std::size_t const index) & noexcept -> ModelInfo& {
	return _models[index];
}

auto ModelSlotMap::insert_impl(ModelInfo&& model) -> ModelInfo& {
	if (!_free) {
		_dead.push_back(false);
		return _models.emplace_back(std::move(model));
	} else {
		auto const index = *_free;
		auto const next = _models[index].entity.id().index;
		
		if (next != Entity::Id::Limits::index) {
			_free = next;
		} else {
			_free.reset();
		}
		
		_dead[index] = false;
		return _models[index] = std::move(model);
	}
}

void ModelSlotMap::remove_impl(ModelInfo const& model) noexcept {
	auto const found = _entities.find(model.handle);
	
	if (found != _entities.end() && !_dead[found->second]) {
		auto const index = found->second;
		_entities.erase(found);
		_models[index].entity = Entity{nullptr, Entity::Id{Entity::Id::Limits::generation, _free ? *_free : Entity::Id::Limits::index}};
		_free = static_cast<std::uint32_t>(index);
		_dead[index] = true;
	}
}
