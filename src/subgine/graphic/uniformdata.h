#pragma once

#include "detail/updatinguniformdata.h"

#include "subgine/resource/resourcetag.h"
#include "subgine/vector/vector.h"

#include <glm/glm.hpp>

#include <any>
#include <variant>
#include <cstdint>

namespace sbg {

using GenericSerializedData = std::variant<
	bool,
	float,
	double,
	std::int32_t,
	std::uint32_t,
	std::uint64_t,
	Vector2f,
	Vector3f,
	Vector4f,
	Vector2i,
	Vector3i,
	Vector4i,
	glm::mat2,
	glm::mat3,
	glm::mat4
>;

struct DynStructBuffer;

using UniformData = std::variant<
	bool,
	float,
	double,
	std::int32_t,
	std::uint32_t,
	std::uint64_t,
	Vector2f,
	Vector3f,
	Vector4f,
	Vector2i,
	Vector3i,
	Vector4i,
	glm::mat2,
	glm::mat3,
	glm::mat4,
	GenericResource,
	DynStructBuffer,
	bool const*,
	float const*,
	double const*,
	std::int32_t const*,
	std::uint32_t const*,
	std::uint64_t const*,
	Vector2f const*,
	Vector3f const*,
	Vector4f const*,
	Vector2i const*,
	Vector3i const*,
	Vector4i const*,
	glm::mat2 const*,
	glm::mat3 const*,
	glm::mat4 const*,
	std::any const*,
	std::any
>;

using ModelUniformData = std::variant<
	UniformData,
	TaggedModelUpdatingFunction,
	TaggedComputedUpdatingFunction
>;

[[noreturn]]
void type_bad_visit(type_id_t type) noexcept;

static constexpr auto visit_type_generic_buffer_data(type_id_t type, auto function) noexcept -> decltype(auto) {
	if (type == type_id<bool>) {
		return function.template operator()<bool>();
	} else if (type == type_id<float>) {
		return function.template operator()<float>();
	} else if (type == type_id<double>) {
		return function.template operator()<double>();
	} else if (type == type_id<std::int32_t>) {
		return function.template operator()<std::int32_t>();
	} else if (type == type_id<std::uint32_t>) {
		return function.template operator()<std::uint32_t>();
	} else if (type == type_id<std::uint64_t>) {
		return function.template operator()<std::uint64_t>();
	} else if (type == type_id<Vector2f>) {
		return function.template operator()<Vector2f>();
	} else if (type == type_id<Vector3f>) {
		return function.template operator()<Vector3f>();
	} else if (type == type_id<Vector4f>) {
		return function.template operator()<Vector4f>();
	} else if (type == type_id<Vector2i>) {
		return function.template operator()<Vector2i>();
	} else if (type == type_id<Vector3i>) {
		return function.template operator()<Vector3i>();
	} else if (type == type_id<Vector4i>) {
		return function.template operator()<Vector4i>();
	} else if (type == type_id<glm::mat2>) {
		return function.template operator()<glm::mat2>();
	} else if (type == type_id<glm::mat3>) {
		return function.template operator()<glm::mat3>();
	} else if (type == type_id<glm::mat4>) {
		return function.template operator()<glm::mat4>();
	}
	
	type_bad_visit(type);
}

static constexpr auto visit_type_uniform_data(type_id_t type, auto function) noexcept -> decltype(auto) {
	if (type == type_id<bool>) {
		return function.template operator()<bool>();
	} else if (type == type_id<float>) {
		return function.template operator()<float>();
	} else if (type == type_id<double>) {
		return function.template operator()<double>();
	} else if (type == type_id<std::int32_t>) {
		return function.template operator()<std::int32_t>();
	} else if (type == type_id<std::uint32_t>) {
		return function.template operator()<std::uint32_t>();
	} else if (type == type_id<std::uint64_t>) {
		return function.template operator()<std::uint64_t>();
	} else if (type == type_id<Vector2f>) {
		return function.template operator()<Vector2f>();
	} else if (type == type_id<Vector3f>) {
		return function.template operator()<Vector3f>();
	} else if (type == type_id<Vector4f>) {
		return function.template operator()<Vector4f>();
	} else if (type == type_id<Vector2i>) {
		return function.template operator()<Vector2i>();
	} else if (type == type_id<Vector3i>) {
		return function.template operator()<Vector3i>();
	} else if (type == type_id<Vector4i>) {
		return function.template operator()<Vector4i>();
	} else if (type == type_id<glm::mat2>) {
		return function.template operator()<glm::mat2>();
	} else if (type == type_id<glm::mat3>) {
		return function.template operator()<glm::mat3>();
	} else if (type == type_id<glm::mat4>) {
		return function.template operator()<glm::mat4>();
	} else if (type == type_id<std::any>) {
		return function.template operator()<std::any>();
	} else if (type == type_id<GenericResource>) {
		return function.template operator()<GenericResource>();
	} else if (type == type_id<DynStructBuffer>) {
		return function.template operator()<DynStructBuffer>();
	} else if (type == type_id<bool const*>) {
		return function.template operator()<bool const*>();
	} else if (type == type_id<float const*>) {
		return function.template operator()<float const*>();
	} else if (type == type_id<double const*>) {
		return function.template operator()<double const*>();
	} else if (type == type_id<std::int32_t const*>) {
		return function.template operator()<std::int32_t const*>();
	} else if (type == type_id<std::uint32_t const*>) {
		return function.template operator()<std::uint32_t const*>();
	} else if (type == type_id<std::uint64_t const*>) {
		return function.template operator()<std::uint64_t const*>();
	} else if (type == type_id<Vector2f const*>) {
		return function.template operator()<Vector2f const*>();
	} else if (type == type_id<Vector3f const*>) {
		return function.template operator()<Vector3f const*>();
	} else if (type == type_id<Vector4f const*>) {
		return function.template operator()<Vector4f const*>();
	} else if (type == type_id<Vector2i const*>) {
		return function.template operator()<Vector2i const*>();
	} else if (type == type_id<Vector3i const*>) {
		return function.template operator()<Vector3i const*>();
	} else if (type == type_id<Vector4i const*>) {
		return function.template operator()<Vector4i const*>();
	} else if (type == type_id<glm::mat2 const*>) {
		return function.template operator()<glm::mat2 const*>();
	} else if (type == type_id<glm::mat3 const*>) {
		return function.template operator()<glm::mat3 const*>();
	} else if (type == type_id<glm::mat4 const*>) {
		return function.template operator()<glm::mat4 const*>();
	} else if (type == type_id<std::any const*>) {
		return function.template operator()<std::any const*>();
	}
	
	type_bad_visit(type);
}

static constexpr auto visit_type_updating_data(type_id_t type, auto function) noexcept -> decltype(auto) {
	if (type == type_id<bool>) {
		return function.template operator()<bool>();
	} else if (type == type_id<float>) {
		return function.template operator()<float>();
	} else if (type == type_id<double>) {
		return function.template operator()<double>();
	} else if (type == type_id<std::int32_t>) {
		return function.template operator()<std::int32_t>();
	} else if (type == type_id<std::uint32_t>) {
		return function.template operator()<std::uint32_t>();
	} else if (type == type_id<std::uint64_t>) {
		return function.template operator()<std::uint64_t>();
	} else if (type == type_id<Vector2f>) {
		return function.template operator()<Vector2f>();
	} else if (type == type_id<Vector3f>) {
		return function.template operator()<Vector3f>();
	} else if (type == type_id<Vector4f>) {
		return function.template operator()<Vector4f>();
	} else if (type == type_id<Vector2i>) {
		return function.template operator()<Vector2i>();
	} else if (type == type_id<Vector3i>) {
		return function.template operator()<Vector3i>();
	} else if (type == type_id<Vector4i>) {
		return function.template operator()<Vector4i>();
	} else if (type == type_id<glm::mat2>) {
		return function.template operator()<glm::mat2>();
	} else if (type == type_id<glm::mat3>) {
		return function.template operator()<glm::mat3>();
	} else if (type == type_id<glm::mat4>) {
		return function.template operator()<glm::mat4>();
	} else if (type == type_id<std::any>) {
		return function.template operator()<std::any>();
	}
	
	type_bad_visit(type);
}

} // namespace sbg
