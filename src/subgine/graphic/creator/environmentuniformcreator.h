#pragma once

#include "../computeduniforms.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/resource/property.h"

namespace sbg {

struct EnvironmentUniformCreatorService;

struct EnvironmentUniformCreator : Creator<SimpleBuilder<std::function<void(EnvironmentUniformEncoder&)>>> {
	using Creator::create;
	
	friend auto service_map(EnvironmentUniformCreator const&) -> EnvironmentUniformCreatorService;
};

using EnvironmentUniformFactory = Factory<EnvironmentUniformCreator>;

} // namespace sbg
