#include "paintercreator.h"

using namespace sbg;

PainterCreator::PainterCreator(ComputedUniformCreator const& computedUniformCreator) noexcept : Creator{PainterApply{computedUniformCreator}} {}
