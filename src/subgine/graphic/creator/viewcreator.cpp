#include "viewcreator.h"

#include "../service/viewservice.h"
#include "paintercreator.h"
#include "environmentuniformcreator.h"
#include "computeduniformcreator.h"
#include "subgine/log.h"

#include "subgine/entity/service/entitymanagerservice.h"

namespace sbg {

ViewCreator::ViewCreator(PainterCreator const& painterCreator, EnvironmentUniformCreator& environmentUniformCreator) noexcept :
	_painterCreator{&painterCreator}, _environmentUniformCreator{&environmentUniformCreator} {}

auto ViewCreator::create(kgr::invoker invoker, std::string_view name, Property data) const -> View& {
	auto& c = invoker([](kgr::container& c) -> auto& { return c; });
	
	auto pipeline = build(invoker, name, data);
	
	EnvironmentUniforms environment;
	
	for (auto const uniform : data["environment"]) {
		environment.uniform_source(_environmentUniformCreator->create(invoker, uniform["type"], uniform["data"]));
	}
	
	c.emplace<ViewService>(View{c.service<EntityManagerService>(), std::move(pipeline), std::move(environment)});
	
	auto& view = c.service<ViewService>();
	
	for (auto const painter : data["painters"]) {
		auto const step = painter["step"];
		auto const steps = step.isNull()
			? std::vector<std::string>{painter["steps"]}
			: std::vector<std::string>{std::string{step}};
		
		auto const name = std::string{painter["name"]};
		auto const type = std::string_view{painter["type"]};
		auto l = Log::trace(SBG_LOG_INFO, "Creating painter", log::enquote(name), "with type", log::enquote(type));
		
		view.addPainter(
			std::move(name),
			_painterCreator->create(invoker, type, painter["data"]),
			steps
		);
	}
	
	return view;
}

} // namespace sbg
