#pragma once

#include "../computeduniforms.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/resource/property.h"

namespace sbg {

struct ComputedUniformCreatorService;

struct ComputedUniformCreator : Creator<auto(sbg::Property) -> ComputedUniforms::UniformMaker> {
	using Creator::create;
	
	friend auto service_map(ComputedUniformCreator const&) -> ComputedUniformCreatorService;
};

using ComputedUniformFactory = Factory<ComputedUniformCreator>;

} // namespace sbg
