#pragma once

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

#include "../view.h"

namespace sbg {

struct ViewCreatorService;

struct PainterCreator;
struct EnvironmentUniformCreator;
struct ComputedUniformCreator;

struct ViewCreator : Creator<View::Pipeline(Property)> {
	explicit ViewCreator(PainterCreator const& painterCreator, EnvironmentUniformCreator& environmentUniformCreator) noexcept;
	
	auto create(kgr::invoker invoker, std::string_view name, Property data) const -> View&;
	
private:
	ComputedUniformCreator* _computedUniformCreator;
	PainterCreator const* _painterCreator;
	EnvironmentUniformCreator* _environmentUniformCreator;
	
	friend auto service_map(ViewCreator const&) -> ViewCreatorService;
};

using ViewFactory = Factory<ViewCreator>;

} // namespace sbg
