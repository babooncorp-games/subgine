#pragma once

#include "../painter.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

#include "../detail/painterapply.h"
#include "../creator/computeduniformcreator.h"

#include <variant>

namespace sbg {

struct PainterCreatorService;

struct PainterCreator : Creator<SimpleBuilder<Painter>, PainterApply> {
	explicit PainterCreator(ComputedUniformCreator const& computedUniformCreator) noexcept;
	using Creator::create;
	
	friend auto service_map(PainterCreator const&) -> PainterCreatorService;
};

using PainterFactory = Factory<PainterCreator>;

} // namespace sbg
