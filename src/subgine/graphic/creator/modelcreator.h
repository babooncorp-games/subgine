#pragma once

#include "../detail/modelapply.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

#include <string_view>

namespace sbg {

struct ModelCreatorService;

struct ModelCreator : Creator<void(std::string_view, Visual&, Entity, Property), ModelApply> {
	using Creator::create;
	
	friend auto service_map(ModelCreator const&) -> ModelCreatorService;
};

using ModelFactory = Factory<ModelCreator>;

} // namespace sbg
