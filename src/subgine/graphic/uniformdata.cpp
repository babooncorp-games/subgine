#include "uniformdata.h"

#include "subgine/log.h"
#include "subgine/log/logdecoration.h"

namespace sbg {

void type_bad_visit(type_id_t type) noexcept {
	Log::fatal(SBG_LOG_INFO, "Unhandled type for uniform data visitation", log::enquote(static_cast<std::size_t>(type)));
}

} // namespace sbg
