#pragma once

#include "uniform.h"

#include <functional>
#include <algorithm>

namespace sbg {

template<typename Sort, typename... Ts>
struct SortedModelVector {
	template<typename T>
	using container = std::vector<T>;

	template<typename... Us>
	using containers = std::tuple<container<Us>...>;
	
	[[nodiscard]]
	auto alive(std::size_t const index) const& noexcept -> bool {
		return _models[index].entity.alive();
	}
	
	template<typename... Args>
	auto insert(ModelInfo model, Sort const& sort, Args&&... data) -> std::tuple<ModelInfo&, Ts&...> {
		auto position = std::upper_bound(
			_sort_data.begin(),
			_sort_data.end(),
			sort,
			std::less{}
		);
		
		auto index = std::distance(_sort_data.begin(), position);
		
		_models.insert(_models.begin() + index, model);
		_sort_data.insert(_sort_data.begin() + index, std::move(sort));
		(std::get<container<Ts>>(_data).insert(std::get<container<Ts>>(_data).begin() + index, std::forward<Args>(data)), ...);
		
		return std::tuple<ModelInfo&, Ts&...>{
			_models[index],
			std::get<container<Ts>>(_data)[index]...
		};
	}
	
	void cleanup() {
		auto first_model = std::find_if(
			_models.begin(), _models.end(),
			[](ModelInfo const& model) {
				return !model.entity;
			}
		);
		
		if (first_model != _models.end()) {
			auto const index_first = std::distance(_models.begin(), first_model);
			auto first_data = _sort_data.begin() + index_first;
			auto data_it = std::tuple{(std::get<container<Ts>>(_data).begin() + index_first)...};
			
			for (auto it = first_model ; it != _models.end() ; ++it) {
				if (it->entity) {
					auto const distance = std::distance(_models.begin(), it);
					((*std::get<typename container<Ts>::iterator>(data_it)++ = std::move(*(std::get<container<Ts>>(_data).begin() + distance))), ...);
					*first_data++ = std::move(*(_sort_data.begin() + distance));
					*first_model++ = std::move(*it);
				}
			}
			
			_models.erase(first_model, _models.end());
			_sort_data.erase(first_data, _sort_data.end());
			(std::get<container<Ts>>(_data).erase(std::get<typename container<Ts>::iterator>(data_it)), ...);
		}
	}
	
	void erase(ModelInfo const& model) noexcept {
		auto const it = std::find_if(_models.begin(), _models.end(), [&model](ModelInfo const& m) {
			return m.handle == model.handle;
		});
		
		if (it != _models.end()) {
			auto const distance = std::distance(_models.begin(), it);
			_sort_data.erase(_sort_data.begin() + std::distance(_models.begin(), it));
			_models.erase(it);
			(std::get<container<Ts>>(_data).erase(std::get<container<Ts>>(_data).begin() + distance), ...);
		}
	}
	
	auto find(ModelHandle handle) const& noexcept -> std::size_t {
		return std::distance(
			_models.begin(),
			std::find_if(
				_models.begin(), _models.end(),
				[&handle](ModelInfo const& model) {
					return model.handle == handle; 
				}
			)
		);
	}
	
	auto models() & noexcept -> std::span<ModelInfo> {
		return _models;
	}
	
	auto models() const& noexcept -> std::span<ModelInfo const> {
		return _models;
	}
	
	[[nodiscard]]
	auto empty() const& noexcept -> bool {
		return _models.empty();
	}
	
	auto size() -> std::size_t {
		return _models.size();
	}

	template<std::size_t n>
	auto data() & noexcept {
		return std::span{std::get<n>(_data)};
	}

	template<typename T>
	auto data() & noexcept {
		return std::span<T>{std::get<container<T>>(_data)};
	}
	
	auto datas() const& -> std::tuple<std::span<Ts const>...> {
		return std::tuple{std::span<Ts const>{std::get<container<Ts>>(_data)}...};
	}
	
	auto datas() & -> std::tuple<std::span<Ts>...> {
		return std::tuple{std::span<Ts>{std::get<container<Ts>>(_data)}...};
	}

	template<std::size_t n>
	auto data() const& noexcept {
		return std::span<typename std::tuple_element_t<n, containers<Ts...>>::value_type const>{std::get<n>(_data)};
	}

	template<typename U>
	auto data() const& noexcept {
		return std::span<U const>(std::get<std::vector<U>>(_data));
	}
	
	auto operator[](std::size_t const index) const& noexcept -> ModelInfo const& {
		return _models[index];
	}
	
	auto operator[](std::size_t const index) & noexcept -> ModelInfo& {
		return _models[index];
	}
	
private:
	std::vector<ModelInfo> _models;
	std::vector<Sort> _sort_data;
	containers<Ts...> _data;
};

} // namespace sbg
