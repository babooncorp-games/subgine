#pragma once

#include "subgine/resource/resourcetag.h"

namespace sbg {
	using TextureTag = ResourceTag<struct TextureTagType>;
}
