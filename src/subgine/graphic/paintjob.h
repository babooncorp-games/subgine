#pragma once

#include "subgine/common/type_id.h"
#include "subgine/common/pairhash.h"
#include "uniform.h"
#include "painter.h"

#include <string_view>
#include <vector>

namespace sbg {

struct PainterModelSystem;

/**
 * @brief Calls the draw function for each painters of the specified step in the view.
 */
struct PaintJob {
	explicit PaintJob(EntityManager const& entityManager, PainterModelSystem& renderSystem) noexcept :
		_entityManager{&entityManager}, _renderSystem{&renderSystem} {}
	
	/**
	 * @brief Calls the draw function for each painters of the specified step in the view.
	 * @param step The drawing step.
	 */
	void draw(std::string_view step) const;
	void prepare(std::string_view step, UniformSet const& context) const;
	
private:
	PainterModelSystem* _renderSystem;
	EntityManager const* _entityManager;
};

} // namespace sbg
