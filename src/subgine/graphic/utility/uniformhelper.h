#include "../uniform.h"
#include "../dynstructbuffer.h"

namespace sbg {

void read_object_uniforms(DynStructBuffer::ConstObject object, auto function) noexcept {
	for (auto const& member : object.type().members) {
		visit_type_generic_buffer_data(member.type, [&]<typename M>() noexcept {
			function(member.id, *std::launder(static_cast<M const*>(static_cast<void const*>(object.raw_access(member)))));
		});
	}
}

[[noreturn]]
void fatal_failed_assign_uniform(DynStructBuffer::MemberData const& member) noexcept;

void write_object_uniforms(DynStructBuffer::Object object, std::invocable<GenericResource&> auto convert_resource, std::same_as<UniformSet> auto const&... sets) {
	write_object_uniforms(object, convert_resource, std::array<UniformSet const*, sizeof...(sets)>{&sets...});
}

void write_object_uniforms(DynStructBuffer::Object object, std::same_as<UniformSet> auto const&... sets) {
	write_object_uniforms(object, [](auto const& r) -> auto const& { return r; }, std::array<UniformSet const*, sizeof...(sets)>{&sets...});
}

void write_object_uniforms(DynStructBuffer::Object object, std::invocable<GenericResource&> auto convert_resource, std::span<UniformSet const* const> sets) noexcept {
	for (auto const& member : object.type().members) {
		auto const visited = std::any_of(sets.begin(), sets.end(), [&](UniformSet const* uniforms) noexcept {
			if (auto const it = uniforms->find(member.name); it != uniforms->uniforms.end()) {
				auto const& [hash, uniform] = *it;
				
				auto const assign_uniform = [&](UniformData const& uniform) {
					if (std::get_if<DynStructBuffer>(&uniform) or std::get_if<std::any>(&uniform) or std::get_if<std::any const*>(&uniform)) {
						return false;
					} else if (std::get_if<GenericResource>(&uniform)) {
						return true;
					} else {
						return visit_type_generic_buffer_data(member.type, [&]<typename M>() noexcept {
							if (auto const* value = std::get_if<M>(&uniform)) {
								new (object.raw_access(member)) M{*value};
								return true;
							} else if (auto const* value = std::get_if<M const*>(&uniform)) {
								new (object.raw_access(member)) M{**value};
								return true;
							} else {
								return false;
							}
						});
					}
				};
				
				if (auto const* resource = std::get_if<GenericResource>(&uniform)) {
					return assign_uniform(convert_resource(*resource));
				} else {
					return assign_uniform(uniform);
				}
			} else {
				return false;
			}
		});

		if (not visited) {
			fatal_failed_assign_uniform(member);
		}
	}
}

} // namespace sbg
