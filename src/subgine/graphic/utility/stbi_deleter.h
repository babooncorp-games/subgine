#pragma once

namespace sbg {

struct stbi_deleter {
	void operator()(void* data) const;
};

} // namespace sbg
