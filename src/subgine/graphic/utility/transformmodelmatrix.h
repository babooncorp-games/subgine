#pragma once

#include "subgine/graphic/component/transform.h"
#include "subgine/entity/component.h"
#include "subgine/common/concepts.h"

#include <glm/glm.hpp>

namespace sbg {

template<typename T>
struct TransformModelMatrix {
	auto operator()(T const& model) const noexcept -> glm::mat4 {
		if constexpr (requires{ { model.parent } -> similar_to<glm::mat4 const*>; }) {
			if (model.parent) {
				return
					  *model.parent
					* model.transform->matrix()
					* model.origin.matrix();
			} else {
				return model.transform->matrix() * model.origin.matrix();
			}
		} else if constexpr (requires{ { model.origin } -> similar_to<sbg::Transform>; }) {
			return model.transform->matrix() * model.origin.matrix();
		} else {
			return model.transform->matrix();
		}
	}
};

} // namespace sbg
