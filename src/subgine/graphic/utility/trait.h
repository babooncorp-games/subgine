#pragma once

#include "subgine/common/function_traits.h"

namespace sbg {

template<typename T>
using model_visitor_t = std::decay_t<function_argument_t<0, T>>;

} // namespace sbg


