#include "uniformhelper.h"

#include "subgine/log.h"

namespace sbg {
	void fatal_failed_assign_uniform(DynStructBuffer::MemberData const& member) noexcept {
		Log::fatal(SBG_LOG_INFO, "Failed to assign uniform with id", log::enquote(member.id), "and type", static_cast<std::size_t>(member.name));
	}
}
