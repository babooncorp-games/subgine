#include "stbi_deleter.h"

#include "stb/stb_image.h"

using namespace sbg;

void stbi_deleter::operator()(void* data) const {
	stbi_image_free(data);
}
