#pragma once

#include "subgine/graphic/dynstructbuffer.h"
#include "subgine/graphic/uniform.h"
#include "subgine/resource/resourcehandle.h"

#include <concepts>
#include <span>
#include <vector>
#include <optional>
#include <compare>
#include <ranges>

namespace sbg {
namespace detail {
	struct ResourcesInTypeSentinel {};
	
	template<resource_tag_concrete T, range_of<UniformSet const*> S>
	struct ResourcesInTypeIterator {
		ResourcesInTypeIterator() = default;
		
		ResourcesInTypeIterator(
			std::vector<DynStructBuffer::MemberData>::const_iterator it,
			std::vector<DynStructBuffer::MemberData>::const_iterator end,
			S const& sets
		) noexcept : _it{it}, _end{end}, _sets{sets} { find_next_iterator(); }
		
		using iterator_category = std::forward_iterator_tag;
		using value_type = T;
		using reference_type = value_type&;
		using const_reference = value_type const&;
		using pointer = value_type*;
		using difference_type = std::ptrdiff_t;
		
		friend auto operator==(ResourcesInTypeIterator const& self, ResourcesInTypeSentinel const&) noexcept -> bool {
			return self._it == self._end;
		}
		
		// TODO: To remove when GCC has better C++20 support
		friend auto operator!=(ResourcesInTypeIterator const& self, ResourcesInTypeSentinel const&) noexcept -> bool {
			return self._it != self._end;
		}
		
		friend auto operator==(ResourcesInTypeIterator const& lhs, ResourcesInTypeIterator const& rhs) noexcept -> bool {
			return lhs._it == rhs._it;
		}
		
		friend auto operator<=>(ResourcesInTypeIterator const& lhs, ResourcesInTypeIterator const& rhs) noexcept -> std::partial_ordering {
			return lhs._it <=> rhs._it;
		}
		
		auto operator++() & noexcept -> ResourcesInTypeIterator& {
			++_it;
			find_next_iterator();
			return *this;
		}
		
		auto operator++(int) & noexcept -> ResourcesInTypeIterator {
			auto prev = this;
			++(*this);
			return prev;
		}
		
		auto operator*() const& noexcept -> T const& {
			return _resource;
		}
		
	private:
		void find_next_iterator() {
			while(_it != _end) {
				if (auto const resource = find_resource(*_it)) {
					_resource = *resource;
					break;
				}
				++_it;
			}
		}
		
		auto find_resource(DynStructBuffer::MemberData const& member) noexcept -> std::optional<T> {
			for (UniformSet const* uniforms : _sets) {
				if (auto const resource = uniforms->optional<T>(member.name)) {
					return resource;
				}
			}
			
			return {};
		}
		
		T _resource;
		std::vector<DynStructBuffer::MemberData>::const_iterator _it;
		std::vector<DynStructBuffer::MemberData>::const_iterator _end;
		S _sets;
	};
	
	template<resource_tag_concrete T, range_of<UniformSet const*> S>
	struct ResourcesInTypeRange : std::ranges::view_interface<ResourcesInTypeRange<T, S>> {
		ResourcesInTypeRange() = default;
		ResourcesInTypeRange(ResourcesInTypeIterator<T, S> begin) noexcept :
			_begin{begin} {}
		
		auto begin() const& noexcept -> ResourcesInTypeIterator<T, S> {
			return _begin;
		}
		
		auto end() const& noexcept -> ResourcesInTypeSentinel {
			return ResourcesInTypeSentinel{};
		}
		
	private:
		ResourcesInTypeIterator<T, S> _begin;
	};
} // namespace detail

template<resource_tag_concrete T>
auto resources_in_object(DynStructBuffer::Type const& type, std::same_as<UniformSet> auto const&... sets) {
	return resources_in_object<T>(type, std::array{std::addressof(sets)...});
}

template<resource_tag_concrete T, range_of<UniformSet const*> S>
auto resources_in_object(DynStructBuffer::Type const& type, S const& sets) {
	auto r = detail::ResourcesInTypeRange{
		detail::ResourcesInTypeIterator<T, S>{type.members.begin(), type.members.end(), sets}
	};
	
	return r;
}

namespace views {
	auto load_resources(auto const& manager) {
		return std::views::transform([manager](resource_tag_concrete auto tag) noexcept {
			return manager.get(tag);
		});
	}
} // namespace views
} // namespace sbg

template <sbg::resource_tag_concrete T, sbg::range_of<sbg::UniformSet const*> S>
inline constexpr bool std::ranges::enable_borrowed_range<sbg::detail::ResourcesInTypeRange<T, S>> = true;
