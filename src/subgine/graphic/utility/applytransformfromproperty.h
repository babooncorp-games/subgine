#pragma once

#include "../component/transform.h"

namespace sbg {

struct Property;

auto apply_transform_from_property(Property const& property, Transform transform = {}) -> Transform;

}
