#include "applytransformfromproperty.h"

#include "subgine/vector/vector.h"
#include "subgine/resource/utility/optionalvalue.h"
#include "subgine/resource/property.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace sbg {
	auto apply_transform_from_property(Property const& data, Transform transform) -> Transform {
		if (auto const scale = value_optional<Vector3f>(data["scale"])) {
			transform = transform.scale3d(*scale);
		} else if (auto const scale = value_optional<Vector2f>(data["scale"])) {
			transform = transform.scale2d(*scale);
		} else if (auto const scale = value_optional<float>(data["scale"])) {
			auto const scaleValue = *scale;
			transform = transform.scale3d(Vector3f{scaleValue});
		}
		
		if (auto const offset = value_optional<Vector3f>(data["offset"])) {
			transform = transform.position3d(*offset);
		} else if (auto const offset = value_optional<Vector2f>(data["offset"])) {
			transform = transform.position2d(*offset);
		}
		
		if (auto const rotation = value_optional<Vector3f>(data["rotation"])) {
			auto const euler = glm::vec3{rotation->x, rotation->y, rotation->z};
			transform = transform.orientation3d(glm::quat{euler});
		} else if (auto const rotation = value_optional<float>(data["rotation"])) {
			transform = transform.orientation2d(*rotation);
		}
		
		return transform;
	}
}
