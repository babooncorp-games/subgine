find_package(glm 0.9.9 EXACT REQUIRED)
find_package(stb REQUIRED)

add_library(graphic
	graphicmodule.cpp
	
	component/transform.cpp
	component/visual.cpp
	
	creator/computeduniformcreator.cpp
	creator/modelcreator.cpp
	creator/paintercreator.cpp
	creator/viewcreator.cpp
	
	detail/painterapply.cpp
	detail/modelapply.cpp
	detail/updatinguniformdata.cpp
	
	model/simplesprite.cpp
	
	utility/applytransformfromproperty.cpp
	utility/resourceuniformhelper.cpp
	utility/stbi_deleter.cpp
	utility/uniformhelper.cpp
	
	camera.cpp
	computeduniforms.cpp
	dynstructbuffer.cpp
	modelslotmap.cpp
	environmentuniforms.cpp
	model.cpp
	modelhandle.cpp
	painter.cpp
	paintjob.cpp
	sortedmodelvector.cpp
	uniform.cpp
	uniformdata.cpp
	view.cpp
	viewport.cpp
)

subgine_configure_target(graphic)

target_compile_definitions(graphic PUBLIC GLM_ENABLE_EXPERIMENTAL)

target_link_libraries(graphic
PUBLIC
	subgine::common
	subgine::entity
	subgine::resource
	subgine::provider
	subgine::log
	stb::image
	stb::image-write
	glm
	
PRIVATE
	subgine::physic
	subgine::vector
	subgine::scene
	subgine::entity
	subgine::window
	subgine::common-warnings
)

add_subdirectory(test)
