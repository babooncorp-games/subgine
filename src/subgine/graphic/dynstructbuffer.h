#pragma once

#include "uniformdata.h"

#include "subgine/common/type_id.h"
#include "subgine/common/concepts.h"
#include "subgine/vector/vector.h"
#include "subgine/common/hash.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include <glm/glm.hpp>
#include <stdexcept>
#include <span>

#include <vector>
#include <memory>
#include <cstddef>
#include <variant>
#include <optional>
#include <tuple>
#include <new>
#include <span>

#include "subgine/common/define.h"

namespace sbg {

struct DynStructBuffer {
	struct Member {
		std::uint32_t id;
		type_id_t type;
		std::string name;
		std::uint32_t alignment = 0;
	};
	
	struct MemberData {
		std::uint32_t id;
		std::uint32_t offset;
		type_id_t type;
		hash_t name;
		
		template<typename T>
		auto visit(T function) const& noexcept -> decltype(auto) {
			return visit_type_generic_buffer_data(type, function);
		}
	};
	
	struct Type {
		std::uint32_t size = 0;
		std::uint16_t alignment = 0;
		std::uint16_t padding = 0;
		std::vector<MemberData> members;
		std::vector<std::string> names;
		
		auto name_of(MemberData const& member) const noexcept -> std::string_view;
		auto name_of(std::uint32_t id) const noexcept -> std::string_view;
		auto name_of(hash_t hash) const noexcept -> std::string_view;
		auto total_size() const& noexcept -> std::size_t;
		auto member(std::string_view name) const& noexcept -> MemberData const*;
		auto member(hash_t name) const& noexcept -> MemberData const*;
		auto member(std::uint32_t id) const& noexcept -> MemberData const*;
	};
	
	struct ConstObject {
		ConstObject() = default;
		ConstObject(Type const& type, std::byte const* object) noexcept;
		auto access(MemberData const& member) const& noexcept -> GenericSerializedData;
		auto access(std::string_view name) const& noexcept -> std::optional<GenericSerializedData>;
		auto access(std::uint32_t id) const& noexcept -> std::optional<GenericSerializedData>;
		auto access(hash_t hash) const& noexcept -> std::optional<GenericSerializedData>;
	
		auto buffer() const& noexcept -> std::span<std::byte const>;
		auto raw_access(MemberData const& member) const& noexcept -> std::byte const*;
			
		[[nodiscard]]
		auto type() const& noexcept -> Type const&;
		
	private:
		Type const* _type = nullptr;
		std::byte const* _object = nullptr;
	};
	
	struct Object {
		Object() = default;
		Object(Type const& type, std::byte* object) noexcept;
		auto assign(MemberData const& member, GenericSerializedData data) const& noexcept -> bool;
		auto assign(std::string_view name, GenericSerializedData data) const& noexcept -> bool;
		auto assign(std::uint32_t id, GenericSerializedData data) const& noexcept -> bool;
		auto assign(hash_t hash, GenericSerializedData data) const& noexcept -> bool;
		auto access(MemberData const& member) const& noexcept -> GenericSerializedData;
		auto access(std::string_view name) const& noexcept -> std::optional<GenericSerializedData>;
		auto access(std::uint32_t id) const& noexcept -> std::optional<GenericSerializedData>;
		auto access(hash_t hash) const& noexcept -> std::optional<GenericSerializedData>;
		
		auto buffer() const& noexcept -> std::span<std::byte>;
		auto raw_access(MemberData const& member) const& noexcept -> std::byte*;
		
		inline operator ConstObject() const& noexcept {
			return ConstObject{*_type, _object};
		}
		
		[[nodiscard]]
		auto type() const& noexcept -> Type const&;
		
	private:
		Type const* _type = nullptr;
		std::byte* _object = nullptr;
	};
	
	template<typename T, typename ObjectType>
	struct ObjectIteratorBase {
		using iterator_category = std::random_access_iterator_tag;
		using value_type = std::pair<std::string const&, MemberData const&>;
		using difference_type = std::ptrdiff_t;
		using pointer = value_type*;
		using reference = value_type&;
		using const_reference = value_type const&;
		
		ObjectIteratorBase(T current, Type const& type) noexcept : _current{current}, _type{&type} {}
		
		auto operator<=>(ObjectIteratorBase const& other) const& noexcept = default;
		
		auto operator++() & noexcept -> ObjectIteratorBase& {
			_current += _type->total_size();
			return *this;
		}
		
		inline auto operator++(int) & noexcept -> ObjectIteratorBase {
			auto self = *this;
			++(*this);
			return self;
		}
		
		auto operator--() & noexcept -> ObjectIteratorBase& {
			_current -= _type->total_size();
			return *this;
		}
		
		inline auto operator--(int) & noexcept -> ObjectIteratorBase {
			auto self = *this;
			--(*this);
			return self;
		}
		
		auto operator*() const& noexcept -> ObjectType {
			return ObjectType{*_type, _current};
		}
		
		friend auto operator+(ObjectIteratorBase const& lhs, std::ptrdiff_t n) -> ObjectIteratorBase {
			return ObjectIteratorBase{lhs._current + lhs._type->total_size() * n, *lhs._type};
		}
		
		friend auto operator+(std::ptrdiff_t n, ObjectIteratorBase const& rhs) -> ObjectIteratorBase {
			return ObjectIteratorBase{rhs._current + rhs._type->total_size() * n, *rhs._type};
		}
		
		friend auto operator-(std::ptrdiff_t n, ObjectIteratorBase const& rhs) -> ObjectIteratorBase {
			return ObjectIteratorBase{rhs._current - rhs._type->total_size() * n, *rhs._type};
		}
		
		friend auto operator-(ObjectIteratorBase const& lhs, std::ptrdiff_t n) -> ObjectIteratorBase {
			return ObjectIteratorBase{lhs._current - lhs._type->total_size() * n, *lhs._type};
		}
		
		friend auto operator-(ObjectIteratorBase const& lhs, ObjectIteratorBase const& rhs) -> difference_type {
			return (lhs._current - rhs._current) / lhs._type->total_size();
		}
		
	private:
		T _current;
		Type const* _type;
	};
	
	using ObjectIterator = ObjectIteratorBase<std::byte*, Object>;
	using ConstObjectIterator = ObjectIteratorBase<std::byte const*, ConstObject>;
	
	DynStructBuffer() noexcept = default;
	DynStructBuffer(DynStructBuffer&&) noexcept = default;
	auto operator=(DynStructBuffer&&) noexcept -> DynStructBuffer& = default;
	DynStructBuffer(DynStructBuffer const&);
	auto operator=(DynStructBuffer const&) -> DynStructBuffer&;
	~DynStructBuffer() noexcept = default;
	
	explicit DynStructBuffer(range_of<Member> auto&& members) {
		for (auto const& member : FWD(members)) {
			if (member.alignment == 0) {
				add_member(member.id, member.type, member.name);
			} else {
				add_member(member.id, member.type, member.name, member.alignment);
			}
		}
	}
	
	explicit DynStructBuffer(forwarded<Member> auto&&... members) : DynStructBuffer{std::array{FWD(members)...}} {}
	
	void reserve(std::size_t amount);
	auto capacity() const noexcept -> std::size_t;
	auto size() const noexcept -> std::size_t;
	auto buffer() const& noexcept -> std::span<std::byte const>;
	inline auto empty() const noexcept -> bool { return _size == 0; }
	void shrink(std::size_t amount_shrink) noexcept;
	void grow(std::size_t amount_grow);
	void resize(std::size_t size);
	auto push() -> Object;
	auto operator[](std::size_t index) & noexcept -> Object;
	auto operator[](std::size_t index) const& noexcept -> ConstObject;
	auto begin() & noexcept -> ObjectIterator;
	auto end() & noexcept -> ObjectIterator;
	auto begin() const& noexcept -> ConstObjectIterator;
	auto end() const& noexcept -> ConstObjectIterator;
	auto cbegin() const& noexcept -> ConstObjectIterator;
	auto cend() const& noexcept -> ConstObjectIterator;
	
	template<typename T, std::size_t Size = std::tuple_size_v<T>>
	void push_back(T const& data) {
		reserve_at_least(_size + 1);
		auto const size = _size++;
		for_sequence<Size>([&](auto i) {
			using namespace std;
			auto const object = object_data_at(size);
			auto const member = _type.members[i];
			auto const vertex_element = get<i>(data);
			using vertex_element_type = std::tuple_element_t<i, T>;
			check_type(type_id<vertex_element_type>, member.type);
			new (object + member.offset) vertex_element_type{vertex_element};
		});
	}
	
	auto emplace_back(auto&&... args) -> Object {
		auto size = _size;
		push_back(std::forward_as_tuple(FWD(args)...));
		return Object{_type, object_data_at(size)};
	}
	
	[[nodiscard]]
	auto type() const& noexcept -> Type const&;
	
private:
	auto object_data_at(std::size_t index) & noexcept -> std::byte*;
	auto object_data_at(std::size_t index) const& noexcept -> std::byte const*;
	auto nth_offset(std::size_t nth) const noexcept -> std::size_t;
	void reallocate(std::size_t capacity);
	void allocate_member(std::uint32_t id, type_id_t type, std::string name, std::uint32_t size, std::uint16_t alignment);
	void reserve_at_least(std::size_t amount);
	void add_member(std::uint32_t id, type_id_t type, std::string name);
	void add_member(std::uint32_t id, type_id_t type, std::string name, std::uint32_t alignment);
	
	void check_type(type_id_t const type1, type_id_t const type2) noexcept;
	
	struct AlignedDelete {
		void operator()(std::byte* ptr) const noexcept;
	};
	
	Type _type;
	std::size_t _size = 0;
	std::size_t _capacity = 0;
	std::unique_ptr<std::byte[], AlignedDelete> _memory = {};
};

} // namespace sbg

#include "subgine/common/undef.h"
