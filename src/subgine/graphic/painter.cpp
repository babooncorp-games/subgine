#include "painter.h"

namespace sbg {

auto Painter::uniforms_for(UniformSet const& model, UniformSet const& environment) -> ComputedUniformEncoder::uniforms_t {
	return _computedUniforms.compute_uniforms(ModelUniformContext{model, environment});
}

void Painter::add(ModelInfo model, UniformSet const& environment) {
	_painter->add(std::move(model), environment);
}

void Painter::draw() const {
	_painter->draw();
}

void Painter::refresh(UniformSet const& environment) {
	_painter->refresh(environment);
}

void Painter::prepare(UniformSet const& context) {
	_painter->prepare(context);
}

void Painter::update(ModelInfo model, UniformSet const& environment) {
	_painter->update(std::move(model), environment);
}

void Painter::remove(ModelInfo const& model) {
	_painter->remove(model);
}

} // namesapce sbg
