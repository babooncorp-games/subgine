#pragma once

#include "subgine/common/hash.h"

#include <functional>
#include <compare>

namespace sbg {

struct ModelHandle {
	void const* pointer;
	
	friend auto operator<=>(ModelHandle const& lhs, ModelHandle const& rhs) noexcept = default;
};

template<>
struct murmur_hash<ModelHandle> {
	auto operator()(ModelHandle const& handle) const noexcept -> std::size_t;
};


} // namespace sbg

template<>
struct std::hash<sbg::ModelHandle> {
	auto operator()(sbg::ModelHandle const& handle) const noexcept -> std::size_t;
};
