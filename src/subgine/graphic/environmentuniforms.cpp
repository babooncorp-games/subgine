#include "environmentuniforms.h"

using namespace sbg;

void EnvironmentUniforms::uniform_source(std::function<void(EnvironmentUniformEncoder&)> source) {
	_sources.emplace_back(source);
}

auto EnvironmentUniforms::uniforms() const -> UniformSet {
	auto encoder = EnvironmentUniformEncoder{};
	
	for (auto const& source : _sources) {
		source(encoder);
	}
	
	return UniformSet{std::move(encoder).uniforms};
}
