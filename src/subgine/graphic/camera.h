#pragma once

#include "subgine/provider/provider.h"

#include <glm/glm.hpp>

namespace sbg {

struct CameraService;

struct Camera {
	Camera() = default;
	Camera(Provider<glm::mat4> projection, Provider<glm::mat4> view) noexcept;
	
	Provider<glm::mat4> projection = glm::mat4{1.f};
	Provider<glm::mat4> view = glm::mat4{1.f};
	
	friend auto service_map(Camera const&) -> CameraService;
};

} // namespace sbg
