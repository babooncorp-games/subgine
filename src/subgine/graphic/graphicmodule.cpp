#include "graphicmodule.h"

#include "subgine/graphic.h"
#include "subgine/resource.h"
#include "subgine/provider.h"
#include "subgine/scene.h"
#include "subgine/window.h"
#include "subgine/entity.h"

#include "subgine/log.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glbinding/gl/functions.h>

using namespace sbg;

void GraphicModule::setupModelCreator(ModelCreator& modelCreator) const {
	modelCreator.add("simple-sprite", [](Entity entity, Property data) {
 		SimpleSprite sprite;
		
		sprite.origin = apply_transform_from_property(data);
		sprite.transform = Component<Transform>{entity};
		sprite.texture = TextureTag{data["texture"]};
		
		return sprite;
	});
}

auto setupEntityBindingCreator(EntityBindingCreator& creator) {
	creator.add("entity-camera", [](Camera& camera, sbg::Entity entity) {
		auto const transform = Component<Transform>{entity};
		camera.view = [transform] {
			auto const position = transform->position2d();
			return glm::translate(glm::mat4{1.f}, glm::vec3{position.x, position.y, 0});
		};
	});
}

void GraphicModule::setupViewCreator(ViewCreator& viewCreator) const {
	viewCreator.add("basic", [](Camera const& camera, Window const& window, Property data) {
		return [
			step = std::string{data["step"]},
			&camera,
			&window
		](auto paintJob, UniformSet environment) {
			auto const windowSize = window.size();
			
			environment.emplace("view", camera.view());
			environment.emplace("projection", camera.projection());
			environment.emplace("viewport", windowSize);
			
			gl::glViewport(0, 0, windowSize.x, windowSize.y);
			
			paintJob.prepare(step, environment);
			paintJob.draw(step);
		};
	});
	
	viewCreator.add("multi", [](Camera const& camera, Window const& window, Property data) {
		return [
			steps = std::vector<std::string>{data["step"]},
			&camera,
			&window
		](auto paintJob, UniformSet environment) {
			auto const windowSize = window.size();
			
			environment.emplace("view", camera.view());
			environment.emplace("projection", camera.projection());
			environment.emplace("viewport", windowSize);
			
			gl::glViewport(0, 0, windowSize.x, windowSize.y);
			
			for (auto const& step : steps) {
				paintJob.prepare(step, environment);
				paintJob.draw(step);
			}
		};
	});
}

void GraphicModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("visual", [](ModelFactory modelFactory, Entity entity, Property data) {
		Visual visual;
		
		for (auto const [name, model] : data["models"].items()) {
			auto const type = std::string_view{model["type"]};
			Log::trace(SBG_LOG_INFO, "Creating model", log::enquote(name), "of type", log::enquote(type));
			modelFactory.create(type, name, visual, entity, model["data"]);
		}
		
		return visual;
	});
	
	componentCreator.add("camera", [](ProviderFactory<glm::mat4> providerFactory, Camera& camera, Entity entity, Property data) {
		auto projection = data["projection"];
		auto view_json = data["view"];
		
		camera.projection = providerFactory.create(projection["type"], entity, projection["data"]);
		camera.view = providerFactory.create(view_json["type"], entity, view_json["data"]);
	});
	
	componentCreator.add("transform", [](Entity entity, Property data) {
		auto transform = Transform{};
		
		if (auto const zvalue = data["z"]; not zvalue.isNull() and data["position3d"].isNull()) {
			transform = transform.zvalue(float{zvalue});
		}
		
		if (auto const position = data["position2d"]; !position.isNull()) {
			transform = transform.position2d(Vector2f{position});
		}
		
		if (auto const orientation = data["orientation2d"]; !orientation.isNull()) {
			transform = transform.orientation2d(float{orientation});
		}
		
		if (auto const scale = data["scale2d"]; !scale.isNull()) {
			transform = transform.scale2d(Vector2f{scale});
		}
		
		if (auto const position = data["position3d"]; !position.isNull()) {
			transform = transform.position3d(Vector3f{position});
		}
		
		if (auto const orientation = data["orientation3d"]; !orientation.isNull()) {
			auto const orientation3d = Vector3f{orientation};
			transform = transform.orientation3d(glm::quat{glm::vec3{orientation3d.x, orientation3d.y, orientation3d.z}});
		}
		
		if (auto const scale = data["scale3d"]; !scale.isNull()) {
			transform = transform.scale3d(Vector3f{scale});
		}
		
		return transform;
	});
}

void GraphicModule::setupEntityBindingCreator(EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("view", [](View& view, Entity entity) {
		view.add(entity);
	});
}

void GraphicModule::setupPluggerCreator(PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("view", [](ViewFactory viewFactory, Window& window, Property data) {
		auto& view = viewFactory.create(data["type"], data["data"]);
		
		return Plugger{
			[&view, &window] {
				window.addView(view);
			},
			[&view, &window] {
				window.removeView(view);
			}
		};
	});
}

void GraphicModule::setupProviderCreator(ProviderCreator<glm::mat4>& providerCreator) const {
	providerCreator.add("translation", [](Entity, Property data) {
		auto const position = Vector3f{data["translation"]};
		return [position] { return glm::translate(glm::mat4{1.f}, glm::vec3{position.x, position.y, position.z}); };
	});
	
	providerCreator.add("identity", [] {
		return glm::mat4{1.f};
	});
}

void GraphicModule::setupComputedUniformCreator(ComputedUniformCreator& computedUniformCreator) const {
	using namespace sbg::literals;
	computedUniformCreator.add("transform", [] {
		return [](ComputedUniformEncoder& encoder, ModelUniformContext /* unused */) {
			encoder
				<< uniform::updating("transform"_h, [](ModelUniformContext const& context) noexcept {
					auto const cameraProjection = context.environment.required<glm::mat4>("projection"_h);
					auto const cameraView = context.environment.required<glm::mat4>("view"_h);
					
					auto const modelMatrix = context.model.required<glm::mat4>("model"_h);
					auto const modelView = cameraView * modelMatrix;
					auto const transform = cameraProjection * modelView;
					
					return transform;
				})
				<< uniform::updating("modelview"_h, [](ModelUniformContext const& context) noexcept {
					auto const modelMatrix = context.model.required<glm::mat4>("model"_h);
					auto const modelView = context.environment.required<glm::mat4>("view"_h) * modelMatrix;
					
					return modelView;
				});
		};
	});
}
