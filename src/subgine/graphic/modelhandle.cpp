#include "modelhandle.h"

using namespace sbg;

auto murmur_hash<ModelHandle>::operator()(ModelHandle const& handle) const noexcept -> std::size_t {
	return murmur_hash<void const*>()(handle.pointer);
}

auto std::hash<sbg::ModelHandle>::operator()(sbg::ModelHandle const& handle) const noexcept -> std::size_t {
	return std::hash<void const*>{}(handle.pointer);
}
