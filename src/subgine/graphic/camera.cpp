#include "camera.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <cmath>

using namespace sbg;

Camera::Camera(Provider<glm::mat4> p, Provider<glm::mat4> v) noexcept : view{std::move(v)}, projection{std::move(p)} {}
