#pragma once

#include "uniform.h"

#include "subgine/common/erasedtype.h"
#include "subgine/common/type_id.h"
#include "subgine/common/detection.h"

#include <vector>

namespace sbg {

template<typename T>
concept serializable_model = requires(T const& t, ModelUniformEncoder& encoder) {
	{ T::serialize(encoder, t) } -> std::same_as<void>;
};

struct Model {
private:
	using ReserializePtr = void(*)(ModelUniformEncoder&, Model const& self);
	
public:
	explicit Model(serializable_model auto m, std::string p) noexcept : Model{std::move(m), std::vector<std::string>{std::move(p)}} {}
	
	template<serializable_model T>
	explicit Model(T m, std::vector<std::string> p) noexcept :
		model{std::move(m)}, painters{std::move(p)},
		reserialize_function{
			[](ModelUniformEncoder& encoder, Model const& self) {
				T::serialize(encoder, self.model.as<T>());
			}
		}
	{}
	
	auto handle() const noexcept -> ModelHandle;
	
	inline auto reserialize() const -> ModelUniformEncoder::uniforms_t {
		auto encoder = ModelUniformEncoder{};
		reserialize_function(encoder, *this);
		return std::move(encoder.uniforms);
	}
	
	ErasedType model;
	std::vector<std::string> painters;
	UniformSet uniforms;
	
private:
	ReserializePtr reserialize_function;
};

} // namespace sbg
