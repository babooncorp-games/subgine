#include "model.h"

using namespace sbg;

auto Model::handle() const noexcept -> ModelHandle {
	return ModelHandle{model.pointer()};
}
