#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

struct Viewport {
	Vector2i position;
	Vector2i size;
};

} // namespace sbg
