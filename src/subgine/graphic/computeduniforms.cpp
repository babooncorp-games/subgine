#include "computeduniforms.h"

using namespace sbg;

auto ComputedUniforms::uniform_source(UniformMaker maker) -> void {
	if (maker) {
		_makers.emplace_back(std::move(maker));
	}
}

auto ComputedUniforms::compute_uniforms(ModelUniformContext context) -> ComputedUniformEncoder::uniforms_t {
	_encoder.uniforms.clear();
	
	for (auto const& maker : _makers) {
		maker(_encoder, context);
	}
	
	return _encoder.uniforms;
}
