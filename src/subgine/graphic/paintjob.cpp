#include "paintjob.h"

#include "view.h"

namespace sbg {

void PaintJob::prepare(std::string_view step, UniformSet const& context) const {
	// update uniform in entity
	// update computed uniforms
	for (auto& [update, updating_infos] : _renderSystem->update_computed) {
		update(_entityManager, updating_infos, context);
	}
	for (auto& [painter_step, painter] : _renderSystem->steps) {
		if (painter_step == step) {
			painter.prepare(context);
		}
	}
}

void PaintJob::draw(std::string_view step) const {
	for (auto const& [painter_step, painter] : _renderSystem->steps) {
		if (painter_step == step) {
			painter.draw();
		}
	}
}

} // namespace sbg
