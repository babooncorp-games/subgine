#pragma once

#include "uniform.h"

#include <functional>

namespace sbg {

// TODO: Do we keep this crappy crapidouldlidou?
struct EnvironmentUniforms {
	auto uniforms() const -> UniformSet;
	void uniform_source(std::function<void(EnvironmentUniformEncoder&)> source);
	
private:
	std::vector<std::function<void(EnvironmentUniformEncoder&)>> _sources;
};

} // namespace sbg
