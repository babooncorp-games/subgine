#include "uniform.h"

#include "subgine/log.h"

namespace sbg {

void UniformSet::fatal_not_found(std::string_view name) noexcept {
	Log::fatal(SBG_LOG_INFO, "Required uniform", log::enquote(name), "not found");
}

void UniformSet::fatal_not_found(hash_t name) noexcept {
	Log::fatal(SBG_LOG_INFO, "Required uniform hash(", log::unpad(static_cast<std::size_t>(name)), ") not found");
}

auto UniformSet::find(hash_t name) const& noexcept -> uniforms_t::const_iterator {
	return uniforms.find(name);
}

void UniformSet::clear() & noexcept {
	uniforms.clear();
}

void UniformSet::emplace(hash_t name, UniformData data) & {
	uniforms.emplace(name, std::move(data));
}

void UniformSet::emplace(std::string_view name, UniformData uniform) & {
	uniforms.emplace(murmur64a(name), std::move(uniform));
}

} // namespace sbg
