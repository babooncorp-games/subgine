#pragma once

#include <glm/glm.hpp>

namespace sbg {

struct GraphicModuleService;

struct ModelCreator;
struct PluggerCreator;
struct ViewCreator;
struct ComponentCreator;
struct EntityBindingCreator;
template<typename>
struct ProviderCreator;
struct ComputedUniformCreator;

struct GraphicModule {
	void setupModelCreator(ModelCreator& modelCreator) const;
	void setupViewCreator(ViewCreator& viewCreator) const;
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	void setupEntityBindingCreator(EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(PluggerCreator& pluggerCreator) const;
	void setupProviderCreator(ProviderCreator<glm::mat4>& providerCreator) const;
	void setupComputedUniformCreator(ComputedUniformCreator& computedUniformCreator) const;
	
	friend auto service_map(GraphicModule const&) -> GraphicModuleService;
};

} // namespace sbg
