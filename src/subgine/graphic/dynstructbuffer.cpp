#include "dynstructbuffer.h"

#include "subgine/graphic/uniformdata.h"
#include "uniform.h"

#include "subgine/log.h"
#include "subgine/common/traits.h"

#include <algorithm>

using namespace sbg;

constexpr auto object_alignement = std::uint32_t{16};
constexpr auto memory_alignement = std::align_val_t{object_alignement};

constexpr static auto growth(std::size_t n) -> std::size_t {
	return n < 8 ? 8 : std::max(std::size_t{1}, (std::size_t{3} * n) / std::size_t{2});
}

static auto allocate_buffer(std::size_t size) -> std::byte* {
	return static_cast<std::byte*>(::operator new[](size, memory_alignement));
}

static constexpr auto memory_property_visitor = []<typename T>() noexcept -> std::pair<std::uint32_t, std::uint32_t> {
	static_assert(alignof(T) <= 16, "Cannot insert an object with alignement larger than 16");
	return std::pair{static_cast<std::uint32_t>(sizeof(T)), static_cast<std::uint32_t>(alignof(T))};
};

void DynStructBuffer::AlignedDelete::operator()(std::byte* ptr) const noexcept {
	::operator delete[](ptr, memory_alignement);
}

auto DynStructBuffer::Type::name_of(MemberData const& member) const noexcept -> std::string_view {
	return name_of(member.id);
}

auto DynStructBuffer::Type::name_of(std::uint32_t id) const noexcept -> std::string_view {
	auto it = std::find_if(members.begin(), members.end(), [&](MemberData const& member) {
		return member.id == id;
	});
	
	return *(names.begin() + std::distance(members.begin(), it));
}

auto DynStructBuffer::Type::name_of(hash_t hash) const noexcept -> std::string_view {
	auto it = std::find_if(members.begin(), members.end(), [&](MemberData const& member) {
		return member.name == hash;
	});
	
	return *(names.begin() + std::distance(members.begin(), it));
}

auto DynStructBuffer::Type::total_size() const& noexcept -> std::size_t {
	return size + padding;
}

auto DynStructBuffer::Type::member(std::string_view name) const& noexcept -> MemberData const* {
	return member(murmur64a(name));
}

auto DynStructBuffer::Type::member(hash_t name) const& noexcept -> MemberData const* {
	auto it = std::find_if(members.begin(), members.end(), [&](auto const& member) {
		return member.name == name;
	});
	
	return it == members.end() ? nullptr : std::addressof(*it);
}

auto DynStructBuffer::Type::member(std::uint32_t id) const& noexcept -> MemberData const* {
	auto it = std::find_if(members.begin(), members.end(), [&](auto const& member) {
		return member.id == id;
	});
	
	return it == members.end() ? nullptr : std::addressof(*it);
}

DynStructBuffer::ConstObject::ConstObject(Type const& type, std::byte const* object) noexcept :
	_type{&type}, _object{object} {}

auto DynStructBuffer::ConstObject::access(MemberData const& member) const& noexcept -> GenericSerializedData {
	auto const member_pointer = _object + member.offset;
	
	return visit_type_generic_buffer_data(member.type, [&]<typename T>() {
		return GenericSerializedData{
			std::in_place_type<T>,
			*static_cast<T const*>(static_cast<void const*>(member_pointer))
		};
	});
}

auto DynStructBuffer::ConstObject::access(std::string_view name) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(name)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::ConstObject::access(std::uint32_t id) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(id)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::ConstObject::access(hash_t hash) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(hash)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::ConstObject::buffer() const& noexcept -> std::span<std::byte const> {
	return std::span{_object, _type->total_size()};
}

auto DynStructBuffer::ConstObject::raw_access(MemberData const& member) const& noexcept -> std::byte const* {
	return _object + member.offset;
}

auto DynStructBuffer::ConstObject::type() const& noexcept -> Type const& {
	return *_type;
}

DynStructBuffer::Object::Object(Type const& type, std::byte* object) noexcept :
	_type{&type}, _object{object} {}

auto DynStructBuffer::Object::assign(MemberData const& member, GenericSerializedData data) const& noexcept -> bool {
	auto const member_pointer = _object + member.offset;
	
	return std::visit(
		[&]<typename T>(T const& data) noexcept {
			if (type_id<T> == member.type) {
				
				new (member_pointer) T{data};
				return true;
			}
			
			return false;
		},
		data
	);
}

auto DynStructBuffer::Object::assign(hash_t hash, GenericSerializedData data) const& noexcept -> bool {
	if (auto const member = _type->member(hash)) {
		return assign(*member, data);
	}
	
	return false;
}

auto DynStructBuffer::Object::assign(std::string_view name, GenericSerializedData data) const& noexcept -> bool {
	if (auto const member = _type->member(name)) {
		return assign(*member, data);
	}
	
	return false;
}

auto DynStructBuffer::Object::assign(std::uint32_t id, GenericSerializedData data) const& noexcept -> bool {
	if (auto const member = _type->member(id)) {
		return assign(*member, data);
	}
	
	return false;
}

auto DynStructBuffer::Object::access(MemberData const& member) const& noexcept -> GenericSerializedData {
	auto const member_pointer = _object + member.offset;
	
	return visit_type_generic_buffer_data(member.type, [&]<typename T>() {
		return GenericSerializedData{
			std::in_place_type<T>,
			*static_cast<T const*>(static_cast<void const*>(member_pointer))
		};
	});
}

auto DynStructBuffer::Object::access(std::string_view name) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(name)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::Object::access(std::uint32_t id) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(id)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::Object::access(hash_t hash) const& noexcept ->  std::optional<GenericSerializedData> {
	if (auto const member = _type->member(hash)) {
		return access(*member);
	}
	
	return {};
}

auto DynStructBuffer::Object::buffer() const& noexcept -> std::span<std::byte> {
	return std::span{_object, _type->total_size()};
}

auto DynStructBuffer::Object::raw_access(MemberData const& member) const& noexcept -> std::byte* {
	return _object + member.offset;
}

auto DynStructBuffer::Object::type() const& noexcept -> Type const& {
	return *_type;
}

DynStructBuffer::DynStructBuffer(DynStructBuffer const& other) :
	_size{other._size},
	_capacity{other._capacity},
	_type{other._type},
	_memory{other._memory ? allocate_buffer(other._capacity * other._type.size) : nullptr}
{
	if (_memory) {
		std::uninitialized_copy_n(std::assume_aligned<object_alignement>(other._memory.get()), nth_offset(_size), std::assume_aligned<object_alignement>(_memory.get()));
	}
}

auto DynStructBuffer::operator=(DynStructBuffer const& other) -> DynStructBuffer& {
	auto copy = DynStructBuffer{other};
	std::swap(*this, copy);
	return *this;
}

void DynStructBuffer::add_member(std::uint32_t id, type_id_t type, std::string name) {
	auto const [size, alignment] = visit_type_generic_buffer_data(type, memory_property_visitor);
	allocate_member(id, type, name, size, alignment);
}

void DynStructBuffer::add_member(std::uint32_t id, type_id_t type, std::string name, std::uint32_t alignment) {
	auto const [size, default_alignment] = visit_type_generic_buffer_data(type, memory_property_visitor);
	
	Log::error(
		[&, &default_alignment = default_alignment]{ return alignment < default_alignment; },
		SBG_LOG_INFO, "Cannot add a member with smaller alignment than the minimum required by its type"
	);
	
	allocate_member(id, type, name, size, std::clamp(alignment, default_alignment, 16u));
}

void DynStructBuffer::allocate_member(std::uint32_t id, type_id_t type, std::string name, std::uint32_t size, std::uint16_t alignment) {
	Log::fatal(
		[&]{ return _memory != nullptr; },
		SBG_LOG_INFO, "Cannot add a new uniform member when objects are allocated"
	);
	
	Log::fatal(
		[&]{ return alignment > object_alignement; },
		SBG_LOG_INFO, "Cannot add a uniform member with an alignment greater than", object_alignement
	);
	
	auto const padding = _type.size % alignment;
	
	_type.members.emplace_back(id, _type.size + padding, type, murmur64a(name));
	_type.names.emplace_back(std::move(name));
	_type.alignment = std::max(_type.alignment, alignment);
	_type.size += size + padding;
	_type.padding = _type.size % _type.alignment;
}

void DynStructBuffer::resize(std::size_t size) {
	reserve_at_least(size);
	_size = size;
}

auto DynStructBuffer::push() -> Object {
	auto const index = _size;
	++_size;
	reserve_at_least(_size);
	return Object{_type, object_data_at(index)};
}

void DynStructBuffer::reserve(std::size_t amount) {
	if (_capacity < amount) {
		reallocate(amount);
	}
}

void DynStructBuffer::reserve_at_least(std::size_t amount) {
	if (_capacity < amount) {
		reallocate(growth(amount));
	}
}

void DynStructBuffer::reallocate(std::size_t capacity) {
	auto memory = std::unique_ptr<std::byte[], AlignedDelete>{allocate_buffer(nth_offset(capacity))};
	
	if (_memory) {
		std::uninitialized_copy_n(_memory.get(), nth_offset(_size), memory.get());
	}
	
	_memory = std::move(memory);
	_capacity = capacity;
}

auto DynStructBuffer::capacity() const noexcept -> std::size_t {
	return _capacity;
}

auto DynStructBuffer::size() const noexcept -> std::size_t {
	return _size;
}

auto DynStructBuffer::begin() & noexcept -> ObjectIterator {
	return ObjectIterator{_memory.get(), _type};
}

auto DynStructBuffer::end() & noexcept -> ObjectIterator {
	return ObjectIterator{object_data_at(_size), _type};
}

auto DynStructBuffer::begin() const& noexcept -> ConstObjectIterator {
	return ConstObjectIterator{_memory.get(), _type};
}

auto DynStructBuffer::end() const& noexcept -> ConstObjectIterator {
	return ConstObjectIterator{object_data_at(_size), _type};
}

auto DynStructBuffer::cbegin() const& noexcept -> ConstObjectIterator {
	return ConstObjectIterator{_memory.get(), _type};
}

auto DynStructBuffer::cend() const& noexcept -> ConstObjectIterator {
	return ConstObjectIterator{object_data_at(_size), _type};
}

auto DynStructBuffer::object_data_at(std::size_t index) & noexcept -> std::byte* {
	return std::assume_aligned<object_alignement>(_memory.get()) + nth_offset(index);
}

auto DynStructBuffer::object_data_at(std::size_t index) const& noexcept -> std::byte const* {
	return std::assume_aligned<object_alignement>(_memory.get()) + nth_offset(index);
}

auto DynStructBuffer::nth_offset(std::size_t nth) const noexcept -> std::size_t {
	return (_type.size + _type.padding) * nth;
}

auto DynStructBuffer::operator[](std::size_t index) & noexcept -> Object {
	return Object{_type, object_data_at(index)};
}

auto DynStructBuffer::operator[](std::size_t index) const& noexcept -> ConstObject {
	return ConstObject{_type, object_data_at(index)};
}

auto DynStructBuffer::type() const& noexcept -> Type const& {
	return _type;
}

auto DynStructBuffer::buffer() const& noexcept -> std::span<std::byte const> {
	return std::span{_memory.get(), _size * _type.total_size()};
}

void DynStructBuffer::shrink(std::size_t amount_shrink) noexcept {
	_size -= amount_shrink;
}

void DynStructBuffer::grow(std::size_t amount_grow) {
	resize(_size + amount_grow);
}

void DynStructBuffer::check_type(type_id_t const type1, type_id_t const type2) noexcept {
	Log::fatal([&]{ return type1 != type2; }, SBG_LOG_INFO, "Vertex don't match member type");
}
