#pragma once

#include "uniform.h"

#include <functional>
#include <stdexcept>
#include <span>

namespace sbg {

struct ComputedUniforms {
	using UniformMaker = std::function<void(ComputedUniformEncoder&, ModelUniformContext)>;
	
	void uniform_source(UniformMaker maker);
	auto compute_uniforms(ModelUniformContext context) -> ComputedUniformEncoder::uniforms_t;
	
private:
	ComputedUniformEncoder _encoder;
	std::vector<UniformMaker> _makers;
};

} // namespace sbg
