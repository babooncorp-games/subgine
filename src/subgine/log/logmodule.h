#pragma once

namespace sbg {

struct LogModuleService;

struct LogModule {
	friend auto service_map(LogModule const&) -> LogModuleService;
};

} // namespace sbg
