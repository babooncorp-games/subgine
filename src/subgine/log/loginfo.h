#pragma once

#include "subgine/common/traits.h"

#include <ostream>

namespace sbg {

template<typename... Args>
struct LogInfo : std::tuple<Args...> {
	using std::tuple<Args...>::tuple;
	
	inline friend std::ostream& operator<<(std::ostream& stream, const LogInfo& logInfo) {
		for_tuple(static_cast<const std::tuple<Args...>&>(logInfo), [&](auto const& arg) {
			stream << arg;
		});
		
		return stream;
	}
};

template<typename... Args>
LogInfo(Args const&...) -> LogInfo<Args const&...>;

// TODO: C++20 remove this and use source_location
#define SBG_LOG_INFO ::sbg::LogInfo{ __FILE__ ":", __LINE__ , ":" }

} // namespace sbg
