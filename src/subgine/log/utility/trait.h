#pragma once

#include "subgine/common/traits.h"
#include "subgine/common/math.h"

#include <ostream>

namespace sbg {

template<typename...>
struct LogInfo;

template<typename, typename>
struct LogDecorationImpl;

template<typename T>
using stream_t = decltype(std::declval<std::ostream&>() << std::declval<T const&>());

template<typename, typename = void>
struct is_loggable : std::false_type {};

template<typename T>
struct is_loggable<T, std::void_t<stream_t<T>>> : std::true_type {};

// TODO: Completely use aliases when MSVC supports it
template<typename T>
inline constexpr auto is_loginfo = is_specialisation_of_v<LogInfo, T>;

template<typename... Ts>
inline constexpr auto are_loggable = std::conjunction_v<is_loggable<Ts>...>;

template<typename... Ts>
inline constexpr auto is_log = is_loginfo<std::tuple_element_t<0, std::tuple<Ts...>>> && are_loggable<Ts...>;

template<typename C, typename... Ts>
inline constexpr auto is_conditional_log = has_call_operator_v<C> && is_log<Ts...>;

template<typename T>
inline constexpr auto is_decorated = is_specialisation_of_v<LogDecorationImpl, T>;

} // namespace sbg
