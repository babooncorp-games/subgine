#pragma once

#ifndef SBG_LOGGER_TYPE
#error "To use the logger, the using module must be linked to sbg::log target"
#endif

#include "logdecoration.h"

#if SBG_LOGGER_TYPE == 0
	#include "logger/nooplogger.h"
	namespace sbg { using Log = NoopLogger; }
#elif SBG_LOGGER_TYPE == 1
	#include "logger/coloredconsolelogger.h"
	namespace sbg { using Log = ColoredConsoleLogger; }
#elif SBG_LOGGER_TYPE == 2
	#include "logger/consolelogger.h"
	namespace sbg { using Log = ConsoleLogger; }
#elif SBG_LOGGER_TYPE == 3
	#include "logger/filelogger.h"
	namespace sbg { using Log = FileLogger; }
#endif
