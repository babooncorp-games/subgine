#pragma once

#include "../logmodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct LogModuleService : kgr::single_service<LogModule> {};

} // namespace sbg
