#pragma once

#include "../utility/trait.h"

namespace sbg {

struct NoopLogger {
private:
	struct NoopCount { inline ~NoopCount(){} inline NoopCount() noexcept {} };
	
public:
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount trace(const Args&... args) {
		return {};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount debug(const Args&... args) {
		return {};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	[[noreturn]] static NoopCount fatal(const Args&... args) {
		std::terminate();
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount info(const Args&... args) {
		return {};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount success(const Args&... args) {
		return {};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount error(const Args&... args) {
		return {};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static NoopCount warning(const Args&... args) {
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static NoopCount debug(Cond condition, const Args&... args) {
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static NoopCount fatal(Cond condition, const Args&... args) {
		if (condition()) {
			std::terminate();
		}
		
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static NoopCount success(Cond condition, const Args&... args) {
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static NoopCount error(Cond condition, const Args&... args) {
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static NoopCount warning(Cond condition, const Args&... args) {
		return {};
	}
};

} // namespace sbg
