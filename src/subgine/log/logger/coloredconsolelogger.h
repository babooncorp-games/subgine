#pragma once

#include "../logger.h"

#include <iostream>

namespace sbg {

struct ColoredConsoleLogger : Logger<ColoredConsoleLogger> {
private:
	friend Logger<ColoredConsoleLogger>;
	
	static constexpr auto level = " ";
	static constexpr auto print_extended_info = false;
	
	template<typename... Args>
	static void log_debug(const Args&... args) {
		log(std::cout, "\033[0;37m", args..., "\033[0m");
	}
	
	template<typename... Args>
	static void log_trace(const Args&... args) {
		log(std::cout, "\033[0;32m", args..., "\033[0m");
	}
	
	template<typename... Args>
	static void log_error(const Args&... args) {
		log(std::cout, "\033[0;31m", args..., "\033[0m");
	}
	
	template<typename... Args>
	static void log_fatal(const Args&... args) {
		log(std::cout, "\033[0;31m", args..., "\033[0m");
	}
	
	template<typename... Args>
	static void log_warning(const Args&... args) {
		log(std::cout, "\033[0;33m", args..., "\033[0m");
	}
};

} // namespace sbg
