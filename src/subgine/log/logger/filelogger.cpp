#include "filelogger.h"

#include "../log.h"
#include "../loginfo.h"

using namespace sbg;

#define SBG_LOG_STRINGIFY_HELPER(x) #x
#define SBG_LOG_STRINGIFY(x) SBG_LOG_STRINGIFY_HELPER(x)

namespace {
	constexpr const char* path = SBG_LOG_STRINGIFY(SBG_LOG_PATH);
}

FileLogger::State& FileLogger::state(const char* path) {
	static State state{std::ofstream{path, std::ios::binary}, path};
	
	return state;
}

FileLogger::State& FileLogger::state() {
	return state(path);
}

void FileLogger::file(const char* path) {
	auto& s = state(path);
	
	if (path != s.path) {
		auto l = Log::warning(SBG_LOG_INFO, "Cannot set file path to an already opened log state.");
		Log::debug(SBG_LOG_INFO, "Desired:", log::enquote(path));
		Log::debug(SBG_LOG_INFO, "Actual:", log::enquote(s.path));
	}
	
	if constexpr (!std::is_same_v<FileLogger, Log>) {
		Log::warning(SBG_LOG_INFO, "Setting logging file path on inactive file logger");
	}
}
