#pragma once

#include "../logger.h"

#include <fstream>

namespace sbg {

struct FileLogger : Logger<FileLogger> {
	static void file(const char* path);
	
private:
	friend Logger<FileLogger>;
	
	static constexpr auto level = "";
	static constexpr auto print_extended_info = true;
	
	struct State {
		std::ofstream file;
		const char* path;
	};
	
	static State& state();
	static State& state(const char* path);
	
	template<typename... Args>
	static void log_default(const Args&... args) {
		log(state().file, args...);
	}
	
};

} // namespace sbg
