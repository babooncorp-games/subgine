#pragma once

#include "../logger.h"

#include <iostream>

namespace sbg {

struct ConsoleLogger : Logger<ConsoleLogger> {
private:
	friend Logger<ConsoleLogger>;
	
	static constexpr auto level = " ";
	static constexpr auto print_extended_info = false;
	
	template<typename... Args>
	static void log_default(const Args&... args) {
		log(std::cout, args...);
	}
};

} // namespace sbg
