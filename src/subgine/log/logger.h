#pragma once

#include "utility/trait.h"
#include "logdecoration.h"

#include <ostream>
#include <mutex>
#include <optional>

namespace sbg {
namespace detail {

struct BaseLogger {
protected:
	static auto level() -> int&;
	static auto lock() -> std::mutex&;

	static void log_start(std::ostream& stream, std::string_view level);
	static void log_start_extended(std::ostream& stream, std::string_view level);
	static void log_flush(std::ostream& stream);
	
	struct level_construct_t {} static constexpr level_construct{};

	struct Level {
		explicit Level(level_construct_t) noexcept { level()++; }
		~Level() { if (_levels) level()--; }
		
		Level(Level const&) = delete;
		Level(Level&& other) { other._levels = false; }
		
		auto operator=(Level const&) -> Level& = delete;
		
		auto operator=(Level&& rhs) -> Level& {
			rhs._levels = false;
			return *this;
		}
		
	private:
		bool _levels = true;
	};

private:
	static void log_level(std::ostream& stream, std::string_view level_string);
};

} // namespace detail

template<typename Derived>
struct Logger : private detail::BaseLogger {
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static auto trace(Args const&... args) -> Level {
		derived_trace(0, "[trace]:", args...);
		
		return Level{level_construct};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static auto debug(Args const&... args) -> Level {
		derived_debug(0, "[debug]:", args...);
		
		return Level{level_construct};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	[[noreturn]]
	static auto fatal(Args const&... args) -> Level {
		derived_fatal(0, "[fatal]:", args...);
		
		std::abort();
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static auto error(Args const&... args) -> Level {
		derived_error(0, "[error]:", args...);
		
		return Level{level_construct};
	}
	
	template<typename... Args, enable_if_i<is_log<Args...>> = 0>
	static auto warning(Args const&... args) -> Level {
		derived_warning(0, "[warning]:", args...);
		
		return Level{level_construct};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static auto debug(Cond condition, Args const&... args) -> std::optional<Level> {
		if (condition()) {
			debug(args...);
			return Level{level_construct};
		}
		
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static auto fatal(Cond condition, Args const&... args) -> std::optional<Level> {
		if (condition()) {
			fatal(args...);
		}
		
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static auto error(Cond condition, Args const&... args) -> std::optional<Level> {
		if (condition()) {
			error(args...);
			return Level{level_construct};
		}
		
		return {};
	}
	
	template<typename Cond, typename... Args, enable_if_i<is_conditional_log<Cond, Args...>> = 0>
	static auto warning(Cond condition, Args const&... args) -> std::optional<Level> {
		if (condition()) {
			warning(args...);
			return Level{level_construct};
		}
		
		return {};
	}
	
protected:
	static constexpr auto level = "";
	
	template<typename... Args>
	static auto log(std::ostream& stream, Args const&... args) -> std::enable_if_t<are_loggable<Args...>> {
		std::lock_guard l{lock()};
		
		if constexpr (Derived::print_extended_info) {
			log_start_extended(stream, Derived::level);
		} else {
			log_start(stream, Derived::level);
		}
		
		(stream << ... << maybe_pad(args));
		
		log_flush(stream);
	}
	
private:
	template<typename T>
	static auto maybe_pad(T const& log) {
		if constexpr (is_decorated<T>) {
			return log;
		} else {
			return log::pad(log);
		}
	}
	
	template<typename D = Derived, typename... Args>
	static auto derived_trace(int, Args const&... args) -> std::void_t<decltype(D::log_trace(args...))> {
		D::log_trace(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static auto derived_debug(int, Args const&... args) -> std::void_t<decltype(D::log_debug(args...))> {
		D::log_debug(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static auto derived_fatal(int, Args const&... args) -> std::void_t<decltype(D::log_fatal(args...))> {
		D::log_fatal(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static auto derived_error(int, Args const&... args) -> std::void_t<decltype(D::log_error(args...))> {
		D::log_error(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static auto derived_warning(int, Args const&... args) -> std::void_t<decltype(D::log_warning(args...))> {
		D::log_warning(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static void derived_trace(void*, Args const&... args) {
		D::log_default(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static void derived_debug(void*, Args const&... args) {
		D::log_default(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static void derived_fatal(void*, Args const&... args) {
		D::log_default(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static void derived_error(void*, Args const&... args) {
		D::log_default(args...);
	}
	
	template<typename D = Derived, typename... Args>
	static void derived_warning(void*, Args const&... args) {
		D::log_default(args...);
	}
};

} // namespace sbg
