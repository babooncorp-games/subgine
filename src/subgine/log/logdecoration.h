#pragma once

#include "subgine/common/traits.h"
#include "kangaru/detail/meta_list.hpp"

#include <ostream>

namespace sbg::detail {

template<std::size_t, typename T>
struct DecorationValue {
	T value;
};

template<typename, typename>
struct LogDecorationImpl;

// TODO: Implement a meta_list in subgine instead of using the private implementation in kangaru
template<typename... Args, std::size_t... S>
struct LogDecorationImpl<kgr::detail::meta_list<Args...>, std::index_sequence<S...>> : DecorationValue<S, Args>... {
	explicit LogDecorationImpl(Args const&... args) noexcept : DecorationValue<S, Args>{args}... {}
	
	inline friend auto operator<<(std::ostream& stream, LogDecorationImpl const& logInfo) -> std::ostream& {
		using expand_t = int[];

		// TODO: Use C++17 fold expression when MSVC supports them correctly
		(void) expand_t{((stream << logInfo.DecorationValue<S, Args>::value), 0)..., 0};
		return stream;
	}
};

template<typename... Args>
using LogDecoration = LogDecorationImpl<kgr::detail::meta_list<Args...>, std::make_index_sequence<sizeof...(Args)>>;

} // namespace sbg::detail

namespace sbg::log {
	
	template<typename Arg>
	constexpr auto unpad(Arg const& arg) {
		return detail::LogDecoration<std::decay_t<Arg const&>>{arg};
	}
	
	template<typename Arg>
	constexpr auto pad(Arg const& arg) {
		return detail::LogDecoration<std::decay_t<Arg const&>, char>{arg, ' '};
	}
	
	template<typename Arg>
	constexpr auto enquote(Arg const& arg) {
		return detail::LogDecoration<char, std::decay_t<Arg const&>, char, char>{'"', arg, '"', ' '};
	}
	
	template<typename Arg>
	constexpr auto point(Arg const& arg) {
		return detail::LogDecoration<char const*, std::decay_t<Arg const&>, char const*>{"--->", arg, "<--- "};
	}
	
} // namespace sbg::log
