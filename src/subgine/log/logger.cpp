#include "logger.h"

#include <thread>
#include <chrono>
#include <string_view>
#include <iomanip>

using namespace sbg;

static const std::chrono::time_point<std::chrono::system_clock> programBegin = std::chrono::system_clock::now();

auto detail::BaseLogger::lock() -> std::mutex& {
	static std::mutex m;
	return m;
}

auto detail::BaseLogger::level() -> int& {
	thread_local int l;
	return l;
}

static void log_time(std::ostream& stream) {
	auto elapsed = std::chrono::system_clock::now() - programBegin;
	stream
		<< '[' << std::setw(2) << std::setfill('0')
		<< std::chrono::duration_cast<std::chrono::minutes>(elapsed).count()
		<< ':' << std::setw(2) << std::setfill('0')
		<< std::chrono::duration_cast<std::chrono::seconds>(elapsed).count() % 60
		<< ':' << std::setw(3) << std::setfill('0')
		<< std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() % 1000
		<< "] ";
}

void detail::BaseLogger::log_level(std::ostream& stream, std::string_view level_string) {
	for (int i = 0 ; i < level() ; ++i) {
		stream << level_string;
	}
}

void detail::BaseLogger::log_start(std::ostream& stream, std::string_view level_string) {
	log_time(stream);
	log_level(stream, level_string);
}

void detail::BaseLogger::log_start_extended(std::ostream& stream, std::string_view level_string) {
	log_time(stream);
	stream << "l:" << level() << ";t:" << std::this_thread::get_id() << "; ";
	log_level(stream, level_string);
}

void detail::BaseLogger::log_flush(std::ostream& stream) {
	stream << '\n';
	stream << std::flush;
}
