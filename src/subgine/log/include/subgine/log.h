#pragma once

#include "log/service.h"

#include "../../utility/trait.h"

#include "../../log.h"
#include "../../logger.h"
#include "../../loginfo.h"
#include "../../logdecoration.h"
