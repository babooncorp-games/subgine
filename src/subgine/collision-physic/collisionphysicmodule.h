#pragma once

namespace sbg {

struct CollisionPhysicModuleService;

struct CollisionReactorCreator;
struct CollisionProfileCreator;

struct CollisionPhysicModule {
	void setupCollisionProfileCreator(CollisionProfileCreator& profileCreator) const;
	void setupCollisionReactorCreator(CollisionReactorCreator& reactorCreator) const;
	
	friend auto service_map(CollisionPhysicModule const&) -> CollisionPhysicModuleService;
};

} // namespace sbg
