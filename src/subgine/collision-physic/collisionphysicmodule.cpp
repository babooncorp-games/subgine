#include "collisionphysicmodule.h"

#include "subgine/collision-physic.h"
#include "subgine/collision.h"
#include "subgine/physic.h"
#include "subgine/entity.h"
#include "subgine/vector.h"
#include "subgine/shape.h"
#include "subgine/resource.h"

#include "subgine/common/kangaru.h"

using namespace sbg;

namespace {
	template<dim_t n>
	auto nSphereBuilder(CollisionReactorFactory collisionReactorFactory, Entity entity, Property data) -> NSphereAspect<n> {
		auto position = [&]() -> Provider<Vector<n, double>> {
			auto const physic = sbg::Component<PhysicPoint<n>>{entity};
			auto const offset = value_or(data["offset"], Vector<n, double>{});
			auto const physicBody = sbg::Component<PhysicBody<n>>{entity};
			return [physic, physicBody, offset] {
				return offset + (physicBody ? physicBody->getPosition() : physic ? physic->getPosition() : Vector<n, double>{});
			};
		}();
		
		return NSphereAspect<n>{
			double{data["radius"]},
			std::move(position)
		};
	}

	template<int n>
	auto alignedBoxBuilder(CollisionReactorFactory collisionReactorFactory, Entity entity, Property data) -> AlignedBoxAspect<n> {
		constexpr auto names = std::array{"width", "height", "depth"};
		auto const size = apply_sequence<n>([&](auto... s) { return Vector{double{data[names[s]]}...}; });
		
		auto position = [&]() -> Provider<Vector<n, double>> {
			auto const offset = value_or(data["offset"], Vector<n, double>{});
			auto const physic = sbg::Component<PhysicPoint<n>>{entity};
			return [physic, offset] {
				return offset + (physic ? physic->getPosition() : Vector<n, double>{});
			};
		}();
		
		return AlignedBoxAspect{
			shape::AlignedBox{-0.5 * size, 0.5 * size},
			std::move(position)
		};
	}

} // namespace

template<typename T>
auto multi_reaction(T reactor) {
	using SingleInfo = function_argument_t<0, T>;
	using MultiInfo = CollisionInfo<MultiManifold<SingleInfo::dimension>>;
	
	return [reactor = std::move(reactor)](MultiInfo info) {
		for (auto const& manifold : info) {
			reactor(CollisionInfo<typename SingleInfo::InfoType>{info, manifold});
		}
	};
}

void CollisionPhysicModule::setupCollisionProfileCreator(CollisionProfileCreator& profileCreator) const {
	profileCreator.add("circle", nSphereBuilder<2>);
	profileCreator.add("square", alignedBoxBuilder<2>);
	profileCreator.add("cube", alignedBoxBuilder<3>);
	profileCreator.add("sphere", nSphereBuilder<3>);
}

void CollisionPhysicModule::setupCollisionReactorCreator(CollisionReactorCreator& reactorCreator) const {
	reactorCreator.add("reciprocal2D", [](kgr::generator<ReciprocalReactor2DService> makeReactor, Entity entity, Property data) {
		return makeReactor(entity);
	});
	
	reactorCreator.add("static2D", [](kgr::generator<StaticReactor2DService> makeReactor, Entity entity, Property data) {
		return makeReactor(entity);
	});
	
	reactorCreator.add("reciprocal3D", [](kgr::generator<ReciprocalReactor3DService> makeReactor, Entity entity, Property data) {
		return makeReactor(entity);
	});
	
	reactorCreator.add("static3D", [](kgr::generator<StaticReactor3DService> makeReactor, Entity entity, Property data) {
		return makeReactor(entity);
	});
	
	reactorCreator.add("reciprocal2D-tilemap", [](kgr::generator<ReciprocalReactor2DService> makeReactor, Entity entity, Property data) {
		return multi_reaction(makeReactor(entity));
	});
	
	reactorCreator.add("static2D-tilemap", [](kgr::generator<StaticReactor2DService> makeReactor, Entity entity, Property data) {
		return multi_reaction(makeReactor(entity));
	});
	
	reactorCreator.add("reciprocal3D-tilemap", [](kgr::generator<ReciprocalReactor3DService> makeReactor, Entity entity, Property data) {
		return multi_reaction(makeReactor(entity));
	});
	
	reactorCreator.add("static3D-tilemap", [](kgr::generator<StaticReactor3DService> makeReactor, Entity entity, Property data) {
		return multi_reaction(makeReactor(entity));
	});
}
