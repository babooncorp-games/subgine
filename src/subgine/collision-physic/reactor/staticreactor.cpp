#include "staticreactor.h"

#include "../material.h"

#include "subgine/entity/component.h"

#include "subgine/physic.h"
#include "subgine/system.h"

#include "subgine/log.h"

namespace sbg {

template<int n>
void StaticReactor<n>::operator()(CollisionInfo<Manifold<n>> manifold) const {
// 	constexpr double threshold = 0.8;
// 	constexpr double factor = 0.9;
	constexpr double threshold = 0.0;
	constexpr double factor = 1;
	
	Log::error(
		[&]{ return _entity == manifold.other; },
		SBG_LOG_INFO, "The entity is colliding with itself"
	);
	
	auto const [physicPoint, physicBody] = [&]() -> std::pair<PhysicPoint<n>*, PhysicBody<n>*> {
		auto const physicPoint = Component<PhysicPoint<n>>{_entity};
		auto const physicBody = Component<PhysicBody<n>>{_entity};
		
		if (physicBody) {
			return std::pair{&*physicBody, &*physicBody};
		} else if (physicPoint) {
			return std::pair{&*physicPoint, nullptr};
		} else {
			// throw std::logic_error("collisionBody is not a physicPoint...");
			std::abort();
		}
	}();
	
// 	Material mat1{1, 1, 0.9, 0.08};
	Material mat2{0.05, 1, 0.9, 0.3};
	
	Material mat1{0.05, 1, 0.9, 0.3};
// 	Material mat2{0.3, 1, 0.7, 0.2};
	
	auto contact = manifold.contacts[0].position - physicPoint->getPosition();
	auto gap = manifold.contacts[0].penetration;
	auto normal = gap.unit();
	
	Vector<n, double> forceTotal;
	Vector<n, double> pulseTotal;
	for (auto& force : physicPoint->getForce()) {
		forceTotal += force.second;
	}
	for (auto& pulse : physicPoint->getPulse()) {
		pulseTotal += pulse.first != "collision" ? pulse.second : Vector<n, double>{};
	}
	
	auto relativeVelocity = physicPoint->getVelocity() + ((forceTotal/physicPoint->getMass()) * _mainEngine.time().next) + (pulseTotal / physicPoint->getMass());
	
	if (physicBody) {
		relativeVelocity += physicBody->getAngularVelocity().cross(contact);
	}
	
	double collisionVelocity = (relativeVelocity).dot(normal);
		
		
		if (collisionVelocity < 0) {
			return;
		}
		
		auto correction = std::max(gap.length() - threshold, 0.0) * normal;
		
		physicPoint->correctPosition("collision", -factor * correction);
	
	double j = (1.0+std::min(mat1.getRestitution(), mat2.getRestitution())) * collisionVelocity;
	j /= (1 / physicPoint->getMass());
	j *= std::min(std::max(std::cbrt(std::cbrt(gap.length())), 1.4) / 1.4, 6.0);
	{
		auto impulse =  -1 * j * normal;
		
		if (physicBody) {
			physicBody->accumulatePulse("collision", impulse, contact);
		} else {
			physicPoint->accumulatePulse("collision", impulse);
		}
	}
	
	{
		auto tangent = (relativeVelocity - relativeVelocity.dot(normal) * normal).unit();
		
		double jt = -1*(relativeVelocity.dot(tangent)) * physicPoint->getMass();
		
		double staticFrictionObject = mat1.getStaticFriction();
		double staticFrictionOther = mat2.getStaticFriction();
		
		double staticFriction = sqrt((staticFrictionObject*staticFrictionObject) + (staticFrictionOther*staticFrictionOther));
		
		Vector<n, double> friction;
		
		if (std::abs(jt) < j * staticFriction) {
			friction = jt * tangent;
		} else {
			double dynFrictionObject = mat1.getDynamicFriction();
			double dynFrictionOther = mat2.getDynamicFriction();
			
			friction = sqrt((dynFrictionObject*dynFrictionObject)+(dynFrictionOther*dynFrictionOther)) * jt * tangent;
		}
		
		if (physicBody) {
			physicBody->accumulatePulse("friction", friction, contact);
		} else {
// 			std::cout << friction << std::endl << std::endl;
			physicPoint->accumulatePulse("friction", friction);
		}
	}
}

template struct StaticReactor<2>;
template struct StaticReactor<3>;

} // namespace sbg
