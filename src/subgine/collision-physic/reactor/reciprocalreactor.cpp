#include "reciprocalreactor.h"

#include "../material.h"

#include "subgine/physic.h"
#include "subgine/system.h"

namespace sbg {

template<int n>
void ReciprocalReactor<n>::operator()(CollisionInfo<Manifold<n>> manifold) const {
	constexpr double threshold = 0.8;
	constexpr double factor = 0.9;
	
	Log::error(
		[&]{ return _entity == manifold.other; },
		SBG_LOG_INFO, "The entity is colliding with itself"
	);
	
// 	Material mat1{1, 1, 0.9, 0.08};
// 	Material mat2{1, 1, 0.9, 0.08};
	
	Material mat1{0, 1, 0.5, 0.2};
	Material mat2{0, 1, 0.5, 0.2};
	
	auto gap = manifold.contacts[0].penetration;
	auto normal = gap.unit();
	Entity other = manifold.other;
	
	if (!_entity.has<PhysicPoint<n>>() || !other.has<PhysicPoint<n>>()) {
		// throw std::logic_error("one or both of collisionBody are not physicPoint...");
		return;
	}
	
// 	auto&& collitionBodyFirst = entity->component<NCollisionBody>();
// 	auto&& collitionBodySecond = other->component<NCollisionBody>();
	
	auto& physicpoint1 = _entity.component<PhysicPoint<n>>();
	auto& physicpoint2 = other.component<PhysicPoint<n>>();
	
	auto contact = manifold.contacts[0].position - physicpoint1.getPosition();
	
	PhysicBody<n>* physicBody1 = nullptr;
	const PhysicBody<n>* physicBody2 = nullptr;
	
	if (_entity.has<PhysicBody2D>()) {
		physicBody1 = &_entity.component<PhysicBody<n>>();
	}
	
	if (other.has<PhysicBody2D>()) {
		physicBody2 = &other.component<PhysicBody<n>>();
	}
	
	auto velocity1 = physicpoint1.getVelocity();
	auto velocity2 = physicpoint2.getVelocity();
	
	Vector<n, double> forceTotal;
	Vector<n, double> pulseTotal;
	
	for (auto& force : physicpoint1.getForce()) {
		forceTotal += force.second;
	}
	
	for (auto& pulse : physicpoint1.getPulse()) {
		pulseTotal += pulse.first != "collision" ? pulse.second : Vector<n, double>{};
	}
	
	velocity1 += (forceTotal / physicpoint1.getMass()) * _mainEngine.time().next;
	velocity1 += pulseTotal / physicpoint1.getMass();
	
	if (physicBody1) {
		velocity1 += physicBody1->getAngularVelocity().cross(contact);
	}
	
	if (physicBody2) {
		velocity2 += physicBody2->getAngularVelocity().cross(contact + physicpoint1.getPosition() - physicBody2->getPosition());
	}
	
	auto relativeVelocity = velocity2 - velocity1;
	auto collisionVelocity = (relativeVelocity).dot(normal);
	
	if (collisionVelocity > 0) {
		return;
	}
	
	auto correction = std::max(gap.length() - threshold, 0.0) * normal;
	
	physicpoint1.correctPosition("collision", -factor *(correction * (physicpoint2.getMass()/(physicpoint1.getMass()+physicpoint2.getMass()))));
	
	double j = -(1 + std::min(mat1.getRestitution(), mat2.getRestitution())) * collisionVelocity;
	j /= (1/physicpoint1.getMass()) + (1/physicpoint2.getMass());
	j *= std::min(std::max(std::cbrt(std::cbrt(gap.length())), 1.5) / 1.5, 6.0);
	
	{
		auto impulse = j * normal * -1;
		
		if (physicBody1) {
			physicBody1->accumulatePulse("collision", impulse, contact);
		} else {
			physicpoint1.accumulatePulse("collision", impulse);
		}
	}
	
	{
		auto tangent = (relativeVelocity - relativeVelocity.dot(normal) * normal).unit();
		
		double jt = (relativeVelocity.dot(tangent));
		jt /= (1/physicpoint1.getMass()) + (1/physicpoint2.getMass());
		
		double staticFrictionObject = mat1.getStaticFriction();
		double staticFrictionOther = mat2.getStaticFriction();
		
		double staticFriction = sqrt((staticFrictionObject*staticFrictionObject) + (staticFrictionOther*staticFrictionOther));
		
		Vector<n, double> friction;
		
		if (std::abs(jt) < j * staticFriction) {
			friction = jt * tangent;
		} else {
			double dynFrictionObject = mat1.getDynamicFriction();
			double dynFrictionOther = mat2.getDynamicFriction();
			
			friction = sqrt((dynFrictionObject*dynFrictionObject)+(dynFrictionOther*dynFrictionOther)) * jt * tangent;
		}
		
		if (physicBody1) {
			physicBody1->accumulatePulse("friction", friction, contact);
		} else {
			physicpoint1.accumulatePulse("friction", friction);
		}
	}
}

template struct ReciprocalReactor<2>;
template struct ReciprocalReactor<3>;

} // namespace sbg
