#pragma once

#include "../manifold.h"
#include "../collisioninfo.h"

#include "subgine/entity/entity.h"

namespace sbg {

template<dim_t n>
struct StaticReactorService;

struct MainEngine;

template<dim_t n>
struct StaticReactor {
	explicit StaticReactor(MainEngine& mainEngine, Entity entity) : _mainEngine{mainEngine}, _entity{entity} {}
	
	void operator()(CollisionInfo<Manifold<n>> manifold) const;
	
private:
	MainEngine& _mainEngine;
	Entity _entity;
	
	friend auto service_map(StaticReactor const&) -> StaticReactorService<n>;
};

extern template struct StaticReactor<2>;
extern template struct StaticReactor<3>;

using StaticReactor2D = StaticReactor<2>;
using StaticReactor3D = StaticReactor<3>;

} // namespace sbg
