#pragma once

#include "../manifold.h"
#include "../collisioninfo.h"

#include "subgine/entity/entity.h"

namespace sbg {

template<dim_t n>
struct ReciprocalReactorService;

struct MainEngine;

template<dim_t n>
struct ReciprocalReactor {
	explicit ReciprocalReactor(MainEngine& mainEngine, Entity entity) : _mainEngine{mainEngine}, _entity{entity} {}
	
	void operator()(CollisionInfo<Manifold<n>> manifold) const;
	
private:
	MainEngine& _mainEngine;
	Entity _entity;
	
	friend auto service_map(ReciprocalReactor const&) -> ReciprocalReactorService<n>;
};

extern template struct ReciprocalReactor<2>;
extern template struct ReciprocalReactor<3>;

using ReciprocalReactor2D = ReciprocalReactor<2>;
using ReciprocalReactor3D = ReciprocalReactor<3>;

} // namespace sbg
