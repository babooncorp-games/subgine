#pragma once

#include "../collisionphysicmodule.h"

#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct CollisionPhysicModuleService : kgr::single_service<CollisionPhysicModule>,
	autocall<
		&CollisionPhysicModule::setupCollisionProfileCreator,
		&CollisionPhysicModule::setupCollisionReactorCreator
	> {};

} // namespace sbg

