#pragma once

#include "../reactor/reciprocalreactor.h"

#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

template<dim_t n>
struct ReciprocalReactorService : kgr::service<ReciprocalReactor<n>, kgr::autowire> {};

using ReciprocalReactor2DService = ReciprocalReactorService<2>;
using ReciprocalReactor3DService = ReciprocalReactorService<3>;

} // namespace sbg
