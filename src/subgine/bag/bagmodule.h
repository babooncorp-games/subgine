#pragma once

namespace sbg {

struct BagModuleService;

struct CollisionReactorCreator;
struct ComponentCreator;

struct BagModule {
	void setupCollisionReactorCreator(CollisionReactorCreator& collisionReactorCreator);
	void setupComponentCreator(ComponentCreator& componentCreator);
	
	friend auto service_map(BagModule const&) -> BagModuleService;
};

} // namespace sbg
