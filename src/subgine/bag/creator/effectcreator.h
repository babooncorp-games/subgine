#pragma once

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

#include "../effect/effect.h"

namespace sbg {

struct EffectCreatorService;

struct EffectCreator : Creator<Effect(Property)> {
	using Creator::create;
	
	friend auto service_map(EffectCreator const&) -> EffectCreatorService;
};

using EffectFactory = Factory<EffectCreator>;

} // namespace sbg
