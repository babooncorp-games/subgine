#pragma once

#include <string>

#include "../component/item.h"
#include "../creator/effectcreator.h"

namespace sbg {

struct ItemFactoryService;
struct GameDatabase;

struct ItemFactory {
	ItemFactory(GameDatabase& database, EffectFactory effectFactory);
	
	Item create(std::string_view name);
	
private:
	GameDatabase& _database;
	EffectFactory _effectFactory;
	
	friend auto service_map(ItemFactory const&) -> ItemFactoryService;
};

} // namespace sbg
