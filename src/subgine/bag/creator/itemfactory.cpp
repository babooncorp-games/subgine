#include "itemfactory.h"

#include "subgine/resource.h"

namespace sbg {

ItemFactory::ItemFactory(GameDatabase& database, EffectFactory effectFactory) : _database{database}, _effectFactory{std::move(effectFactory)} {}

Item ItemFactory::create(std::string_view name) {
	auto data = _database.get("item", name);
	Item item;
	
	for (auto const effect : data["effects"]) {
		item.addEffect(_effectFactory.create(effect["type"], effect["data"]));
	}
	
	return item;
}

} // namespace sbg
