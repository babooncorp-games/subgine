#pragma once

#include "abstracteffect.h"

namespace sbg {

template<typename E>
struct ConcreteEffect : AbstractEffect {
	ConcreteEffect(E effect) : _effect{std::move(effect)} {}
	
	bool apply(Entity target) override {
		return _effect.apply(target);
	}
	
private:
	E _effect;
};

} // namespace sbg
