#pragma once

#include "subgine/entity/entity.h"

namespace sbg {

struct AbstractEffect {
	virtual ~AbstractEffect() = default;
	virtual bool apply(Entity target) = 0;
};

} // namespace sbg
