#pragma once

#include "subgine/common/types.h"
#include "../detail/concreteeffect.h"

namespace sbg {

struct Effect {
	template<typename E>
	Effect(E effect) : _effect{std::make_unique<ConcreteEffect<E>>(std::move(effect))} {}
	
	bool apply(Entity target);
	
private:
	std::unique_ptr<AbstractEffect> _effect;
};

} // namespace sbg
