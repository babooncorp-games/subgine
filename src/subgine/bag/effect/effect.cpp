#include "effect.h"

namespace sbg {

bool Effect::apply(Entity target) {
	return _effect->apply(target);
}

} // namespace sbg
