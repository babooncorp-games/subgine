#pragma once

#include "subgine/common/kangaru.h"

#include "../creator/itemfactory.h"

#include "subgine/resource/service/gamedatabaseservice.h"
#include "effectcreatorservice.h"

namespace sbg {

struct ItemFactoryService : kgr::service<ItemFactory, kgr::autowire> {};

} // namespace sbg
