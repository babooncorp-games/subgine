#pragma once

#include "../creator/effectcreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EffectCreatorService : kgr::single_service<EffectCreator> {};
using EffectFactoryService = FactoryService<EffectFactory>;

} // namespace sbg
