#pragma once

#include "../bagmodule.h"

#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BagModuleService : kgr::single_service<BagModule>, autocall<
	&BagModule::setupCollisionReactorCreator,
	&BagModule::setupComponentCreator
> {};

} // namespace sbg
