#include "bagmodule.h"

#include "service/itemfactoryservice.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/system.h"

namespace sbg {

void BagModule::setupCollisionReactorCreator(CollisionReactorCreator& collisionReactorCreator) {
	collisionReactorCreator.add("pickup", [](DeferredScheduler& scheduler, Entity entity, Property data) {
		return [entity, &scheduler](BasicCollisionInfo info) {
			if (entity.has<Item>()) {
				entity.component<Item>().use(info.other);
				scheduler.defer([entity] { entity.destroy(); });
			}
		};
	});
}

void BagModule::setupComponentCreator(ComponentCreator& componentCreator) {
	componentCreator.add("item", [](ItemFactory itemFactory, Entity entity, Property data){
		return itemFactory.create(data["name"]);
	});
}

} // namespace sbg
