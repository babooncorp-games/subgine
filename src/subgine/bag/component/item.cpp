#include "item.h"

namespace sbg {

bool Item::use(Entity target) {
	bool success = true;
	
	for (auto& effect : _effects) {
		success = success && effect.apply(target);
	}
	
	return success;
}

void Item::addEffect(Effect effect) {
	_effects.push_back(std::move(effect));
}

} // namespace sbg
