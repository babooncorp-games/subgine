#pragma once

#include <vector>

#include "../effect/effect.h"

namespace sbg {

struct Item {
	bool use(Entity target);
	
	void addEffect(Effect effect);
	
private:
	std::vector<Effect> _effects;
};

} // namespace sbg
