#pragma once

#include "bag/service.h"

#include "../../component/item.h"
#include "../../creator/effectcreator.h"
#include "../../creator/itemfactory.h"
#include "../../detail/abstracteffect.h"
#include "../../detail/concreteeffect.h"
#include "../../effect/effect.h"
