#include "entitymodule.h"

#include "subgine/entity.h"

namespace sbg {

void EntityModule::setupComponentCreator(ComponentCreator& ef) const {
	//ef.add("timed", [](Entity entity, Property data){
	//	// TODO: Implement a timed object
	//});
}

void EntityModule::setupContainer(kgr::container& container) const {
	container.emplace<EntityManagerService>();
}

} // namespace sbg
