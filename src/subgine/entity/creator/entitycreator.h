#pragma once

#include "../entity.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/common/concepts.h"
#include "kangaru/kangaru.hpp"

#include <string_view>

namespace sbg {

struct EntityCreatorService;

struct ComponentCreator;
struct EntityBindingCreator;
struct EntityManager;
struct GameDatabase;

struct EntityCreator {
	EntityCreator(
		ComponentCreator& componentCreator,
		EntityBindingCreator& entityBindingCreator,
		EntityManager& entityManager
	) noexcept;
	
	/**
	 * This function makes the entity.
	 * name is the name attached to the entity type
	 * entity is sent to start the making of the entity with an existing one.
	 * It will send the entity to engines and views to make the game knowing about this entity so it can be processed.
	 */
	Entity create(kgr::invoker invoker, std::string_view name, Entity entity) const;
	
	auto create(kgr::invoker invoker, std::string_view name, different_from<Entity> auto... components) const {
		auto const entity = _entityManager->create();
		
		((entity.assign(std::move(components))), ...);
		
		return create(invoker, name, entity);
	}
	
private:
	ComponentCreator* _componentCreator;
	EntityBindingCreator* _entityBindingCreator;
	EntityManager* _entityManager;
	
	friend auto service_map(EntityCreator const&) -> EntityCreatorService;
};

using EntityFactory = Factory<EntityCreator>;

} // namespace sbg
