#pragma once

#include "../entity.h"
#include "../entityassignapply.h"

#include "subgine/common/types.h"
#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

#include <memory>

namespace sbg {

struct ComponentCreatorService;

/**
 * This class make an entity of a particular type.
 * 
 * Builder signature:
 * @code
 * <component>(Entity, Property) or void(Entity, Property)
 * @endcode
 * 
 * When returning void, the builder is expected to assign a component to the entity.
 */
struct ComponentCreator : Creator<VoidBuilder, EntityAssignApply> {
	/*
	 * This function makes the entity.
	 * name is the name attached to the entity type
	 * entity is sent to start the making of the entity with an existing one.
	 */
	void create(kgr::invoker invoker, Property data, Entity entity = {}) const;
	
	friend auto service_map(ComponentCreator const&) -> ComponentCreatorService;
};

using ComponentFactory = Factory<ComponentCreator>;

} // namespace sbg
