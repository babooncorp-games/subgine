#pragma once

#include "../entity.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

struct EntityBindingCreatorService;

struct ComponentCreator;
struct JsonManager;

/**
 * This class make an entity of a particular type.
 * It is very similar to the EntityCreator, but it dispatches the entity to where it belongs (the view, physic engine, collision engine and more).
 * It reads a json file to associate strings to an entity type.
 */
struct EntityBindingCreator : Creator<VoidBuilder> {
	void create(kgr::invoker invoker, Property data, Entity entity) const;
	
	friend auto service_map(EntityBindingCreator const&) -> EntityBindingCreatorService;
};

using EntityBindingFactory = Factory<EntityBindingCreator>;

} // namespace sbg
