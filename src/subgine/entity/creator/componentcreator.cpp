#include "componentcreator.h"

#include "../entitymanager.h"
#include "subgine/log.h"

namespace sbg {

void ComponentCreator::create(kgr::invoker invoker, Property data, Entity entity) const {
	for (auto const [type, data] : data.items()) {
		auto l = Log::trace(SBG_LOG_INFO, "Creating component of type", log::enquote(type));
		build(invoker, type, entity, data);
	}
}

} // namespace sbg
