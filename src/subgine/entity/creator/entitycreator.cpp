#include "entitycreator.h"

#include "componentcreator.h"
#include "entitybindingcreator.h"
#include "../entitymanager.h"

#include "subgine/scene/scene.h"
#include "subgine/scene/service/currentsceneservice.h"
#include "subgine/resource.h"

using namespace sbg;

EntityCreator::EntityCreator(
	ComponentCreator& componentCreator,
	EntityBindingCreator& entityBindingCreator,
	EntityManager& entityManager
) noexcept :
	_componentCreator{&componentCreator},
	_entityBindingCreator{&entityBindingCreator},
	_entityManager{&entityManager}
{}

auto EntityCreator::create(kgr::invoker invoker, std::string_view name, Entity entity) const -> Entity {
	auto l = Log::trace(SBG_LOG_INFO, "Creating bound entity", log::enquote(name));

	Log::trace(SBG_LOG_INFO, "Entity ID", entity.id().index);
	
	auto const& data = invoker([&](GameDatabase const& database) {
		return database.get("entity", name);
	});
	
	_componentCreator->create(invoker, data["components"], entity);
	_entityBindingCreator->create(invoker, data["bindings"], entity);
	
	invoker([&entity](Scene& scene) {
		// TODO: Break circular dependency
		scene.attach(entity);
	});
	
	return entity;
}
