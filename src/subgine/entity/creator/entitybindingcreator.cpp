#include "entitybindingcreator.h"

#include "componentcreator.h"
#include "../entitymanager.h"

#include "subgine/resource.h"
#include "subgine/log.h"

namespace sbg {

void EntityBindingCreator::create(kgr::invoker invoker, Property data, Entity entity) const {
	for (auto const bind : data) {
		auto const type = std::string_view{bind["type"]};
		auto l = Log::trace(SBG_LOG_INFO, "Binding entity to", type);
		build(invoker, type, entity, bind["data"]);
	}
}

} // namespace sbg
