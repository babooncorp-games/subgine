#pragma once

#include "entity.h"

#include "subgine/common/dropping_invoke.h"

#include <type_traits>

namespace sbg {

struct EntityAssignApply {
	template<typename I, typename B, typename... Args, enable_if_i<is_dropping_invocable_v<I, B&, Entity, Args...>> = 0>
	void operator()(I invoker, B& builder, Entity entity, Args&&... args) const {
		if constexpr (!std::is_void_v<dropping_invoke_result_t<I, B&, Entity, Args...>>) {
			entity.assign(dropping_invoke(invoker, builder, entity, std::forward<Args>(args)...));
		} else {
			dropping_invoke(invoker, builder, entity, std::forward<Args>(args)...);
		}
	}
};

} // namesapce sbg
