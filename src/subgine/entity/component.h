#pragma once

#include "entity.h"

#include <kangaru/type_id.hpp>
#include <memory>

namespace sbg {

template<typename T>
struct Component {
	constexpr Component() = default;
	
	constexpr explicit Component(EntityManager::Storage* storage, BasicEntity entity) noexcept :
		_component{entity.definitely_dead() ? nullptr : storage->at_allocating<T>(entity.id().index)}, _storage{storage}, _entity{entity} {}
	
	explicit Component(Entity entity) noexcept : _entity{entity} {
		if (entity._manager and not entity.definitely_dead()) {
			auto& storage = entity._manager->storage<T>();
			_component = storage.template at_allocating<T>(entity.id().index);
			_storage = &storage;
		}
	}
	
	Component(Component const&) = default;
	Component(Component&&) = default;
	Component& operator=(Component const&) = default;
	Component& operator=(Component&&) = default;
	
	friend auto operator==(Component const&, Component const&) -> bool = default;
	friend auto operator<=>(Component const&, Component const&) = default;
	
	auto get() const noexcept -> T& {
		auto const type_name = kgr::detail::type_name<T>();
		Log::fatal(
			[&]{ return !valid(); },
			SBG_LOG_INFO, "Component of type", log::enquote(std::string_view{type_name.begin(), type_name.size()}), "is invalid. The entity is either dead or missing the component"
		);
		return *_component;
	}
	
	auto ptr() const noexcept -> T* {
		return &get();
	}
	
	auto unchecked_ptr() const noexcept -> T* {
		return _component;
	}
	
	auto operator*() const -> T& {
		return get();
	}
	
	auto operator->() const -> T* {
		return ptr();
	}
	
	bool valid() const noexcept {
		if (_storage and _component) {
			auto const id = _entity.id();
			auto const& generations = _storage->storage_generations();
			return
				not _entity.definitely_dead()
				and generations.size() > id.index
				and generations[id.index] == id.generation;
		} else {
			return false;
		}
	}
	
	void destroy() const noexcept {
		_storage->remove(_entity);
	}
	
	explicit operator bool() const noexcept {
		return valid();
	}
	
private:
	T* _component = nullptr;
	EntityManager::Storage* _storage = nullptr;
	BasicEntity _entity;
};

} // namespace sbg
