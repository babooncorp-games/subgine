#include "entitymanager.h"

#include "entity.h"

#include <algorithm>
#include <functional>
#include <utility>

namespace sbg {

auto EntityManager::create() -> Entity {
	auto const [generation, id] = std::invoke([&] {
		if (_free != no_index) {
			auto const id = std::exchange(_free, std::uint32_t{_generation[_free].index});
			return std::pair{static_cast<std::uint8_t>(_generation[id].generation), id};
		} else if (_generation.size() != Entity::Id::Limits::index - 1) {
			_generation.emplace_back(BasicEntity::Id{.generation = 0, .index = no_index});
			auto const id = static_cast<std::uint32_t>(_generation.size()) - 1;
			return std::pair{static_cast<std::uint8_t>(_generation[id].generation), id};
		} else {
			Log::fatal(SBG_LOG_INFO, "Entity count has overflowed the maximum");
		}
	});
	
	return Entity{this, Entity::Id{generation, id}};
}

void EntityManager::destroy(BasicEntity entity) {
	const auto index = entity.id().index;
	
	if (_generation[index].generation == entity.id().generation) {
		auto& id = _generation[index];
		++id.generation;
		id.generation *= static_cast<int>(id.generation != 255);
		id.index = std::exchange(_free, index);
	}

	for (auto& [type, storage] : _components) {
		storage.remove(entity);
	}
}

bool EntityManager::alive(BasicEntity entity) const {
	auto const id = entity.id();
	return not entity.definitely_dead() and _generation[id.index].generation == id.generation;
}

} // namespace sbg
