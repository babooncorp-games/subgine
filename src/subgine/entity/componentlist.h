#pragma once

#include "component.h"
#include "entity.h"
#include "entitymanager.h"

#include <array>
#include <tuple>
#include <vector>
#include <iterator>

namespace sbg {

template<typename... Types>
struct ComponentList {
	template<typename Underlying, typename List>
	struct BasicIterator {
		using iterator_category = std::random_access_iterator_tag;
		using value_type = std::tuple<Component<Types>...>;
		using reference = std::add_lvalue_reference_t<value_type>;
		using pointer = std::add_pointer_t<value_type>;
		using difference_type = std::ptrdiff_t;
		
		BasicIterator(Underlying it, List list) noexcept : _it{it}, _list{list} {};
		
		auto operator*() const -> value_type {
			return apply_sequence_for<Types...>([&](auto... s) {
				return std::tuple{Component<Types>{_list->_storages[s], *_it}...};
			});
		}
		
		auto operator++() -> BasicIterator& {
			++_it;
			return *this;
		}
		
		auto operator++(int) -> BasicIterator {
			return Iterator{_it++, _list};
		}
		
		auto operator--() -> BasicIterator {
			--_it;
			return *this;
		}
		
		auto operator--(int) -> BasicIterator {
			return BasicIterator{_it--, _list};
		}
		
		friend auto operator+(BasicIterator const& lhs, std::ptrdiff_t n) -> BasicIterator {
			return BasicIterator{lhs._it + n, lhs._list};
		}
		
		friend auto operator+(std::ptrdiff_t n, BasicIterator const& rhs) -> BasicIterator {
			return BasicIterator{rhs._it + n, rhs._list};
		}
		
		friend auto operator-(std::ptrdiff_t n, BasicIterator const& rhs) -> BasicIterator {
			return BasicIterator{rhs._it - n, rhs._list};
		}
		
		friend auto operator-(BasicIterator const& lhs, std::ptrdiff_t n) -> BasicIterator {
			return BasicIterator{lhs._it - n, lhs._list};
		}
		
		friend auto operator-(BasicIterator const& lhs, BasicIterator const& rhs) -> difference_type {
			return lhs._it - rhs._it;
		}
		
		friend auto operator!=(BasicIterator const& lhs, BasicIterator const& rhs) -> bool {
			return lhs._it != rhs._it;
		}
		
		friend auto operator==(BasicIterator const& lhs, BasicIterator const& rhs) {
			return lhs._it == rhs._it;
		}
		
	private:
		friend ComponentList;
		
		Underlying _it;
		List _list;
	};
	
	using Iterator = BasicIterator<std::vector<BasicEntity>::iterator, ComponentList*>;
	using ConstIterator = BasicIterator<std::vector<BasicEntity>::const_iterator, ComponentList const*>;

	auto begin() -> Iterator {
		return Iterator{_entities.begin(), this};
	}

	auto end() -> Iterator {
		return Iterator{_entities.end(), this};
	}
	
	auto begin() const -> ConstIterator {
		return ConstIterator{_entities.begin(), this};
	}
	
	auto end() const -> ConstIterator {
		return ConstIterator{_entities.end(), this};
	}
	
	auto cbegin() const -> ConstIterator {
		return ConstIterator{_entities.cbegin(), this};
	}
	
	auto cend() const -> ConstIterator {
		return ConstIterator{_entities.cend(), this};
	}
	
	auto emplace(Entity entity) -> std::tuple<Component<Types>...> {
		if (!_manager) {
			_manager = entity._manager;
			_storages = {&_manager->storage<Types>()...};
		}
		_entities.emplace_back(entity);
		return apply_sequence_for<Types...>([&](auto... s) {
			return std::tuple{Component<Types>{_storages[s], entity}...};
		});
	}

	auto erase(Iterator it) -> Iterator {
		return Iterator{_entities.erase(it._it), this};
	}

	auto erase(Iterator first, Iterator last) -> Iterator {
		return Iterator{_entities.erase(first._it, last._it), this};
	}

	[[nodiscard]]
	auto empty() const noexcept -> bool {
		return _entities.empty();
	}

	auto size() const noexcept -> std::size_t {
		return _entities.size();
	}

	void clear() noexcept {
		_entities.clear();
	}

	void cleanup() {
		_entities.erase(
			std::remove_if(
				_entities.begin(), _entities.end(),
				[&](BasicEntity entity) { return !_manager->alive(entity); }
			),
			_entities.end()
		);
	}
	
private:
	EntityManager* _manager = nullptr;
	std::array<EntityManager::Storage*, sizeof...(Types)> _storages = {};
	std::vector<BasicEntity> _entities;
};

} // namespace sbg
