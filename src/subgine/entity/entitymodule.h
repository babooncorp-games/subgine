#pragma once

#include "subgine/common/kangaru.h"

namespace sbg {

struct EntityModuleService;

struct MainEngine;
struct ComponentCreator;

/**
 * This class is the module class for Entity module.
 */
struct EntityModule {
	void setupComponentCreator(ComponentCreator& ef) const;
	void setupContainer(kgr::container&) const;
	
	friend auto service_map(EntityModule const&) -> EntityModuleService;
};

} // namespace sbg
