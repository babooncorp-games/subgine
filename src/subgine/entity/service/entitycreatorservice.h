#pragma once

#include "../creator/entitycreator.h"

#include "entitybindingcreatorservice.h"
#include "componentcreatorservice.h"
#include "entitymanagerservice.h"

#include "subgine/resource/service/gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EntityCreatorService : kgr::single_service<EntityCreator, kgr::autowire> {};
using EntityFactoryService = FactoryService<EntityFactory>;

} // namespace sbg
