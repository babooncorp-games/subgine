#pragma once

#include "../entitymodule.h"

#include "entitybindingcreatorservice.h"
#include "componentcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EntityModuleService : kgr::single_service<EntityModule>,
	autocall<
		&EntityModule::setupComponentCreator,
		&EntityModule::setupContainer
	> {};

} // namespace sbg
