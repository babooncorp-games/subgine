#pragma once

#include "../creator/componentcreator.h"

#include "entitymanagerservice.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ComponentCreatorService : kgr::single_service<ComponentCreator>{};
using ComponentFactoryService = FactoryService<ComponentFactory>;

} // namespace sbg
