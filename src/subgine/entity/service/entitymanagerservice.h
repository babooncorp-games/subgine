#pragma once

#include "../entitymanager.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EntityManagerService : kgr::single_service<EntityManager> {};

} // namespace sbg
