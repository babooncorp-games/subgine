#pragma once

#include "../creator/entitybindingcreator.h"

#include "componentcreatorservice.h"
#include "entitymanagerservice.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EntityBindingCreatorService : kgr::single_service<EntityBindingCreator> {};
using EntityBindingFactoryService = FactoryService<EntityBindingFactory>;

} // namespace sbg
