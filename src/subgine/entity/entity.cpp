#include "entity.h"

#include "entitymanager.h"

namespace sbg {

bool Entity::alive() const {
	return not definitely_dead() and _manager and _manager->alive(*this);
}

void Entity::destroy() const {
	if (alive()) {
		_manager->destroy(*this);
	}
}

Entity::operator bool() const {
	return alive();
}

} // namespace sbg
