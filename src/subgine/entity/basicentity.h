#pragma once

#include "subgine/common/hash.h"

#include <cstdint>
#include <functional>

namespace sbg {

struct BasicEntity {
	friend struct std::hash<BasicEntity>;
	friend struct murmur_hash<BasicEntity>;
	
	struct Id {
		struct Limits {
			static constexpr auto generation = static_cast<std::uint32_t>((1ull << 8ull) - 1);
			static constexpr auto index = static_cast<std::uint32_t>((1ull << 24ull) - 1);
		} static constexpr limits{};
		
		std::uint32_t generation : 8 = limits.generation;
		std::uint32_t index : 24 = limits.index;
		
		constexpr auto value() const noexcept -> std::uint32_t {
			return generation | index << 8;
		}

		static constexpr auto from_value(std::uint32_t value) noexcept -> Id {
			return Id{
				.generation = value & limits.generation,
				.index = value >> 8 & limits.index,
			};
		}
		
		friend constexpr auto operator==(Id const& lhs, Id const& rhs) noexcept -> bool {
			return lhs.value() == rhs.value();
		}
		
		friend constexpr auto operator<=>(Id const& lhs, Id const& rhs) noexcept {
			return lhs.value() <=> rhs.value();
		}
	};
	
	friend constexpr auto operator==(BasicEntity const& lhs, BasicEntity const& rhs) noexcept -> bool {
		return lhs._id == rhs._id;
	}
	
	friend constexpr auto operator<=>(BasicEntity const& lhs, BasicEntity const& rhs) noexcept {
		return lhs._id <=> rhs._id;
	}
	
	constexpr BasicEntity() = default;
	explicit constexpr BasicEntity(Id id) noexcept : _id{id} {}
	
	constexpr auto id() const noexcept -> Id const& {
		return _id;
	}
	
	constexpr auto definitely_dead() const noexcept -> bool {
		return _id.value() == ~0u;
	}
	
private:
	Id _id = {};
};

template<>
struct murmur_hash<BasicEntity> {
	auto operator()(BasicEntity const& entity) const noexcept -> std::size_t {
		return murmur_hash<std::uint32_t>{}(entity._id.value());
	}
};

template<>
struct murmur_hash<sbg::BasicEntity::Id> {
	auto operator()(BasicEntity::Id const& id) const noexcept -> std::size_t {
		return murmur_hash<std::uint32_t>{}(id.value());
	}
};


} // namespace sbg

template<>
struct std::hash<sbg::BasicEntity> {
	auto operator()(sbg::BasicEntity const& entity) const -> size_t {
		return hash<std::uint32_t>{}(entity._id.value());
	}
};

template<>
struct std::hash<sbg::BasicEntity::Id> {
	auto operator()(sbg::BasicEntity::Id const& id) const -> size_t {
		return hash<std::uint32_t>{}(id.value());
	}
};
