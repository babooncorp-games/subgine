#pragma once

#include "basicentity.h"
#include "entitymanager.h"

#include "subgine/common/traits.h"
#include "subgine/common/pairhash.h"
#include "subgine/common/types.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include <functional>
#include <compare>

namespace sbg {

struct Entity : BasicEntity {
	constexpr Entity() = default;
	explicit constexpr Entity(EntityManager* manager, Id id) noexcept : BasicEntity{id}, _manager{manager} {}
	
	friend struct std::hash<Entity>;
	friend struct murmur_hash<Entity>;
	template<typename> friend struct Component;
	template<typename...> friend struct ComponentList;
	
	friend auto operator==(Entity const&, Entity const&) -> bool = default;
	friend auto operator<=>(Entity const&, Entity const&) = default;
	
	explicit operator bool() const;
	
	template<typename T>
	auto component() const -> T& {
		Log::fatal([&]{ return !alive(); }, SBG_LOG_INFO, "Access to a dead entity");
		
		return _manager->component<T>(*this);
	}
	
	template<typename T>
	bool has() const {
		Log::fatal([&]{ return !alive(); }, SBG_LOG_INFO, "Access to a dead entity");
		
		return _manager->has<T>(*this);
	}
	
	template<typename T>
	void assign(T&& component) const {
		Log::fatal([&]{ return !alive(); }, SBG_LOG_INFO, "Access to a dead entity");
		
		_manager->assign<std::decay_t<T>>(*this, std::forward<T>(component));
	}
	
	template<typename T>
	void remove() const {
		Log::fatal([&]{ return !alive(); }, SBG_LOG_INFO, "Access to a dead entity");
		
		_manager->remove<T>(*this);
	}
	
	template<typename T, typename... Args>
	void assign(Args&&... args) const {
		Log::fatal([&]{ return !alive(); }, SBG_LOG_INFO, "Access to a dead entity");
		
		_manager->assign<T>(*this, std::forward<Args>(args)...);
	}
	
	bool alive() const;
	void destroy() const;
	
private:
	EntityManager* _manager = nullptr;
};

template<>
struct murmur_hash<Entity> {
	auto operator()(Entity const& entity) const noexcept -> std::size_t {
		auto seed = std::size_t{0};
		
		murmur_hash_combine(seed, entity._manager);
		murmur_hash_combine(seed, static_cast<BasicEntity const&>(entity));
		
		return seed;
	}
};

} // namespace sbg

template<>
struct std::hash<sbg::Entity> {
	auto operator()(sbg::Entity const& entity) const noexcept -> std::size_t {
		std::size_t seed = 0;
		
		sbg::hash_combine(seed, entity._manager);
		sbg::hash_combine(seed, static_cast<sbg::BasicEntity const&>(entity));
		
		return seed;
	}
};
