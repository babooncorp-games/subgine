#include "catch2/catch.hpp"

#include "../component.h"
#include "../entity.h"
#include "../entitymanager.h"
#include "../service/entitymanagerservice.h"

#include "subgine/common/kangaru.h"

using namespace sbg;

TEST_CASE("Component Empty", "[entity]") {
	struct Data {};
	
	Component<Data> component;
	
	REQUIRE(!component);
	REQUIRE(!component.valid());
}

TEST_CASE("Component Alive/Retreive/Remove", "[entity]") {
	kgr::container container;
	auto& manager = container.service<EntityManagerService>();
	
	struct Data {
		int id;
	};
	
	Entity entity = manager.create();
	
	entity.assign(Data{12});
	CHECK(entity.has<Data>());
	CHECK(entity.component<Data>().id == 12);
	
	auto component = Component<Data>{entity};
	
	SECTION("Alive") {
		REQUIRE(component);
		REQUIRE(component.valid());
		REQUIRE(entity.has<Data>());
	}
	
	SECTION("Retreive") {
		REQUIRE(&*component == &entity.component<Data>());
	}
	
	SECTION("Remove") {
		CHECK(component);
		
		component.destroy();
		
		REQUIRE(!component);
		REQUIRE(!entity.has<Data>());
	}
	
	SECTION("Remove From Entity") {
		CHECK(component);
		
		entity.remove<Data>();
		
		REQUIRE(!component);
		REQUIRE(!entity.has<Data>());
	}
	
	SECTION("Dead") {
		CHECK(component);
		
		entity.destroy();
		
		REQUIRE(!component);
	}
}
