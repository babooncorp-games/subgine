#include "catch2/catch.hpp"

#include "../entity.h"
#include "../entitymanager.h"
#include "../service/entitymanagerservice.h"

#include "subgine/common/kangaru.h"

using namespace sbg;

TEST_CASE("Entity manager manages entity", "[entity]") {
	kgr::container container;
	auto& manager = container.service<EntityManagerService>();

	SECTION("Can convert entity ids") {
		auto id = BasicEntity::Id{};
		id.index = 1;
		id.generation = 2;

		REQUIRE(id.value() == 258);
		REQUIRE(BasicEntity::Id::from_value(258) == id);
	}
	
	SECTION("Can create an entity") {
		Entity entity = manager.create();
		
		REQUIRE(entity);
		REQUIRE(entity.alive());
		
		
		struct Component {
			int id;
		};

		SECTION("Can Add/Retrieve Component") {
			entity.assign(Component{12});
			CHECK(entity.has<Component>());
			REQUIRE(entity.component<Component>().id == 12);

			SECTION("Can Remove Component") {
				entity.remove<Component>();
				REQUIRE(!entity.has<Component>());
			}
		}

		SECTION("Can Destroy Entity") {
			kgr::container container;
			auto& manager = container.service<EntityManagerService>();
			
			struct Component {
				int id;
			};
			
			Entity entity = manager.create();
			CHECK(entity);
			entity.destroy();
			REQUIRE(!entity);
		}
	}
	
	SECTION("Can create and delete 256 times the same entity") {
		for (int i = 0; i < 256; ++i) {
			Entity entity = manager.create();
			CHECK(entity);
			entity.destroy();
			CHECK(!entity);
		}
	}
}
