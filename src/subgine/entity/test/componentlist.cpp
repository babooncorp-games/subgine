#include "catch2/catch.hpp"

#include "../componentlist.h"
#include "../service/entitymanagerservice.h"

#include "subgine/common/kangaru.h"

using namespace sbg;

TEST_CASE("ComponentList", "[entity]") {
	kgr::container container;
	auto& manager = container.service<EntityManagerService>();

	struct Data {
		int data;
	};

	SECTION("ComponentList with 1 component type") {
		ComponentList<Data> components;

		Entity entity = manager.create();
		auto data = Data{42};
		entity.assign(data);

		[[maybe_unused]]
		auto [emplacedData] = components.emplace(entity);

		SECTION("Emplace") {
			REQUIRE(Component<Data>{entity} == emplacedData);
		}

		SECTION("Size") {
			REQUIRE(components.size() == 1);
		}

		SECTION("Iteration") {
			int nbIteration = 0;
			for (auto [monBeauData] : components) {
				(void) monBeauData;
				++nbIteration;
				static_assert(std::is_same_v<decltype(monBeauData), Component<Data>>, "monBeauData must be a Component<Data>");
				REQUIRE(monBeauData->data == 42);
			}
			
			REQUIRE(nbIteration == 1);
		}

		SECTION("Erase") {
			components.erase(components.begin());
			REQUIRE(components.empty());
		}

		SECTION("Erase range") {
			components.erase(components.begin(), components.end());
			REQUIRE(components.empty());
		}

		SECTION("Clear") {
			components.clear();
			REQUIRE(components.empty());
		}

		SECTION("Cleanup") {
			entity.destroy();
			components.cleanup();
			REQUIRE(components.empty());
		}

		SECTION("Multiple entities") {
			Entity entity2 = manager.create();
			auto data2 = Data{42};
			entity2.assign(data2);

			[[maybe_unused]]
			auto [emplacedData2] = components.emplace(entity2);

			SECTION("Size") {
				REQUIRE(components.size() == 2);
			}

			SECTION("Iteration") {
				int nbIteration = 0;
				for (auto [monBeauData] : components) {
					(void) monBeauData;
					++nbIteration;
				}
				
				REQUIRE(nbIteration == 2);
			}

			SECTION("Erase") {
				components.erase(components.begin());
				REQUIRE(components.size() == 1);
			}

			SECTION("Erase range") {
				components.erase(components.begin(), components.end());
				REQUIRE(components.empty());
			}
		}
	}

	SECTION("ComponentList with 2 component type") {
		struct OtherData {
			int data;
		};

		ComponentList<Data, OtherData> components;

		Entity entity = manager.create();
		auto data = Data{20};
		entity.assign(data);
		auto otherData = OtherData{30};
		entity.assign(otherData);

		[[maybe_unused]]
		auto [emplacedData, emplacedOtherData] = components.emplace(entity);

		auto [containedData, containedOtherData] = *components.begin();
		REQUIRE(emplacedData == containedData);
		REQUIRE(emplacedOtherData == containedOtherData);
	}
}
