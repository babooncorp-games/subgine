#pragma once

#include "basicentity.h"

#include "subgine/common/math.h"
#include "subgine/common/map.h"
#include "subgine/common/stable_vector.h"
#include "subgine/common/type_id.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include <vector>
#include <memory>
#include <new>

namespace sbg {

struct EntityManagerService;
struct Entity;

// TODO: Make thread friendlier
struct EntityManager {
	void destroy(BasicEntity entity);
	bool alive(BasicEntity entity) const;
	
	auto create() -> Entity;
	
	template<typename T, typename... Args>
	void assign(BasicEntity entity, Args&&... args) {
		auto const id = entity.id();
		auto [components, generations] = component_list<T>();
		
		Log::fatal(
			[&]{ return not alive(entity); },
			SBG_LOG_INFO, "Attempt to assign a component to a dead entity id", log::enquote(id.index)
		);
		
		if (id.index >= components.size()) {
			components.resize(_generation.size());
		}
		
		if (id.index >= generations.size()) {
			generations.resize(_generation.size(), 255);
		}
		
		if (generations[id.index] != 255) {
			components[id.index].destroy();
		}
		
		generations[id.index] = id.generation;
		components[id.index].emplace(std::forward<Args>(args)...);
	}
	
	template<typename T>
	auto component(BasicEntity entity) -> T& {
		auto const id = entity.id();
		auto [components, generations] = component_list<T>();
		
		Log::fatal([&, &generations = generations]{ return generations.size() <= id.index || generations[id.index] != id.generation; },
			SBG_LOG_INFO, "No component with the specified type was found");
		
		return components[id.index].component;
	}
	
	template<typename T>
	void remove(BasicEntity entity) noexcept {
		auto const id = entity.id();
		
		// TODO: Not call component_storage since it may allocate, thus not noexcept.
		auto& storage = component_storage<T>();
		auto& generations = storage.storage_generations();
		
		Log::warning([&]{ return generations.size() <= id.index || generations[id.index] != id.generation; },
			SBG_LOG_INFO, "Removing a missing component from an entity");
		
		storage.remove(entity);
	}
	
	template<typename T>
	bool has(BasicEntity entity) noexcept {
		auto const id = entity.id();
		
		// TODO: Not call component_list since it may allocate, thus not noexcept.
		auto [components, generations] = component_list<T>();
		return generations.size() > id.index and generations[id.index] == id.generation;
	}
	
	struct Storage {
	private:
		template<typename T>
		union ComponentStorage {
			ComponentStorage() noexcept : empty{} {}
			~ComponentStorage() {}
			
			template<typename... Args>
			auto emplace(Args&&... args) -> T& {
				if constexpr (std::is_constructible_v<T, Args...>) {
					new (&component) T(std::forward<Args>(args)...);
				} else {
					new (&component) T{std::forward<Args>(args)...};
				}
				
				return component;
			}
			
			void destroy() noexcept {
				component.~T();
				empty = {};
			}
			
			T component;
			char empty;
		};
		
	public:
		template<typename T>
		using container_type = mknejp::vmcontainer::pinned_vector<ComponentStorage<T>>;
		using sample_type = container_type<void*>;
		
		~Storage() {
			_destroy(*this);
		}
		
		void remove(BasicEntity entity) {
			auto const id = entity.id();
			if (_generations.size() > id.index and _generations[id.index] == id.generation) {
				_remove(*this, id.index);
			}
		}
		
		template<typename T>
		void emplace() {
			static_assert(sizeof(container_type<T>) <= sizeof(sample_type));
			static_assert(alignof(container_type<T>) <= alignof(sample_type));
			(new (&_storage) container_type<T>(mknejp::vmcontainer::max_elements(BasicEntity::Id::Limits::index), 0))->reserve(1 << 9);
			
			_destroy = [](Storage& self) noexcept {
				auto& list = self.cast<T>();
				auto it_storage = list.begin();
				auto it_generations = self._generations.begin();
				for (; it_generations != self._generations.end() ; ++it_storage, ++it_generations) {
					if (*it_generations != std::uint8_t{255u}) {
						it_storage->destroy();
					}
				}
				using container_actual_type = container_type<T>;
				list.~container_actual_type();
			};
			
			_remove = [](Storage& self, std::uint32_t index) noexcept {
				auto& list = self.cast<T>();
				list[index].destroy();
				self._generations[index] = 255;
			};
		}
		
		template<typename T>
		auto at_allocating(std::size_t index) -> T* {
			container_type<T>& list = cast<T>();
			if (list.size() <= index) {
				list.resize(index + 1);
			}
			
			return std::addressof(list[index].component);
		}
		
		auto has(BasicEntity::Id id) const -> bool {
			return _generations.size() > id.index and _generations[id.index] != id.generation;
		}
		
		template<typename T>
		auto cast() noexcept -> container_type<T>& {
			return *std::launder(static_cast<container_type<T>*>(static_cast<void*>(&_storage)));
		}
		
		template<typename T>
		auto cast() const noexcept -> container_type<T> const& {
			return *std::launder(static_cast<container_type<T> const*>(static_cast<void const*>(&_storage)));
		}
		
		auto storage_generations() noexcept -> std::vector<std::uint8_t>& {
			return _generations;
		}
		
		auto storage_generations() const noexcept -> std::vector<std::uint8_t> const& {
			return _generations;
		}
		
	private:
		alignas(sample_type) std::byte _storage[sizeof(sample_type)];
		std::vector<std::uint8_t> _generations;
		void(*_destroy)(Storage&) noexcept;
		void(*_remove)(Storage&, std::uint32_t) noexcept;
	};
	
	template<typename T>
	auto storage() -> Storage& {
		return component_storage<T>();
	}
	
private:
	template<typename T>
	auto component_list() -> std::pair<Storage::container_type<T>&, std::vector<std::uint8_t>&> {
		auto& service_storage = component_storage<T>();
		return {service_storage.template cast<T>(), service_storage.storage_generations()};
	}
	
	template<typename T>
	auto component_storage() -> Storage& {
		if(auto it = _components.find(type_id<T>); it != _components.end()) {
			return it->second;
		} else {
			auto& storage = _components[type_id<T>];
			storage.template emplace<T>();
			return storage;
		}
	}
	
	static constexpr auto no_index = BasicEntity::Id::Limits::index;
	static constexpr auto alive_index = no_index - 1;
	
	std::unordered_map<type_id_t, Storage, murmur_hash<type_id_t>> _components;
	std::vector<BasicEntity::Id> _generation;
	std::uint32_t _free = no_index;
	
	friend auto service_map(EntityManager const&) -> EntityManagerService;
};

} // namespace sbg
