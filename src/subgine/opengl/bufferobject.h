#pragma once

#include <glbinding/gl/types.h>
#include <vector>
#include <span>
#include <type_traits>
#include <compare>

namespace sbg {

struct BufferObject {
	BufferObject() = default;
	BufferObject(gl::GLuint id, gl::GLenum target) noexcept;
	
	friend auto operator==(const BufferObject& lhs, const BufferObject& rhs) noexcept -> bool = default;
	friend auto operator<=>(const BufferObject& lhs, const BufferObject& rhs) noexcept = default;
	
	void bind() const& noexcept;
	void unbind() const& noexcept;
	
	void free() & noexcept;
	void create() & noexcept;
	
	auto id() const& noexcept -> gl::GLuint;
	
	auto data(std::span<std::byte const>, gl::GLenum usage) const& noexcept -> void;
	
	template<typename T> requires (
		    std::is_trivial_v<typename T::value_type>
		and requires(T t) {
			requires std::is_pointer_v<decltype(t.data())>;
			{ t.size() } -> std::same_as<std::size_t>;
		}
	)
	auto data(T const& buffer_data, gl::GLenum usage) const& noexcept -> void {
		data(std::span<std::byte const>{reinterpret_cast<std::byte const*>(buffer_data.data()), sizeof(T) * buffer_data.size()}, usage);
	}
	
private:
	gl::GLuint _id = 0;
	gl::GLenum _target = {};
};

struct BufferObjectFormatPart {
	gl::GLenum type;
	gl::GLuint size;
	gl::GLsizei offset;
};

using BufferObjectFormat = std::vector<BufferObjectFormatPart>;

struct BufferObjectArray {
	BufferObjectArray() = default;
	explicit BufferObjectArray(std::size_t size) : _formats(size), _buffers(size), _targets(size) {}
	
	using FormatRef = std::span<BufferObjectFormatPart const>;
	
	auto create() & noexcept -> void;
	auto free() & noexcept -> void;
	auto buffer(std::size_t i) const& noexcept -> BufferObject;
	auto format(std::size_t i) const& noexcept -> FormatRef;
	auto size() const& noexcept -> std::size_t;
	auto data(std::size_t i, std::span<std::byte const> data, gl::GLenum target, gl::GLenum flag, BufferObjectFormat format) & noexcept -> void;
	
	struct BufferIterator {
		using format_iterator = std::vector<std::pair<BufferObjectFormat, std::size_t>>::const_iterator;
		using buffer_iterator = std::vector<gl::GLuint>::const_iterator;
		using target_iterator = std::vector<gl::GLenum>::const_iterator;
		
		BufferIterator(format_iterator format_it, buffer_iterator buffer_it, target_iterator target_it) noexcept :
			_format_it{format_it}, _buffer_it{buffer_it}, _target_it{target_it} {}
		
		auto operator*() const& noexcept -> std::tuple<BufferObject, FormatRef, std::size_t> {
			return {
				BufferObject{*_buffer_it, *_target_it},
				_format_it->first,
				_format_it->second
			};
		}
		
		auto operator++() & noexcept -> BufferIterator& {
			++_format_it;
			++_buffer_it;
			++_target_it;
			return *this;
		}
		
		auto friend operator<=>(BufferIterator const&, BufferIterator const&) noexcept = default;
		
	private:
		format_iterator _format_it;
		buffer_iterator _buffer_it;
		target_iterator _target_it;
	};
	
	struct BufferRange {
		BufferRange(BufferIterator begin, BufferIterator end) noexcept : _begin{begin}, _end{end} {}
		
		auto begin() const noexcept {
			return _begin;
		}
		
		auto end() const noexcept {
			return _end;
		}
		
	private:
		BufferIterator _begin;
		BufferIterator _end;
	};
	
	auto buffers() const& noexcept -> BufferRange;
	
private:
	// TODO: Maybe use a static vector with fixed capacity
	std::vector<std::pair<BufferObjectFormat, std::size_t>> _formats;
	std::vector<gl::GLuint> _buffers;
	std::vector<gl::GLenum> _targets;
};

} // namespace sbg
