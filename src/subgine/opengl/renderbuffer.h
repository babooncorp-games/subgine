#pragma once

#include "subgine/vector/vector.h"

#include <glbinding/gl/types.h>

namespace sbg {

struct RenderBuffer {
	RenderBuffer() = default;
	void free() noexcept;
	
	auto format() const noexcept -> gl::GLenum;
	
	void create() noexcept;
	void storage(gl::GLenum format, Vector2<gl::GLsizei> size) noexcept;
	
	void bind() const noexcept;
	void unbind() const noexcept;
	
	gl::GLuint id() const noexcept;
	
private:
	gl::GLuint _id = 0;
	gl::GLenum _format = {};
};

} // namespace sbg
