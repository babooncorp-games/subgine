#pragma once

#include "opengl/service.h"

#include "../../factory/arraymeshfactory.h"
#include "../../factory/bufferobjectfactory.h"
#include "../../factory/instancedarraymeshfactory.h"
#include "../../factory/programfactory.h"
#include "../../factory/shaderfactory.h"
#include "../../factory/sharedarraymeshfactory.h"
#include "../../factory/texturefactory.h"
#include "../../factory/vertexarrayobjectfactory.h"

#include "../../painter/spritepainter.h"

#include "../../utility/membersofprogram.h"
#include "../../utility/trait.h"

#include "../../arraymesh.h"
#include "../../bufferobject.h"
#include "../../framebuffer.h"
#include "../../instancedarraymesh.h"
#include "../../program.h"
#include "../../renderbuffer.h"
#include "../../shader.h"
#include "../../texture.h"
#include "../../vertexarrayobject.h"
