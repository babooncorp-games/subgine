#pragma once

namespace sbg {

struct OpenglModuleService;
struct PainterCreator;

struct OpenglModule {
	void setupPainterCreator(PainterCreator& painterCreator) const;
	friend auto service_map(OpenglModule const&) -> OpenglModuleService;
}; 

} // namespace sbg
