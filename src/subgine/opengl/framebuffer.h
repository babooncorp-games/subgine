#pragma once

#include "subgine/resource/resourcehandle.h"
#include "subgine/resource/ownedresource.h"
#include "subgine/common/types.h"
#include "texture.h"
#include "renderbuffer.h"

#include <glbinding/gl/types.h>
#include <span>

#include <vector>
#include <variant>

namespace sbg {

struct FrameBuffer {
	void create() noexcept;
	void free() noexcept;
	bool check() const noexcept;
	
	void bind() const noexcept;
	void unbind() const noexcept;
	
	template<typename... Args, enable_if_i<(std::is_same_v<Args, gl::GLenum> && ...)> = 0>
	void draw(Args... buffers) const noexcept {
		auto const bufs = std::array{buffers...};
		draw(bufs);
	}
	
	void draw(std::span<gl::GLenum const> buffers) const noexcept;
	void read(gl::GLenum buffer) const noexcept;
	
	void attach(gl::GLenum attachmentPoint, OwnedResource<Texture> texture);
	void attach(gl::GLenum attachmentPoint, ResourceHandle<Texture> texture);
	void attach(gl::GLenum attachmentPoint, OwnedResource<RenderBuffer> buffer);
	void attach(gl::GLenum attachmentPoint, ResourceHandle<RenderBuffer> buffer);
	void attach(gl::GLenum attachmentPoint, Texture texture) noexcept;
	void attach(gl::GLenum attachmentPoint, RenderBuffer buffer) noexcept;
	
	auto id() const noexcept -> gl::GLuint;
	
private:
	using AttachementType = std::variant<
		ResourceHandle<Texture>,
		OwnedResource<Texture>,
		OwnedResource<RenderBuffer>,
		ResourceHandle<RenderBuffer>
	>;
	
	std::vector<AttachementType> _owned;
	gl::GLuint _id = 0;
};

} // namespace sbg
