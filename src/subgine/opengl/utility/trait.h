#pragma once

#include "../program.h"

#include "subgine/common/traits.h"

#include <string_view>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace sbg {

struct Uniform;

template<typename, typename = void>
inline constexpr auto is_uniform_value = false;

template<typename T>
inline constexpr auto is_uniform_value<T,
	std::void_t<decltype(std::declval<Program>().uniformValue(std::declval<std::string_view>(), std::declval<const T&>()))>
> = true;

namespace detail {

template<typename T>
struct buffer_data_type {
	using type = T;
};

template<typename T, dim_t n>
struct buffer_data_type<Vector<n, T>> {
	using type = T;
};

template<typename T, glm::precision p>
struct buffer_data_type<glm::tmat2x2<T, p>> {
	using type = T;
};

template<typename T, glm::precision p>
struct buffer_data_type<glm::tmat3x3<T, p>> {
	using type = T;
};

template<typename T, glm::precision p>
struct buffer_data_type<glm::tmat4x4<T, p>> {
	using type = T;
};

template<typename T>
using buffer_data_type_t = typename buffer_data_type<T>::type;

template<typename>
inline constexpr auto buffer_data_size = 1;

template<typename T, sbg::dim_t n>
inline constexpr auto buffer_data_size<Vector<n, T>> = n;

template<typename T, glm::precision p>
inline constexpr auto buffer_data_size<glm::tmat2x2<T, p>> = 4;

template<typename T, glm::precision p>
inline constexpr auto buffer_data_size<glm::tmat3x3<T, p>> = 9;

template<typename T, glm::precision p>
inline constexpr auto buffer_data_size<glm::tmat4x4<T, p>> = 16;

} // namespace detail
} // namespace sbg
