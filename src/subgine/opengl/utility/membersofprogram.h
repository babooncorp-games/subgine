#pragma once

#include "../program.h"

#include "subgine/graphic/dynstructbuffer.h"

#include <ranges>

namespace sbg {

inline auto members_of_program(Program const& program) {
	struct MemberRange {
		MemberRange(Program::InspectIterator begin, Program::InspectSentinel end) noexcept : _begin{begin}, _end{end} {}
		
		struct MemberIterator;
		struct MemberSentinel {
			constexpr explicit MemberSentinel(Program::InspectSentinel it) noexcept : _it{it} {}
			
		private:
			friend MemberIterator;
			Program::InspectSentinel _it;
		};
		
		struct MemberIterator {
			explicit MemberIterator(Program::InspectIterator const& it) noexcept : _it{it} {}
			
			[[nodiscard]]
			auto operator*() const& noexcept {
				auto const [name, type, location] = *_it;
				return DynStructBuffer::Member{location, type, name};
			}
			
			auto operator++() & noexcept -> MemberIterator& {
				++_it;
				return *this;
			}
			
			constexpr auto operator==(MemberSentinel const& rhs) const& noexcept -> bool {
				return _it == rhs._it;
			}
			
		private:
			Program::InspectIterator _it;
		};
		
		auto begin() const& noexcept -> MemberIterator {
			return MemberIterator{_begin};
		}
		
		auto end() const& noexcept -> MemberSentinel {
			return MemberSentinel{_end};
		}
		
	private:
		Program::InspectIterator _begin;
		Program::InspectSentinel _end;
	};
	
	auto const uniforms = program.uniforms();
	
	return MemberRange{uniforms.begin(), uniforms.end()};
}

} // namespace sbg

