#include "renderbuffer.h"

#include <glbinding/gl/functions.h>
#include <glbinding/gl/enum.h>

using namespace sbg;

void RenderBuffer::free() noexcept {
	if (_id != 0) {
		gl::glDeleteRenderbuffers(1, &_id);
		_id = 0;
	}
}

void RenderBuffer::bind() const noexcept {
	gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, _id);
}

void RenderBuffer::unbind() const noexcept {
	gl::glBindRenderbuffer(gl::GL_RENDERBUFFER, 0);
}

auto RenderBuffer::format() const noexcept -> gl::GLenum {
	return _format;
}

gl::GLuint RenderBuffer::id() const noexcept {
	return _id;
}

void RenderBuffer::create() noexcept {
	free();
	gl::glCreateRenderbuffers(1, &_id);
}

void RenderBuffer::storage(gl::GLenum format, Vector2<gl::GLsizei> size) noexcept {
	_format = format;
	gl::glNamedRenderbufferStorage(_id, format, size.x, size.y);
}