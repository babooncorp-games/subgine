#include "openglmodule.h"

#include "subgine/resource.h"
#include "subgine/graphic.h"
#include "subgine/opengl.h"

using namespace sbg::literals;

namespace sbg {

void OpenglModule::setupPainterCreator(PainterCreator& painterCreator) const {
	painterCreator.add("sprite-painter", [](
		kgr::generator<SpritePainterService> build,
		ProgramManager programManager,
		SharedArrayMeshFactory meshFactory,
		Property data
	) {
		return build(programManager.get(data["program"_h]), meshFactory.create(data["mesh"_h]));
	});
}

} // namespace sbg
