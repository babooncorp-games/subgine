#pragma once

#include "shader.h"

#include "subgine/vector/vector.h"
#include "subgine/resource/resourcehandle.h"
#include "subgine/common/hash.h"
#include "subgine/common/type_id.h"
#include "subgine/common/map.h"
#include "subgine/graphic/component/transform.h"

#include <glm/glm.hpp>

#include <vector>
#include <string_view>
#include <string>
#include <memory>
#include <map>
#include <iterator>
#include <glbinding/gl46/enum.h>
#include <glbinding/gl46/types.h>

namespace sbg {

struct Program {
	Program() = default;
	
	struct InspectIterator;
	
	struct InspectSentinel {
		InspectSentinel() = default;
		constexpr explicit InspectSentinel(int index) noexcept : _index{index} {}
		
	private:
		friend InspectIterator;
		int _index = 0;
	};
	
	struct InspectIterator {
		static constexpr auto properties = std::array{gl::GL_BLOCK_INDEX, gl::GL_TYPE, gl::GL_NAME_LENGTH, gl::GL_LOCATION};
		
		constexpr explicit InspectIterator() = default;
		explicit InspectIterator(gl::GLuint id, gl::GLint amount) noexcept;
		
		constexpr auto operator==(InspectSentinel const& rhs) const& noexcept -> bool {
			return _index == rhs._index;
		}
		
		auto operator*() const& noexcept -> std::tuple<std::string, type_id_t, std::uint32_t>;
		auto operator++() & noexcept -> InspectIterator&;
		
	private:
		std::array<gl::GLint, 4> _values = {};
		gl::GLuint _id = {};
		gl::GLint _amount = {};
		int _index = 0;
	};
	
	struct InspectRange {
		constexpr InspectRange(InspectIterator begin, InspectSentinel end) noexcept : _begin{begin}, _end{end} {}
		
		constexpr auto begin() const& noexcept -> InspectIterator {
			return _begin;
		}
		
		constexpr auto end() const& noexcept -> InspectSentinel {
			return _end;
		}
		
	private:
		InspectIterator _begin;
		InspectSentinel _end;
	};
	
	auto uniforms() const& noexcept -> InspectRange;
	
	void attach(Shader shader);
	void link();
	void free();
	void create();
	
	void uniformValue(std::string_view var, float value) const;
	void uniformValue(std::string_view var, double value) const;
	void uniformValue(std::string_view var, std::int32_t value) const;
	void uniformValue(std::string_view var, std::uint32_t value) const;
	void uniformValue(std::string_view var, std::uint64_t value) const;
	void uniformValue(std::string_view var, Vector2f value) const;
	void uniformValue(std::string_view var, Vector3f value) const;
	void uniformValue(std::string_view var, Vector4f value) const;
	void uniformValue(std::string_view var, Vector2i value) const;
	void uniformValue(std::string_view var, Vector3i value) const;
	void uniformValue(std::string_view var, Vector4i value) const;
	void uniformValue(std::string_view var, glm::mat2 const& value) const;
	void uniformValue(std::string_view var, glm::mat3 const& value) const;
	void uniformValue(std::string_view var, glm::mat4 const& value) const;
	void uniformValue(std::string_view var, Transform const& value) const;
	
	void uniformValue(unsigned int id, float value) const;
	void uniformValue(unsigned int id, double value) const;
	void uniformValue(unsigned int id, std::int32_t value) const;
	void uniformValue(unsigned int id, std::uint32_t value) const;
	void uniformValue(unsigned int id, std::uint64_t value) const;
	void uniformValue(unsigned int id, Vector2f value) const;
	void uniformValue(unsigned int id, Vector3f value) const;
	void uniformValue(unsigned int id, Vector4f value) const;
	void uniformValue(unsigned int id, Vector2i value) const;
	void uniformValue(unsigned int id, Vector3i value) const;
	void uniformValue(unsigned int id, Vector4i value) const;
	void uniformValue(unsigned int id, glm::mat2 const& value) const;
	void uniformValue(unsigned int id, glm::mat3 const& value) const;
	void uniformValue(unsigned int id, glm::mat4 const& value) const;
	void uniformValue(unsigned int id, Transform const& value) const;
	
	template<typename E, enable_if_i<std::is_enum_v<E>> = 0>
	void uniformValue(std::string_view var, E value) const {
		uniformValue(var, static_cast<std::underlying_type_t<E>>(value));
	}
	
	void attributeLocation(std::string_view name, unsigned int location);
	void fragmentLocation(std::string_view name, unsigned int location);
	
	void bind() const;
	void unbind() const;
	
	void mapUniform(char const* name, std::string mappedName);
	
	[[nodiscard]]
	auto uniform_location(std::string_view var) const& -> int;
	
	[[nodiscard]]
	auto id() const& -> unsigned int;
	
private:
	using cache_t = dictionary<unsigned int>;
	
	std::shared_ptr<cache_t> _locations = {};
	unsigned int _id = 0;
};

} // namespace sbg
