#include "vertexarrayobjectview.h"

#include "vertexarrayobject.h"

using namespace sbg;

VertexArrayObjectView::VertexArrayObjectView(VertexArrayObject const& vao) noexcept : _id{vao.id()} {}

auto VertexArrayObjectView::id() const noexcept -> gl::GLuint {
	return _id;
}

void VertexArrayObjectView::bind() const noexcept {
	gl::glBindVertexArray(_id);
}

void VertexArrayObjectView::unbind() const noexcept {
	gl::glBindVertexArray(0);
}
