#include "framebuffer.h"

#include "renderbuffer.h"

#include <glbinding/gl46core/functions.h>
#include <glbinding/gl46core/enum.h>
#include <cassert>

using namespace sbg;

void FrameBuffer::free() noexcept {
	if (_id != 0) {
		gl::glDeleteFramebuffers(1, &_id);
		_id = 0;
	}
}

void FrameBuffer::bind() const noexcept {
	gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, _id);
}

void FrameBuffer::unbind() const noexcept {
	gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
}

void FrameBuffer::create() noexcept {
	assert(_id == 0);
	gl::glCreateFramebuffers(1, &_id);
}

void FrameBuffer::draw(std::span<gl::GLenum const> buffers) const noexcept {
	gl::glNamedFramebufferDrawBuffers(_id, static_cast<gl::GLsizei>(buffers.size()), buffers.data());
}

void FrameBuffer::read(gl::GLenum buffer) const noexcept {
	gl::glNamedFramebufferReadBuffer(_id, buffer);
}

void FrameBuffer::attach(gl::GLenum attachmentPoint, RenderBuffer buffer) noexcept {
	gl::glNamedFramebufferRenderbuffer(
		_id,
		attachmentPoint,
		gl::GL_RENDERBUFFER,
		buffer.id()
	);
}

void FrameBuffer::attach(gl::GLenum attachmentPoint, Texture texture) noexcept {
	gl::glNamedFramebufferTexture2DEXT(
		_id,
		attachmentPoint,
		texture.target(),
		texture.id(),
		0
	);
}

void FrameBuffer::attach(gl::GLenum attachmentPoint, OwnedResource<RenderBuffer> buffer) {
	attach(attachmentPoint, buffer.get());
	_owned.emplace_back(std::move(buffer));
}

void FrameBuffer::attach(gl::GLenum attachementPoint, ResourceHandle<RenderBuffer> buffer) {
	attach(attachementPoint, buffer.resource());
	_owned.emplace_back(buffer);
}

void FrameBuffer::attach(gl::GLenum attachmentPoint, OwnedResource<Texture> texture) {
	attach(attachmentPoint, texture.get());
	_owned.emplace_back(std::move(texture));
}

void FrameBuffer::attach(gl::GLenum attachmentPoint, ResourceHandle<Texture> texture) {
	attach(attachmentPoint, texture.resource());
	_owned.emplace_back(texture);
}

bool FrameBuffer::check() const noexcept {
	return gl::glCheckNamedFramebufferStatus(_id, gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE;
}

auto FrameBuffer::id() const noexcept -> gl::GLuint {
	return _id;
}
