#include "texture.h"

#include "subgine/log.h"
#include <glbinding/gl46/gl.h>

#include <cassert>

using namespace sbg;

Texture::Handle::Handle(gl::GLuint64 value) noexcept : _handle{value} {}

void Texture::Handle::make_resident() const noexcept {
	gl::glMakeTextureHandleResidentARB(_handle);
}

void Texture::Handle::make_non_resident() const noexcept {
	gl::glMakeTextureHandleNonResidentARB(_handle);
}

auto Texture::Handle::value() const noexcept -> gl::GLuint64 {
	return _handle;
}

Texture::Texture(gl::GLenum target) noexcept : _target{target} {}

auto Texture::id() const noexcept -> gl::GLuint {
	return _id;
}

auto Texture::target() const noexcept -> gl::GLenum {
	return _target;
}

void Texture::parameter(gl::GLenum parameter, gl::GLenum value) const noexcept {
	gl::glTextureParameteri(_id, parameter, value);
}

void Texture::parameter(gl::GLenum parameter, int value) const noexcept {
	gl::glTextureParameteri(_id, parameter, value);
}

void Texture::parameter(gl::GLenum parameter, Vector4f value) const noexcept {
	auto const values = std::array{value.x, value.y, value.z, value.w};
	gl::glTextureParameterfv(_id, parameter, values.data());
}

void Texture::generateMipmaps() const noexcept {
	gl::glGenerateTextureMipmap(_id);
}

void Texture::free() noexcept {
	if (_id != 0) {
		gl::glDeleteTextures(1, &_id);
		_id = 0;
	}
}

void Texture::create() {
	assert(_id == 0);
	assert(_target != gl::GLenum{});
	gl::glCreateTextures(_target, 1, &_id);
}

void Texture::bind() const noexcept {
	gl::glBindTexture(_target, _id);
}

void Texture::unbind() const noexcept {
	gl::glBindTexture(_target, 0);
}

void Texture::image(TextureFormat1D const& format, void const* data) const noexcept {
	gl::glTextureImage1DEXT(_id, _target, format.level, format.format, format.size.x, 0, format.sourceFormat, format.type, data);
}

void Texture::image(TextureFormat2D const& format, void const* data) const noexcept {
	gl::glTextureImage2DEXT(_id, _target, format.level, format.format, format.size.x, format.size.y, 0, format.sourceFormat, format.type, data);
}

void Texture::image(TextureFormat3D const& format, void const* data) const noexcept {
	gl::glTextureImage3DEXT(_id, _target, format.level, format.format, format.size.x, format.size.y, format.size.z, 0, format.sourceFormat, format.type, data);
}

void Texture::cubeimage(TextureFormat2D const& format, int nth_face, void const* data) const noexcept {
	gl::glTextureImage2DEXT(_id, gl::GL_TEXTURE_CUBE_MAP_POSITIVE_X + nth_face, format.level, format.format, format.size.x, format.size.y, 0, format.sourceFormat, format.type, data);
}

void Texture::subimage(SubImageFormat1D const& format, Vector1i offset, void const* data) const noexcept {
	gl::glTextureSubImage1DEXT(_id, _target, format.level, offset.x, format.size.x, format.sourceFormat, format.type, data);
}

void Texture::subimage(SubImageFormat2D const& format, Vector2i offset, void const* data) const noexcept {
	gl::glTextureSubImage2DEXT(_id, _target, format.level, offset.x, offset.y, format.size.x, format.size.y, format.sourceFormat, format.type, data);
}

void Texture::subimage(SubImageFormat3D const& format, Vector3i offset, void const* data) const noexcept {
	gl::glTextureSubImage3DEXT(_id, _target, format.level, offset.x, offset.y, offset.z, format.size.x, format.size.y, format.size.z, format.sourceFormat, format.type, data);
}

auto Texture::handle() const noexcept -> Handle {
	return Handle{gl::glGetTextureHandleARB(_id)};
}

namespace sbg {
	template struct TextureFormat<1>;
	template struct TextureFormat<2>;
	template struct TextureFormat<3>;
}
