#pragma once

#include "vertexarrayobject.h"
#include "vertexarrayobjectview.h"
#include "subgine/resource/resourcehandle.h"
#include "subgine/resource/ownedresource.h"

#include "subgine/common/types.h"
#include "subgine/common/traits.h"

#include <glbinding/gl/types.h>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/functions.h>

namespace sbg {

struct ArrayMesh {
	ArrayMesh() = default;
	ArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLenum mode, gl::GLint start, gl::GLsizei size) noexcept;
	ArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept;
	
	auto vao() const& noexcept -> VertexArrayObject const&;
	auto vao() & noexcept -> VertexArrayObject&;
	
	auto size() const noexcept -> gl::GLsizei;
	auto start() const noexcept -> gl::GLint;
	
	void draw() const noexcept;
	void free() noexcept;
	
	gl::GLenum mode = gl::GL_TRIANGLES;
	
private:
	VertexArrayObject _vao;
	gl::GLint _start;
	gl::GLsizei _size;
};

struct SharedArrayMesh {
	SharedArrayMesh() = default;
	SharedArrayMesh(ResourceHandle<VertexArrayObject> vao, gl::GLenum mode, gl::GLint start, gl::GLsizei size) noexcept;
	SharedArrayMesh(ResourceHandle<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept;
	
	auto vao() const& noexcept -> VertexArrayObject const&;
	auto vao() & noexcept -> VertexArrayObject&;
	
	auto size() const noexcept -> gl::GLsizei;
	auto start() const noexcept -> gl::GLint;
	
	void draw() const noexcept;
	
	gl::GLenum mode = gl::GL_TRIANGLES;
	
private:
	ResourceHandle<VertexArrayObject> _vao;
	gl::GLint _start;
	gl::GLsizei _size;
};

struct ArrayMeshView {
	ArrayMeshView() = default;
	ArrayMeshView(ArrayMesh const&) noexcept;
	ArrayMeshView(SharedArrayMesh const&) noexcept;
	
	auto vao() const noexcept -> VertexArrayObjectView;
	auto size() const noexcept -> gl::GLsizei;
	auto start() const noexcept -> gl::GLsizei;
	auto mode() const noexcept -> gl::GLenum;
	
	void draw() const noexcept;
	
private:
	VertexArrayObjectView _vao;
	gl::GLsizei _start = 0;
	gl::GLsizei _size = 0;
	gl::GLenum _mode = gl::GL_TRIANGLES;
};

} // namespace sbg
