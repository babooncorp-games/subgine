#include "arraymesh.h"

using namespace sbg;

ArrayMesh::ArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLenum _mode, gl::GLint start, gl::GLsizei size) noexcept :
	mode{_mode}, _vao{vao.release()}, _start{start}, _size{size} {}

ArrayMesh::ArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept :
	_vao{vao.release()}, _start{start}, _size{size} {}

auto ArrayMesh::vao() const& noexcept -> VertexArrayObject const& {
	return _vao;
}

auto ArrayMesh::vao() & noexcept -> VertexArrayObject& {
	return _vao;
}

auto ArrayMesh::size() const noexcept -> gl::GLsizei {
	return _size;
}

auto ArrayMesh::start() const noexcept -> gl::GLint {
	return _start;
}

void ArrayMesh::draw() const noexcept {
	gl::glDrawArrays(mode, _start, _size);
}

void ArrayMesh::free() noexcept {
	_vao.free();
}

SharedArrayMesh::SharedArrayMesh(ResourceHandle<VertexArrayObject> vao, gl::GLenum _mode, gl::GLint start, gl::GLsizei size) noexcept :
	mode{_mode}, _vao{vao}, _start{start}, _size{size} {}

SharedArrayMesh::SharedArrayMesh(ResourceHandle<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept :
	_vao{vao}, _start{start}, _size{size} {}

auto SharedArrayMesh::vao() const& noexcept -> VertexArrayObject const& {
	return *_vao;
}

auto SharedArrayMesh::vao() & noexcept -> VertexArrayObject& {
	return *_vao;
}

auto SharedArrayMesh::size() const noexcept -> gl::GLsizei {
	return _size;
}

auto SharedArrayMesh::start() const noexcept -> gl::GLint {
	return _start;
}

void SharedArrayMesh::draw() const noexcept {
	gl::glDrawArrays(mode, _start, _size);
}

ArrayMeshView::ArrayMeshView(ArrayMesh const& mesh) noexcept :
	_vao{mesh.vao()}, _start{mesh.start()}, _size{mesh.size()}, _mode{mesh.mode} {}

ArrayMeshView::ArrayMeshView(SharedArrayMesh const& mesh) noexcept :
	_vao{mesh.vao()}, _start{mesh.start()}, _size{mesh.size()}, _mode{mesh.mode} {}

auto ArrayMeshView::vao() const noexcept -> VertexArrayObjectView {
	return _vao;
}

auto ArrayMeshView::size() const noexcept -> gl::GLsizei {
	return _size;
}

auto ArrayMeshView::start() const noexcept -> gl::GLsizei {
	return _start;
}

auto ArrayMeshView::mode() const noexcept -> gl::GLenum {
	return _mode;
}

void ArrayMeshView::draw() const noexcept {
	gl::glDrawArrays(_mode, _start, _size);
}