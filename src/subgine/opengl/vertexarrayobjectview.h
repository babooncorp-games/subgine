#pragma once

#include <glbinding/gl/types.h>

namespace sbg {

struct VertexArrayObject;

struct VertexArrayObjectView {
	VertexArrayObjectView() = default;
	VertexArrayObjectView(VertexArrayObject const& vao) noexcept;
	
	/**
	 * Getter for _id
	 * @returns the reffered vao's id.
	 */
	auto id() const noexcept -> gl::GLuint;
	
	/**
	 * Binds the opengl vao
	 */
	void bind() const noexcept;
	
	/**
	 * Unbinds the opengl vao
	 */
	void unbind() const noexcept;
	
private:
	gl::GLuint _id = 0;
};

} // namespace sbg
