#version 460 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 texturePosition;

layout(location = 0) out vec2 UV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
	gl_Position = projection * view * model * vec4(vertexPosition, 1);
	UV = texturePosition;
}
