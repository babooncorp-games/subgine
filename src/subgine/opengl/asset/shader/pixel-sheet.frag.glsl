#version 460 core

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 UV;
uniform sampler2DArray spritesheet;
uniform int frame;

vec2 pixelise(sampler2DArray tex, vec2 coord);

void main(void) {
	outColor = texture(spritesheet, vec3(pixelise(spritesheet, UV), frame));
}
