#version 460 core

// Adjust alpha for the amount of blurring
const vec2 alpha = vec2(0.124700538379252);

vec2 pixelise(sampler2D tex, vec2 coord) {
	ivec2 size = textureSize(tex, 0);
	vec2 vUv = coord * size;

	vec2 x = fract(vUv);
	
	vec2 x_ = clamp(0.5 / alpha * x, 0.0, 0.5) + clamp(0.5 / alpha * (x - 1.0) + 0.5,  0.0, 0.5);
	
	
	return (floor(vUv) + x_) / size;
}

vec2 pixelise(sampler2DArray tex, vec2 coord) {
	ivec2 size = textureSize(tex, 0).xy;
	vec2 vUv = coord * size;
	
	vec2 x = fract(vUv);
	vec2 x_ = clamp(0.5 / alpha * x, 0.0, 0.5) + clamp(0.5 / alpha * (x - 1.0) + 0.5,  0.0, 0.5);
	
	return (floor(vUv) + x_) / size;
}

