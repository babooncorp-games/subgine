#version 460 core

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 UV;

uniform sampler2D tex;
uniform vec4 tint;

void main() {
	outColor = texture(tex, UV) * tint;
}
