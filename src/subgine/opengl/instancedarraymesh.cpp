#include "instancedarraymesh.h"

#include <glbinding/gl/functions.h>
#include "subgine/log.h"

using namespace sbg;

InstancedArrayMesh::InstancedArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLenum _mode, gl::GLint start, gl::GLsizei size) noexcept :
	mode{_mode}, _vao{vao.release()}, _start{start}, _size{size} {}

InstancedArrayMesh::InstancedArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept :
	_vao{vao.release()}, _start{start}, _size{size} {}

auto InstancedArrayMesh::vao() const noexcept -> VertexArrayObject const& {
	return _vao;
}

auto InstancedArrayMesh::vao() noexcept -> VertexArrayObject& {
	return _vao;
}

auto InstancedArrayMesh::size() const noexcept -> gl::GLsizei {
	return _size;
}

auto InstancedArrayMesh::instance() const noexcept -> gl::GLsizei {
	return _instance;
}

auto InstancedArrayMesh::start() const noexcept -> gl::GLint {
	return _start;
}

void InstancedArrayMesh::instance(gl::GLsizei instance) noexcept {
	_instance = instance;
}

void InstancedArrayMesh::draw() const noexcept {
	gl::glDrawArraysInstanced(mode, _start, _size, _instance);
}

void InstancedArrayMesh::free() noexcept {
	_vao.free();
}

InstancedArrayMeshView::InstancedArrayMeshView(const InstancedArrayMesh& mesh) noexcept :
	_vao{mesh.vao()}, _start{mesh.start()}, _size{mesh.size()}, _instance{mesh.instance()} {}

const VertexArrayObjectView& InstancedArrayMeshView::vao() const noexcept {
	return _vao;
}

gl::GLsizei InstancedArrayMeshView::size() const noexcept {
	return _size;
}

gl::GLsizei InstancedArrayMeshView::instance() const noexcept {
	return _instance;
}

gl::GLint InstancedArrayMeshView::start() const noexcept {
	return _start;
}

void InstancedArrayMeshView::draw() const noexcept {
	gl::glDrawArraysInstanced(mode, _start, _size, _instance);
}
