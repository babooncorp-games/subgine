#pragma once

#include "subgine/vector/vector.h"

#include <glbinding/gl/types.h>

namespace sbg {

template<dim_t n>
struct TextureFormat {
	gl::GLint level;
	gl::GLenum format;
	sbg::Vector<n, gl::GLsizei> size;
	gl::GLenum sourceFormat;
	gl::GLenum type;
};

using TextureFormat1D = TextureFormat<1>;
using TextureFormat2D = TextureFormat<2>;
using TextureFormat3D = TextureFormat<3>;

template<dim_t n>
struct SubImageFormat {
	gl::GLint level;
	sbg::Vector<n, gl::GLsizei> size;
	gl::GLenum sourceFormat;
	gl::GLenum type;
};

using SubImageFormat1D = SubImageFormat<1>;
using SubImageFormat2D = SubImageFormat<2>;
using SubImageFormat3D = SubImageFormat<3>;

struct Texture {
	struct Handle {
		Handle() = default;
		explicit Handle(gl::GLuint64 value) noexcept;
		
		void make_resident() const noexcept;
		void make_non_resident() const noexcept;
		
		auto value() const noexcept -> gl::GLuint64;
		
	private:
		gl::GLuint64 _handle = 0;
	};
	
	Texture() = default;
	explicit Texture(gl::GLenum target) noexcept;
	
	auto id() const noexcept -> gl::GLuint;
	auto target() const noexcept -> gl::GLenum;
	
	void create();
	
	void parameter(gl::GLenum parameter, gl::GLenum value) const noexcept;
	void parameter(gl::GLenum parameter, int value) const noexcept;
	void parameter(gl::GLenum parameter, Vector4f value) const noexcept;
	void generateMipmaps() const noexcept;
	
	void free() noexcept;
	
	void bind() const noexcept;
	void unbind() const noexcept;
	
	void image(TextureFormat1D const& format, void const* data) const noexcept;
	void image(TextureFormat2D const& format, void const* data) const noexcept;
	void image(TextureFormat3D const& format, void const* data) const noexcept;
	
	void cubeimage(TextureFormat2D const& format, int nth_face, void const* data) const noexcept;
	void cubeimage(TextureFormat3D const& format, int nth_face, void const* data) const noexcept;
	
	void subimage(SubImageFormat1D const& format, Vector1i offset, void const* data) const noexcept;
	void subimage(SubImageFormat2D const& format, Vector2i offset, void const* data) const noexcept;
	void subimage(SubImageFormat3D const& format, Vector3i offset, void const* data) const noexcept;
	
	friend auto operator==(Texture const&, Texture const&) -> bool = default;
	friend auto operator<=>(Texture const&, Texture const&) = default;
	
	auto handle() const noexcept -> Handle;
	
private:
	gl::GLuint _id = 0;
	gl::GLenum _target = {};
};

extern template struct TextureFormat<1>;
extern template struct TextureFormat<2>;
extern template struct TextureFormat<3>;

} // namespace sbg
