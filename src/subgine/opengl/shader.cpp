#include "shader.h"

#include "subgine/common.h"
#include "subgine/log.h"

#include <vector>
#include <cstdio>
#include <iostream>

using namespace sbg;

void Shader::create(gl::GLenum type) {
	free();
	_id = gl::glCreateShader(type);
}

void Shader::free() {
	if (_id != 0) {
		gl::glDeleteShader(_id);
		_id = 0;
	}
}

auto Shader::id() const noexcept -> gl::GLuint {
	return _id;
}

void Shader::build(std::string_view source) {
	int result = 0;
	
	auto shaderSource = source.data();
	gl::glShaderSource(_id, 1, &shaderSource, 0);
	gl::glCompileShader(_id);
	
	gl::glGetShaderiv(_id, gl::GL_COMPILE_STATUS, &result);
	
	if (!result) {
		int logLength = 0;
		gl::glGetShaderiv(_id, gl::GL_INFO_LOG_LENGTH, &logLength);
		
		std::vector<char> error(logLength);
		
		gl::glGetShaderInfoLog(_id, logLength, 0, error.data());
		
		std::cerr << error.data() << std::endl;
		sbg::Log::fatal(SBG_LOG_INFO, "Shader error");
	}
}
