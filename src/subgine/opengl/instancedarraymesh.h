#pragma once

#include "vertexarrayobject.h"
#include "vertexarrayobjectview.h"
#include "subgine/resource/ownedresource.h"

#include <glbinding/gl/types.h>
#include <glbinding/gl/enum.h>

namespace sbg {

struct InstancedArrayMesh  {
	InstancedArrayMesh() = default;
	InstancedArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLenum _mode, gl::GLint start, gl::GLsizei size) noexcept;
	InstancedArrayMesh(OwnedResource<VertexArrayObject> vao, gl::GLint start, gl::GLsizei size) noexcept;
	
	auto vao() const noexcept -> VertexArrayObject const&;
	auto vao() noexcept -> VertexArrayObject&;
	
	auto size() const noexcept -> gl::GLsizei;
	auto instance() const noexcept -> gl::GLsizei;
	auto start() const noexcept -> gl::GLint;
	
	void instance(gl::GLsizei instance) noexcept;
	
	void draw() const noexcept;
	void free() noexcept;
	
	gl::GLenum mode = gl::GL_TRIANGLES;
	
private:
	VertexArrayObject _vao;
	gl::GLint _start = 0;
	gl::GLsizei _size = 0;
	gl::GLsizei _instance = 0;
};

struct InstancedArrayMeshView {
	InstancedArrayMeshView() = default;
	InstancedArrayMeshView(const InstancedArrayMesh& mesh) noexcept;
	
	const VertexArrayObjectView& vao() const noexcept;
	gl::GLsizei size() const noexcept;
	gl::GLsizei instance() const noexcept;
	gl::GLint start() const noexcept;
	
	void draw() const noexcept;
	
	gl::GLenum mode = gl::GL_TRIANGLES;
	
private:
	VertexArrayObjectView _vao;
	gl::GLint _start = 0;
	gl::GLsizei _size = 0;
	gl::GLsizei _instance = 0;
};

} // namespace sbg
