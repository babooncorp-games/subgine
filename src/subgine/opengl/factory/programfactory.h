#pragma once

#include "shaderfactory.h"
#include "../program.h"

#include "subgine/resource/resourcemanager.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/common/types.h"

#include <string>

namespace sbg {

struct ProgramLoaderService;

struct ProgramLoader {
	ProgramLoader(ShaderManager shaderResource);
	
	Program create(Property data) const;
	
	static constexpr auto path = "program";
	
private:
	ShaderManager _shaderResource;
	
	friend auto service_map(ProgramLoader const&) -> ProgramLoaderService;
};

using ProgramFactory = ResourceFactory<ProgramLoader>;
using ProgramManager = ResourceManager<ProgramFactory>;

} // namespace sbg
