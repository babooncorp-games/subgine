#pragma once

#include "vertexarrayobjectfactory.h"
#include "../arraymesh.h"

#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"

#include <glbinding/gl/enum.h>

namespace sbg {

struct SharedArrayMeshLoaderService;

struct SharedArrayMeshLoader {
	static constexpr auto path = "mesh";
	explicit SharedArrayMeshLoader(VertexArrayObjectManager vaoManager) noexcept : _vaoManager{vaoManager} {}
	
	auto create(Property data) const -> SharedArrayMesh;
	
private:
	VertexArrayObjectManager _vaoManager;
	
	friend auto service_map(SharedArrayMeshLoader const&) -> SharedArrayMeshLoaderService;
};

using SharedArrayMeshFactory = ResourceFactory<SharedArrayMeshLoader>;

} // namespace sbg
