#include "bufferobjectfactory.h"
#include "../detail/vertexattributetype.h"
#include "subgine/common/lookup_table.h"

#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include <glbinding/gl46/enum.h>

using namespace sbg;

namespace {
	constexpr auto targets = sbg::makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_ARRAY_BUFFER", gl::GL_ARRAY_BUFFER},
		{"GL_ATOMIC_COUNTER_BUFFER", gl::GL_ATOMIC_COUNTER_BUFFER},
		{"GL_COPY_READ_BUFFER", gl::GL_COPY_READ_BUFFER},
		{"GL_COPY_WRITE_BUFFER", gl::GL_COPY_WRITE_BUFFER},
		{"GL_DISPATCH_INDIRECT_BUFFER", gl::GL_DISPATCH_INDIRECT_BUFFER},
		{"GL_DRAW_INDIRECT_BUFFER", gl::GL_DRAW_INDIRECT_BUFFER},
		{"GL_ELEMENT_ARRAY_BUFFER", gl::GL_ELEMENT_ARRAY_BUFFER},
		{"GL_PIXEL_PACK_BUFFER", gl::GL_PIXEL_PACK_BUFFER},
		{"GL_PIXEL_UNPACK_BUFFER", gl::GL_PIXEL_UNPACK_BUFFER},
		{"GL_QUERY_BUFFER", gl::GL_QUERY_BUFFER},
		{"GL_SHADER_STORAGE_BUFFER", gl::GL_SHADER_STORAGE_BUFFER},
		{"GL_TEXTURE_BUFFER", gl::GL_TEXTURE_BUFFER},
		{"GL_TRANSFORM_FEEDBACK_BUFFER", gl::GL_TRANSFORM_FEEDBACK_BUFFER},
		{"GL_UNIFORM_BUFFER", gl::GL_UNIFORM_BUFFER}
	});
	
	constexpr auto usages = sbg::makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_STREAM_DRAW", gl::GL_STREAM_DRAW},
		{"GL_STREAM_READ", gl::GL_STREAM_READ},
		{"GL_STREAM_COPY", gl::GL_STREAM_COPY},
		{"GL_STATIC_DRAW", gl::GL_STATIC_DRAW},
		{"GL_STATIC_READ", gl::GL_STATIC_READ},
		{"GL_STATIC_COPY", gl::GL_STATIC_COPY},
		{"GL_DYNAMIC_DRAW", gl::GL_DYNAMIC_DRAW},
		{"GL_DYNAMIC_READ", gl::GL_DYNAMIC_READ},
		{"GL_DYNAMIC_COPY", gl::GL_DYNAMIC_COPY}
	});
	
	auto visitVertex(BufferObjectFormatPart format, auto function) -> decltype(auto) {
		detail::visit_gltype(format.type, [&]<typename T>() -> decltype(auto) {
			switch(format.size) {
				case 1: return function.template operator()<T, 1>();
				case 2: return function.template operator()<T, 2>();
				case 3: return function.template operator()<T, 3>();
				case 4: return function.template operator()<T, 4>();
				default: std::abort(); // never happen
			}
		});
	}
	
	auto createFormatItem(Property data, std::size_t currentSize) -> std::tuple<BufferObjectFormatPart, std::size_t> {
		auto const formatItem = std::string_view{data};
		
		auto size = std::size_t{1};
		auto type = gl::GL_NONE;
		
		if (formatItem.starts_with("vector") and formatItem.size() >= 7) {
			size = formatItem[6] - '0';
			if (size > 4) {
				Log::fatal(SBG_LOG_INFO, "Unrecognized format", log::enquote(formatItem), "size must be > 0 and <= 4");
			}
		}
		
		if (formatItem.ends_with("i8")) {
			type = gl::GL_BYTE;
		} else if (formatItem.ends_with("u8")) {
			type = gl::GL_UNSIGNED_BYTE;
		} else if (formatItem.ends_with("i16")) {
			type = gl::GL_SHORT;
		} else if (formatItem.ends_with("u16")) {
			type = gl::GL_UNSIGNED_SHORT;
		} else if (formatItem.ends_with("i32")) {
			type = gl::GL_INT;
		} else if ((formatItem == "index" and size == 1) or formatItem.ends_with("u32")) {
			type = gl::GL_UNSIGNED_INT;
		} else if (formatItem.ends_with("f32")) {
			type = gl::GL_FLOAT;
		} else if (formatItem.ends_with("f64")) {
			type = gl::GL_DOUBLE;
		} else {
			Log::fatal(SBG_LOG_INFO, "Unrecognized format", log::enquote(formatItem), "invalid data type");
		}
		
		auto const typeSize = detail::size_of_gltype(type);
		auto const offset = static_cast<gl::GLsizei>(currentSize + currentSize % typeSize);
		return {
			BufferObjectFormatPart{type, static_cast<gl::GLuint>(size), offset},
			offset + size * typeSize
		};
	}
	
	auto createFormat(Property data) -> std::tuple<BufferObjectFormat, std::size_t> {
		// we create the pair for NRVO of the format
		auto returnValue = std::tuple<BufferObjectFormat, std::size_t>{};
		auto& [format, offset] = returnValue;
		
		if (data.isString()) {
			auto const [formatItem, nextOffset] = createFormatItem(data, 0);
			format.emplace_back(formatItem);
			offset = nextOffset;
		} else if (data.isArray()) {
			format.reserve(data.size());
			for (auto const formatItemData : data) {
				auto const [formatItem, nextOffset] = createFormatItem(formatItemData, offset);
				format.emplace_back(formatItem);
				offset = nextOffset;
			}
		} else {
			Log::fatal(SBG_LOG_INFO, "Unrecognized format", log::enquote(data), "invalid data type");
		}
		
		return returnValue;
	}
	
	auto createBuffer(BufferObjectArray& bufferArray, std::size_t nth, Property data) -> void {
		auto [format, size] = createFormat(data["format"]);
		auto const vertices = data["data"];
		auto const amount = vertices.size();
		
		auto const createVertexElement = [](std::byte* memory, BufferObjectFormatPart format, Property data) {
			visitVertex(format, [&]<typename T, std::size_t size>() {
				if constexpr (size == 1) {
					new (memory + format.offset) T{data};
				} else {
					new (memory + format.offset) Vector<size, T>{data};
				}
			});
		};
		
		auto const memory = std::make_unique<std::byte[]>(amount * size);
		auto current = memory.get();
		for (auto const vertex : data["data"]) {
			auto nthElement = std::size_t{0};
			for (auto const formatItem : format) {
				createVertexElement(current, formatItem, format.size() == 1 ? vertex : vertex[nthElement++]);
			}
			current += size;
		}
		
		auto const target = targets[data["target"]];
		auto const usage = usages[data["usage"]];
		
		bufferArray.data(nth, std::span{memory.get(), amount * size}, target, usage, std::move(format));
	}
}

auto BufferObjectArrayLoader::create(Property data) const& -> BufferObjectArray {
	auto const buffers = data["buffers"];
	auto bufferArray = BufferObjectArray{buffers.size()};
	
	bufferArray.create();
	
	auto nth = std::size_t{0};
	for (auto const& buffer : buffers) {
		createBuffer(bufferArray, nth++, buffer);
	}
	
	return bufferArray;
}
