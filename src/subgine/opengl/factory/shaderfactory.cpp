#include "shaderfactory.h"

#include "../shader.h"

#include "subgine/resource.h"
#include "subgine/common/lookup_table.h"

#include <unordered_map>
#include <string_view>

namespace sbg {

constexpr static auto types = makeLookupTable<std::string_view, gl::GLenum>({
	{"GL_VERTEX_SHADER", gl::GL_VERTEX_SHADER},
	{"GL_FRAGMENT_SHADER", gl::GL_FRAGMENT_SHADER}
});

Shader ShaderLoader::create(Property data) const {
	Shader shader;
	
	shader.create(types[data["type"]]);
	shader.build(std::string_view{data["source"]});
	
	return shader;
}

} // namespace sbg
