#pragma once

#include "../texture.h"

#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg {

struct TextureLoaderService;

/**
 * @brief A loader for textures.
 */
struct TextureLoader {
	/**
	 * Loads a texture from the data.
	 * 
	 * @param data the texture data, including its type and size (if needed).
	 */
	auto create(Property data) const -> Texture;
	
	static constexpr auto path = "texture";
	
	friend auto service_map(TextureLoader const&) -> TextureLoaderService;
};

using TextureFactory = ResourceFactory<TextureLoader>;
using TextureManager = ResourceManager<TextureFactory>;

} // namespace sbg
