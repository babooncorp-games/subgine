#pragma once

#include "vertexarrayobjectfactory.h"
#include "../instancedarraymesh.h"

#include "subgine/resource/ownedresource.h"
#include "subgine/resource/resourcefactory.h"

namespace sbg {

struct InstancedArrayMeshLoaderService;

struct InstancedArrayMeshLoader {
	static constexpr auto path = "mesh";
	explicit InstancedArrayMeshLoader(VertexArrayObjectFactory vaoFactory) noexcept :
		_vaoFactory{std::move(vaoFactory)} {}
	
	auto create(Property data) const -> InstancedArrayMesh;
	
private:
	VertexArrayObjectFactory _vaoFactory;
	
	friend auto service_map(InstancedArrayMeshLoader const&) -> InstancedArrayMeshLoaderService;
};

using InstancedArrayMeshFactory = ResourceFactory<InstancedArrayMeshLoader>;

} // namespace sbg
