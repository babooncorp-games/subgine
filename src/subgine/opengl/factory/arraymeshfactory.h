#pragma once

#include "vertexarrayobjectfactory.h"
#include "../arraymesh.h"

#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/property.h"

#include <glbinding/gl/enum.h>

namespace sbg {

struct ArrayMeshLoaderService;

struct ArrayMeshLoader {
	static constexpr auto path = "mesh";
	explicit ArrayMeshLoader(VertexArrayObjectFactory vaoFactory) noexcept;
	
	auto create(Property data) const -> ArrayMesh;
	
private:
	VertexArrayObjectFactory _vaoFactory;
	
	friend auto service_map(ArrayMeshLoader const&) -> ArrayMeshLoaderService;
};

using ArrayMeshFactory = ResourceFactory<ArrayMeshLoader>;

} // namespace sbg
