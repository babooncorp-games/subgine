#include "instancedarraymeshfactory.h"

#include "../detail/arraymeshfactorybase.h"

using namespace sbg;

auto InstancedArrayMeshLoader::create(Property data) const -> InstancedArrayMesh {
	return InstancedArrayMesh{
		_vaoFactory.create(data["buffer"]),
		data["mode"].empty() ? gl::GL_TRIANGLES : detail::modes[data["mode"]],
		gl::GLint{data["start"]},
		gl::GLsizei{data["size"]}
	};
}
