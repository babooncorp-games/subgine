#include "texturefactory.h"

#include "subgine/common/lookup_table.h"

#include "subgine/graphic/utility/stbi_deleter.h"
#include "subgine/resource.h"
#include "subgine/log.h"

#include "stb/stb_image.h"

#include <string_view>
#include <glbinding/gl46core/enum.h>
#include <glbinding/gl46core/functions.h>
#include <glbinding/gl46core/types.h>

using namespace std::literals;
using namespace sbg;

namespace {
	constexpr auto targets = makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_TEXTURE_1D", gl::GL_TEXTURE_1D},
		{"GL_TEXTURE_2D", gl::GL_TEXTURE_2D},
		{"GL_TEXTURE_3D", gl::GL_TEXTURE_3D},
		{"GL_TEXTURE_RECTANGLE", gl::GL_TEXTURE_RECTANGLE},
		{"GL_TEXTURE_BUFFER", gl::GL_TEXTURE_BUFFER},
		{"GL_TEXTURE_CUBE_MAP", gl::GL_TEXTURE_CUBE_MAP},
		{"GL_TEXTURE_1D_ARRAY", gl::GL_TEXTURE_1D_ARRAY},
		{"GL_TEXTURE_2D_ARRAY", gl::GL_TEXTURE_2D_ARRAY},
		{"GL_TEXTURE_CUBE_MAP_ARRAY", gl::GL_TEXTURE_CUBE_MAP_ARRAY},
		{"GL_TEXTURE_2D_MULTISAMPLE", gl::GL_TEXTURE_2D_MULTISAMPLE},
		{"GL_TEXTURE_2D_MULTISAMPLE_ARRAY", gl::GL_TEXTURE_2D_MULTISAMPLE_ARRAY}
	});
	
	constexpr auto magnifyFilters = makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_NEAREST", gl::GL_NEAREST},
		{"GL_LINEAR", gl::GL_LINEAR}
	});
	
	constexpr auto minifyFilters = makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_NEAREST", gl::GL_NEAREST},
		{"GL_LINEAR", gl::GL_LINEAR},
		{"GL_NEAREST_MIPMAP_NEAREST", gl::GL_NEAREST_MIPMAP_NEAREST},
		{"GL_LINEAR_MIPMAP_NEAREST", gl::GL_LINEAR_MIPMAP_NEAREST},
		{"GL_NEAREST_MIPMAP_LINEAR", gl::GL_NEAREST_MIPMAP_LINEAR},
		{"GL_LINEAR_MIPMAP_LINEAR", gl::GL_LINEAR_MIPMAP_LINEAR}
	});
	
	constexpr auto texture_wrap = makeLookupTable<std::string_view, gl::GLenum>({
		{"GL_CLAMP_TO_EDGE", gl::GL_CLAMP_TO_EDGE},
		{"GL_MIRRORED_REPEAT", gl::GL_MIRRORED_REPEAT},
		{"GL_REPEAT", gl::GL_REPEAT},
		{"GL_CLAMP_TO_BORDER", gl::GL_CLAMP_TO_BORDER},
		{"GL_MIRROR_CLAMP_TO_EDGE", gl::GL_MIRROR_CLAMP_TO_EDGE}
	});
	
	constexpr auto textureDimentions = makeLookupTable<gl::GLenum, dim_t>({
		{gl::GL_TEXTURE_1D, 1},
		{gl::GL_TEXTURE_2D, 2},
		{gl::GL_TEXTURE_3D, 3},
		{gl::GL_TEXTURE_RECTANGLE, 2},
		{gl::GL_TEXTURE_BUFFER, 1},
		{gl::GL_TEXTURE_CUBE_MAP, 2},
		{gl::GL_TEXTURE_1D_ARRAY, 2},
		{gl::GL_TEXTURE_2D_ARRAY, 3},
		{gl::GL_TEXTURE_CUBE_MAP_ARRAY, 3},
		{gl::GL_TEXTURE_2D_MULTISAMPLE, 2},
		{gl::GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 3}
	});
	
	constexpr auto channelCountFormat = makeLookupTable<int, gl::GLenum>({
		{1, gl::GL_RED},
		{2, gl::GL_RG},
		{3, gl::GL_RGB},
		{4, gl::GL_RGBA}
	});
}

auto TextureLoader::create(Property data) const -> Texture {
	Vector2i size;
	int channels;
	auto const texture_path = asset_directory() / std::filesystem::path{data["path"]};
	auto const pixels = std::unique_ptr<unsigned char, stbi_deleter>{stbi_load(texture_path.string().c_str(), &size.x, &size.y, &channels, 0)};
	
	if (!pixels) {
		Log::error(SBG_LOG_INFO, "STB image cannot load image at path", texture_path);
		return {};
	}
	
	Texture texture{targets[data["target"]]};
	
	texture.create();
	
	auto const dimensions = textureDimentions[texture.target()];
	if (texture.target() == gl::GL_TEXTURE_CUBE_MAP) {
		auto const width = int{data["width"]};
		auto const height = int{data["height"]};
		
		for (auto i = 0 ; i < 6 ; ++i) {
			texture.cubeimage(
				TextureFormat2D{
					0,
					gl::GL_SRGB8_ALPHA8,
					Vector2i{width, height},
					channelCountFormat[channels],
					gl::GL_UNSIGNED_BYTE
				},
				i,
				pixels.get() + (width * height * channels * i)
			);
		}
	} else if (texture.target() == gl::GL_TEXTURE_CUBE_MAP_ARRAY) {
		// TODO: write proper todo.
		// TODO: 
	} else if (dimensions == 1) {
		texture.image(TextureFormat{0, gl::GL_SRGB8_ALPHA8, Vector1i{size.x * size.y}, channelCountFormat[channels], gl::GL_UNSIGNED_BYTE}, pixels.get());
	} else if (dimensions == 2) {
		texture.image(TextureFormat{0, gl::GL_SRGB8_ALPHA8, size, channelCountFormat[channels], gl::GL_UNSIGNED_BYTE}, pixels.get());
	} else {
		auto const width = int{data["width"]};
		auto const height = int{data["height"]};
		auto const size3d = Vector3i{width, height, (size.x * size.y) / (width * height)};
		auto const format = TextureFormat{0, gl::GL_SRGB_ALPHA, size3d, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE};
		
		texture.image(format, pixels.get());
	}
	
	texture.parameter(gl::GL_TEXTURE_MIN_FILTER,
		data["minify-filter"].empty() ? gl::GL_LINEAR_MIPMAP_LINEAR : minifyFilters[data["minify-filter"]]
	);
	
	texture.parameter(gl::GL_TEXTURE_MAG_FILTER,
		data["magnify-filter"].empty() ? gl::GL_LINEAR : magnifyFilters[data["magnify-filter"]]
	);
	
	texture.parameter(gl::GL_TEXTURE_WRAP_S, texture_wrap[value_or(data["wrap_s"], "GL_CLAMP_TO_EDGE"sv)]);
	texture.parameter(gl::GL_TEXTURE_WRAP_T, texture_wrap[value_or(data["wrap_t"], "GL_CLAMP_TO_EDGE"sv)]);
	
	texture.generateMipmaps();
	texture.bind();
	auto value = gl::GLfloat{};
	gl::glGetFloatv(gl::GL_MAX_TEXTURE_MAX_ANISOTROPY, &value);
	gl::glTexParameterf(texture.target(), gl::GL_TEXTURE_MAX_ANISOTROPY, value);
	
	return texture;
}
