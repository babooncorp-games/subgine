#pragma once

#include "../shader.h"

#include "subgine/common/types.h"
#include "subgine/resource/property.h"
#include "subgine/resource/resourcemanager.h"
#include "subgine/resource/resourcefactory.h"

namespace sbg {

struct ShaderLoaderService;

struct ShaderLoader {
	Shader create(Property data) const;
	
	static constexpr auto path = "shader";
	
	friend auto service_map(const ShaderLoader&) -> ShaderLoaderService;
};

using ShaderFactory = ResourceFactory<ShaderLoader>;
using ShaderManager = ResourceManager<ShaderFactory>;

} // namespace sbg
