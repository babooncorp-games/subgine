#pragma once

#include "bufferobjectfactory.h"
#include "../vertexarrayobject.h"

#include "subgine/resource/resourcemanager.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/common/hash.h"

#include <string_view>

namespace sbg {

struct VertexArrayObjectFactoryService;

struct VertexArrayObjectFactory {
	VertexArrayObjectFactory(BufferObjectArrayManager bufferManager) noexcept : _bufferManager{bufferManager} {}
	
	using Result = VertexArrayObject;
	
	auto create(std::string_view name) const -> OwnedResource<Result> {
		auto vao = OwnedResource<VertexArrayObject>{};
		
		vao->create();
		vao->bind();
		vao->attach(_bufferManager.get(name));
		vao->unbind();
		
		return vao;
	}

	auto create(hash_t hash) const -> OwnedResource<Result> {
		auto vao = OwnedResource<VertexArrayObject>{};
		
		vao->create();
		vao->bind();
		vao->attach(_bufferManager.get(hash));
		vao->unbind();
		
		return vao;
	}
	
	static constexpr auto path = "vertex-array-object";
	
private:
	BufferObjectArrayManager _bufferManager;
	
	friend auto service_map(VertexArrayObjectFactory const&) -> VertexArrayObjectFactoryService;
};

using VertexArrayObjectManager = ResourceManager<VertexArrayObjectFactory>;

} // namespace sbg
