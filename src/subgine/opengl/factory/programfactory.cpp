#include "programfactory.h"

namespace sbg {

ProgramLoader::ProgramLoader(
	ShaderManager shaderManager
) : _shaderResource{shaderManager} {}

Program ProgramLoader::create(Property data) const {
	Program program;
	
	program.create();
	
	for (auto const shader : data["shaders"]) {
		program.attach(_shaderResource.get(shader).resource());
	}
	
	program.link();
	
	return program;
}

} // namespace sbg
