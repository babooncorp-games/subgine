#include "arraymeshfactory.h"

#include "../detail/arraymeshfactorybase.h"

using namespace sbg;

ArrayMeshLoader::ArrayMeshLoader(VertexArrayObjectFactory vaoFactory) noexcept : _vaoFactory{vaoFactory} {}

auto ArrayMeshLoader::create(Property data) const -> ArrayMesh {
	return ArrayMesh{
		_vaoFactory.create(data["buffer"]),
		data["mode"].empty() ? gl::GL_TRIANGLES : detail::modes[data["mode"]],
		gl::GLint{data["start"]},
		gl::GLsizei{data["size"]}
	};
}
