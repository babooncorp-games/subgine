#pragma once

#include "../bufferobject.h"

#include "subgine/resource/resourcemanager.h"
#include "subgine/resource/resourcefactory.h"

namespace sbg {

struct BufferObjectArrayLoaderService;

struct BufferObjectArrayLoader {
	static constexpr auto path = "buffer";
	auto create(Property data) const& -> BufferObjectArray;
	
private:
	friend auto service_map(BufferObjectArrayLoader const&) -> BufferObjectArrayLoaderService;
};

using BufferObjectArrayFactory = ResourceFactory<BufferObjectArrayLoader>;
using BufferObjectArrayManager = ResourceManager<BufferObjectArrayFactory>;

} // namespace sb
