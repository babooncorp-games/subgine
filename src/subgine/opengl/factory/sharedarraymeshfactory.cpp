 #include "sharedarraymeshfactory.h"

#include "../detail/arraymeshfactorybase.h"

using namespace sbg;

auto SharedArrayMeshLoader::create(Property data) const -> SharedArrayMesh {
	return SharedArrayMesh{
		_vaoManager.get(data["buffer"]),
		data["mode"].empty() ? gl::GL_TRIANGLES : detail::modes[data["mode"]],
		gl::GLint{data["start"]},
		gl::GLsizei{data["size"]}
	};
}
