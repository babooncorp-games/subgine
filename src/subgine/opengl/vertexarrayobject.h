#pragma once

#include "bufferobject.h"
#include "detail/vertexattributetype.h"
#include "utility/trait.h"

#include "subgine/resource/ownedresource.h"
#include "subgine/common/ownershiptoken.h"

#include <glbinding/gl/functions.h>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/types.h>

namespace sbg {

struct VertexArrayObject {
	/**
	 * Attach a single buffer to the vao.
	 * 
	 * There can be as many buffers to attach as the vao can take.
	 * Can be called multiple times until the vao is full.
	 * 
	 * @param buffer The buffer to be attached. Can be a shared pointer or an object.
	 */
	void attach(BufferObject buffer, auto const& format, std::size_t stride) {
		assert_bound();
		attach_helper(buffer, format, stride);
	}
	
	/**
	 * Attach a group of buffer to the vao.
	 * 
	 * There can be as many buffers to attach as the vao can take.
	 * Can be called multiple times until the vao is full.
	 * 
	 * @param buffer A resource handle to the buffer to be attached.
	 */
	auto attach(ResourceHandle<BufferObjectArray> const& bufferArray) -> void;
	
	/**
	 * Attach a group of buffer to the vao.
	 * 
	 * There can be as many buffers to attach as the vao can take.
	 * Can be called multiple times until the vao is full.
	 * 
	 * @param buffer The buffer to be attached.
	 */
	auto attach(BufferObjectArray const& bufferArray) noexcept -> void;
	
	/**
	 * Attach a part of a buffer object
	 *
	 * Useful for attaching a buffer from a DynStructBuffer
	 * 
	 * @param buffer The buffer to be attached. Can be a shared pointer or an object.
	 */
	auto attach_part(BufferObject buffer, BufferObjectFormatPart format, std::size_t stride) noexcept -> void;

	/**
	 * Returns the number of attributes this VAO has attached
	 */
	auto attribute_count() const& noexcept -> std::size_t;
	
	/**
	 * Creates the opengl vao
	 */
	void create() noexcept;
	
	/**
	 * Destroys the opengl vao
	 */
	void free() noexcept;
	
	/**
	 * Binds the opengl vao
	 */
	void bind() const noexcept;
	
	/**
	 * Unbinds the opengl vao
	 */
	void unbind() const noexcept;
	
	/**
	 * Returns the vao handle
	 */
	auto id() const noexcept -> gl::GLuint;
	
private:
	/**
	 * Asserts that the VAO is bound before doing operations on it
	 */
	void assert_bound() const noexcept;
	
	/**
	 * Attaches a particular buffer object to the vao.
	 * We iterate on each buffers of the buffer object and attach that buffer.
	 */
	auto attach_helper(BufferObject buffer, auto const& format, std::size_t stride) -> void {
		buffer.bind();
		for (auto const elementFormat : format) {
			auto const index = next_index();
			attribute_pointer(index, elementFormat.type, elementFormat.size, static_cast<gl::GLsizei>(stride), elementFormat.offset);
		}
		buffer.unbind();
	}
	
	/**
	 * Activate an attribute in the vao
	 */
	static auto attribute_pointer(gl::GLuint index, gl::GLenum type, gl::GLint size, gl::GLsizei stride, std::size_t offset) noexcept -> void;
	
	/**
	 * Increment and returns the index.
	 * 
	 * @return the index before incrementation
	 */
	auto next_index() noexcept -> gl::GLuint;
	
	gl::GLuint _index = 0;
	gl::GLuint _id = 0;
	
	std::vector<OwnershipToken> _refs;
};

} // namespace sbg
