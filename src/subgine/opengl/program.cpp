#include "program.h"

#include "subgine/log.h"

#include <glbinding/gl/functions.h>
#include <iostream>
#include <cassert>

using namespace sbg;

static auto type_id_for_gltype(int id) noexcept {
	switch(static_cast<gl::GLenum>(id)) {
		case gl::GL_BOOL: return type_id<bool>;
		case gl::GL_INT: return type_id<std::int32_t>;
		case gl::GL_UNSIGNED_INT: return type_id<std::uint32_t>;
		case gl::GL_FLOAT: return type_id<float>;
		case gl::GL_DOUBLE: return type_id<double>;
		case gl::GL_FLOAT_VEC2: return type_id<Vector2f>;
		case gl::GL_FLOAT_VEC3: return type_id<Vector3f>;
		case gl::GL_FLOAT_VEC4: return type_id<Vector4f>;
		case gl::GL_INT_VEC2: return type_id<Vector2i>;
		case gl::GL_INT_VEC3: return type_id<Vector3i>;
		case gl::GL_INT_VEC4: return type_id<Vector4i>;
		case gl::GL_FLOAT_MAT2: return type_id<glm::mat2>;
		case gl::GL_FLOAT_MAT3: return type_id<glm::mat3>;
		case gl::GL_FLOAT_MAT4: return type_id<glm::mat4>;
		case gl::GL_SAMPLER_1D: return type_id<int>;
		case gl::GL_SAMPLER_2D: return type_id<int>;
		case gl::GL_SAMPLER_3D: return type_id<int>;
		case gl::GL_SAMPLER_CUBE: return type_id<int>;
		case gl::GL_SAMPLER_1D_SHADOW: return type_id<int>;
		case gl::GL_SAMPLER_2D_SHADOW: return type_id<int>;
		case gl::GL_SAMPLER_2D_RECT: return type_id<int>;
		case gl::GL_SAMPLER_2D_RECT_SHADOW: return type_id<int>;
		case gl::GL_SAMPLER_2D_ARRAY: return type_id<int>;
		default: Log::fatal(SBG_LOG_INFO, "Unhandled OpenGL type id", log::enquote(id));
	}
}

Program::InspectIterator::InspectIterator(gl::GLuint id, gl::GLint amount) noexcept : _id{id}, _amount{amount} {
	if (_amount > 0) {
		_index = -1;
		operator++();
	}
}

auto Program::InspectIterator::operator*() const& noexcept -> std::tuple<std::string, type_id_t, std::uint32_t> {
	auto name = std::string(static_cast<std::string::size_type>(_values[2]), '\0');
	gl::glGetProgramResourceName(_id, gl::GL_UNIFORM, _index, static_cast<gl::GLsizei>(name.size()), nullptr, name.data());
	name.resize(_values[2] - 1);
	
	return std::tuple{std::move(name), type_id_for_gltype(_values[1]), _values[3]};
}

auto Program::InspectIterator::operator++() & noexcept -> InspectIterator& {
	_values = {};
	
	// Continue until we hit a uniform not in a block
	while (_values[0] != -1) {
		++_index;
		
		if (_index == _amount) {
			break;
		}
		
		gl::glGetProgramResourceiv(
			_id,
			gl::GL_UNIFORM,
			_index,
			static_cast<gl::GLsizei>(properties.size()),
			properties.data(),
			static_cast<gl::GLsizei>(_values.size()),
			nullptr,
			_values.data()
		);
	}
	
	return *this;
}

auto Program::uniforms() const& noexcept -> InspectRange {
	gl::GLint uniform_amount = 0;
	gl::glGetProgramInterfaceiv(_id, gl::GL_UNIFORM, gl::GL_ACTIVE_RESOURCES, &uniform_amount);
	return InspectRange{
		InspectIterator{_id, uniform_amount},
		InspectSentinel{uniform_amount}
	};
}

void Program::create() {
	assert(_id == 0);
	_locations = std::make_shared<cache_t>();
	_id = gl::glCreateProgram();
}

void Program::free() {
	if (_id != 0) {
		gl::glDeleteProgram(_id);
		_locations = nullptr;
		_id = 0;
	}
}

void Program::attach(Shader shader) {
	gl::glAttachShader(_id, shader.id());
}

void Program::link() {
	gl::glLinkProgram(_id);
	
	int result = 0;
	
	gl::glGetProgramiv(_id, gl::GL_LINK_STATUS, &result);
	
	if (!result) {
		int logLength = 0;
		gl::glGetProgramiv(_id, gl::GL_INFO_LOG_LENGTH, &logLength);
		
		std::vector<char> error_message( logLength );
		gl::glGetProgramInfoLog(_id, logLength, 0, error_message.data());
		
		std::cerr << error_message.data() << std::endl;
		sbg::Log::fatal(SBG_LOG_INFO, "Program link error");
	}
}

void Program::attributeLocation(std::string_view name, unsigned location) {
	gl::glBindAttribLocation(_id, location, name.data());
}

void Program::bind() const {
	gl::glUseProgram(_id);
}

void Program::unbind() const {
	gl::glUseProgram(0);
}

void Program::uniformValue(std::string_view var, float value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, double value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, std::int32_t value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, std::uint32_t value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, std::uint64_t value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector2f value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector3f value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector4f value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector2i value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector3i value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Vector4i value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, glm::mat2 const& value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, glm::mat3 const& value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, glm::mat4 const& value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(std::string_view var, Transform const& value) const {
	auto location = uniform_location(var);
	
	if (location != -1) {
		uniformValue(location, value);
	}
}

void Program::uniformValue(unsigned int id, float value) const {
	gl::glUniform1f(id, value);
}

void Program::uniformValue(unsigned int id, double value) const {
	gl::glUniform1d(id, value);
}

void Program::uniformValue(unsigned int id, std::int32_t value) const {
	gl::glUniform1i(id, value);
}

void Program::uniformValue(unsigned int id, std::uint32_t value) const {
	gl::glUniform1ui(id, value);
}

void Program::uniformValue(unsigned int id, std::uint64_t value) const {
	gl::glUniformHandleui64ARB(id, value);
}

void Program::uniformValue(unsigned int id, Vector2f value) const {
	gl::glUniform2f(id, value.x, value.y);
}

void Program::uniformValue(unsigned int id, Vector3f value) const {
	gl::glUniform3f(id, value.x, value.y, value.z);
}

void Program::uniformValue(unsigned int id, Vector4f value) const {
	gl::glUniform4f(id, value.x, value.y, value.z, value.w);
}

void Program::uniformValue(unsigned int id, Vector2i value) const {
	gl::glUniform2i(id, value.x, value.y);
}

void Program::uniformValue(unsigned int id, Vector3i value) const {
	gl::glUniform3i(id, value.x, value.y, value.z);
}

void Program::uniformValue(unsigned int id, Vector4i value) const {
	gl::glUniform4i(id, value.x, value.y, value.z, value.w);
}

void Program::uniformValue(unsigned int id, glm::mat2 const& value) const {
	gl::glUniformMatrix2fv(id, 1, gl::GL_FALSE, &value[0][0]);
}

void Program::uniformValue(unsigned int id, glm::mat3 const& value) const {
	gl::glUniformMatrix3fv(id, 1, gl::GL_FALSE, &value[0][0]);
}

void Program::uniformValue(unsigned int id, glm::mat4 const& value) const {
	gl::glUniformMatrix4fv(id, 1, gl::GL_FALSE, &value[0][0]);
}

void Program::uniformValue(unsigned int id, Transform const& value) const {
	auto const matrix = value.matrix();
	gl::glUniformMatrix4fv(id, 1, gl::GL_FALSE, &matrix[0][0]);
}

void Program::fragmentLocation(std::string_view name, gl::GLuint location) {
	gl::glBindFragDataLocation(_id, location, name.data());
}

auto Program::uniform_location(std::string_view var) const& -> int {
	assert(*(var.data() + var.length()) == '\0');
	auto it = _locations->find(var);
	
	if (it != _locations->end()) {
		return it->second;
	} else {
		auto varName = std::string{var};
		auto location = gl::glGetUniformLocation(_id, varName.data());
	
		if (location != -1) {
			_locations->emplace(varName, location);
		}
		
		return location;
	}
}

void Program::mapUniform(char const* name, std::string mappedName) {
	auto location = gl::glGetUniformLocation(_id, name);
	
	if (location != -1) {
		(*_locations)[mappedName] = location;
	}
}

auto Program::id() const& -> unsigned int {
	return _id;
}
