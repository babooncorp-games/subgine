#pragma once

#include "subgine/common/lookup_table.h"

#include <string_view>
#include <glbinding/gl/enum.h>

namespace sbg::detail {

inline constexpr auto modes = sbg::makeLookupTable<std::string_view, gl::GLenum>({
	{"GL_POINTS", gl::GL_POINTS},
	{"GL_LINE_STRIP", gl::GL_LINE_STRIP},
	{"GL_LINE_LOOP", gl::GL_LINE_LOOP},
	{"GL_LINES", gl::GL_LINES},
	{"GL_LINE_STRIP_ADJACENCY", gl::GL_LINE_STRIP_ADJACENCY},
	{"GL_LINES_ADJACENCY", gl::GL_LINES_ADJACENCY},
	{"GL_TRIANGLE_STRIP", gl::GL_TRIANGLE_STRIP},
	{"GL_TRIANGLE_FAN", gl::GL_TRIANGLE_FAN},
	{"GL_TRIANGLES", gl::GL_TRIANGLES},
	{"GL_TRIANGLE_STRIP_ADJACENCY", gl::GL_TRIANGLE_STRIP_ADJACENCY},
	{"GL_TRIANGLES_ADJACENCY", gl::GL_TRIANGLES_ADJACENCY},
	{"GL_PATCHES", gl::GL_PATCHES}
});

} // namespace sbg::detail
