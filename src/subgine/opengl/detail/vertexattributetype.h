#pragma once

#include <cstdint>
#include <cstddef>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/types.h>

#include "subgine/common/define.h"
#include "subgine/common/type_id.h"

namespace sbg::detail {

template<typename>
inline constexpr gl::GLenum vertexAttributeType = gl::GL_NONE;

template<>
inline constexpr gl::GLenum vertexAttributeType<float> = gl::GL_FLOAT;

template<>
inline constexpr gl::GLenum vertexAttributeType<double> = gl::GL_DOUBLE;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::int32_t> = gl::GL_INT;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::uint32_t> = gl::GL_UNSIGNED_INT;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::int16_t> = gl::GL_SHORT;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::uint16_t> = gl::GL_UNSIGNED_SHORT;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::int8_t> = gl::GL_BYTE;

template<>
inline constexpr gl::GLenum vertexAttributeType<std::uint8_t> = gl::GL_UNSIGNED_BYTE;

constexpr auto id_to_gltype(type_id_t type) {
	switch(type) {
		case type_id<float>: return gl::GL_FLOAT;
		case type_id<double>: return gl::GL_DOUBLE;
		case type_id<std::int32_t>: return gl::GL_INT;
		case type_id<std::uint32_t>: return gl::GL_UNSIGNED_INT;
		case type_id<std::int16_t>: return gl::GL_SHORT;
		case type_id<std::uint16_t>: return gl::GL_UNSIGNED_SHORT;
		case type_id<std::int8_t>: return gl::GL_BYTE;
		case type_id<std::uint8_t>: return gl::GL_UNSIGNED_BYTE;
		default: return gl::GL_NONE;
	}
}

constexpr auto visit_gltype(gl::GLenum type, auto&& function) noexcept -> decltype(auto) {
	switch (type) {
		case gl::GL_FLOAT: return FWD(function).template operator()<gl::GLfloat>();
		case gl::GL_DOUBLE: return FWD(function).template operator()<gl::GLdouble>();
		case gl::GL_INT: return FWD(function).template operator()<gl::GLint>();
		case gl::GL_UNSIGNED_INT: return FWD(function).template operator()<gl::GLuint>();
		case gl::GL_SHORT: return FWD(function).template operator()<gl::GLshort>();
		case gl::GL_UNSIGNED_SHORT: return FWD(function).template operator()<gl::GLushort>();
		case gl::GL_BYTE: return FWD(function).template operator()<gl::GLbyte>();
		case gl::GL_UNSIGNED_BYTE: return FWD(function).template operator()<gl::GLubyte>();
		default: std::abort();
	}
}

constexpr auto size_of_gltype(gl::GLenum type) noexcept -> std::size_t {
	return visit_gltype(type, []<typename T>() {
		return sizeof(T);
	});
}

} // namespace sbg::detail

#include "subgine/common/undef.h"
