#pragma once

#include <glbinding/gl/gl.h>

#include <string_view>

namespace sbg {

struct Shader {
	void create(gl::GLenum);
	void free();
	
	void build(std::string_view source);
	auto id() const noexcept -> gl::GLuint;
	
protected:
	gl::GLuint _id = 0;
};

} // namespace sbg
