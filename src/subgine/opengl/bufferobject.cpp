#include "bufferobject.h"

#include "detail/vertexattributetype.h"

#include <glbinding/gl46/gl.h>

#include <numeric>
#include <cassert>

using namespace sbg;

BufferObject::BufferObject(gl::GLuint id, gl::GLenum target) noexcept : _id{id}, _target{target} {}

void BufferObject::bind() const& noexcept {
	gl::glBindBuffer(_target, _id);
}

void BufferObject::unbind() const& noexcept {
	gl::glBindBuffer(_target, 0);
}

void BufferObject::free() & noexcept {
	if (_id != 0) {
		gl::glDeleteBuffers(1, &_id);
	}
}

void BufferObject::create() & noexcept {
	assert(_id == 0);
	gl::glGenBuffers(1, &_id);
}

auto BufferObject::id() const& noexcept -> gl::GLuint {
	return _id;
}

void BufferObject::data(std::span<std::byte const> data, gl::GLenum usage) const& noexcept {
	bind();
	gl::glBufferData(_target, data.size(), data.data(), usage);
}

auto BufferObjectArray::create() & noexcept -> void {
	assert(_buffers.size() > 0);
	gl::glGenBuffers(static_cast<gl::GLsizei>(_buffers.size()), _buffers.data());
}

auto BufferObjectArray::free() & noexcept -> void {
	if (_buffers.empty()) return;
	
	if (_buffers.front() != 0) {
		gl::glDeleteBuffers(static_cast<gl::GLsizei>(_buffers.size()), _buffers.data());
	}
	
	_buffers.assign(_buffers.size(), 0);
	_targets.assign(_targets.size(), gl::GL_NONE);
}

auto BufferObjectArray::buffer(std::size_t index) const& noexcept -> BufferObject {
	assert(index < _buffers.size());
	return BufferObject{_buffers[index], _targets[index]};
}

auto BufferObjectArray::format(std::size_t index) const& noexcept -> FormatRef {
	assert(index < _formats.size());
	return FormatRef{_formats[index].first};
}

auto BufferObjectArray::size() const& noexcept -> std::size_t {
	return _buffers.size();
}

auto BufferObjectArray::data(std::size_t i, std::span<std::byte const> data, gl::GLenum target, gl::GLenum flag, BufferObjectFormat format) & noexcept -> void {
	assert(i < _buffers.size());
	assert(_buffers[i] != 0);
	
	auto const size = std::accumulate(
		format.begin(), format.end(),
		std::size_t{0},
		[](std::size_t stride, BufferObjectFormatPart const& item) {
			return stride + detail::size_of_gltype(item.type) * item.size;
		}
	);
	
	_targets[i] = target;
	_formats[i] = {std::move(format), size};
	
	buffer(i).bind();
	gl::glBufferData(target, data.size(), data.data(), flag);
	buffer(i).unbind();
}

auto BufferObjectArray::buffers() const& noexcept -> BufferRange {
	return BufferRange{
		BufferIterator{_formats.begin(), _buffers.begin(), _targets.begin()},
		BufferIterator{_formats.end(), _buffers.end(), _targets.end()}
	};
}
