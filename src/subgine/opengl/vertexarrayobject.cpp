#include "vertexarrayobject.h"

#include "subgine/log.h"

#include <cassert>

using namespace sbg;

auto VertexArrayObject::attach(ResourceHandle<BufferObjectArray> const& bufferArray) -> void {
	_refs.emplace_back(bufferArray.token());
	attach(*bufferArray);
}

auto VertexArrayObject::attach(BufferObjectArray const& bufferArray) noexcept -> void {
	assert_bound();
	for (auto const& [buffer, format, stride] : bufferArray.buffers()) {
		attach_helper(buffer, format, stride);
	}
}

auto VertexArrayObject::attach_part(BufferObject buffer, BufferObjectFormatPart format, std::size_t stride) noexcept -> void {
	assert_bound();
	attach_helper(buffer, std::array{format}, stride);
}

void VertexArrayObject::create() noexcept {
	assert(_id == 0);
	gl::glGenVertexArrays(1, &_id);
}

void VertexArrayObject::free() noexcept  {
	if (_id != 0) {
		gl::glDeleteVertexArrays(1, &_id);
		_id = 0;
	}
}

auto VertexArrayObject::attribute_count() const& noexcept -> std::size_t {
	return _index;
}

void VertexArrayObject::assert_bound() const noexcept {
#ifndef NDEBUG
	auto current_vao = gl::GLint{};
	gl::glGetIntegerv(gl::GL_VERTEX_ARRAY_BINDING, &current_vao);
	assert(current_vao == static_cast<gl::GLint>(_id));
#endif
}

auto VertexArrayObject::attribute_pointer(gl::GLuint index, gl::GLenum type, gl::GLint size, gl::GLsizei stride, std::size_t offset) noexcept -> void {
	gl::glEnableVertexAttribArray(index);
	auto offsetPtr = reinterpret_cast<void const*>(offset);
	if (type == gl::GL_DOUBLE) {
		gl::glVertexAttribLPointer(index, size, type, stride, offsetPtr);
	} else if (type == gl::GL_FLOAT) {
		gl::glVertexAttribPointer(index, size, type, gl::GL_FALSE, stride, offsetPtr);
	} else {
		gl::glVertexAttribIPointer(index, size, type, stride, offsetPtr);
	}
}

void VertexArrayObject::bind() const noexcept {
	gl::glBindVertexArray(_id);
}

void VertexArrayObject::unbind() const noexcept {
	gl::glBindVertexArray(0);
}

auto VertexArrayObject::id() const noexcept -> gl::GLuint {
	return _id;
}

auto VertexArrayObject::next_index() noexcept -> gl::GLuint {
	return _index++;
}
