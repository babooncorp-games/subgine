#include "spritepainter.h"

#include "../utility/membersofprogram.h"

#include "subgine/graphic/texturetag.h"
#include "subgine/graphic/utility/resourceuniformhelper.h"
#include "subgine/graphic/utility/uniformhelper.h"
#include "subgine/resource/utility/resourceunwrap.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <ranges>
#include <algorithm>

using namespace sbg;
using namespace sbg::literals;

SpritePainter::SpritePainter(TextureManager textureManager, ResourceHandle<Program> program, SharedArrayMesh mesh) noexcept :
	_textureManager{textureManager},
	_program{std::move(program)},
	_drawCalls{members_of_program(*_program)},
	_mesh{std::move(mesh)}
{}

void SpritePainter::add(ModelInfo model, UniformSet const& environment) {
	auto const visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	
	if (visible) {
		insert_call(std::move(model), environment);
	}
}

void SpritePainter::draw() const {
	if (_drawCalls.empty() or !_mesh.vao().id()) [[unlikely]] return;
	
	// We bind the vao and the program (shader)
	_mesh.vao().bind();
	_program->bind();
	
	for (auto i = std::size_t{0} ; i < _drawCalls.size() ; ++i) {
		auto const& drawData = _drawData[i];
		auto const& drawUniforms = _drawCalls[i];
		
		// Bind All textures
		for (auto tex_id = std::size_t{0} ; tex_id < drawData.textures.size() ; ++tex_id) {
			gl::glActiveTexture(gl::GL_TEXTURE0 + static_cast<std::int32_t>(tex_id));
			drawData.textures[tex_id].bind();
		}
		
		// Set all the uniform for an object
		read_object_uniforms(drawUniforms, [&](std::uint32_t id, auto const& data) {
			_program->uniformValue(id, data);
		});
		
		// Draw the object
		_mesh.draw();
	}
}

void SpritePainter::prepare(UniformSet const& context) {
	_entityModel.cleanup();
	
	auto drawIndex = std::size_t{0};
	for (auto index = std::size_t{0} ; index < _entityModel.size() ; ++index) {
		if (!_entityModel.alive(index)) continue;
		auto const& model = _entityModel[index];
		auto const& owner = _ownerData[index];
		auto& draw_data = _drawData[drawIndex];
		
		auto texture_unit = [unit = 0](GenericResource resource) mutable -> UniformData {
			if (resource.is<TextureTag>()) {
				return unit++;
			} else {
				// Ignore this uniform
				return resource;
			}
		};
		
		write_object_uniforms(_drawCalls[drawIndex], texture_unit, model.uniforms, context);
		draw_data.textures.clear();
		std::ranges::copy(owner.textures | views::unwrap_resources, std::back_inserter(draw_data.textures));
		++drawIndex;
	}
	
	_drawCalls.resize(drawIndex);
}

void SpritePainter::update(ModelInfo model, UniformSet const& environment) {
	auto const visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	auto const index = _entityModel.find(model.handle);
	
	if (visible) {
		if (index == _entityModel.size()) {
			insert_call(model, environment);
		} else {
			auto const& uniforms = _entityModel[index].uniforms = std::move(model.uniforms);
			auto& owner = _ownerData[index];
			
			owner.textures.clear();
			std::ranges::copy(
				resources_in_object<TextureTag>(_drawCalls.type(), uniforms, environment)
					| views::load_resources(_textureManager),
				std::back_inserter(owner.textures)
			);
		}
	} else {
		if (index != _entityModel.size()) {
			remove_call(model, index);
		}
	}
}

void SpritePainter::refresh(UniformSet const& environment) {
	_entityModel.cleanup();
	
	for (auto index = std::size_t{0}; index < _entityModel.size(); ++index) {
		if (!_entityModel.alive(index)) continue;
		auto const& model = _entityModel[index];
		auto& owner = _ownerData[index];
		
		owner.textures.clear();
		std::ranges::copy(
			resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment)
				| views::load_resources(_textureManager),
			std::back_inserter(owner.textures)
		);
	}
}

void SpritePainter::remove(ModelInfo const& model) {
	auto const found_index = _entityModel.find(model.handle);
	
	if (found_index != _entityModel.size()) {
		remove_call(model, found_index);
	} else {
		Log::warning(SBG_LOG_INFO, "The model attached to entity", log::enquote(model.entity.id().index), "cannot be found in painter for removal");
	}
}

void SpritePainter::insert_call(ModelInfo m, UniformSet const& environment) {
	auto&& [model, index] = _entityModel.insert(std::move(m));
	
	if (_ownerData.size() <= index) {
		_ownerData.resize(index + 1);
	}
	
	auto& owner = _ownerData[index];
	owner.textures.clear();
	std::ranges::copy(
		resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment)
			| views::load_resources(_textureManager),
		std::back_inserter(owner.textures)
	);
	
	_drawCalls.grow(1);
	auto const size = _drawCalls.size();
	if (_drawData.size() <= size) {
		_drawData.resize(size + 1);
		_drawData.back().textures.reserve(owner.textures.size());
	}
}

void SpritePainter::remove_call(ModelInfo const& model, std::size_t index) noexcept {
	_entityModel.erase(model);
	_drawCalls.shrink(1);
	_ownerData[index] = {};
}
