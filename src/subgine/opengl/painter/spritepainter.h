#pragma once

#include "subgine/graphic/environmentuniforms.h"
#include "subgine/graphic/uniform.h"
#include "subgine/graphic/dynstructbuffer.h"
#include "subgine/graphic/modelslotmap.h"
#include "subgine/graphic/texturetag.h"
#include "subgine/resource/resourcehandle.h"
#include "subgine/graphic/computeduniforms.h"
#include "../program.h"
#include "../texture.h"
#include "../factory/texturefactory.h"
#include "../arraymesh.h"

#include "subgine/common/kangaru.h"

#include <vector>

namespace sbg {

struct SpritePainter {
	SpritePainter(TextureManager textureManager, ResourceHandle<Program> program, SharedArrayMesh mesh) noexcept;
	
	void add(ModelInfo model, UniformSet const& environment);
	void draw() const;
	void prepare(UniformSet const& context);
	void update(ModelInfo model, UniformSet const& environment);
	void refresh(UniformSet const& environment);
	void remove(ModelInfo const& model);
	
private:
	struct DrawData {
		std::vector<Texture> textures;
	};
	
	struct OwnerData {
		std::vector<ResourceHandle<Texture>> textures = {};
	};
	
	void insert_call(ModelInfo model, UniformSet const& environment);
	void remove_call(ModelInfo const& model, std::size_t index) noexcept;
	
	ModelSlotMap _entityModel;
	TextureManager _textureManager;
	std::vector<DrawData> _drawData;
	std::vector<OwnerData> _ownerData;
	ResourceHandle<Program> _program;
	DynStructBuffer _drawCalls;
	SharedArrayMesh _mesh;
	
	friend auto service_map(SpritePainter const&) -> kgr::autowire;
};

using SpritePainterService = kgr::mapped_service_t<SpritePainter const&>;

} // namespace sbg
