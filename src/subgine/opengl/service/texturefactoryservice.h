#pragma once

#include "../factory/texturefactory.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TextureLoaderService : kgr::service<TextureLoader, kgr::autowire> {};

using TextureFactoryService = ResourceFactoryService<TextureLoader>;
using TextureManagerService = ResourceManagerService<TextureManager>;

} // namespace sbg
