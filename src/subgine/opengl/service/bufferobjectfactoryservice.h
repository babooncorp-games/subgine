#pragma once

#include "../factory/bufferobjectfactory.h"

#include "subgine/resource/service/resourcemanagerservice.h"
#include "subgine/resource/service/resourcefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BufferObjectArrayLoaderService : kgr::service<BufferObjectArrayLoader>{};
using BufferObjectArrayFactoryService = ResourceFactoryService<BufferObjectArrayFactory>;
using BufferObjectArrayManagerService = ResourceManagerService<BufferObjectArrayManager>;

} // namespace sbg
