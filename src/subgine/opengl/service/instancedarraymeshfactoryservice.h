#pragma once

#include "../factory/instancedarraymeshfactory.h"

#include "vertexarrayobjectfactoryservice.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct InstancedArrayMeshLoaderService : kgr::service<InstancedArrayMeshLoader, kgr::autowire> {};
using InstancedArrayMeshFactoryService = ResourceFactoryService<InstancedArrayMeshFactory>;

} // namespace sbg
