#pragma once

#include "../factory/shaderfactory.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ShaderLoaderService : kgr::service<ShaderLoader> {};

using ShaderFactoryService = ResourceFactoryService<ShaderFactory>;
using ShaderManagerService = ResourceManagerService<ShaderManager>;

} // namespace sbg
