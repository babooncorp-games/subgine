#pragma once

#include "../factory/programfactory.h"

#include "../service/shaderfactoryservice.h"

#include "subgine/resource/service/jsonmanagerservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"
#include "subgine/resource/service/resourcefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ProgramLoaderService : kgr::service<ProgramLoader, kgr::autowire> {};

using ProgramFactoryService = ResourceFactoryService<ProgramFactory>;
using ProgramManagerService = ResourceManagerService<ProgramManager>;

} // namespace sbg
