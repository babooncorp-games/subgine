#pragma once

#include "../factory/arraymeshfactory.h"

#include "vertexarrayobjectfactoryservice.h"

#include "subgine/resource/service/resourcefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ArrayMeshLoaderService : kgr::service<ArrayMeshLoader, kgr::autowire> {};
using ArrayMeshFactoryService = ResourceFactoryService<ArrayMeshFactory>;

} // namespace sbg
