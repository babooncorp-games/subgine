#pragma once

#include "../factory/sharedarraymeshfactory.h"

#include "vertexarrayobjectfactoryservice.h"

#include "subgine/resource/service/resourcefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SharedArrayMeshLoaderService : kgr::service<SharedArrayMeshLoader, kgr::autowire> {};
using SharedArrayMeshFactoryService = ResourceFactoryService<SharedArrayMeshFactory>;

} // namespace sbg
