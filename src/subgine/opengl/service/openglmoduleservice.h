#pragma once

#include "../openglmodule.h"

#include "subgine/graphic/service/paintercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct OpenglModuleService : kgr::single_service<OpenglModule>,
	autocall<
		&OpenglModule::setupPainterCreator
	> {};

} // namespace sbg
