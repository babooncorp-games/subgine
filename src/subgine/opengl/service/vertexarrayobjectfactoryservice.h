#pragma once

#include "../factory/vertexarrayobjectfactory.h"

#include "bufferobjectfactoryservice.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct VertexArrayObjectFactoryService : kgr::service<VertexArrayObjectFactory, kgr::autowire> {};
using VertexArrayObjectManagerService = ResourceManagerService<VertexArrayObjectManager>;

} // namespace sbg
