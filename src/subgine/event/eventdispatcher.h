#pragma once

#include "detail/subscriber.h"
#include "detail/subscription.h"
#include "utility/trait.h"

#include "subgine/common/type_id.h"
#include "subgine/common/map.h"

#include <string>
#include <unordered_map>
#include <algorithm>
#include <limits>
#include <vector>

namespace sbg {

struct EventDispatcherService;

/**
 * This class is an implementation of the mediator pattern. It dispatch events to subscribers.
 * 
 * It serves as a general purpose mediator that can work with any type.
 */
struct EventDispatcher {
	struct SubscriptionHandle {
		SubscriptionHandle() = default;
		explicit SubscriptionHandle(
			EventDispatcher& dispatcher,
			type_id_t type,
			std::uint16_t id
		) noexcept :
			_dispatcher{&dispatcher}, _type{type}, _id{id} {}
		
		/**
		 * Returns if the subscription is still valid and listen to events.
		 */
		[[nodiscard]]
		auto subscribed() const noexcept -> bool;
		
		/**
		 * Unsubscribe the referenced subscriber.
		 * 
		 * Can only be called on a valid subscription. See `SubscriptionHandle::subscribed`.
		 */
		void unsubscribe() const noexcept;
		
	private:
		EventDispatcher* _dispatcher = nullptr;
		type_id_t _type = null_type_id;
		std::uint16_t _id = nullid;
	};

	/**
	 * This function has the purpose of dispatching events.
	 * 
	 * Dispatched events can be any copiable types.
	 * The event will be dispatched to any subscriber that listen to the type of event E.
	 */
	template<typename E>
	void dispatch(E const& event) {
		if (_needCleanup && !_shouldClear) {
			cleanup();
		} else if (_shouldClear) {
			_subscriptions.clear();
			_chainedDispatchers.clear();
			return;
		}
		
		auto& e_subscriptions = _subscriptions[type_id<E>];
		
		for (auto& subscription : e_subscriptions) {
			subscription.template as<AbstractSubscriber<E>>().handle(event);
		}
		
		auto chainedDispatchers = _chainedDispatchers;
		for (auto const& dispatcher : chainedDispatchers) {
			dispatcher->dispatch(event);
		}
	}
	
	/**
	 * This function subscribes an object to a certain type of event.
	 * /
	 * A subscriber must have a handle(E) function or the operator()(E) overloaded.
	 * The handle or operator() function will be called every time an event of the type E is dispatched.
	 * 
	 * If the subscriber is moved to the event dispatcher, the event dispatcher will be the owner of the subscriber.
	 * 
	 * The parameter `subscriber` must be a handler of some sort or this function won't be a valid overload.
	 */
	template<typename E, typename T, enable_if_i<is_handler<std::decay_t<T>, E>> = 0>
	auto subscribe(T&& subscriber) -> SubscriptionHandle {
		_needCleanup = true;
		// We add the subscriber to the list to be added.
		// The `T` parameter is not decayed, even if T is a forwarding reference.
		auto& subscription = _addedSubscriptions.emplace_back(type_id<E>, makeSubscription<T, E>(std::forward<T>(subscriber), _nextId++));
		
		return SubscriptionHandle{*this, type_id<E>, subscription.second.id};
	}
	
	/**
	 * This function subscribes an object to a certain type of event using deduction.
	 * 
	 * A subscriber must have a handle(A) function or the operator()(A) overloaded.
	 * If it has many handle function or many operator(), the deduction process will fail.
	 * 
	 * This function will call `subscribe` with the deduced event type.
	 * 
	 * The parameter `subscriber` must be a handler of some sort or this function won't be a valid overload.
	 * The parameter `subscriber` must have a function `handle(A)` or `operator()(A)` without oveloads, this function won't be a valid overload.
	 */
	template<typename T, enable_if_i<is_deductible_handler<std::decay_t<T>>> = 0>
	auto subscribe(T&& subscriber) -> SubscriptionHandle {
		// We call subscribe with the deduced event type
		return subscribe<deduced_event_t<std::decay_t<T>>>(std::forward<T>(subscriber));
	}
	
	/**
	 * This subscribe function will chain the dispatcher
	 * All dispatched event will be forwarded to this one too
	 */
	void subscribe(EventDispatcher& dispatcher);
	
	/**
	 * This function unsubscribes all object subscribed to a certain type of event.
	 * 
	 * The handle or operator() function will no longer be called.
	 */
	template<typename T>
	void unsubscribe(T const& subscriber) {
		_needCleanup = true;
		for (auto& [type, subscriptions] : _subscriptions) {
			for (auto& subscription : subscriptions) {
				if (subscription.handle == &subscriber) {
					subscription.expired = true;
				}
			}
		}
		
		for (auto& [type, subscription] : _addedSubscriptions) {
			if (subscription.handle == &subscriber) {
				subscription.expired = true;
			}
		}
	}
	
	/**
	 * Remove the chaining of a dispatcher
	 * No further events will be forwarded to the unsubscribed dispatcher.
	 */
	void unsubscribe(EventDispatcher const& dispatcher);
	void clear();
	
private:
	template<typename T, typename E, typename Arg>
	static Subscription makeSubscription(Arg&& arg, std::uint16_t id) {
		return Subscription{&arg, Subscriber<T, E>{std::forward<Arg>(arg)}, id};
	}
	
	void cleanup();
	
	std::vector<std::pair<type_id_t, Subscription>> _addedSubscriptions;
	hash_map<type_id_t, std::vector<Subscription>> _subscriptions;
	std::vector<EventDispatcher*> _chainedDispatchers;
	std::uint16_t _nextId = 0;
	bool _shouldClear = false;
	bool _needCleanup = false;
	
	static constexpr auto nullid = std::numeric_limits<std::uint16_t>::max();
	
	friend auto service_map(EventDispatcher const&) -> EventDispatcherService;
};

} // namespace sbg
