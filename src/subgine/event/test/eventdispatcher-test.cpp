#include "catch2/catch.hpp"

#include "../eventdispatcher.h"

struct Event1 {};
struct Event2 {};

struct Handler1 {
	int called = 0;
	void handle(Event1) { ++called; }
};

struct HandlerMulti {
	int called1 = 0;
	int called2 = 0;
	void handle(Event1) { ++called1; }
	void handle(Event2) { ++called2; }
};

TEST_CASE("EventDispatcher", "[event]") {
	auto events = sbg::EventDispatcher{};
	
	SECTION("Can subscribe a handler") {
		auto handler = Handler1{};
		auto subscription = events.subscribe(handler);
		REQUIRE(subscription.subscribed());
		
		SECTION("and unsubscribe it using the handle") {
			subscription.unsubscribe();
			REQUIRE(!subscription.subscribed());
		}
		
		SECTION("and unsubscribe it with it's reference") {
			events.unsubscribe(handler);
			REQUIRE(!subscription.subscribed());
		}
	}
	
	SECTION("Can dispatch to a handler") {
		auto handler = Handler1{};
		events.subscribe(handler);
		
		SECTION("One time") {
			events.dispatch(Event1{});
			REQUIRE(handler.called == 1);
		}
		
		SECTION("Multiple times") {
			events.dispatch(Event1{});
			events.dispatch(Event1{});
			events.dispatch(Event1{});
			REQUIRE(handler.called == 3);
		}
		
		SECTION("Ignoring other events") {
			events.dispatch(Event1{});
			events.dispatch(Event2{});
			REQUIRE(handler.called == 1);
		}
	}
	
	SECTION("Won't dispatch to an unsubscribed handler") {
		auto handler = Handler1{};
		events.subscribe(handler);
		events.unsubscribe(handler);
		
		events.dispatch(Event1{});
		
		REQUIRE(handler.called == 0);
	}
	
	SECTION("Can unsubscribe lambda handler using it's subscription") {
		int called = 0;
		auto subscription = events.subscribe([&](Event1){ ++called; });
		
		events.dispatch(Event1{});
		REQUIRE(called == 1);
		
		subscription.unsubscribe();
		
		events.dispatch(Event1{});
		REQUIRE(called == 1);
	}
	
	SECTION("Can subscribe a multi-handler") {
		auto multi = HandlerMulti{};
		
		auto sub1 = events.subscribe<Event1>(multi);
		auto sub2 = events.subscribe<Event2>(multi);
		
		REQUIRE(sub1.subscribed());
		REQUIRE(sub2.subscribed());
		
		SECTION("and can unsubscribe in order") {
			sub1.unsubscribe();
			REQUIRE(!sub1.subscribed());
			REQUIRE(sub2.subscribed());
			sub2.unsubscribe();
			REQUIRE(!sub1.subscribed());
			REQUIRE(!sub2.subscribed());
		}
		
		SECTION("and can unsubscribe in reverse order") {
			sub2.unsubscribe();
			REQUIRE(sub1.subscribed());
			REQUIRE(!sub2.subscribed());
			sub1.unsubscribe();
			REQUIRE(!sub1.subscribed());
			REQUIRE(!sub2.subscribed());
		}
		
		SECTION("and can dispatch distinct event types") {
			events.dispatch(Event1{});
			REQUIRE(multi.called1 == 1);
			REQUIRE(multi.called2 == 0);
			events.dispatch(Event1{});
			REQUIRE(multi.called1 == 2);
			REQUIRE(multi.called2 == 0);
			events.dispatch(Event2{});
			REQUIRE(multi.called1 == 2);
			REQUIRE(multi.called2 == 1);
		}
		
		SECTION("and can be unsubscribed using the dispatcher") {
			events.unsubscribe(multi);
			REQUIRE(!sub1.subscribed());
			REQUIRE(!sub2.subscribed());
			events.dispatch(Event1{});
			REQUIRE(multi.called1 == 0);
			REQUIRE(multi.called2 == 0);
			events.dispatch(Event2{});
			REQUIRE(multi.called1 == 0);
			REQUIRE(multi.called2 == 0);
			
		}
	}
}
