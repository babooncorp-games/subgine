#pragma once

#include "subgine/common/traits.h"
#include "subgine/common/function_traits.h"

namespace sbg {

template<typename, typename, typename = void>
inline constexpr auto has_handle = false;

template<typename T, typename A>
inline constexpr auto has_handle<T, A, std::void_t<decltype(std::declval<T>().handle(std::declval<A&>()))>> = true;

template<typename T, typename A>
inline constexpr auto is_callable_handler = !has_handle<T, A> && std::is_invocable_v<T, A>;

template<typename T, typename A>
inline constexpr auto is_handler = has_handle<T, A> || std::is_invocable_v<T, A>;

template<typename, typename = void>
inline constexpr auto has_deductible_handle = false;

template<typename T>
inline constexpr auto has_deductible_handle<T, std::void_t<decltype(&T::handle)>> = function_arity_v<decltype(&T::handle)> == 1;

template<typename, typename = void>
inline constexpr bool has_deductible_callable = false;

template<typename T>
inline constexpr bool has_deductible_callable<T, std::enable_if_t<has_call_operator_v<T>>> = function_arity_v<T> == 1;

template<typename T>
inline constexpr bool is_deductible_handler = has_deductible_callable<T> || has_deductible_handle<T>;

template<typename, typename = void>
struct deduced_event {};

template<typename T>
struct deduced_event<T, std::enable_if_t<has_deductible_handle<T>>> {
	using type = std::remove_cv_t<std::remove_reference_t<function_argument_t<0, decltype(&T::handle)>>>;
};

template<typename T>
struct deduced_event<T, std::enable_if_t<!has_deductible_handle<T> && has_deductible_callable<T>>> {
	using type = std::remove_cv_t<std::remove_reference_t<function_argument_t<0, T>>>;
};

template<typename T>
using deduced_event_t = typename deduced_event<T>::type;

} // namespace sbg
