#include "eventdispatcher.h"

#include "subgine/log.h"
#include "subgine/common/algorithm.h"

using namespace sbg;

void EventDispatcher::SubscriptionHandle::unsubscribe() const noexcept {
	auto& subscription_of_type = _dispatcher->_subscriptions[_type];
	
	for (auto& subscription : subscription_of_type) {
		if (subscription.id == _id) {
			subscription.expired = true;
			_dispatcher->_needCleanup = true;
			return;
		}
	}
	
	for (auto& [type, subscriber] : _dispatcher->_addedSubscriptions) {
		if (type == _type && subscriber.id == _id) {
			subscriber.expired = true;
			_dispatcher->_needCleanup = true;
			return;
		}
	}
}

auto EventDispatcher::SubscriptionHandle::subscribed() const noexcept -> bool {
	if (!_dispatcher || _type == null_type_id || _id == nullid) return false;
	
	auto const& subscriptions_of_type = _dispatcher->_subscriptions[_type];
	
	for (auto const& subscription : subscriptions_of_type) {
		if (subscription.id == _id) return !subscription.expired;
	}
	
	for (auto const& [type, subscriber] : _dispatcher->_addedSubscriptions) {
		if (type == _type && subscriber.id == _id) return !subscriber.expired;
	}
	
	return false;
}

void EventDispatcher::clear() {
	_shouldClear = true;
}

void EventDispatcher::subscribe(EventDispatcher& dispatcher) {
	auto alreadyChained = [&]{
		return std::find(_chainedDispatchers.begin(), _chainedDispatchers.end(), &dispatcher) != _chainedDispatchers.end();
	};
	
	Log::error(alreadyChained, SBG_LOG_INFO, "Chaining an already chained event dispatcher");
	
	_chainedDispatchers.emplace_back(&dispatcher);
}

void EventDispatcher::unsubscribe(EventDispatcher const& dispatcher) {
	auto it = std::find(std::begin(_chainedDispatchers), std::end(_chainedDispatchers), &dispatcher);
	
	if (it != _chainedDispatchers.end()) {
		*it = nullptr;
	}
}

void EventDispatcher::cleanup() {
	_chainedDispatchers.erase(
		std::remove(_chainedDispatchers.begin(), _chainedDispatchers.end(), nullptr),
		_chainedDispatchers.end()
	);
	
	for (auto& [type, subscriptions] : _subscriptions) {
		std::erase_if(subscriptions, [](auto const& subscription) {
			return subscription.expired;
		});
	}
	
	_subscriptions.reserve(_subscriptions.size() + _addedSubscriptions.size());
	
	for (auto& [type, subscription] : _addedSubscriptions) {
		if (not subscription.expired) {
			_subscriptions[type].emplace_back(std::move(subscription));
		}
	}
	
	_addedSubscriptions.clear();
	_needCleanup = false;
}
