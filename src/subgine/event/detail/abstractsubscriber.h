#pragma once

#include "subgine/common/types.h"

namespace sbg {

template<typename A>
struct AbstractSubscriber {
	virtual ~AbstractSubscriber() = default;
	virtual void handle(A const& action) = 0;
};

} // namespace sbg
