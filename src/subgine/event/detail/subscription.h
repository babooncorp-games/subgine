#pragma once

#include "subgine/common/erasedtype.h"
#include "subscriber.h"

#include <memory>

namespace sbg {

/**
 * This class is a named pair.
 * 
 * This class mimic the purpose of std::pair but with betters name for members.
 * It serves as eye candy for the event dispatcher too.
 * 
 * It contains the pointer to the subscriber and a pointer to the object the subscriber refers to.
 * If the subscriber is owner, the handle will be null.
 */
struct Subscription : ErasedType {
	template<typename T, typename A>
	explicit Subscription(void const* _handle, Subscriber<T, A>&& _subscriber, std::uint16_t _id) : 
		ErasedType{static_cast<std::unique_ptr<AbstractSubscriber<A>>>(std::make_unique<Subscriber<T, A>>(std::move(_subscriber)))},
		handle{std::is_rvalue_reference<T>::value ? nullptr : _handle}, id{_id} {}
	
	void const* handle = nullptr;
	std::uint16_t id = 0;
	bool expired = false;
};

} // namespace sbg
