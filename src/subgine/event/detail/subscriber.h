#pragma once

#include "abstractsubscriber.h"
#include "../utility/trait.h"

namespace sbg {

template<typename T, typename A>
struct Subscriber : AbstractSubscriber<A>, std::tuple<remove_rvalue_reference_t<T>> {
	using parent = std::tuple<remove_rvalue_reference_t<T>>;
	
	Subscriber(T&& subscriber) : parent{std::forward<T>(subscriber)} {}
	
	void handle(A const& event) override {
		auto& subscriber = std::get<0>(static_cast<parent&>(*this));

		static_assert(is_handler<T, A>, "T must be an handler of A");

		if constexpr (has_handle<T, A>) {
			subscriber.handle(event);
		} else if constexpr (std::is_invocable_v<T, A>) {
			subscriber(event);
		}
	}
};

} // namespace sbg
