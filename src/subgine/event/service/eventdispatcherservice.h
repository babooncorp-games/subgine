#pragma once

#include "../eventdispatcher.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EventDispatcherService : kgr::single_service<EventDispatcher> {};

} // namespace sbg
