#pragma once

#include "../eventmodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct EventModuleService : kgr::single_service<EventModule> {};

} // namespace sbg
