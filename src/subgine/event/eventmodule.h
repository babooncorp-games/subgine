#pragma once

namespace sbg {

struct EventModuleService;

struct EventModule {
	friend auto service_map(const EventModule&) -> EventModuleService;
};

} // namespace sbg
