#pragma once

#include "../tiledopenglmodule.h"

#include "subgine/graphic/service/paintercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TiledOpenglModuleService : kgr::single_service<TiledOpenglModule, kgr::autowire>,
	autocall<
		&TiledOpenglModule::setupPainterCreator
	> {};

} // namespace sbg
