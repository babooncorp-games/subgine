subgine_build_assets(tiled-opengl-assets
	ASSET subgine/tiled-opengl
		TYPE json
		SOURCES tiled-opengl.json
	
	ASSET subgine/shader/tiled-tile-layer-pixel.frag
		TYPE file
		SOURCES shader/tiled-tile-layer-pixel.frag.glsl
	
	ASSET subgine/shader/tiled-tile-layer.frag
		TYPE file
		SOURCES shader/tiled-tile-layer.frag.glsl
	
	ASSET subgine/shader/tiled-tile-layer.vert
		TYPE file
		SOURCES shader/tiled-tile-layer.vert.glsl
)
