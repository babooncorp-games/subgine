#version 460 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in int textureIndex;
layout(location = 3) in ivec2 tilePosition;

layout(location = 0) out vec2 UV;
layout(location = 1) flat out int index;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 parallax_matrix;

void main()
{
	vec4 v = vec4(vertexPosition + vec3(tilePosition, 0), 1);
	
	gl_Position = projection * parallax_matrix * view * model * v;
	
	UV = vertexUV;
	index = textureIndex;
}
