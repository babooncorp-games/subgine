#version 460 core

layout(location = 0) out vec4 color;
layout(location = 0) in vec2 UV;
layout(location = 1) flat in int index;

uniform sampler2DArray tex;
uniform vec4 blendColor;
uniform float opacity;
uniform vec4 tint;

void main() {
	vec4 outColor = texture( tex, vec3(UV, index) );
	outColor = vec4(mix(tint.rgb * outColor.rgb, blendColor.rgb, vec3(blendColor.a)), outColor.a * opacity * tint.a);
	color = outColor;
}
