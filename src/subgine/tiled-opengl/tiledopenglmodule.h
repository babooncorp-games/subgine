#pragma once

namespace sbg {

struct PainterCreator;
struct TiledOpenglModuleService;

struct TiledOpenglModule {
	void setupPainterCreator(PainterCreator& painterCreator) const;
	
	friend auto service_map(TiledOpenglModule const&) -> TiledOpenglModuleService;
};

} // namespace sbg
