#pragma once

#include "subgine/graphic/environmentuniforms.h"
#include "subgine/graphic/uniform.h"
#include "subgine/graphic/dynstructbuffer.h"
#include "subgine/graphic/computeduniforms.h"
#include "subgine/graphic/modelslotmap.h"
#include "subgine/graphic/sortedmodelvector.h"
#include "subgine/opengl/program.h"
#include "subgine/opengl/texture.h"
#include "subgine/opengl/instancedarraymesh.h"
#include "subgine/opengl/factory/texturefactory.h"
#include "subgine/resource/resourcehandle.h"

#include "subgine/common/kangaru.h"

namespace sbg::tiled_opengl {

struct TilePainter {
	using InstancedArrayMeshMaker = std::function<OwnedResource<InstancedArrayMesh>()>;
	
	TilePainter(TextureManager textureManager, ResourceHandle<Program> program, InstancedArrayMeshMaker createArrayMesh) noexcept;
	
	void add(ModelInfo model, UniformSet const& environment);
	void draw() const;
	void prepare(UniformSet const& context);
	void update(ModelInfo model, UniformSet const& environment);
	void refresh(UniformSet const& environment);
	void remove(ModelInfo const& model);
	
private:
	struct DrawData {
		std::vector<Texture> textures;
		InstancedArrayMeshView mesh;
	};
	
	struct OwnerData {
		OwnedResource<InstancedArrayMesh> mesh;
		OwnedResource<BufferObject> buffer;
		std::vector<ResourceHandle<Texture>> textures = {};
	};
	
	void insert_call(ModelInfo model, UniformSet const& environment);
	void remove_call(ModelInfo const& model);
	
	SortedModelVector<int, DrawData, OwnerData> _entityModel;
	TextureManager _textureManager;
	ResourceHandle<Program> _program;
	InstancedArrayMeshMaker _createArrayMesh;
	DynStructBuffer _drawCalls;
	
	friend auto service_map(TilePainter const&) -> kgr::autowire;
};

using TilePainterService = kgr::mapped_service_t<TilePainter const&>;

} // namespace sbg::tiled_opengl
