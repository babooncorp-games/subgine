#include "tilepainter.h"

#include "subgine/graphic/texturetag.h"
#include "subgine/opengl/utility/membersofprogram.h"
#include "subgine/graphic/utility/resourceuniformhelper.h"
#include "subgine/graphic/utility/uniformhelper.h"
#include "subgine/resource/utility/resourceunwrap.h"

#include <algorithm>
#include <ranges>

using namespace sbg;
using namespace sbg::literals;
using namespace sbg::tiled_opengl;

TilePainter::TilePainter(TextureManager textureManager, ResourceHandle<Program> program, InstancedArrayMeshMaker createArrayMesh) noexcept :
	_textureManager{textureManager},
	_program{std::move(program)},
	_createArrayMesh{std::move(createArrayMesh)},
	_drawCalls{members_of_program(*_program)}
{}

void TilePainter::add(ModelInfo model, UniformSet const& environment) {
	auto const visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	
	if (visible) {
		insert_call(std::move(model), environment);
	}
}

void TilePainter::draw() const {
	if (_drawCalls.empty()) [[unlikely]] return;
	
	// We bind the vao and the program (shader)
	_program->bind();
	
	auto const drawDatas = _entityModel.data<DrawData>();
	
	for (auto i = std::size_t{0} ; i < _drawCalls.size() ; ++i) {
		// Bind All textures
		auto const& drawData = drawDatas[i];
		auto const object = _drawCalls[i];
		
		for (auto tex_id = std::size_t{0} ; tex_id < drawData.textures.size() ; ++tex_id) {
			gl::glActiveTexture(gl::GL_TEXTURE0 + static_cast<std::int32_t>(tex_id));
			drawData.textures[tex_id].bind();
		}
		
		drawData.mesh.vao().bind();
		
		// Set all the uniform for an object
		read_object_uniforms(object, [&](std::uint32_t id, auto const& data) {
			_program->uniformValue(id, data);
		});
		
		// Draw the object
		drawData.mesh.draw();
	}
}

void TilePainter::prepare(UniformSet const& context) {
	_entityModel.cleanup();
	auto [drawDatas, owners] = _entityModel.datas();
	
	auto drawIndex = std::size_t{0};
	for (auto index = std::size_t{0} ; index < _entityModel.size() ; ++index) {
		if (!_entityModel.alive(index)) continue;
		auto const& model = _entityModel[index];
		auto& draw_data = drawDatas[drawIndex];
		auto& owner = owners[drawIndex];
		
		auto const texture_unit = [unit = 0](GenericResource resource) mutable -> UniformData {
			if (resource.is<TextureTag>()) {
				return unit++;
			} else {
				return resource;
			}
		};
		
		write_object_uniforms(_drawCalls[drawIndex], texture_unit, model.uniforms, context);
		draw_data.textures.clear();
		std::ranges::copy(owner.textures | views::unwrap_resources, std::back_inserter(draw_data.textures));
		draw_data.mesh = *owner.mesh;
		++drawIndex;
	}
	
	_drawCalls.resize(drawIndex);
}

void TilePainter::update(ModelInfo model, UniformSet const& environment) {
	auto visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	auto const index = _entityModel.find(model.handle);
	
	// TODO: Update tile data
	if (visible) {
		if (index == _entityModel.size()) {
			insert_call(model, environment);
		} else {
			auto const& uniforms = _entityModel[index].uniforms = std::move(model.uniforms);
			auto& owner = _entityModel.data<OwnerData>()[index];
			owner.textures.clear();
			std::ranges::copy(
				resources_in_object<TextureTag>(_drawCalls.type(), uniforms, environment) | views::load_resources(_textureManager),
				std::back_inserter(owner.textures)
			);
		}
	} else {
		if (index != _entityModel.size()) {
			remove_call(model);
		}
	}
}

void TilePainter::refresh(UniformSet const& environment) {
	_entityModel.cleanup();
	
	for (auto index = std::size_t{0}; index < _entityModel.size(); ++index) {
		if (!_entityModel.alive(index)) continue;
		auto const& model = _entityModel[index];
		auto& owner = _entityModel.data<OwnerData>()[index];
		owner.textures.clear();
		std::ranges::copy(
			resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment) | views::load_resources(_textureManager),
			std::back_inserter(owner.textures)
		);
	}
}

void TilePainter::remove(ModelInfo const& model) {
	auto found_index = _entityModel.find(model.handle);
	
	if (found_index != _entityModel.size()) {
		remove_call(model);
	} else {
		Log::warning(SBG_LOG_INFO, "The model attached to entity", log::enquote(model.entity.id().index), "cannot be found in painter for removal");
	}
}

void TilePainter::insert_call(ModelInfo m, UniformSet const& environment) {
	auto const plane = m.uniforms.required<int>("plane"_h);
	auto&& [model, draw_data, owner] = _entityModel.insert(std::move(m), plane, DrawData{}, OwnerData{});
	
	auto& mesh = owner.mesh = _createArrayMesh();
	auto& vao = mesh->vao();
	auto& buffer = owner.buffer;
	auto const& tiles = model.uniforms.required<DynStructBuffer>("tiles"_h);
	
	buffer = OwnedResource{BufferObject{0, gl::GL_ARRAY_BUFFER}};
	buffer->create();
	buffer->data(tiles.buffer(), gl::GL_STATIC_DRAW);
	mesh->instance(static_cast<gl::GLsizei>(tiles.size()));
	draw_data.mesh = *mesh;
	
	auto const basic_attributes = vao.attribute_count();
	
	// TODO: Make this much simpler and readable
	vao.bind();
	for (auto const& member : tiles.type().members) {
		member.visit([&]<typename T>() {
			if constexpr (is_vector<T>) {
				vao.attach_part(*buffer, BufferObjectFormatPart{detail::vertexAttributeType<typename T::type>, T::size, static_cast<gl::GLint>(member.offset)}, tiles.type().total_size());
			} else {
				vao.attach_part(*buffer, BufferObjectFormatPart{detail::vertexAttributeType<T>, 1, static_cast<gl::GLint>(member.offset)}, tiles.type().total_size());
			}
		});
	}
	
	for (auto const& member : tiles.type().members) {
		// TODO: Find a way to do that in the VAO
		gl::glVertexAttribDivisor(basic_attributes + member.id, 1u);
	}
	
	owner.textures.clear();
	std::ranges::copy(
		resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment) | views::load_resources(_textureManager),
		std::back_inserter(owner.textures)
	);
	
	_drawCalls.grow(1);
}

void TilePainter::remove_call(ModelInfo const& model) {
	_entityModel.erase(model);
	_drawCalls.shrink(1);
}
