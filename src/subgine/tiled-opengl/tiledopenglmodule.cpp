#include "tiledopenglmodule.h"


#include "subgine/graphic.h"
#include "subgine/opengl.h"
#include "subgine/resource.h"
#include "subgine/tiled-opengl.h"
#include "subgine/vector.h"
#include "subgine/common/kangaru.h"

using namespace sbg;
using namespace tiled_opengl;

void TiledOpenglModule::setupPainterCreator(PainterCreator& painterCreator) const {
	painterCreator.add("tiled-tile-painter", [](
		kgr::generator<TilePainterService> build,
		ProgramManager programManager,
		InstancedArrayMeshFactory meshFactory,
		Property data
	) {
		return build(
			programManager.get(data["program"]),
			[mesh = std::string{data["mesh"]}, meshFactory] {
				return meshFactory.create(mesh);
			}
		);
	});
}
