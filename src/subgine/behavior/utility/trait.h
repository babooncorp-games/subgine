#pragma once

#include "../status.h"

#include "subgine/common/detection.h"
#include "subgine/common/concepts.h"

#include <utility>
#include <type_traits>

namespace sbg {

template<typename T>
concept behavior_node = object<T> and requires(T& behaviour) {
	{ behaviour.execute() } -> std::same_as<BehaviorStatus>;
};

template<typename T>
concept behavior_lambda = object<T> and requires(T& behavior) {
	{ behavior() } -> std::same_as<BehaviorStatus>;
};

template<typename T>
concept behavior = behavior_node<T> or behavior_lambda<T>;

} // namespace sbg
