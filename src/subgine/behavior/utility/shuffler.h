#pragma once

#include <random>
#include <vector>

#include "../behavior.h"

namespace sbg {

struct BehaviorShufflerService;

struct BehaviorShuffler {
	BehaviorShuffler();
	
	void shuffle(std::vector<Behavior>::iterator begin, std::vector<Behavior>::iterator end);
	
private:
	std::mt19937 random;
	
	friend auto service_map(BehaviorShuffler const&) -> BehaviorShufflerService;
};

} // namespace sbg
