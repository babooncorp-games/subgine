#include "timer.h"

using namespace sbg;

void Timer::start() {
	_startTime = std::chrono::high_resolution_clock::now();
	_running = true;
}

void Timer::stop() {
	_endTime = std::chrono::high_resolution_clock::now();
	_running = false;
}

auto Timer::running() const -> bool {
	return _running;
}

auto Timer::elapsedMilliseconds() const -> double {
	auto const endTime = _running ? std::chrono::high_resolution_clock::now() : _endTime;
	return std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - _startTime).count();
}

auto Timer::elapsedSeconds() const -> double {
	return elapsedMilliseconds() / 1000.0;
}
