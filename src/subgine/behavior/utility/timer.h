#pragma once

#include <chrono>
#include <ctime>
#include <cmath>

namespace sbg {

// https://gist.github.com/mcleary/b0bf4fa88830ff7c882d
struct Timer {
	void start();
	void stop();
	
	[[nodiscard]] auto running() const -> bool;
	[[nodiscard]] auto elapsedMilliseconds() const -> double;
	[[nodiscard]] auto elapsedSeconds() const -> double;

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> _startTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> _endTime;
	bool _running = false;
};

} // namespace sbg
