#include "shuffler.h"

#include <ranges>
#include <algorithm>

using namespace sbg;

template<typename T>
static auto seeded_mersenne_twister() -> T {
	auto source = std::random_device{};
	constexpr auto N = T::state_size * sizeof(typename T::result_type);
	auto random_data =
		  std::views::iota(std::size_t(), (N - 1) / sizeof(source()) + 1)
		| std::views::transform([&](auto){ return source(); });
	
	// std::seed_seq uses initializer list, so we must use parens here
	auto seeds = std::seed_seq(std::begin(random_data), std::end(random_data));
	return T{seeds};
}

BehaviorShuffler::BehaviorShuffler() : random{seeded_mersenne_twister<std::mt19937>()} {}

void BehaviorShuffler::shuffle(std::vector<Behavior>::iterator begin, std::vector<Behavior>::iterator end) {
	std::shuffle(begin, end, random);
}
