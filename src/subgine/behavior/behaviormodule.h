#pragma once

namespace sbg {

struct BehaviorModuleService;

struct BehaviorCreator;
struct ComponentCreator;
struct EntityBindingCreator;
struct MainEngine;
struct BehaviorEngine;

struct BehaviorModule {
	void setupMainEngine(MainEngine& mainEngine, BehaviorEngine& behaviorEngine) const;
	void setupBehaviorCreator(BehaviorCreator& behaviorCreator) const;
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	void setupEntityBindingCreator(EntityBindingCreator& factoryBinder) const;

	friend auto service_map(BehaviorModule const&) -> BehaviorModuleService;
};

} // namespace sbg
