#pragma once

#include "../utility/shuffler.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BehaviorShufflerService : kgr::single_service<BehaviorShuffler> {};

} // namespace sbg
