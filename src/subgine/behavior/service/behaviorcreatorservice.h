#pragma once

#include "../creator/behaviorcreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BehaviorCreatorService : kgr::single_service<BehaviorCreator> {};

} // namespace sbg
