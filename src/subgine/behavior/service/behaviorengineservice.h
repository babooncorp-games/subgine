#pragma once

#include "../engine/behaviorengine.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BehaviorEngineService : kgr::single_service<BehaviorEngine> {};

} // namespace sbg
