#pragma once

#include "../behaviormodule.h"
#include "behaviorcreatorservice.h"
#include "behaviorengineservice.h"

#include "subgine/system/service/mainengineservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct BehaviorModuleService : kgr::single_service<BehaviorModule>, autocall<
	&BehaviorModule::setupMainEngine,
	&BehaviorModule::setupBehaviorCreator,
	&BehaviorModule::setupComponentCreator,
	&BehaviorModule::setupEntityBindingCreator
> {};

} // namespace sbg
