#pragma once

namespace sbg {

enum struct BehaviorStatus { Success, Failure, Running };

} // namespace sbg
