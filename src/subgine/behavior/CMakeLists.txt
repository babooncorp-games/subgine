add_library(behavior
	behaviormodule.cpp

	behavior/endlessrepeater.cpp
	behavior/inverter.cpp
	behavior/randomselector.cpp
	behavior/randomsequence.cpp
	behavior/repeater.cpp
	behavior/repeatuntilfail.cpp
	behavior/selector.cpp
	behavior/sequence.cpp
	behavior/succeeder.cpp
	behavior/wait.cpp

	component/behaviortree.cpp

	engine/behaviorengine.cpp

	utility/shuffler.cpp
	utility/timer.cpp
	
	behavior.cpp
	status.cpp
)

subgine_configure_target(behavior)

target_link_libraries(behavior
PUBLIC
	subgine::common
	subgine::entity
	subgine::resource
	
PRIVATE
	subgine::system
	subgine::common-warnings
)

add_subdirectory(test)
