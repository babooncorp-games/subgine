#pragma once

#include "../component/behaviortree.h"

#include "subgine/system/time.h"
#include "subgine/common/ownershiptoken.h"
#include "subgine/entity/componentlist.h"
#include "subgine/entity/entity.h"

#include <vector>

namespace sbg {

struct BehaviorEngineService;

struct BehaviorEngine {
	void execute(Time time);
	void owner(OwnershipToken owner) noexcept;

	void add(Entity entity);

private:
	ComponentList<BehaviorTree> _behaviors;
	OwnershipToken _owner;
	
	friend auto service_map(BehaviorEngine const&) -> BehaviorEngineService;
};

} // namespace sbg
