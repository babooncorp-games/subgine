#include "behaviorengine.h"

using namespace sbg;

void BehaviorEngine::owner(OwnershipToken owner) noexcept {
	_owner = std::move(owner);
}

void BehaviorEngine::execute(Time time) {
	_behaviors.cleanup();
	
	for (auto const [behavior] : _behaviors) {
		if (behavior->running()) {
			behavior->execute();
		}
	}
}

void BehaviorEngine::add(Entity entity) {
	_behaviors.emplace(entity);
}
