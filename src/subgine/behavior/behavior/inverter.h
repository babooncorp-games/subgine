#pragma once

#include "../behavior.h"

namespace sbg {

struct Inverter {
	explicit Inverter(Behavior child) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	Behavior _child;
};

} // namespace sbg
