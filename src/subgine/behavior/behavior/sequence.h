#pragma once

#include "../behavior.h"

#include <vector>

namespace sbg {

struct Sequence {
	explicit Sequence(std::vector<Behavior> sequence) noexcept;
	
	explicit Sequence(behavior auto... sequence) {
		(_sequence.emplace_back(std::move(sequence)), ...);
		_it = _sequence.end();
	}
	
	auto execute() -> BehaviorStatus;
	
private:
	std::vector<Behavior> _sequence;
	std::vector<Behavior>::iterator _it;
};

} // namespace sbg
