#pragma once

#include "../behavior.h"

#include <vector>

namespace sbg {

struct Selector {
	explicit Selector(std::vector<Behavior> sequence) noexcept;
	
	explicit Selector(behavior auto... sequence) {
		(_sequence.emplace_back(std::move(sequence)), ...);
		_it = _sequence.end();
	}
	
	auto execute() -> BehaviorStatus;
	
private:
	std::vector<Behavior> _sequence;
	std::vector<Behavior>::iterator _it;
};

} // namespace sbg
