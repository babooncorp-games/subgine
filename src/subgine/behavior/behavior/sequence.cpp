#include "sequence.h"

using namespace sbg;

Sequence::Sequence(std::vector<Behavior> sequence) noexcept : _sequence{std::move(sequence)}, _it{_sequence.end()} {}

auto Sequence::execute() -> BehaviorStatus {
	// Here we execute the initialization when we start from halted to running
	if (_it == _sequence.end()) {
		_it = _sequence.begin();
	}
	
	for (; _it != _sequence.end(); ++_it) {
		auto const status = _it->execute();
		
		if (status == BehaviorStatus::Failure) {
			_it = _sequence.end();
			return status;
		} else if (status == BehaviorStatus::Running) {
			return status;
		}
	}
	
	return BehaviorStatus::Success;
}
