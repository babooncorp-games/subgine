#include "repeater.h"

using namespace sbg;

Repeater::Repeater(std::int32_t loop, Behavior child) noexcept : _loop{loop}, _child{std::move(child)} {}

auto Repeater::execute() -> BehaviorStatus {
	auto const result = _child.execute();
	
	if (result == BehaviorStatus::Success || result == BehaviorStatus::Failure) {
		_count++;
	}
	
	if (_count >= _loop) {
		_count = 0;
		return BehaviorStatus::Success;
	}
	
	return BehaviorStatus::Running;
}
