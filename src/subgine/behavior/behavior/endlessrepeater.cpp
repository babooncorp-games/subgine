#include "endlessrepeater.h"

using namespace sbg;

EndlessRepeater::EndlessRepeater(Behavior child) noexcept : _child{std::move(child)} {}

auto EndlessRepeater::execute() -> BehaviorStatus {
	_child.execute();
	return BehaviorStatus::Running;
}
