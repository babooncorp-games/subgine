#pragma once

#include "../behavior.h"

namespace sbg {

struct EndlessRepeater {
	explicit EndlessRepeater(Behavior child) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	Behavior _child;
};

} // namespace sbg
