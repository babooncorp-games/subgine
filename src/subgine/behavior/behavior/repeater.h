#pragma once

#include "../behavior.h"

#include <cstdint>

namespace sbg {

struct Repeater {
	Repeater(std::int32_t loop, Behavior child) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	std::int32_t _loop;
	std::int32_t _count = 0;
	Behavior _child;
};

} // namespace sbg
