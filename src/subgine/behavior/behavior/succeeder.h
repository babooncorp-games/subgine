#pragma once

#include "../behavior.h"

namespace sbg {

struct Succeeder {
	explicit Succeeder(Behavior child) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	Behavior _child;
};

} // namespace sbg
