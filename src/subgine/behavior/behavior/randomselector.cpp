#include "randomselector.h"

#include "../utility/shuffler.h"

using namespace sbg;

RandomSelector::RandomSelector(BehaviorShuffler& shuffler, std::vector<Behavior> sequence) noexcept : _sequence{std::move(sequence)}, _it{_sequence.end()}, _shuffler{shuffler} {}

auto RandomSelector::execute() -> BehaviorStatus {
	// Here we execute the initialization when we start from halted to running
	if (_it == _sequence.end()) {
		_shuffler.shuffle(_sequence.begin(), _sequence.end());
		_it = _sequence.begin();
	}
	
	for (; _it != _sequence.end(); ++_it) {
		auto const status = _it->execute();
		
		if (status == BehaviorStatus::Success) {
			_it = _sequence.end();
			return status;
		} else if (status == BehaviorStatus::Running) {
			return status;
		}
	}
	
	return BehaviorStatus::Failure;
}
