#pragma once

#include "../behavior.h"

namespace sbg {

struct RepeatUntilFail {
	explicit RepeatUntilFail(Behavior child) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	Behavior _child;
};

} // namespace sbg
