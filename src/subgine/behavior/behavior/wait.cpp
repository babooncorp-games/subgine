#include "wait.h"

using namespace sbg;

Wait::Wait(double duration) noexcept : _duration{duration} {}

BehaviorStatus Wait::execute() {
	if (_timer.running()) {
		if (_timer.elapsedMilliseconds() >= _duration) {
			_timer.stop();
			return BehaviorStatus::Success;
		}
	} else {
		if (_timer.elapsedMilliseconds() >= _duration) {
			_timer = Timer{};
		}
		_timer.start();
	}
	
	return BehaviorStatus::Running;
}
