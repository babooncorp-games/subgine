#include "repeatuntilfail.h"

using namespace sbg;

RepeatUntilFail::RepeatUntilFail(Behavior child) noexcept : _child{std::move(child)} {}

auto RepeatUntilFail::execute() -> BehaviorStatus {
	auto const result = _child.execute();
	
	if (result == BehaviorStatus::Failure) {
		return BehaviorStatus::Success;
	}
	
	return BehaviorStatus::Running;
}
