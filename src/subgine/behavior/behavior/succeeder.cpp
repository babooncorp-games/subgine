#include "succeeder.h"

using namespace sbg;

Succeeder::Succeeder(Behavior child) noexcept : _child{std::move(child)} {}

auto Succeeder::execute() -> BehaviorStatus {
	if (auto const result = _child.execute(); result == BehaviorStatus::Running) {
		return result;
	}
	
	return BehaviorStatus::Success;
}
