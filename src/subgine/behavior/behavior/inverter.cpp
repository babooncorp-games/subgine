#include "inverter.h"

using namespace sbg;

Inverter::Inverter(Behavior child) noexcept : _child{std::move(child)} {}

BehaviorStatus Inverter::execute() {
	auto const result = _child.execute();
	
	if (result == BehaviorStatus::Failure) {
		return BehaviorStatus::Success;
	} else if (result == BehaviorStatus::Success) {
		return BehaviorStatus::Failure;
	}
	
	return BehaviorStatus::Running;
}
