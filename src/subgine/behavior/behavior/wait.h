#pragma once

#include "../behavior.h"
#include "../utility/timer.h"
#include "subgine/system/time.h"

namespace sbg {

struct Wait {
	// TODO: Use time literals instead of milliseconds as double
	explicit Wait(double duration) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	// TODO: Use game-time synchronized timer
	Timer _timer = {};
	double _duration;
};

} // namespace sbg
