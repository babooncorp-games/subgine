#pragma once

#include "subgine/common/kangaru.h"

#include "../behavior.h"

#include <vector>

namespace sbg {

struct BehaviorShuffler;

struct RandomSelector {
	RandomSelector(BehaviorShuffler& shuffler, std::vector<Behavior> sequence) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	std::vector<Behavior> _sequence;
	std::vector<Behavior>::iterator _it;
	BehaviorShuffler& _shuffler;
	
	friend auto service_map(RandomSelector const&) -> kgr::autowire;
};

} // namespace sbg
