#pragma once

#include "../behavior.h"
#include "subgine/common/kangaru.h"

#include <vector>

namespace sbg {

struct BehaviorShuffler;

struct RandomSequence {
	RandomSequence(BehaviorShuffler& shuffler, std::vector<Behavior> sequence) noexcept;
	
	auto execute() -> BehaviorStatus;
	
private:
	std::vector<Behavior> _sequence;
	std::vector<Behavior>::iterator _it;
	BehaviorShuffler& _shuffler;
	
	friend auto service_map(RandomSequence const&) -> kgr::autowire;
};

} // namespace sbg
