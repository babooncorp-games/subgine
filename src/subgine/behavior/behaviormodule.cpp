#include "behaviormodule.h"

#include "subgine/entity.h"
#include "subgine/behavior.h"
#include "subgine/system.h"

using namespace sbg;

void BehaviorModule::setupMainEngine(MainEngine& mainEngine, BehaviorEngine& behaviorEngine) const {
	mainEngine.add(behaviorEngine);
}

void BehaviorModule::setupBehaviorCreator(BehaviorCreator& behaviorCreator) const {
	behaviorCreator.add("inverter", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const child = data["child"];
		return Inverter{
			behaviorFactory.create(child["type"], entity, child["data"])
		};
	});
	
	behaviorCreator.add("succeeder", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const child = data["child"];
		return Succeeder{
			behaviorFactory.create(child["type"], entity, child["data"])
		};
	});
	
	behaviorCreator.add("success", [](){
		return [] {
			return BehaviorStatus::Success;
		};
	});
	
	behaviorCreator.add("failure", [](){
		return [] {
			return BehaviorStatus::Failure;
		};
	});
	
	behaviorCreator.add("endless-repeater", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const child = data["child"];
		return EndlessRepeater{
			behaviorFactory.create(child["type"], entity, child["data"])
		};
	});
	
	behaviorCreator.add("repeater", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const child = data["child"];
		return Repeater{
			std::int32_t{data["loop"]},
			behaviorFactory.create(child["type"], entity, child["data"])
		};
	});

	behaviorCreator.add("repeat-until-fail", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const child = data["child"];
		return RepeatUntilFail{
			behaviorFactory.create(child["type"], entity, child["data"])
		};
	});
	
	behaviorCreator.add("selector", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const children = data["children"];
		auto sequence = std::vector<Behavior>{};
		
		sequence.reserve(children.size());
		for (auto const child : children) {
			sequence.emplace_back(behaviorFactory.create(child["type"], entity, child["data"]));
		}
		
		return Selector{std::move(sequence)};
	});
	
	behaviorCreator.add("random-selector", [](BehaviorShuffler& shuffler, BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const children = data["children"];
		auto sequence = std::vector<Behavior>{};
		
		sequence.reserve(children.size());
		for (auto const child : children) {
			sequence.emplace_back(behaviorFactory.create(child["type"], entity, child["data"]));
		}
		
		return RandomSelector{shuffler, std::move(sequence)};
	});
	
	behaviorCreator.add("sequence", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const children = data["children"];
		auto sequence = std::vector<Behavior>{};
		
		sequence.reserve(children.size());
		for (auto const child : children) {
			sequence.emplace_back(behaviorFactory.create(child["type"], entity, child["data"]));
		}
		
		return Sequence{std::move(sequence)};
	});
	
	behaviorCreator.add("random-sequence", [](BehaviorShuffler& shuffler, BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const children = data["children"];
		auto sequence = std::vector<Behavior>{};
		
		sequence.reserve(children.size());
		for (auto const child : children) {
			sequence.emplace_back(behaviorFactory.create(child["type"], entity, child["data"]));
		}
		
		return RandomSequence{shuffler, std::move(sequence)};
	});
	
	behaviorCreator.add("wait", [](Entity entity, Property data) {
		return Wait{double{data["duration"]}};
	});
	
	behaviorCreator.add("wait2", [](Entity entity, Property data) {
		return Wait{1000.0 * double{data["duration"]}};
	});
}

void BehaviorModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("behavior-tree", [](BehaviorFactory behaviorFactory, Entity entity, Property data) {
		auto const root = data["root"];
		auto tree = BehaviorTree{behaviorFactory.create(root["type"], entity, root["data"])};
		
		if (bool{data["autostart"]}) {
			tree.start();
		}
		
		return tree;
	});
}

void BehaviorModule::setupEntityBindingCreator(EntityBindingCreator& factoryBinder) const {
	factoryBinder.add("behavior", [](BehaviorEngine& engine, Entity entity, Property) {
		engine.add(entity);
	});
}
