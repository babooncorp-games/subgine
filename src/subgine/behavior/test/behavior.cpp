#include "catch2/catch.hpp"

#include "subgine/behavior.h"

#include <vector>

namespace sbg {

struct AlwaysSuccess {
	auto execute() -> BehaviorStatus {
		return BehaviorStatus::Success;
	}
};

struct Decorator {
	explicit Decorator(Behavior child) noexcept : _child{std::move(child)} {}
	
	auto execute() -> BehaviorStatus {
		return _child.execute();
	}
	
	Behavior _child;
};

TEST_CASE("Behavior Tree", "[behavior]") {
	auto alwaysSuccessLambda = Behavior([]() {
		return BehaviorStatus::Success;
	});
	
	REQUIRE(alwaysSuccessLambda.execute() == BehaviorStatus::Success);
	
	auto alwaysSuccess = Behavior{AlwaysSuccess{}};
	
	REQUIRE(alwaysSuccess.execute() == BehaviorStatus::Success);
	
	SECTION("Decorator Behavior") {
		auto decorator = Decorator{std::move(alwaysSuccess)};
		REQUIRE(decorator.execute() == BehaviorStatus::Success);
	}
	
	SECTION("Inverter Behavior") {
		auto inverter = Inverter{std::move(alwaysSuccess)};
		REQUIRE(inverter.execute() == BehaviorStatus::Failure);
	}
	
	SECTION("Basic Sequence Behavior") {
		auto children = std::vector<Behavior>{};
		children.emplace_back(std::move(alwaysSuccessLambda));
		children.emplace_back(std::move(alwaysSuccess));
		
		auto behavior = Sequence{std::move(children)};
		
		REQUIRE(behavior.execute() == BehaviorStatus::Success);
	}
	
	SECTION("Failing Sequence Behavior") {
		auto children = std::vector<Behavior>{};
		children.emplace_back(std::move(alwaysSuccessLambda));
		children.emplace_back(Inverter{std::move(alwaysSuccess)});
		
		auto sequence = Sequence{std::move(children)};
		
		REQUIRE(sequence.execute() == BehaviorStatus::Failure);
	}
}

} // namespace sbg
