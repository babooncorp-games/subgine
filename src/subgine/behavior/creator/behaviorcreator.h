#pragma once

#include "../behavior.h"
#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

struct BehaviorCreatorService;

struct BehaviorCreator : Creator<DefaultBuilder<Behavior>> {
	using Creator::create;
	friend auto service_map(BehaviorCreator const&) -> BehaviorCreatorService;
};

using BehaviorFactory = Factory<BehaviorCreator>;

} // namespace sbg
