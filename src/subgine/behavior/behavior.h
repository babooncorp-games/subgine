#pragma once

#include "status.h"
#include "utility/trait.h"

#include "subgine/common/traits.h"

#include <memory>

#include "subgine/common/define.h"

namespace sbg {

struct Behavior {
	template<behavior_lambda E>
	explicit(false) Behavior(E lambda) : _behavior{std::make_unique<LambdaBehavior<E>>(std::move(lambda))} {}

	template<behavior_node T>
	explicit(false) Behavior(T object) : _behavior{std::make_unique<ObjectBehavior<T>>(std::move(object))} {}

	auto execute() -> BehaviorStatus {
		return _behavior->execute();
	}

private:
	struct AbstractBehavior {
		virtual ~AbstractBehavior() = default;
		virtual auto execute() -> BehaviorStatus = 0;
	};

	template<behavior_lambda E>
	struct LambdaBehavior : AbstractBehavior {
		explicit LambdaBehavior(E lambda) noexcept : _lambda{std::move(lambda)} {}

		auto execute() -> BehaviorStatus override {
			return _lambda();
		}

	private:
		NO_UNIQUE_ADDRESS
		E _lambda;
	};

	template<behavior_node T>
	struct ObjectBehavior : AbstractBehavior {
		explicit ObjectBehavior(T object) noexcept : _object{std::move(object)} {}

		auto execute() -> BehaviorStatus override {
			return _object.execute();
		}

	private:
		NO_UNIQUE_ADDRESS
		T _object;
	};
	
	std::unique_ptr<AbstractBehavior> _behavior;
};

} // namespace sbg

#include "subgine/common/undef.h"
