#pragma once

#include "../behavior.h"

#include <optional>

namespace sbg {

struct BehaviorTree {
	explicit BehaviorTree(Behavior root) noexcept;
	
	void execute();
	
	void start();
	void pause();
	
	auto running() const -> bool;
	auto failed() const -> bool;
	auto succeeded() const -> bool;
	auto paused() const -> bool;
	
private:
	Behavior _root;
	std::optional<BehaviorStatus> _lastResult;
};

} // namespace sbg
