#include "behaviortree.h"

#include <utility>

using namespace sbg;

BehaviorTree::BehaviorTree(Behavior root) noexcept : _root{std::move(root)} {}

void BehaviorTree::execute() {
	_lastResult = _root.execute();
}

void BehaviorTree::start() {
	_lastResult = BehaviorStatus::Running;
}

void BehaviorTree::pause() {
	_lastResult = {};
}

auto BehaviorTree::running() const -> bool {
	return _lastResult == BehaviorStatus::Running;
}

auto BehaviorTree::failed() const -> bool {
	return _lastResult == BehaviorStatus::Failure;
}

auto BehaviorTree::succeeded() const -> bool {
	return _lastResult == BehaviorStatus::Success;
}

auto BehaviorTree::paused() const -> bool {
	return _lastResult == std::nullopt;
}
