#pragma once

namespace sbg {

struct ComponentCreator;
struct SceneCreator;
struct CollisionProfileCreator;
struct ModelCreator;

struct TiledModuleService;

struct TiledModule {
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	void setupSceneCreator(SceneCreator& scenecreator) const;
	void setupCollisionProfileCreator(CollisionProfileCreator& collisionProfileCreator) const;
	void setupModelCreator(ModelCreator& ModelCreator) const;
	
	friend auto service_map(TiledModule const&) -> TiledModuleService;
};

} // namespace sbg
