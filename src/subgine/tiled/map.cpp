#include "map.h"

#include <algorithm>
#include <ranges>

#include "subgine/log.h"

using namespace sbg::tiled;

auto Map::tilesetForGid(std::uint32_t gid) const noexcept -> MapTileset {
	auto const found = std::adjacent_find(tilesets.begin(), tilesets.end(), [&](MapTileset const& first, MapTileset const& second) {
		return first.firstgid <= gid && second.firstgid > gid;
	});
	
	if (found != tilesets.end()) {
		return *found;
	} else {
		return tilesets.back();
	}
}

auto Map::findTilesetByName(std::string_view name) const noexcept -> std::optional<MapTileset> {
	auto const it = std::ranges::find(tilesets, name, [](MapTileset const& tileset) {
		return std::visit([](auto const& tileset){ return std::string_view{tileset.name}; }, tileset.tileset);
	});
	
	if (it != tilesets.end()) {
		return *it;
	} else {
		return {};
	}
}

auto Map::dereferenceObject(ObjectReference reference) const noexcept -> Object {
	auto const doWalkLayers = [&](auto const doWalkLayers, auto const function, ImmutableVector<Layer> const& layers) -> bool {
		for (auto const& layer : layers) {
			auto const should_continue = std::visit(
				[&](auto const& layer) {
					using T = std::decay_t<decltype(layer)>;
					
					if (not function(layer)) {
						return false;
					}
					
					if constexpr (std::is_same_v<T, GroupLayer>) {
						return doWalkLayers(doWalkLayers, function, layer.layers);
					} else {
						return true;
					}
				},
				layer
			);
			
			if (not should_continue) {
				return false;
			}
		}
		
		return true;
	};
	
	auto object = Object{};
	
	auto const found = not doWalkLayers(doWalkLayers, [&]<typename T>(T const& layer) {
		if constexpr (std::same_as<T, ObjectLayer>) {
			auto const it = std::ranges::find_if(layer.objects, [&](Object const& object) {
				return object.id == reference.id;
			});
			
			// If the object found, return false and assign object to *it
			return it == layer.objects.end() or (object = *it, false);
		} else {
			return true;
		}
	}, layers);
	
	Log::error([found]{ return not found; }, SBG_LOG_INFO, "Dereference object haven't found the object with id", log::enquote(reference.id));
	
	return object;
}
