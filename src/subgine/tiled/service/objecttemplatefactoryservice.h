#pragma once

#include "../factory/objecttemplatefactory.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg::tiled {

struct ObjectTemplateLoaderService : kgr::service<ObjectTemplateLoader, kgr::autowire> {};

using ObjectTemplateFactoryService = ResourceFactoryService<ObjectTemplateLoader>;
using ObjectTemplateManagerService = ResourceManagerService<ObjectTemplateManager>;

} // namespace sbg::tiled
