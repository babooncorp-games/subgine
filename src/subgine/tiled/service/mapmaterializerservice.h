#pragma once

#include "../mapmaterializer.h"

#include "subgine/entity/service/entitycreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg::tiled {

struct MapMaterializerService : kgr::service<MapMaterializer, kgr::autowire> {};

} // namespace sbg::tiled
