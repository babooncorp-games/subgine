#pragma once

#include "../factory/mapfactory.h"

#include "tilesetfactoryservice.h"
#include "objecttemplatefactoryservice.h"

#include "subgine/resource/service/assetmapperservice.h"
#include "subgine/resource/service/gamedatabaseservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg::tiled {

struct MapFactoryService : kgr::service<MapFactory, kgr::autowire> {};

using MapManagerService = ResourceManagerService<MapManager>;

} // namespace sbg::tiled
