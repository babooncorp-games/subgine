#pragma once

#include "../factory/tilesetfactory.h"

#include "subgine/resource/service/assetmapperservice.h"
#include "subgine/resource/service/gamedatabaseservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg::tiled {

struct TilesetFactoryService : kgr::service<TilesetFactory, kgr::autowire> {};

using TilesetManagerService = ResourceManagerService<TilesetManager>;

} // namespace sbg::tiled
