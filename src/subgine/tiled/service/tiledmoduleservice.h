#pragma once

#include "../tiledmodule.h"

#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"
#include "subgine/scene/service/scenecreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TiledModuleService : kgr::single_service<TiledModule>,
	autocall<
		&TiledModule::setupComponentCreator,
		&TiledModule::setupModelCreator,
		&TiledModule::setupCollisionProfileCreator,
		&TiledModule::setupSceneCreator
	> {};

auto service_map(TiledModule const&) -> TiledModuleService;

} // namespace sbg
