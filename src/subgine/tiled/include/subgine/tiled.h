#pragma once

#include "tiled/service.h"

#include "../../component/data.h"

#include "../../factory/mapfactory.h"
#include "../../factory/tilesetfactory.h"
#include "../../factory/objecttemplatefactory.h"

#include "../../model/basiclayermodel.h"
#include "../../model/imagelayermodel.h"
#include "../../model/tilelayermodel.h"

#include "../../utility/colorparse.h"
#include "../../utility/objectid.h"
#include "../../utility/parseproperties.h"
#include "../../utility/shapeparse.h"
#include "../../utility/objectparse.h"

#include "../../layer.h"
#include "../../map.h"
#include "../../mapmaterializer.h"
#include "../../materializedmap.h"
#include "../../object.h"
#include "../../objecttemplate.h"
#include "../../tileset.h"
