#pragma once

#include "object.h"
#include "customproperty.h"

#include "subgine/graphic/texturetag.h"
#include "subgine/common/immutablevector.h"

#include <string>
#include <variant>

namespace sbg::tiled {

struct BasicTileset {
	std::string name;
	CustomProperties properties;
};

struct TileTileset : BasicTileset {
	struct Tile {
		std::int32_t id;
		ImmutableVector<Object> objects;
	};
	
	TextureTag texture;
	std::int32_t tileCount;
	Vector2i tileSize;
	ImmutableVector<Tile> tiles;
};

struct ImageTileset : BasicTileset {
	struct Tile {
		std::int32_t id;
		std::string type;
		ImmutableVector<Object> objects;
		TextureTag texture;
		Vector2i size;
	};
	
	std::int32_t tileCount;
	ImmutableVector<Tile> tiles;
};

using Tileset = std::variant<TileTileset, ImageTileset>;

} // namespace sbg::tiled
