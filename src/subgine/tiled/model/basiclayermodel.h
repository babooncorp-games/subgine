#pragma once

#include "subgine/vector/vector.h"
#include "subgine/graphic/utility/uniformencoderforward.h"

namespace sbg::tiled {

struct BasicLayerModel {
	Vector4f blend;
	Vector2f offset;
	Vector4f tint = {1, 1, 1, 1};
	float parallax = 1;
	float parallaxScaling = 1;
	float opacity = 1;
	bool visible = true;
	
	static void serialize(ModelUniformEncoder& uniforms, BasicLayerModel const& sprite);
};

} // namespace sbg::tiled
