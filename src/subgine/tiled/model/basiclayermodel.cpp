#include "basiclayermodel.h"

#include "subgine/graphic/uniform.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace sbg::tiled;
using namespace sbg::literals;

void BasicLayerModel::serialize(ModelUniformEncoder& uniforms, BasicLayerModel const& sprite) {
	uniforms
		<< uniform::value("blendColor"_h, sprite.blend)
		<< uniform::value("tint"_h, sprite.tint)
		<< uniform::value("offset"_h, sprite.offset)
		<< uniform::value("parallax"_h, sprite.parallax)
		<< uniform::value("parallax_scaling"_h, sprite.parallaxScaling)
		<< uniform::value("parallax_matrix"_h, glm::scale(glm::vec3{sprite.parallax, sprite.parallax, 1}))
		<< uniform::value("opacity"_h, sprite.opacity)
		<< uniform::value("visible"_h, sprite.visible);
}
