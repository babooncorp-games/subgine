#pragma once

#include "basiclayermodel.h"

#include "subgine/graphic/texturetag.h"
#include "subgine/graphic/utility/uniformencoderforward.h"
#include "subgine/vector/vector.h"

namespace sbg::tiled {

struct ImageLayerModel : BasicLayerModel {
	TextureTag texture;
	
	static void serialize(ModelUniformEncoder& uniforms, ImageLayerModel const& model);
};

} // namespace sbg::tiled
