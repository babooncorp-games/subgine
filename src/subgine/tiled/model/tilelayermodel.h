#pragma once

#include "basiclayermodel.h"

#include "subgine/vector/vector.h"
#include "subgine/graphic/texturetag.h"
#include "subgine/graphic/utility/uniformencoderforward.h"

#include <vector>
#include <utility>
#include <cstdint>

namespace sbg::tiled {

/// This model contains all the changes made to a layer in order to update the tile map data.
struct TileLayerModel : BasicLayerModel {
	int width = 0;
	int plane = 0;
	std::vector<std::int32_t> tiles;
	TextureTag texture;
	Vector2i tileSize;
	
	static void serialize(ModelUniformEncoder& uniforms, TileLayerModel const& sprite);
};

} // namespace sbg::tiled
