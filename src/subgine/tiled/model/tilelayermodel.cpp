#include "tilelayermodel.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/dynstructbuffer.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace sbg;
using namespace tiled;
using namespace sbg::literals;

static auto to_vertex(std::size_t position, std::int32_t id, TileLayerModel const& model) noexcept -> std::pair<int, Vector2i> {
	return {id, Vector2i{static_cast<int>(position) % model.width, static_cast<int>(position) / model.width}};
}

void TileLayerModel::serialize(ModelUniformEncoder& uniforms, TileLayerModel const& model) {
	BasicLayerModel::serialize(uniforms, model);
	
	auto const scale =
		  glm::scale(glm::vec3{model.tileSize.x * 1.f, model.tileSize.y * 1.f, 1.f})
		* glm::scale(glm::vec3{model.parallaxScaling, model.parallaxScaling, 1.f})
		* glm::scale(glm::vec3{1.f / model.parallax, 1.f / model.parallax, 1.f});
	
	auto const modelMatrix = glm::translate(glm::vec3{model.offset.x, model.offset.y, 0.f}) * scale;
	
	auto tiles = DynStructBuffer{
		DynStructBuffer::Member{0, type_id<int>, "id"},
		DynStructBuffer::Member{1, type_id<Vector2i>, "position"}
	};
	
	auto cursor = std::size_t{0};
	for (auto const tile : model.tiles) {
		auto const c = cursor++;
		if (tile != 0) {
			tiles.push_back(to_vertex(c, tile, model));
		}
	}
	
	uniforms
		<< uniform::value("width"_h, model.width)
		<< uniform::value("size"_h, static_cast<Vector2f>(model.tileSize))
		<< uniform::value("model"_h, modelMatrix)
		<< uniform::resource("tex"_h, model.texture)
		<< uniform::value("plane"_h, model.plane)
		<< uniform::value("tiles"_h, std::move(tiles));
}
