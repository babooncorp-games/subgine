#include "imagelayermodel.h"

#include "subgine/graphic/uniform.h"

using namespace sbg::tiled;
using namespace sbg::literals;

void ImageLayerModel::serialize(ModelUniformEncoder& uniforms, ImageLayerModel const& model) {
	BasicLayerModel::serialize(uniforms, model);
	
	uniforms
		<< uniform::resource("tex"_h, model.texture);
}
