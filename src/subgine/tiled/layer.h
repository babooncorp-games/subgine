#pragma once

#include "customproperty.h"
#include "object.h"

#include "subgine/vector/vector.h"
#include "subgine/graphic/texturetag.h"
#include "subgine/common/immutablevector.h"

#include <optional>
#include <variant>
#include <vector>

namespace sbg::tiled {

struct BasicLayer {
	std::string name;
	std::int32_t id;
	float opacity;
	Vector4f tint;
	Vector2f offset;
	CustomProperties properties;
};

struct TileLayer : BasicLayer {
	using TileId = std::int32_t;
	ImmutableVector<TileId> tiles = {};
};

struct ImageLayer : BasicLayer {
	TextureTag texture = {};
	std::optional<Vector4f> transparentColor = {};
};

struct ObjectLayer : BasicLayer {
	enum struct DrawingOrder : bool { TopDown, Manual } drawingOrder = {};
	ImmutableVector<Object> objects = {};
};

using Layer = std::variant<TileLayer, ImageLayer, ObjectLayer, struct GroupLayer>;

struct GroupLayer : BasicLayer {
	ImmutableVector<Layer> layers = {};
};

} // namespace sbg::tiled
