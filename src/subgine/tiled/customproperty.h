#pragma once

#include "utility/objectid.h"

#include "subgine/vector/vector.h"

#include <unordered_map>
#include <memory>
#include <filesystem>
#include <variant>
#include <string>
#include <optional>

namespace sbg::tiled {

struct ObjectReference {
	ObjectId id;
};

using CustomProperty = std::variant<
	bool,
	std::int32_t,
	float,
	std::string,
	Vector4f,
	ObjectReference,
	std::filesystem::path
>;

using Properties = std::pair<char const*, CustomProperty>[];

struct CustomProperties {
	using MapType = std::unordered_map<std::string, CustomProperty>;
	
	CustomProperties() : _properties{std::make_shared<MapType>()} {}
	
	template<std::size_t N>
	CustomProperties(std::pair<char const*, CustomProperty>(&&properties)[N]) :
		_properties{std::make_shared<MapType>([&]{
			auto map = MapType{};
			
			for (auto const& property : properties) {
				map.emplace(property);
			}
			
			return map;
		}())} {}
	
	explicit CustomProperties(MapType const& map) : _properties{std::make_shared<MapType>(map)} {}
	explicit CustomProperties(MapType&& map) : _properties{std::make_shared<MapType>(std::move(map))} {}
	
	template<typename F> requires std::is_invocable_r_v<MapType, F, MapType const&>
	[[nodiscard]] auto update(F function) const -> CustomProperties {
		return CustomProperties{function(*_properties)};
	}
	
	// TODO: C++20 use std::string_view
	template<typename T>
	auto get_optional(std::string const& name) const -> std::optional<T> {
		if (auto const it = _properties->find(name); it != _properties->end()) {
			if (auto const ptr = std::get_if<T>(&it->second)) {
				return *ptr;
			}
		}
		
		return {};
	}
	
	template<typename T>
	auto get_or(std::string const& name, T const& default_value) const -> T {
		if (auto const it = _properties->find(name); it != _properties->end()) {
			if (auto const ptr = std::get_if<T>(&it->second)) {
				return *ptr;
			}
		}
		
		return default_value;
	}
	
	template<typename T>
	auto get(std::string const& name) const -> T {
		return std::get<T>(_properties->at(name));
	}
	
	template<typename T, typename F, enable_if_i<std::is_invocable_r_v<T, F, T const&>> = 0>
	static auto set(MapType& map, std::string const& name, T const& default_value, F function) noexcept {
		if (auto const it = map.find(name); it != map.end()) {
			if (auto const ptr = std::get_if<T>(&it->second)) {
				it->second = function(std::as_const(*ptr));
			}
		} else {
			map.emplace(name, function(default_value));
		}
	}
	
	auto begin() const noexcept -> typename MapType::const_iterator {
		return _properties->cbegin();
	}
	
	auto end() const noexcept -> typename MapType::const_iterator {
		return _properties->cend();
	}
	
private:
	std::shared_ptr<MapType const> _properties;
};

} // namespace sbg::tiled
