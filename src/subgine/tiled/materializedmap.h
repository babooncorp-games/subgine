#pragma once

#include "utility/objectid.h"

#include "subgine/entity/entity.h"
#include "subgine/common/immutablevector.h"

#include <unordered_map>
#include <vector>
#include <cstdint>
#include <variant>

namespace sbg::tiled {

struct MaterializedObject {
	std::string name;
	Entity entity;
};

struct MaterializedBasicLayer {
	std::string name;
	Entity entity;
};

struct MaterializedTileLayer : MaterializedBasicLayer {};
struct MaterializedImageLayer : MaterializedBasicLayer {};

struct MaterializedObjectLayer : MaterializedBasicLayer {
	std::unordered_map<ObjectId, MaterializedObject> objects = {};
};

using MaterializedLayer = std::variant<
	MaterializedTileLayer,
	MaterializedImageLayer,
	MaterializedObjectLayer,
	struct MaterializedGroupLayer
>;

struct MaterializedGroupLayer : MaterializedBasicLayer {
	ImmutableVector<MaterializedLayer> layers = {};
};

struct MaterializedMap {
	Entity entity;
	ImmutableVector<MaterializedLayer> layers = {};
	
	template<typename F>
	auto walkLayers(F function) const {
		auto const doWalkLayers = [&](auto const doWalkLayers, ImmutableVector<MaterializedLayer> const& layers) -> void {
			for (auto const& layer : layers) {
				std::visit(
					[&](auto const& layer) {
						using T = std::decay_t<decltype(layer)>;
						
						function(layer);
						
						if constexpr (std::is_same_v<T, MaterializedGroupLayer>) {
							return doWalkLayers(doWalkLayers, layer.layers);
						}
					},
					layer
				);
			}
		};
		
		doWalkLayers(doWalkLayers, layers);
	}
	
	auto findObjectById(ObjectId id) const noexcept -> MaterializedObject;
	auto findObjectByName(std::string_view name) const noexcept -> MaterializedObject;

private:
	template<typename F>
	auto findObjectImpl(F function) const noexcept -> MaterializedObject;
};

} // namespace sbg::tiled
