#include "materializedmap.h"

using namespace sbg::tiled;

auto MaterializedMap::findObjectById(ObjectId id) const noexcept -> MaterializedObject {
	return findObjectImpl([&](MaterializedObjectLayer const& layer) {
		if (auto const it = layer.objects.find(id); it != layer.objects.end()) {
			return it->second;
		} else {
			return MaterializedObject{};
		}
	});
}

auto MaterializedMap::findObjectByName(std::string_view name) const noexcept -> MaterializedObject {
	return findObjectImpl([&](MaterializedObjectLayer const& layer) {
		if (
			auto const it = std::find_if(layer.objects.begin(), layer.objects.end(), [&](auto const& item) {
				auto const& [id, object] = item;
				return object.name == name;
			});
			it != layer.objects.end()
		) {
			return it->second;
		} else {
			return MaterializedObject{};
		}
	});
}

template<typename F>
auto MaterializedMap::findObjectImpl(F function) const noexcept -> MaterializedObject {
		auto const walkLayers = [&](auto const walkLayers, auto const& layers) -> MaterializedObject {
		for (auto const& layer : layers) {
			auto const object = std::visit(
				[&](auto const& layer) {
					using T = std::decay_t<decltype(layer)>;
					if constexpr (std::is_same_v<T, MaterializedObjectLayer>) {
						return function(layer);
					} else if constexpr (std::is_same_v<T, MaterializedGroupLayer>) {
						return walkLayers(walkLayers, layer.layers);
					} else {
						return MaterializedObject{};
					}
				},
				layer
			);
			
			if (object.entity) {
				return object;
			}
		}
		
		return MaterializedObject{};
	};
	
	return walkLayers(walkLayers, layers);
}
