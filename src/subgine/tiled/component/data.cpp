#include "data.h"

#include <ranges>
#include <algorithm>

#include "subgine/log.h"

using namespace sbg::tiled;

auto Data::getLinkedEntityFromProperties(CustomProperties const& properties, std::string const& name) const noexcept -> Entity {
	Log::fatal([&]{ return !materialized; }, SBG_LOG_INFO, "Cannot get linked entities until map is materialized");
	
	if (auto const objectReference = properties.get_optional<ObjectReference>(name)) {
		return materialized->map.findObjectById(objectReference->id).entity;
	} else {
		return Entity{};
	}
}

void Data::assertEntity(Entity entity, std::string_view name) noexcept {
	Log::fatal([&]{ return !entity.alive(); }, SBG_LOG_INFO, "Custom property", log::enquote(name), "not found in custom properties");
}

auto Data::getTileForObject() const -> std::variant<std::monostate, TileTileset::Tile, ImageTileset::Tile> {
	if (not object->gid) return {};
	
	auto const [firstgid, tileset] = map.tilesetForGid(*object->gid);
	auto const tileId = *object->gid - firstgid;
	
	if (auto const imageTileset = std::get_if<ImageTileset>(&tileset)) {
		auto const it = std::ranges::find(imageTileset->tiles, tileId, &ImageTileset::Tile::id);
		
		if (it == imageTileset->tiles.end()) {
			return {};
		}
		
		return *it;
	} else if (auto const tileTileset = std::get_if<TileTileset>(&tileset)) {
		auto const it = std::ranges::find(tileTileset->tiles, tileId, &TileTileset::Tile::id);
		
		if (it == tileTileset->tiles.end()) {
			return {};
		}
		
		return *it;
	}
	
	return {};
}
