#pragma once

#include "../map.h"
#include "../layer.h"
#include "../tileset.h"
#include "../customproperty.h"
#include "../materializedmap.h"

#include "subgine/entity/entity.h"

#include <optional>
#include <string>
#include <string_view>
#include <variant>

namespace sbg::tiled {

struct Data {
	struct Materialized {
		MaterializedMap map;
		
		std::optional<MaterializedTileLayer> tileLayer;
		std::optional<MaterializedImageLayer> imageLayer;
		std::optional<MaterializedObjectLayer> objectLayer;
		std::optional<MaterializedGroupLayer> groupLayer;
		std::optional<MaterializedObject> object;
	};
	
	Map map;
	std::optional<TileLayer> tileLayer = {};
	std::optional<ImageLayer> imageLayer = {};
	std::optional<ObjectLayer> objectLayer = {};
	std::optional<GroupLayer> groupLayer = {};
	std::optional<Object> object = {};
	
	std::optional<Materialized> materialized = {};
	
	template<typename T>
	auto getOptionalLinkedEntity(T const& tiledConcreteData, std::string const& name) const noexcept -> Entity {
		return getLinkedEntityFromProperties(tiledConcreteData.properties, name);
	}
	
	template<typename T>
	auto getLinkedEntity(T const& tiledConcreteData, std::string const& name) const noexcept -> Entity {
		auto const entity = getLinkedEntityFromProperties(tiledConcreteData.properties, name);
		assertEntity(entity, name);
		return entity;
	}
	
	auto findEntityByName(std::string_view name) const noexcept -> Entity;
	auto findEntityById(ObjectId id) const noexcept -> Entity;
	
	inline auto findEntityById(ObjectReference ref) const noexcept -> Entity {
		return findEntityById(ref.id);
	}
	
	auto getTileForObject() const -> std::variant<std::monostate, TileTileset::Tile, ImageTileset::Tile>;
	
private:
	static void assertEntity(Entity entity, std::string_view name) noexcept;
	auto getLinkedEntityFromProperties(CustomProperties const& properties, std::string const& name) const noexcept -> Entity;
};

} // namespace sbg::tiled
