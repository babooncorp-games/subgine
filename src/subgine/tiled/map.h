#pragma once

#include "tileset.h"
#include "layer.h"
#include "customproperty.h"

#include "subgine/resource/resourcehandle.h"
#include "subgine/common/immutablevector.h"

#include <string>

namespace sbg::tiled {

struct Map {
	enum struct TileRenderOrder : std::int32_t {
		RightDown, RightUp, LeftDown, LeftUp
	};
	
	struct MapTileset {
		std::uint32_t firstgid;
		Tileset tileset;
	};
	
	auto tilesetForGid(std::uint32_t gid) const noexcept -> MapTileset;
	auto findTilesetByName(std::string_view name) const noexcept -> std::optional<MapTileset>;
	auto dereferenceObject(ObjectReference reference) const noexcept -> Object;
	
	template<typename F>
	auto walkLayers(F function) const {
		auto const doWalkLayers = [&](auto const doWalkLayers, ImmutableVector<Layer> const& layers) -> void {
			for (auto const& layer : layers) {
				std::visit(
					[&](auto const& layer) {
						using T = std::decay_t<decltype(layer)>;
						
						function(layer);
						
						if constexpr (std::is_same_v<T, GroupLayer>) {
							return doWalkLayers(doWalkLayers, layer.layers);
						}
					},
					layer
				);
			}
		};
		
		doWalkLayers(doWalkLayers, layers);
	}
	
	std::string name;
	std::int32_t height;
	std::int32_t width;
	Vector2i tileSize;
	TileRenderOrder tileRenderOrder;
	ImmutableVector<MapTileset> tilesets;
	ImmutableVector<Layer> layers;
	CustomProperties properties;
};

} // namespace sbg::tiled
