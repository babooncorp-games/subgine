#pragma once

#include "../objecttemplate.h"

#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg::tiled {

struct ObjectTemplateLoaderService;

struct ObjectTemplateLoader {
	auto create(Property data) const -> ObjectTemplate;
	static constexpr auto path = "object-template";
	
	friend auto service_map(ObjectTemplateLoader const&) -> ObjectTemplateLoaderService;
};

using ObjectTemplateFactory = ResourceFactory<ObjectTemplateLoader>;
using ObjectTemplateManager = ResourceManager<ObjectTemplateFactory>;

} // namespace sbg::tiled
