#include "objecttemplatefactory.h"

#include "../utility/shapeparse.h"
#include "../utility/parseproperties.h"

#include "subgine/resource/utility/optionalvalue.h"

using namespace sbg::tiled;
using namespace std::literals;

auto ObjectTemplateLoader::create(Property data) const -> ObjectTemplate {
	auto const objectData = data["data"]["object"];
	
	return ObjectTemplate{
		value_or(objectData["name"], ""s),
		std::int32_t{objectData["id"]},
		value_optional<std::int32_t>(objectData["gid"]),
		value_or(objectData["type"], ""s),
		Vector2f{float{objectData["width"]}, float{objectData["height"]}},
		float{objectData["rotation"]},
		load_shape(objectData),
		load_custom_properties(objectData["properties"])
	};
}
