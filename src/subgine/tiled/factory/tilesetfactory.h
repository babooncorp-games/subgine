#pragma once

#include "../tileset.h"

#include "subgine/resource/assetmapper.h"
#include "subgine/resource/property.h"
#include "subgine/resource/resourcemanager.h"

#include <string_view>

namespace sbg {
	struct GameDatabase;
}

namespace sbg::tiled {

struct TilesetFactoryService;

struct TilesetFactory {
	explicit TilesetFactory(GameDatabase& gameDatabase, AssetMapper assetMapper) noexcept;
	
	auto create(std::string_view name) const -> Tileset;
	static void load_properties(Property data, /* out */ TileTileset& tileset);
	static void load_properties(Property data, /* out */ ImageTileset& tileset, std::string_view name);
	
	using Result = Tileset;
	inline static constexpr auto path = "tileset";
	
private:
	static void load_basic_properties(Property data, BasicTileset& tileset);
	GameDatabase* _gameDatabase;
	AssetMapper _assetMapper;
	
	friend auto service_map(TilesetFactory const&) -> TilesetFactoryService;
};

using TilesetManager = ResourceManager<TilesetFactory>;

} // namespace sbg::tiled
