#include "mapfactory.h"

#include "../utility/parseproperties.h"
#include "../utility/objectparse.h"
#include "../utility/colorparse.h"
#include "../utility/shapeparse.h"

#include "subgine/common/lookup_table.h"
#include "subgine/resource/utility/optionalvalue.h"

#include <algorithm>
#include <string_view>

using namespace sbg;
using namespace tiled;
using namespace std::literals;

constexpr auto objectDrawingOrder = makeLookupTable<std::string_view, ObjectLayer::DrawingOrder>({
	{"topdown", ObjectLayer::DrawingOrder::TopDown},
	{"index", ObjectLayer::DrawingOrder::Manual}
});

MapFactory::MapFactory(GameDatabase& gameDatabase, TilesetManager tilesetManager, ObjectTemplateManager objectTemplateManager, AssetMapper assetMapper) noexcept :
	_gameDatabase{&gameDatabase}, _tilesetManager{tilesetManager}, _objectTemplateManager{objectTemplateManager}, _assetMapper{assetMapper} {}

static auto load_basic_layer(Property jsonLayer) -> BasicLayer {
	auto const tintColor = value_optional<std::string_view>(jsonLayer["tintcolor"]);
	return BasicLayer{
		std::string{jsonLayer["name"]},
		std::int32_t{jsonLayer["id"]},
		float{jsonLayer["opacity"]},
		tintColor ? parse_hex_rgba(*tintColor) : Vector4f{1, 1, 1, 1},
		Vector2f{value_or(jsonLayer["offsetx"], 0.f), value_or(jsonLayer["offsety"], 0.f)},
		load_custom_properties(jsonLayer["properties"])
	};
}

template<typename TextureNameResolver, typename ObjectTemplateNameResolver>
static auto load_layer(Property jsonLayer, ObjectTemplateManager objectTemplateManager, TextureNameResolver resolveTextureName, ObjectTemplateNameResolver resolveObjectTemplateName) -> Layer {
	auto const type = std::string_view{jsonLayer["type"]};
	
	if (type == "tilelayer") {
		auto layer = TileLayer{
			load_basic_layer(jsonLayer)
		};
		
		layer.tiles = layer.tiles.update([&](auto tiles) {
			auto const data = jsonLayer["data"];
			tiles.reserve(data.size());
			std::transform(
				data.begin(), data.end(),
				std::back_inserter(tiles),
				[](Property tileId) {
					return std::uint32_t{tileId};
				}
			);
			
			return tiles;
		});
		
		return layer;
	} else if (type == "imagelayer") {
		auto const transparentColor = value_optional<std::string_view>(jsonLayer["transparentcolor"]);
		return ImageLayer{
			load_basic_layer(jsonLayer),
			TextureTag{resolveTextureName(std::filesystem::path{jsonLayer["image"]})},
			transparentColor ? std::optional<Vector4f>{parse_hex_rgba(*transparentColor)} : std::optional<Vector4f>{}
		};
	} else if (type == "objectgroup") {
		auto layer = ObjectLayer{
			load_basic_layer(jsonLayer)
		};
		
		layer.drawingOrder = objectDrawingOrder[jsonLayer["draworder"]];
		
		layer.objects = layer.objects.update([&](std::vector<Object> objects) {
			auto const jsonObjects = jsonLayer["objects"];
			objects.reserve(jsonObjects.size());
			
			std::transform(
				jsonObjects.begin(), jsonObjects.end(),
				std::back_inserter(objects),
				[&](Property jsonObject) -> Object {
					if (jsonObject["template"].isNull()) {
						return load_object(jsonObject);
					} else {
						// TODO: Find a way to merge json
						auto const objectTemplate = *objectTemplateManager.get(resolveObjectTemplateName(std::filesystem::path{jsonObject["template"]}));
						auto const gid = value_optional<std::int32_t>(jsonObject["gid"]);
						auto const width = value_optional<float>(jsonObject["width"]);
						auto const height = value_optional<float>(jsonObject["height"]);
						auto const shape = load_shape(jsonObject);
						
						// TODO: Remove support for type once we drop old tiled support
						return Object{
							.name = value_or(jsonObject["name"], objectTemplate.name),
							.id = std::int32_t{jsonObject["id"]},
							.gid = gid ? gid : objectTemplate.gid,
							.type = value_or(jsonObject["type"], value_or(jsonObject["class"], objectTemplate.type)),
							.position = Vector2f{float{jsonObject["x"]}, float{jsonObject["y"]}},
							.size = (width && height) ? Vector2f{*width, *height} : objectTemplate.size,
							.rotation = value_or(jsonObject["rotation"], objectTemplate.rotation),
							.shape = shape.index() == 0 ? objectTemplate.shape : shape,
							.properties = load_custom_properties(jsonObject["properties"], objectTemplate.properties),
						};
					}
				}
			);
			
			return objects;
		});
		
		return layer;
	} else {
		auto layer = GroupLayer{
			load_basic_layer(jsonLayer)
		};
		
		layer.layers = layer.layers.update([&](std::vector<Layer> layers) -> std::vector<Layer> {
			auto const jsonLayers = jsonLayer["layers"];
			layers.reserve(jsonLayers.size());
			std::transform(
				jsonLayers.begin(), jsonLayers.end(),
				std::back_inserter(layers),
				[&](Property jsonLayer) -> Layer {
					return load_layer(jsonLayer, objectTemplateManager, resolveTextureName, resolveObjectTemplateName);
				}
			);
			
			return layers;
		});
		
		return layer;
	}
}

constexpr auto tileRenderOrder = makeLookupTable<std::string_view, Map::TileRenderOrder>({
	{"right-down", Map::TileRenderOrder::RightDown},
	{"right-up", Map::TileRenderOrder::RightUp},
	{"left-down", Map::TileRenderOrder::LeftDown},
	{"left-up", Map::TileRenderOrder::LeftUp}
});

auto MapFactory::create(std::string_view name) const -> Map {
	auto map = Map{};
	
	auto const data = _gameDatabase->get("map", name)["data"];
	
	map.name = name;
	map.height = std::int32_t{data["height"]};
	map.width = std::int32_t{data["width"]};
	map.tileSize = Vector2i{std::int32_t{data["tilewidth"]}, std::int32_t{data["tileheight"]}};
	map.tileRenderOrder = tileRenderOrder[data["renderorder"]];
	map.properties = load_custom_properties(data["properties"]);
	
	map.tilesets = map.tilesets.update([&](auto tilesets) {
		auto jsonTilesets = data["tilesets"];
		tilesets.reserve(jsonTilesets.size());
		std::transform(
			jsonTilesets.begin(), jsonTilesets.end(),
			std::back_inserter(tilesets),
			[&](Property jsonTileset) -> Map::MapTileset {
				if (jsonTileset["source"].isNull()) {
					if (jsonTileset["image"].isNull()) {
						auto embeddedTileset = ImageTileset{};
						TilesetFactory::load_properties(jsonTileset, embeddedTileset, name);
						return Map::MapTileset{std::uint32_t{jsonTileset["firstgid"]}, embeddedTileset};
					} else {
						auto embeddedTileset = TileTileset{};
						auto const tilesetTexture = _assetMapper.relative_from_name("texture", "map", name, std::filesystem::path{jsonTileset["image"]});
						embeddedTileset.texture = TextureTag{tilesetTexture};
						TilesetFactory::load_properties(jsonTileset, embeddedTileset);
						
						// TODO: C++20 remove Map::MapTileset
						return Map::MapTileset{std::uint32_t{jsonTileset["firstgid"]}, embeddedTileset};
					}
				} else {
					auto tilesetName = _assetMapper.relative_from_name("tileset", "map", name, std::filesystem::path{jsonTileset["source"]});
					return Map::MapTileset{std::uint32_t{jsonTileset["firstgid"]}, *_tilesetManager.get(tilesetName)};
				}
			}
		);
		
		return tilesets;
	});
	
	auto const resolveTextureName = [&](std::filesystem::path path) {
		return _assetMapper.relative_from_name("texture", "map", name, path);
	};
	
	auto const resolveObjectTemplateName = [&](std::filesystem::path path) {
		return _assetMapper.relative_from_name("object-template", "map", name, path);
	};
	
	auto const jsonLayers = data["layers"];
	map.layers = map.layers.update([&](auto layers) {
		layers.reserve(jsonLayers.size());
		std::transform(
			jsonLayers.begin(), jsonLayers.end(),
			std::back_inserter(layers),
			[&](Property jsonLayer) {
				return load_layer(jsonLayer, _objectTemplateManager, resolveTextureName, resolveObjectTemplateName);
			}
		);
		return layers;
	});
	
	return map;
}
