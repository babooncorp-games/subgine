#include "tilesetfactory.h"

#include "../utility/parseproperties.h"
#include "../utility/objectparse.h"

#include "subgine/resource/gamedatabase.h"
#include "subgine/resource/utility/optionalvalue.h"

#include <filesystem>
#include <string>
#include <algorithm>
#include <ranges>
#include <iostream>

using namespace sbg;
using namespace tiled;

using namespace std::literals;

static auto load_tile_objects(Property objectGroup) -> std::vector<Object> {
	if (objectGroup.isNull()) {
		return {};
	} else {
		auto const jsonObjects = objectGroup["objects"];
		auto objects = std::vector<Object>{};
		
		objects.reserve(jsonObjects.size());
		std::transform(
			jsonObjects.begin(), jsonObjects.end(),
			std::back_inserter(objects),
			[](Property objectData) -> Object {
				return load_object(objectData);
			}
		);
		
		return objects;
	}
}

TilesetFactory::TilesetFactory(GameDatabase& gameDatabase, AssetMapper assetMapper) noexcept : _gameDatabase{&gameDatabase}, _assetMapper{assetMapper} {}

auto TilesetFactory::create(std::string_view name) const -> Tileset {
	auto const data = _gameDatabase->get("tileset", name)["data"];
	
	if (!data["image"].isNull()) {
		auto tileset = TileTileset{};
		tileset.texture = TextureTag{_assetMapper.relative_from_name("texture", "tileset", name, std::filesystem::path{data["image"]})};
		load_properties(data, tileset);
		return tileset;
	} else {
		auto tileset = ImageTileset{};
		load_properties(data, tileset, name);
		return tileset;
	}
}

void TilesetFactory::load_properties(Property data, TileTileset& tileset) {
	load_basic_properties(data, tileset);
	tileset.tileCount = std::int32_t{data["tilecount"]};
	tileset.tileSize = Vector2i{std::int32_t{data["tilewidth"]}, std::int32_t{data["tileheight"]}};
	tileset.tiles = tileset.tiles.update([&](std::vector<TileTileset::Tile> tiles) -> std::vector<TileTileset::Tile> {
		auto const jsonTiles = data["tiles"];
		tiles.resize(tileset.tileCount);
		std::ranges::generate(tiles, [n = 0]() mutable { return TileTileset::Tile{n++, {}}; });
		
		if (jsonTiles.isArray()) {
			for (auto const jsonTile : jsonTiles) {
				tiles[std::size_t{jsonTile["id"]}].objects = load_tile_objects(jsonTile["objectgroup"]);
			}
		}
		
		return tiles;
	});
}

void TilesetFactory::load_properties(Property data, ImageTileset& tileset, std::string_view tilesetName) {
	load_basic_properties(data, tileset);
	tileset.tileCount = std::int32_t{data["tilecount"]};
	tileset.tiles = tileset.tiles.update([&](std::vector<ImageTileset::Tile> tiles) -> std::vector<ImageTileset::Tile> {
		auto const jsonTiles = data["tiles"];
		
		if (jsonTiles.isArray()) {
			for (auto&& jsonTile : jsonTiles) {
				tiles.emplace_back(ImageTileset::Tile{
					.id = std::int32_t{jsonTile["id"]},
					.type = value_or(jsonTile["type"], value_or(jsonTile["class"], ""s)),
					.objects = load_tile_objects(jsonTile["objectgroup"]),
					// TODO: Make asset mapper aware of the context
					// .texture = TextureTag{_assetMapper.relative_from_name("texture", "tileset", tilesetName, std::filesystem::path{data["image"]})},
					.texture = {},
					.size = Vector2i{int{jsonTile["imagewidth"]}, int{jsonTile["imageheight"]}},
				});
			}
		}
		
		return tiles;
	});
}

void TilesetFactory::load_basic_properties(Property data, BasicTileset& tileset) {
	tileset.name = std::string{data["name"]};
	tileset.properties = load_custom_properties(data["properties"]);
}
