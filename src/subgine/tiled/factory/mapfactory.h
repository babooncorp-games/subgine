#pragma once

#include "../map.h"
#include "tilesetfactory.h"
#include "objecttemplatefactory.h"

#include "subgine/resource/assetmapper.h"
#include "subgine/resource/property.h"
#include "subgine/common/hash.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg::tiled {

struct MapFactoryService;

struct MapFactory {
	explicit MapFactory(GameDatabase& gameDatabase, TilesetManager tilesetManager, ObjectTemplateManager objectTemplateManager, AssetMapper assetMapper) noexcept;
	
	auto create(std::string_view name) const -> Map;
	auto create(hash_t hash) const -> Map;
	
	using Result = Map;
	
private:
	GameDatabase* _gameDatabase;
	TilesetManager _tilesetManager;
	ObjectTemplateManager _objectTemplateManager;
	AssetMapper _assetMapper;

	friend auto service_map(MapFactory const&) -> MapFactoryService;
};

using MapManager = ResourceManager<MapFactory>;

} // namespace sbg::tiled
