#pragma once

#include "object.h"

#include "customproperty.h"
#include "subgine/vector/vector.h"

#include <optional>
#include <string>
#include <cstdint>

namespace sbg::tiled {

struct ObjectTemplate {
	std::string name;
	std::int32_t id;
	std::optional<std::int32_t> gid;
	std::string type;
	Vector2f size;
	float rotation;
	Object::Shape shape;
	CustomProperties properties;
};

} // namespace sbg::tiled
