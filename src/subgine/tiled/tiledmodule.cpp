#include "tiledmodule.h"

#include "subgine/tiled.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/scene.h"

#include "subgine/common/math.h"
#include "subgine/log.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <algorithm>

using namespace sbg;
using namespace tiled;

void TiledModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("tiled-transform", [](Entity entity, Property data){
		Log::fatal([&]{ return !entity.has<Data>(); }, SBG_LOG_INFO, "TiledData component needed for tiled-transform component");
		auto const& tiledData = entity.component<Data>();
		Log::fatal([&]{ return !tiledData.objectLayer.has_value(); }, SBG_LOG_INFO, "TiledData component need to contain the object layer properties");
		Log::fatal([&]{ return !tiledData.object.has_value(); }, SBG_LOG_INFO, "TiledData component need to contain the object properties");
		
		auto transform = Transform{}
			.position2d(tiledData.objectLayer->offset + tiledData.object->position)
			.scale2d(tiledData.object->size)
			.orientation2d(glm::radians(tiledData.object->rotation));
		
		return transform;
	});
}

void TiledModule::setupSceneCreator(SceneCreator& sceneCreator) const {
	sceneCreator.add("map", [](MapFactory tiledMapFactory, MapMaterializer materializer, Property data) {
		return [tiledMapFactory, materializer, name = std::string{data["name"]}] {
			auto map = tiledMapFactory.create(name);
			auto materializedMap = materializer.materialize(map);
		};
	});
}

void TiledModule::setupCollisionProfileCreator(CollisionProfileCreator& collisionProfileCreator) const {
	collisionProfileCreator.add("tiled-layer-quadtree", [](Entity entity, Property data) {
		Log::fatal([&]{ return !entity.has<Data>(); }, SBG_LOG_INFO, "TiledData component needed for tiled-layer-quadtree collision profile");
		auto const& tiledData = entity.component<Data>();
		Log::fatal([&]{ return !tiledData.tileLayer.has_value(); }, SBG_LOG_INFO, "TiledData component need to contain the tile layer properties");
		
		auto aspects = std::vector<shape::AlignedBox2D>{};
		auto position = Vector2i{0, 0};
		auto const width = tiledData.map.width;
		
		for (auto const tileId : tiledData.tileLayer->tiles) {
			auto const [firstGid, tileset] = tiledData.map.tilesetForGid(tileId);
			auto const currentPosition = position;
			
			position.x++;
			if (position.x >= width) {
				position.y++;
			}
			position.x %= width;
			
			if (tileId == 0) continue;
			
			if (auto const tileTileset = std::get_if<TileTileset>(&tileset)) {
				auto const tile = tileTileset->tiles[tileId - firstGid];
				for (auto const& object : tile.objects) {
					if (!object.properties.get_or("Collidable", false)) continue;
					if (auto const rectangle = std::get_if<Object::Rectangle>(&object.shape)) {
						auto const position = currentPosition * tileTileset->tileSize;
						aspects.emplace_back(rectangle->alignedBox.top + position, rectangle->alignedBox.bottom + position);
					}
				}
			}
		}
		
		auto const treeSize = tiledData.map.tileSize * Vector2i{tiledData.map.width, tiledData.map.height};
		auto quadtree = QuadtreeAspect<shape::AlignedBox2D>{};
		quadtree.initialize(Vector2d{0, 0}, treeSize, std::move(aspects));
		
		auto const group = tiledData.tileLayer->properties.get_optional<std::string>("Collision Group");
		
		return CollisionProfile{std::move(quadtree), group ? std::vector{*group} : std::vector<std::string>{}, std::vector<std::string>{}};
	});
}

void TiledModule::setupModelCreator(ModelCreator& modelCreator) const {
	modelCreator.add("tiled-tile-layer", [](Entity entity) {
		Log::fatal([&]{ return !entity.has<Data>(); }, SBG_LOG_INFO, "TiledData component needed for tilelayer model");
		auto const& tiledData = entity.component<Data>();
		Log::fatal([&]{ return !tiledData.tileLayer.has_value(); }, SBG_LOG_INFO, "TiledData component need to contain the tile layer properties");
		
		auto model = TileLayerModel{};
		
		model.texture = std::get<0>(tiledData.map.tilesets.front().tileset).texture;
		model.width = tiledData.map.width;
		model.opacity = tiledData.tileLayer->opacity;
		model.tileSize = tiledData.map.tileSize;
		model.tint = tiledData.tileLayer->tint;
		model.blend = tiledData.tileLayer->properties.get_or("Blend", Vector4f{});
		model.parallax = tiledData.tileLayer->properties.get_or("Parallax", 1.f);
		model.parallaxScaling = tiledData.tileLayer->properties.get_or("Parallax Scaling", 1.f);
		model.visible = !tiledData.tileLayer->properties.get_or("Invisible In Game", false);
		
		std::transform(
			tiledData.tileLayer->tiles.begin(),
			tiledData.tileLayer->tiles.end(),
			std::back_inserter(model.tiles),
			[](std::uint32_t id) {
				return static_cast<std::int32_t>(id);
			}
		);
		
		return model;
	});
	
	modelCreator.add("tiled-image-layer", [](Entity entity) {
		auto model = ImageLayerModel{};
		
		Log::error([&]{ return !entity.has<Data>(); }, SBG_LOG_INFO, "TiledData component needed for imagelayer model");
		auto const& tiledData = entity.component<Data>();
		Log::fatal([&]{ return !tiledData.imageLayer.has_value(); }, SBG_LOG_INFO, "TiledData component need to contain the image layer properties");
		
		model.texture = tiledData.imageLayer->texture;
		
		return model;
	});
}
