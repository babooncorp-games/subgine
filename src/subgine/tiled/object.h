#pragma once

#include "customproperty.h"

#include "subgine/shape/alignedbox.h"
#include "subgine/shape/polytope.h"
#include "subgine/shape/nellipsoid.h"
#include "subgine/vector/vector.h"

#include <optional>
#include <variant>
#include <vector>
#include <cstdint>

namespace sbg::tiled {

struct Object {
	struct Point {
		sbg::Vector2f position;
	};
	
	struct Ellipse {
		shape::Ellipse ellipse;
	};
	
	struct Rectangle {
		shape::AlignedBox2D alignedBox;
	};
	
	struct Polygon {
		shape::Polygon vertices;
	};
	
	using Shape = std::variant<Rectangle, Point, Ellipse, Polygon>;
	
	std::string name;
	std::int32_t id;
	std::optional<std::int32_t> gid;
	std::string type;
	Vector2f position;
	Vector2f size;
	float rotation;
	Shape shape;
	CustomProperties properties;
};

} // namespace sbg::tiled
