#pragma once

#include "map.h"
#include "materializedmap.h"

#include "subgine/entity/entity.h"
#include "subgine/entity/creator/entitycreator.h"
#include "subgine/entity/entitymanager.h"

#include <optional>
#include <variant>
#include <vector>
#include <string>
#include <string_view>
#include <cstdint>
#include <unordered_map>

namespace sbg::tiled {

struct MapMaterializerService;

struct MapMaterializer {
	explicit MapMaterializer(EntityFactory entityFactory) noexcept;
	
	struct MaterializeCallbacks {
		std::variant<std::function<auto(Map const&, TileLayer const&) -> Entity>, std::string> createTileLayer = "tiled-tile-layer";
		std::variant<std::function<auto(Map const&, ImageLayer const&) -> Entity>, std::string> createImageLayer = "tiled-image-layer";
		std::variant<std::function<auto(Map const&, ObjectLayer const&) -> Entity>, std::string> createObjectLayer = {};
		std::variant<std::function<auto(Map const&, GroupLayer const&) -> Entity>, std::string> createGroupLayer = {};
		std::function<auto(Map const&, ObjectLayer const&, Object const&) -> Entity> createObject = {};
		std::variant<std::function<auto(Map const&) -> Entity>, std::string> createMap = {};
		std::function<auto(GroupLayer const&, Layer const&) -> Layer> inheritProperties = {};
	};
	
	auto materialize(Map const& map, MaterializeCallbacks const& callbacks = defaultCallbacks()) const -> MaterializedMap;
	static auto defaultPropertyInheritance(GroupLayer const& parent, Layer const& layer) -> Layer;
	
private:
	EntityFactory _entities;
	
	// TODO: I have no idea why GCC need this. To remove.
	static inline auto defaultCallbacks() -> MaterializeCallbacks {
		return {};
	}
	
	friend auto service_map(MapMaterializer const&) -> MapMaterializerService;
};

} // namespace sbg::tiled
