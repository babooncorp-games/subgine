#include "colorparse.h"

#include <charconv>

namespace sbg::tiled {

auto parse_hex_rgba(std::string_view hex) -> Vector4f {
	auto const hasAlpha = hex.size() == 9;
	auto redPosition = hasAlpha ? 3 : 1;

	auto const hexRed = hex.substr(redPosition, 2);
	auto const hexGreen = hex.substr(redPosition + 2, 2);
	auto const hexBlue = hex.substr(redPosition + 4, 2);
	
	auto red = std::uint8_t{};
	auto green = std::uint8_t{};
	auto blue = std::uint8_t{};
	auto alpha = std::uint8_t{255};

	std::from_chars(hexRed.data(), hexRed.data() + hexRed.size(), red, 16);
	std::from_chars(hexGreen.data(), hexGreen.data() + hexGreen.size(), green, 16);
	std::from_chars(hexBlue.data(), hexBlue.data() + hexBlue.size(), blue, 16);
	
	if (hasAlpha) {
		auto const hexAlpha = hex.substr(1, 2);
		std::from_chars(hexAlpha.data(), hexAlpha.data() + hexAlpha.size(), alpha, 16);
	}
	
	return Vector4f{Vector<4, std::uint8_t>{red, green, blue, alpha}} / 255.f;
}

} // namespace sbg::tiled
