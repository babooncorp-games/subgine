#pragma once

#include "../object.h"

#include "subgine/resource/property.h"

namespace sbg::tiled {

auto load_object(Property data) -> Object;

} // namespace sbg::tiled
