#pragma once

#include <cstdint>

namespace sbg::tiled {

using ObjectId = std::int32_t;

} // namespace sbg::tiled
