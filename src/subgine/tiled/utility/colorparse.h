#pragma once

#include "subgine/vector/vector.h"

#include <string_view>

namespace sbg::tiled {

auto parse_hex_rgba(std::string_view hex) -> Vector4f;

} // namesapce sbg::tiled
