#include "objectparse.h"

#include "parseproperties.h"
#include "shapeparse.h"

#include "subgine/resource/utility/optionalvalue.h"

using namespace std::literals;

namespace sbg::tiled {

auto load_object(Property data) -> Object {
	// TODO: Remove support for type once we drop old tiled support
	return Object{
		.name = value_or(data["name"], ""s),
		.id = std::int32_t{data["id"]},
		.gid = value_optional<std::int32_t>(data["gid"]),
		.type = value_or(data["type"], value_or(data["class"], ""s)),
		.position = Vector2f{float{data["x"]}, float{data["y"]}},
		.size = Vector2f{float{data["width"]}, float{data["height"]}},
		.rotation = float{data["rotation"]},
		.shape = load_shape(data),
		.properties = load_custom_properties(data["properties"]),
	};
}

} // namespace sbg::tiled
