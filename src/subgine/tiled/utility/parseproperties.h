#pragma once

#include "../customproperty.h"

#include "subgine/resource/property.h"

namespace sbg::tiled {

auto load_custom_properties(Property data, CustomProperties const& from = {}) -> CustomProperties;

} // namespace sbg::tiled
