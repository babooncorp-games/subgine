#include "shapeparse.h"

#include "subgine/resource/utility/optionalvalue.h"

#include <algorithm>

namespace sbg::tiled {

auto load_shape(Property data) -> Object::Shape {
	if (auto const jsonPoint = data["point"]; jsonPoint.isBool() && bool{jsonPoint}) {
		return Object::Point{
			.position = sbg::Vector2f{float{data["x"]}, float{data["y"]}}
		};
	} else if (auto const jsonEllipse = data["ellipse"]; jsonEllipse.isBool() && bool{jsonEllipse}) {
		auto const radius = sbg::Vector2f{float{data["width"]}, float{data["height"]}} / 2.f;
		return Object::Ellipse{
			.ellipse = sbg::shape::Ellipse{
				.radius = radius,
				.position = sbg::Vector2f{float{data["x"]}, float{data["y"]}} + radius,
			},
		};
	} else if (auto const jsonPolygon = data["polygon"]; jsonPolygon.isArray()) {
		auto polygon = Object::Polygon{};
		
		std::transform(
			jsonPolygon.begin(), jsonPolygon.end(),
			std::back_inserter(polygon.vertices.vertices),
			[](Property jsonVertex) -> Vector2d {
				return Vector2d{
					double{jsonVertex["x"]},
					double{jsonVertex["y"]}
				};
			}
		);
		
		return polygon;
	} else {
		auto const x = value_optional<double>(data["x"]);
		auto const y = value_optional<double>(data["y"]);
		auto const position = (x && y) ? Vector2d{*x, *y} : Vector2d{};
		auto const width = value_optional<double>(data["width"]);
		auto const height = value_optional<double>(data["height"]);
		auto const size = (width && height) ? Vector2d{*width, *height} : Vector2d{};
		
		return Object::Rectangle{
			shape::AlignedBox2D{
				position,
				position + size
			}
		};
	}
}

} // namespace sbg::tiled
