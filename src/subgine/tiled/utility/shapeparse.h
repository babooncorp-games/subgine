#pragma once

#include "../layer.h"

#include "subgine/resource/property.h"

namespace sbg::tiled {

auto load_shape(Property data) -> Object::Shape;

} // namespace sbg::tiled
