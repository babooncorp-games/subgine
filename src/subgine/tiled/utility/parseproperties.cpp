#include "parseproperties.h"

#include "colorparse.h"

#include "subgine/log.h"

#include <algorithm>

namespace sbg::tiled {

static auto property_from_data(Property data) -> CustomProperty {
	auto type = std::string_view{data["type"]};
	
	if (type == "bool") {
		return CustomProperty{std::in_place_index<0>, bool{data["value"]}};
	} else if (type == "int") {
		return CustomProperty{std::in_place_index<1>, std::int32_t{data["value"]}};
	} else if (type == "float") {
		return CustomProperty{std::in_place_index<2>, float{data["value"]}};
	} else if (type == "string") {
		return CustomProperty{std::in_place_index<3>, std::string{data["value"]}};
	} else if (type == "color") {
		return CustomProperty{std::in_place_index<4>, parse_hex_rgba(std::string_view{data["value"]})};
	} else if (type == "object") {
		return CustomProperty{std::in_place_index<5>, ObjectReference{std::int32_t{data["value"]}}};
	} else if (type == "file") {
		return CustomProperty{std::in_place_index<6>, std::filesystem::path{data["value"]}};
	}
	
	Log::fatal(SBG_LOG_INFO, "Unknown property type", log::enquote(type));
	std::abort();
}

auto load_custom_properties(Property data, CustomProperties const& from) -> CustomProperties {
	auto properties = CustomProperties::MapType{};
	
	std::transform(
		data.begin(), data.end(),
		std::inserter(properties, properties.end()),
		[](Property data) -> std::pair<std::string, CustomProperty> {
			return std::pair{
				std::string{data["name"]},
				property_from_data(data)
			};
		}
	);
	
	std::copy(
		from.begin(), from.end(),
		std::inserter(properties, properties.end())
	);
	
	return CustomProperties{properties};
}

} // namespace sbg::tiled
