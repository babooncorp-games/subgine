#include "mapmaterializer.h"

#include "component/data.h"
#include "layer.h"

#include "subgine/entity/component.h"
#include "subgine/common/math.h"

#include <algorithm>

using namespace sbg;
using namespace tiled;

struct MaterializeCallback {
	std::function<auto(TileLayer const&) -> Entity> createTileLayer;
	std::function<auto(ImageLayer const&) -> Entity> createImageLayer;
	std::function<auto(ObjectLayer const&) -> Entity> createObjectLayer;
	std::function<auto(GroupLayer const&) -> Entity> createGroupLayer;
	std::function<auto(ObjectLayer const&, Object const&) -> Entity> createObject;
	std::function<auto(GroupLayer const&, Layer const&) -> Layer> inheritProperties;
};

static auto materializeLayer(Layer const& layer, MaterializeCallback const& callbacks) -> MaterializedLayer {
	return std::visit(
		[&](auto const& layer) -> MaterializedLayer {
			using LayerType = std::decay_t<decltype(layer)>;
			
			if constexpr (std::is_same_v<LayerType, TileLayer>) {
				return MaterializedTileLayer{layer.name, callbacks.createTileLayer(layer)};
			} else if constexpr (std::is_same_v<LayerType, ImageLayer>) {
				return MaterializedImageLayer{layer.name, callbacks.createImageLayer(layer)};
			} else if constexpr (std::is_same_v<LayerType, ObjectLayer>) {
				auto materialized = MaterializedObjectLayer{layer.name, callbacks.createObjectLayer(layer)};
				
				materialized.objects.reserve(layer.objects.size());
				
				std::transform(
					layer.objects.begin(), layer.objects.end(),
					std::inserter(materialized.objects, materialized.objects.end()),
					[&](Object const& object) -> std::pair<ObjectId, MaterializedObject> {
						return std::pair{object.id, MaterializedObject{object.name, callbacks.createObject(layer, object)}};
					}
				);
				
				return materialized;
			} else if constexpr (std::is_same_v<LayerType, GroupLayer>) {
				auto materialized = MaterializedGroupLayer{layer.name, callbacks.createGroupLayer(layer)};
				
				materialized.layers = materialized.layers.update([&](auto layers) {
					layers.reserve(layer.layers.size());
					std::transform(
						layer.layers.begin(), layer.layers.end(),
						std::back_inserter(layers),
						[&](Layer const& childLayer) -> MaterializedLayer {
							auto const inheritedLayer = callbacks.inheritProperties
								? callbacks.inheritProperties(layer, childLayer)
								: MapMaterializer::defaultPropertyInheritance(layer, childLayer);
							
							return materializeLayer(inheritedLayer, callbacks);
						}
					);
					return layers;
				});
				
				return materialized;
			}
		},
		layer
	);
}

MapMaterializer::MapMaterializer(EntityFactory entities) noexcept : _entities{entities}  {}

auto MapMaterializer::materialize(Map const& map, MaterializeCallbacks const& callbacks) const -> MaterializedMap {
	auto materialized = MaterializedMap{};
	
	materialized.layers = materialized.layers.update([&](auto layers) {
		layers.reserve(map.layers.size());
		
		if (auto name = std::get_if<std::string>(&callbacks.createMap)) {
			materialized.entity = _entities.create(*name, Data{.map = map});
		} else if (auto function = std::get_if<0>(&callbacks.createMap); function && *function) {
			materialized.entity = (*function)(map);
		}
		
		std::transform(
			map.layers.begin(), map.layers.end(),
			std::back_inserter(layers),
			[&](Layer const& layer) -> MaterializedLayer {
				return materializeLayer(layer, {
					[&](TileLayer const& layer) {
						if (auto name = std::get_if<std::string>(&callbacks.createTileLayer)) {
							return _entities.create(*name, Data{.map = map, .tileLayer = layer});
						} else if (auto function = std::get_if<0>(&callbacks.createTileLayer); function && *function) {
							return (*function)(map, layer);
						}
						
						return Entity{};
					},
					[&](ImageLayer const& layer) {
						if (auto name = std::get_if<std::string>(&callbacks.createImageLayer)) {
							return _entities.create(*name, Data{.map = map, .imageLayer = layer});
						} else if (auto function = std::get_if<0>(&callbacks.createImageLayer); function && *function) {
							return (*function)(map, layer);
						}
						
						return Entity{};
					},
					[&](ObjectLayer const& layer) {
						if (auto name = std::get_if<std::string>(&callbacks.createObjectLayer)) {
							return _entities.create(*name, Data{.map = map, .objectLayer = layer});
						} else if (auto function = std::get_if<0>(&callbacks.createObjectLayer); function && *function) {
							return (*function)(map, layer);
						}
						
						return Entity{};
					},
					[&](GroupLayer const& layer) {
						if (auto name = std::get_if<std::string>(&callbacks.createGroupLayer)) {
							return _entities.create(*name, Data{.map = map, .groupLayer = layer});
						} else if (auto function = std::get_if<0>(&callbacks.createGroupLayer); function && *function) {
							return (*function)(map, layer);
						}
						
						return Entity{};
					},
					[&](ObjectLayer const& layer, Object const& object) {
						if (!callbacks.createObject) {
							return _entities.create(object.type, Data{
								.map = map,
								.objectLayer = layer,
								.object = object,
							});
						} else {
							return callbacks.createObject(map, layer, object);
						}
						
						return Entity{};
					},
					callbacks.inheritProperties
				});
			}
		);
		
		return layers;
	});
	
	if (auto const tiledData = Component<Data>{materialized.entity}) {
		tiledData->materialized = Data::Materialized{materialized, std::nullopt, std::nullopt, std::nullopt, std::nullopt, std::nullopt};
	}
	
	materialized.walkLayers([&](auto const& layer) {
		using T = std::decay_t<decltype(layer)>;
		
		if constexpr (std::is_same_v<T, MaterializedTileLayer>) {
			if (auto const tiledData = Component<Data>{layer.entity}) {
				tiledData->materialized = Data::Materialized{materialized, layer, std::nullopt, std::nullopt, std::nullopt, std::nullopt};
			}
		} else if constexpr (std::is_same_v<T, MaterializedImageLayer>) {
			if (auto const tiledData = Component<Data>{layer.entity}) {
				tiledData->materialized = Data::Materialized{materialized, std::nullopt, layer, std::nullopt, std::nullopt, std::nullopt};
			}
		} else if constexpr (std::is_same_v<T, MaterializedObjectLayer>) {
			if (auto const tiledData = Component<Data>{layer.entity}) {
				tiledData->materialized = Data::Materialized{materialized, std::nullopt, std::nullopt, layer, std::nullopt, std::nullopt};
			}
			
			for (auto const& [id, object] : layer.objects) {
				if (auto const tiledData = Component<Data>{object.entity}) {
					tiledData->materialized = Data::Materialized{materialized, std::nullopt, std::nullopt, layer, std::nullopt, object};
				}
			}
		} else if constexpr (std::is_same_v<T, MaterializedGroupLayer>) {
			if (auto const tiledData = Component<Data>{layer.entity}) {
				tiledData->materialized = Data::Materialized{materialized, std::nullopt, std::nullopt, std::nullopt, layer, std::nullopt};
			}
		}
	});
	
	return materialized;
}

auto MapMaterializer::defaultPropertyInheritance(GroupLayer const& parent, Layer const& layer) -> Layer {
	return std::visit(
		[&](auto layer) -> Layer {
			layer.opacity *= parent.opacity;
			layer.tint *= parent.tint;
			layer.offset += parent.offset;
			
			layer.properties = layer.properties.update([&](CustomProperties::MapType properties) {
				if (auto const parentBlend = parent.properties.get_optional<Vector4f>("Blend")) {
					CustomProperties::set(properties, "Blend", *parentBlend, [&](Vector4f blend) {
						auto const mixed = lerp(*parentBlend, blend, 1.f - parentBlend->w);
						return Vector4f{mixed.x, mixed.y, mixed.z, 1.f - ((1.f - parentBlend->w) * (1.f - blend.w))};
					});
				}
				
				if (auto const parentScale = parent.properties.get_optional<float>("Parallax Scale")) {
					CustomProperties::set(properties, "Parallax Scale", *parentScale, [&](float scale) {
						return *parentScale * scale;
					});
				}
				
				if (auto const parentParallax = parent.properties.get_optional<float>("Parallax")) {
					CustomProperties::set(properties, "Parallax", *parentParallax, [&](float parallax) {
						return *parentParallax * parallax;
					});
				}
				
				return properties;
			});
			
			return layer;
		},
		layer
	);
}
