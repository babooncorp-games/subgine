#pragma once

#include "subgine/common/kangaru.h"
#include "subgine/entity/basicentity.h"
#include "plugger.h"
#include "loader.h"

namespace sbg {

struct CurrentSceneService;
struct SceneService;

struct Scene {
	/**
	 * Constructor for scene. Initialize it's loading function using makeLoad.
	 * 
	 * @param container Container to be this scene's container. Used to call makeLoad.
	 * @param parent A reference to the parent's container.
	 * @param makeLoad A function that returns the loading function of this scene.
	 */
	template<typename T, enable_if_i<std::is_invocable_v<T, kgr::container&>> = 0>
	Scene(kgr::container container, kgr::container& parent, T makeLoad) :
		_container{std::move(container)}, _parent{parent}, _loader{(setupContainer(_container), makeLoad(_container))} {}
	
	/**
	 * Destructor of this scene.
	 * 
	 * Will call unplug and free in this order.
	 */
	~Scene();
	
	/**
	 * Plugs the scene into the global state.
	 * By convetion makes the scene visible and interactible with the user.
	 */
	void plug();
	
	/**
	 * Unplugs the scene into the global state.
	 * Remove all reference of this scene from the global state of the game.
	 */
	void unplug();
	
	/**
	 * Remove all entities tied to this scene.
	 */
	void unload();
	
	/**
	 * Calls the loading function.
	 */
	void load();
	
	/**
	 * Attach an entity to this scene.
	 */
	void attach(BasicEntity entity);
	
	/**
	 * Detach an entity from this scene. Could be used for moving an entity between scenes.
	 */
	void detach(BasicEntity entity);
	
	/**
	 * Adds a new plugger to be called when plugging/unplugging the scene.
	 * 
	 * @param plugger The plugger to be added.
	 */
	void addPlugger(Plugger plugger);
	
	/**
	 * Returns true if the scene is acually plugged into the game.
	 */
	bool isPlugged() const;
	
	/**
	 * Returns if the scene is loaded.
	 */
	bool isLoaded() const;
	
	/**
	 * Returns a reference the this scene's container
	 */
	kgr::container& container();
	const kgr::container& container() const;
	
	/**
	 * Calls a function in the context of the parent scene.
	 * 
	 * @param function The function to be called.
	 */
	template<typename T>
	decltype(auto) parent(T&& function) {
		return _parent.invoke(function);
	}
	
private:
	void setupContainer(kgr::container& container);
	
	bool _plugged = false;
	bool _loaded = false;
	kgr::container _container;
	kgr::container& _parent;
	Loader _loader;
	std::vector<Plugger> _pluggers;
	std::vector<BasicEntity> _entities;
	
	friend auto service_map(Scene const&, kgr::map_t<>) -> CurrentSceneService;
	friend auto service_map(Scene&&) -> SceneService;
};

} // namespace sbg
