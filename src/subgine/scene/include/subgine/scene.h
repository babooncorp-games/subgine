#pragma once

#include "scene/service.h"

#include "../../creator/pluggercreator.h"
#include "../../creator/scenecreator.h"

#include "../../loader.h"
#include "../../plugger.h"
#include "../../scene.h"
#include "../../scenegraph.h"
#include "../../scenemanager.h"
#include "../../scenenode.h"

#include "../../utility/trait.h"
