#include "scenemodule.h"

#include "subgine/scene.h"

namespace sbg {

void SceneModule::setupSceneCreator(SceneCreator& sceneCreator) {
	sceneCreator.add("empty", []{ return []{}; });
}

} // namespace sbg
