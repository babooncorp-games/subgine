#pragma once

#include "utility/trait.h"
#include "subgine/common/traits.h"

#include <memory>

namespace sbg {

struct Loader {
	template<typename L, typename U, enable_if_i<std::is_invocable_v<L> && std::is_invocable_v<U>> = 0>
	explicit Loader(L l, U u) : _loader{std::make_unique<LambdaLoader<L, U>>(std::move(l), std::move(u))} {}
	
	template<typename T, enable_if_i<is_loader<T>> = 0>
	explicit Loader(T loader) : _loader{std::make_unique<ObjectLoader<T>>(std::move(loader))} {}
	
	template<typename L, enable_if_i<!is_loader<L> && std::is_invocable_v<L>> = 0>
	explicit Loader(L loader) : _loader{std::make_unique<LambdaLoader<L, EmptyLambda>>(std::move(loader), empty_lambda)} {}
	
	void load() const {
		_loader->load();
	}
	
	void unload() const {
		_loader->unload();
	}
	
private:
	struct EmptyLambda {
		constexpr void operator()() const noexcept {}
	} static constexpr empty_lambda{};
	
	struct AbstractLoader {
		virtual ~AbstractLoader() = default;
		virtual void load() = 0;
		virtual void unload() = 0;
	};
	
	template<typename L, typename U>
	struct LambdaLoader : AbstractLoader {
		explicit LambdaLoader(L l, U u) noexcept : _load{std::move(l)}, _unload{std::move(u)} {}
		
		void load() override {
			_load();
		}
		
		void unload() override {
			_unload();
		}
		
	private:
		L _load;
		U _unload;
	};
	
	template<typename T>
	struct ObjectLoader : AbstractLoader {
		explicit ObjectLoader(T loader) noexcept : _loader{std::move(loader)} {}
		
		void load() override {
			_loader.load();
		}
		
		void unload() override {
			_loader.unload();
		}
		
	private:
		T _loader;
	};
	
	std::unique_ptr<AbstractLoader> _loader;
};

} // namespace sbg
