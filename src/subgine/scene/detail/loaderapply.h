#pragma once

#include "subgine/common/kangaru.h"
#include "subgine/common/traits.h"
#include "subgine/common/dropping_invoke.h"
#include "subgine/resource/property.h"

#include <functional>

namespace sbg {

struct LoaderApply {
	using Result = std::pair<std::function<void()>, std::function<void()>>;
	
	template<typename B, enable_if_i<std::is_constructible_v<Result, dropping_invoke_result_t<kgr::invoker, B, Property>>> = 0>
	auto operator()(kgr::invoker invoker, B& builder, Property data) const {
		return dropping_invoke(invoker, builder, std::move(data));
	}
	
	template<typename B, enable_if_i<std::is_constructible_v<std::function<void()>, dropping_invoke_result_t<kgr::invoker, B, Property>>> = 0>
	auto operator()(kgr::invoker invoker, B& builder, Property data) const {
		return std::make_pair(dropping_invoke(invoker, builder, std::move(data)), []{});
	}
};

}
