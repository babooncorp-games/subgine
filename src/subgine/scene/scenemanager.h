#pragma once

#include "creator/scenecreator.h"

#include <string_view>

namespace sbg {

struct SceneManagerService;

struct Scene;
struct SceneGraph;

struct SceneManager {
	SceneManager(SceneGraph& sceneGraph, SceneFactory factory);
	
	Scene& root() const;
	
	/**
	 * Load the scene and all its children.
	 */
	void load(Scene& scene);
	
	/**
	 * Plug the scene and all its children.
	 */
	void plug(Scene& scene);
	
	/**
	 * Unload the scene and all its children.
	 */
	void unload(Scene& scene);
	
	/**
	 * Unplug the scene and all its children.
	 */
	void unplug(Scene& scene);
	
	/**
	 * Create a scene and its children.
	 */
	Scene& create(std::string_view name);
	
	/**
	 * Delete a scene and its children. Killing immediatly any entity in these scenes.
	 */
	void destroy(Scene& scene);
	
private:
	SceneGraph& _sceneGraph;
	SceneFactory _factory;
	
	/**
	 * The current scene; the one that contains this service.
	 * Should never be null after creation.
	 */
	Scene* _root = nullptr;
	
	friend auto service_map(SceneManager const&) -> SceneManagerService;
};

} // namespace sbg
