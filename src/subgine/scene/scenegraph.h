#pragma once

#include "scenenode.h"
#include <list>
#include <unordered_map>

namespace sbg {

struct SceneGraphService;

struct SceneGraph {
	void add(SceneNode node);
	void remove(SceneNode& node);
	
	SceneNode& node(const Scene& scene) const;
	auto extract(Scene const& scene) noexcept -> SceneNode;
	void adopt(SceneNode&);
	
	template<typename T>
	static auto walkChildren(SceneNode& scene, T function) -> std::enable_if_t<std::is_invocable_v<T, Scene&>> {
		for (auto& child : scene.children) {
			function(*child.scene);
		}
		
		for (auto& child : scene.children) {
			walkChildren(child, function);
		}
	}
	
	template<typename T>
	static auto walkChildren(SceneNode& scene, T function) -> std::enable_if_t<std::is_invocable_v<T, SceneNode&>> {
		for (auto& child : scene.children) {
			function(child);
		}
		
		for (auto& child : scene.children) {
			walkChildren(child, function);
		}
	}
	
	template<typename T>
	static auto walkChildren(SceneNode& scene, T function) -> std::enable_if_t<std::is_invocable_v<T, Scene&, SceneNode&>> {
		for (auto& child : scene.children) {
			function(*child.scene, child);
		}
		
		for (auto& child : scene.children) {
			walkChildren(child, function);
		}
	}
	
private:
	std::list<SceneNode> _graph;
	std::unordered_map<const Scene*, SceneNode*> _nodes;
	
	friend auto service_map(SceneGraph const&) -> SceneGraphService;
};

} // namespace sbg
