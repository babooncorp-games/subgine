#pragma once

namespace sbg {

struct SceneModuleService;

struct SceneCreator;

struct SceneModule {
	void setupSceneCreator(SceneCreator& sceneCreator);
	
	friend auto service_map(const SceneModule&) -> SceneModuleService;
};

} // namespace sbg
