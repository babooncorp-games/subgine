#include "scenecreator.h"

#include "../scene.h"

#include "../service/scenemanagerservice.h"
#include "../service/sceneservice.h"
#include "../service/currentsceneservice.h"

#include "subgine/resource.h"
#include "subgine/log.h"

using namespace sbg;

SceneCreator::SceneCreator(GameDatabase& database, kgr::generator<SceneService> sceneGenerator, PluggerCreator& pluggerCreator) :
	_database{&database}, _sceneGenerator{sceneGenerator}, _pluggerCreator{&pluggerCreator} {}

SceneNode SceneCreator::create(kgr::invoker invoker, std::string_view name) const {
	auto const data = _database->get("scene", name);
	
	auto l = Log::trace(SBG_LOG_INFO, "Creating root scene", name);
	return create(invoker, data);
}

SceneNode SceneCreator::create(kgr::invoker invoker, Property data) const {
	auto& parent = invoker([](kgr::container& c) -> auto& { return c; });
	
	auto scene = _sceneGenerator(
		parent,
		[&](kgr::container& c) {
			c.replace<GameDatabaseService>();
			auto& database = c.service<GameDatabaseService>();
			data.bind(database);
			
			auto const loader = data["load"];
			auto const type = std::string_view{loader["type"]};
			auto l = Log::trace(SBG_LOG_INFO, "Creating scene of type", log::enquote(type));
			
			{
				auto l = Log::trace(SBG_LOG_INFO, "Loading databases", data["databases"]);
				database.load(std::vector<std::filesystem::path>{data["databases"]});
			}
			
			return build(c.service<kgr::invoker_service>(), type, loader["data"]);
		}
	);
	
	for (auto const plugger : data["pluggers"]) {
		auto const type = std::string_view{plugger["type"]};
		auto l = Log::trace(SBG_LOG_INFO, "Creating plugger of type", log::enquote(type));
		scene->addPlugger(_pluggerCreator->create(scene->container().service<kgr::invoker_service>(), type, plugger["data"]));
	}
	
	SceneNode node{
		std::move(scene),
		parent.contains<CurrentSceneService>() ? &parent.service<CurrentSceneService>() : nullptr
	};
	
	for (auto const child : data["children"]) {
		auto l = Log::trace(SBG_LOG_INFO, "Creating child scene");
		node.children.emplace_back(create(node.scene->container().service<kgr::invoker_service>(), child));
	}
	
	return node;
}
