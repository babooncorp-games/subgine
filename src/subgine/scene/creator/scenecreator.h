#pragma once

#include "pluggercreator.h"
#include "../loader.h"
#include "../service/sceneservice.h"
#include "../scenenode.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/resource/property.h"
#include "subgine/common/kangaru.h"

#include <string_view>

namespace sbg {

struct SceneCreatorService;
struct GameDatabase;
struct SceneGraph;

struct SceneCreator : Creator<SimpleBuilder<Loader>> {
	SceneCreator(GameDatabase& database, kgr::generator<SceneService> sceneGenerator, PluggerCreator& pluggerCreator);
	
	SceneNode create(kgr::invoker invoker, std::string_view name) const;
	SceneNode create(kgr::invoker invoker, Property data) const;
	
private:
	PluggerCreator* _pluggerCreator;
	GameDatabase* _database;
	mutable kgr::generator<SceneService> _sceneGenerator;
	
	friend auto service_map(SceneCreator const&) -> SceneCreatorService;
};

using SceneFactory = Factory<SceneCreator>;

} // namespace sbg
