#pragma once

#include "../plugger.h"
#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

struct PluggerCreatorService;

struct PluggerCreator : Creator<SimpleBuilder<Plugger>> {
	using Creator::create;
	friend auto service_map(PluggerCreator const&) -> PluggerCreatorService;
};

using PluggerFactory = Factory<PluggerCreator>;

} // namespace sbg
