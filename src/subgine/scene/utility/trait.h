#pragma once

#include "subgine/common/detection.h"

#include <utility>

namespace sbg {
namespace detail::scene {

template<typename T>
using plug_t = decltype(std::declval<T>().plug());

template<typename T>
using unplug_t = decltype(std::declval<T>().unplug());

template<typename T>
using load_t = decltype(std::declval<T>().load());

template<typename T>
using unload_t = decltype(std::declval<T>().unload());

} // namespace detail::scene

template<typename T>
inline constexpr bool is_plugger = is_detected_v<detail::scene::plug_t, T> && is_detected_v<detail::scene::unplug_t, T>;

template<typename T>
inline constexpr bool is_loader = is_detected_v<detail::scene::load_t, T> && is_detected_v<detail::scene::unload_t, T>;

} // namespace sbg
