#pragma once

#include "utility/trait.h"
#include "subgine/common/traits.h"

#include <memory>

namespace sbg {

struct Plugger {
	template<typename P, typename U, enable_if_i<std::is_invocable_v<P> && std::is_invocable_v<U>> = 0>
	explicit Plugger(P p, U u) : _plugger{std::make_unique<LambdaPlugger<P, U>>(std::move(p), std::move(u))} {}
	
	template<typename T, enable_if_i<is_plugger<T>> = 0>
	explicit Plugger(T plugger) : _plugger{std::make_unique<ObjectPlugger<T>>(std::move(plugger))} {}
	
	template<typename P, enable_if_i<!is_plugger<P> && std::is_invocable_v<P>> = 0>
	explicit Plugger(P plugger) : _plugger{std::make_unique<LambdaPlugger<P, EmptyLambda>>(std::move(plugger), empty_lambda)} {}
	
	void plug() const {
		_plugger->plug();
	}
	
	void unplug() const {
		_plugger->unplug();
	}
	
private:
	struct EmptyLambda {
		constexpr void operator()() const noexcept {}
	} static constexpr empty_lambda{};
	
	struct AbstractPlugger {
		virtual ~AbstractPlugger() = default;
		virtual void plug() = 0;
		virtual void unplug() = 0;
	};
	
	template<typename P, typename U>
	struct LambdaPlugger : AbstractPlugger {
		explicit LambdaPlugger(P p, U u) noexcept : _plug{std::move(p)}, _unplug{std::move(u)} {}
		
		void plug() override {
			_plug();
		}
		
		void unplug() override {
			_unplug();
		}
		
	private:
		P _plug;
		U _unplug;
	};
	
	template<typename T>
	struct ObjectPlugger : AbstractPlugger {
		explicit ObjectPlugger(T plugger) noexcept : _plugger{std::move(plugger)} {}
		
		void plug() override {
			_plugger.plug();
		}
		
		void unplug() override {
			_plugger.unplug();
		}
		
	private:
		T _plugger;
	};
	
	std::unique_ptr<AbstractPlugger> _plugger;
};

} // namespace sbg
