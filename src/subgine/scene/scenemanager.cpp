#include "scenemanager.h"

#include "scenegraph.h"
#include "creator/scenecreator.h"

namespace sbg {

SceneManager::SceneManager(SceneGraph& sceneGraph, SceneFactory factory) : _sceneGraph{sceneGraph}, _factory{factory} {}

Scene& SceneManager::root() const {
	return *_root;
}

void SceneManager::load(Scene& scene) {
	auto& node = _sceneGraph.node(scene);
	
	scene.load();
	
	_sceneGraph.walkChildren(node, [](Scene& child){
		child.load();
	});
}

void SceneManager::plug(Scene& scene) {
	auto& node = _sceneGraph.node(scene);
	
	if (_root) {
		unplug(*_root);
	}
	
	scene.plug();
	_root = &scene;
	
	_sceneGraph.walkChildren(node, [](Scene& child){
		child.plug();
	});
}

void SceneManager::unload(Scene& scene) {
	auto& node = _sceneGraph.node(scene);
	
	scene.unload();
	
	_sceneGraph.walkChildren(node, [](Scene& child){
		child.unload();
	});
}

void SceneManager::unplug(Scene& scene) {
	auto& node = _sceneGraph.node(scene);
	
	scene.unplug();
	
	if (&scene == _root) {
		_root = nullptr;
	}
	
	_sceneGraph.walkChildren(node, [](Scene& child){
		child.unplug();
	});
}

Scene& SceneManager::create(std::string_view name) {
	auto node = _factory.create(name);
	auto& scene = *node.scene;
	
	_sceneGraph.add(std::move(node));
	
	return scene;
}

void SceneManager::destroy(Scene& scene) {
	auto& node = _sceneGraph.node(scene);
	
	unplug(scene);
	
	_sceneGraph.remove(node);
}

} // namespace sbg
