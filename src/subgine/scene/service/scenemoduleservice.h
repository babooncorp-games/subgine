#pragma once

#include "../scenemodule.h"

#include "scenecreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SceneModuleService : kgr::single_service<SceneModule>,
	autocall<
		&SceneModule::setupSceneCreator
	> {};

} // namespace evo
