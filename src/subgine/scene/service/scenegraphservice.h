#pragma once

#include "../scenegraph.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct SceneGraphService : kgr::single_service<SceneGraph> {};

} // namespace sbg
