#pragma once

#include "../creator/pluggercreator.h"

#include "subgine/resource/service/factoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct PluggerCreatorService : kgr::single_service<PluggerCreator> {};
using PluggerFactoryService = FactoryService<PluggerFactory>;

} // namespace sbg
