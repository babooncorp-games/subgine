#pragma once

#include "../scene.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SceneService : kgr::unique_service<Scene, kgr::autowire> {};

} // namespace sbg
