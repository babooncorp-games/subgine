#pragma once

#include "../creator/scenecreator.h"

#include "sceneservice.h"
#include "scenegraphservice.h"
#include "pluggercreatorservice.h"

#include "subgine/resource/service/factoryservice.h"
#include "subgine/resource/service/gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SceneCreatorService : kgr::single_service<SceneCreator, kgr::autowire> {};
using SceneFactoryService = FactoryService<SceneFactory>;

} // namespace sbg
