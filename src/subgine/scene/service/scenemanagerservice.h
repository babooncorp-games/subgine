#pragma once

#include "../scenemanager.h"

#include "scenecreatorservice.h"
#include "scenegraphservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct SceneManagerService : kgr::single_service<SceneManager, kgr::autowire> {};

} // namespace sbg
