#pragma once

#include "subgine/common/kangaru.h"

namespace sbg {

struct Scene;

struct CurrentSceneService : kgr::single_service<Scene*>, kgr::supplied {
	using kgr::single_service<Scene*>::single_service;
	
	inline static auto construct(Scene& scene) -> decltype(auto) {
		return kgr::inject(&scene);
	}
	
	inline Scene& forward() {
		return *instance();
	}
};

} // namespace sbg
