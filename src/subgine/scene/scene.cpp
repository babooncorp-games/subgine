#include "scene.h"

#include "service/currentsceneservice.h"
#include "subgine/entity/service/entitymanagerservice.h"

#include <algorithm>

namespace sbg {

Scene::~Scene() {
	unplug();
	unload();
}

void Scene::plug() {
	if (!_plugged) {
		_plugged = true;
		for (auto& plugger : _pluggers) {
			plugger.plug();
		}
	}
}

void Scene::unplug() {
	if (_plugged) {
		_plugged = false;
		std::for_each(std::rbegin(_pluggers), std::rend(_pluggers), [](auto& plugger) {
			plugger.unplug();
		});
	}
}

void Scene::addPlugger(Plugger plugger) {
	_pluggers.emplace_back(std::move(plugger));
}

void Scene::load() {
	if (!_loaded) {
		_loaded = true;
		_loader.load();
	}
}

void Scene::unload() {
	if (_loaded) {
		_loaded = false;
		
		auto& entityManager = _container.service<EntityManagerService>();
		
		_loader.unload();
		
		for (auto const entity : _entities) {
			entityManager.destroy(entity);
		}
		
		_entities.clear();
	}
}

void Scene::attach(BasicEntity entity) {
	_entities.emplace_back(entity);
}

void Scene::detach(BasicEntity entity) {
	_entities.erase(std::remove(_entities.begin(), _entities.end(), entity), _entities.end());
}

bool Scene::isPlugged() const {
	return _plugged;
}

bool Scene::isLoaded() const {
	return _loaded;
}

kgr::container& Scene::container() {
	return _container;
}

const kgr::container& Scene::container() const {
	return _container;
}

void Scene::setupContainer(kgr::container& container) {
	container.emplace<CurrentSceneService>(*this);
}

} // namespace sbg
