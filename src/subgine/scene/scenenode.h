#pragma once

#include "scene.h"
#include <list>

namespace sbg {

struct SceneNode {
	std::unique_ptr<Scene> scene;
	Scene* parent;
	std::list<SceneNode> children = {};
	
	bool operator == (const SceneNode& node) const { return scene == node.scene; }
	bool operator != (const SceneNode& node) const { return !operator==(node); }
};

} // namespace sbg
