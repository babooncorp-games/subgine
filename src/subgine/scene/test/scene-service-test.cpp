#include "catch2/catch.hpp"

#include "subgine/scene/service.h"
#include "subgine/scene/module.h"

using namespace sbg;

TEST_CASE("Scene services", "[scene, service]") {
	auto makeLoad = [](kgr::container&) { return []{}; };
	CHECK((kgr::detail::is_service_valid<CurrentSceneService>::value));
	CHECK((kgr::detail::is_service_valid<PluggerCreatorService>::value));
	CHECK((kgr::detail::is_service_valid<SceneCreatorService>::value));
	CHECK((kgr::detail::is_service_valid<SceneFactoryService>::value));
	CHECK((kgr::detail::is_service_valid<SceneGraphService>::value));
	CHECK((kgr::detail::is_service_valid<SceneManagerService>::value));
	CHECK((kgr::detail::is_service_valid<SceneModuleService>::value));
	CHECK((kgr::detail::is_service_valid<SceneService, decltype(makeLoad)>::value));
}
