#include "scenegraph.h"

namespace sbg {

void SceneGraph::add(SceneNode node) {
	auto& inserted = _graph.emplace_back(std::move(node));
	
	_nodes.emplace(inserted.scene.get(), &inserted);
	
	walkChildren(inserted, [&](const Scene& scene, SceneNode& n) {
		_nodes.emplace(&scene, &n);
	});
}

void SceneGraph::remove(SceneNode& node) {
	_nodes.erase(node.scene.get());
	
	walkChildren(node, [&](const Scene& scene) {
		_nodes.erase(&scene);
	});
	
	_graph.remove(node);
}

SceneNode& SceneGraph::node(const Scene& scene) const {
	return *_nodes.at(&scene);
}

auto SceneGraph::extract(Scene const& scene) noexcept -> SceneNode {
	auto element = _nodes.extract(&scene);
	auto node = std::move(*element.mapped());
	
	std::erase_if(_graph, [&](SceneNode const& node) {
		return node.scene.get() == &scene;
	});
	
	return node;
}

void SceneGraph::adopt(SceneNode& node) {
	_nodes.emplace(node.scene.get(), &node);
}

} // namespace sbg
