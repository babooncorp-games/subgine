#pragma once

namespace sbg {

struct ConsoleModuleService;

struct ConsoleModule {
	friend auto service_map(ConsoleModule const&) -> ConsoleModuleService;
};

} // namespace sbg
