#include "consolecommands.h"

#include "subgine/log.h"

using namespace sbg;

auto ConsoleCommands::execute(kgr::invoker invoker, ConsoleContext context) const -> int {
	auto const it = _commands.find(context.args[0]);
	
	if (it != _commands.end()) {
		if (context.args.size() >= 2) {
			auto const sub = it->second.subcommands.find(context.args[1]);
			
			if (sub != it->second.subcommands.end()) {
				return sub->second.command(invoker, context.subcommand_context());
			}
		}
		
		if (it->second.command) {
			return it->second.command(invoker, context.command_context());
		} else {
			context.error("error: No subcommand for \"", context.args[0], "\" specified.\nusage: ");
			for (auto const& command : it->second.subcommands) {
				context.error(context.args[0], ' ', command.first, "\n         ");
			}
			context.error('\n');
		}
	} else {
		context.error("error: Unknown command: ", context.args[0], '\n');
	}
	
	return 1;
}

void ConsoleCommands::insert_command(std::string_view name, ConsoleCommands::command_action_t command, std::string_view help) {
	_commands.try_emplace(name, Command{{command, help}});
}

void ConsoleCommands::insert_subcommand(std::string_view name, std::string_view subname, ConsoleCommands::command_action_t command, std::string_view help) {
	auto [it, inserted] = _commands.try_emplace(name);
	auto [subit, subinserted] = it->second.subcommands.try_emplace(subname, BasicCommand{command, help});
	
	Log::error([subinserted = subinserted]{ return !subinserted; },
		SBG_LOG_INFO, "Subcommand", log::enquote(subname), "is already present in command", log::enquote(name)
	);
}
