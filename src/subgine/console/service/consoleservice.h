#pragma once

#include "../console.h"
#include "consolecommandsservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleService : kgr::service<Console, kgr::autowire> {};

} // namespace sbg
