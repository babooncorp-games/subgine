#pragma once

#include "../consolemodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleModuleService : kgr::single_service<ConsoleModule> {};

} // namespace sbg
