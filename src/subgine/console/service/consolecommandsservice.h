#pragma once

#include "../consolecommands.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleCommandsService : kgr::single_service<ConsoleCommands> {};

} // namespace sbg
