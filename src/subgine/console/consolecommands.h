#pragma once

#include "consolecontext.h"
#include "subgine/common/kangaru.h"
#include "subgine/common/traits.h"

#include <map>
#include <string_view>
#include <functional>

namespace sbg {

struct ConsoleCommandsService;

struct ConsoleCommands {
	auto execute(kgr::invoker invoker, ConsoleContext args) const -> int;
	
	template<typename F, enable_if_i<std::is_invocable_v<kgr::invoker, F, ConsoleContext>> = 0>
	void command(std::string_view name, F function, std::string_view help = {}) {
		insert_command(name, make_command(function), help);
	}
	
	template<typename F, enable_if_i<std::is_invocable_v<kgr::invoker, F, ConsoleContext>> = 0>
	void command(std::string_view name, std::string_view subname, F function, std::string_view help = {}) {
		insert_subcommand(name, subname, make_command(function), help);
	}
	
private:
	using command_action_t = std::function<int(kgr::invoker, ConsoleContext)>;
	
	struct BasicCommand {
		command_action_t command = {};
		std::string_view help = {};
	};
	
	struct Command : BasicCommand {
		std::map<std::string_view, BasicCommand> subcommands = {};
	};
	
	void insert_command(std::string_view name, command_action_t command, std::string_view help);
	void insert_subcommand(std::string_view name, std::string_view subname, command_action_t command, std::string_view help);
	
	template<typename F>
	auto make_command(F function) -> command_action_t {
		return [function](kgr::invoker invoker, ConsoleContext args) {
			return invoker(function, args);
		};
	}
	
	std::map<std::string_view, Command> _commands;
	
	friend auto service_map(ConsoleCommands const&) -> ConsoleCommandsService;
};

} // namespace sbg
