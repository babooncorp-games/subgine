#include "consolecontext.h"

#include <string>

using namespace sbg;

auto ConsoleContext::subcommand_context() const& noexcept -> ConsoleContext {
	return ConsoleContext{*out, *err, *in, program, args.subspan(2)};
}

auto ConsoleContext::command_context() const& noexcept -> ConsoleContext {
	return ConsoleContext{*out, *err, *in, program, args.subspan(1)};
}

auto ConsoleContext::input() const& -> std::string {
	std::string str;
	std::getline(*in, str);
	return str;
}

auto ConsoleContext::prompt(std::string_view message) const& -> std::string {
	output(message);
	return input();
}
