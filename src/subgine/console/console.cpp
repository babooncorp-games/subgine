#include "console.h"

#include "consolecontext.h"
#include "consolecommands.h"

#include <iostream>

using namespace sbg;

auto Console::default_context(int argc, const char** argv) -> ConsoleContext {
	return ConsoleContext{std::cout, std::cerr, std::cin, argv[0], {argv + 1, argv + argc}};
}

void Console::execute(ConsoleContext context) const {
	_commands->execute(_invoker, context);
}
