#pragma once

#include "subgine/common/memberreference.h"

#include <stdexcept>
#include <span>
#include <string_view>
#include <ostream>
#include <istream>

namespace sbg {

struct ConsoleContext {
	ConsoleContext(
		std::ostream& _out,
		std::ostream& _err,
		std::istream& _in,
		std::string_view _program,
		std::span<char const*> _args
	) noexcept :
		program{_program}, args{_args}, out{&_out}, err{&_err}, in{&_in} {}
	
	std::string_view const program;
	std::span<char const*> const args;
	
	[[nodiscard]] auto subcommand_context() const& noexcept -> ConsoleContext;
	[[nodiscard]] auto command_context() const& noexcept -> ConsoleContext;
	
	template<typename... Args>
	void output(Args const&... args) const& {
		((*out) << ... << args);
	}
	
	auto input() const& -> std::string;
	auto prompt(std::string_view message) const& -> std::string;
	
	template<typename... Args>
	void error(Args const&... args) const& {
		((*err) << ... << args);
	}
	
private:
	std::ostream* out;
	std::ostream* err;
	std::istream* in;
};

} // namespace sbg
