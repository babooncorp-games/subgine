#pragma once

#include "consolecontext.h"
#include "subgine/common/kangaru.h"

#include <map>
#include <functional>
#include <stdexcept>
#include <span>

namespace sbg {

struct ConsoleService;
struct ConsoleCommands;

struct Console {
	Console(kgr::invoker invoker, ConsoleCommands& commands) : _invoker{invoker}, _commands{&commands} {}
	
	void execute(ConsoleContext context) const;
	static auto default_context(int argc, char const** argv) -> ConsoleContext;
	
private:
	kgr::invoker _invoker;
	ConsoleCommands* _commands;
	
	friend auto service_map(Console const&) -> ConsoleService;
};

} // namespace sbg
