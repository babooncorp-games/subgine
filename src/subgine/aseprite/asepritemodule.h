#pragma once

namespace sbg {

struct AsepriteModuleService;

struct ComponentCreator;
struct ModelCreator;

struct AsepriteModule {
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	void setupModelCreator(ModelCreator& modelCreator) const;
	
	friend auto service_map(AsepriteModule const&) -> AsepriteModuleService;
};

} // namespace sbg
