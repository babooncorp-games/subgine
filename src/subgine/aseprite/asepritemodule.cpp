#include "asepritemodule.h"

#include "subgine/animation.h"
#include "subgine/aseprite.h"
#include "subgine/entity.h"
#include "subgine/resource.h"
#include "subgine/graphic.h"
#include "subgine/log.h"
#include "subgine/physic.h"

using namespace sbg;

void AsepriteModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("aseprite-animation", [](AnimationEngine& engine, Entity entity, Property data) {
		auto component = AsepriteAnimation{};
		auto const callback = [component = Component<AsepriteAnimation>{entity}](int frame) {
			if (!component) return;
			component->frame(frame);
		};
		
		auto frames = data["source"]["frames"];
		auto tags = data["source"]["meta"]["frameTags"];
		auto extra = data["extra"];
		auto defaultTag = data["default"];
		
		for (auto const& tag : tags) {
			auto tagName = std::string{tag["name"]};
			auto keyframes = get_aseprite_keyframes(tag, frames);
			auto modeName = get_aseprite_mode_name(tagName, extra).value_or("repeat");
			auto const animation = engine.add(
				keyframes,
				Nearest{},
				get_aseprite_mode(modeName),
				callback
			);
			
			animation.attach(entity);
			
			component.add(tagName, animation);
		}
		
		if (auto tag = value_optional<std::string>(defaultTag)) {
			component.select(*tag);
		}
		
		return component;
	});
}

void AsepriteModule::setupModelCreator(ModelCreator& modelCreator) const {
	modelCreator.add("aseprite-sprite", [](Entity entity, Property data) {
		auto sprite = AsepriteSprite{};
		auto const transform = Component<Transform>{entity};
		auto const animation = Component<AsepriteAnimation>{entity};
		auto const origin = apply_transform_from_property(data);
		
		sprite.animation = animation;
		sprite.transform = transform;
		sprite.origin = origin;
		
		if (auto const texture = value_optional<TextureTag>(data["texture"])) {
			sprite.texture = *texture;
		}
		
		return sprite;
	});
}
