#pragma once

#include "subgine/common/kangaru.h"

#include "../asepritemodule.h"

#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"

namespace sbg {

struct AsepriteModuleService : kgr::single_service<AsepriteModule>,
	autocall<
		&AsepriteModule::setupComponentCreator,
		&AsepriteModule::setupModelCreator
	> {};

} // namespace sbg
