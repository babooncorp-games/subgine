#pragma once

#include "../component/asepriteanimation.h"
#include "subgine/graphic/model/simplesprite.h"
#include "subgine/entity/component.h"
#include "subgine/graphic/utility/uniformencoderforward.h"

namespace sbg {

struct AsepriteSprite : SimpleSprite {
	Component<AsepriteAnimation> animation;
	
	static void serialize(ModelUniformEncoder& uniforms, AsepriteSprite const& sprite);
};

} // namespace sbg
