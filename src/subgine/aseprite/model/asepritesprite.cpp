#include "asepritesprite.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/utility/transformmodelmatrix.h"

using namespace sbg;
using namespace sbg::literals;

void AsepriteSprite::serialize(ModelUniformEncoder& uniforms, AsepriteSprite const& sprite) {
	uniforms
		<< uniform::resource("spritesheet"_h, sprite.texture == no_resource ? sprite.animation->texture : sprite.texture)
		<< uniform::updating("frame"_h, [](AsepriteSprite const& self) noexcept {
			return self.animation->frame();
		})
		<< uniform::updating("model"_h, TransformModelMatrix<SimpleSprite>{})
		<< uniform::value("visible"_h, sprite.visible);
}
