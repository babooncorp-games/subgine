#include "parseaseprite.h"

#include "subgine/animation.h"
#include "subgine/resource/utility/optionalvalue.h"

#include <ranges>
#include <algorithm>

namespace sbg {

auto get_aseprite_mode(std::string_view modeName) -> AnimationEngine::Mode {
	if (modeName == "clamp") {
		return Clamp{};
	} else if (modeName == "pingpong") {
		return PingPong{};
	}
	
	return Repeat{};
}

auto get_aseprite_mode_name(std::string_view tag, Property extra) -> std::optional<std::string_view> {
	auto taggedExtra = extra[tag];
	if (!taggedExtra.empty()) {
		return value_optional<std::string_view>(taggedExtra["mode"]);
	}
	return {};
}

auto get_aseprite_keyframes(Property tag, Property frames) -> std::vector<Keyframe<int>> {
	auto keyframes = std::vector<Keyframe<int>>{};
	auto const direction = std::string_view{tag["direction"]};
	auto const from = std::int32_t{tag["from"]};
	
	// We add one because aseprite has inclusive intervals
	auto const to = std::int32_t{tag["to"]} + 1;
	
	keyframes.reserve(to - from);

	// TODO: C++23 use enumerate
	auto const animation_data =
		  std::views::iota(from, to)
		| std::views::transform([frames](std::int32_t index) {
			return std::pair{index, frames[index]};
		});
	
	auto const to_keyframe = std::views::transform(
		[](std::pair<std::int32_t, Property> frame) {
			// milliseconds to seconds
			return Keyframe{frame.first, float{frame.second["duration"]} / 1000.f};
		}
	);
	
	if (direction == "reverse") {
		for (auto const keyframe : animation_data | std::views::reverse | to_keyframe) {
			keyframes.emplace_back(keyframe);
		}
	} else if (direction == "pingpong") {
		for (auto const keyframe : animation_data | to_keyframe) {
			keyframes.emplace_back(keyframe);
		}
		
		for (auto const keyframe : animation_data | std::views::reverse | std::views::drop(1) | to_keyframe) {
			keyframes.emplace_back(keyframe);
		}
	} else {
		for (auto const keyframe : animation_data | to_keyframe) {
			keyframes.emplace_back(keyframe);
		}
	}
	
	return keyframes;
}

} // namespace sbg
