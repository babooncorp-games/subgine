#pragma once

#include "subgine/animation/engine/animationengine.h"
#include "subgine/resource/property.h"

#include <string_view>
#include <vector>
#include <optional>

namespace sbg {

auto get_aseprite_mode(std::string_view modeName) -> AnimationEngine::Mode;
auto get_aseprite_mode_name(std::string_view tag, Property extra) -> std::optional<std::string_view>;
auto get_aseprite_keyframes(Property tag, Property frames) -> std::vector<Keyframe<int>>;

} // namespace sbg
