#pragma once

#include "subgine/animation/engine/animationengine.h"
#include "subgine/entity/component.h"
#include "subgine/graphic/texturetag.h"
#include "subgine/common/map.h"

#include <string>
#include <unordered_map>

namespace sbg {

/**
 * TODO: Determine if there can be no selected animation.
 */
struct AsepriteAnimation {
	void add(std::string tag, AnimationEngine::AnimationHandle animation);
	void select(std::string_view tag);
	bool at(std::string_view tag);
	
	[[nodiscard]]
	auto selected() const -> AnimationEngine::AnimationHandle;
	
	[[nodiscard]]
	auto frame() const noexcept -> int;
	void frame(int frame) noexcept;
	
	TextureTag texture;
	
private:
	int _frame;
	std::string _selectedTag;
	AnimationEngine::AnimationHandle _selectedAnimation;
	dictionary<AnimationEngine::AnimationHandle> _animations;
};

} // namespace sbg
