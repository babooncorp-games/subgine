#include "asepriteanimation.h"

#include "subgine/log.h"

using namespace sbg;

void AsepriteAnimation::add(std::string tag, AnimationEngine::AnimationHandle animation) {
	_animations.emplace(tag, animation);
}

void AsepriteAnimation::select(std::string_view tag) {
	auto animation = _animations.find(tag);
	if (animation != _animations.end()) {
		if (_selectedAnimation.valid()) {
			_selectedAnimation.stop();
		}
		
		animation->second.reset();
		animation->second.start();
		
		_selectedTag = tag;
		_selectedAnimation = animation->second;
	} else {
		Log::error(SBG_LOG_INFO, "Trying to select non-existing aseprite animation with tag", log::enquote(tag));
	}
}

auto AsepriteAnimation::selected() const -> AnimationEngine::AnimationHandle {
	return _selectedAnimation;
}

bool AsepriteAnimation::at(std::string_view tag) {
	return _selectedTag == tag;
}

auto AsepriteAnimation::frame() const noexcept -> int {
	return _frame;
}

void AsepriteAnimation::frame(int frame) noexcept {
	_frame = frame;
}
