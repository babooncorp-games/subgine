#pragma once

#include "subgine/vector/vector.h"

namespace sbg::shape {

template<dim_t n>
struct NEllipsoid {
	sbg::Vector<n, double> radius;
	sbg::Vector<n, double> position = {};
};

using Ellipse = NEllipsoid<2>;
using Ellipsoid = NEllipsoid<3>;

// TODO: Is foci defined for ellipsoids?
auto foci(Ellipse const& ellipse) noexcept -> std::pair<sbg::Vector2f, sbg::Vector2f>;

} // namespace sbg::shape
