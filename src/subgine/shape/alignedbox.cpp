#include "alignedbox.h"

namespace sbg {
namespace shape {

template<dim_t n>
auto AlignedBox<n>::infinitesimal() const noexcept -> bool {
	return top == bottom;
}

template struct AlignedBox<2>;
template struct AlignedBox<3>;

} // namespace shape
} // namespace sbg
