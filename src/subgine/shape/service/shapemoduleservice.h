#pragma once

#include "../shapemodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ShapeModuleService : kgr::single_service<ShapeModule> {};

} // namespace sbg
