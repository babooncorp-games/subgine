#pragma once

#include "subgine/vector/vector.h"
#include <utility>
#include <array>

namespace sbg::shape {

template<dim_t n>
struct Face {
	Vector<n, double> normal() const;
	
	std::array<Vector<n, double>, n> vertices;
};

extern template struct Face<2>;
extern template struct Face<3>;

using Face2D = Face<2>;
using Face3D = Face<3>;

} // namespace sbg::shape
