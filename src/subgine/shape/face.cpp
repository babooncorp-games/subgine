#include "face.h"

namespace sbg::shape {

template<dim_t n>
Vector<n, double> Face<n>::normal() const {
	if constexpr (n == 2)
	{
		auto const& [a, b] = vertices;
		return a.normal(b);
	}
	else
	{
		auto const& [a, b, c] = vertices;
		
		auto u = a - c;
		auto v = a - b;
		
		return u.normal(v);
	}
}

template struct Face<2>;
template struct Face<3>;

} // namespace sbg::shape
