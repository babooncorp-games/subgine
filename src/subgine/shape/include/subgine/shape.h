#pragma once

#include "shape/service.h"

#include "../../alignedbox.h"
#include "../../face.h"
#include "../../nellipsoid.h"
#include "../../nsphere.h"
#include "../../polytope.h"
