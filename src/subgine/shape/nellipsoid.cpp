#include "nellipsoid.h"

namespace sbg::shape {

auto foci(Ellipse const& ellipse) noexcept -> std::pair<sbg::Vector2f, sbg::Vector2f> {
	auto const f = static_cast<float>(std::sqrt(sbg::power<2>(ellipse.radius.x) - sbg::power<2>(ellipse.radius.y)));
	if (ellipse.radius.x > ellipse.radius.y) {
		auto const focus = sbg::Vector2f{f, 0};
		return {ellipse.position - focus, ellipse.position + focus};
	} else {
		auto const focus = sbg::Vector2f{0, f};
		return {ellipse.position - focus, ellipse.position + focus};
	}
}

} // namespace sbg::shape
