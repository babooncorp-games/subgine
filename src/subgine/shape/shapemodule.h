#pragma once

namespace sbg {

struct ShapeModuleService;

struct ShapeModule {
	friend auto service_map(ShapeModule const&) -> ShapeModuleService;
};

} // namespace sbg
