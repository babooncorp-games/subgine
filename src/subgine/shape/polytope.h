#pragma once

#include "subgine/vector/vector.h"

#include <vector>

namespace sbg::shape {

template<dim_t n>
struct Polytope {
	Polytope() = default;
	explicit Polytope(std::vector<Vector<n, double>> vertices, Vector<n, double> position = {}) noexcept;
	constexpr static auto dimensions = n;
	
	std::vector<Vector<n, double>> vertices;
	sbg::Vector<n, double> position = {};
	// Do not change those. Always construct a new one
	Vector<n, double> top = {}, bottom = {};
};

extern template struct Polytope<2>;
extern template struct Polytope<3>;

using Polygon = Polytope<2>;
using Polyhedron = Polytope<3>;

} // namespace sbg::shape
