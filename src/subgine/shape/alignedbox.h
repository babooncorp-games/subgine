#pragma once

#include "subgine/vector/vector.h"

namespace sbg::shape {

template<dim_t n>
struct AlignedBox {
	Vector<n, double> top, bottom;
	
	[[nodiscard]]
	auto infinitesimal() const noexcept -> bool;
	
	friend auto aligned_box_shape(AlignedBox const& box) -> AlignedBox {
		return box;
	}
};

template<dim_t n, typename T>
AlignedBox(Vector<n, T>, Vector<n, T>) -> AlignedBox<n>;

extern template struct AlignedBox<2>;
extern template struct AlignedBox<3>;

using AlignedBox2D = AlignedBox<2>;
using AlignedBox3D = AlignedBox<3>;

} // namespace sbg
