#pragma once

#include "alignedbox.h"

namespace sbg::shape {

template<dim_t n>
struct NSphere {
	double radius;
	Vector<n, double> position = {};
	
	constexpr static auto dimensions = n;
};

using Circle = NSphere<2>;
using Sphere = NSphere<3>;

} // namespace sbg::shape
