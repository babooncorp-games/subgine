#include "polytope.h"

#include "alignedbox.h"

#include "subgine/common/traits.h"

namespace sbg::shape {

template<dim_t n>
Polytope<n>::Polytope(std::vector<Vector<n, double>> vertices, sbg::Vector<n, double> positione) noexcept : vertices{std::move(vertices)}, position{position} {
	if (vertices.empty()) return;
	auto box = shape::AlignedBox{
		Vector<n, double>{std::numeric_limits<double>::infinity()},
		Vector<n, double>{std::numeric_limits<double>::infinity()} * -1
	};
	
	apply_sequence<n>([&](auto... s) {
		for (auto const& vertex : vertices) {
			box.top = sbg::Vector{std::min(get<s>(vertex), get<s>(box.top))...};
			box.bottom = sbg::Vector{std::max(get<s>(vertex), get<s>(box.bottom))...};
		}
	});
	
	top = box.top;
	bottom = box.bottom;
}

template struct Polytope<2>;
template struct Polytope<3>;

} // namespace sbg::shape
