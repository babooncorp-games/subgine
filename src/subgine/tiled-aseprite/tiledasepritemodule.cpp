#include "tiledasepritemodule.h"

#include "subgine/animation.h"
#include "subgine/aseprite.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/log.h"
#include "subgine/resource.h"
#include "subgine/tiled.h"

#include <filesystem>
#include <string>

using namespace sbg;

void TiledAsepriteModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("tiled-aseprite-animation", [](JsonManager& jsonManager, GameDatabase& database, AssetMapper assetMapper, AnimationEngine& engine, Entity entity, Property data) {
		auto component = AsepriteAnimation{};
		auto const callback = [component = Component<AsepriteAnimation>{entity}](int frame) {
			if (!component) return;
			component->frame(frame);
		};
		
		Log::error([&]{ return !entity.has<tiled::Data>(); }, SBG_LOG_INFO, "tiled::Data component needed for tiled aseprite animation component");
		auto const& tiledData = entity.component<tiled::Data>();
		Log::fatal([&]{ return !tiledData.object.has_value(); }, SBG_LOG_INFO, "tiled::Data component need to contain the object properties");
		
		auto const jsonPath = tiledData.object->properties.get<std::filesystem::path>("Aseprite Animation");
		auto const initialTag = tiledData.object->properties.get_optional<std::string>("Aseprite Initial Tag");
		
		auto const animationName = assetMapper.relative_from_name("aseprite", "map", tiledData.map.name, jsonPath);
		auto const animationData = database.get("aseprite", animationName);
		auto const animationPath = std::filesystem::path{animationData["path"]};
		
		auto const source = jsonManager.get((asset_directory() / animationPath).string());
		
		auto const texturePath = std::filesystem::path{source["meta"]["image"]};
		auto const textureName = assetMapper.relative_from_name("texture", "aseprite", animationName, texturePath);
		
		component.texture = TextureTag{textureName};
		
		// Log::fatal(SBG_LOG_INFO, "Animation path", log::enquote(animationPath));
		
		auto frames =source["frames"];
		auto tags = source["meta"]["frameTags"];
		auto extra = animationData["extra"];
		
		for (auto const tag : tags) {
			auto tagName = std::string{tag["name"]};
			auto keyframes = get_aseprite_keyframes(tag, frames);
			auto modeName = get_aseprite_mode_name(tagName, extra).value_or("repeat");
			auto const animation = engine.add(
				keyframes,
				Nearest{},
				get_aseprite_mode(modeName),
				callback
			);
			
			animation.attach(entity);
			
			component.add(tagName, animation);
		}
		
		if (initialTag) {
			component.select(*initialTag);
		}
		
		return component;
	});
}
