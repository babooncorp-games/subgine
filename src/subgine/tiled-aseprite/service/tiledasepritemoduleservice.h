#pragma once

#include "../tiledasepritemodule.h"

#include "subgine/entity/service/componentcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TiledAsepriteModuleService : kgr::single_service<TiledAsepriteModule>,
	autocall<
		&TiledAsepriteModule::setupComponentCreator
	> {};
} // namespace sbg
