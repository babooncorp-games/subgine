#pragma once

namespace sbg {

struct TiledAsepriteModuleService;
	
struct ComponentCreator;

struct TiledAsepriteModule {
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	
	friend auto service_map(TiledAsepriteModule const&) -> TiledAsepriteModuleService;
};

} // namespace sbg
