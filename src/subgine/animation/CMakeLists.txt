add_library(animation
	animationmodule.cpp
	
	engine/animationengine.cpp
	mode/clamp.cpp
	mode/pingpong.cpp
	mode/repeat.cpp
	transition/linear.cpp
	transition/nearest.cpp
	transition/sine.cpp
	keyframe.cpp
)

subgine_configure_target(animation)

target_link_libraries(animation PUBLIC
	subgine::common
	subgine::entity
	
PRIVATE
	subgine::log
	subgine::system
	subgine::common-warnings
)

add_subdirectory(test)
