#pragma once

namespace sbg {

template<typename T>
struct Keyframe {
	T value;
	float duration;
};

} // namespace sbg
