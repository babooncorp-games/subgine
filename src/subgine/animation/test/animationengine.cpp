#include "catch2/catch.hpp"

#include "subgine/animation.h"

#include <cmath>

namespace sbg {

static auto addSimpleClampAnimation(AnimationEngine& engine) -> AnimationEngine::AnimationHandle {
	auto keyframes = std::vector<Keyframe<int>>{{1, 1}, {2, 1}, {3, 1}, {4, 1}};
	auto transition = Nearest{};
	auto mode = Clamp{};
	auto callback = [n = 0](int mixedValue) mutable {
		n++;
		REQUIRE(mixedValue == n);
	};
	return engine.add(keyframes, transition, mode, callback);
}

[[maybe_unused]]
static auto addSimpleRepeatAnimation(AnimationEngine& engine) -> AnimationEngine::AnimationHandle {
	auto keyframes = std::vector<Keyframe<int>>{{1, 1}, {2, 1}, {3, 1}, {4, 1}};
	auto transition = Nearest{};
	auto mode = Repeat{};
	auto callback = [n = 0](int mixedValue) mutable {
		n = n%4+1;
		REQUIRE(mixedValue == n);
	};
	return engine.add(keyframes, transition, mode, callback);
}

TEST_CASE("AnimationEngine manages animations", "[animation]") {
	auto engine = AnimationEngine{};
	SECTION("Can add new animations") {
		auto animation = addSimpleClampAnimation(engine);
		
		REQUIRE(animation.valid());
		REQUIRE(animation.speed() == 1);
		
		SECTION("Can change the animation speed") {
			animation.speed(2);
			REQUIRE(animation.speed() == 2);
		}
		
		SECTION("Can change the animation cursor") {
			animation.cursor(0.5);
			REQUIRE(animation.cursor() == 0.5);
		}
		
		SECTION("Animation can advance through time") {
			engine.execute(Time{0.5});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
		}
		
		SECTION("Animation can advance through time at double speed") {
			animation.speed(2);
			engine.execute(Time{0.25});
			engine.execute(Time{0.5});
			engine.execute(Time{0.5});
			engine.execute(Time{0.5});
		}
		
		SECTION("Animation can be destroyed") {
			animation.destroy();
			REQUIRE(!animation.valid());
			
			SECTION("New animations cannot revalidate destroyed animations") {
				auto animation2 = addSimpleClampAnimation(engine);
				REQUIRE(!animation.valid());
				REQUIRE(animation2.valid());
			}
		}
		
		SECTION("Animations can be attached to the lifetime of an entity") {
			EntityManager entities;
			auto entity = entities.create();
			
			animation.attach(entity);
			engine.execute(Time{1});
			REQUIRE(animation.valid());
			entity.destroy();
			REQUIRE(!animation.valid());
			engine.execute(Time{1});
			REQUIRE(!animation.valid());
		}
		
		SECTION("Animations can be detached from the lifetime of an entity") {
			EntityManager entities;
			auto entity = entities.create();
			
			animation.attach(entity);
			engine.execute(Time{1});
			REQUIRE(animation.valid());
			animation.detach();
			entity.destroy();
			REQUIRE(animation.valid());
			engine.execute(Time{1});
			REQUIRE(animation.valid());
		}
	}
	
	SECTION("Can add new animations with repeat mode") {
		auto animation = addSimpleRepeatAnimation(engine);
		
		REQUIRE(animation.valid());
		REQUIRE(animation.speed() == 1);
		
		SECTION("Animation can advance through time") {
			engine.execute(Time{0.5});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
			engine.execute(Time{1});
		}
	}
}

} // namespace sbg
