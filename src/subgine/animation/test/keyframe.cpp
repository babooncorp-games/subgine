#include "catch2/catch.hpp"

#include "../keyframe.h"

namespace sbg {

TEST_CASE("KeyFrame with int", "[animation]") {
	Keyframe<int> key{1, 1.0};
	
	REQUIRE(1 == key.value);
	REQUIRE(1.0 == key.duration);
}

TEST_CASE("KeyFrame with string", "[animation]") {
	Keyframe<std::string> key{"test", 1.0};
	
	REQUIRE("test" == key.value);
	REQUIRE(1.0 == key.duration);
}

} // namespace sbg
