#pragma once

namespace sbg {

struct AnimationModuleService;

struct MainEngine;
struct AnimationEngine;

/*
 * This class is the module class for the AnimationModule.
 * It is use to setup the the class in the module and to glue this module to the others.
 */
struct AnimationModule {
	void setupMainEngine(MainEngine& mainEngine, AnimationEngine& animationEngine) const;
	
	friend auto service_map(AnimationModule const&) -> AnimationModuleService;
};

} // namespace sbg
