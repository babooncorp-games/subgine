#pragma once

namespace sbg {

struct Sine {
	auto operator()(double advancement) const noexcept -> double;
};

} // namespace sbg
