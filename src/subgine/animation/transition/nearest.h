#pragma once

namespace sbg {

struct Nearest {
	auto operator()(double advancement) const noexcept -> double;
};

} // namespace sbg
