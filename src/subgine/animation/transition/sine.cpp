#include "sine.h"

#include "subgine/common/math.h"

#include <cmath>

using namespace sbg;

auto Sine::operator()(double advancement) const noexcept -> double {
	return 0.5 * std::sin((advancement * (tau / 2) - (tau / 4))) + 0.5;
}
