#pragma once

namespace sbg {

struct Linear {
	auto operator()(double advancement) const noexcept -> double;
};

} // namespace sbg
