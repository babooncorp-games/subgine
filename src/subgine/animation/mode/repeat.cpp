#include "repeat.h"

#include <cmath>

using namespace sbg;

auto Repeat::operator()(double progression) const noexcept -> double {
	double dummy;
	return std::modf(progression, &dummy);
}
