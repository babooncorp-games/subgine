#pragma once

namespace sbg {

struct Repeat {
	auto operator()(double progression) const noexcept -> double;
};

} // namespace sbg
