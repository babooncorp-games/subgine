#include "clamp.h"

#include <algorithm>
#include <cmath>

using namespace sbg;

auto Clamp::operator()(double progression) const noexcept -> double {
	return std::min(progression, 1.);
}
