#pragma once

namespace sbg {

struct PingPong {
	auto operator()(double progression) const noexcept -> double;
};

} // namespace sbg
