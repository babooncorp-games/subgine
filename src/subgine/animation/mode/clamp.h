#pragma once

namespace sbg {

struct Clamp {
	auto operator()(double progression) const noexcept -> double;
};

} // namespace sbg
