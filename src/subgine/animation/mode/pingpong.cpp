#include "pingpong.h"

#include "subgine/log.h"

#include <cmath>

using namespace sbg;

auto PingPong::operator()(double progression) const noexcept -> double {
	double loop;
	double progress = std::modf(progression, &loop);

	if ((static_cast<int>(loop) % 2) == 0) {
		return progress;
	} else {
		return 1.0 - progress;
	}
}
