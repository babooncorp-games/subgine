#pragma once

#include "animation/service.h"

#include "../../engine/animationengine.h"

#include "../../mode/clamp.h"
#include "../../mode/pingpong.h"
#include "../../mode/repeat.h"

#include "../../transition/linear.h"
#include "../../transition/nearest.h"
#include "../../transition/sine.h"

#include "../../keyframe.h"
