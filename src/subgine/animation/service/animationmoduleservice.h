#pragma once

#include "subgine/common/kangaru.h"

#include "../animationmodule.h"
#include "animationengineservice.h"

#include "subgine/system/service/mainengineservice.h"

namespace sbg {

struct AnimationModuleService : kgr::single_service<AnimationModule>,
	autocall<
		&AnimationModule::setupMainEngine
	> {};

} // namespace sbg
