#pragma once

#include "../engine/animationengine.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct AnimationEngineService : kgr::single_service<AnimationEngine> {};

} // namespace sbg
