#include "animationmodule.h"

#include "subgine/animation.h"
#include "subgine/system.h"

using namespace sbg;

void AnimationModule::setupMainEngine(MainEngine& mainEngine, AnimationEngine& animationEngine) const {
	mainEngine.add(animationEngine);
}
