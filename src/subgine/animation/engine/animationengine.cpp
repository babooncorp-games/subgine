#include "animationengine.h"

using namespace sbg;

void AnimationEngine::owner(OwnershipToken owner) noexcept {
	_owner = std::move(owner);
}

void AnimationEngine::remove(std::size_t index, Animation& animation) {
	Log::error(
		[&]{ return !animation.animationData; },
		SBG_LOG_INFO, "Deleting an already deleted animation at index", index
	);
	
	_freeIndices.emplace_back(index);
	animation.animationData.reset();
	animation.mode = {};
	animation.transition = {};
	_generations[index]++;
}

void AnimationEngine::execute(Time time) {
	// TODO: C++23: Use range enumerate
	for (auto i = std::size_t{0} ; i < _animations.size() ; ++i) {
		auto& animation = _animations[i];
		if (animation.animationData && animation.entity && !animation.entity->alive()) {
			remove(i, animation);
			continue;
		}
		
		if (animation.animationData && animation.running) {
			auto const alteredDeltaTime = time.current * animation.speed;
			auto const animationTotalDuration = animation.totalDuration.count();
			animation.runningTime += alteredDeltaTime / animationTotalDuration;
			animation.cursor = advance(animation);
			transit(animation);
		}
	}
}

auto AnimationEngine::advance(Animation const& animation) -> double {
	return animation.mode(animation.runningTime);
}

void AnimationEngine::transit(Animation& animation) {
	auto const animationTotalDuration = animation.totalDuration.count();
	auto [keyframeDuration, absoluteTime] = animation.animationData->closest_keyframe(animation.cursor * animationTotalDuration);
	auto mixPercent = animation.transition((animation.cursor * animationTotalDuration - absoluteTime) / keyframeDuration);
	animation.animationData->transit_value(mixPercent, animation.cursor * animationTotalDuration);
}

AnimationEngine::AnimationHandle::AnimationHandle(std::size_t index, AnimationEngine& engine) noexcept :
	_index{index}, _engine{&engine}, _generation{engine._generations[index]} {}

auto AnimationEngine::AnimationHandle::animation() const noexcept -> Animation& {
	return _engine->_animations[_index];
}

void AnimationEngine::AnimationHandle::start() const noexcept {
	animation().running = true;
}

void AnimationEngine::AnimationHandle::pause() const noexcept {
	animation().running = false;
}

void AnimationEngine::AnimationHandle::reset() const noexcept {
	cursor(0);
	animation().runningTime = 0.0;
}

void AnimationEngine::AnimationHandle::stop() const noexcept {
	pause();
	reset();
}

void AnimationEngine::AnimationHandle::destroy() const noexcept {
	_engine->remove(_index, animation());
}

bool AnimationEngine::AnimationHandle::valid() const noexcept {
	return _engine != nullptr && _generation == _engine->_generations[_index] && (!animation().entity || animation().entity->alive());
}

void AnimationEngine::AnimationHandle::cursor(double cursor) const {
	auto& anim = animation();
	anim.cursor = cursor;
	anim.runningTime = anim.totalDuration.count() * cursor;
}

void AnimationEngine::AnimationHandle::speed(double speed) const {
	animation().speed = speed;
}

auto AnimationEngine::AnimationHandle::duration() const -> std::chrono::duration<double> {
	return animation().totalDuration;
}

void AnimationEngine::AnimationHandle::mode(Mode mode) const {
	animation().mode = mode;
}

void AnimationEngine::AnimationHandle::transition(Transition transition) const {
	animation().transition = transition;
}

double AnimationEngine::AnimationHandle::cursor() const {
	return animation().cursor;
}

double AnimationEngine::AnimationHandle::speed() const {
	return animation().speed;
}

auto AnimationEngine::AnimationHandle::running() const -> bool {
	return animation().running;
}

void AnimationEngine::AnimationHandle::attach(Entity entity) const noexcept {
	animation().entity = entity;
}

void AnimationEngine::AnimationHandle::detach() const noexcept {
	animation().entity = {};
}
