#pragma once

#include "../keyframe.h"

#include "subgine/system/time.h"
#include "subgine/common/math.h"
#include "subgine/entity/entity.h"
#include "subgine/common/ownershiptoken.h"

#include <chrono>
#include <deque>
#include <functional>
#include <algorithm>
#include <memory>
#include <vector>
#include <optional>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <concepts>

namespace sbg {

struct AnimationEngineService;

struct AnimationEngine {
private:
	struct AnimationBase;
	struct Animation;
	
public:
	using Transition = std::function<double(double)>;
	using Mode = std::function<double(double)>;
	
	struct Autostart {} static constexpr autostart{};
	
	struct AnimationHandle {
		AnimationHandle() = default;
		AnimationHandle(std::size_t index, AnimationEngine& engine) noexcept;
		
		void start() const noexcept;
		void pause() const noexcept;
		void stop() const noexcept;
		void reset() const noexcept;
		
		void destroy() const noexcept;
		bool valid() const noexcept;
		
		void mode(Mode mode) const;
		void transition(Transition transition) const;
		void speed(double speed) const;
		void cursor(double cursor) const;
		void attach(Entity entity) const noexcept;
		void detach() const noexcept;
		
		auto speed() const -> double;
		auto cursor() const -> double;
		auto running() const -> bool;
		auto duration() const -> std::chrono::duration<double>;
		
	private:
		auto animation() const noexcept -> Animation&;
		
		std::uint32_t _generation;
		std::size_t _index = 0;
		AnimationEngine* _engine = nullptr;
	};
	
	void execute(Time time);
	void owner(OwnershipToken owner) noexcept;
	
	/**
	 * @param keyframes All the keyframes of the animation, except the last one.
	 * @param transition The transition to be used by the animation.
	 * @param mode The mode to be used by the animation.
	 * @param callback Operation(s) to be done with the value of the animation when it advances.
	 */
	template<typename T, typename C>
	auto add(std::vector<Keyframe<T>> keyframes, Transition transition, Mode mode, C callback, std::optional<Autostart> autostart = {}) -> AnimationHandle {
		assert(!keyframes.empty());
		auto positionedKeyframes = std::vector<PositionedKeyframe<T>>{};
		auto totalDuration = double{};
		
		for (auto const& keyframe : keyframes) {
			positionedKeyframes.emplace_back(PositionedKeyframe{keyframe, totalDuration});
			totalDuration += keyframe.duration;
		}
		
		auto animation = Animation{
			std::make_unique<AnimationData<T>>(positionedKeyframes, callback),
			std::chrono::duration<double>{totalDuration},
			std::nullopt, autostart.has_value(), 1, 0, transition, mode
		};
		
		if (_freeIndices.empty()) {
			_generations.emplace_back(0);
			_animations.emplace_back(std::move(animation));
			return AnimationHandle{_animations.size()-1, *this};
		} else {
			auto index = _freeIndices.front();
			_animations[index] = std::move(animation);
			_freeIndices.pop_front();
			return AnimationHandle{index, *this};
		}
	}
	
private:
	template<typename T>
	struct PositionedKeyframe {
		Keyframe<T> keyframe;
		double absolutePosition;
	};
	
	struct AnimationBase {
		virtual ~AnimationBase() = default;
		virtual auto closest_keyframe(double cursor) -> std::pair<double, double> = 0;
		virtual void transit_value(double mix, double cursor) = 0;
	};
	
	/**
	 * @brief Animation data that contains values of type T.
	 */
	template<typename T>
	struct AnimationData : AnimationBase {
		AnimationData(std::vector<PositionedKeyframe<T>> _keyframes, std::function<void(T)> _callback) noexcept :
			keyframes{std::move(_keyframes)}, callback{std::move(_callback)} {}
		
		auto keyframe_before(double cursor) {
			return std::adjacent_find(
				keyframes.begin(), keyframes.end(),
				[&](auto const& keyframe1, auto const& keyframe2) {
					return keyframe2.absolutePosition > cursor;
				}
			);
		}
		
		/**
		 * @param cursor the cursor in real time (total duration * advancement percent).
		 */
		auto closest_keyframe(double cursor) -> std::pair<double, double> override {
			auto const it = keyframe_before(cursor);
			
			if (it == keyframes.end()) {
				auto const& last = keyframes.back();
				return {last.keyframe.duration, last.absolutePosition};
			} else {
				return {it->keyframe.duration, it->absolutePosition};
			}
		}
		
		/**  
		 * @param mix The amount to take from the keyframe after (in percent)
		 * @param cursor the cursor in real time (total duration * advancement percent).
		 */
		void transit_value(double mix, double cursor) override {
			auto const it = keyframe_before(cursor);
			
			if (it == keyframes.end()) {
				auto const& last = keyframes.back();
				callback(last.keyframe.value);
			} else {
				using std::lerp;
				if constexpr (std::floating_point<T>) {
					callback(lerp(it->keyframe.value, std::next(it)->keyframe.value, static_cast<T>(mix)));
				} else {
					callback(static_cast<T>(lerp(it->keyframe.value, std::next(it)->keyframe.value, mix)));
				}
			}
		}
		
		std::vector<PositionedKeyframe<T>> keyframes;
		std::function<void(T)> callback;
	};
	
	struct Animation {
		std::unique_ptr<AnimationBase> animationData;
		std::chrono::duration<double> totalDuration;
		std::optional<Entity> entity;
		bool running = false;
		double speed = 1.;
		double cursor;
		Transition transition;
		Mode mode;
		double runningTime = 0.;
	};
	
	[[nodiscard]]
	auto advance(Animation const& animation) -> double;
	void transit(Animation& animation);
	void remove(std::size_t index, Animation& animation);
	
	std::vector<Animation> _animations;
	std::vector<std::uint32_t> _generations;
	std::deque<std::size_t> _freeIndices;
	OwnershipToken _owner;
	
	friend auto service_map(AnimationEngine const&) -> AnimationEngineService;
};

} // namespace sbg
