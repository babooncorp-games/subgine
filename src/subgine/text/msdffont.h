#pragma once

#include "font.h"

#include "subgine/resource/resourcehandle.h"
#include "subgine/resource/ownedresource.h"

#include <optional>

namespace sbg {

struct MsdfFont {
	MsdfFont() = default;
	explicit MsdfFont(ResourceHandle<Font> font) noexcept;
	
	[[nodiscard]]
	auto load_glyph(char32_t code) const -> std::optional<std::vector<float>>;
	
	ResourceHandle<Font> font;
	
	sbg::Vector2d offset = {};
	sbg::Vector2i size = {};
	double range = 0;
	double padding = 0;
};

} // namespace sbg
