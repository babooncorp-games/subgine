#pragma once

#include "../fonttag.h"
#include "../font.h"

#include "subgine/common/memberreference.h"
#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg {

struct FontLoaderService;
struct FontTagType;

/**
 * @brief A loader for fonts.
 */
struct FontLoader {
	FontLoader(FreeType const& freetype) noexcept;
	
	/**
	 * Loads a font from the json data.
	 * 
	 * @param data the font data, including the path to the ttf file.
	 */
	auto create(Property data) const -> Font;
	
	static constexpr auto path = "font";
	
private:
	MemberReference<FreeType const> _freetype;
	
	friend auto service_map(FontLoader const&) -> FontLoaderService;
};

using FontFactory = ResourceFactory<FontLoader>;
using FontManager = ResourceManager<FontFactory>;

} // namespace sbg
