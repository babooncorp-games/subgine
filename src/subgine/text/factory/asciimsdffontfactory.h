#pragma once

#include "msdffontfactory.h"
#include "../asciimsdffont.h"

#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg {

struct AsciiMsdfFontLoaderService;

struct AsciiMsdfFontLoader {
	explicit AsciiMsdfFontLoader(MsdfFontLoader fonts) noexcept;
	
	auto create(Property data) const -> AsciiMsdfFont;
	static constexpr auto path = "ascii-msdf-font";
	
private:
	MsdfFontLoader _fonts;
	
	friend auto service_map(AsciiMsdfFontLoader const&) -> AsciiMsdfFontLoaderService;
};

using AsciiMsdfFontFactory = ResourceFactory<AsciiMsdfFontLoader>;
using AsciiMsdfFontManager = ResourceManager<AsciiMsdfFontFactory>;

} // namespace sbg
