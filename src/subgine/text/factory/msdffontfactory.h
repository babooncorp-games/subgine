#pragma once

#include "../fonttag.h"
#include "../msdffont.h"
#include "fontfactory.h"

#include "subgine/common/memberreference.h"
#include "subgine/resource/property.h"
#include "subgine/resource/resourcefactory.h"
#include "subgine/resource/resourcemanager.h"

namespace sbg {

struct MsdfFontLoaderService;

/**
 * @brief A loader for msdf fonts.
 */
struct MsdfFontLoader {
	MsdfFontLoader(FontManager fonts) noexcept;
	
	/**
	 * Loads a msdf capable font from the json data.
	 * 
	 * @param data the msdf data, including which font it uses.
	 */
	auto create(Property data) const -> MsdfFont;
	
	static constexpr auto path = "msdf-font";
	
private:
	FontManager _fonts;
	
	friend auto service_map(MsdfFontLoader const&) -> MsdfFontLoaderService;
};

using MsdfFontFactory = ResourceFactory<MsdfFontLoader>;
using MsdfFontManager = ResourceManager<MsdfFontFactory>;

} // namespace sbg
