#include "msdffontfactory.h"

using namespace sbg;

MsdfFontLoader::MsdfFontLoader(FontManager fonts) noexcept : _fonts{fonts} {}

auto MsdfFontLoader::create(Property data) const -> MsdfFont {
	auto font = MsdfFont{_fonts.get(data["font"])};
	
	font.offset = sbg::Vector2d{data["offset"]};
	font.size = sbg::Vector2i{data["size"]};
	font.padding = double{data["padding"]};
	font.range = double{data["range"]};
	
	return font;
}
