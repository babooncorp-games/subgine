#include "fontfactory.h"

#include "subgine/resource/utility/assetdirectory.h"

#include <filesystem>

using namespace sbg;

FontLoader::FontLoader(FreeType const& freetype) noexcept : _freetype{freetype} {}

auto FontLoader::create(Property data) const -> Font {
	auto font = Font{};
	
	font.from(*_freetype, asset_directory() / std::filesystem::path{data["path"]});
	
	return font;
}
