#include "asciimsdffontfactory.h"

using namespace sbg;

AsciiMsdfFontLoader::AsciiMsdfFontLoader(MsdfFontLoader fonts) noexcept : _fonts{std::move(fonts)} {}

auto AsciiMsdfFontLoader::create(Property data) const -> AsciiMsdfFont {
	auto font = AsciiMsdfFont{_fonts.create(data)};
	
	font.load_texture();
	
	return font;
}
