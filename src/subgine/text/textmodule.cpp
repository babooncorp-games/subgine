#include "textmodule.h"

#include "subgine/log.h"

#include "subgine/text.h"

#include "subgine/graphic.h"
#include "subgine/opengl.h"
#include "subgine/log.h"
#include "subgine/window.h"

#include "msdfgen/msdfgen.h"
#include "msdfgen/msdfgen-ext.h"

using namespace sbg;

void TextModule::setupPainterCreator(PainterCreator& painterCreator) const {
	painterCreator.add("text-painter", [](
		kgr::generator<TextPainterService> build,
		ProgramManager programManager,
		InstancedArrayMeshFactory meshFactory,
		Property data
	) {
		return build(programManager.get(data["program"]),
		[mesh = std::string{data["mesh"]}, meshFactory] {
			return meshFactory.create(mesh);
		});
	});
}

void TextModule::setupComputedUniformCreator(ComputedUniformCreator& computedUniformCreator) const {
	using namespace sbg::literals;
	computedUniformCreator.add("msdf-scale-factor", [](Window& window, AsciiMsdfFontManager fonts) {
		return [&window, fonts](ComputedUniformEncoder& encoder, ModelUniformContext /* unused */) {
			encoder
				<< uniform::updating("scaleFactor"_h, [](ModelUniformContext const& context) noexcept {
					auto const cameraProjection = context.environment.required<glm::mat4>("projection"_h);
					auto const cameraView = context.environment.required<glm::mat4>("view"_h);
					auto const modelMatrix = context.model.required<glm::mat4>("model"_h);
					
					auto const viewport = context.environment.required<Vector2i>("viewport"_h);
					auto const msdfSize = context.model.required<Vector2i>("msdfSize"_h);
					
					auto const transform = cameraProjection * cameraView * modelMatrix;
					auto const scaleFactor = transform[0][0] * viewport.x * 0.5f / msdfSize.x;
					
					return scaleFactor;
				});
		};
	});
}
