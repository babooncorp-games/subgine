#include "asciimsdffont.h"

#include "msdffonttexture.h"

using namespace sbg;

AsciiMsdfFont::AsciiMsdfFont(MsdfFont font) noexcept : _font{std::move(font)} {}

void AsciiMsdfFont::load_texture() & {
	_texture = msdf_fixed_ascii(_font).release();
}

void AsciiMsdfFont::free() & noexcept {
	_texture.free();
}

auto AsciiMsdfFont::texture() const& noexcept -> Texture {
	return _texture;
}

auto AsciiMsdfFont::msdf() const& noexcept -> MsdfFont const& {
	return _font;
}
