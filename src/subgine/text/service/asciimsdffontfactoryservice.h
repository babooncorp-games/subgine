#pragma once

#include "../factory/asciimsdffontfactory.h"
#include "freetypeservice.h"

#include "msdffontfactoryservice.h"
#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct AsciiMsdfFontLoaderService : kgr::service<AsciiMsdfFontLoader, kgr::autowire> {};

using AsciiMsdfFontFactoryService = ResourceFactoryService<AsciiMsdfFontLoader>;
using AsciiMsdfFontManagerService = ResourceManagerService<AsciiMsdfFontManager>;

} // namespace sbg
