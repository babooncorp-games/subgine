#pragma once

#include "../painter/textpainter.h"

#include "asciimsdffontfactoryservice.h"
#include "subgine/opengl/service/texturefactoryservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TextPainterService : kgr::service<TextPainter, kgr::autowire> {};

} // namespace sbg
