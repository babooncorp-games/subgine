#pragma once

#include "../textmodule.h"
#include "subgine/graphic/service/paintercreatorservice.h"
#include "subgine/graphic/service/computeduniformcreatorservice.h"

#include "freetypeservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct TextModuleService : kgr::single_service<TextModule>,
	autocall<
		&TextModule::setupFreeType,
		&TextModule::setupPainterCreator,
		&TextModule::setupComputedUniformCreator
	> {};

} // namespace sbg

