#pragma once

#include "../factory/fontfactory.h"
#include "freetypeservice.h"

#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct FontLoaderService : kgr::service<FontLoader, kgr::autowire> {};

using FontFactoryService = ResourceFactoryService<FontLoader>;
using FontManagerService = ResourceManagerService<FontManager>;

} // namespace sbg
