#pragma once

#include "../factory/msdffontfactory.h"
#include "freetypeservice.h"

#include "fontfactoryservice.h"
#include "subgine/resource/service/resourcefactoryservice.h"
#include "subgine/resource/service/resourcemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct MsdfFontLoaderService : kgr::service<MsdfFontLoader, kgr::autowire> {};

using MsdfFontFactoryService = ResourceFactoryService<MsdfFontLoader>;
using MsdfFontManagerService = ResourceManagerService<MsdfFontManager>;

} // namespace sbg
