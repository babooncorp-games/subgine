#pragma once

#include "subgine/resource/resourcetag.h"

namespace sbg {
	using FontTag = ResourceTag<struct FontTagType>;
}
