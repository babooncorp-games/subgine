#pragma once

#include "subgine/vector/vector.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace sbg {

template<typename T>
struct MsdfTextModelMatrix {
	auto operator()(T const& model) const noexcept -> glm::mat4 {
		auto const transform = model.transform
			->scale3d(model.transform->scale3d() * sbg::Vector3f{model.size, model.size, 1});
		
		auto const msdf_padding = model.msdf->padding;
		auto const msdf_offset = model.msdf->offset;
		auto const msdf_size = model.msdf->size;
		
		auto const area = (msdf_size.y - msdf_padding) / msdf_size.y;
		
		auto const alignMatrix = [&]{
			switch(model.verticalAlign) {
				case RectVerticalAlign::top: return glm::translate(glm::mat4{1.f}, area * glm::vec3{0.5, -0.5, 0});
				case RectVerticalAlign::bottom: return glm::translate(glm::mat4{1.f}, area * glm::vec3{0.5, 0.5, 0});
				case RectVerticalAlign::middle: return glm::mat4{1.f};
				case RectVerticalAlign::baseline: return glm::translate(glm::mat4{1.f},
						glm::vec3{0.5, 0.5, 0}
					+ glm::vec3{msdf_offset.x, -msdf_offset.y, 0} / (32.f / area) // what is 32.f doing here?
				);
				default: return glm::mat4{1.f};
			}
		}();
		
		if constexpr (requires{ model.origin; }) {
			return
				  transform.matrix()
				* glm::scale(glm::vec3{1.f, -1.f, 1.f})
				* model.origin.matrix()
				* alignMatrix;
		} else {
			return
				  transform.matrix()
				* glm::scale(glm::vec3{1.f, -1.f, 1.f})
				* alignMatrix;
		}
	}
};

}
