#pragma once

namespace sbg {
	enum struct RectVerticalAlign : std::uint8_t { baseline, top, bottom, middle };
	enum struct TextHorizontalAlign : std::uint8_t { left = 0, right = 1, center = 2 };
}
