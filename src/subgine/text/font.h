#pragma once

#include "glyph.h"
#include "freetype.h"

#include <freetype/freetype.h>
#include <filesystem>

namespace sbg {

struct Font {
	Font() = default;
	
	auto glyph(char32_t id) const noexcept -> Glyph;
	void from(FreeType const& freetype, std::filesystem::path const& file);
	void free() noexcept;
	auto kerning(char32_t char1, char32_t char2) noexcept -> std::int32_t;
	
	[[nodiscard]]
	auto handle() const noexcept -> FT_Face;
	
private:
	FT_Face _face = {};
};

} // namespace sbg
