#pragma once

#include "msdffont.h"
#include "subgine/opengl/texture.h"

namespace sbg {

struct AsciiMsdfFont {
	AsciiMsdfFont() = default;
	explicit AsciiMsdfFont(MsdfFont font) noexcept;
	
	void load_texture() &;
	void free() & noexcept;
	
	[[nodiscard]]
	auto texture() const& noexcept -> Texture;
	
	[[nodiscard]]
	auto msdf() const& noexcept -> MsdfFont const&;
	
private:
	MsdfFont _font;
	Texture _texture;
};

} // namespace sbg
