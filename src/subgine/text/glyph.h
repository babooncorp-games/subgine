#pragma once

#include "subgine/vector/vector.h"

#include <string>

namespace sbg {

struct Glyph {
	struct Metric {
		Vector2i bearing;
		Vector2i size;
		std::int32_t advance;
	} metrics;
	char32_t id;
};

} // namespace sbg
