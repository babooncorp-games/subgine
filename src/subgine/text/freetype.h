#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include "subgine/common/kangaru.h"

namespace sbg {

struct FreeType {
	FreeType();
	~FreeType();
	
	FreeType(FreeType const&) = delete;
	FreeType(FreeType&&) = delete;
	auto operator=(FreeType const&) -> FreeType& = delete;
	auto operator=(FreeType&&) -> FreeType& = delete;
	
	auto instance() const noexcept -> FT_Library;
	
private:
	FT_Library _instance{};
	
	friend auto service_map(FreeType const&) -> kgr::autowire_single;
};

} // namespace sbg
