#pragma once

namespace sbg {

struct PainterCreator;
struct ComputedUniformCreator;
struct TextModuleService;
struct FreeType;

struct TextModule {
	inline void setupFreeType(FreeType const&) const {}
	void setupPainterCreator(PainterCreator& painterCreator) const;
	void setupComputedUniformCreator(ComputedUniformCreator& computedUniformCreator) const;
	
	friend auto service_map(TextModule const&) -> TextModuleService;
};

} // namespace sbg
