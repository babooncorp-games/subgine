#pragma once

#include "subgine/vector/vector.h"
#include "subgine/resource/resourcehandle.h"
#include "subgine/graphic/utility/uniformencoderforward.h"
#include "subgine/graphic/component/transform.h"
#include "subgine/entity/component.h"

#include "../utility/rectverticalalign.h"
#include "../fonttag.h"
#include "../msdffont.h"

#include <string>

namespace sbg {

struct TextModel {
	std::u8string text;
	sbg::Component<sbg::Transform> transform;
	float size;
	FontTag font;
	ResourceHandle<MsdfFont> msdf;
	Vector4f color = {1, 1, 1, 1};
	Vector4f background = {0, 0, 0, 0};
	Vector4f borderColor = {0, 0, 0, 0};
	float borderThickness = 0;
	RectVerticalAlign verticalAlign = RectVerticalAlign::baseline;
	TextHorizontalAlign align = TextHorizontalAlign::left;
	Transform origin = {};
	bool visible = true;
	
	static void serialize(ModelUniformEncoder& uniforms, TextModel const& model);
};

} // namespace sbg
