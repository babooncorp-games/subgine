#include "textmodel.h"

#include "../utility/msdftextmodelmatrix.h"
#include "subgine/graphic/uniform.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace sbg;
using namespace sbg::literals;

void TextModel::serialize(ModelUniformEncoder& uniforms, TextModel const& model) {
	uniforms
		<< uniform::resource("font"_h, model.font)
		<< uniform::updating("size"_h, model.size)
		<< uniform::updating("background"_h, model.background)
		<< uniform::updating("color"_h, model.color)
		<< uniform::updating("borderColor"_h, model.borderColor)
		<< uniform::updating("borderThickness"_h, model.borderThickness)
		<< uniform::updating("align"_h, [](TextModel const& model) noexcept {
			return static_cast<int>(model.align);
		})
		<< uniform::updating("model"_h, MsdfTextModelMatrix<TextModel>{})
		<< uniform::value("range"_h, static_cast<float>(model.msdf->range))
		<< uniform::value("msdfSize"_h, model.msdf->size)
		<< uniform::value("visible"_h, model.visible)
		<< uniform::updating("text"_h, [](TextModel const& model) noexcept {
			return std::any{model.text};
		});
}
