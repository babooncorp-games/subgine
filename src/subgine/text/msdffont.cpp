#include "msdffont.h"

#include "msdfgen/msdfgen.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

using namespace sbg;

template<typename T> [[nodiscard]]
static constexpr auto f26dot6_to_double(T value) noexcept -> double {
	return static_cast<double>(value) / 64.;
}

struct FtContext {
	msdfgen::Point2 position;
	msdfgen::Shape* shape;
	msdfgen::Contour*contour;
};

static auto ftPoint2(FT_Vector const& vector) {
	return msdfgen::Point2{f26dot6_to_double(vector.x), f26dot6_to_double(vector.y)};
}

static auto ftMoveTo(FT_Vector const* to, void* user) -> int {
	auto const context = static_cast<FtContext*>(user);
	
	if (!(context->contour && context->contour->edges.empty())) {
		context->contour = &context->shape->addContour();
	}
	 
	context->position = ftPoint2(*to);
	
	return 0;
}

static auto ftLineTo(FT_Vector const* to, void* user) -> int {
	auto const context = static_cast<FtContext*>(user);
	auto const endpoint = ftPoint2(*to);
	
	if (endpoint != context->position) {
		context->contour->addEdge(new msdfgen::LinearSegment{context->position, endpoint});
		context->position = endpoint;
	}
	
	return 0;
}

static auto ftConicTo(FT_Vector const* control, FT_Vector const* to, void* user) -> int {
	auto const context = static_cast<FtContext*>(user);
	
	context->contour->addEdge(new msdfgen::QuadraticSegment{context->position, ftPoint2(*control), ftPoint2(*to)});
	context->position = ftPoint2(*to);
	
	return 0;
}

static auto ftCubicTo(FT_Vector const* control1, FT_Vector const* control2, FT_Vector const* to, void* user) -> int {
	auto const context = static_cast<FtContext*>(user);
	
	context->contour->addEdge(new msdfgen::CubicSegment{context->position, ftPoint2(*control1), ftPoint2(*control2), ftPoint2(*to)});
	context->position = ftPoint2(*to);
	
	return 0;
}

static auto loadGlyph(msdfgen::Shape& output, Font const& font, char32_t code) -> bool {
	if (FT_Load_Char(font.handle(), code, FT_LOAD_DEFAULT | FT_LOAD_NO_SCALE)) return false;
	
	output.contours.clear();
	output.inverseYAxis = false;
	
	auto context = FtContext{};
	context.shape = &output;
	FT_Outline_Funcs ftFunctions;
	ftFunctions.move_to = ftMoveTo;
	ftFunctions.line_to = ftLineTo;
	ftFunctions.conic_to = ftConicTo;
	ftFunctions.cubic_to = ftCubicTo;
	ftFunctions.shift = 0;
	ftFunctions.delta = 0;
	
	if (FT_Outline_Decompose(&font.handle()->glyph->outline, &ftFunctions, &context)) return false;
	
	if (!output.contours.empty() && output.contours.back().edges.empty()) {
		output.contours.pop_back();
	}
	
	return true;
}

MsdfFont::MsdfFont(ResourceHandle<Font> font) noexcept : font{font} {}

auto MsdfFont::load_glyph(char32_t code) const -> std::optional<std::vector<float>> {
	auto const [width, height] = size;
	auto pixels = std::vector<float>{};
	auto shape = msdfgen::Shape{};	
	
	if (!loadGlyph(shape, *font, code)) return {};
	pixels.resize(width * height * 3);
	shape.normalize();

	if (shape.contours.empty()) return pixels;
	
	msdfgen::edgeColoringSimple(shape, 3.0);
	msdfgen::generateMSDF(msdfgen::BitmapRef<float, 3>{pixels.data(), width, height}, shape, range, (height - padding) / 32.f, msdfgen::Vector2{offset.x, offset.y});
	
	return pixels;
}
