#include "msdffonttexture.h"

#include "glbinding/gl/enum.h"

using namespace sbg;

auto sbg::msdf_fixed_ascii(MsdfFont const& font) -> OwnedResource<Texture> {
	auto texture = OwnedResource{Texture{gl::GL_TEXTURE_2D_ARRAY}};
	texture->create();
	texture->image(TextureFormat3D{0, gl::GL_RGB, Vector3i{font.size.x, font.size.y, 128}, gl::GL_RGB, gl::GL_FLOAT}, nullptr);
	
	for (auto glyph = char32_t{}; glyph < 128 ; ++glyph) {
		if (auto const pixels = font.load_glyph(glyph)) {
			auto const format = SubImageFormat3D{0, {font.size.x, font.size.y, 1}, gl::GL_RGB, gl::GL_FLOAT};
			texture->subimage(format, Vector3i{0, 0, static_cast<int>(glyph)}, pixels->data());
		}
	}
	
	texture->parameter(gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_BORDER);
	texture->parameter(gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_BORDER);
	texture->parameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
	texture->parameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
	texture->parameter(gl::GL_TEXTURE_BASE_LEVEL, 0);
	texture->parameter(gl::GL_TEXTURE_MAX_LEVEL, 0);
	texture->parameter(gl::GL_TEXTURE_BORDER_COLOR, Vector4f{0, 0, 0, 0});

	return texture;
}
