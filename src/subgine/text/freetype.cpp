#include "freetype.h"

#include "subgine/log.h"

using namespace sbg;

FreeType::FreeType() {
	if (FT_Init_FreeType(&_instance)) {
		Log::fatal(SBG_LOG_INFO, "Cannot initialize freetype");
	}
}

FreeType::~FreeType() {
	FT_Done_FreeType(_instance);
}

auto FreeType::instance() const noexcept -> FT_Library {
	return _instance;
}
