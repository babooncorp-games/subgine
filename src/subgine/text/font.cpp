#include "font.h"

#include "subgine/log.h"

using namespace sbg;

auto Font::glyph(char32_t id) const noexcept -> Glyph {
	FT_Load_Char(_face, id, FT_LOAD_DEFAULT | FT_LOAD_NO_SCALE);
	return Glyph{
		Glyph::Metric{
			Vector{_face->glyph->metrics.horiBearingX, _face->glyph->metrics.horiBearingY},
			Vector{_face->glyph->metrics.width, _face->glyph->metrics.height},
			static_cast<std::int32_t>(_face->glyph->metrics.horiAdvance)
		},
		id
	};
}

void Font::from(FreeType const& freetype, std::filesystem::path const& file) {
	auto const error = FT_New_Face(freetype.instance(), file.string().c_str(), 0, &_face);
	
	if (error) {
		Log::fatal(SBG_LOG_INFO, "Cannot load font face at path", file);
	}
}

void Font::free() noexcept {
	FT_Done_Face(_face);
}

auto Font::handle() const noexcept -> FT_Face {
	return _face;
}
