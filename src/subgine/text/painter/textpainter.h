#pragma once

#include "../factory/fontfactory.h"
#include "../factory/asciimsdffontfactory.h"
#include "../msdffont.h"

#include "subgine/resource/ownedresource.h"
#include "subgine/resource/resourcehandle.h"
#include "subgine/graphic/computeduniforms.h"
#include "subgine/graphic/dynstructbuffer.h"
#include "subgine/graphic/modelslotmap.h"
#include "subgine/opengl/factory/texturefactory.h"
#include "subgine/opengl/texture.h"
#include "subgine/opengl/program.h"
#include "subgine/opengl/instancedarraymesh.h"

#include <string>
#include <vector>

namespace sbg {

struct TextPainterService;

struct TextPainter {
	using InstancedArrayMeshMaker = std::function<OwnedResource<InstancedArrayMesh>()>;
	
	TextPainter(AsciiMsdfFontManager fonts, TextureManager textures, ResourceHandle<Program> program, InstancedArrayMeshMaker createArrayMesh) noexcept;
	
	void add(ModelInfo model, UniformSet const& environment);
	void draw() const;
	void prepare(UniformSet const& context);
	void update(ModelInfo model, UniformSet const& environment);
	void refresh(UniformSet const& environment);
	void remove(ModelInfo const& model);
	
private:
	struct OwnerData {
		OwnedResource<InstancedArrayMesh> mesh;
		OwnedResource<BufferObject> buffer;
		ResourceHandle<AsciiMsdfFont> font;
		std::vector<ResourceHandle<Texture>> textures = {};
		std::u8string last_text;
		float last_advance = 0;
	};
	
	struct DrawData {
		InstancedArrayMeshView mesh;
		Texture msdf;
		std::vector<Texture> textures = {};
	};
	
	struct GlyphData {
		int code_index;
		float advance;
	};
	
	static auto fill_glyph_buffer(std::vector<GlyphData>& glyphBuffer, Font const& font, std::u8string text) -> float;
	
	void insert_call(ModelInfo model, UniformSet const& environment);
	void remove_call(ModelInfo const& model, std::size_t index) noexcept;
	
	TextureManager _textures;
	AsciiMsdfFontManager _fonts;
	ModelSlotMap _entityModel;
	ResourceHandle<Program> _program;
	InstancedArrayMeshMaker _createArrayMesh;
	DynStructBuffer _drawCalls;
	std::vector<OwnerData> _ownerData;
	std::vector<DrawData> _drawData;
	std::vector<GlyphData> _glyphBuffer;
	
	friend auto service_map(TextPainter const&) -> TextPainterService;
};

} // namespace sbg
