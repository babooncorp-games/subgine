#include "textpainter.h"

#include "../fonttag.h"
#include "../msdffonttexture.h"

#include "subgine/graphic/texturetag.h"
#include "subgine/opengl/utility/membersofprogram.h"
#include "subgine/graphic/utility/resourceuniformhelper.h"
#include "subgine/graphic/utility/uniformhelper.h"
#include "subgine/resource/utility/resourceunwrap.h"

using namespace sbg;
using namespace sbg::literals;

auto TextPainter::fill_glyph_buffer(std::vector<GlyphData>& glyphBuffer, Font const& font, std::u8string text) -> float {
	glyphBuffer.clear();
	
	auto advance = float{0.f};
	for (auto code_point : text) {
		auto const glyph = font.glyph(code_point);
		glyphBuffer.emplace_back(GlyphData{code_point, advance});
		advance += glyph.metrics.advance / 32.f / 64.f;
	}
	
	return advance;
}

TextPainter::TextPainter(
	AsciiMsdfFontManager fonts,
	TextureManager textures,
	ResourceHandle<Program> program,
	InstancedArrayMeshMaker createArrayMesh
) noexcept :
	_textures{textures},
	_fonts{fonts},
	_program{std::move(program)},
	_createArrayMesh{std::move(createArrayMesh)},
	_drawCalls{members_of_program(*_program)}
{
	_glyphBuffer.reserve(255);
}

void TextPainter::add(ModelInfo model, UniformSet const& environment) {
	auto const visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	
	if (visible) {
		insert_call(std::move(model), environment);
	}
}

void TextPainter::draw() const {
	if (_drawCalls.empty()) [[unlikely]] return;
	
	// We bind the vao and the program (shader)
	_program->bind();
	
	for (auto i = std::size_t{0} ; i < _drawCalls.size() ; ++i) {
		// Bind the vao
		auto const& drawData = _drawData[i];
		auto const& object = _drawCalls[i];
		drawData.mesh.vao().bind();
		
		// Bind text texture
		gl::glActiveTexture(gl::GL_TEXTURE0);
		drawData.msdf.bind();
		
		// Bind other textures
		for (auto tex_id = std::size_t{0} ; tex_id < drawData.textures.size() ; ++tex_id) {
			gl::glActiveTexture(gl::GL_TEXTURE1 + static_cast<std::int32_t>(tex_id));
			drawData.textures[tex_id].bind();
		}
		
		// Set all the uniform for an object
		read_object_uniforms(object, [&](std::uint32_t id, auto const& data) {
			_program->uniformValue(id, data);
		});
		
		// Draw the object
		drawData.mesh.draw();
	}
}

void TextPainter::prepare(UniformSet const& context) {
	_entityModel.cleanup();
	
	auto drawIndex = std::size_t{0};
	for (auto index = std::size_t{0} ; index < _entityModel.size() ; ++index) {
		if (!_entityModel.alive(index)) continue;
		
		auto const object = _drawCalls[drawIndex];
		auto& model = _entityModel[index];
		auto& owner = _ownerData[index];
		auto& draw_data = _drawData[drawIndex];
		
		auto const text = std::any_cast<std::u8string>(model.uniforms.required<std::any>("text"_h));
		
		if (owner.last_text != text) {
			[[maybe_unused]]
			auto const total_advance = fill_glyph_buffer(_glyphBuffer, *owner.font->msdf().font, text);
			
			// TODO: uh oh, this is a opengl call
			owner.buffer->data(_glyphBuffer, gl::GL_STATIC_DRAW);
			owner.mesh->instance(static_cast<gl::GLsizei>(text.size()));
			
			owner.last_text = text;
			owner.last_advance = total_advance;
		}
		
		// Pre-allocated, will only do assignment
		model.uniforms.uniforms.insert_or_assign("total_advance"_h, owner.last_advance);
		
		auto const texture_unit = [unit = 1](GenericResource resource) mutable -> UniformData {
			if (resource.is<TextureTag>()) {
				return unit++;
			} else if (resource.is<FontTag>()) {
				return 0;
			} else {
				// Noop
				return resource;
			}
		};
		
		write_object_uniforms(object, texture_unit, model.uniforms, context);
		draw_data.textures.clear();
		std::ranges::copy(owner.textures | views::unwrap_resources, std::back_inserter(draw_data.textures));
		draw_data.mesh = *owner.mesh;
		draw_data.msdf = owner.font->texture();
		++drawIndex;
	}
	
	_drawCalls.resize(drawIndex);
}

void TextPainter::update(ModelInfo model, UniformSet const& environment) {
	auto const visible = model.uniforms.optional<bool>("visible"_h).value_or(true);
	auto const index = _entityModel.find(model.handle);
	
	if (visible) {
		if (index == _entityModel.size()) {
			insert_call(model, environment);
		} else {
			auto const& uniforms = _entityModel[index].uniforms = std::move(model.uniforms);
			auto& owner = _ownerData[index];
			
			owner.textures.clear();
			std::ranges::copy(
				resources_in_object<TextureTag>(_drawCalls.type(), uniforms, environment) | views::load_resources(_textures),
				std::back_inserter(owner.textures)
			);
			
			auto font_resource = model.uniforms.optional<FontTag>("font"_h);
			if (!font_resource) font_resource = environment.optional<FontTag>("font"_h);
			
			if (font_resource) {
				_ownerData[index].font = _fonts.get(*font_resource);
			}
		}
	} else {
		if (index != _entityModel.size()) {
			remove_call(model, index);
		}
	}
}

void TextPainter::refresh(UniformSet const& environment) {
	_entityModel.cleanup();
	
	for (auto index = std::size_t{0}; index < _entityModel.size(); ++index) {
		if (!_entityModel.alive(index)) continue;
		auto const& model = _entityModel[index];
		auto& owner = _ownerData[index];
		
		owner.textures.clear();
		std::ranges::copy(
			resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment) | views::load_resources(_textures),
			std::back_inserter(owner.textures)
		);
		
		auto font_resource = model.uniforms.optional<FontTag>("font"_h);
		if (!font_resource) font_resource = environment.optional<FontTag>("font"_h);
		
		if (font_resource) {
			owner.font = _fonts.get(*font_resource);
		}
	}
}

void TextPainter::remove(ModelInfo const& model) {
	auto const found_index = _entityModel.find(model.handle);
	
	if (found_index != _entityModel.size()) {
		remove_call(model, found_index);
	} else {
		Log::warning(SBG_LOG_INFO, "The model attached to entity", log::enquote(model.entity.id().index), "cannot be found in painter for removal");
	}
}

void TextPainter::insert_call(ModelInfo m, UniformSet const& environment) {
	auto&& [model, index] = _entityModel.insert(std::move(m));
	
	if (_ownerData.size() <= index) {
		_ownerData.resize(index + 1);
	}
	
	if (_drawData.size() <= index) {
		_drawData.resize(index + 1);
	}
	
	auto& owner = _ownerData[index] = {};
	owner.buffer = {};
	owner.font = {};
	owner.last_advance = {};
	owner.last_text.clear();
	owner.mesh = {};
	owner.textures.clear();
	
	auto& mesh = owner.mesh = _createArrayMesh();
	auto& vao = mesh->vao();
	auto& buffer = owner.buffer;
	
	buffer = OwnedResource{BufferObject{0, gl::GL_ARRAY_BUFFER}};
	buffer->create();
	
	vao.bind();
	vao.attach(*buffer, std::array{
		BufferObjectFormatPart{gl::GL_INT, 1, 0},
		BufferObjectFormatPart{gl::GL_FLOAT, 1, 4}
	}, 8);
	gl::glVertexAttribDivisor(2 + 0, 1);
	gl::glVertexAttribDivisor(2 + 1, 1);
	
	auto font_resource = model.uniforms.optional<FontTag>("font"_h);
	if (!font_resource) font_resource = environment.optional<FontTag>("font"_h);
	
	if (font_resource) {
		owner.font = _fonts.get(*font_resource);
	}
	
	std::ranges::copy(
		resources_in_object<TextureTag>(_drawCalls.type(), model.uniforms, environment) | views::load_resources(_textures),
		std::back_inserter(owner.textures)
	);
	
	// Pre-allocate total_advance
	model.uniforms.uniforms.insert_or_assign("total_advance"_h, 0.f);
	_drawCalls.grow(1);
}

void TextPainter::remove_call(ModelInfo const& model, std::size_t index) noexcept {
	_entityModel.erase(model);
	_drawCalls.shrink(1);
	_ownerData[index] = {};
}
