#pragma once

#include "text/service.h"

#include "../../factory/asciimsdffontfactory.h"
#include "../../factory/fontfactory.h"
#include "../../factory/msdffontfactory.h"
#include "../../model/textmodel.h"
#include "../../painter/textpainter.h"
#include "../../utility/msdftextmodelmatrix.h"
#include "../../utility/rectverticalalign.h"
