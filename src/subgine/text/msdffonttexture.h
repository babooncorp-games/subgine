#pragma once

#include "msdffont.h"
#include "subgine/opengl/texture.h"
#include "subgine/resource/ownedresource.h"

namespace sbg {

auto msdf_fixed_ascii(MsdfFont const& font) -> OwnedResource<Texture>;

} // namespace sbg
