#version 140

out vec4 outColor;

in vec2 UV;
flat in int glyph;

uniform sampler2DArray font;
uniform float range;
uniform float scaleFactor;
uniform float borderThickness;
uniform vec4 background;
uniform vec4 borderColor;
uniform vec4 color;
uniform mat4 model;
uniform float size;

float median(float r, float g, float b) {
	return max(min(r, g), min(max(r, g), b));
}

void main() {
	vec3 sample = texture(font, vec3(UV, glyph)).rgb;
	float median = median(sample.r, sample.g, sample.b);
	
	if (borderThickness > 0) {
		float scale = model[0][0] / size;
		float borderPositionDist = (scale * 0.5 * borderThickness / (scaleFactor * range));
		
		float sigDist = range * scaleFactor * ((median - 0.5) - borderPositionDist);
		float opacity = clamp(sigDist + 0.5 + borderPositionDist, 0.0, 1.0);
		
		float sigDist2 = range * scaleFactor * ((median - 0.5) + borderPositionDist);
		float opacity2 = clamp(sigDist2 + (0.5 - borderPositionDist), 0.0, 1.0);
		outColor = mix(background, mix(borderColor, color, opacity), opacity2);
	} else {
		float sigDist = range * scaleFactor * (median - 0.5);
		float opacity = clamp(sigDist + 0.5, 0.0, 1.0);
		outColor = mix(background, mix(vec4(color.rgb, background.a), color, opacity), opacity);
	}
}
