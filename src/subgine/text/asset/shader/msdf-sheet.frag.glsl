#version 140

out vec4 outColor;
in vec2 UV;

uniform sampler2DArray msdf;
uniform int glyph;
uniform float distanceFactor;
uniform vec4 bgColor;
uniform vec4 fgColor;

float median(float r, float g, float b) {
	return max(min(r, g), min(max(r, g), b));
}

void main() {
	vec3 sample = texture(msdf, vec3(UV, glyph)).rgb;
	float median = median(sample.r, sample.g, sample.b);
	float sigDist = distanceFactor*(median - 0.5);
	float sigDist2 = distanceFactor*(median - 0.3);
	float opacity = clamp(sigDist + 0.5, 0.0, 1.0);
	float opacity2 = clamp(sigDist2 + 0.3, 0.0, 1.0);
// 	outColor = mix(bgColor, fgColor, opacity);
	outColor = mix(bgColor, mix(vec4(0.2, 0.5, 0.1, 1), fgColor, opacity), opacity2);
// 	outColor = vec4(median, median, median, 1);
}
