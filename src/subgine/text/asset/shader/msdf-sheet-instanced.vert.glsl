#version 140

in vec3 vertexPosition;
in vec2 vertexUV;
in int vertexGlyph;
in float advancement;

out vec2 UV;
flat out int glyph;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float total_advance;
uniform int align;

float aligned_advancement(int align, float total_advance, float advancement) {
	if (align == 0) {
		return advancement;
	} else if (align == 1) {
		return advancement - total_advance;
	} else if (align == 2) {
		return advancement - total_advance / 2;
	} else {
		return 0.0; // Garbage in, garbage out
	}
}

void main()
{
	vec4 v = vec4(vertexPosition + vec3(aligned_advancement(align, total_advance, advancement), 0, 0), 1);
	
	gl_Position = projection * view * model * v;
	
	UV = vertexUV;
	glyph = vertexGlyph;
}
