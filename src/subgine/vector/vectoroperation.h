#pragma once

#include "vectorfwd.h"
#include "get.h"

#include "subgine/common/math.h"
#include "subgine/common/traits.h"

#include <utility>

namespace sbg {
namespace detail {


template<typename T, std::size_t... S>
struct VectorOperation {
private:
	static constexpr dim_t size = sizeof...(S);
	using self_t = Vector<size, T>;
	
	constexpr auto self() const -> self_t const& {
		return static_cast<self_t const&>(*this);
	}
	
	constexpr auto self() -> self_t& {
		return static_cast<self_t&>(*this);
	}
	
	constexpr T hypot(T x) const {
		return x;
	}
	
	auto hypot(T x, T y) const {
		return std::hypot(x, y);
	}
	
	auto hypot(T x, T y, T z) const {
		return std::hypot(x, y, z);
	}
	
	auto hypot(T x, T y, T z, T w) const {
		return std::sqrt(power<2>(x) + power<2>(y) + power<2>(z) + power<2>(w));
	}
	
public:
	constexpr auto operator-() const {
		return Vector<size, T>{-get<S>(self())...};
	}
	
	auto length() const -> math_t<T> {
		return hypot(get<S>(self())...);
	}
	
	auto length(math_t<T> len) const -> self_t {
		if constexpr (size > 1) {
			if (!null()) {
				auto factor = len / length();
				
				return self_t{(get<S>(self()) * factor)...};
			} else {
				return self_t{(S == 0 ? len : 0)...};
			}
		} else {
			return self_t{len};
		}
	}
	
	void applyLength(math_t<T> len) {
		self() = length(len);
	}
	
	constexpr bool null() const {
		return ((get<S>(self()) == 0) && ...);
	}
	
	template<typename U>
	constexpr auto dot(const Vector<size, U>& vec) const {
		return ((get<S>(self()) * get<S>(vec)) + ...);
	}
	
	template<typename U = T, enable_if_i<std::is_signed_v<U> || size != 1> = 0>
	auto unit() const {
		if (!null()) {
			auto len = length();
			
			return self_t{(get<S>(self()) / len)...};
		} else {
			self_t result;
			
			result.x = 1;
			
			return result;
		}
	}
	
	template<typename U = T, enable_if_i<!std::is_signed_v<U> && size == 1> = 0>
	auto unit() const {
		return self_t{1};
	}
	
	template<typename U>
	constexpr bool operator==(const Vector<size, U>& other) const {
		return ((get<S>(self()) == get<S>(other)) && ...);
	}
	
	template<typename U>
	constexpr bool operator!=(const Vector<size, U>& other) const {
		return !(*this == other);
	}
	
	template<typename U>
	constexpr bool operator<(const Vector<size, U>& other) const {
		return (power<2>(get<S>(self())) + ...) < (power<2>(get<S>(other)) + ...);
	}
	
	template<typename U>
	constexpr bool operator>(const Vector<size, U>& other) const {
		return (power<2>(get<S>(self())) + ...) > (power<2>(get<S>(other)) + ...);
	}
	
	constexpr bool operator<(math_t<T> length) const {
		return (power<2>(get<S>(self())) + ...) < power<2>(length);
	}
	
	constexpr bool operator>(math_t<T> length) const {
		return (power<2>(get<S>(self())) + ...) > power<2>(length);
	}
	
	template<typename U>
	constexpr bool operator<=(const Vector<size, U>& other) const {
		return (power<2>(get<S>(self())) + ...) <= (power<2>(get<S>(other)) + ...);
	}
	
	template<typename U>
	constexpr bool operator>=(const Vector<size, U>& other) const {
		return (power<2>(get<S>(self())) + ...) >= (power<2>(get<S>(other)) + ...);
	}
	
	constexpr bool operator<=(math_t<T> length) const {
		return (power<2>(get<S>(self())) + ...) <= power<2>(length);
	}
	
	constexpr bool operator>=(math_t<T> length) const {
		return (power<2>(get<S>(self())) + ...) >= power<2>(length);
	}
	
	template<typename U, enable_if_i<std::is_arithmetic_v<U>> = 0>
	constexpr auto operator/(U divider) const {
		return Vector{(get<S>(self()) / divider)...};
	}
	
	template<typename U>
	constexpr auto operator/(const Vector<size, U>& vec) const {
		return Vector{(get<S>(self()) / get<S>(vec))...};
	}
	
	template<typename U, enable_if_i<std::is_arithmetic_v<U>> = 0>
	constexpr auto operator*(U multiplier) const {
		return Vector{(get<S>(self()) * multiplier)...};
	}
	
	template<typename U>
	constexpr auto operator*(const Vector<size, U>& vec) const {
		return Vector{(get<S>(self()) * get<S>(vec))...};
	}
	
	template<typename U>
	constexpr auto operator+(const Vector<size, U>& vec) const {
		return Vector{(get<S>(self()) + get<S>(vec))...};
	}
	
	template<typename U>
	constexpr auto operator-(const Vector<size, U>& vec) const {
		return Vector{(get<S>(self()) - get<S>(vec))...};
	}
	
	constexpr auto operator+=(const Vector<size, T>& vec) -> Vector<size, T>& {
		self() = self() + vec;
		return self();
	}
	
	template<typename U>
	constexpr auto operator-=(const Vector<size, U>& vec) -> Vector<size, T>& {
		self() = self() - vec;
		return self();
	}
	
	template<typename U>
	constexpr auto operator/=(const Vector<size, U>& vec) -> Vector<size, T>& {
		self() = self() / vec;
		return self();
	}
	
	template<typename U>
	auto operator*=(const Vector<size, U>& vec) -> Vector<size, T>& {
		self() = self() * vec;
		return self();
	}
	
	auto operator*=(math_t<T> multiplier) -> Vector<size, T>& {
		self() = self() * multiplier;
		return self();
	}
	
	auto operator/=(math_t<T> divider) -> Vector<size, T>& {
		self() = self() / divider;
		return self();
	}
};

template<typename U, typename T, std::size_t... S, enable_if_i<std::is_arithmetic_v<U>> = 0>
constexpr bool operator<(U length, VectorOperation<T, S...> const& vec) {
	constexpr std::size_t size = sizeof...(S);
	auto&& self = static_cast<Vector<size, T> const&>(vec);
	
	return power<2>(length) < (power<2>(get<S>(self)) + ...);
}

template<typename U, typename T, std::size_t... S, enable_if_i<std::is_arithmetic_v<U>> = 0>
constexpr bool operator>(U length, VectorOperation<T, S...> const& vec) {
	constexpr std::size_t size = sizeof...(S);
	auto&& self = static_cast<Vector<size, T> const&>(vec);
	
	return power<2>(length) > (power<2>(get<S>(self)) + ...);
}

template<typename U, typename T, std::size_t... S, enable_if_i<std::is_arithmetic_v<U>> = 0>
constexpr auto operator*(U multiplier, VectorOperation<T, S...> const& vec) {
	constexpr std::size_t size = sizeof...(S);
	auto&& self = static_cast<Vector<size, T> const&>(vec);
	
	return Vector{(get<S>(self) * multiplier)...};
}

template<typename U, typename T, std::size_t... S, enable_if_i<std::is_arithmetic_v<U>> = 0>
constexpr auto operator/(U numerator, VectorOperation<T, S...> const& vec) {
	constexpr std::size_t size = sizeof...(S);
	auto&& self = static_cast<Vector<size, T> const&>(vec);
	
	return Vector{(numerator / get<S>(self))...};
}

} // namespace detail
} // namespace sbg
