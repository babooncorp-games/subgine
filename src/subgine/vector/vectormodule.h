#pragma once

namespace sbg {

struct VectorModuleService;

struct VectorModule {
	friend auto service_map(VectorModule const&) -> VectorModuleService;
};

} // namespace sbg
