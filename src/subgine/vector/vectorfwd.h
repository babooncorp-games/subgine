#pragma once

#include <type_traits>

namespace sbg {

using dim_t = int;

template<typename T>
using math_t = std::conditional_t<std::is_floating_point<T>::value, T, double>;

template<dim_t, typename>
struct Vector;

template<typename>
inline constexpr auto is_vector = false;

template<dim_t n, typename T>
inline constexpr auto is_vector<Vector<n, T>> = true;

namespace detail {

template<typename, typename>
struct VectorParts;

template<typename, std::size_t>
struct VectorElement;

} // namespace detail
} // namespace sbg
