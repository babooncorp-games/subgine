#pragma once

#include "vectorparts.h"
#include "specificoperation.h"
#include "hash.h"

#include "subgine/common/traits.h"

#include <ostream>

#include "subgine/common/define.h"

namespace sbg {

template<dim_t n, typename T>
struct SUBGINE_EMPTY_BASES Vector : detail::VectorParts<T, std::make_index_sequence<n>>, detail::SpecificOperation<Vector<n, T>> {
	using detail::VectorParts<T, std::make_index_sequence<n>>::VectorParts;
	
	friend auto operator<<(std::ostream& o, Vector<n, T> const& vec) -> std::ostream& {
		for_sequence<n>([&](auto i) {
			if constexpr (i != 0) {
				o << ',' << ' ';
			}
			
			o << get<i>(vec);
		});
		
		return o;
	}
	
	template<typename To, enable_if_i<std::is_convertible_v<T, To>> = 0>
	constexpr operator Vector<n, To> () const {
		return convert<To>();
	}
	
	template<typename To, enable_if_i<is_strictly_explicitly_convertible<T, To>::value> = 0>
	constexpr explicit operator Vector<n, To> () const {
		return convert<To>();
	}
	
	template<typename To, std::size_t size = n, enable_if_i<std::is_convertible_v<T, To> && size == 1> = 0>
	constexpr operator To() const {
		return static_cast<To>(this->x);
	}
	
	template<typename To, std::size_t size = n, enable_if_i<is_strictly_explicitly_convertible_v<T, To> && size == 1> = 0>
	constexpr explicit operator To() const {
		return static_cast<To>(this->x);
	}
	
private:
	template<typename To>
	constexpr auto convert() const {
		auto to = [](auto v) { return static_cast<To>(v); };
		
		return apply_sequence<n>([&](auto... s) {
			return Vector<n, To>{to(get<s>(*this))...};
		});
	}
};

template<typename First, typename... Rest, enable_if_i<std::conjunction_v<std::is_same<First, Rest>...>> = 0>
Vector(First, Rest...) -> Vector<sizeof...(Rest) + 1, First>;

extern template struct Vector<1, float>;
extern template struct Vector<1, double>;
extern template struct Vector<1, long double>;
extern template struct Vector<1, int>;
extern template struct Vector<1, unsigned int>;
extern template struct Vector<1, long>;
extern template struct Vector<1, unsigned long>;
extern template struct Vector<1, short>;
extern template struct Vector<1, unsigned short>;
extern template struct Vector<1, char>;
extern template struct Vector<1, unsigned char>;
extern template struct Vector<2, float>;
extern template struct Vector<2, double>;
extern template struct Vector<2, long double>;
extern template struct Vector<2, int>;
extern template struct Vector<2, unsigned int>;
extern template struct Vector<2, long>;
extern template struct Vector<2, unsigned long>;
extern template struct Vector<2, short>;
extern template struct Vector<2, unsigned short>;
extern template struct Vector<2, char>;
extern template struct Vector<2, unsigned char>;
extern template struct Vector<3, float>;
extern template struct Vector<3, double>;
extern template struct Vector<3, long double>;
extern template struct Vector<3, int>;
extern template struct Vector<3, unsigned int>;
extern template struct Vector<3, long>;
extern template struct Vector<3, unsigned long>;
extern template struct Vector<3, short>;
extern template struct Vector<3, unsigned short>;
extern template struct Vector<3, char>;
extern template struct Vector<3, unsigned char>;
extern template struct Vector<4, float>;
extern template struct Vector<4, double>;
extern template struct Vector<4, long double>;
extern template struct Vector<4, int>;
extern template struct Vector<4, unsigned int>;
extern template struct Vector<4, long>;
extern template struct Vector<4, unsigned long>;
extern template struct Vector<4, short>;
extern template struct Vector<4, unsigned short>;
extern template struct Vector<4, char>;
extern template struct Vector<4, unsigned char>;

template<typename T>
using Vector1 = Vector<1, T>;

using Vector1f = Vector1<float>;
using Vector1d = Vector1<double>;
using Vector1ld = Vector1<long double>;
using Vector1i = Vector1<int>;
using Vector1ui = Vector1<unsigned int>;
using Vector1l = Vector1<long>;
using Vector1ul = Vector1<unsigned long>;
using Vector1s = Vector1<short>;
using Vector1us = Vector1<unsigned short>;
using Vector1c = Vector1<char>;
using Vector1uc = Vector1<unsigned char>;

template<typename T>
using Vector2 = Vector<2, T>;

using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;
using Vector2ld = Vector2<long double>;
using Vector2i = Vector2<int>;
using Vector2ui = Vector2<unsigned int>;
using Vector2l = Vector2<long>;
using Vector2ul = Vector2<unsigned long>;
using Vector2s = Vector2<short>;
using Vector2us = Vector2<unsigned short>;
using Vector2c = Vector2<char>;
using Vector2uc = Vector2<unsigned char>;

template<typename T>
using Vector3 = Vector<3, T>;

using Vector3f = Vector3<float>;
using Vector3d = Vector3<double>;
using Vector3ld = Vector3<long double>;
using Vector3i = Vector3<int>;
using Vector3ui = Vector3<unsigned int>;
using Vector3l = Vector3<long>;
using Vector3ul = Vector3<unsigned long>;
using Vector3s = Vector3<short>;
using Vector3us = Vector3<unsigned short>;
using Vector3c = Vector3<char>;
using Vector3uc = Vector3<unsigned char>;

template<typename T>
using Vector4 = Vector<4, T>;

using Vector4f = Vector4<float>;
using Vector4d = Vector4<double>;
using Vector4ld = Vector4<long double>;
using Vector4i = Vector4<int>;
using Vector4ui = Vector4<unsigned int>;
using Vector4l = Vector4<long>;
using Vector4ul = Vector4<unsigned long>;
using Vector4s = Vector4<short>;
using Vector4us = Vector4<unsigned short>;
using Vector4c = Vector4<char>;
using Vector4uc = Vector4<unsigned char>;

template<dim_t n, typename T, typename F>
constexpr auto lerp(Vector<n, T> a, Vector<n, T> b, F t) -> Vector<n, T> {
	return apply_sequence<n>([a, b, t](auto... s) {
		return Vector<n, T>{static_cast<T>(std::lerp(get<s>(a), get<s>(b), t))...};
	});
}

} // namespace sbg

#include "subgine/common/undef.h"
