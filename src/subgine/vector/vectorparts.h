#pragma once

#include "vectorfwd.h"
#include "vectoroperation.h"

#include <tuple>

namespace sbg::detail {

template<typename T>
struct VectorElement<T, 1> {
	T x = {};
};

template<typename T>
struct VectorElement<T, 2> {
	T x = {};
	T y = {};
};

template<typename T>
struct VectorElement<T, 3> {
	T x = {};
	T y = {};
	T z = {};
};

template<typename T>
struct VectorElement<T, 4> {
	T x = {};
	T y = {};
	T z = {};
	T w = {};
};

template<typename T, std::size_t... S>
struct VectorParts<T, std::index_sequence<S...>> : VectorElement<T, sizeof...(S)>, VectorOperation<T, S...> {
private:
	template<typename U, std::size_t>
	using parameter_expand = U;
	
public:
	static constexpr dim_t size = sizeof...(S);
	using type = T;
	
	constexpr VectorParts() = default;
	
	template<std::size_t s = size, enable_if_i<s != 1> = 0>
	constexpr explicit VectorParts(T value) : VectorElement<T, size>{(void(S), value)...} {}
	
	constexpr VectorParts(parameter_expand<T, S>... values) : VectorElement<T, size>{values...} {}
};

} // namespace sbg::detail
