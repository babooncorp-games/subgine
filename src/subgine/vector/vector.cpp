#include "vector.h"

namespace sbg {

template struct Vector<1, float>;
template struct Vector<1, double>;
template struct Vector<1, long double>;
template struct Vector<1, int>;
template struct Vector<1, unsigned int>;
template struct Vector<1, long>;
template struct Vector<1, unsigned long>;
template struct Vector<1, short>;
template struct Vector<1, unsigned short>;
template struct Vector<1, char>;
template struct Vector<1, unsigned char>;
template struct Vector<2, float>;
template struct Vector<2, double>;
template struct Vector<2, long double>;
template struct Vector<2, int>;
template struct Vector<2, unsigned int>;
template struct Vector<2, long>;
template struct Vector<2, unsigned long>;
template struct Vector<2, short>;
template struct Vector<2, unsigned short>;
template struct Vector<2, char>;
template struct Vector<2, unsigned char>;
template struct Vector<3, float>;
template struct Vector<3, double>;
template struct Vector<3, long double>;
template struct Vector<3, int>;
template struct Vector<3, unsigned int>;
template struct Vector<3, long>;
template struct Vector<3, unsigned long>;
template struct Vector<3, short>;
template struct Vector<3, unsigned short>;
template struct Vector<3, char>;
template struct Vector<3, unsigned char>;
template struct Vector<4, float>;
template struct Vector<4, double>;
template struct Vector<4, long double>;
template struct Vector<4, int>;
template struct Vector<4, unsigned int>;
template struct Vector<4, long>;
template struct Vector<4, unsigned long>;
template struct Vector<4, short>;
template struct Vector<4, unsigned short>;
template struct Vector<4, char>;
template struct Vector<4, unsigned char>;

}
