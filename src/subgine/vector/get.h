#pragma once

#include "vectorfwd.h"
#include "subgine/common/traits.h"

#include <utility>
#include <tuple>

#include "subgine/common/define.h"

namespace sbg {

template<std::size_t I, typename T, typename V = std::decay_t<T>, enable_if_i<(I < V::size) && is_vector<V>> = 0>
constexpr auto get(T&& vec) -> auto&& {
	if constexpr (I == 0) {
		return FWD(vec).x;
	} else if constexpr (I == 1) {
		return FWD(vec).y;
	} else if constexpr (I == 2) {
		return FWD(vec).z;
	} else if constexpr (I == 3) {
		return FWD(vec).w;
	}
}

} // namespace sbg

template<sbg::dim_t n, typename T>
struct std::tuple_size<sbg::Vector<n, T>> : std::integral_constant<std::size_t, n> {};

template<std::size_t I, sbg::dim_t n, typename T>
struct std::tuple_element<I, sbg::Vector<n, T>> {
	static_assert(I < n, "Index overflow");
	using type = T;
};

#include "subgine/common/undef.h"
