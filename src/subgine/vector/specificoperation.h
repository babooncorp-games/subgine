#pragma once

#include "vectorfwd.h"

#include "subgine/common/math.h"

namespace sbg {
namespace detail {

template<typename>
struct SpecificOperation {};

template<typename T>
struct SpecificOperationCommon {
	auto self() const -> T const& {
		return static_cast<const T&>(*this);
	}
	
	auto self() -> T& {
		return static_cast<T&>(*this);
	}
};

template<typename T>
struct SpecificOperation<Vector<1, T>> : SpecificOperationCommon<Vector<1, T>> {
private:
	using SpecificOperationCommon<Vector<1, T>>::self;
	
public:
	template<typename U, enable_if_i<std::is_signed_v<U>> = 0>
	constexpr auto cross(Vector<2, U> const& other) const {
		return Vector{-self().x * other.y, self().x * other.x};
	}
};

template<typename T>
struct SpecificOperation<Vector<2, T>> : SpecificOperationCommon<Vector<2, T>> {
private:
	using SpecificOperationCommon<Vector<2, T>>::self;
	
public:
	auto angle() const -> math_t<T> {
		auto angle = std::atan2(self().y, self().x);
		
		if (angle < 0) {
			angle += static_cast<decltype(angle)>(tau);
		}
		
		return angle;
	}
	
	void applyAngle(math_t<T> a) {
		self() = angle(a);
	}
	
	Vector<2, T> angle(math_t<T> angle) const {
		Vector<2, T> value;
		
		if (!self().null()) {
			auto lenght = self().length();
			value.x = static_cast<T>(std::cos(angle) * lenght);
			value.y = static_cast<T>(std::sin(angle) * lenght);
		}
		
		return value;
	}
	
	template<typename U>
	auto normal(Vector<2, U> const& other) const {
		return (self() - other).cross(1).unit();
	}
	
	template<typename U>
	constexpr auto cross(Vector<2, U> const& other) const {
		return Vector{(self().x * other.y) - (self().y * other.x)};
	}
	
	template<typename U, enable_if_i<std::is_arithmetic_v<U>> = 0>
	constexpr auto cross(U multiplier) const {
		return Vector{multiplier * self().y, -multiplier * self().x};
	}
	
	template<typename U>
	constexpr auto cross(Vector<1, U> const& multiplier) const {
		return Vector{multiplier * self().y, -multiplier * self().x};
	}
};

template<typename T>
struct SpecificOperation<Vector<3, T>> : SpecificOperationCommon<Vector<3, T>> {
private:
	using SpecificOperationCommon<Vector<3, T>>::self;
	
public:
	template<typename U>
	constexpr auto cross(Vector<3, U> const& other) const {
		return Vector{
			self().y * other.z - self().z * other.y,
			-1 * self().x * other.z + self().z * other.x,
			self().x * other.y - self().y * other.x
		};
	}
	
	template<typename U>
	constexpr auto normal(const Vector<3, U>& other) const {
		return cross(other).unit();
	}
	
	void applyAngle(Vector<2, math_t<T>> const& angles) {
		if (!self().null()) {
			auto lenght = self().length();
			
			self().x = static_cast<T>(std::sin(angles.y) * std::cos(angles.x) * lenght);
			self().y = static_cast<T>(std::sin(angles.x) * std::sin(angles.y) * lenght);
			self().z = static_cast<T>(std::cos(angles.y) * lenght);
		}
	}
	
	auto angle(Vector<2, math_t<T>> const& angles) const -> Vector<3, T> {
		Vector<3, T> value;
		
		if (!self().null()) {
			auto lenght = self().length();
			value.x = static_cast<T>(std::sin(angles.y) * std::cos(angles.x) * lenght);
			value.y = static_cast<T>(std::sin(angles.x) * std::sin(angles.y) * lenght);
			value.z = static_cast<T>(std::cos(angles.y) * lenght);
		}
		
		return value;
	}
	
	auto angle() const -> Vector<2, math_t<T>> {
		return Vector{
			std::atan(static_cast<math_t<T>>(self().y) / static_cast<math_t<T>>(self().x)),
			std::acos(static_cast<math_t<T>>(self().z) / self().length())
		};
	}
};

} // namespace detail
} // namespace sbg
