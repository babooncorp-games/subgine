#pragma once

#include "../vectormodule.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct VectorModuleService : kgr::single_service<VectorModule> {};

} // namespace sbg
