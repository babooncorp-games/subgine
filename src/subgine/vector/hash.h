#pragma once

#include "vectorfwd.h"
#include "get.h"

#include "subgine/common/math.h"
#include "subgine/common/traits.h"
#include "subgine/common/pairhash.h"

#include <functional>

namespace std {

template<int n, typename T>
struct hash<sbg::Vector<n, T>> {
	size_t operator()(sbg::Vector<n, T> const& vec) const {
		std::size_t seed = 0;
		
		sbg::for_tuple(vec, [&](auto const& v) {
			sbg::hash_combine(seed, v);
		});
		
		return seed;
	}
};

} // namespace std
