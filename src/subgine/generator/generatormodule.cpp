#include "generatormodule.h"

#include "subgine/console.h"

#include "nlohmann/json.hpp"
#include "cpplocate/cpplocate.h"
#include "inja/inja.hpp"

#include <filesystem>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <fstream>

using namespace sbg;
namespace fs = std::filesystem;

namespace {
	auto kebab_to_camel(std::string_view original_name) -> std::string {
		return std::accumulate(
			original_name.begin(), original_name.end(), std::string{},
			[&](std::string name, char const next_character) {
				if (name.size() > 0 && name.back() == '-') {
					name.back() = std::toupper(next_character);
					return name;
				}
				
				if (name.size() == 1) {
					name.front() = std::toupper(name.front());
				}
				
				return name + next_character;
			}
		);
	}

	auto camel_to_kebab(std::string_view original_name) -> std::string {
		return std::accumulate(
			original_name.begin(), original_name.end(), std::string{},
			[&](std::string name, char const next_character) {
				if (name.size() > 1 && std::isupper(next_character)) {
					return name + '-' + static_cast<char>(std::tolower(next_character));
				}
				
				if (name.size() == 1) {
					name.front() = std::tolower(name.front());
				}
				
				return name + next_character;
			}
		);
	}
	
	auto render_content(inja::Environment& env, nlohmann::json const& data, fs::path const& path) -> std::string {
		auto const temp = env.parse_template(path.string());
		return env.render(temp, data);
	}
	
	auto render_filename_path(inja::Environment& env, nlohmann::json const& data, fs::path const& path) -> fs::path {
		return fs::path{env.render(path.string(), data)};
	}
	
	void write_directory(ConsoleContext const& context, fs::path const& target_full_path) {
		context.output("Making directory ", target_full_path.string(), "\n");
		fs::create_directory(target_full_path);
	}
	
	void write_template(ConsoleContext const& context, inja::Environment& env, nlohmann::json const& data, fs::path const& input_path, fs::path const& target_full_path) {
		auto const content = render_content(env, data, input_path);
		context.output("Writing into ", target_full_path.string(), "... ");
		std::ofstream{target_full_path} << content;
		context.output("done!\n");
	}
	
	void write_file(ConsoleContext const& context, fs::path const& input_path, fs::path const& target_full_path) {
		context.output("Copying file ", target_full_path.string(), "... ");
		fs::copy_file(input_path, target_full_path);
		context.output("done!\n");
	}
	
	void write_template_structure(ConsoleContext const& context, fs::path const& template_directory, fs::path const& output_dir, inja::Environment& env, nlohmann::json const& data) {
		for (auto const& entry : fs::recursive_directory_iterator(template_directory)) {
			auto const relative_path = fs::relative(entry.path(), template_directory);
			auto const target_path = render_filename_path(env, data, relative_path);
			
			if (entry.is_directory()) {
				write_directory(context, output_dir / target_path);
			} else if (target_path.extension() == ".inja") {
				write_template(context, env, data, entry.path(), output_dir / target_path.parent_path() / target_path.stem());
			} else {
				write_file(context, entry.path(), output_dir / target_path);
			}
		}
	}
	
	auto data_parameters(std::string_view module_namespace, std::string_view first_namespace, std::string_view full_namespace, std::string_view class_name) {
		auto target_name = first_namespace.empty() ? std::string{class_name} : std::string{first_namespace} + "::" + std::string{class_name};
		std::transform(target_name.begin(), target_name.end(), target_name.begin(), tolower);
		
		auto module_kebab = camel_to_kebab(std::string{class_name});
		
		return std::tuple{
			std::string{module_namespace},
			target_name,
			std::string{full_namespace},
			std::string{class_name},
			module_kebab,
			module_kebab
		};
	}
	
	auto parse_class_name(std::string_view full_name) {
		auto const first_namespace_pos = full_name.find_first_of("::");
		auto const full_namespace_pos = full_name.find_last_of("::");
		
		return std::tuple{
			first_namespace_pos != full_name.npos ? full_name.substr(0, first_namespace_pos) : "",
			full_namespace_pos != full_name.npos ? full_name.substr(0, full_namespace_pos - 1) : "",
			full_namespace_pos != full_name.npos ? full_name.substr(full_namespace_pos + 1) : full_name
		};
	}
	
	auto get_module_data(ConsoleContext const& context) {
		auto data = nlohmann::json{};
		auto const full_class_name = context.prompt("Module class name (CamelCase with C++ namespace): ");
		auto const module_namespace = context.prompt("Module namespace (subdirectory): ");
		
		auto module_kebab = std::string{};
		auto const [first_namespace, full_namespace, class_name] = parse_class_name(full_class_name);
		
		std::tie(
			data["module_namespace"],
			data["target"],
			data["namespace"],
			data["module"],
			data["module_kebab"],
			module_kebab
		) = data_parameters(module_namespace, first_namespace, full_namespace, class_name);
		
		return std::tuple{data, module_kebab, module_namespace};
	}
	
	auto get_project_data(ConsoleContext const& context) {
		auto data = nlohmann::json{};
		auto const project_name = context.prompt("Project name (kebab-case): ");
		auto const project_namespace = context.prompt("Namespace (C++ namespace): ");
		auto const class_name = kebab_to_camel(project_name);
		
		data["class_name"] = class_name;
		data["namespace"] = project_namespace;
		data["project_name"] = project_name;
		
		return std::tuple{data, project_name};
	}
	
	auto template_root() -> fs::path {
		return fs::path{cpplocate::getModulePath()}.parent_path() / "share/subgine/template/";
	}
}

void GeneratorModule::setupConsoleCommands(ConsoleCommands& consoleCommands) const {
	consoleCommands.command("generate", "module", [](ConsoleContext context) -> int {
		auto env = inja::Environment{};
		
		auto const current_dir = context.args.size() == 1 ? fs::path{context.args[0]} : fs::current_path();
		auto const src = current_dir / "src";
		auto const [data, module_kebab, module_namespace] = get_module_data(context);
		
		if (module_namespace.find_first_of(".") != module_namespace.npos || module_namespace.find_first_of("/") != module_namespace.npos) {
			context.error("Error: Module namespace cannot contain '.' or '/'\n");
			return 1;
		}
		
		if (!fs::is_directory(src)) {
			context.error("Error: directory \"./src/\" don't exists.\nPlease run this command in the root directory of your project.\n");
			return 1;
		}
		
		auto const namespace_root = src / module_namespace;
		auto const module_root = namespace_root / module_kebab;
		
		if (!fs::is_directory(namespace_root)) {
			context.error("Error: namespace directory \"", module_root.string(), "\" don't exists.\nPlease create this directory before running this command.\n");
			return 1;
		}
		
		if (fs::is_directory(module_root) && !fs::is_empty(module_root)) {
			context.error("Module directory \"", module_root.string(), "\" exists and is not empty.\nDelete existing directory? [N/y]: ");
			auto const choice = context.input();
			if (choice.size() == 1 && tolower(choice[0]) == 'y') {
				fs::remove_all(module_root);
			} else {
				context.error("Aborting\n");
				return 1;
			}
		}
		
		fs::create_directory(module_root);
		
		auto const template_directory = template_root() / "module";
		write_template_structure(context, template_directory, module_root, env, data);
		context.output("All done!\n\nDon't forget to add your new module to your namespace's CMake file and don't forget to instantiate it in your app class.\nHappy coding!\n");
		
		return 0;
	});
	
	consoleCommands.command("generate", "project", [](ConsoleContext context) -> int {
		auto env = inja::Environment{};
		auto const [data, project_name] = get_project_data(context);
		auto const current_dir = context.args.size() == 1 ? fs::path{context.args[0]} : fs::current_path();
		auto const project_root_dir = current_dir / project_name;
		
		if (fs::is_directory(project_root_dir) && !fs::is_empty(project_root_dir)) {
			context.error("Project directory \"", project_root_dir.string(), "\" exists and is not empty. Aborting.\n");
			return 1;
		}
		
		fs::create_directory(project_root_dir);
		
		auto const template_directory = template_root() / "project";
		write_template_structure(context, template_directory, project_root_dir, env, data);
		context.output("All done!\n\nSimply run 'subgine-pkg setup' and 'subgine-pkg install' in the created directory to make the new project ready to build.\nHappy coding!\n");
		
		return 0;
	});
}
