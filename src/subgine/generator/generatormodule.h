#pragma once

namespace sbg {

struct ConsoleCommands;

struct GeneratorModule {
	void setupConsoleCommands(ConsoleCommands& consoleCommands) const;
};

} // namespace sbg
