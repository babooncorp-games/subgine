#pragma once

#include "../generatormodule.h"

#include "subgine/console/service/consolecommandsservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct GeneratorModuleService : kgr::single_service<GeneratorModule>, autocall<&GeneratorModule::setupConsoleCommands> {};

auto service_map(GeneratorModule const&) -> GeneratorModuleService;

} // namespace sbg
