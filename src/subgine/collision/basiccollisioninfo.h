#pragma once

#include "subgine/entity/entity.h"

#include <string_view>

namespace sbg {

struct BasicCollisionInfo {
	std::string_view group;
	Entity other;
};

} // namespace sbg
