#pragma once

#include "../component/collisionbody.h"
#include "../creator/collisionprofilecreator.h"

#include "subgine/entity/entity.h"
#include "subgine/resource/property.h"

namespace sbg {

struct CollisionBodyFactoryService;

struct CollisionBodyFactory {
	CollisionBodyFactory(CollisionProfileFactory profileFactory);
	
	CollisionBody create(Entity entity, Property data) const;
	
private:
	CollisionProfileFactory _profileFactory;
	
	friend auto service_map(CollisionBodyFactory const&) -> CollisionBodyFactoryService;
};

} // namespace sbg
