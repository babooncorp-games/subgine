#include "collisionbodyfactory.h"

#include "subgine/log.h"

namespace sbg {

CollisionBodyFactory::CollisionBodyFactory(CollisionProfileFactory profileFactory) : _profileFactory{profileFactory} {}

CollisionBody CollisionBodyFactory::create(Entity entity, Property data) const {
	CollisionBody body;
	
	for (auto const [name, profile] : data["profiles"].items()) {
		auto const type = std::string_view{profile["type"]};
		Log::trace(SBG_LOG_INFO, "Creating profile of type", log::enquote(type));
		body.profiles.emplace(name, _profileFactory.create(type, entity, profile["data"]));
	}
	
	return body;
}

} // namespace sbg
