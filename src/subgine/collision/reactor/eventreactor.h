#pragma once

#include "../utility/traits.h"
#include "../collisioninfo.h"

#include "subgine/entity/entity.h"
#include "subgine/system/deferredscheduler.h"
#include "subgine/common/function_traits.h"
#include "subgine/common/dropping_invoke.h"
#include "subgine/frame/frameevent.h"

#include <tuple>

#include "subgine/common/define.h"

namespace sbg {

template<typename Reactor, typename Enter, typename Exit>
struct EventReactorService;

struct DefaultReactor {
	constexpr inline void operator()(BasicCollisionInfo const&) const noexcept {}
};

/**
 * This class is a reactor that has a enter and exit callback.
 * The reactor type and callback types are template parameters.
 * If polymorphic callbacks are needed, set Enter and Exit to be std::function
 * 
 * The threshold set the number of time the deferred scheduler should be
 * called without any collision to happen before calling the exit event.
 */
template<typename Enter, typename Reactor, typename Exit>
struct EventReactor {
private:
	template<typename T>
	using first_argument = function_argument_t<0, T>;
	
	using ReactorInfoType = std::decay_t<detected_or<BasicCollisionInfo, first_argument, Reactor>>;
	
	using InfoType =
		std::conditional_t<
			std::is_same_v<ReactorInfoType, BasicCollisionInfo>,
			std::decay_t<detected_or<BasicCollisionInfo, first_argument, Enter>>,
			ReactorInfoType
		>;
	
	static_assert(is_dropping_invocable_v<Enter, InfoType const&>, "The Enter callback must be callable with a collision info");
	static_assert(is_dropping_invocable_v<Exit, BasicCollisionInfo const&>, "The Exit callback must be callable with a collision info");
	
	struct EnterFunction {
		BasicCollisionInfo* collision;
		
		NO_UNIQUE_ADDRESS
		Enter enter;
		
		auto operator()(InfoType const& info) const -> void {
			*collision = info;
			dropping_invoke(enter, info);
		}
	};
	
	struct ReactorFunction {
		BasicCollisionInfo* collision;
		
		NO_UNIQUE_ADDRESS
		Reactor react;
		
		auto operator()(InfoType const& info) const -> void {
			*collision = info;
			dropping_invoke(react, info);
		}
	};
	
	struct ExitFunction {
		BasicCollisionInfo* collision;
		
		NO_UNIQUE_ADDRESS
		Exit exit;
		
		auto operator()() const -> void {
			dropping_invoke(exit, std::exchange(*collision, {}));
		}
	};
	
public:
	explicit EventReactor(DeferredScheduler& scheduler, Entity entity, Enter enter, Reactor reactor, Exit exit) noexcept :
		_frameEvent{
			scheduler, entity,
			EnterFunction{std::addressof(_collision), std::move(enter)},
			ReactorFunction{std::addressof(_collision), std::move(reactor)},
			ExitFunction{std::addressof(_collision), std::move(exit)},
		} {}
	
	explicit EventReactor(DeferredScheduler& scheduler, Entity entity, Enter enter, Exit exit) noexcept :
		_frameEvent{
			scheduler, entity,
			EnterFunction{std::addressof(_collision), std::move(enter)},
			ReactorFunction{std::addressof(_collision), Reactor{}},
			ExitFunction{std::addressof(_collision), std::move(exit)},
		} {}
	
	auto operator()(InfoType const& info) -> void {
		_frameEvent(info);
	}
	
private:
	FrameEvent<EnterFunction, ReactorFunction, ExitFunction> _frameEvent;
	BasicCollisionInfo _collision;
	
	friend auto service_map(EventReactor const&) -> EventReactorService<Reactor, Enter, Exit>;
};

template<typename A, typename B>
EventReactor(DeferredScheduler&, Entity, A, B) -> EventReactor<A, DefaultReactor, B>;

} // namespace sbg

#include "subgine/common/undef.h"
