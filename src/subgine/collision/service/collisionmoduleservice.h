#pragma once

#include "../collisionmodule.h"

#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "collisionengineservice.h"
#include "collisionbodyfactoryservice.h"
#include "collisionreactorcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct CollisionModuleService : kgr::single_service<CollisionModule>,
	autocall<
		&CollisionModule::setupMainEngine,
		&CollisionModule::setupCollisionEngine,
		&CollisionModule::setupComponentCreator,
		&CollisionModule::setupEntityBindingCreator,
		&CollisionModule::setupCollisionReactorCreator
	> {};

} // namespace sbg
