#pragma once

#include "../creator/collisionreactorcreator.h"
#include "subgine/resource/service/factoryservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct CollisionReactorCreatorService : kgr::single_service<CollisionReactorCreator> {};
using CollisionReactorFactoryService = FactoryService<CollisionReactorFactory>;

} // namespace sbg
