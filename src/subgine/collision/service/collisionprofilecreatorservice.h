#pragma once

#include "../creator/collisionprofilecreator.h"

#include "collisionreactorcreatorservice.h"

#include "subgine/resource/service/factoryservice.h"
#include "subgine/common/kangaru.h"

namespace sbg {

struct CollisionProfileCreatorService : kgr::single_service<CollisionProfileCreator, kgr::autowire> {};
using CollisionProfileFactoryService = FactoryService<CollisionProfileFactory>;

} // namespace sbg
