#pragma once

#include "subgine/common/kangaru.h"
#include "../factory/collisionbodyfactory.h"
#include "collisionprofilecreatorservice.h"

namespace sbg {

struct CollisionBodyFactoryService : kgr::service<CollisionBodyFactory, kgr::autowire> {};

} // namespace sbg
