#pragma once

#include "../reactor/eventreactor.h"

#include "subgine/system/service/deferredschedulerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

template<typename Reactor, typename Enter, typename Exit>
struct EventReactorService : kgr::service<EventReactor<Reactor, Enter, Exit>, kgr::autowire> {};

} // namespace sbg
