#pragma once

#include "../engine/collisionengine.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct CollisionEngineService : kgr::single_service<CollisionEngine> {};

} // namespace sbg
