#pragma once

#include "subgine/entity/entity.h"

#include <string_view>
#include "./utility/reciprocality.h"

namespace sbg {

struct CollisionReactor;

struct CollisionTestInfo {
	struct ReactorInfo {
		Entity entity;
		CollisionReactor* reactor;
	};
	
	[[nodiscard]]
	constexpr auto reverse() const& noexcept -> CollisionTestInfo {
		return CollisionTestInfo{group, second, first, reciprocality};
	}
	
	std::string_view group;
	ReactorInfo first;
	ReactorInfo second;
	test_reciprocality reciprocality;
};

} // namespace sbg
