#include "collisionmodule.h"

#include "subgine/collision.h"
#include "subgine/system.h"
#include "subgine/entity.h"
#include "subgine/resource.h"

namespace sbg {

void CollisionModule::setupMainEngine(MainEngine& mainEngine, CollisionEngine& collisionEngine) const {
	mainEngine.add(collisionEngine);
}

void CollisionModule::setupCollisionEngine(CollisionEngine& collisionEngine) const {
	collisionEngine.addTester<CircleAspect, CircleAspect>(CircleCircleAlgorithm{});
	collisionEngine.addTester<CircleAspect, AlignedBox2DAspect>(CircleAlignedBox2DAlgorithm{});
	collisionEngine.addTester<SphereAspect, SphereAspect>(SphereSphereAlgorithm{});
	collisionEngine.addTester<AlignedBox2DAspect, AlignedBox2DAspect>(AlignedBox2DAlignedBox2DAlgorithm{});
	collisionEngine.addTester<AlignedBox2DAspect, QuadtreeAspect<shape::AlignedBox2D>>(NTreeAlgorithm<AlignedBox2DAlignedBox2DAlgorithm>{});
	collisionEngine.addTester<CircleAspect, QuadtreeAspect<shape::AlignedBox2D>>(NTreeAlgorithm<CircleAlignedBox2DAlgorithm>{});
	collisionEngine.addTester<CircleAspect, QuadtreeAspect<shape::Polygon>>(NTreeAlgorithm<CircleAlignedBox2DAlgorithm>{});
	collisionEngine.addTester<AlignedBox2DAspect, QuadtreeAspect<shape::Polygon>>(NTreeAlgorithm<AlignedBox2DAlignedBox2DAlgorithm>{});
	collisionEngine.addTester<PolygonAspect, QuadtreeAspect<shape::Polygon>>(NTreeAlgorithm<AlignedBox2DAlignedBox2DAlgorithm>{});
}

void CollisionModule::setupComponentCreator(ComponentCreator& componentCreator) const {
	componentCreator.add("collision", [](CollisionBodyFactory factory, Entity entity, Property data) {
		return factory.create(entity, data);
	});
}

void CollisionModule::setupEntityBindingCreator(EntityBindingCreator& factoryBinder) const {
	factoryBinder.add("collision", [](CollisionEngine& engine, Entity entity, Property) {
		engine.add(entity);
	});
}

void CollisionModule::setupCollisionReactorCreator(CollisionReactorCreator& reactorCreator) const {
	reactorCreator.add("destroy", [](DeferredScheduler& scheduler, Entity entity) {
		return [entity, &scheduler](BasicCollisionInfo const&) {
			scheduler.defer([entity]{ Entity{entity}.destroy(); });
		};
	});
}

} // namespace sbg
