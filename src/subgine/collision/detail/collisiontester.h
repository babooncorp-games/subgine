#pragma once

#include "../collisionreactor.h"

#include "subgine/common/types.h"
#include "subgine/entity/entity.h"

#include <vector>
#include <span>

namespace sbg {

struct CollisionTester {
	struct Aspect {
		Aspect(ErasedType const& _data, CollisionReactor& reactor, Entity _entity) noexcept : data{&_data}, entity{_entity}, reactor{&reactor} {}
		
		ErasedType const* data;
		CollisionReactor* reactor;
		Entity entity;
	};
	struct Test {
		Test(std::string _group, Aspect const& _first, Aspect const& _second, test_reciprocality _reciprocality) noexcept;
		
		auto expired() const noexcept -> bool;
		auto related(Entity const& entity) const noexcept -> bool;
		
		Aspect first;
		Aspect second;
		std::string group;
		test_reciprocality reciprocality;
	};
	
	using algorithm_t = std::function<void(std::span<Test const>)>;
	
	CollisionTester(algorithm_t algorithm);
	
	void test() const;
	void addTest(Test test);
	void removeEntities(std::span<Entity> entities);
	void cleanup();
	void clear();
	
private:
	std::vector<Test> _tests;
	algorithm_t _algorithm;
};

} // namespace sbg
