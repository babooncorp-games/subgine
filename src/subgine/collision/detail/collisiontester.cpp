#include "collisiontester.h"

#include <algorithm>
#include <utility>

using namespace sbg;

CollisionTester::CollisionTester(algorithm_t algorithm) : _algorithm{algorithm} {}

void CollisionTester::test() const {
	_algorithm(_tests);
}

void CollisionTester::addTest(Test test) {
	_tests.emplace_back(std::move(test));
}

void CollisionTester::removeEntities(std::span<Entity> entities) {
	_tests.erase(std::remove_if(_tests.begin(), _tests.end(), [&](auto const& test) {
		return test.expired() || std::find_if(entities.begin(), entities.end(), [&](auto const& entity) {
			return test.related(entity);
		}) != entities.end();
	}), _tests.end());
}

void CollisionTester::cleanup() {
	_tests.erase(std::remove_if(_tests.begin(), _tests.end(), [&](auto const& test) {
		return test.expired();
	}), _tests.end());
}

void CollisionTester::clear() {
	_tests.clear();
}

CollisionTester::Test::Test(std::string _group, Aspect const& _first, Aspect const& _second, test_reciprocality _reciprocality) noexcept :
	group{_group},
	first{_first},
	second{_second},
	reciprocality{_reciprocality} {}

auto CollisionTester::Test::expired() const noexcept -> bool {
	return !first.entity || !second.entity;
}

auto CollisionTester::Test::related(Entity const& entity) const noexcept -> bool {
	return first.entity == entity || second.entity == entity;
}
