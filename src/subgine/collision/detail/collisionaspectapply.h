#pragma once

#include "collisionprofile.h"

#include "subgine/entity/entity.h"
#include "subgine/common/traits.h"
#include "subgine/common/concepts.h"
#include "subgine/common/dropping_invoke.h"

#include "subgine/resource/property.h"
#include "subgine/common/kangaru.h"

#include <concepts>

namespace sbg {

struct CollisionReactorCreator;

struct CollisionAspectApply {
	template<typename B> requires(not std::same_as<CollisionProfile, dropping_invoke_result_t<kgr::invoker, B&, Entity, Property>>)
	auto operator()(kgr::invoker invoker, B& builder, Entity entity, Property data) const {
		return CollisionProfile{
			.aspect = dropping_invoke(invoker, builder, entity, data),
			.groups = std::vector<std::string>{data["groups"]},
			.colliding = std::vector<std::string>{data["colliding"]}
		};
	}
	
	template<typename B> requires std::same_as<CollisionProfile, dropping_invoke_result_t<kgr::invoker, B&, Entity, Property>>
	auto operator()(kgr::invoker invoker, B& builder, Entity entity, Property data) const {
		return dropping_invoke(invoker, builder, entity, data);
	}
};

} // namespace sbg
