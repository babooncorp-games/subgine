#pragma once

#include "abstractreactor.h"

namespace sbg {

struct BasicCollisionInfo;

template<typename T, typename InfoType>
struct Reactor : AbstractReactor<InfoType> {
	Reactor(T reactor) : _reactor{std::move(reactor)} {}
	
	void react(CollisionInfo<InfoType> const& info) override {
		_reactor(std::move(info));
	}
	
private:
	T _reactor;
};

template<typename T>
struct Reactor<T, BasicCollisionInfo> : AbstractReactor<BasicCollisionInfo> {
	Reactor(T reactor) : _reactor{std::move(reactor)} {}
	
	void react(BasicCollisionInfo const& info) override {
		_reactor(info);
	}
	
private:
	T _reactor;
};

} // namespace sbg
