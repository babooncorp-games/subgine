#pragma once

#include "../collisioninfo.h"

namespace sbg {

struct BasicCollisionInfo;

template<typename InfoType>
struct AbstractReactor {
	virtual ~AbstractReactor() = default;
	virtual void react(const CollisionInfo<InfoType>& info) = 0;
};

template<>
struct AbstractReactor<BasicCollisionInfo> {
	virtual ~AbstractReactor() = default;
	virtual void react(const BasicCollisionInfo&) = 0;
};

} // namespace sbg
