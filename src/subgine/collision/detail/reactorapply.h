#pragma once

#include "../collisionreactor.h"
#include "../utility/traits.h"
#include "subgine/common/kangaru.h"
#include "subgine/common/dropping_invoke.h"

namespace sbg {

struct ReactorApply {
	template<typename B, typename... Args> requires reactor<dropping_invoke_result_t<kgr::invoker, B&, Args...>>
	void operator()(kgr::invoker invoker, B& builder, std::string group, CollisionReactor& reactor, Args&&... args) const {
		reactor.addReactor(std::move(group), dropping_invoke(invoker, builder, std::forward<Args>(args)...));
	}
};

} // namespace sbg
