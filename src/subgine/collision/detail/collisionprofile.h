#pragma once

#include "../collisionreactor.h"

#include "subgine/common/erasedtype.h"

#include <string>
#include <vector>

namespace sbg {

struct CollisionProfile {
	ErasedType aspect;
	std::vector<std::string> groups;
	std::vector<std::string> colliding;
	CollisionReactor reactors = {};
};

} // namespace sbg
