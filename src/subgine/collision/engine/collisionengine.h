#pragma once

#include "../detail/collisionprofile.h"
#include "../detail/collisiontester.h"
#include "../utility/traits.h"
#include "../collisiontestinfo.h"
#include "../collisioninfo.h"

#include "subgine/common/pairhash.h"
#include "subgine/common/erasedtype.h"
#include "subgine/common/type_id.h"
#include "subgine/common/types.h"
#include "subgine/system/time.h"
#include "subgine/common/ownershiptoken.h"

#include <unordered_map>
#include <optional>
#include <concepts>

namespace sbg {

struct CollisionEngineService;

struct CollisionEngine {
	CollisionEngine() = default;
	CollisionEngine(CollisionEngine&&) = default;
	auto operator=(CollisionEngine&&) -> CollisionEngine& = default;
	CollisionEngine(CollisionEngine const&) = delete;
	auto operator=(CollisionEngine const&) -> CollisionEngine& = delete;
	
	void execute(Time time);
	void owner(OwnershipToken owner) noexcept;
	
	template<typename F, typename S>
	void addTester(std::invocable<F, S> auto tester) {
		_testersers.insert_or_assign(
			std::pair{type_id<F>, type_id<S>},
			CollisionTester{
				makeTesterLoop<F, S, reversal::normal>(tester)
			}
		);
		
		_testersers.insert_or_assign(
			std::pair{type_id<S>, type_id<F>},
			CollisionTester{
				makeTesterLoop<S, F, reversal::reversed>(tester)
			}
		);
	}
	
	void add(Entity entity);
	void remove(Entity entity);
	void refresh(Entity entity);
	
private:
	using Tester = std::function<void(const ErasedType&, const ErasedType&, CollisionTestInfo)>;
	using TesterMap = std::unordered_map<std::pair<type_id_t, type_id_t>, Tester>;
	
	void cleanup();
	void update();
	
	enum struct reversal : bool {
		normal = true, reversed = false
	};
	
	template<typename F, typename S, reversal test_reversal>
	static constexpr auto makeTester(auto algorithm) {
		return [algorithm](ErasedType const& first, ErasedType const& second, auto info) {
			auto const test = [&algorithm](auto const& first, auto const& second) {
				if constexpr (test_reversal == reversal::normal) {
					return algorithm(first, second);
				} else {
					auto const result = algorithm(second, first);
					return result ? std::optional{result->reverse()} : result;
				}
			};
			
			if (auto const result = test(first.as<F>(), second.as<S>())) {
				info.first.reactor->react(CollisionInfo{info, *result});
				
				if (info.reciprocality == test_reciprocality::reciprocal) {
					info.second.reactor->react(CollisionInfo{info.reverse(), result->reverse()});
				}
			}
		};
	}
	
	template<typename F, typename S, reversal test_reversal, typename T>
	static constexpr auto makeTesterLoop(T tester) {
		return [algorithm = makeTester<F, S, test_reversal>(tester)](std::span<CollisionTester::Test const> tests) {
			for (auto& test : tests) {
				if (test.expired()) continue;
				algorithm(
					*test.first.data,
					*test.second.data,
					CollisionTestInfo{
						test.group,
						{test.first.entity, test.first.reactor},
						{test.second.entity, test.second.reactor},
						test.reciprocality
					}
				);
			}
		};
	}
	
	bool _shouldClean = false;
	std::unordered_map<std::pair<type_id_t, type_id_t>, CollisionTester> _testersers;
	std::vector<Entity> _entities;
	std::vector<Entity> _addedEntities;
	std::vector<Entity> _removedEntities;
	
	OwnershipToken _owner;
	std::unordered_multimap<std::string, std::pair<Entity, CollisionProfile*>> _groups;
	
	friend auto service_map(CollisionEngine const&) -> CollisionEngineService;
};

} // namespace sbg
