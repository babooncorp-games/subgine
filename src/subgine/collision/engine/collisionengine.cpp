#include "collisionengine.h"

#include "../component/collisionbody.h"

#include "subgine/common.h"
#include "subgine/entity.h"

#include <algorithm>
#include <set>
#include <unordered_set>
#include <tuple>

namespace sbg {

void CollisionEngine::execute(Time time) {
	if (!_addedEntities.empty() || !_removedEntities.empty()) {
		update();
	}
	
	cleanup();
	
	for (auto& tester : _testersers) {
		tester.second.test();
	}
}

void CollisionEngine::owner(OwnershipToken owner) noexcept {
	_owner = std::move(owner);
}

void CollisionEngine::add(Entity entity) {
	_addedEntities.emplace_back(entity);
}

void CollisionEngine::remove(Entity entity) {
	_removedEntities.emplace_back(entity);
	_addedEntities.erase(std::remove(_addedEntities.begin(), _addedEntities.end(), entity), _addedEntities.end());
}

void CollisionEngine::refresh(Entity entity) {
	remove(entity);
	add(entity);
}

void CollisionEngine::cleanup() {
	_entities.erase(std::remove_if(_entities.begin(), _entities.end(), [&](auto const& entity) {
		return !entity || std::find(_removedEntities.begin(), _removedEntities.end(), entity) != _removedEntities.end();;
	}), _entities.end());
	
	for (auto& tester : _testersers) {
		tester.second.removeEntities(_removedEntities);
		tester.second.cleanup();
	}
	
	erase_if(_groups, [&](auto const& group) {
		return !group.second.first || std::find(_removedEntities.begin(), _removedEntities.end(), group.second.first) != _removedEntities.end();
	});
	
	_removedEntities.clear();
	_shouldClean = false;
}

void CollisionEngine::update() {
	cleanup();
	
	if (!_addedEntities.empty()) {
		std::vector<std::pair<Entity, CollisionProfile*>> profiles;
		
		_entities.reserve(_addedEntities.size() + _entities.size());
		
		for (auto const entity : _addedEntities) {
			if (entity) {
				_entities.emplace_back(entity);
				
				auto& collisionBody = entity.component<CollisionBody>();
				for (auto& [name, profile] : collisionBody.profiles) {
					for (auto const& group : profile.groups) {
						_groups.emplace(group, std::pair{entity, &profile});
					}
				}
			}
		}
		
		_addedEntities.clear();
		
		for (auto const entity : _entities) {
			auto& collisionBody = entity.component<CollisionBody>();
			for (auto& [name, bodyProfiles] : collisionBody.profiles) {
				profiles.emplace_back(entity, &bodyProfiles);
			}
		}
		
		std::unordered_set<std::tuple<std::string_view, CollisionProfile*, CollisionProfile*>> reciprocals;
		
		for (auto& tester : _testersers) {
			tester.second.clear();
		}
		
		auto is_reciprocal = [](CollisionProfile& profile1, CollisionProfile& profile2, std::string_view group) {
			return
				std::find(profile1.groups.begin(), profile1.groups.end(), group) != profile1.groups.end() &&
				std::find(profile2.colliding.begin(), profile2.colliding.end(), group) != profile2.colliding.end();
		};
		
		for (auto const& [entity1, profile1] : profiles) {
			for (auto const& group : profile1->colliding) {
				for (auto [begin, end] = _groups.equal_range(group); begin != end ; ++begin) {
					auto [entity2, profile2] = begin->second;
					
					if (entity1 == entity2) continue;
					
					auto const it = _testersers.find(std::pair{profile1->aspect.type(), profile2->aspect.type()});
					
					if (it != _testersers.end()) {
						auto const reciprocal = is_reciprocal(*profile1, *profile2, group);
						
						if (!reciprocal || reciprocals.find(std::tuple{group, profile1, profile2}) == reciprocals.end()) {
							it->second.addTest(CollisionTester::Test{
								group,
								CollisionTester::Aspect{profile1->aspect, profile1->reactors, entity1},
								CollisionTester::Aspect{profile2->aspect, profile2->reactors, entity2},
								reciprocal ? test_reciprocality::reciprocal : test_reciprocality::nonreciprocal
							});
							
							if (reciprocal) {
								reciprocals.emplace(group, profile2, profile1);
							}
						}
					}
				}
			}
		}
	}
}

} // namespace sbg
