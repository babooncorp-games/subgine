#pragma once

#include "subgine/common/traits.h"
#include "subgine/common/function_traits.h"
#include "subgine/common/concepts.h"

namespace sbg {

template<object>
struct CollisionInfo;
struct CollisionTestInfo;
struct BasicCollisionInfo;

template<typename T>
concept generic_reactor = similar_to<BasicCollisionInfo, function_argument_t<0, T>>;

template<typename T>
concept specific_reactor = is_specialisation_of_v<CollisionInfo, std::decay_t<function_argument_t<0, T>>>;

template<typename T, typename I>
concept specific_reactor_of = similar_to<CollisionInfo<I>, function_argument_t<0, T>>;

template<typename T>
concept reactor =
	   generic_reactor<T>
	or specific_reactor<T>;

template<reactor>
struct reactor_info_type {};

template<specific_reactor T>
struct reactor_info_type<T> {
	using type = typename std::decay_t<function_argument_t<0, T>>::InfoType;
};

template<generic_reactor T>
struct reactor_info_type<T> {
	using type = BasicCollisionInfo;
};

template<typename T>
using reactor_info_type_t = typename reactor_info_type<T>::type;

} // namespace sbg
