#pragma once

namespace sbg {

enum struct test_reciprocality : bool {
	nonreciprocal = false, reciprocal = true
};

}