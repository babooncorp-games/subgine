#include "collisionprofilecreator.h"

#include "../creator/collisionreactorcreator.h"

using namespace sbg;

CollisionProfileCreator::CollisionProfileCreator(CollisionReactorCreator& reactorCreator) noexcept : _reactorCreator{reactorCreator} {}

auto CollisionProfileCreator::create(kgr::invoker invoker, std::string_view name, Entity entity, Property data) const -> CollisionProfile {
	auto profile = build(invoker, name, entity, data);
	
	for (auto reactor : data["reactors"]) {
		_reactorCreator->create(invoker, reactor["type"], std::string{reactor["group"]}, profile.reactors, entity, reactor["data"]);
	}
	
	return profile;
}
