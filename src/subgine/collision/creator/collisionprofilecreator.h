#pragma once

#include "../detail/collisionprofile.h"
#include "../detail/collisionaspectapply.h"

#include "subgine/common/types.h"
#include "subgine/common/memberreference.h"
#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"
#include "subgine/entity/entity.h"

#include "subgine/resource/property.h"

namespace sbg {

struct CollisionProfileCreatorService;
struct CollisionReactorCreator;

struct CollisionProfileCreator : Creator<DefaultBuilder<CollisionProfile>, CollisionAspectApply> {
	explicit CollisionProfileCreator(CollisionReactorCreator& reactorCreator) noexcept;
	auto create(kgr::invoker invoker, std::string_view name, Entity entity, Property data) const -> CollisionProfile;
	
private:
	MemberReference<CollisionReactorCreator> _reactorCreator;
	
	friend auto service_map(CollisionProfileCreator const&) -> CollisionProfileCreatorService;
};

using CollisionProfileFactory = Factory<CollisionProfileCreator>;

} // namespace sbg
