#pragma once

#include "../collisioninfo.h"
#include "../detail/reactorapply.h"

#include "subgine/resource/creator.h"
#include "subgine/resource/factory.h"

namespace sbg {

struct CollisionReactorCreatorService;

struct CollisionReactorCreator : Creator<void(std::string, CollisionReactor&, Entity, Property), ReactorApply> {
	using Creator::create;
	
	friend auto service_map(const CollisionReactorCreator&) -> CollisionReactorCreatorService;
};

using CollisionReactorFactory = Factory<CollisionReactorCreator>;

} // namespace sbg
