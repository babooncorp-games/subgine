#pragma once

namespace sbg {

struct CollisionModuleService;

struct CollisionEngine;
struct MainEngine;
struct ComponentCreator;
struct EntityBindingCreator;
struct CollisionReactorCreator;

struct CollisionModule {
	void setupMainEngine(MainEngine& mainEngine, CollisionEngine& collisionEngine) const;
	void setupCollisionEngine(CollisionEngine& collisionEngine) const;
	void setupComponentCreator(ComponentCreator& componentCreator) const;
	void setupEntityBindingCreator(EntityBindingCreator& factoryBinder) const;
	void setupCollisionReactorCreator(CollisionReactorCreator& reactorCreator) const;
	
	friend auto service_map(CollisionModule const&) -> CollisionModuleService;
};

} // namespace sbg
