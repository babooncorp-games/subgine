#pragma once

#include "subgine/shape/alignedbox.h"
#include "subgine/shape/nsphere.h"
#include "subgine/shape/polytope.h"

namespace sbg {

template<dim_t n>
inline auto aligned_box_shape(shape::AlignedBox<n> const& box) noexcept -> shape::AlignedBox<n> {
	return box;
}

template<dim_t n>
inline auto nsphere_shape(shape::AlignedBox<n> const& box) noexcept -> shape::NSphere<n> {
	return shape::NSphere<n>{
		.radius = (box.top - box.bottom).length(),
		.position = (box.top + box.bottom) / 2,
	};
}

template<dim_t n>
inline auto aligned_box_shape(shape::NSphere<n> const& sphere) noexcept -> shape::AlignedBox<n> {
	return shape::AlignedBox{
		.top = sphere.position + Vector<n, double>{sphere.radius} * -1,
		.bottom = sphere.position + Vector<n, double>{sphere.radius},
	};
}

template<dim_t n>
inline auto nsphere_shape(shape::NSphere<n> const& sphere) noexcept -> shape::NSphere<n> {
	return sphere;
}

template<dim_t n>
inline auto aligned_box_shape(shape::Polytope<n> const& polytope) noexcept -> shape::AlignedBox<n> {
	return shape::AlignedBox<n>{
		.top = polytope.top + polytope.position,
		.bottom = polytope.bottom + polytope.position,
	};
}

template<dim_t n>
inline auto nsphere_shape(shape::Polytope<n> const& polytope) noexcept -> shape::NSphere<n> {
	return shape::NSphere<n>{
		.radius = polytope._nsphere_radius
	};
}

} // namespace sbg
