#include "multimanifold.h"

using namespace sbg;

template<dim_t n>
auto MultiManifold<n>::reverse() const -> MultiManifold {
	auto result = *this;
	
	for (auto& manifold : result) {
		manifold = manifold.reverse();
	}
	
	return result;
}

namespace sbg {
	template struct MultiManifold<2>;
	template struct MultiManifold<3>;
}
