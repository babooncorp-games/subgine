#include "manifold.h"

namespace sbg {

template<int n, std::size_t... S>
Manifold<n> Manifold<n, std::index_sequence<S...>>::reverse() const {
	return Manifold<n>{contacts[S].reverse()...};
}

template struct Manifold<2>;
template struct Manifold<3>;

} // namespace sbg
