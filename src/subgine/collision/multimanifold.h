#pragma once

#include "manifold.h"

#include <vector>

namespace sbg {

template<dim_t n> // There's no problem with that, move along.
struct MultiManifold : std::vector<Manifold<n>> {
	[[nodiscard]]
	auto reverse() const -> MultiManifold;
};

using MultiManifold2D = MultiManifold<2>;
using MultiManifold3D = MultiManifold<3>;

} // namespace sbg
