#pragma once

#include "basiccollisioninfo.h"
#include "collisiontestinfo.h"

#include "subgine/entity/entity.h"
#include "subgine/common/types.h"
#include "subgine/common/concepts.h"

namespace sbg {

template<object T>
struct CollisionInfo : BasicCollisionInfo, T {
	using InfoType = T;
	
	constexpr CollisionInfo(CollisionTestInfo const& basic, T info) noexcept :
		BasicCollisionInfo{basic.group, basic.second.entity}, T{std::move(info)} {}

	constexpr CollisionInfo(BasicCollisionInfo const& basic, T info) noexcept :
		BasicCollisionInfo{basic}, T{std::move(info)} {}
};

} // namespace sbg
