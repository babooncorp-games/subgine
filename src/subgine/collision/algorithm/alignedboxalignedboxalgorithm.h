#pragma once

#include "../manifold.h"
#include "../utility/traits.h"
#include "../shapeinterface.h"

#include "subgine/common/math.h"
#include <optional>

namespace sbg {

template<dim_t n>
struct AlignedBoxContainedAlgorithm {
	auto operator()(auto const& a, auto const& b) const -> bool {
 		return apply_sequence<n>([&](auto... s) {
			auto boxA = aligned_box_shape(a);
			auto boxB = aligned_box_shape(b);
			
			// If box A is contained in box B
			return ((get<s>(boxA.top) <= get<s>(boxB.top) && get<s>(boxA.bottom) >= get<s>(boxB.bottom)) && ...);
 		});
	}
};

using AlignedBox2DContainedAlgorithm = AlignedBoxContainedAlgorithm<2>;
using AlignedBox3DContainedAlgorithm = AlignedBoxContainedAlgorithm<3>;

template<dim_t n>
struct AlignedBoxIntersectAlgorithm {
	auto operator()(const auto& a, const auto& b) const -> bool {
		return apply_sequence<n>([&](auto... s) {
			auto const boxA = aligned_box_shape(a);
			auto const boxB = aligned_box_shape(b);
			
			return ((get<s>(boxA.bottom) > get<s>(boxB.top) and get<s>(boxA.top) < get<s>(boxB.bottom)) && ...);
		});
	}
};

using AlignedBox2DIntersectAlgorithm = AlignedBoxIntersectAlgorithm<2>;
using AlignedBox3DIntersectAlgorithm = AlignedBoxIntersectAlgorithm<3>;

template<dim_t n>
struct AlignedBoxAlignedBoxAlgorithm {
	auto operator()(auto const& a, auto const& b) const {
		return apply_sequence<n>([&](auto... s) -> std::optional<Manifold<n>> {
			auto const boxA = aligned_box_shape(a);
			auto const boxB = aligned_box_shape(b);
			
			if (not AlignedBoxIntersectAlgorithm<n>{}(boxA, boxB)) {
				return {};
			}
			
			auto manifold = Manifold<n>{};
			
			auto const extentA = (boxA.bottom - boxA.top) / 2;
			auto const extentB = (boxB.bottom - boxB.top) / 2;
			auto const relative = (boxB.bottom + boxB.top) / 2 - (boxA.bottom + boxA.top) / 2;
			auto const overlap = sbg::Vector{(get<s>(extentA) + get<s>(extentB) - std::abs(get<s>(relative)))...};
			
			if (((get<s>(overlap) > 0) && ...)) {
				auto const minimum = std::min({ get<s>(overlap)... });
				
				manifold.contacts[0].penetration = {
					(get<s>(overlap) == minimum ? get<s>(overlap) * (get<s>(relative) > 0 ? 1 : -1) : 0)...
				};
			}
			
			return manifold;
		});
	}
};

using AlignedBox2DAlignedBox2DAlgorithm = AlignedBoxAlignedBoxAlgorithm<2>;
using AlignedBox3DAlignedBox3DAlgorithm = AlignedBoxAlignedBoxAlgorithm<3>;

} // namespace sbg

