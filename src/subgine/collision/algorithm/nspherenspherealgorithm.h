#pragma once

#include "../manifold.h"
#include "../aspect/nsphereaspect.h"
#include "../shapeinterface.h"

#include <optional>
#include <algorithm>

namespace sbg {

template<int>
struct NSphereAspect;

template<int n>
struct NSphereNSphereAlgorithm {
	auto operator()(auto const& a, auto const& b) const -> std::optional<Manifold<n>> {
		auto const spherea = nsphere_shape(a);
		auto const sphereb = nsphere_shape(b);
		
		if (spherea.radius + sphereb.radius > sphereb.position - spherea.position) {
			return {};
		}
		
		auto manifold = Manifold<n>{};
		auto contact = Contact<n>{};
		
		contact.penetration = sphereb.position - spherea.position;
		contact.penetration.applyLength(spherea.radius + sphereb.radius - contact.penetration.length());
		contact.position = (sphereb.position - spherea.position).length(spherea.radius) + spherea.position;
		
		manifold.contacts[0] = contact;
		
		return manifold;
	}
};

using CircleCircleAlgorithm = NSphereNSphereAlgorithm<2>;
using SphereSphereAlgorithm = NSphereNSphereAlgorithm<3>;

extern template struct NSphereNSphereAlgorithm<2>;
extern template struct NSphereNSphereAlgorithm<3>;

} // namespace sbg
