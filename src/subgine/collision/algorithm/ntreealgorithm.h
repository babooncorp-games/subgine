#pragma once

#include "alignedboxalignedboxalgorithm.h"
#include "../aspect/ntreeaspect.h"
#include "../multimanifold.h"
#include "../shapeinterface.h"

#include "subgine/common/types.h"

#include <optional>
#include <algorithm>
#include <numeric>
#include <ranges>

#include "subgine/common/define.h"

namespace sbg {

/**
 * NTreeAlgorithm is a function object that verify collision
 * between an object and all aspects contained in an NTree.
 * 
 * @tparam T: The algorithm to test collisions between the aspects in the tree and the aspect to check
 */
template<typename T>
struct NTreeAlgorithm {
	template<dim_t n, typename Shape>
	auto operator()(auto const& object, NTreeAspect<n, Shape> const& tree) const -> std::optional<MultiManifold<n>> {
		auto const object_box = aligned_box_shape(object);
		if (!AlignedBoxIntersectAlgorithm<n>{}(tree, object_box)) return {};
		
		auto const nearObjects = tree.near(object_box);
		auto manifolds = MultiManifold<n>{};
		manifolds.reserve(nearObjects.size());
		
		std::ranges::copy(
			  nearObjects
			| std::views::transform([&](auto const& near) EXPR(algorithm(object, near)))
			| std::views::filter([](auto const& manifold) EXPR(manifold.has_value()))
			| std::views::transform([](auto const& manifold) { return *manifold; }),
			std::back_inserter(manifolds)
		);
		
		if (manifolds.empty()) {
			return {};
		} else {
			return manifolds;
		}
	}
	
	NO_UNIQUE_ADDRESS
	T algorithm = {};
};

} // namespace sbg

#include "subgine/common/undef.h"
