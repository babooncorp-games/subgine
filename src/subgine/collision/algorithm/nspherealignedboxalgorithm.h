#pragma once

#include "../manifold.h"
#include "../utility/traits.h"
#include "../shapeinterface.h"

#include <algorithm>
#include <optional>
#include <cmath>

namespace sbg {

template<int n>
struct NSphereAlignedBoxAlgorithm {
	template<typename T1, typename T2>
	auto operator()(const T1& a, const T2& b) const {
		auto const sphere = nsphere_shape(a);
		auto const box = aligned_box_shape(b);

		return apply_sequence<n>([&](auto... s) -> std::optional<Manifold<n>> {
			auto const closestPoint = sphere.position - Vector{std::clamp(get<s>(sphere.position), get<s>(box.top), get<s>(box.bottom))...};
			if (!(closestPoint < sphere.radius)) {
				return {};
			}

			bool contained = false;
			auto distance = Vector{std::clamp(get<s>(sphere.position), get<s>(box.top), get<s>(box.bottom))...};

			if (distance == sphere.position) {
				contained = true;
				auto center = ((box.top + box.bottom) / 2.);
				auto extent = (((box.bottom - center) - (box.top - center)) / 2.);
				auto relative = center - sphere.position;
				auto closest = std::max({std::abs(get<s>(relative))...});

				((get<s>(distance) = closest == std::abs(get<s>(relative)) ? get<s>(center) - get<s>(extent) : get<s>(distance)), ...);
			}

			distance = sphere.position - distance;
			auto penetration = distance.length(distance.length() - sphere.radius);

			return Manifold<n>{{(contained ? -1 : 1) * penetration, (-distance.unit() * sphere.radius) + sphere.position}};
		});
	}
};

using CircleAlignedBox2DAlgorithm = NSphereAlignedBoxAlgorithm<2>;
using SphereAlignedBox3DAlgorithm = NSphereAlignedBoxAlgorithm<3>;

} // namespace sbg
