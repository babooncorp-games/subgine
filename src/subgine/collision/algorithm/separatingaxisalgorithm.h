#pragma once

#include "../manifold.h"
#include "../utility/traits.h"

#include <vector>
#include <optional>

namespace sbg {

struct SeparatingAxisAlgorithm {
	template<typename F, typename S, enable_if_i<is_polytope<F> && is_nsphere<S>> = 0>
	std::optional<Manifold2D> operator()(const F& f, const S& s) const {
		if constexpr (is_polytope<F> && is_nsphere<S>) {
// 			Manifold<2> manifold;
// 			Vector2d center = s.position() - f.position();
// 			
// 			auto separation = -std::numeric_limits<double>::infinity();
// 			Vector2d bestNormal;
// 			Vector2d bestVertex;
// 			Vector2d bastLastVertex;
// 			
// 			{
// 				auto last = f.vertices().back();
// 				for (auto&& vertex : f.vertices()) {
// 					auto normal = vertex.normal(last);
// 					auto separatingAxis = normal.dot(center - vertex);
// 				
// 					if(separatingAxis > s.radius)
// 						return manifold;
// 				
// 					if(separatingAxis > separation) {
// 						separation = separatingAxis;
// 						bestNormal = normal;
// 						bestVertex = vertex;
// 						bastLastVertex = last;
// 					}
// 					
// 					last = vertex;
// 				}
// 			}
// 			
// 			Vector2d v1 = bestVertex;
// 			Vector2d v2 = bastLastVertex;
// 			
// 			if(separation < std::numeric_limits<double>::epsilon()) {
// //	 			m->normal = -(B->u * bestNormal);
// 				manifold.contacts[0].penetration = bestNormal.length(s.radius);
// 				manifold.contacts[0].position = (bestNormal * s.radius) + s.position();
// 				return manifold;
// 			}
// 			
// 			double dot1 = (center - v1).dot(v2 - v1);
// 			double dot2 = (center - v2).dot(v1 - v2);
// 			auto penetration = s.radius - separation;
// 			
// 			if (dot1 <= 0.0f) {
// 				if(!(center - v1 > s.radius)) {
// 					Vector2d n = v1 - center;
// 		// 			n = B->u * n;
// 					manifold.contacts[0].penetration = penetration * n.unit();
// 		// 			v1 = B->u * v1 + b->position;
// 					manifold.contacts[0].position = v1 + f.position();
// 				}
// 			} else if (dot2 <= 0.0f) {
// 				if(!(center - v2 > s.radius)) {
// 					Vector2d n = v2 - center;
// 		// 			v2 = B->u * v2 + b->position;
// 					manifold.contacts[0].position = v2 + f.position();
// 		// 			n = B->u * n;
// 					manifold.contacts[0].penetration = penetration * n.unit();
// 				}
// 			} else {
// 				if(!((center - v1).dot(bestNormal) > s.radius)) {
// 		// 			n = B->u * n;
// 					manifold.contacts[0].penetration =  (bestNormal * s.radius) + s.position();
// 				}
// 			}
// 			
// 			return manifold;
// 			
// 			SeparatingAxisTrait<F> first{f};
// 			SeparatingAxisTrait<S> second{s};
// 			
// 			auto&& normals = first.normals();
// 			normals.emplace_back(first.nearest(s.position()).unit());
// 			
// 			Vector2d overlap;
// 			
// 			for (auto&& normal : normals) {
// 				auto projectionFirst = first.projection(normal);
// 				auto projectionSecond = second.projection(normal);
// 				
// 
// 				if (projectionFirst.second < projectionSecond.first || projectionFirst.first > projectionSecond.second) {
// 					return {};
// 				} else {
// 					if (overlap.null() || overlap > projectionFirst.second - projectionSecond.first) {
// 						overlap = (projectionFirst.second - projectionSecond.first) * normal;
// 					}
// 				}
// 			}
// 			
// 			
// 			Manifold2D manifold;
// 			
// 			manifold.contacts[0].penetration = overlap;
// 			manifold.contacts[0].position = (-overlap.unit() * s.radius) + s.position();
// 			
// 			return manifold;
		}
		
		if constexpr (is_nsphere<F> && is_polytope<S>) {
			// reverse above
		}
		
		return {};
	}
};

} // namespace sbg
