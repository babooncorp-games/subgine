#pragma once

#include "collision/service.h"

#include "../../algorithm/alignedboxalignedboxalgorithm.h"
#include "../../algorithm/nspherealignedboxalgorithm.h"
#include "../../algorithm/nspherenspherealgorithm.h"
// #include "../../algorithm/separatingaxisalgorithm.h"
#include "../../algorithm/ntreealgorithm.h"

#include "../../aspect/alignedboxaspect.h"
#include "../../aspect/nsphereaspect.h"
#include "../../aspect/ntreeaspect.h"
#include "../../aspect/polytopeaspect.h"

#include "../../component/collisionbody.h"

#include "../../creator/collisionprofilecreator.h"
#include "../../creator/collisionreactorcreator.h"

#include "../../engine/collisionengine.h"

#include "../../factory/collisionbodyfactory.h"

#include "../../reactor/eventreactor.h"

#include "../utility/reciprocality.h"
#include "../utility/traits.h"

#include "../../collisionreactor.h"
#include "../../collisioninfo.h"
#include "../../collisiontestinfo.h"
#include "../../contact.h"
#include "../../indexiterator.h"
#include "../../indexviewrange.h"
#include "../../manifold.h"
#include "../../aspect/ntreeaspect.h"
