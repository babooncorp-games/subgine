#pragma once

#include "../detail/collisionprofile.h"

#include "subgine/common/emilib/hash_map.hpp"

#include <vector>

namespace sbg {

struct CollisionBody {
	using Profiles = emilib::hash_map<std::string, CollisionProfile>;
	Profiles profiles;
};

} // namespace sbg
