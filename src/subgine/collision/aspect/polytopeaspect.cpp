#include "polytopeaspect.h"

#include "subgine/log.h"

#include <cmath>
#include <algorithm>

namespace sbg {

template<dim_t n>
PolytopeAspect<n>::PolytopeAspect(std::vector<Vector<n, double>> _vertices, Provider<Vector<n, double>> _position, Provider<Vector<freedom(n), double>> _orientation) : 
	shape::Polytope<n>{std::move(_vertices)},
	position{std::move(_position)},
	orientation{std::move(_orientation)}
{
	updateCache();
}

template<dim_t n>
PolytopeAspect<n>::PolytopeAspect(shape::Polytope<n> shape, Provider<Vector<n, double>> _position, Provider<Vector<freedom(n), double>> _orientation) : 
	shape::Polytope<n>{std::move(shape)},
	position{std::move(_position)},
	orientation{std::move(_orientation)}
{
	updateCache();
}

template<dim_t n>
void PolytopeAspect<n>::vertices(std::vector<Vector<n, double>> _vertices) {
	shape::Polytope<n>::vertices = std::move(_vertices);
	updateCache();
}

template<dim_t n>
const std::vector<Vector<n, double>> & PolytopeAspect<n>::vertices() const {
	return shape::Polytope<n>::vertices;
}

template<dim_t n>
const std::vector<Vector<n, double>> & PolytopeAspect<n>::normals() const {
	return _normals;
}

template<dim_t n>
void PolytopeAspect<n>::updateCache() {
	auto box = shape::AlignedBox{Vector<n, double>{std::numeric_limits<double>::infinity()}, Vector<n, double>{-std::numeric_limits<double>::infinity()}};
	auto nsphere_radius = double{};
	
	apply_sequence<n>([&](auto... s) {
		for (auto const& vertex : vertices()) {
			box.top = {std::min(get<s>(vertex), get<s>(box.top))...};
			box.bottom = {std::max(get<s>(vertex), get<s>(box.bottom))...};
			nsphere_radius = std::max(vertex.length(), nsphere_radius);
		}
	});
	
	if (!vertices().empty()) {
		_normals.clear();
		auto previous = vertices().back();
		for (auto const& current : vertices()) {
			_normals.emplace_back(current.normal(previous).unit());
			previous = current;
		}
	}
	
	_nsphere_radius = nsphere_radius;
	_box = box;
}

template struct PolytopeAspect<2>;
template struct PolytopeAspect<3>;

} // namespace sbg
