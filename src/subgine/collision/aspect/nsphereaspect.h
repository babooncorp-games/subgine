#pragma once

#include "subgine/common/traits.h"
#include "subgine/shape/nsphere.h"
#include "subgine/shape/alignedbox.h"
#include "subgine/provider/provider.h"
#include "subgine/vector/vector.h"

namespace sbg {

template<dim_t n>
struct NSphereAspect {
	double radius;
	Provider<Vector<n, double>> position;
	
	friend auto aligned_box_shape(NSphereAspect const& aspect) -> shape::AlignedBox<n> {
		auto const position = aspect.position();
		return shape::AlignedBox{
			Vector<n, double>{-1 * aspect.radius} + position,
			Vector<n, double>{aspect.radius} + position
		};
	}
	
	friend auto nsphere_shape(NSphereAspect const& sphere) noexcept -> shape::NSphere<n> {
		return shape::NSphere<n>{
			.radius = sphere.radius,
			.position = sphere.position(),
		};
	}
};

using CircleAspect = NSphereAspect<2>;
using SphereAspect = NSphereAspect<3>;

} // namespace sbg
