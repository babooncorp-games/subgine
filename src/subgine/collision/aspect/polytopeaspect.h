#pragma once

#include "subgine/shape/polytope.h"
#include "subgine/shape/alignedbox.h"
#include "subgine/shape/nsphere.h"
#include "subgine/provider/provider.h"

namespace sbg {

template<dim_t n>
struct PolytopeAspect : private shape::Polytope<n> {
	explicit PolytopeAspect(std::vector<Vector<n, double>> _vertices, Provider<Vector<n, double>> _position, Provider<Vector<freedom(n), double>> _orientation);
	explicit PolytopeAspect(shape::Polytope<n> shape, Provider<Vector<n, double>> _position, Provider<Vector<freedom(n), double>> _orientation);
	
	const std::vector<Vector<n, double>>& vertices() const;
	void vertices(std::vector<Vector<n, double>> _vertices);
	
	const std::vector<Vector<n, double>>& normals() const;
	
	Provider<Vector<n, double>> position;
	Provider<Vector<freedom(n), double>> orientation;
	
	friend auto aligned_box_shape(PolytopeAspect aspect) -> shape::AlignedBox<n> {
		auto const position = aspect.position();
		return shape::AlignedBox<n>{
			.top = aspect._box.top + position,
			.bottom = aspect._box.bottom + position,
		};
	}
	
	friend auto nsphere_shape(PolytopeAspect aspect) -> shape::NSphere<n> {
		return shape::NSphere<n>{
			.radius = aspect._nsphere_radius
		};
	}
	
private:
	double _nsphere_radius;
	shape::AlignedBox<n> _box;
	void updateCache();
	
	std::vector<Vector<n, double>> _normals;
};

extern template struct PolytopeAspect<2>;
extern template struct PolytopeAspect<3>;

using PolygonAspect = PolytopeAspect<2>;
using PolyhedronAspect = PolytopeAspect<3>;

} // namespace sbg
