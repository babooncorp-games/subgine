#pragma once

#include "subgine/common/traits.h"
#include "subgine/provider/provider.h"
#include "subgine/shape/nsphere.h"
#include "subgine/shape/alignedbox.h"

namespace sbg {

template<dim_t n>
struct AlignedBoxAspect {
	shape::AlignedBox<n> box;
	Provider<Vector<n, double>> position;
	
	friend auto aligned_box_shape(AlignedBoxAspect const& aspect) -> shape::AlignedBox<n> {
		auto const position = aspect.position();
		return shape::AlignedBox<n>{
			.top = aspect.box.top + position,
			.bottom = aspect.box.bottom + position,
		};
	}
	
	friend auto nsphere_shape(AlignedBoxAspect const& aspect) -> shape::NSphere<n> {
		return nsphere_shape(aligned_box_shape(aspect));
	}
};

template<typename T, dim_t n> requires(std::invoke_result_t<T>::size == n)
AlignedBoxAspect(shape::AlignedBox<n>, T) -> AlignedBoxAspect<n>;

template<dim_t n>
AlignedBoxAspect(shape::AlignedBox<n>, sbg::Vector<n, double>) -> AlignedBoxAspect<n>;

using AlignedBox2DAspect = AlignedBoxAspect<2>;
using AlignedBox3DAspect = AlignedBoxAspect<3>;

} // namespace sbg
