#pragma once

#include "../algorithm/alignedboxalignedboxalgorithm.h"
#include "../indexviewrange.h"

#include "subgine/shape/alignedbox.h"
#include "subgine/common/types.h"

#include <span>
#include <cmath>
#include <algorithm>
#include <cassert>

#include "subgine/common/define.h"

namespace sbg {

/**
 * This class is used to partition space in equal bounding box.
 * It is used to only test collision that has high chance to occur.
 * Only static objects (in term of not moving) can be inserted inside the tree.
 * 
 * @tparam n: The number of spatial dimentions the tree sould deal with.
 * @tparam Shape: The shape to be contained in the tree
 */
template<dim_t n, typename Shape>
struct NTreeAspect {
	/**
	 * This function add a bunch of shapes to be inserted into the tree.
	 * It will call `add` for the root node.
	 */
	void initialize(Vector<n, double> top, Vector<n, double> bottom, std::span<Shape const> shapes) {
		_elements.reserve(shapes.size());
		_nodes.reserve(shapes.size());
		
		_nodes.emplace_back(top, bottom);
		
		auto nth = std::size_t{0};
		for (auto const& shape : shapes) {
			if(_intersect(_nodes[0], shape)) {
				_elements.push_back(shape);
				_nodes[0].add(*this, 0, nth++);
			}
		}
	}
	
	/**
	 * This function returns a range that enumerate all shapes contained in the tree that are close to the `other` parameter.
	 * T should be the type of an object that can be tested using IntersectAlgorithm.
	 */
	template<typename A = AlignedBoxIntersectAlgorithm<n>>
	auto near(shape::AlignedBox<n> const& object, A algorithm = {}) const {
		auto near = std::vector<std::size_t>{};
		
		near.reserve(divisions * subdivideThreshold);
		_nodes[0].near(*this, object, algorithm, near);
		
		// TODO: Use a flat hash set instead of a vector or a heap structure
		std::sort(near.begin(), near.end());
		near.erase(std::unique(near.begin(), near.end()), near.end());
		
		return IndexViewRange<Shape const, std::vector<std::size_t>>{_elements, std::move(near)};
	}
	
private:
	/**
	 * This is the number of subnodes it contains.
	 * It can be seen as the number of space partition this partition contains.
	 */
	static constexpr auto divisions = std::size_t{power<n>(2)};
	
	/**
	 * This is the maximum depth before the tree will stop subdividing.
	 * There is a point of diminishing return and we decided it's 6
	 */
	static constexpr auto maxDepth = std::int32_t{6};
	
	/**
	 * This is the number of elements a node need to contain before subividing into subnodes
	 */
	static constexpr auto subdivideThreshold = std::int32_t{power<n>(2)};
	
	/**
	 * This class represent a node of the tree.
	 * It holds a list of subnodes and a list of object that intersect with the node.
	 * If it has subnodes, the list of object that intersect will be empty as they will be contained in subnodes.
	 */
	struct Node {
		explicit Node(shape::AlignedBox<n> box) noexcept : top{box.top}, bottom{box.bottom} {}
		explicit Node(Vector<n, double> _top, Vector<n, double> _bottom) noexcept : top{_top}, bottom{_bottom} {}
		
		/**
		 * This function add an object in the node.
		 * The object is a integer of size type as it is it's index in the _elements vector in the tree.
		 * 
		 * The logic goes as follow:
		 *  - If the node self is below threshold, undivided, and dividing would result in a smaller box than shape
		 *    or we did reached the max depth, add the shape to self
		 *  - Else we add by subdividing. If we have shape in self, we add them subdividing too.
		 * 
		 * This will have an effect of having shapes only in leaf nodes, which is what we want.
		 * 
		 * We also only subdividing when adding an shape and don't subdivide if the shape would
		 * be the only one in the node. We don't want to add more level than shapes in a node, or
		 * checking shapes one by one would be more efficient.
		 */
		static void add(NTreeAspect& tree, std::size_t self, std::size_t shape, std::int32_t depth = 0) {
			if (auto& this_node = tree._nodes[self];
				not this_node.subdivided()
				and (
					   this_node._elements.size() < subdivideThreshold
					or depth == maxDepth
					or not this_node.large_enough(tree._elements[shape])
				)
			) {
				this_node._elements.emplace_back(shape);
			} else {
				for (auto i = std::size_t{0} ; i < tree._nodes[self]._elements.size() ; ++i) {
					auto const object = tree._nodes[self]._elements[i];
					subdivide(tree, self, object, depth);
				}
				
				subdivide(tree, self, shape, depth);
				tree._nodes[self]._elements.clear();
			}
		}
		
		/**
		 * This function returns the indexes of near objects in the node.
		 * The parameter is the object to test if it's near.
		 */
		template<typename A>
		void near(NTreeAspect const& tree, shape::AlignedBox<n> const& object, A& intersect, std::vector<std::size_t>& near) const {
			for (auto const& node : _children) {
				if (node and intersect(object, tree._nodes[node])) {
					tree._nodes[node].near(tree, object, intersect, near);
				}
			}
			
			if (not _elements.empty()) {
				std::copy(_elements.begin(), _elements.end(), std::back_inserter(near));
			}
		}
		
		Vector<n, double> top, bottom;
		
	private:
		/**
		 * Returns true only if a a subnode would be large enough to contain the shape.
		 */
		[[nodiscard]]
		constexpr auto large_enough(Shape const& shape) const noexcept -> bool {
			auto const box = aligned_box_shape(shape);
			auto const size = (bottom - top) / 2;
			auto const difference = size - (box.bottom - box.top);
			
			return apply_sequence<n>([&](auto... s) {
				return ((get<s>(difference) >= 0) and ...);
			});
		}
		
		/**
		 * Returns true if the current node has a subdivision
		 */
		[[nodiscard]]
		constexpr auto subdivided() const noexcept -> bool {
			return std::any_of(_children.begin(), _children.end(), [](std::size_t child) {
				return child != 0;
			});
		}
		
		/**
		 * This function add an shape by subdividing.
		 * 
		 * If an shape intersect with the space a subnode would occupy,
		 * we create that subnode and add the shape into it.
		 * 
		 * If the node was already there, simply add using the add function recursively.
		 */
		static void subdivide(NTreeAspect& tree, std::size_t self, std::size_t shape, std::int32_t depth) {
			for (auto i = std::size_t{0}; i < divisions; ++i) {
				auto const child = tree._nodes[self]._children[i];
				auto const box = tree._nodes[self].child_box(i);
				
				if (tree._intersect(tree._elements[shape], box)) {
					if (!child) {
						tree._nodes[self]._children[i] = tree._nodes.size();
						tree._nodes.emplace_back(box)._elements.emplace_back(shape);
					} else {
						tree._nodes[self].add(tree, child, shape, depth + 1);
					}
				}
			}
		}
		
		/**
		 * This function return the position of the top left and the bottom right of the to be created node.
		 * It will return a position depending on the number of dimension of the tree and the position of the node.
		 * 
		 * 
		 * 
		 * Here's a table of how the node it placed depending of I in 2D:
		 *  nth| s[0] | s[1]
		 *  ----------------
		 *   0 |  0   |  0
		 *   1 |  1   |  0
		 *   2 |  0   |  1
		 *   3 |  1   |  1
		 * 
		 * 
		 * 
		 * For three dimensions, the value of the third parameter will be 0 from a S of 0 to 7 and 1 from a S of 8 to 15
		 * It basically create the row from table of all possible binary composition of size of S.
		 * We use this to compute the position we should give to the node.
		 * This magical vector will multiply the size of the parent node divided by two, placing it into the right place.
		 */
		auto child_box(std::size_t nth_index) const noexcept -> shape::AlignedBox<n> {
			auto const nth = static_cast<std::int32_t>(nth_index);
			auto const size = (bottom - top) / 2.;
			
			return apply_sequence<n>([&](auto... s) {
				auto const position = Vector{((nth / power<s>(2)) % 2)...};
				return shape::AlignedBox{
					.top = top + (size * position),
					.bottom = top + (size * (position + Vector<n, double>{1})),
				};
			});
		}
		
		/**
		 * Computes the bounding box of a node
		 */
		friend auto aligned_box_shape(Node const& node) noexcept -> shape::AlignedBox<n> {
			return shape::AlignedBox<n>{
				.top = node.top,
				.bottom = node.bottom,
			};
		}
		
		std::array<std::size_t, divisions> _children = {};
		std::vector<std::size_t> _elements = {};
	};

	/**
	 * Computes the bounding box of the whole tree
	 */
	friend auto aligned_box_shape(NTreeAspect const& tree) noexcept -> shape::AlignedBox<n> {
		if (tree._nodes.empty()) return {};
		auto const& root = tree._nodes[0];
		return shape::AlignedBox<n>{root.top, root.bottom};
	}
	
	std::vector<Node> _nodes;
	std::vector<Shape> _elements;
	
	NO_UNIQUE_ADDRESS
	AlignedBoxIntersectAlgorithm<n> _intersect;
};

template<typename Shape>
using QuadtreeAspect = NTreeAspect<2, Shape>;

template<typename Shape>
using OctreeAspect = NTreeAspect<3, Shape>;

} // namespace sbg

#include "subgine/common/undef.h"
