#pragma once

#include <utility>
#include <iterator>

namespace sbg {

template<typename T, typename KeyIterator>
struct IndexIterator {
	IndexIterator() = default;
	IndexIterator(T* data, KeyIterator it) : _data{data}, _it{it} {}

	using iterator_category = std::random_access_iterator_tag;
	using value_type = T;
	using reference = T&;
	
	using const_reference = T const&;
	using pointer = T*;
	using difference_type = typename KeyIterator::difference_type;
	
	auto operator*() const noexcept -> reference {
		return _data[*_it];
	}
	
	auto operator->() const noexcept -> pointer {
		return std::addressof(_data[*_it]);
	}
		
	auto operator++() noexcept -> IndexIterator& {
		++_it;
		return *this;
	}
	
	auto operator++(int) noexcept -> IndexIterator {
		return IndexIterator{_data, _it++};
	}
	
	auto operator--() noexcept -> IndexIterator {
		--_it;
		return *this;
	}
	
	auto operator--(int) noexcept -> IndexIterator {
		return IndexIterator{_data, _it--};
	}
		
	friend auto operator<=>(IndexIterator const& lhs, IndexIterator const& rhs) noexcept = default;
	friend auto operator==(IndexIterator const& lhs, IndexIterator const& rhs) noexcept -> bool = default;
	
	friend auto operator+(IndexIterator const& lhs, difference_type n) noexcept -> IndexIterator {
		return IndexIterator{lhs._data, lhs._it + n};
	}
	
	friend auto operator+(difference_type n, IndexIterator const& rhs) noexcept -> IndexIterator {
		return IndexIterator{rhs._data, rhs._it + n};
	}
	
	friend auto operator-(difference_type n, IndexIterator const& rhs) noexcept -> IndexIterator {
		return IndexIterator{rhs._data, rhs._it - n};
	}
	
	friend auto operator-(IndexIterator const& lhs, difference_type n) noexcept -> IndexIterator {
		return IndexIterator{lhs._data, lhs._it - n};
	}
	
	friend auto operator-(IndexIterator const& lhs, IndexIterator const& rhs) noexcept -> difference_type {
		return lhs._it - rhs._it;
	}
	
private:
	T* _data = nullptr;
	KeyIterator _it = {};
};

} // namespace sbg
