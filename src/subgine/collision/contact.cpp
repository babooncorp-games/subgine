#include "contact.h"

namespace sbg {

template<dim_t n>
auto Contact<n>::reverse() const -> Contact {
	return Contact<n>{-penetration, position};
}

template struct Contact<2>;
template struct Contact<3>;

} // namespace sbg
