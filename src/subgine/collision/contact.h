#pragma once

#include "subgine/vector/vector.h"

namespace sbg {

template<dim_t n>
struct Contact {
	sbg::Vector<n, double> penetration, position;
	auto reverse() const -> Contact;
};

using Contact2D = Contact<2>;
using Contact3D = Contact<3>;

extern template struct Contact<2>;
extern template struct Contact<3>;

template<dim_t n>
Contact(Vector<n, double>, Vector<n, double>) -> Contact<n>;

} // namespace sbg
