#pragma once

#include "indexiterator.h"

#include <span>

namespace sbg {

template<typename T, typename IndexContainer>
struct IndexViewRange {
	IndexViewRange(std::span<T> container, IndexContainer indexes) : _container{container}, _indexes{std::move(indexes)} {}
	
	using iterator = IndexIterator<T, typename IndexContainer::const_iterator>;
	
	auto begin() const& -> iterator {
		return iterator{_container.data(), _indexes.cbegin()};
	}
	
	auto end() const& -> iterator {
		return iterator{_container.data(), _indexes.cend()};
	}
	
	auto size() const -> std::size_t {
		return _indexes.size();
	}
	
private:
	std::span<T> _container;
	IndexContainer _indexes;
};

} // namespace sbg
