#include "catch2/catch.hpp"

#include "../engine/collisionengine.h"
#include "../algorithm/alignedboxalignedboxalgorithm.h"
#include "../aspect/alignedboxaspect.h"
#include "../aspect/nsphereaspect.h"
#include "../component/collisionbody.h"

#include "subgine/entity.h"
#include "subgine/common/kangaru.h"

using namespace sbg;

TEST_CASE("Match entities with their groups", "[collision]") {
	EntityManager entities;
	auto entity1 = entities.create(), entity2 = entities.create();
	
	static int react1group1Called, react2group1Called, react1group2Called, react2group2Called;
	
	react1group1Called = 0;
	react2group1Called = 0;
	react1group2Called = 0;
	react2group2Called = 0;
	
	CollisionBody collisionBody1, collisionBody2;
	
	AlignedBoxAspect aspect1{shape::AlignedBox{Vector2d{0, 0}, Vector2d{5, 5}}, Vector2d{}};
	AlignedBoxAspect aspect2{shape::AlignedBox{Vector2d{1, 1}, Vector2d{6, 6}}, Vector2d{}};
	
	CollisionEngine engine;
	
	engine.addTester<AlignedBox2DAspect, AlignedBox2DAspect>(AlignedBox2DAlignedBox2DAlgorithm{});
	
	auto execute = [&]{
		entity1.assign(std::move(collisionBody1));
		entity2.assign(std::move(collisionBody2));
		
		engine.add(entity1);
		engine.add(entity2);
		
		engine.execute(Time{});
	};
	
	auto add_aspect = [&](CollisionBody& collisionBody, std::string const& name, auto&& aspect, std::vector<std::string> groups, std::vector<std::string> colliding) {
		collisionBody.profiles.emplace(name, CollisionProfile{
			std::forward<decltype(aspect)>(aspect),
			groups,
			colliding
		});
	};
	
	auto setup_reactors = [](CollisionBody& body1, CollisionBody& body2) {
		body1.profiles.at("profile1").reactors.addReactor("group1", [](BasicCollisionInfo) { react1group1Called++; });
		body1.profiles.at("profile1").reactors.addReactor("group2", [](BasicCollisionInfo) { react1group2Called++; });
		body2.profiles.at("profile1").reactors.addReactor("group1", [](BasicCollisionInfo) { react2group1Called++; });
		body2.profiles.at("profile1").reactors.addReactor("group2", [](BasicCollisionInfo) { react2group2Called++; });
	};
	
	SECTION("One to one one sided") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1"},
			{"group2"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group2"},
			{}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 0);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 0);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one reciprocal") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1"},
			{"group1"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group1"},
			{"group1"}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 1);
		CHECK(react1group2Called == 0);
		CHECK(react2group1Called == 1);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one heterogenous reciprocal") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1"},
			{"group2"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group2"},
			{"group1"}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 0);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 1);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one multiple groups") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1", "group2"},
			{"group2"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group1", "group2"},
			{}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 0);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 0);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one multiple collision") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1", "group2"},
			{"group2", "group1"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group1", "group2"},
			{}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 1);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 0);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one multiple collision reciprocal one") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1", "group2"},
			{"group2", "group1"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group1", "group2"},
			{"group1"}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 1);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 1);
		CHECK(react2group2Called == 0);
	}
	
	SECTION("One to one multiple collision reciprocal one") {
		add_aspect(
			collisionBody1,
			"profile1",
			std::move(aspect1),
			{"group1", "group2"},
			{"group2", "group1"}
		);
		
		add_aspect(
			collisionBody2,
			"profile1",
			std::move(aspect2),
			{"group1", "group2"},
			{"group1", "group2"}
		);
		
		setup_reactors(collisionBody1, collisionBody2);
		
		execute();
		
		CHECK(react1group1Called == 1);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 1);
		CHECK(react2group2Called == 1);
	}
	
	SECTION("Reverse one-way algorithm") {
		struct TestResult {
			void const* ptr_first;
			void const* ptr_second;
			
			auto reverse() const -> TestResult {
				return TestResult{ptr_second, ptr_first};
			}
		};
		
		engine.addTester<AlignedBox2DAspect, CircleAspect>([](AlignedBox2DAspect const& box, CircleAspect const& circle) {
			return std::optional{TestResult{&box, &circle}};
		});
		
		auto collisionBody3 = CollisionBody{};
		auto aspect3 = CircleAspect{100, Vector2d{}};
		
		add_aspect(
			collisionBody3,
			"profile1",
			aspect3,
			{"group1"},
			{"group2"}
		);
		
		add_aspect(
			collisionBody1,
			"profile1",
			aspect1,
			{"group2"},
			{}
		);
		
		collisionBody3.profiles.at("profile1").reactors.addReactor("group1", [&](CollisionInfo<TestResult> const& info) { react1group1Called++; CHECK(info.ptr_first == &entity1.component<CollisionBody>().profiles.at("profile1").aspect.as<CircleAspect>()); });
		collisionBody3.profiles.at("profile1").reactors.addReactor("group2", [&](CollisionInfo<TestResult> const& info) { react1group2Called++; CHECK(info.ptr_first == &entity1.component<CollisionBody>().profiles.at("profile1").aspect.as<CircleAspect>()); });
		collisionBody1.profiles.at("profile1").reactors.addReactor("group1", [&](CollisionInfo<TestResult> const& info) { react2group1Called++; CHECK(info.ptr_second == &entity2.component<CollisionBody>().profiles.at("profile1").aspect.as<AlignedBox2DAspect>()); });
		collisionBody1.profiles.at("profile1").reactors.addReactor("group2", [&](CollisionInfo<TestResult> const& info) { react2group2Called++; CHECK(info.ptr_second == &entity2.component<CollisionBody>().profiles.at("profile1").aspect.as<AlignedBox2DAspect>()); });
		
		entity1.assign(std::move(collisionBody3));
		entity2.assign(std::move(collisionBody1));
		
		engine.add(entity1);
		engine.add(entity2);
		
		engine.execute(Time{});
		
		CHECK(react1group1Called == 0);
		CHECK(react1group2Called == 1);
		CHECK(react2group1Called == 0);
		CHECK(react2group2Called == 0);
	}
}
