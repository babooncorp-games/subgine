#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/system.h"

#include "catch2/catch.hpp"

#include "subgine/common/kangaru.h"

using namespace sbg;
using namespace std::literals;

TEST_CASE("EventReactor", "[collision]") {
	auto c = kgr::container{};
	c.service<MainEngineService>();
	auto& scheduler = c.service<DeferredSchedulerService>();
	auto& entities = c.service<EntityManagerService>();
	
	auto const e = entities.create();
	
	auto enter = std::int32_t{0};
	auto react = std::int32_t{0};
	auto exit = std::int32_t{0};
	
	
	SECTION("Can handle basic collisions") {
		auto reactor = EventReactor{scheduler, e,
			[&](BasicCollisionInfo const& info) {
				enter++;
			},
			[&](BasicCollisionInfo const& info) {
				react++;
			},
			[&](BasicCollisionInfo const& info) {
				exit++;
			}
		};
		
		reactor(BasicCollisionInfo{});
		
		REQUIRE(enter == 1);
		REQUIRE(react == 1);
		REQUIRE(exit == 0);
		
		SECTION("Will forward events to the next frame") {
			scheduler.execute(sbg::Time{1.0, 1.0});
			
			// We are at the end of current frame, so there is no exit event yet
			CHECK(exit == 0);
			
			// Now we pass a whole frame without collision, the exit should trigger!
			scheduler.execute(sbg::Time{1.0, 1.0});
			
			CHECK(exit == 1);
		}
	}
	
	SECTION("Can handle complex collision types") {
		struct Collision {
			int value;
		};
		
		auto const value = 0xbeef;
		
		auto reactor = EventReactor{scheduler, e,
			[&](BasicCollisionInfo const& info) {
				enter++;
			},
			[&](CollisionInfo<Collision> const& info) {
				REQUIRE(info.value == value);
				react++;
			},
			[&](BasicCollisionInfo const& info) {
				exit++;
			}
		};
		
		reactor(CollisionInfo<Collision>{BasicCollisionInfo{}, Collision{.value = value}});
		
		REQUIRE(enter == 1);
		REQUIRE(react == 1);
		REQUIRE(exit == 0);
		
		scheduler.execute(Time{1, 1});
		scheduler.execute(Time{1, 1});
		
		REQUIRE(enter == 1);
		REQUIRE(react == 1);
		REQUIRE(exit == 1);
	}
	
	SECTION("Can handle complex collision types with only enter exit") {
		struct Collision {
			int value;
		};
		
		auto const value = 0xbeef;
		
		auto reactor = EventReactor{scheduler, e,
			[&](CollisionInfo<Collision> const& info) {
				REQUIRE(info.value == value);
				enter++;
			},
			[&](BasicCollisionInfo const& info) {
				exit++;
			}
		};
		
		reactor(CollisionInfo<Collision>{BasicCollisionInfo{}, Collision{.value = value}});
		
		REQUIRE(enter == 1);
		REQUIRE(react == 0);
		REQUIRE(exit == 0);
		
		scheduler.execute(Time{1, 1});
		scheduler.execute(Time{1, 1});
		
		REQUIRE(enter == 1);
		REQUIRE(react == 0);
		REQUIRE(exit == 1);
	}
}
