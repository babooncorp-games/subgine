#pragma once

#include "collisioninfo.h"
#include "detail/reactor.h"
#include "utility/traits.h"

#include "subgine/common/erasedtype.h"
#include "subgine/common/pairhash.h"

#include <unordered_map>
#include <utility>
#include <functional>

namespace sbg {

struct CollisionReactor {
	template<typename InfoType>
	void react(CollisionInfo<InfoType> const& info) {
		{
			auto [begin, end] = _reactors.equal_range(std::pair{std::string{info.group}, type_id<InfoType>});
			for (auto it = begin ; it != end ; it++) {
				ErasedType& reactor = it->second;
				reactor.as<AbstractReactor<InfoType>>().react(info);
			}
		}
		
		{
			auto [begin, end] = _reactors.equal_range(std::pair{std::string{info.group}, type_id<BasicCollisionInfo>});
			for (auto it = begin ; it != end ; it++) {
				ErasedType& reactor = it->second;
				reactor.as<AbstractReactor<BasicCollisionInfo>>().react(info);
			}
		}
	}
	
	auto addReactor(std::string group, reactor auto reactor) {
		using R = decltype(reactor);
		using InfoType = reactor_info_type_t<R>;
		
		// TODO: To use more value semantics here
		auto reactorPtr = std::make_unique<Reactor<R, InfoType>>(std::move(reactor));
		
		_reactors.emplace(
			std::pair{std::move(group), type_id<InfoType>},
			static_cast<std::unique_ptr<AbstractReactor<InfoType>>>(std::move(reactorPtr))
		);
	}
	
private:
	std::unordered_multimap<std::pair<std::string, type_id_t>, ErasedType> _reactors;
};

} // namespace sbg
