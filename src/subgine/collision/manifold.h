#pragma once

#include "contact.h"
#include "subgine/common/traits.h"
#include "subgine/common/types.h"
#include <array>

namespace sbg {

template<dim_t n, typename = std::make_index_sequence<n>>
struct Manifold;

template<dim_t n, std::size_t... S>
struct Manifold<n, std::index_sequence<S...>> {
private:
	template<std::size_t>
	using ExpandContact = Contact<n>;

public:
	explicit Manifold() = default;
	Manifold(ExpandContact<S>... _contacts) : contacts{_contacts...} {}
	
	template<std::size_t size = n, enable_if_i<size != 1> = 0>
	explicit Manifold(Contact<n> contact) : contacts{(void(S), contact)...} {}
	
	std::array<Contact<n>, n> contacts;
	
	Manifold<n> reverse() const;
	
	static constexpr auto dimension = n;
};

using Manifold2D = Manifold<2>;
using Manifold3D = Manifold<3>;

extern template struct Manifold<2>;
extern template struct Manifold<3>;

template<dim_t n, enable_if_i<n != 1> = 0>
Manifold(Contact<n>) -> Manifold<n>;

template<typename... Args>
Manifold(Args const&...) -> Manifold<sizeof...(Args)>;

} // namespace sbg
