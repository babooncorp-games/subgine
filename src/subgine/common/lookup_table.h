#pragma once

#include "traits.h"

#include <array>
#include <algorithm>
#include <stdexcept>

namespace sbg {

/**
 * A immutable table of value associated with a key.
 * 
 * Does linear lookup in it's array.
 */
template<typename K, typename V, std::size_t s>
struct LookupTable : private std::array<std::pair<K, V>, s> {
private:
	using Parent = std::array<std::pair<K, V>, s>;
	
public:
	constexpr static auto size = s;
	
	/**
	 * Table constructor. The only way to assign a value.
	 * 
	 * The table should be constructed with makeLookupTable
	 */
	template<typename... Args, enable_if_i<is_brace_constructible_v<Parent, Args...>> = 0>
	constexpr LookupTable(Args... args) : Parent{std::move(args)...} {}
	
	using KeyType = K;
	using ValueType = V;
	
	/**
	 * Lookup function of the table.
	 * 
	 * The key can be anything that can compare to the key type of the table.
	 * 
	 * It should be used like this:
	 * 
	 * @code
	 * auto valueResult = tableInstance[keyValue]
	 * @endcode
	 * 
	 * @param key The key to lookup in the table.
	 * @throw std::out_of_range when the lookup fail.
	 */
	template<typename Key, enable_if_i<is_comparable_v<const K&, const Key&>> = 0>
	auto operator[](const Key& key) const -> const V& {
		auto it = std::find_if(this->begin(), this->end(), [&](auto const& element) {
			return element.first == key;
		});
		
		if (it != this->end()) {
			return it->second;
		}
		
		std::abort();
	}
};

/**
 * Creates a LookupTable, with K as it's key type and V as it's value type.
 * 
 * Should be used like this:
 * @code
 * auto table = makeLookupTable<KeyType, ValueType>({
 *     {key1, value1},
 *     {key2, value2}
 * });
 * @endcode
 */
template<typename K, typename V, std::size_t N>
constexpr auto makeLookupTable(const std::pair<K, V>(&values)[N]) {
	return apply_sequence<N>([&](auto... i) { return LookupTable<K, V, N>{values[i]...}; });
}

/**
 * Creates a LookupTable, with K as it's key type and V as it's value type.
 * 
 * Should be used like this:
 * @code
 * auto table = makeLookupTable<KeyType, ValueType>({
 *     {key1, value1},
 *     {key2, value2}
 * });
 * @endcode
 */
template<typename K, typename V, std::size_t N>
constexpr auto makeLookupTable(std::pair<K, V>(&&values)[N]) {
	return apply_sequence<N>([&](auto... i) { return LookupTable<K, V, N>{std::move(values[i])...}; });
}

} // namespace sbg
