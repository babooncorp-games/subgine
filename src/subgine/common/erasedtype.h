#pragma once

#include "traits.h"
#include "type_id.h"

#include <memory>

namespace sbg {

/**
 * This class is mimic the behaviour of std::any, but also support abstract types and immovable data.
 * We want to acheive a simple, ligther and faster any that suits our needs.
 */
struct ErasedType {
	template<typename T,
		enable_if_i<!is_specialisation_of_v<std::unique_ptr, T>> = 0,
		enable_if_i<!std::is_abstract_v<std::decay_t<T>>> = 0,
		enable_if_i<std::is_constructible_v<std::decay_t<T>, T>> = 0,
		enable_if_i<is_not_self_v<ErasedType, T>> = 0>
	explicit(false) ErasedType(T&& value) : _ptr{new std::decay_t<T>(std::forward<T>(value)), &ErasedType::deleter<std::decay_t<T>>} {}
	
	template<typename T>
	explicit ErasedType(std::unique_ptr<T>&& ptr) noexcept : _ptr{ptr.release(), &ErasedType::deleter<T>} {}
	
	template<typename T> [[nodiscard]] // requires object<T>
	auto as() const& noexcept -> T const& {
		assert_same_type(type_id<std::remove_const_t<T>>);
		return *static_cast<T*>(_ptr.get());
	}
	
	template<typename T> [[nodiscard]] // requires object<T>
	auto as() & noexcept -> T& {
		assert_same_type(type_id<std::remove_const_t<T>>);
		return *static_cast<T*>(_ptr.get());
	}
	
	template<typename T> [[nodiscard]] // requires object<T>
	auto as() && noexcept -> T&& {
		assert_same_type(type_id<std::remove_const_t<T>>);
		return *static_cast<T*>(_ptr.get());
	}
	
	template<typename T> [[nodiscard]] // requires object<T>
	auto is() const noexcept -> bool {
		return type() == type_id<T>;
	}

	[[nodiscard]]
	auto pointer() const noexcept -> void const*;

	[[nodiscard]]
	auto pointer() noexcept -> void*;
	
	[[nodiscard]]
	auto type() const noexcept -> type_id_t {
		return _ptr.get_deleter()(nullptr);
	}
	
private:
	using deleter_type = type_id_t(*)(void*) noexcept;
	
	void assert_same_type(type_id_t as_type) const noexcept;
	
	/**
	 * This function deletes the subscriber. We can't delete a void*, so we must downcast it back to a type.
	 * 
	 * It also returns the type_id of the contained type.
	 */
	template<typename T>
	static auto deleter(void* ptr) noexcept -> type_id_t {
		if (ptr) {
			delete static_cast<T*>(ptr);
		}
		
		return type_id<T>;
	}
	
	std::unique_ptr<void, deleter_type> _ptr;
};

} // namespace sbg
