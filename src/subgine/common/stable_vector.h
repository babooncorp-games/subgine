#pragma once

#include <vmcontainer/pinned_vector.hpp>

namespace sbg {

template<typename T>
using stable_vector = mknejp::vmcontainer::pinned_vector<T>;

inline auto stable_vector_size(std::size_t size) -> mknejp::vmcontainer::max_size_t {
	return mknejp::vmcontainer::max_elements(size);
}

} // namespace sbg
