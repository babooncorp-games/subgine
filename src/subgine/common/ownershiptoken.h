#pragma once

#include <memory>

namespace sbg {

struct OwnershipToken {
	OwnershipToken() = default;
	explicit OwnershipToken(std::shared_ptr<void const> token) : _token{std::move(token)} {}
	
	[[nodiscard]]
	inline auto remaining() const& noexcept -> long {
		return _token.use_count();
	}
	
private:
	std::shared_ptr<void const> _token;
};

} // namespace sbg
