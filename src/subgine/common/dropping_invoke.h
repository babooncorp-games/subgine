#pragma once

#include <type_traits>
#include <functional>

#include "traits.h"


namespace sbg::detail {

template<typename, typename>
struct drop_last_helper;

template<typename T, std::size_t... S>
struct drop_last_helper<T, std::index_sequence<S...>> {
	using type = std::tuple<std::tuple_element_t<S, T>...>;
};

template<typename>
struct drop_last;

template<typename... Args>
struct drop_last<std::tuple<Args...>> : drop_last_helper<std::tuple<Args...>, std::make_index_sequence<sizeof...(Args) - 1>> {};

template<typename T>
using drop_last_t = typename drop_last<T>::type;

template<typename, typename, typename = std::tuple<>, typename = void>
struct dropping_invoke_result_helper {};

template<typename F, typename... Args, typename... Drop>
struct dropping_invoke_result_helper<F, std::tuple<Args...>, std::tuple<Drop...>, std::enable_if_t<std::is_invocable_v<F, Args...>>> {
	using type = std::invoke_result_t<F, Args...>;
	
	static auto invoke(F&& f, Args&&... args, Drop&&...) -> std::invoke_result_t<F, Args...> {
		return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
	}
};

template<typename F, typename H, typename... T, typename... Drop>
struct dropping_invoke_result_helper<F, std::tuple<H, T...>, std::tuple<Drop...>, std::enable_if_t<!std::is_invocable_v<F, H, T...>>> :
	dropping_invoke_result_helper<F, detail::drop_last_t<std::tuple<H, T...>>, std::tuple<std::tuple_element_t<sizeof...(T), std::tuple<H, T...>>, Drop...>> {};

} // namespace sbg::detail

namespace sbg {

template<typename F, typename... Args>
using dropping_invoke_result = detail::dropping_invoke_result_helper<F, std::tuple<Args...>>;

template<typename F, typename... Args>
using dropping_invoke_result_t = typename dropping_invoke_result<F, Args...>::type;

template<typename F, typename... Args>
using is_dropping_invocable = is_detected<dropping_invoke_result_t, F, Args...>;

template<typename F, typename... Args>
constexpr bool is_dropping_invocable_v = is_dropping_invocable<F, Args...>::value;

template<typename F, typename... Args>
auto dropping_invoke(F&& f, Args&&... args) -> dropping_invoke_result_t<F, Args...> {
	return dropping_invoke_result<F, Args...>::invoke(std::forward<F>(f), std::forward<Args>(args)...);
}

} // namespace sbg
