#pragma once

#include <concepts>
#include <type_traits>

namespace sbg {

template<typename T1, typename T2>
concept similar_to =
	    std::same_as<std::decay_t<T1>, std::decay_t<T2>>
	and std::convertible_to<T1, T2>;

template<typename T>
concept object = std::is_object_v<T> and not std::is_reference_v<T>;

template<typename R, typename T>
concept range_of = requires(R r, decltype(r.begin())& begin) {
	{ *begin } -> similar_to<T>;
	{ ++begin } -> std::same_as<decltype(begin)>;
	{ r.begin() != r.end() } -> std::convertible_to<bool>;
};

template<typename T1, typename T2>
concept different_from = not similar_to<T1, T2>;

template<typename T, typename F>
concept forwarded = std::same_as<std::remove_cvref_t<F>, T>;

} // namespace sbg
