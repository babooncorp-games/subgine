#include "hash.h"

#include <algorithm>

namespace sbg {
	template struct murmur_hash<std::uint8_t>;
	template struct murmur_hash<std::uint16_t>;
	template struct murmur_hash<std::uint32_t>;
	template struct murmur_hash<std::uint64_t>;
	template struct murmur_hash<std::int8_t>;
	template struct murmur_hash<std::int16_t>;
	template struct murmur_hash<std::int32_t>;
	template struct murmur_hash<std::int64_t>;
}

