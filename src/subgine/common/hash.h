#pragma once

#include "fixed_string.h"

#include <cstdint>
#include <cstring>
#include <concepts>
#include <span>
#include <bit>
#include <string_view>
#include <array>
#include <string>

namespace sbg {

template<std::size_t n>
constexpr auto fixed_string_to_bytes(fixed_string<n> string) -> std::array<std::byte, n> {
	auto converted = std::array<std::byte, n>{};
	
	for (auto index = std::size_t{0}; index < n; ++index) {
		converted[index] = static_cast<std::byte>(string[index]);
	}
	
	return converted;
}

template<fixed_string string>
consteval auto operator""_b() noexcept -> std::array<std::byte, string.size()> {
	return fixed_string_to_bytes(string);
}

consteval auto operator""_b(unsigned long long c) -> std::byte {
	if (c > 255) throw "Byte out of range";
	return static_cast<std::byte>(c);
}

enum struct hash_t : std::size_t {};
inline constexpr auto murmur_default_seed = hash_t{};

constexpr auto murmur64a(std::span<std::byte const> const buf, hash_t const seed = murmur_default_seed) noexcept -> hash_t {
	auto constexpr m = std::uint64_t{0xc6a4a7935bd1e995ull};
	auto constexpr r = int{47};
	
	auto const length = buf.size();
	
	auto hash = static_cast<std::size_t>(seed) ^ (length * m);
	auto data = buf.data();
	auto const end = buf.data() + (length & ~0b111ull); // floor to closest multiple of 8
	
	while (data != end) {
		auto k = std::uint64_t{0}
			| static_cast<std::uint64_t>(data[0]) << 0
			| static_cast<std::uint64_t>(data[1]) << 8
			| static_cast<std::uint64_t>(data[2]) << 16
			| static_cast<std::uint64_t>(data[3]) << 24
			| static_cast<std::uint64_t>(data[4]) << 32
			| static_cast<std::uint64_t>(data[5]) << 40
			| static_cast<std::uint64_t>(data[6]) << 48
			| static_cast<std::uint64_t>(data[7]) << 56;
		
		k *= m;
		k ^= k >> r;
		k *= m;
		
		hash ^= k;
		hash *= m;
		
		data += sizeof(std::uint64_t);
	}
	
	switch (length & 0b111ull) {
		case 7: hash ^= static_cast<std::uint64_t>(data[6]) << 48; [[fallthrough]];
		case 6: hash ^= static_cast<std::uint64_t>(data[5]) << 40; [[fallthrough]];
		case 5: hash ^= static_cast<std::uint64_t>(data[4]) << 32; [[fallthrough]];
		case 4: hash ^= static_cast<std::uint64_t>(data[3]) << 24; [[fallthrough]];
		case 3: hash ^= static_cast<std::uint64_t>(data[2]) << 16; [[fallthrough]];
		case 2: hash ^= static_cast<std::uint64_t>(data[1]) << 8; [[fallthrough]];
		case 1: hash ^= static_cast<std::uint64_t>(data[0]) << 0;
		hash *= m;
	}
	
	hash ^= hash >> r;
	hash *= m;
	hash ^= hash >> r;
	
	return static_cast<hash_t>(hash);
}

// This is not constexpr since we reinterpret
inline auto murmur64a(std::string_view const str, hash_t const seed = murmur_default_seed) -> hash_t {
	return murmur64a(std::span{reinterpret_cast<std::byte const*>(str.data()), str.size()}, seed);
}

namespace literals {
	template<fixed_string string>
	consteval auto operator""_h() -> hash_t {
		return murmur64a(fixed_string_to_bytes(string));
	}
}

template<typename T>
struct murmur_hash;

template<typename T>
inline auto murmur_hash_combine(std::size_t& seed, T const& v) noexcept {
	seed ^= murmur_hash<T>{}(v) + 0x9e3779b8 + (seed << 6) + (seed >> 2);
}

template<typename T> requires std::is_integral_v<T>
struct murmur_hash<T> {
	inline auto operator()(T const n) const& noexcept -> std::size_t {
		auto const bytes = std::bit_cast<std::array<std::byte, sizeof(T)>>(n);
		return static_cast<std::uint64_t>(murmur64a(bytes));
	}
};

// TODO: Why the hell is this needed??
template<>
struct murmur_hash<unsigned long> {
	inline auto operator()(unsigned long const n) const& noexcept -> std::size_t {
		auto const bytes = std::bit_cast<std::array<std::byte, sizeof(unsigned long)>>(n);
		return static_cast<std::uint64_t>(murmur64a(bytes));
	}
};

template<>
struct murmur_hash<std::byte> {
	inline auto operator()(std::byte const b) const& noexcept -> std::size_t {
		return static_cast<std::uint64_t>(murmur64a(std::span<std::byte const, 1>{&b, 1}));
	}
};

template<>
struct murmur_hash<std::string_view> {
	inline auto operator()(std::string_view const str) const& noexcept -> std::size_t {
		return static_cast<std::size_t>(murmur64a(str));
	}
};

template<>
struct murmur_hash<std::string> {
	inline auto operator()(std::string const& str) const& noexcept -> std::size_t {
		return murmur_hash<std::string_view>{}(str);
	}
};

template<typename T>
struct murmur_hash<T*> {
	inline auto operator()(T* const ptr) const& noexcept -> std::size_t {
		std::uintptr_t value; // uninitialized
		std::memcpy(std::addressof(value), std::addressof(ptr), sizeof(ptr));
		return murmur_hash<std::uintptr_t>{}(value);
	}
};

struct string_murmur_hash {
	using is_transparent = void;
	auto operator()(std::convertible_to<std::string_view> auto const& str) const& noexcept -> std::size_t {
		return murmur_hash<std::string_view>{}(str);
	}
};

struct key_is_hash {
	constexpr auto operator()(hash_t hash) const& noexcept -> std::size_t {
		return static_cast<std::size_t>(hash);
	}
};

extern template struct murmur_hash<std::uint8_t>;
extern template struct murmur_hash<std::uint16_t>;
extern template struct murmur_hash<std::uint32_t>;
extern template struct murmur_hash<std::uint64_t>;
extern template struct murmur_hash<std::int8_t>;
extern template struct murmur_hash<std::int16_t>;
extern template struct murmur_hash<std::int32_t>;
extern template struct murmur_hash<std::int64_t>;

} // namespace sbg
