#pragma once

#include <string_view>

namespace sbg {

auto assign_project_name(std::string_view const project_name) noexcept -> void;
auto project_name() noexcept -> std::string_view;

} // namespace sbg
