// By Emil Ernerfeldt 2014-2017
// LICENSE:
//   This software is dual-licensed to the public domain and under the following
//   license: you are granted a perpetual, irrevocable license to copy, modify,
//   publish, and distribute this file as you see fit.

#pragma once

#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iterator>
#include <utility>
#include <cassert>
#include <stdexcept>

// #include <loguru.hpp>
#define DCHECK_F(A) assert(A)
#define DCHECK_EQ_F(A, B) assert(A == B)
#define DCHECK_LT_F(A, B) assert(A < B)
#define DCHECK_NE_F(A, B) assert(A != B)

#include "../define.h"

namespace emilib {

/// like std::equal_to but no need to #include <functional>
struct hash_map_equal_to
{
	template<typename T1, typename T2>
	constexpr auto operator()(const T1& lhs, const T2& rhs) const -> decltype(lhs == rhs)
	{
		return lhs == rhs;
	}
};

/// A cache-friendly hash table with open addressing, linear probing and power-of-two capacity
template <typename KeyT, typename ValueT, typename HashT = std::hash<KeyT>, typename EqT = hash_map_equal_to>
class hash_map
{
private:
	using MyType = hash_map<KeyT, ValueT, HashT, EqT>;

	using PairT = std::pair<KeyT, ValueT>;
public:
	using size_type       = size_t;
	using value_type      = PairT;
	using reference       = PairT&;
	using const_reference = const PairT&;
	using key_type        = KeyT;
	using mapped_type     = ValueT;
	
	template<typename T>
	static constexpr auto keylike =
		   std::same_as<T, KeyT>
		or std::convertible_to<T&&, KeyT>
		or requires(T t, KeyT k, HashT h, EqT e) {
			requires std::same_as<void, typename HashT::is_transparent>;
			{ e(t, k) } -> std::same_as<bool>;
			{ e(k, t) } -> std::same_as<bool>;
			{ h(t) } -> std::same_as<std::size_t>;
		};
	
	enum { no_index = static_cast<size_t>(-1) };

	class iterator
	{
	public:
		using iterator_category = std::forward_iterator_tag;
		using difference_type   = std::ptrdiff_t;
		using value_type        = std::pair<KeyT, ValueT>;
		using pointer           = value_type*;
		using reference         = value_type&;

		iterator() { }

		iterator(MyType* hash_map, size_t bucket) : _map(hash_map), _bucket(bucket)
		{
		}

		iterator& operator++()
		{
			this->goto_next_element();
			return *this;
		}

		iterator operator++(int)
		{
			size_t old_index = _bucket;
			this->goto_next_element();
			return iterator(_map, old_index);
		}

		reference operator*() const
		{
			return _map->_pairs[_bucket];
		}

		pointer operator->() const
		{
			return _map->_pairs + _bucket;
		}

		bool operator==(const iterator& rhs) const
		{
			DCHECK_EQ_F(_map, rhs._map);
			return this->_bucket == rhs._bucket;
		}

		bool operator!=(const iterator& rhs) const
		{
			DCHECK_EQ_F(_map, rhs._map);
			return this->_bucket != rhs._bucket;
		}

	private:
		void goto_next_element()
		{
			DCHECK_LT_F(_bucket, _map->_num_buckets);
			do {
				_bucket++;
			} while (_bucket < _map->_num_buckets && _map->get_state_at(_map->_states, _bucket) != State::FILLED);
		}

	//private:
	//	friend class MyType;
	public:
		MyType* _map;
		size_t  _bucket;
	};

	class const_iterator
	{
	public:
		using iterator_category = std::forward_iterator_tag;
		using difference_type   = std::ptrdiff_t;
		using value_type        = const std::pair<KeyT, ValueT>;
		using pointer           = value_type*;
		using reference         = value_type&;

		const_iterator() { }

		const_iterator(iterator proto) : _map(proto._map), _bucket(proto._bucket)
		{
		}

		const_iterator(const MyType* hash_map, size_t bucket) : _map(hash_map), _bucket(bucket)
		{
		}

		const_iterator& operator++()
		{
			this->goto_next_element();
			return *this;
		}

		const_iterator operator++(int)
		{
			size_t old_index = _bucket;
			this->goto_next_element();
			return const_iterator(_map, old_index);
		}

		reference operator*() const
		{
			return _map->_pairs[_bucket];
		}

		pointer operator->() const
		{
			return _map->_pairs + _bucket;
		}

		bool operator==(const const_iterator& rhs) const
		{
			DCHECK_EQ_F(_map, rhs._map);
			return this->_bucket == rhs._bucket;
		}

		bool operator!=(const const_iterator& rhs) const
		{
			DCHECK_EQ_F(_map, rhs._map);
			return this->_bucket != rhs._bucket;
		}

	private:
		void goto_next_element()
		{
			DCHECK_LT_F(_bucket, _map->_num_buckets);
			do {
				_bucket++;
			} while (_bucket < _map->_num_buckets && _map->get_state_at(_map->_states, _bucket) != State::FILLED);
		}

	//private:
	//	friend class MyType;
	public:
		const MyType* _map;
		size_t        _bucket;
	};

	// ------------------------------------------------------------------------

	hash_map() = default;

	hash_map(const hash_map& other)
	{
		reserve(other.size());
		insert(other.cbegin(), other.cend());
	}

	hash_map(hash_map&& other) noexcept
	{
		*this = std::move(other);
	}

	template<typename ItType>
	hash_map(ItType b, ItType e) {
		insert(b, e);
	}

	hash_map& operator=(const hash_map& other)
	{
		clear();
		reserve(other.size());
		insert(other.cbegin(), other.cend());
		return *this;
	}

	INLINE void operator=(hash_map&& other) noexcept
	{
		this->swap(other);
	}

	~hash_map()
	{
		for (size_t bucket=0; bucket<_num_buckets; ++bucket) {
			if (get_state_at(_states, bucket) == State::FILLED) {
				_pairs[bucket].~PairT();
			}
		}
		free(_states);
		free(_pairs);
	}

	friend INLINE void swap(hash_map& lhs, hash_map& rhs) noexcept
	{
		lhs.swap(rhs);
	}

	void swap(hash_map& other) noexcept
	{
		std::swap(_hasher,           other._hasher);
		std::swap(_eq,               other._eq);
		std::swap(_states,           other._states);
		std::swap(_pairs,            other._pairs);
		std::swap(_num_buckets,      other._num_buckets);
		std::swap(_num_filled,       other._num_filled);
		std::swap(_max_probe_length, other._max_probe_length);
		std::swap(_mask,             other._mask);
	}

	// -------------------------------------------------------------

	iterator begin()
	{
		size_t bucket = 0;
		while (bucket<_num_buckets && get_state_at(_states, bucket) != State::FILLED) {
			++bucket;
		}
		return iterator(this, bucket);
	}

	const_iterator cbegin() const
	{
		size_t bucket = 0;
		while (bucket<_num_buckets && get_state_at(_states, bucket) != State::FILLED) {
			++bucket;
		}
		return const_iterator(this, bucket);
	}

	const_iterator begin() const
	{
		return cbegin();
	}

	iterator end()
	{
		return iterator(this, _num_buckets);
	}

	const_iterator cend() const
	{
		return const_iterator(this, _num_buckets);
	}

	const_iterator end() const
	{
		return cend();
	}

	size_t size() const
	{
		return _num_filled;
	}

	bool empty() const
	{
		return _num_filled==0;
	}

	// Returns the number of buckets.
	size_t bucket_count() const
	{
		return _num_buckets;
	}

	/// Returns average number of elements per bucket.
	float load_factor() const
	{
		return static_cast<float>(_num_filled) / static_cast<float>(_num_buckets);
	}
	
	template<typename KeyLike> requires keylike<KeyLike>
	auto at(KeyLike const& key) const -> ValueT& {
		auto const [bucket, hash_value] = this->find_filled_bucket(key);
		if (bucket == no_index) {
			throw std::out_of_range("hash_map::at");
		}
		return _pairs[bucket].second;
	}
	
	template<typename KeyLike> requires keylike<KeyLike>
	auto at(KeyLike const& key) -> ValueT& {
		auto const [bucket, hash_value] = this->find_filled_bucket(key);
		if (bucket == no_index) {
			throw std::out_of_range("hash_map::at");
		}
		return _pairs[bucket].second;
	}

	// ------------------------------------------------------------
	iterator find_by_hash(std::size_t hash_key) {
		auto bucket = this->find_filled_bucket_by_hash(hash_key);
		if (bucket == no_index) {
			return this->end();
		}
		return iterator(this, bucket);
	}

	const_iterator find_by_hash(std::size_t hash_key) const {
		auto bucket = this->find_filled_bucket_by_hash(hash_key);
		if (bucket == no_index) {
			return this->end();
		}
		return const_iterator(this, bucket);
	}

	template<typename KeyLike> requires keylike<KeyLike>
	iterator find(KeyLike const& key)
	{
		auto const [bucket, hash_value] = this->find_filled_bucket(key);
		if (bucket == no_index) {
			return this->end();
		}
		return iterator(this, bucket);
	}

	template<typename KeyLike> requires keylike<KeyLike>
	const_iterator find(KeyLike const& key) const
	{
		auto [bucket, hash_value] = this->find_filled_bucket(key);
		if (bucket == no_index)
		{
			return this->end();
		}
		return const_iterator(this, bucket);
	}

	template<typename KeyLike> requires keylike<KeyLike>
	bool contains(KeyLike const& k) const
	{
		return find_filled_bucket(k).first != no_index;
	}

	bool contains_by_hash(std::size_t hash_value) const
	{
		return find_filled_bucket_by_hash(hash_value) != no_index;
	}

	template<typename KeyLike> requires keylike<KeyLike>
	size_t count(KeyLike const& k) const
	{
		return find_filled_bucket(k).first != no_index ? 1 : 0;
	}

	size_t count_by_hash(std::size_t hash_value) const {
		return find_filled_bucket_by_hash(hash_value) != no_index ? 1 : 0;
	}

	/// Returns the matching ValueT or nullptr if k isn't found.
	template<typename KeyLike> requires keylike<KeyLike>
	ValueT* try_get(KeyLike const& k)
	{
		auto const [bucket, hash_value] = find_filled_bucket(k);
		if (bucket != no_index) {
			return &_pairs[bucket].second;
		} else {
			return nullptr;
		}
	}

	/// Const version of the above
	template<typename KeyLike> requires keylike<KeyLike>
	const ValueT* try_get(KeyLike const& k) const
	{
		auto const [bucket, hash_value] = find_filled_bucket(k);
		if (bucket != no_index) {
			return &_pairs[bucket].second;
		} else {
			return nullptr;
		}
	}

	/// Returns the matching ValueT or nullptr if k isn't found.
	ValueT* try_get_by_hash(std::size_t hash_value)
	{
		auto bucket = find_filled_bucket_by_hash(hash_value);
		if (bucket != no_index) {
			return &_pairs[bucket].second;
		} else {
			return nullptr;
		}
	}

	/// Const version of the above
	const ValueT* try_get_by_hash(std::size_t hash_value) const
	{
		auto bucket = find_filled_bucket_by_hash(hash_value);
		if (bucket != no_index) {
			return &_pairs[bucket].second;
		} else {
			return nullptr;
		}
	}


	/// Convenience function.
	template<typename KeyLike> requires keylike<KeyLike>
	auto get_or_return_default(KeyLike const& k) const -> ValueT {
		const ValueT* ret = try_get(k);
		if (ret) {
			return *ret;
		} else {
			return ValueT{};
		}
	}

	/// Convenience function.
	auto get_by_hash_or_return_default(std::size_t hash_value) const -> ValueT {
		const ValueT* ret = try_get_by_hash(hash_value);
		if (ret) {
			return *ret;
		} else {
			return ValueT{};
		}
	}

	static constexpr auto max_size() noexcept {
		return no_index - 255;
	}

	auto emplace(auto&&... args) -> std::pair<iterator, bool> {
		auto p = PairT(FWD(args)...);
		check_expand_need();

		auto const hash_value = _hasher(p.first);
		auto const bucket = find_or_allocate(p.first, hash_value);

		if (get_state_at(_states, bucket) == State::FILLED) {
			return { iterator(this, bucket), false };
		} else {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(std::move(p));
			_num_filled++;
			return { iterator(this, bucket), true };
		}
	}
	// -----------------------------------------------------

	std::pair<iterator, bool> insert(const_reference p) {
		check_expand_need();

		auto const hash_value = _hasher(p.first);
		auto const bucket = find_or_allocate(p.first, hash_value);

		if (get_state_at(_states, bucket) == State::FILLED) {
			return { iterator(this, bucket), false };
		} else {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(p);
			_num_filled++;
			return { iterator(this, bucket), true };
		}
	}

	std::pair<iterator, bool> insert(value_type&& p)
	{
		check_expand_need();

		auto const hash_value = _hasher(p.first);
		auto const bucket = find_or_allocate(p.first, hash_value);

		if (get_state_at(_states, bucket) == State::FILLED) {
			return { iterator(this, bucket), false };
		} else {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(std::move(p));
			_num_filled++;
			return { iterator(this, bucket), true };
		}
	}
	
	auto insert([[maybe_unused]] const_iterator hint, const_reference value) -> iterator {
		auto [it, _] = insert(value);
		return it;
	}
	
	auto insert([[maybe_unused]] const_iterator hint, value_type&& value) -> iterator {
		auto [it, _] = insert(std::move(value));
		return it;
	}
	
	
	auto insert([[maybe_unused]] const_iterator hint, std::convertible_to<value_type> auto&& value) -> iterator {
		auto [it, _] = insert(FWD(value));
		return it;
	}

	template<typename ItType>
	void insert(ItType begin, ItType end)
	{
		auto const size = std::distance(begin, end);
		reserve(size);
		for (; begin != end; ++begin) {
			insert(*begin);
		}
	}

	/// Same as above, but contains(key) MUST be false
	void insert_unique(KeyT&& key, ValueT&& value)
	{
		DCHECK_F(!contains(key));
		check_expand_need();
		auto const hash_value = _hasher(key);
		auto const bucket = find_empty_bucket(key, hash_value);
		set_state_at(_states, bucket, State::FILLED);
		set_hash_at(_states, bucket, hash_value);
		new(_pairs + bucket) PairT(std::move(key), std::move(value));
		_num_filled++;
	}

	void insert_unique(std::pair<KeyT, ValueT>&& p)
	{
		insert_unique(std::move(p.first), std::move(p.second));
	}

	void insert_or_assign(const KeyT& key, ValueT&& value)
	{
		check_expand_need();

		auto const hash_value = _hasher(key);
		auto const bucket = find_or_allocate(key, hash_value);

		// Check if inserting a new value rather than overwriting an old entry
		if (get_state_at(_states, bucket) == State::FILLED) {
			_pairs[bucket].second = value;
		} else {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(key, value);
			_num_filled++;
		}
	}

	/// Return the old value or ValueT() if it didn't exist.
	ValueT set_get(const KeyT& key, const ValueT& new_value)
	{
		check_expand_need();

		auto const hash_value = _hasher(key);
		auto const bucket = find_or_allocate(key, hash_value);

		// Check if inserting a new value rather than overwriting an old entry
		if (get_state_at(_states, bucket) == State::FILLED) {
			ValueT old_value = _pairs[bucket].second;
			_pairs[bucket] = new_value.second;
			return old_value;
		} else {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(key, new_value);
			_num_filled++;
			return ValueT();
		}
	}

	/// Like std::map<KeyT,ValueT>::operator[].
	ValueT& operator[](const KeyT& key)
	{
		check_expand_need();

		auto const hash_value = _hasher(key);
		auto const bucket = find_or_allocate(key, hash_value);

		/* Check if inserting a new value rather than overwriting an old entry */
		if (get_state_at(_states, bucket) != State::FILLED) {
			set_state_at(_states, bucket, State::FILLED);
			set_hash_at(_states, bucket, hash_value);
			new(_pairs + bucket) PairT(std::piecewise_construct, std::tie(key), std::tuple<>{});
			_num_filled++;
		}

		return _pairs[bucket].second;
	}

	// -------------------------------------------------------

	/// Erase an element from the hash table.
	/// return false if element was not found
	bool erase(const KeyT& key)
	{
		auto const [bucket, hash_value] = find_filled_bucket(key);
		if (bucket != no_index) {
			set_state_at(_states, bucket, State::ACTIVE);
			_pairs[bucket].~PairT();
			_num_filled -= 1;
			return true;
		} else {
			return false;
		}
	}

	/// Erase an element using an iterator.
	/// Returns an iterator to the next element (or end()).
	iterator erase(iterator it)
	{
		DCHECK_EQ_F(it._map, this);
		DCHECK_LT_F(it._bucket, _num_buckets);
		set_state_at(_states, it._bucket, State::ACTIVE);
		_pairs[it._bucket].~PairT();
		_num_filled -= 1;
		return ++it;
	}

	/// Remove all elements, keeping full capacity.
	void clear()
	{
		for (size_t bucket=0; bucket<_num_buckets; ++bucket) {
			if (get_state_at(_states, bucket) == State::FILLED) {
				set_state_at(_states, bucket, State::INACTIVE);
				_pairs[bucket].~PairT();
			}
		}
		_num_filled = 0;
		_max_probe_length = -1;
	}

	/// Make room for this many elements
	void reserve(size_t num_elems)
	{
		size_t required_buckets = num_elems + num_elems/2 + 1;
		if (required_buckets <= _num_buckets) {
			return;
		}
		
		// Rouded up to next power of two
		auto num_buckets = required_buckets - 1;
		num_buckets |= num_buckets >> 1;
		num_buckets |= num_buckets >> 2;
		num_buckets |= num_buckets >> 4;
		num_buckets |= num_buckets >> 8;
		num_buckets |= num_buckets >> 16;
		num_buckets |= num_buckets >> 32;
		num_buckets += 1;
		num_buckets = std::max(num_buckets, std::size_t{32});

		auto const size_states = ((((num_buckets - 1) | 0b11111) + 1) >> 5);
		auto const new_states = (State32*)malloc(size_states * sizeof(State32));
		auto const new_pairs  = (PairT*)malloc(num_buckets * sizeof(PairT));

		if (!new_states || !new_pairs) {
			free(new_states);
			free(new_pairs);
			throw std::bad_alloc();
		}

		//auto old_num_filled  = _num_filled;
		auto old_num_buckets = _num_buckets;
		auto old_states      = _states;
		auto old_pairs       = _pairs;

		_num_filled  = 0;
		_num_buckets = num_buckets;
		_mask        = _num_buckets - 1;
		_states      = new_states;
		_pairs       = new_pairs;

		std::fill_n(_states, size_states, State32{});

		_max_probe_length = -1;

		for (size_t src_bucket=0; src_bucket<old_num_buckets; src_bucket++) {
			if (get_state_at(old_states, src_bucket) == State::FILLED) {
				auto& src_pair = old_pairs[src_bucket];
				auto const hash_value = get_hash_at(old_states, src_bucket);

				auto dst_bucket = find_empty_bucket(src_pair.first, hash_value);
				DCHECK_NE_F(dst_bucket, no_index);
				DCHECK_NE_F(get_state_at(_states, dst_bucket), State::FILLED);
				set_state_at(_states, dst_bucket, State::FILLED);
				set_hash_at(_states, dst_bucket, hash_value);
				new(_pairs + dst_bucket) PairT(std::move_if_noexcept(src_pair));
				_num_filled += 1;

				src_pair.~PairT();
			}
		}

		//DCHECK_EQ_F(old_num_filled, _num_filled);

		free(old_states);
		free(old_pairs);
	}

private:
	// Can we fit another element?
	INLINE void check_expand_need()
	{
		reserve(_num_filled + 1);
	}

	size_t find_filled_bucket_by_hash(size_t hash_value) const {
		if (empty()) { return no_index; } // Optimization
	
		for (int offset=0; offset<=_max_probe_length; ++offset) {
			auto bucket = (hash_value + offset) & _mask;
			if (get_state_at(_states, bucket) == State::FILLED) {
				if (get_hash_at(_states, bucket) == hash_value) {
					return bucket;
				}
			} else if (get_state_at(_states, bucket) == State::INACTIVE) {
				return no_index; // End of the chain!
			}
		}	
		return no_index;
	}

	// Find the bucket with this key, or return no_index
	template<typename KeyLike>
	std::pair<size_t, size_t> find_filled_bucket(const KeyLike& key) const
	{
		if (empty()) { return {no_index, 0}; } // Optimization

		auto hash_value = _hasher(key);
		for (int offset=0; offset<=_max_probe_length; ++offset) {
			auto bucket = (hash_value + offset) & _mask;
			if (get_state_at(_states, bucket) == State::FILLED) {
				if (_eq(_pairs[bucket].first, key)) {
					return {bucket, hash_value};
				}
			} else if (get_state_at(_states, bucket) == State::INACTIVE) {
				return {no_index, 0}; // End of the chain!
			}
		}	
		return {no_index, 0};
	}

	// Find the bucket with this key, or return a good empty bucket to place the key in.
	// In the latter case, the bucket is expected to be filled.
	size_t find_or_allocate(const KeyT& key, std::size_t hash_value)
	{
		size_t hole = no_index;
		int offset=0;
		for (; offset<=_max_probe_length; ++offset) {
			auto bucket = (hash_value + offset) & _mask;

			if (get_state_at(_states, bucket) == State::FILLED) {
				if (_eq(_pairs[bucket].first, key)) {
					return bucket;
				}
			} else if (get_state_at(_states, bucket) == State::INACTIVE) {
				return bucket;
			} else {
				// ACTIVE: keep searching
				if (hole == no_index) {
					hole = bucket;
				}
			}
		}

		// No key found - but maybe a hole for it

		DCHECK_EQ_F(offset, _max_probe_length+1);

		if (hole != no_index) {
			return hole;
		}

		// No hole found within _max_probe_length
		for (; ; ++offset) {
			auto bucket = (hash_value + offset) & _mask;

			if (get_state_at(_states, bucket) != State::FILLED) {
				_max_probe_length = offset;
				return bucket;
			}
		}
	}

	// key is not in this map. Find a place to put it.
	size_t find_empty_bucket(const KeyT& key, std::size_t hash_value)
	{
		for (int offset=0; ; ++offset) {
			auto bucket = (hash_value + offset) & _mask;
			if (get_state_at(_states, bucket) != State::FILLED) {
				if (offset > _max_probe_length) {
					_max_probe_length = offset;
				}
				return bucket;
			}
		}
	}

private:
	enum class State : std::uint8_t
	{
		INACTIVE, // Never been touched
		ACTIVE,   // Is inside a search-chain, but is empty
		FILLED    // Is set with key/value
	};

	struct State32 {
		std::uint64_t byte;
		std::size_t hashes[32];

		[[nodiscard]]
		INLINE constexpr auto state(std::size_t const nth) const& noexcept -> State {
			auto const n = nth << 1ull;
			return static_cast<State>((byte >> n) & 0b11ull);
		}

		INLINE constexpr auto state(std::size_t const nth, State const state) & noexcept -> void {
			auto const value = static_cast<std::size_t>(state);
			auto const n = nth << 1ull;
			byte = (byte & ~(0b11ull << n)) | (value << n);
		}
	};


	INLINE static auto get_state_at(State32 const* states, std::size_t const nth) noexcept -> State {
		auto const lower = nth & 0b11111u;
		auto const upper = (nth & ~0b11111u) >> 5u;
		return states[upper].state(lower);
	}

	INLINE static auto set_state_at(State32* states, std::size_t const nth, State const state) noexcept -> void {
		auto const lower = nth & 0b11111u;
		auto const upper = (nth & ~0b11111u) >> 5u;
		states[upper].state(lower, state);
	}

	INLINE static auto get_hash_at(State32 const* states, std::size_t const nth) noexcept -> std::size_t {
		auto const lower = nth & 0b11111u;
		auto const upper = (nth & ~0b11111u) >> 5u;
		return states[upper].hashes[lower];
	}

	INLINE static auto set_hash_at(State32* states, std::size_t const nth, std::size_t const hash) noexcept -> void {
		auto const lower = nth & 0b11111u;
		auto const upper = (nth & ~0b11111u) >> 5u;
		states[upper].hashes[lower] = hash;
	}


	NO_UNIQUE_ADDRESS HashT _hasher;
	NO_UNIQUE_ADDRESS EqT   _eq;

	State32* _states           = nullptr;
	PairT*   _pairs            = nullptr;
	size_t   _num_buckets      = 0;
	size_t   _num_filled       = 0;
	int      _max_probe_length = -1; // Our longest bucket-brigade is this long. ONLY when we have zero elements is this ever negative (-1).
	size_t   _mask             = 0;  // _num_buckets minus one
};

#undef DCHECK_F
#undef DCHECK_EQ_F
#undef DCHECK_LT_F
#undef DCHECK_NE_F

} // namespace emilib

#include "../undef.h"
