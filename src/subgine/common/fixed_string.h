#pragma once

#include <cstddef>
#include <compare>

namespace sbg {

template<std::size_t n>
struct fixed_string {
	constexpr fixed_string() = default;
	constexpr fixed_string(const char(&str)[n + 1]) noexcept {
		auto i = std::size_t{0};
		for (char const c : str) {
			_data[i++] = c;
		}
	}
	
	friend constexpr auto operator<=>(fixed_string const&, fixed_string const&) = default;
	
	[[nodiscard]]
	static constexpr auto size() noexcept -> std::size_t {
		return n;
	}
	
	constexpr auto data() const& noexcept {
		return _data;
	}
	
	constexpr auto data() & noexcept {
		return _data;
	}
	
	constexpr auto begin() const& noexcept -> char const* {
		return _data;
	}
	
	constexpr auto end() const& noexcept -> char const* {
		return _data + n;
	}
	
	constexpr auto begin() & noexcept -> char* {
		return _data;
	}
	
	constexpr auto end() & noexcept -> char* {
		return _data + n;
	}
	
	constexpr auto operator[](std::size_t index) noexcept {
		return _data[index];
	}
	
	char _data[n + 1];
};

template<std::size_t n>
fixed_string(char const(&)[n]) -> fixed_string<n - 1>;

} // namespace sbg

