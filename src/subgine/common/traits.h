#pragma once

#include "detection.h"

#include <type_traits>
#include <utility>
#include <tuple>

namespace sbg {
namespace detail {

template<typename... Args>
void test(Args...);

template<typename T, typename... Args>
struct is_brace_constructible_helper {
private:
	template<typename U, typename... As>
	static decltype(static_cast<void>(U{std::declval<As>()...}), std::true_type{}) test(int);
	
	template<typename...>
	static std::false_type test(...);
	
public:
	using type = decltype(test<T, Args...>(0));
};

template<typename Tuple, typename = std::make_index_sequence<std::tuple_size_v<Tuple>>>
struct tuple_sequence_helper;

template<typename T, typename I, I... S>
struct tuple_sequence_helper<T, std::integer_sequence<I, S...>> {
	using type = std::tuple<std::integral_constant<I, S>...>;
};

template<typename T>
using call_operator_t = decltype(&T::operator());

} // namespace detail

template<typename From, typename To, typename = void>
inline constexpr bool is_explicitly_convertible_v = false;

template<typename From, typename To>
inline constexpr bool is_explicitly_convertible_v<From, To, std::void_t<decltype(detail::test<To>(static_cast<To>(std::declval<From>())))>> = true;

template<typename From, typename To>
using is_explicitly_convertible = std::bool_constant<is_explicitly_convertible_v<From, To>>;

template<typename From, typename To>
inline constexpr bool is_strictly_explicitly_convertible_v = !std::is_convertible_v<From, To> && is_explicitly_convertible_v<From, To>;

template<typename From, typename To>
using is_strictly_explicitly_convertible = std::bool_constant<is_strictly_explicitly_convertible_v<From, To>>;

template<bool b>
using enable_if_i = std::enable_if_t<b, int>;

template<typename T>
struct remove_rvalue_reference { using type = T; };

template<typename T>
struct remove_rvalue_reference<T&&> { using type = T; };

template<typename T>
using remove_rvalue_reference_t = typename remove_rvalue_reference<T>::type;

template<typename Self, typename T>
inline constexpr bool is_not_self_v = !std::is_same<Self, std::decay_t<T>>::value && !std::is_base_of<Self, std::decay_t<T>>::value;

template<typename T, typename U>
using is_not_self = std::bool_constant<is_not_self_v<T, U>>;

template<template<typename...> typename T, typename Types>
struct is_specialisation_of : std::false_type {};

template<template<typename...> typename T, typename... Types>
struct is_specialisation_of<T, T<Types...>> : std::true_type {};

template<template<typename...> typename T, typename Type>
inline constexpr auto is_specialisation_of_v = is_specialisation_of<T, Type>::value;

template<typename, typename, typename = void>
struct is_comparable : std::false_type {};

template<typename L, typename R>
struct is_comparable<L, R, std::void_t<
	decltype(std::declval<L>() == std::declval<R>())
>> : std::true_type {};

template<typename L, typename R>
inline constexpr auto is_comparable_v = is_comparable<L, R>::value;

template<typename T, typename... Args>
using is_brace_constructible = typename detail::is_brace_constructible_helper<T, Args...>::type;

template<typename T, typename... Args>
inline constexpr auto is_brace_constructible_v = is_brace_constructible<T, Args...>::value;

template<typename T, typename F, typename... Args>
inline constexpr auto is_contructible_from_invoke = std::is_constructible_v<T, detected_t<std::invoke_result_t, F, Args...>> || std::is_invocable_r_v<T, F, Args...>;

template<typename... Tup>
using tuple_cat_t = decltype(std::tuple_cat(std::declval<Tup>()...));

template<typename T>
using tuple_sequence_t = typename detail::tuple_sequence_helper<T>::type;

template<typename T>
constexpr auto tuple_sequence = tuple_sequence_t<T>{};

template<auto N, typename F>
constexpr auto apply_sequence(F function) -> decltype(auto) {
	return std::apply(function, typename detail::tuple_sequence_helper<void, std::make_integer_sequence<decltype(N), N>>::type{});
}

template<typename... Types, typename F>
constexpr auto apply_sequence_for(F function) -> decltype(auto) {
	return std::apply(function, typename detail::tuple_sequence_helper<void, std::index_sequence_for<Types...>>::type{});
}

template<auto n, typename F>
constexpr void for_sequence(F f) {
	apply_sequence<n>([&](auto... s) {
		(void(f(s)), ...);
	});
}

template<typename F, typename... Args>
constexpr void for_sequence(std::tuple<Args...> const&, F f) {
	for_sequence<sizeof...(Args)>(std::move(f));
}

template<typename Tuple, typename F>
constexpr void for_tuple(Tuple&& tuple, F&& function) {
	using T = std::decay_t<Tuple>;
	for_sequence<std::tuple_size_v<T>>([&](auto i) {
		using namespace std;
		using element_t = decltype(get<i>(std::forward<Tuple>(tuple)));
		
		if constexpr (std::is_invocable_v<F&, decltype(i), element_t>) {
			function(i, get<i>(std::forward<Tuple>(tuple)));
		} else {
			function(get<i>(std::forward<Tuple>(tuple)));
		}
	});
}

template<typename... Ts>
constexpr auto tie_tuple(std::tuple<Ts...>& tup) {
	return apply_sequence_for<Ts...>([&](auto... s) {
		return std::tuple<Ts&...>{std::get<s>(tup)...};
	});
}

template<typename... Ts>
constexpr auto tie_tuple(const std::tuple<Ts...>& tup) {
	return apply_sequence_for<Ts...>([&](auto... s) {
		return std::tuple<const Ts&...>{std::get<s>(tup)...};
	});
}

template<typename T>
using has_call_operator = is_detected<detail::call_operator_t, T>;

template<typename T>
inline constexpr auto has_call_operator_v = has_call_operator<T>::value;

} // namespace sbg
