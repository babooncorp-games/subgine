#pragma once

#include <concepts>
#include <type_traits>
#include <utility>
#include <cmath>
#include <cstdint>

namespace sbg {

#if defined(__GNUC__) && !defined(__clang__)
constexpr double tau = 8.0 * std::atan(1.0);
constexpr double pi = tau / 2.0;
#elif !defined(__GNUC__) || defined(__clang__)
constexpr double pi = 3.141592653589793238462643383279502884;
constexpr double tau = pi * 2.0;
#endif

constexpr float pi_f = static_cast<float>(pi);
constexpr float tau_f = static_cast<float>(tau);

constexpr unsigned int freedom(unsigned int n) {
	return n * (n - 1) / 2;
}

template<unsigned short n, typename T>
constexpr auto power(T const& num) {
	if constexpr (n == 0) {
		return T{1};
	} else if (n == 1) {
		return num;
	} else {
		return num * power<n - 1>(num);
	}
}

constexpr auto next_pow2(std::integral auto const n) requires std::is_unsigned_v<decltype(n)> {
#if defined(__GNUC__) && !defined(__clang__)
	using T = std::decay_t<decltype(n)>;
	if constexpr (std::same_as<unsigned long long, T>) {
		return n == 1 ? 1 : 1ull << ((8 * sizeof(unsigned long long)) - __builtin_clzll(n - 1));
	} else if constexpr (std::same_as<unsigned long, T>) {
		return n == 1 ? 1 : 1ul << ((8 * sizeof(unsigned long)) - __builtin_clzl(n - 1));
	} else if constexpr (std::same_as<unsigned int, T>) {
		return n == 1 ? 1 : 1u << ((8 * sizeof(unsigned int)) - __builtin_clz(n - 1));
	}
#endif
	auto v = n - 1u;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	if constexpr (sizeof(v) > sizeof(std::uint8_t)) v |= v >> 8;
	if constexpr (sizeof(v) > sizeof(std::uint16_t)) v |= v >> 16;
	if constexpr (sizeof(v) > sizeof(std::uint32_t)) v |= v >> 32;
	return v + 1u;
}

} // namespace sbg
