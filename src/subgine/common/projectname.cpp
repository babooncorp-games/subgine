#include "projectname.h"

#include <cassert>

static std::string_view project_name_value;

namespace sbg {
	auto assign_project_name(std::string_view const project_name) noexcept -> void {
		static char _ = [&]{
			project_name_value = project_name;
			return '\0';
		}();
	}
	
	auto project_name() noexcept -> std::string_view {
		assert(not project_name_value.empty());
		return project_name_value;
	}
} // namespace sbg
