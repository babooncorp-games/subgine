#pragma once

namespace sbg {

template<typename T>
struct MemberReference {
	constexpr MemberReference(T& r) : ref{&r} {}
	
	constexpr auto operator*() const -> T const& {
		return *ref;
	}
	
	constexpr auto operator->() const -> T const* {
		return ref;
	}
	
	constexpr auto operator*() -> T& {
		return *ref;
	}
	
	constexpr auto operator->() -> T* {
		return ref;
	}
	
private:
	T* ref;
};

} // namespace sbg
