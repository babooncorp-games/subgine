#pragma once

#include "subgine/common/traits.h"
#include <functional>

namespace sbg {

template<typename T>
constexpr void hash_combine(std::size_t& seed, T const& key) noexcept {
	seed ^= std::hash<T>{}(key) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct HashFirst {
	template<typename T1, typename T2>
	std::size_t operator()(const std::pair<T1, T2>& x) const {
		return std::hash<T1>{}(x.first);
	}
};

} // namespace sbg

namespace std {

template<typename T1, typename T2>
struct hash<std::pair<T1, T2>> {
	std::size_t operator()(const std::pair<T1, T2>& x) const {
		std::size_t seed = 0;
		
		sbg::hash_combine(seed, x.first);
		sbg::hash_combine(seed, x.second);
		
		return seed;
	}
};

template<typename... Ts>
struct hash<std::tuple<Ts...>> {
	auto operator()(std::tuple<Ts...> const& x) const -> std::size_t {
		auto seed = std::size_t{0};
		
		sbg::for_sequence<sizeof...(Ts)>([&](auto i) {
			sbg::hash_combine(seed, std::get<i>(x));
		});
		
		return seed;
	}
};

} // namespace std
