#pragma once

namespace sbg {
	template<typename... Ts>
	struct overload : Ts... {
		using Ts::operator()...;
	};
	
	// TODO: Remove when clang is smarter
	template<typename... Ts>
	overload(Ts...) -> overload<Ts...>;
} // namespace sbg
