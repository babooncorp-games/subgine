#pragma once

#include "kangaru/kangaru.hpp"

namespace sbg {
	// TODO: Use an alias in the next visual studio version
	template<auto... functions>
	struct autocall : kgr::autocall<kgr::method<decltype(functions), functions>...> {};
}
