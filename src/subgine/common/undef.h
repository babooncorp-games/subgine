#ifdef SUBGINE_COMMON_DEFINE_H
	#undef SUBGINE_COMMON_DEFINE_H
	#undef FWD
	#undef EXPR
	#undef LIFT
	#undef UNREACHABLE
	#undef INLINE
	#undef NO_UNIQUE_ADDRESS
	#undef SUBGINE_EMPTY_BASES
#else
	#error "Cannot include undef before including define"
#endif
