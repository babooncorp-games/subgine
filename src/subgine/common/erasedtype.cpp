#include "erasedtype.h"

#include <cassert>

using namespace sbg;

void ErasedType::assert_same_type(type_id_t as_type) const noexcept {
	assert(as_type == type());
}

auto ErasedType::pointer() const noexcept -> void const* {
	return _ptr.get();
}

auto ErasedType::pointer() noexcept -> void* {
	return _ptr.get();
}
