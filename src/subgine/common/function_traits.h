#pragma once

#include <tuple>

#include "traits.h"

// I know what I'm doing
// TODO: Not use implementation detail of kangaru
#include "kangaru/detail/meta_list.hpp"

namespace sbg {

template <typename>
struct function_traits_helper {};

template <typename Type, typename R, bool noex, typename... Args>
struct base_function_traits {
	using object_type = Type;
	using return_type = R;
	
	
	// TODO: Not use implementation detail of kangaru
	using argument_types = kgr::detail::meta_list<Args...>;
	template<std::size_t n> using argument_type = kgr::detail::meta_list_element_t<n, argument_types>;
	static constexpr std::size_t arity = sizeof...(Args);
	static constexpr bool is_noexcept = noex;
};

template <typename R, bool noex, typename... Args>
struct base_non_member_function_traits {
	using return_type = R;
	
	// TODO: Not use implementation detail of kangaru
	using argument_types = kgr::detail::meta_list<Args...>;
	template<std::size_t n> using argument_type = kgr::detail::meta_list_element_t<n, argument_types>;
	constexpr static std::size_t arity = sizeof...(Args);
	static constexpr bool is_noexcept = noex;
};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...)> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const &&> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) &&> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const &> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) &> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...)> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const &&> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) &&> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const &> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) &> : base_function_traits<Type, R, false, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const && noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) && noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) const & noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args...) & noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const && noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) && noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) const & noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename Type, typename R, typename... Args>
struct function_traits_helper<R(Type::*)(Args..., ...) & noexcept> : base_function_traits<Type, R, true, Args...> {};

template <typename R, typename... Args>
struct function_traits_helper<R(*)(Args...)> : base_non_member_function_traits<R, false, Args...> {};

template <typename R, typename... Args>
struct function_traits_helper<R(*)(Args..., ...)> : base_non_member_function_traits<R, false, Args...> {};

template <typename R, typename... Args>
struct function_traits_helper<R(*)(Args...) noexcept> : base_non_member_function_traits<R, true, Args...> {};

template <typename R, typename... Args>
struct function_traits_helper<R(*)(Args..., ...) noexcept> : base_non_member_function_traits<R, true, Args...> {};

template<typename F, typename = void>
struct function_traits : function_traits_helper<F> {};

template<typename T>
struct function_traits<T, std::enable_if_t<has_call_operator_v<T>>> : function_traits_helper<decltype(&T::operator())> {};

template <typename F>
using function_arguments_t = typename function_traits<F>::argument_types;

template <typename F>
using function_result_t = typename function_traits<F>::return_type;

template <typename F>
constexpr std::size_t function_arity_v = function_traits<F>::arity;

template <typename F>
inline constexpr bool function_noexcept_v = function_traits<F>::is_noexcept;

template <typename F>
using object_type_t = typename function_traits<F>::object_type;

template <std::size_t n, typename F>
using function_argument_t = typename function_traits<F>::template argument_type<n>;

template<typename T>
inline constexpr auto is_function_v = is_detected_v<function_result_t, T>;

} // namespace sbg
