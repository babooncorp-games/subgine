#ifndef SUBGINE_COMMON_DEFINE_H
#define SUBGINE_COMMON_DEFINE_H
	#define FWD(...) static_cast<decltype(__VA_ARGS__)&&>(__VA_ARGS__)
	
	#define EXPR(...) \
		noexcept(noexcept(__VA_ARGS__)) \
		-> decltype(__VA_ARGS__) { \
			return __VA_ARGS__; \
		}
	
	#define LIFT(func) [&](auto&&... args) EXPR(func(FWD(args)...))
	
	// TODO: C++23 remove UNREACHABLE
	#if defined(_MSC_VER)
		#define UNREACHABLE __assume(0)
		#define INLINE __forceinline
		#define NO_UNIQUE_ADDRESS [[msvc::no_unique_address]]
		#define SUBGINE_EMPTY_BASES __declspec(empty_bases)
	#elif defined(__GNUC__) || defined(__clang__)
		#define UNREACHABLE __builtin_unreachable()
		#define INLINE inline __attribute__((always_inline))
		#define NO_UNIQUE_ADDRESS [[no_unique_address]]
		#define SUBGINE_EMPTY_BASES
	#else
		#define UNREACHABLE
		#define NO_UNIQUE_ADDRESS [[no_unique_address]]
		#define INLINE inline
		#define SUBGINE_EMPTY_BASES
	#endif
#else
	#error "A previous header leaked the macros"
#endif
