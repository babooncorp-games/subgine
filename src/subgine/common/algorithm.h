#pragma once

namespace sbg {

template<typename ContainerT, typename PredicateT>
void erase_if(ContainerT& items, PredicateT predicate) {
	for (auto it = items.begin(); it != items.end();) {
		if (predicate(*it)) {
			it = items.erase(it);
		} else {
			++it;
		}
	}
}

} // namespace sbg
