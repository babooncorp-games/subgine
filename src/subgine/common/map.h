#pragma once

#include "emilib/hash_map.hpp"
#include "hash.h"

namespace sbg {

template<typename T>
using dictionary = emilib::hash_map<std::string, T, string_murmur_hash>;

template<typename K, typename V, typename H = std::hash<K>>
using hash_map = emilib::hash_map<K, V, H>;

} // namespace sbg
