#pragma once

#include "kangaru/type_id.hpp"

namespace sbg {

using type_id_t = kgr::type_id_t;

template <typename T>
constexpr type_id_t type_id = kgr::type_id<T>();

inline auto constexpr null_type_id = type_id_t{};

} // namespace sbg
