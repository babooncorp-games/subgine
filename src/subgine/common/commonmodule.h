#pragma once

#include "kangaru.h"

namespace sbg {

struct CommonModuleService;

struct CommonModule {
	void setupContainer(kgr::container& container) const;
	
	friend auto service_map(const CommonModule&) -> CommonModuleService;
};

} // namespace sbg
