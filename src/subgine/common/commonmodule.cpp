#include "commonmodule.h"

#include "service/rootcontainerservice.h"

using namespace sbg;

void CommonModule::setupContainer(kgr::container& container) const {
	container.emplace<RootContainerService>(container);
}
