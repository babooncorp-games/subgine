#pragma once

#include <type_traits>

namespace sbg {

struct nonesuch {
	nonesuch() = delete;
	~nonesuch() = delete;
	nonesuch(nonesuch const&) = delete;
	void operator=(nonesuch const&) = delete;
	nonesuch(nonesuch&&) = delete;
	void operator=(nonesuch&&) = delete;
};

namespace detail::detection {
	template <typename Default, typename AlwaysVoid, template<typename...> typename Op, typename... Args>
	struct detector {
		using value_t = std::false_type;
		using type = Default;
	};

	template <typename Default, template<typename...> typename Op, typename... Args>
	struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
		using value_t = std::true_type;
		using type = Op<Args...>;
	};

	/*
	* This higher order metafunction only instantiate a template using `detector` if a condition is satisfied.
	* Useful when the instantiation of a template is known to trigger a hard error.
	*/
	template<bool, typename Default, template<typename...> class, typename...>
	struct instantiate_if {
		using type = Default;
	};

	template<typename Default, template<typename...> class Template, typename... Args>
	struct instantiate_if<true, Default, Template, Args...> {
		using type = typename detector<Default, void, Template, Args...>::type;
	};
} // namespace detail::detection

template <template<typename...> typename Op, typename... Args>
using is_detected = typename detail::detection::detector<nonesuch, void, Op, Args...>::value_t;

template <template<typename...> typename Op, typename... Args>
constexpr auto is_detected_v = detail::detection::detector<nonesuch, void, Op, Args...>::value_t::value;

template <template<typename...> typename Op, typename... Args>
using detected_t = typename detail::detection::detector<nonesuch, void, Op, Args...>::type;

template <typename Default, template<typename...> typename Op, typename... Args>
using detected_or = typename detail::detection::detector<Default, void, Op, Args...>::type;

template <bool b, template<class...> class Template, typename... Args>
using instantiate_if_t = typename detail::detection::instantiate_if<b, nonesuch, Template, Args...>::type;

template <bool b, typename Default, template<class...> class Template, typename... Args>
using instantiate_if_or = typename detail::detection::instantiate_if<b, Default, Template, Args...>::type;

} // namespace sbg
