#pragma once

#include "../commonmodule.h"
#include "../kangaru.h"

namespace sbg {

struct CommonModuleService : kgr::single_service<CommonModule>,
	autocall<&CommonModule::setupContainer> {};

} // namespace sbg
