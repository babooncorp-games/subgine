#pragma once

#include "../kangaru.h"

namespace sbg {

struct RootContainerService : kgr::extern_service<kgr::container> {};

} // namespace sbg
