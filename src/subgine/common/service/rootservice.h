#pragma once

#include "rootcontainerservice.h"
#include "../kangaru.h"

#include <functional>

namespace sbg {
namespace detail {

template<typename T, typename D>
struct root_single_service : kgr::single_service<T, D> {};

template<typename T, typename D>
using root_service_value = kgr::lazy<root_single_service<T, D>>;

} // namespace detail

template<typename T, typename D = kgr::dependency<>>
struct root_service : kgr::generic_service<detail::root_service_value<T, D>> {
	using kgr::generic_service<detail::root_service_value<T, D>>::generic_service;
	
	constexpr static auto construct(kgr::inject_t<RootContainerService> c) -> kgr::inject_result<kgr::container&> {
		return kgr::inject(c.forward());
	}
	
	auto forward() -> T& {
		return *instance();
	}
	
	template<typename M, typename... Args>
	auto call(M method, Args&&... args) -> std::invoke_result_t<M, T&, Args...> {
		return std::invoke(method, *instance(), std::forward<Args>(args)...);
	}
	
private:
	using kgr::generic_service<detail::root_service_value<T, D>>::instance;
};

} // namespace sbg
