#pragma once

#include <vector>
#include <cstdint>
#include <type_traits>
#include <limits>
#include <cassert>
#include <concepts>
#include <bit>
#include <iterator>
#include <compare>
#include <memory>
#include <algorithm>
#include <utility>
#include <cstring>

#include "define.h"

namespace sbg {

template<std::uint16_t I, std::uint16_t G>
using basic_slot_list_id_int = std::conditional_t<I + G <= 8,
	std::uint8_t,
	std::conditional_t<I + G <= 16,
		std::uint16_t,
		std::conditional_t<I + G <= 32,
			std::uint32_t,
			std::uint64_t
		>
	>
>;

template<std::uint16_t I, std::uint16_t G>
struct basic_slot_list_id {
	using representation = basic_slot_list_id_int<I, G>;
	
	std::uint64_t index : I = static_cast<std::uint64_t>((1ull << I) - 2);
	std::uint64_t generation : G = 0;

	friend auto operator<=>(basic_slot_list_id const&, basic_slot_list_id const&) = default;
	friend auto operator==(basic_slot_list_id const&, basic_slot_list_id const&) -> bool = default;
	
	[[nodiscard]]
	constexpr auto value() const noexcept -> representation {
		return static_cast<representation>(generation | index << G);
	}
	
	[[nodiscard]]
	static constexpr auto from_value(representation const value) noexcept -> basic_slot_list_id {
		return basic_slot_list_id{
			.index = static_cast<std::uint64_t>(value >> G & (1ull << I) - 1),
			.generation = static_cast<std::uint64_t>(value & (1ull << G) - 1),
		};
	}
	
	[[nodiscard]]
	constexpr auto unassigned() const -> bool {
		return index == static_cast<std::uint64_t>((1ull << I) - 2);
	}
};

template<typename T, std::uint16_t I, std::uint64_t G>
struct basic_slot_list {
private:
	using _key_type = basic_slot_list_id<I, G>;
	using _const_reference = std::pair<_key_type, T const&>;
	using _reference = std::pair<_key_type, T&>;
	using _size_type = typename _key_type::representation;
	
	static constexpr auto no_index = static_cast<_size_type>((1ull << I) - 1);
	static constexpr auto alive = static_cast<_size_type>(no_index - 1);
	
	struct empty_byte {};
	union storage {
		constexpr storage() noexcept : empty{} {}
		
		// We never let a storage go out of scope with value engaged
		constexpr ~storage() noexcept { std::destroy_at(&empty); }
		
		empty_byte empty;
		T value;
	};
	
	template<bool constant>
	struct dereference_wrapper {
	private:
		using object_type = std::conditional_t<constant, T const, T>;
		using reference = std::pair<_key_type, object_type&>;
		
	public:
		explicit constexpr dereference_wrapper(reference value) noexcept : value{value} {}
		
		constexpr auto operator->() noexcept -> reference* {
			return &value;
		}
		
	private:
		reference value;
	};
	
	template<bool constant>
	struct basic_iterator {
	private:
		using object_type = std::conditional_t<constant, T const, T>;
		using storage_pointer = std::conditional_t<constant, storage const*, storage*>;
		
	public:
		using iterator_category = std::forward_iterator_tag;
		using difference_type = std::ptrdiff_t;
		using value_type = std::pair<_key_type, object_type>;
		using pointer = dereference_wrapper<constant>;
		using reference = std::pair<_key_type, object_type&>;
		
		basic_iterator() = default;
		constexpr basic_iterator(storage_pointer it, _key_type const* key, _key_type const* first_key, _key_type const* last_key) noexcept :
			it{it}, key{key}, first_key{first_key}, last_key{last_key} {}

		friend auto operator==(basic_iterator const&, basic_iterator const&) noexcept -> bool = default;
		friend auto operator<=>(basic_iterator const&, basic_iterator const&) noexcept = default;
		
		constexpr auto operator++() -> basic_iterator& {
			do {
				++it;
				++key;
			} while(key != last_key and key->index != alive);
			
			return *this;
		}
		
		constexpr auto operator++(int) -> basic_iterator {
			auto const self = *this;
			++(*this);
			return self;
		}
		
		constexpr auto operator*() const noexcept -> reference {
			auto const generation = key->generation;
			return reference{
				_key_type{
					.index = static_cast<_size_type>(key - first_key),
					.generation = generation,
				},
				it->value,
			};
		}
		
		constexpr auto operator->() const noexcept -> pointer {
			auto const generation = key->generation;
			// The operator-> will recursively be called on
			// the "pointer" type until it's really a pointer 
			return pointer{
				reference{
					_key_type{
						.index = static_cast<_size_type>(key - first_key),
						.generation = generation,
					},
					it->value,
				},
			};
		}
		
	private:
		storage_pointer it = nullptr;
		_key_type const* key = nullptr;
		_key_type const* first_key = nullptr;
		_key_type const* last_key = nullptr;
	};
	
public:
	using key_type = _key_type;
	using mapped_type = T;
	using value_type = std::pair<key_type const, mapped_type>;
	using size_type = _size_type;
	using difference_type = std::ptrdiff_t;
	using reference = _reference;
	using const_reference = std::pair<key_type, mapped_type const&>;
	using pointer = dereference_wrapper<false>;
	using const_pointer = dereference_wrapper<true>;
	using iterator = basic_iterator<false>;
	using const_iterator = basic_iterator<true>;
	
	basic_slot_list() = default;
	
	basic_slot_list(basic_slot_list const& other) : free{other.free} {
		if (other.size) {
			reserve_at_least(other.size);
			other.copy_to_new_buffer(entities.get(), generations.get(), other.size);
			size = other.size;
		}
	}
	
	auto operator=(basic_slot_list const& lhs) -> basic_slot_list& {
		if (this == &lhs) return *this;
		free = lhs.free;
		if (lhs.size) {
			reserve_at_least(lhs.size);
			lhs.copy_to_new_buffer(entities.get(), generations.get(), lhs.size);
			size = lhs.size;
		}
		return *this;
	}
	
	basic_slot_list(basic_slot_list&& other) noexcept :
		free{std::exchange(other.free, {})},
		size{std::exchange(other.size, {})},
		capacity{std::exchange(other.capacity, {})},
		generations{std::exchange(other.generations, {})},
		entities{std::exchange(other.entities, {})} {}
	
	auto operator=(basic_slot_list&& lhs) noexcept -> basic_slot_list& {
		if (this == &lhs) return *this;
		std::swap(free, lhs.free);
		std::swap(size, lhs.size);
		std::swap(capacity, lhs.capacity);
		std::swap(generations, lhs.generations);
		std::swap(entities, lhs.entities);
		return *this;
	}
	
	// TODO: Use a constexpr enable unique_ptr to add constexpr
	~basic_slot_list() {
		if (entities and generations) {
			deallocate(std::move(entities), std::move(generations), size);
		}
	}
	
	auto emplace(auto&&... args) -> key_type requires(std::constructible_from<T, decltype(args)...>) {
		if (free == no_index) {
			assert(size != max_size());
			auto const index = size;
			reserve_at_least(size + 1);
			std::construct_at(&entities[index].value, FWD(args)...);
			auto const key = generations[index] = key_type{.index = alive, .generation = 0};
			++size;
			return key_type{.index = index, .generation = key.generation};
		} else {
			auto& id = generations[free];
			auto const index = free;
			std::construct_at(&entities[free].value, FWD(args)...);
			free = id.index;
			id.index = alive;
			return key_type{.index = index, .generation = id.generation};
		}
	}
	
	auto erase(key_type const key) -> void {
		assert(key.index < size);
		if (key.generation == generations[key.index].generation) {
			auto& entity = entities[key.index];
			std::destroy_at(&entity.value);
			entity.empty = empty_byte{};
			
			auto& id = generations[key.index];
			id.generation++;
			id.index = free;
			free = key.index;
		}
	}
	
	auto contains(key_type const key) const noexcept -> bool {
		if (key.index < size) {
			auto const id = generations[key.index];
			if (id.index == alive and id.generation == key.generation) {
				return true;
			}
		}
		
		return false;
	}
	
	auto clear() -> void {
		if (entities and generations) {
			auto const current_size = size;
			
			// TODO: C++23 use zip_view
			for (auto index = size_type{0}; index < current_size; ++index) {
				auto& entity = entities[index];
				auto& id = generations[index];
				
				if (id.index == alive) {
					if constexpr (not std::is_trivially_constructible_v<T>) {
						std::destroy_at(&entity.value);
						entity.empty = empty_byte{};
					}
					id.generation++;
					id.index = no_index;
				}
			}
			
			size = 0;
		} else {
			size = 0;
			capacity = 0;
		}
	}
	
	static constexpr auto max_size() noexcept -> size_type {
		return no_index - 2;
	}
	
	auto find(key_type const key) -> iterator {
		if (not contains(key)) return end();
		return iterator{entities.get() + key.index, generations.get() + key.index, generations.get(), generations.get() + size};
	}
	
	auto find(key_type const key) const -> const_iterator {
		if (not contains(key)) return end();
		return const_iterator{entities.get() + key.index, generations.get() + key.index, generations.get(), generations.get() + size};
	}
	
	auto at(key_type const key) -> T& {
		if (not contains(key)) std::abort();
		return entities[key.index].value;
	}
	
	auto at(key_type const key) const -> T const& {
		if (not contains(key)) std::abort();
		return entities[key.index].value;
	}
	
	auto operator[](key_type const key) -> T& {
		assert(contains(key));
		return entities[key.index].value;
	}
	
	auto operator[](key_type const key) const -> T const& {
		assert(contains(key));
		return entities[key.index].value;
	}
	
	auto reserve(size_type const at_least) {
		reserve_at_least(at_least);
	}
	
	auto begin() -> iterator {
		auto const first = first_living_index();
		return iterator{entities.get() + first, generations.get() + first, generations.get(), generations.get() + size};
	}
	
	auto begin() const -> const_iterator {
		auto const first = first_living_index();
		return const_iterator{entities.get() + first, generations.get() + first, generations.get(), generations.get() + size};
	}
	
	auto cbegin() const -> const_iterator {
		auto const first = first_living_index();
		return const_iterator{entities.get() + first, generations.get() + first, generations.get(), generations.get() + size};
	}
	
	auto end() -> iterator {
		return iterator{entities.get() + size, generations.get() + size, generations.get(), generations.get() + size};
	}
	
	auto end() const -> const_iterator {
		return const_iterator{entities.get() + size, generations.get() + size, generations.get(), generations.get() + size};
	}
	
	auto cend() const -> const_iterator {
		return const_iterator{entities.get() + size, generations.get() + size, generations.get(), generations.get() + size};
	}
	
private:
	auto reserve_at_least(size_type const at_least) -> void {
		assert(at_least <= max_size());
		
		if (at_least > capacity) {
			auto const new_capacity = grow(at_least);
			auto new_entities = std::make_unique<storage[]>(new_capacity);
			auto new_generations = std::make_unique<key_type[]>(new_capacity);
			
			if (size > 0) {
				// Might throw if copy constructor is selected
				move_to_new_buffer(new_entities.get(), new_generations.get(), size);
			}
			
			deallocate(
				std::exchange(entities, std::move(new_entities)),
				std::exchange(generations, std::move(new_generations)),
				size
			);
			capacity = new_capacity;
		}
	}
	
	auto copy_to_new_buffer(storage* new_entities, key_type* new_generations, size_type current_size) -> void {
		auto it_new_entities = new_entities;
		auto it_new_generations = new_generations;
		auto it_entities = entities.get();
		auto it_generations = generations.get();
		
		if constexpr (not std::is_trivially_copyable_v<T>) {
			auto const end = it_generations + size;
			for (; it_generations < end; ++it_generations) {
				if (it_generations->index == alive) {
					std::construct_at(std::addressof((it_new_entities++)->value), (it_entities++)->value);
				} else {
					std::construct_at(std::addressof((it_new_entities++)->empty));
				}
				
				*it_new_generations++ = *it_generations;
			}
		} else {
			std::memcpy(it_new_entities, entities.get(), current_size * sizeof(storage));
			std::memcpy(it_new_generations, generations.get(), current_size * sizeof(key_type));
		}
	}
	
	auto move_to_new_buffer(storage* new_entities, key_type* new_generations, size_type current_size) -> void {
		auto it_new_entities = new_entities;
		auto it_new_generations = new_generations;
		auto it_entities = entities.get();
		auto it_generations = generations.get();
		
		if constexpr (not std::is_trivially_copyable_v<T>) {
			auto const end = it_generations + size;
			for (; it_generations < end; ++it_generations) {
				if (it_generations->index == alive) {
					std::construct_at(std::addressof((it_new_entities++)->value), std::move_if_noexcept((it_entities++)->value));
				} else {
					std::construct_at(std::addressof((it_new_entities++)->empty));
				}
				
				*it_new_generations++ = *it_generations;
			}
		} else {
			std::memcpy(it_new_entities, entities.get(), current_size * sizeof(storage));
			std::memcpy(it_new_generations, generations.get(), current_size * sizeof(key_type));
		}
	}
	
	static auto deallocate(std::unique_ptr<storage[]> entities, std::unique_ptr<key_type[]> generations, std::size_t current_size) {
		if constexpr (not std::is_trivially_destructible_v<T>) {
			auto it_entities = entities.get();
			auto it_generations = generations.get();
			
			for (auto index = size_type{0}; index < current_size; ++index) {
				if (it_generations->index == alive) {
					std::destroy_at(&(it_entities)->value);
					it_entities->empty = empty_byte{};
					++it_entities;
				}
				++it_generations;
			}
		}
	}
	
	static auto grow(size_type const n) -> size_type {
		return std::clamp(static_cast<size_type>((3 * n) / 2), size_type{16}, max_size());
	}
	
	auto first_living_index() const -> size_type {
		auto const begin = generations.get();
		auto const end = generations.get() + size;
		
		auto key = begin;
		while (key != end and key->index != alive) {
			++key;
		}
		
		return static_cast<size_type>(key - begin);
	}
	
	std::unique_ptr<storage[]> entities;
	std::unique_ptr<key_type[]> generations;
	size_type capacity = 0;
	size_type size = 0;
	size_type free = no_index;
};

template<typename T>
using small_slot_list = basic_slot_list<T, 12, 4>;

template<typename T>
using slot_list = basic_slot_list<T, 24, 8>;

template<typename T>
using large_slot_list = basic_slot_list<T, 48, 16>;

using small_slot_list_id = basic_slot_list_id<12, 4>;
using slot_list_id = basic_slot_list_id<24, 8>;
using large_slot_list_id = basic_slot_list_id<48, 16>;

} // namespace sbg


template<std::uint16_t I, std::uint16_t G>
struct std::hash<sbg::basic_slot_list_id<I, G>> {
	auto operator()(sbg::basic_slot_list_id<I, G> const& id) const noexcept -> std::size_t {
		return std::hash<sbg::basic_slot_list_id_int<I, G>>{}(id.value());
	}
};


#include "undef.h"
