#pragma once

#include "subgine/common/traits.h"

#include <vector>
#include <memory>

namespace sbg {

template<typename T, typename Alloc = typename std::vector<T>::allocator_type>
struct ImmutableVector {
	using box_type = std::vector<T, Alloc>;
	using value_type = typename box_type::value_type;
	using allocator_type = typename box_type::allocator_type;
	using size_type = typename box_type::size_type;
	using difference_type = typename box_type::difference_type;
	using reference = typename box_type::const_reference;
	using pointer = typename box_type::const_pointer;
	using const_reference = typename box_type::const_reference;
	using const_pointer = typename box_type::const_pointer;
	using iterator = typename box_type::const_iterator;
	using const_iterator = typename box_type::const_iterator;
	using reverse_iterator = typename box_type::const_reverse_iterator;
	using const_reverse_iterator = typename box_type::const_reverse_iterator;
	
	ImmutableVector() : _value{std::make_shared<box_type>()} {}
	
	ImmutableVector(ImmutableVector const&) = default;
	ImmutableVector(ImmutableVector&&) = default;
	auto operator=(ImmutableVector const&) -> ImmutableVector&  = default;
	auto operator=(ImmutableVector&&) -> ImmutableVector& = default;
	
	template<typename Arg1, typename... Args, enable_if_i<is_not_self_v<ImmutableVector, Arg1> && (std::is_constructible_v<std::vector<T>, Args...> || is_brace_constructible_v<std::vector<T>, Args...>)> = 0>
	ImmutableVector(Arg1&& arg1, Args&&... args) : _value{make_value(std::forward<Arg1>(arg1), std::forward<Args>(args)...)} {}
	
	// We replicate std::vector behavior here
	ImmutableVector(std::initializer_list<T> init) : _value{make_value(init)} {}
	
	template<typename F, enable_if_i<std::is_invocable_r_v<box_type, F, box_type const&>> = 0>
	auto update(F function) const noexcept(std::is_nothrow_invocable_v<F, box_type const&>) -> ImmutableVector {
		return ImmutableVector{std::invoke(function, *_value)};
	}
	
	auto get_allocator() const noexcept -> allocator_type {
		return _value->get_allocator();
	}
	
	auto at(size_type index) const -> const_reference {
		return _value->at(index);
	}
	
	auto operator[](size_type index) const -> const_reference {
		return (*_value)[index];
	}
	
	auto front() const -> const_reference {
		return _value->front();
	}
	
	auto back() const -> const_reference {
		return _value->back();
	}
	
	auto data() const noexcept -> T* {
		return _value->data();
	}
	
	auto begin() const noexcept -> const_iterator {
		return _value->begin();
	}
	
	auto end() const noexcept -> const_iterator {
		return _value->end();
	}
	
	auto rbegin() const noexcept -> const_reverse_iterator {
		return _value->rbegin();
	}
	
	auto rend() const noexcept -> const_reverse_iterator {
		return _value->rend();
	}
	
	auto cbegin() const noexcept -> const_iterator {
		return _value->cbegin();
	}
	
	auto cend() const noexcept -> const_iterator {
		return _value->cend();
	}
	
	auto empty() const noexcept -> bool {
		return _value->emtpy();
	}
	
	auto size() const noexcept -> size_type {
		return _value->size();
	}
	
	auto max_size() const noexcept -> size_type {
		return _value->max_size();
	}
	
	auto capacity() const noexcept -> size_type {
		return _value->capacity();
	}
	
	// TODO: C++20 add operator<=>
	
private:
	template<typename... Args>
	static auto make_value(Args&&... args) {
		if constexpr(std::is_constructible_v<std::vector<T>, Args...>) {
			return std::make_shared<box_type>(std::forward<Args>(args)...);
		} else {
			return std::make_shared<box_type>(std::vector<T>{std::forward<Args>(args)...});
		}
	}
	
	std::shared_ptr<box_type const> _value;
};

template<
	typename InputIt,
	typename Alloc = std::allocator<typename std::iterator_traits<InputIt>::value_type>
>
ImmutableVector(InputIt, InputIt, Alloc = Alloc()) -> ImmutableVector<typename std::iterator_traits<InputIt>::value_type, Alloc>;

// TODO: C++20 Add erase_if, erase

} // namesapce sbg
