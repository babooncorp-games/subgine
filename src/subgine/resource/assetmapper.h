#pragma once

#include "gamedatabase.h"

#include <string>
#include <string_view>
#include <filesystem>

namespace sbg {

struct AssetMapperService;

struct AssetMapper {
	explicit AssetMapper(GameDatabase& database) noexcept;
	
	auto relative_from_name(std::string_view table_to, std::string_view table_from, std::string_view name, std::filesystem::path relative) const -> std::string;
	auto relative_from_path(std::string_view table, std::filesystem::path base, std::filesystem::path relative) const -> std::string;
	
private:
	GameDatabase* _database;
	
	static auto to_asset_path(std::filesystem::path relativePath, std::filesystem::path base) -> std::filesystem::path;
	friend auto service_map(AssetMapper const&) -> AssetMapperService;
};

} // namespace sbg
