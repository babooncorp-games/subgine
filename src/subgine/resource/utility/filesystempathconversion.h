#pragma once

#include "../json.h"

#include <filesystem>

template<>
struct nlohmann::adl_serializer<std::filesystem::path> {
	static void to_json(sbg::resource::json& j, std::filesystem::path const& path) {
		j = path.string();
	}
	
	static void from_json(sbg::resource::json const& j, std::filesystem::path& path) {
		path = j.get_ref<std::string const&>();
	}
};