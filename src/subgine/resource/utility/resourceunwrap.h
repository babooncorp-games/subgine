#pragma once

#include "../resourcehandle.h"

#include <ranges>

namespace sbg {
	struct ResourceUnwrap {
		template<typename T>
		constexpr auto operator()(ResourceHandle<T> const& resource) const& noexcept -> T const& {
			return resource.resource();
		}
		
		template<typename T>
		constexpr auto operator()(ResourceHandle<T>& resource) const& noexcept -> T& {
			return resource.resource();
		}
		
		template<typename T>
		constexpr auto operator()(ResourceHandle<T>&& resource) const& noexcept -> T&& {
			return std::move(resource).resource();
		}
	} inline constexpr resourceUnwrap;
}

namespace sbg::views {
	inline constexpr auto unwrap_resources = std::views::transform(resourceUnwrap);
}
