#pragma once

#include "../property.h"
#include "../resourcetag.h"
#include "subgine/vector/vectorfwd.h"

#include <optional>

namespace sbg {

template<typename T>
auto value_or(Property const& property, T const& value) -> std::decay_t<T const&> {
	if constexpr (std::is_integral_v<T> && !std::is_same_v<bool, T> && !std::is_pointer_v<T>)
	{
		return property.isInt() ? property.as<T>() : value;
	}
	else if constexpr (std::is_floating_point_v<T>)
	{
		return property.isFloat() ? property.as<T>() : value;
	}
	else if constexpr (std::is_same_v<bool, T>)
	{
		return property.isBool() ? property.as<T>() : value;
	}
	else if constexpr (std::is_same_v<std::string, T>)
	{
		return property.isString() ? std::string{property} : value;
	}
	else if constexpr (std::is_same_v<std::string_view, T>)
	{
		return property.isString() ? std::string_view{property} : value;
	}
	else if constexpr (std::is_same_v<char const*, std::decay_t<T const&>>)
	{
		return property.isString() ? static_cast<char const*>(property) : value;
	}
	else if constexpr (std::is_same_v<std::filesystem::path, T>)
	{
		return property.isString() ? property.as<T>() : value;
	}
	else if constexpr (resource_tag<T>)
	{
		return property.isString() or property.isInt() ? property.as<T>() : value;
	}
	else if constexpr (is_vector<T>)
	{
		return property.isArray() and property.size() == T::size ? property.as<T>() : value;
	}
	
	static_assert(
		resource_tag<T> ||
		is_vector<T> ||
		std::is_integral_v<T> ||
		std::is_floating_point_v<T> ||
		std::is_same_v<std::string, T> ||
		std::is_same_v<std::string_view, T> ||
		std::is_same_v<std::filesystem::path, T> ||
		std::is_same_v<char const*, std::decay_t<T>>,
		"The converting type must be a fundamental type, a sbg::Vector type, a resource tag or a string type"
	);
}

template<typename T>
auto value_optional(Property const& property) -> std::optional<T> {
	using R = std::optional<std::decay_t<T const&>>;
	
	if constexpr (std::is_integral_v<T> && !std::is_same_v<bool, T> && !std::is_pointer_v<T>)
	{
		return property.isNumeric() ? R{property.as<T>()} : R{};
	}
	else if constexpr (std::is_floating_point_v<T>)
	{
		return property.isNumeric() ? R{property.as<T>()} : R{};
	}
	else if constexpr (std::is_same_v<bool, T>)
	{
		return property.isBool() ? R{property.as<T>()} : R{};
	}
	else if constexpr (std::is_same_v<std::string, T>)
	{
		return property.isString() ? R{std::string{property}} : R{};
	}
	else if constexpr (std::is_same_v<std::string_view, T>)
	{
		return property.isString() ? R{std::string_view{property}} : R{};
	}
	else if constexpr (std::is_same_v<char const*, std::decay_t<T const&>>)
	{
		return property.isString() ? R{static_cast<char const*>(property)} : R{};
	}
	else if constexpr (std::is_same_v<std::filesystem::path, T>)
	{
		return property.isString() ? R{property.as<T>()} : R{};
	}
	else if constexpr (resource_tag<T>)
	{
		return property.isString() or property.isInt() ? R{property.as<T>()} : R{};
	}
	else if constexpr (is_vector<T>)
	{
		return property.isArray() and property.size() == T::size ? R{property.as<T>()} : R{};
	}
	
	static_assert(
		resource_tag<T> ||
		is_vector<T> ||
		std::is_integral_v<T> ||
		std::is_floating_point_v<T> ||
		std::is_same_v<std::string, T> ||
		std::is_same_v<std::string_view, T> ||
		std::is_same_v<std::filesystem::path, T> ||
		std::is_same_v<char const*, std::decay_t<T>>,
		"The converting type must be a fundamental type, a sbg::Vector, a resource tag or a string type"
	);
}

} // namespace sbg
