#pragma once

#include <exception>

namespace sbg {

struct PropertyNotFound : std::exception {
	PropertyNotFound(const char* message);
	const char * what() const noexcept override;
	
private:
	const char* _message;
};

} // namespace sbg
