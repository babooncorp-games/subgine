#pragma once

#include "../json.h"
#include "subgine/common/traits.h"
#include "subgine/vector/vector.h"

namespace sbg {

template<dim_t n, typename T>
void to_json(resource::json& json, Vector<n, T> const& vector) {
	using json_type = std::decay_t<decltype(json)>;
	apply_sequence<n>([&](auto... s) {
		json = json_type::array({get<s>(vector)...});
	});
}

template<dim_t n, typename T>
void from_json(resource::json const& json, Vector<n, T>& vector) {
	for_sequence<n>([&](auto i) {
		json.at(i).get_to(get<i>(vector));
	});
}

} // namespace sbg
