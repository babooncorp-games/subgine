#include "propertynotfound.h"

namespace sbg {

PropertyNotFound::PropertyNotFound(const char* message) : _message{message} {}

const char* PropertyNotFound::what() const noexcept {
	return _message;
}

} // namespace sbg
