#pragma once

#include "../hash.h"
#include "../json.h"
#include "../resourcetag.h"

#include <string_view>

namespace sbg {

void to_json(resource::json& json, resource_tag auto tag) {
	json = static_cast<std::size_t>(tag.hash);
}

void from_json(resource::json const& json, resource_tag auto& tag) {
	if (json.is_number_integer()) {
		tag.hash = static_cast<hash_t>(json.get<std::size_t>());
	} else if (json.is_string()) {
		tag.hash = murmur64a(json.get<std::string_view>());
	}
}

} // namespace sbg
