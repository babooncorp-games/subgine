#include "filetostring.h"

#include <fstream>

namespace sbg {

std::string fileToString(std::ifstream in) {
	std::string content;
	
	if (in) {
		in.seekg(0, std::ios::end);
		content.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&content[0], content.size());
		in.close();
	}
	
	return content;
}

} // namespace sbg
