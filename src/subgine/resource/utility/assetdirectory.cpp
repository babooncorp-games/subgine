#include "assetdirectory.h"

#include "subgine/common/projectname.h"

#include <cpplocate/cpplocate.h>

namespace sbg {
	
auto asset_directory() -> std::filesystem::path {
	auto const executable_directory = std::filesystem::path{cpplocate::getExecutablePath()}.parent_path().parent_path();
	return executable_directory / "share" / project_name();
}

}
