#pragma once

#include "subgine/common/traits.h"
#include "subgine/common/kangaru.h"

namespace sbg {

template<typename, typename S, typename>
inline constexpr auto is_builder = []{
	static_assert(std::is_function_v<S>, "The signature of the builder must be declared like <return-value>(<arguments>...)");
	return false;
}();

template<typename B, typename R, typename... Args, typename Apply>
inline constexpr auto is_builder<B, R(Args...), Apply> = is_contructible_from_invoke<R, Apply const&, kgr::invoker, B const&, Args...>;

namespace detail::resource {
	template<typename T>
	using path_t = decltype(T::path);
	
	template<typename T>
	using free_t = decltype(std::declval<T&>().free());
}

template<typename T>
inline constexpr bool has_path = is_detected_v<detail::resource::path_t, T>;

template<typename T>
inline constexpr bool is_managed = !is_detected_v<detail::resource::free_t, T> && std::is_copy_constructible_v<T>;

} // namespace sbg
