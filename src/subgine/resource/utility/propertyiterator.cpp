#include "propertyiterator.h"

#include "../property.h"
#include "../detail/entrytyperesolver.h"

using namespace sbg;

PropertyIterator::PropertyIterator(resource::json::const_iterator iterator, detail::EntryTypeResolver* resolver) :
	_iterator{iterator}, _resolver{resolver} {}

auto PropertyIterator::operator==(PropertyIterator const& other) const noexcept -> bool {
	return other._iterator == _iterator;
}

auto PropertyIterator::operator*() const -> Property {
	auto const& json = *_iterator;
	
	if (_resolver && json.is_object()) {
		auto const itEntryType = json.find("$entry-type");
		
		if (itEntryType != json.end()) {
			return _resolver->toProperty(itEntryType.value().get<std::string_view>(), json);
		}
	}
	
	return Property{&json, _resolver};
}

auto PropertyIterator::operator++() noexcept -> PropertyIterator& {
	++_iterator;
	return *this;
}

auto PropertyIterator::operator++(int) noexcept -> PropertyIterator {
	return PropertyIterator{_iterator++, _resolver};
}


PropertyObjectIterator::PropertyObjectIterator(resource::json::const_iterator iterator, detail::EntryTypeResolver* resolver) :
	_iterator{iterator}, _resolver{resolver} {}

auto PropertyObjectIterator::operator!=(PropertyObjectIterator const& other) const noexcept -> bool {
	return other._iterator != _iterator;
}

auto PropertyObjectIterator::operator*() const -> std::pair<std::string_view, Property> {
	auto const& json = *_iterator;
	
	if (_resolver && json.is_object()) {
		auto const itEntryType = json.find("$entry-type");
		
		if (itEntryType != json.end()) {
			return {_iterator.key(), _resolver->toProperty(itEntryType.value().get<std::string_view>(), json)};
		}
	}
	
	return {_iterator.key(), Property{&json, _resolver}};
}

auto PropertyObjectIterator::operator++() noexcept -> PropertyObjectIterator& {
	++_iterator;
	return *this;
}

auto PropertyObjectIterator::operator++(int) noexcept -> PropertyObjectIterator {
	return PropertyObjectIterator{_iterator++, _resolver};
}

PropertyObjectRange::PropertyObjectRange(PropertyObjectIterator begin, PropertyObjectIterator end) noexcept :
	_begin{begin}, _end{end} {}

auto PropertyObjectRange::begin() const noexcept -> PropertyObjectIterator {
	return _begin;
}

auto PropertyObjectRange::end() const noexcept -> PropertyObjectIterator {
	return _end;
}
