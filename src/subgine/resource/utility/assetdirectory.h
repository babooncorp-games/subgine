#pragma once

#include <filesystem>

namespace sbg {

auto asset_directory() -> std::filesystem::path;

} // namespace sbg