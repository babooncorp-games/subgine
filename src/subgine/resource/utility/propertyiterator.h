#pragma once

#include "../json.h"

namespace sbg {
	
namespace detail {
	struct EntryTypeResolver;
}

struct Property;

struct PropertyIterator {
	PropertyIterator() = default;
	explicit PropertyIterator(resource::json::const_iterator iterator, detail::EntryTypeResolver* resolver);
	
	using value_type = Property;
	using reference = Property;
	using iterator_category = std::forward_iterator_tag;
	using difference_type = std::ptrdiff_t;
	
	auto operator==(const PropertyIterator& other) const noexcept -> bool;
	auto operator*() const -> Property;
	auto operator++() noexcept -> PropertyIterator&;
	auto operator++(int) noexcept -> PropertyIterator;
	
private:
	resource::json::const_iterator _iterator = {};
	detail::EntryTypeResolver* _resolver = nullptr;
};

struct PropertyObjectIterator {
	explicit PropertyObjectIterator(resource::json::const_iterator iterator, detail::EntryTypeResolver* resolver);
	
	using value_type = std::pair<std::string_view, Property>;
	using reference = std::pair<std::string_view, Property>;
	using iterator_category = std::forward_iterator_tag;
	using difference_type = std::ptrdiff_t;
	
	auto operator!=(const PropertyObjectIterator& other) const noexcept -> bool;
	auto operator*() const -> std::pair<std::string_view, Property>;
	auto operator++() noexcept -> PropertyObjectIterator&;
	auto operator++(int) noexcept -> PropertyObjectIterator;
	
private:
	resource::json::const_iterator _iterator = {};
	detail::EntryTypeResolver* _resolver = nullptr;
};

struct PropertyObjectRange {
	explicit PropertyObjectRange(PropertyObjectIterator begin, PropertyObjectIterator end) noexcept;
	
	auto begin() const noexcept -> PropertyObjectIterator;
	auto end() const noexcept -> PropertyObjectIterator;
	
private:
	PropertyObjectIterator _begin;
	PropertyObjectIterator _end;
};

} // namespace sbg
