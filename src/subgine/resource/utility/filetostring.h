#pragma once

#include <string>

namespace sbg {

std::string fileToString(std::ifstream in);

} // namespace sbg
