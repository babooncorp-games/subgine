#pragma once

#include "gamedatabase.h"
#include "ownedresource.h"
#include "utility/trait.h"
#include "subgine/common/function_traits.h"
#include "subgine/common/hash.h"

#include <string_view>

#include "subgine/common/define.h"

namespace sbg {

template<typename>
struct ResourceFactoryService;

template<typename L>
struct ResourceFactory {
	
	explicit ResourceFactory(GameDatabase& database, L loader) noexcept :
		_loader{std::move(loader)},
		_database{&database} {}
	
	using Result = function_result_t<decltype(&L::create)>;
	using Resource = std::conditional_t<is_managed<Result>, Result, OwnedResource<Result>>;
	static constexpr auto path = L::path;
	
	auto create(std::string_view name) const -> Resource {
		return Resource{_loader.create(_database->get(L::path, name))};
	}

	auto create(hash_t hash) const -> Resource {
		return Resource{_loader.create(_database->get(L::path, hash))};
	}
	
private:
	NO_UNIQUE_ADDRESS
	L _loader;
	GameDatabase* _database;
	
	friend auto service_map(ResourceFactory const&) -> ResourceFactoryService<ResourceFactory<L>>;
};

} // namespace sbg

#include "subgine/common/undef.h"
