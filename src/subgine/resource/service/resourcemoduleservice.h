#pragma once

#include "../resourcemodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ResourceModuleService : kgr::single_service<ResourceModule> {};

} // namespace sbg
