#pragma once

#include "subgine/common/kangaru.h"
#include "subgine/common/service/rootservice.h"

#include "../manager/jsonmanager.h"

#include "filemanagerservice.h"

namespace sbg {

// TODO: Find why autowire won't work here
struct JsonManagerService : root_service<JsonManager, kgr::dependency<FileManagerService>> {};

} // namespace sbg
