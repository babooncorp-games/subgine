#pragma once

#include "../gamedatabase.h"

#include "jsonmanagerservice.h"
#include "filemanagerservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct GameDatabaseService : kgr::single_service<GameDatabase, kgr::autowire>{};

} // namespace sbg
