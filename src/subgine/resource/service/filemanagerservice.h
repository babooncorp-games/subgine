#pragma once

#include "../manager/filemanager.h"

#include "subgine/common/service/rootservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct FileManagerService : root_service<FileManager> {};

} // namespace sbg
