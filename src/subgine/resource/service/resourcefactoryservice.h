#pragma once

#include "../resourcefactory.h"

#include "gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

template<typename>
struct ResourceFactoryService;

template<typename C>
struct ResourceFactoryService<ResourceFactory<C>> : kgr::service<ResourceFactory<C>, kgr::autowire> {};

} // namespace sbg
