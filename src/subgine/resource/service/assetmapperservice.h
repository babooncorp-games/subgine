#pragma once

#include "../assetmapper.h"

#include "gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct AssetMapperService : kgr::service<AssetMapper, kgr::autowire> {};

} // namespace sbg
