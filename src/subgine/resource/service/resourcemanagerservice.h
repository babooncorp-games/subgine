#pragma once

#include "../resourcemanager.h"

#include "resourcecacheservice.h"
#include "gamedatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

template<typename>
struct ResourceManagerService;

template<typename T>
struct ResourceManagerService<ResourceManager<T>> : kgr::service<ResourceManager<T>, kgr::autowire>{};

} // namespace sbg
