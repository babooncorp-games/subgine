#pragma once

#include "../factory.h"

#include "subgine/common/kangaru.h"

#include <utility>

namespace sbg {

template<typename>
struct FactoryService;

template<typename C>
struct FactoryService<Factory<C>> : kgr::service<Factory<C>, kgr::autowire> {};

} // namespace sbg
