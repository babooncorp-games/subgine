#pragma once

#include "../resourcecache.h"

namespace sbg {

struct ResourceCacheService : kgr::single_service<ResourceCache, kgr::autowire> {};

} // namespace sbg
