#pragma once

#include "filemanager.h"
#include "../property.h"
#include "../detail/entrytyperesolver.h"

#include "subgine/common/types.h"

namespace sbg {

struct JsonManagerService;

/**
 * @brief Stores and supplies all requested JSON files.
 * 
 * The files are kept in a cache; When a file is requested,
 * if it isn't already stored, it is parsed and added to the cache.
 */
struct JsonManager {
	JsonManager(FileManager& fileManager);
	
	/**
	 * This function returns the property that represents the entire json file,
	 * which is binded to the given path.
	 * 
	 * @return A property that contains the entire json file.
	 */
	Property get(std::string const& path);
	
	/**
	 * Uses the FileManager to get the content of a file as a string,
	 * then returning the string as a Property.
	 * 
	 * @todo: Move this logic to the file entry type.
	 * 
	 * @return A property that contains the entire json file as a string.
	 */
	Property getAsString(const std::string& path);
	
	void clear();
	
private:
	Property parse(const std::string& path);
	
	std::unordered_map<std::string, resource::json> _jsonFiles;
	std::unordered_map<std::string, resource::json> _fileStrings;
	FileManager& _fileManager;
	detail::EntryTypeResolver _defaultResolver{*this, nullptr};
	
	friend auto service_map(JsonManager const&) -> JsonManagerService;
};

} // namespace sbg
