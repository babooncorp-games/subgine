#pragma once

#include "subgine/common/types.h"

#include <unordered_map>
#include <filesystem>

namespace sbg {

struct FileManagerService;

struct FileManager {
	// TODO: return a std::string_view as soon as nlohmann json supports it
	inline auto get(std::string const& path) -> std::string const& {
		return get(std::filesystem::path{path});
	}
	
	auto get(std::filesystem::path const& path) -> std::string const&;
	
	void clear();
	
private:
	std::unordered_map<std::string, std::string> _files;
	
	friend auto service_map(FileManager const&) -> FileManagerService;
};

} // namespace sbg
