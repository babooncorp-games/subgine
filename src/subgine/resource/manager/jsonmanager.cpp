#include "jsonmanager.h"

#include "subgine/log.h"

#include "nlohmann/json.hpp"
#include "../manager/filemanager.h"
#include "../utility/propertynotfound.h"

using namespace sbg;

static bool ends_with(std::string_view value, std::string_view ending) {
	if (ending.size() > value.size()) return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

JsonManager::JsonManager(FileManager& fileManager) : _fileManager{fileManager} {}

Property JsonManager::get(std::string const& path) {
	return parse(path);
}

Property JsonManager::getAsString(const std::string& path) {
	auto const it = _fileStrings.find(path);
	
	if (it != _fileStrings.end()) {
		return Property{&it->second, &_defaultResolver};
	}
	
	resource::json content = _fileManager.get(path);
	
	return Property{&_fileStrings.emplace(path, std::move(content)).first->second, &_defaultResolver};
}

Property JsonManager::parse(const std::string& path) {
	auto const it = _jsonFiles.find(path);
	
	if (it != _jsonFiles.end()) {
		return Property{&it->second, &_defaultResolver};
	}
	
	auto root = [&] {
		if (ends_with(path, ".json")) {
			return resource::json::parse(_fileManager.get(path));
		} else {
			return resource::json::from_cbor(_fileManager.get(path));
		}
	}();
	
	Log::fatal([&]{ return root.is_null(); }, SBG_LOG_INFO, "Unable to parse Json file", log::enquote(path));
	
	return Property{&_jsonFiles.emplace(path, std::move(root)).first->second, &_defaultResolver};
}

void JsonManager::clear() {
	_fileStrings.clear();
	_jsonFiles.clear();
}
