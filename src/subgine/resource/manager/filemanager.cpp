#include "filemanager.h"

#include "../utility/filetostring.h"

#include "subgine/log.h"

#include <fstream>

namespace sbg {

auto FileManager::get(std::filesystem::path const& path) -> std::string const& {
	auto cache = _files.find(path.string());
	
	if (cache != _files.end()) {
		return cache->second;
	}
	
	std::ifstream file{path, std::ios::in | std::ios::binary};
	
	Log::error([&]{ return !file; }, SBG_LOG_INFO, "Unable to open file with path", path.string());
	
	return _files.emplace(path.string(), fileToString(std::move(file))).first->second;
}

void FileManager::clear() {
	_files.clear();
}

} // namespace sbg
