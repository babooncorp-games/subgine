#pragma once

#include "subgine/common/kangaru.h"

namespace sbg {

template<typename, typename>
struct AbstractBuilder;

template<typename R, typename Apply, typename... Args>
struct AbstractBuilder<R(Args...), Apply> {
	virtual ~AbstractBuilder() = default;
	virtual R create(kgr::invoker invoker, const Apply& apply, Args... args) const = 0;
};

} // namespace sbg
