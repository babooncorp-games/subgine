#include "catch2/catch.hpp"

#include "subgine/common/hash.h"

#include <string_view>

using namespace std::literals;
using namespace sbg::literals;
using namespace sbg;

namespace reference {
	auto MurmurHash64A(void const* key, std::size_t len, std::uint64_t seed) -> std::uint64_t {
		auto const m = std::uint64_t{0xc6a4a7935bd1e995ull};
		auto const r = std::int32_t{47};
		
		auto h = seed ^ (len * m);
		
		auto data = (std::uint64_t const*)key;
		const auto end = data + (len/8);
		
		while(data != end)
		{
			std::uint64_t k = *data++;
			
			k *= m;
			k ^= k >> r;
			k *= m;
			
			h ^= k;
			h *= m;
		}

		const unsigned char * data2 = (const unsigned char*)data;
		
		switch(len & 7)
		{
		case 7: h ^= std::uint64_t(data2[6]) << 48; [[fallthrough]];
		case 6: h ^= std::uint64_t(data2[5]) << 40; [[fallthrough]];
		case 5: h ^= std::uint64_t(data2[4]) << 32; [[fallthrough]];
		case 4: h ^= std::uint64_t(data2[3]) << 24; [[fallthrough]];
		case 3: h ^= std::uint64_t(data2[2]) << 16; [[fallthrough]];
		case 2: h ^= std::uint64_t(data2[1]) << 8; [[fallthrough]];
		case 1: h ^= std::uint64_t(data2[0]);
			    h *= m;
		};
		
		h ^= h >> r;
		h *= m;
		h ^= h >> r;
		
		return h;
	}
}

TEST_CASE("murmur64a hashes values from a byte buffer") {
	SECTION("Hashes single values") {
		auto const value = std::as_bytes(std::span{"a"sv});
		CHECK(static_cast<std::uint64_t>(murmur64a(value)) == reference::MurmurHash64A(value.data(), value.size(), 0));
		CHECK(static_cast<std::uint64_t>(murmur64a(value, hash_t{42})) == reference::MurmurHash64A(value.data(), value.size(), 42));
	}

	SECTION("Hashes short strings") {
		auto const value = std::as_bytes(std::span{"ab"sv});
		CHECK(static_cast<std::uint64_t>(murmur64a(value)) == reference::MurmurHash64A(value.data(), value.size(), 0));
		CHECK(static_cast<std::uint64_t>(murmur64a(value, hash_t{42})) == reference::MurmurHash64A(value.data(), value.size(), 42));

		auto const value2 = std::as_bytes(std::span{"abcdefg"sv});
		CHECK(static_cast<std::uint64_t>(murmur64a(value2)) == reference::MurmurHash64A(value2.data(), value2.size(), 0));
		CHECK(static_cast<std::uint64_t>(murmur64a(value2, hash_t{42})) == reference::MurmurHash64A(value2.data(), value2.size(), 42));
	}
	
	SECTION("Hashes long strings") {
		auto const value = std::as_bytes(std::span{"abcdefghigklmnopqrstuvwxyz"sv});
		CHECK(static_cast<std::uint64_t>(murmur64a(value)) == reference::MurmurHash64A(value.data(), value.size(), 0));
		CHECK(static_cast<std::uint64_t>(murmur64a(value, hash_t{42})) == reference::MurmurHash64A(value.data(), value.size(), 42));
	}

	SECTION("Has Compile time hashing") {
		auto const compiletime = "abcde"_h;
		auto const runtime = std::as_bytes(std::span{"abcde"sv});
		CHECK(murmur64a(runtime) == compiletime);
	}
	
	CHECK("blue-mountains"_h != "floating-islands"_h);
}
