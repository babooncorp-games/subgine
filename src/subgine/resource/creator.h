#pragma once

#include "property.h"
#include "builder.h"
#include "defaultapply.h"
#include "utility/trait.h"

#include "subgine/common/kangaru.h"
#include "subgine/common/types.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include <unordered_map>
#include <string_view>

#include "subgine/common/define.h"

namespace sbg {

struct Entity;

// This block is copied here because kdevelop read this one.
/**
 *
 * This class is the base class for factories that need to build a different object depending on it's name.
 * It receive the builder's signature and the service map.
 * It can be used like this:
 * \code
 * struct MyCreator : Creator<SomeType(Entity, int)> {};
 * \endcode
 * 
 * It is recommended to use provided shortcuts:
 * \code
 * struct MyCreator : Creator<DefaultBuilder<SomeType>> {};
 * \endcode
 */
template<typename, typename = DefaultApply>
struct Creator;

/**
 * This class is the base class for factories that need to build a different object depending on it's name.
 * It receive the builder's signature and the service map.
 * It can be used like this:
 * \code
 * struct MyCreator : Creator<SomeType(Entity, int)> {};
 * \endcode
 * 
 * It is recommended to use provided shortcuts:
 * \code
 * struct MyCreator : Creator<DefaultBuilder<SomeType>> {};
 * \endcode
 */
template<typename R, typename... Args, typename Apply>
struct Creator<R(Args...), Apply> {
	using Signature = R(Args...);
	
	struct NotInvokableError {
		template<typename T = void>
		NotInvokableError(...) {
			static_assert(!std::is_same<void, T>::value, "The builder sent is not invokable. Make sure to include all the needed service definitions and to recieve all required parameters.");
		}
	};
	
	Creator() = default;
	explicit Creator(Apply apply) noexcept : _apply{std::move(apply)} {}
	
	Creator(Creator const&) = delete;
	Creator& operator=(Creator const&) = delete;
	
	// TODO: To remove when GCC12 is fixed
	Creator(Creator&&);
	Creator& operator=(Creator&&);
	
	/**
	 * This function adds a builder to the creator.
	 * The builder should have the signature designated by the creator.
	 * It can also receive any service and can receive additional parameters from the creator.
	 * 
	 * @param name The builder's name.
	 * @param builder The builder to add.
	 */
	template<typename T>
	auto add(std::string_view name, T builder) -> std::enable_if_t<is_builder<T, Signature, Apply>> {
		_builders.insert_or_assign(name, makeBuilder(builder));
	}
	
protected:
	R build(kgr::invoker invoker, std::string_view type, Args... args) const {
		auto it = _builders.find(type);
		
		if (it != _builders.end()) {
			return static_cast<R>(it->second->create(invoker, _apply, std::forward<Args>(args)...));
		}
		
		if constexpr (std::is_default_constructible_v<R>) {
			Log::error(SBG_LOG_INFO, "Builder", type, "not found, returning default value");
			return R{};
		} else if constexpr(std::is_void_v<R>) {
			Log::warning(SBG_LOG_INFO, "Builder", type, "not found, no builder called");
		} else {
			Log::fatal(SBG_LOG_INFO, "Builder", type, "not found, aborting");
			std::abort();
		}
	}
	
	/**
	 * Creates an object through a builder
	 * 
	 * @param invoker The invoking container object.
	 * @param name The builder to invoker.
	 * @param args Arguments to be sent to the builder.
	 */
	R create(kgr::invoker invoker, std::string_view name, Args... args) const {
		return build(invoker, name, std::forward<Args>(args)...);
	}
	
private:
	using BuilderType = AbstractBuilder<Signature, Apply>;
	using BuilderCollection = std::unordered_map<std::string_view, std::unique_ptr<BuilderType>>;
	
	template<typename T>
	static std::unique_ptr<BuilderType> makeBuilder(T callback) {
		return std::make_unique<Builder<T, Signature, Apply>>(callback);
	}
	
	BuilderCollection _builders;
	
	NO_UNIQUE_ADDRESS
	Apply _apply;
};

/**
 * @brief This is the default builder for a creator, the most common case.
 * Creator using this builder will have thier build and/or create function
 * taking a Entity and a Property, returning a T.
 */
template<typename T>
using DefaultBuilder = T(Entity, Property);

/**
 * @brief This is a builder that don't produces anything.
 * It may mutate the entity, or applying transformation to it.
 */
using VoidBuilder = void(Entity, Property);

/**
 * @brief This is a builder for simple cases where information from the entity isn't needed.
 */
template<typename T>
using SimpleBuilder = T(Property);

/**
 * @brief This builder is used when you want the result of the builder to be applied to an object of type T.
 * The application can be done through assignation, transformation or through a setter.
 */
template<typename T>
using AppliedBuilder = void(T&, Entity, Property);

} // namespace sbg

#include "subgine/common/undef.h"
