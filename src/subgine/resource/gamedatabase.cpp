#include "gamedatabase.h"

#include "manager/filemanager.h"
#include "utility/assetdirectory.h"

#include "subgine/log.h"

#include <fstream>

using namespace sbg;

void GameDatabase::load_impl(std::span<std::filesystem::path const> paths) {
	// TODO: Find a better spot to instantiate the resource cache
	(void) *_cache;
	for (auto const& path : paths) {
		_jsonTables.merge_patch(resource::json::from_cbor(_fileManager.get(asset_directory() / path)));
	}
}

void GameDatabase::drop() noexcept {
	_jsonTables.clear();
}

auto GameDatabase::get(std::string_view table, std::string_view name) const -> Property {
	auto const json_table = _jsonTables.find(table);
	
	Log::fatal([&]{ return json_table == _jsonTables.end(); },
		SBG_LOG_INFO, "Table", log::enquote(table), "not found in database"
	);
	
	auto const json_object = json_table->find(name);
	
	Log::fatal([&]{ return json_object == json_table->end(); },
		SBG_LOG_INFO, "Object", log::enquote(name), "not found in table", log::enquote(table)
	);
	
	if (json_object->is_object()) {
		auto const it = json_object->find("$entry-type");
		
		if (it != json_object->end()) {
			return _resolver.toProperty(it->get<std::string_view>(), *json_object);
		}
	}
	
	return Property{&*json_object, &_resolver};
}

auto GameDatabase::get(std::string_view table, hash_t hash) const -> Property {
	auto const json_table = _jsonTables.find(table);
	
	Log::fatal([&]{ return json_table == _jsonTables.end() or not json_table->is_object(); },
		SBG_LOG_INFO, "Table", log::enquote(table), "not found in database"
	);
	
	auto const& table_map = json_table->get_ref<resource::json::object_t const&>();
	auto const json_object = table_map.find_by_hash(static_cast<std::size_t>(hash));
	
	Log::fatal([&]{ return json_object == table_map.end(); },
		SBG_LOG_INFO, "Object with hash", log::enquote(static_cast<std::size_t>(hash)), "not found in table", log::enquote(table)
	);
	
	auto const& [_, json] = *json_object;
	
	if (json.is_object()) {
		auto const it = json.find("$entry-type");
		
		if (it != json.end()) {
			return _resolver.toProperty(it->get<std::string_view>(), json);
		}
	}
	
	return Property{&json, &_resolver};
}
