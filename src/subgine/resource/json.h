#include <nlohmann/json.hpp>
#include "subgine/common/map.h"
#include "subgine/common/hash.h"

namespace sbg::resource::detail {
	template<typename K, typename V, typename, typename>
	using JsonObject = sbg::hash_map<K, V, string_murmur_hash>;
}

namespace sbg::resource {
	using json = nlohmann::basic_json<detail::JsonObject>;
}
