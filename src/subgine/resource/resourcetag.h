#pragma once

#include <string>
#include <string_view>
#include <type_traits>
#include <concepts>
#include <compare>

#include "../hash.h"

#include "subgine/common/traits.h"
#include "subgine/common/define.h"
#include "subgine/common/type_id.h"

namespace sbg {

template<typename T>
struct ResourceTag {
	ResourceTag() = default;
	
	ResourceTag(ResourceTag const&) = default;
	ResourceTag(ResourceTag&&) noexcept = default;
	
	explicit ResourceTag(std::convertible_to<std::string_view> auto&& string_like) :
		hash{murmur64a(FWD(string_like))} {}
	
	explicit ResourceTag(hash_t hash) : hash{hash} {}
	
	friend auto operator<=>(ResourceTag const&, ResourceTag const&) = default;
	
	auto operator=(ResourceTag const&) -> ResourceTag& = default;
	auto operator=(ResourceTag&&) noexcept -> ResourceTag& = default;
	
	// We tag this type as a resource tag type
	using resource_tag = void;
	static constexpr auto type = type_id<ResourceTag>;
	hash_t hash = {};
};

template<typename T>
concept resource_tag =
	    std::same_as<typename T::resource_tag, void>
	and requires(T tag) {
		{ tag.hash } -> std::same_as<hash_t&>;
		{ std::as_const(tag).type } -> std::same_as<type_id_t const&>;
	};

template<typename T>
concept resource_tag_concrete =
	    resource_tag<T>
	and std::constructible_from<T, hash_t>
	and std::constructible_from<T, std::string_view>
	and requires{ type_id<T> == T::type; };

struct GenericResource {
	GenericResource() = default;
	
	constexpr GenericResource(type_id_t type, hash_t hash) noexcept : type{type}, hash{hash} {}

	template<resource_tag T>
	constexpr GenericResource(T resource) noexcept : type{type_id<T>}, hash{resource.hash} {}
	
	friend auto operator==(resource_tag auto const& lhs, resource_tag auto const& rhs) noexcept -> bool {
		return lhs.type == rhs.type and lhs.hash == rhs.hash;
	}
	
	friend auto operator<=>(resource_tag auto const& lhs, resource_tag auto const rhs) noexcept -> std::strong_ordering {
		if (auto const cmp = lhs.type <=> rhs.type != 0; cmp != std::strong_ordering::equal) {
			return cmp;
		}
		
		return lhs.hash <=> rhs.hash;
	}
	
	template<resource_tag_concrete T>
	constexpr auto is() const& noexcept {
		return type == T::type;
	}
	
	template<resource_tag_concrete T>
	constexpr auto as() const& noexcept {
		return T{hash};
	}
	
	// We tag this type as a generic resource type
	using resource_tag = void;
	type_id_t type = null_type_id;
	hash_t hash = {};
};

struct no_resource_t {
	template<typename T>
	constexpr operator ResourceTag<T> () const noexcept {
		return ResourceTag<T>{};
	}
	
	constexpr operator GenericResource () const noexcept {
		return GenericResource{};
	}
	
	template<typename T>
	friend constexpr auto operator==(no_resource_t, ResourceTag<T> resource) noexcept -> bool {
		return resource.hash == hash_t{};
	}
	
	template<typename T>
	friend constexpr auto operator<=>(no_resource_t, ResourceTag<T> resource) noexcept {
		return resource.hash <=> hash_t{};
	}
	
	template<typename T>
	friend constexpr auto operator==(no_resource_t, GenericResource resource) noexcept -> bool {
		return resource == GenericResource{};
	}
	
	template<typename T>
	friend constexpr auto operator<=>(no_resource_t, GenericResource resource) noexcept {
		return resource <=> GenericResource{};
	}
} inline constexpr no_resource{};

} // namespace sbg

#include "subgine/common/undef.h"
