#pragma once

#include <unordered_map>
#include <shared_mutex>
#include <mutex>

#include "subgine/common/kangaru.h"
#include "subgine/common/algorithm.h"
#include "subgine/common/erasedtype.h"
#include "subgine/common/map.h"
#include "subgine/common/hash.h"
#include "resourcehandle.h"
#include "ownedresource.h"
#include "utility/trait.h"

#include <optional>
#include <string>
#include <list>
#include <string_view>
#include <any>

namespace sbg {

struct ResourceCacheService;

struct ResourceCache {
	ResourceCache() = default;
	
	// TODO: To remove when GCC12 is fixed
	ResourceCache(ResourceCache&& o) noexcept;
	
	ResourceCache& operator=(ResourceCache&&) = delete;
	
	ResourceCache(ResourceCache const&) = delete;
	ResourceCache& operator=(ResourceCache const&) = delete;
	
	template<typename T>
	using Resource = std::conditional_t<is_managed<T>, T, OwnedResource<T>>;
	
	template<typename T, typename... Args>
	auto cache(std::string_view table, hash_t hash, Resource<T> resource) -> ResourceHandle<T> {
		auto const lock = std::unique_lock{_mutex};
		
		constexpr auto unwrap = [](auto const& resource) -> decltype(auto) {
			if constexpr (is_managed<std::decay_t<decltype(resource)>>) {
				return resource;
			} else {
				return resource.get();
			}
		};
		
		auto const handle = sbg::ResourceHandle{
			unwrap(resource),
			OwnershipToken{std::make_shared<char>()}
		};
		
		_cache.emplace(
			salted_hash(table, hash),
			std::pair<ErasedType, OwnershipToken>{ErasedType{std::move(resource)}, handle.token()}
		);
		
		return handle;
	}
	
	template<typename T>
	auto check(std::string_view table, hash_t hash) const -> std::optional<ResourceHandle<T>> {
		auto const lock = std::shared_lock{_mutex};
		auto const it = _cache.find(salted_hash(table, hash));
		
		if (it != _cache.end()) {
			return handle_for<T>(it->second);
		}
		
		return {};
	}
	
	void clear() {
		auto const lock = std::unique_lock{_mutex};
		_cache.clear();
	}
	
private:
	template<typename T>
	static auto handle_for(std::pair<ErasedType, OwnershipToken> const& object) noexcept -> ResourceHandle<T> {
		if constexpr (is_managed<T>) {
			return ResourceHandle<T>{object.first.as<T>(), object.second};
		} else {
			return ResourceHandle<T>{object.first.as<OwnedResource<T>>().get(), object.second};
		}
	}
	
	static auto salted_hash(std::string_view table, hash_t hash) -> hash_t {
		return murmur64a(table, hash);
	}
	
	using cache_t = hash_map<hash_t, std::pair<ErasedType, OwnershipToken>, key_is_hash>;
	
	mutable std::shared_mutex _mutex;
	cache_t _cache;
	
	friend auto service_map(ResourceCache const&) -> ResourceCacheService;
};

} // namespace sbg
