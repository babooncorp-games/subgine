#pragma once

#include "subgine/common/ownershiptoken.h"

namespace sbg {

template<typename T>
struct ResourceHandle {
	ResourceHandle() = default;
	explicit ResourceHandle(T object, OwnershipToken token) noexcept : _resource{std::move(object)}, _token{std::move(token)} {}
	
	[[nodiscard]]
	auto operator==(ResourceHandle const& other) const& noexcept -> bool {
		return static_cast<T const&>(*this) == static_cast<T const&>(other);
	}
	
	auto operator!=(ResourceHandle const& other) const& noexcept -> bool {
		return !(*this == other);
	}
	
	auto operator<(ResourceHandle const& other) const& noexcept -> bool {
		return static_cast<T const&>(*this) < static_cast<T const&>(other);
	}
	
	auto operator<=(ResourceHandle const& other) const& noexcept -> bool {
		return static_cast<T const&>(*this) <= static_cast<T const&>(other);
	}
	
	auto operator>(ResourceHandle const& other) const& noexcept -> bool {
		return static_cast<T const&>(*this) > static_cast<T const&>(other);
	}
	
	auto operator>=(ResourceHandle const& other) const& noexcept -> bool {
		return static_cast<T const&>(*this) >= static_cast<T const&>(other);
	}
	
	[[nodiscard]]
	constexpr auto resource() const& noexcept -> T const& {
		return _resource;
	}
	
	[[nodiscard]]
	constexpr auto resource() & noexcept -> T& {
		return _resource;
	}
	
	[[nodiscard]]
	constexpr auto resource() && noexcept -> T&& {
		return std::move(_resource);
	}
	
	[[nodiscard]]
	auto token() const noexcept -> OwnershipToken {
		return _token;
	}
	
	constexpr auto operator->() & noexcept -> T* {
		return &_resource;
	}
	
	constexpr auto operator->() const& noexcept -> T const* {
		return &_resource;
	}
	
	constexpr auto operator*() & noexcept -> T& {
		return _resource;
	}
	
	constexpr auto operator*() const& noexcept -> T const& {
		return _resource;
	}
	
	constexpr auto operator*() && noexcept -> T&& {
		return std::move(_resource);
	}
	
private:
	T _resource;
	OwnershipToken _token;
};

} // namespace sbg
