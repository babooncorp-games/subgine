#include "property.h"

#include "manager/jsonmanager.h"
#include "detail/entrytyperesolver.h"
#include "gamedatabase.h"

#include <algorithm>

#include "subgine/log.h"

using namespace sbg;

resource::json const Property::null = resource::json();

Property::Property(resource::json const* value, detail::EntryTypeResolver* resolver) : _value{value}, _resolver{resolver} {}

auto Property::index(std::size_t index) const& -> Property {
	if (!_value->is_array() || _value->size() <= index) {
		return toProperty(null);
	}
	
	return toProperty((*_value)[index]);
}

auto Property::index(hash_t hash) const& -> Property {
	if (not _value->is_object()) {
		return toProperty(null);
	}
	
	auto const hash_value = static_cast<std::size_t>(hash);
	auto const& object = _value->get_ref<resource::json::object_t const&>();
	auto const it = object.find_by_hash(hash_value);
	
	if (it != object.end()) {
		return toProperty(it->second);
	} else {
		return toProperty(null);
	}
}

auto Property::toProperty(resource::json const& json) const& -> Property {
	if (_resolver && json.is_object()) {
		auto itEntryType = json.find("$entry-type");
		
		if (itEntryType != json.end()) {
			return _resolver->toProperty(itEntryType.value().get<std::string_view>(), json);
		}
	}
	
	return Property{&json, _resolver};
}

auto Property::asString() const& noexcept -> std::string const& {
	if (auto const string_ptr = _value->get_ptr<resource::json::string_t const*>()) {
		return *string_ptr;
	}
	
	Log::fatal(SBG_LOG_INFO, "Conversion required a string, but the current json value isn't");
}

auto Property::asU8String() const& -> std::u8string {
	auto value = std::u8string{};
	auto const stored = std::string_view{*this};
	
	value.resize(stored.size());
	std::transform(stored.begin(), stored.end(), value.begin(), [](char c){ return static_cast<char8_t>(c); });
	return value;
}

void Property::bind(GameDatabase const& database) & noexcept {
	_resolver = &database._resolver;
}

auto Property::begin() const& noexcept -> PropertyIterator {
	return PropertyIterator{_value->begin(), _resolver};
}

auto Property::end() const& noexcept -> PropertyIterator {
	return PropertyIterator{_value->end(), _resolver};
}

auto Property::items() const& noexcept -> PropertyObjectRange {
	return PropertyObjectRange{
		PropertyObjectIterator{_value->begin(), _resolver},
		PropertyObjectIterator{_value->end(), _resolver}
	};
}