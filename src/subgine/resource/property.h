#pragma once

#include "utility/propertyiterator.h"
#include "utility/vectorconversion.h"
#include "utility/resourcetagconversion.h"
#include "utility/filesystempathconversion.h"

#include "subgine/common/types.h"
#include "subgine/common/traits.h"
#include "subgine/common/hash.h"

#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"

#include "json.h"

#include <string>
#include <string_view>
#include <concepts>
#include <functional>
#include <compare>

namespace sbg {

namespace detail {
	struct EntryTypeResolver;
}

struct GameDatabase;

// Concept that determine if the type T can stream the value of type V
template<typename T, typename V>
concept streams = requires(T& stream, V const& value) {
	{ stream << value } -> std::same_as<T&>;
};

/**
 * @brief Data container with JSON-like structure.
 * 
 * Abstraction layer over the JSON library, in order to limit its use across the engine and add extensions.
 */
struct Property {
	Property() = default;
	explicit Property(resource::json const* value, detail::EntryTypeResolver* resolver = nullptr);
	
	friend struct std::hash<Property>;
	
	friend auto operator<=>(Property const& lhs, Property const& rhs) noexcept = default;
	
	/**
	 * Indexing operator. Works with both strings, indexes and pre-hashed strings.
	 */
	template<typename T>
	requires(
		std::same_as<T, hash_t> ||
		std::convertible_to<T const&, std::size_t> ||
		std::constructible_from<std::string_view, T const&>
	)
	inline auto operator[](T const& i) const& -> Property {
		if constexpr (std::constructible_from<std::string_view, T const&>) {
			return index(murmur64a(std::string_view{i}));
		} else {
			return index(i);
		}
	}
	
	/**
	 * @brief Converts the property value to T.
	 * 
	 * @return Converted property value.
	 */
	template<typename T>
	requires(
		   std::is_convertible_v<resource::json, T>
		or std::same_as<T, char const*>
		or std::same_as<T, std::string_view>
		or std::same_as<T, std::u8string_view>
		or std::same_as<T, std::u8string>
	)
	auto as() const& -> std::conditional_t<std::is_same_v<T, std::string>, T const&, T> {
		if constexpr (std::is_same_v<T, std::string>) {
			return asString();
		} else if constexpr (std::is_same_v<T, std::string_view>) {
			return std::string_view{asString()};
		} else if constexpr (std::is_same_v<T, char const*>) {
			return asCString();
		} else if constexpr (std::is_same_v<T, std::u8string_view>) {
			// This is UB, but it's the only way to get a std::u8string_view from a std::string_view
			return std::u8string_view{reinterpret_cast<char8_t const*>(asCString())};
		} else if constexpr (std::is_same_v<T, std::u8string>) {
			return asU8String();
		} else {
			return _value->get<T>();
		}
	}
	
	template<typename T>
	requires(
		(
			   std::is_convertible_v<resource::json, T>
			or std::same_as<T, std::u8string>
			or std::same_as<T, std::u8string_view>
		)
		and not std::same_as<T, std::string>
		and not std::same_as<T, std::string_view>
		and not std::same_as<T, char const*>
	)
	inline explicit operator T () const& {
		return as<T>();
	}
	
	inline operator std::string_view () const& noexcept {
		return as<std::string_view>();
	}
	
	inline explicit operator std::string const& () const& noexcept {
		return asString();
	}
	
	inline explicit operator char const* () const& noexcept {
		return asCString();
	}
	
	inline bool isBool() const& { return _value->is_boolean(); }
	inline bool isObject() const& { return _value->is_object(); }
	inline bool isArray() const& { return _value->is_array(); }
	inline bool isNumeric() const& { return _value->is_number(); }
	inline bool isInt() const& { return _value->is_number_integer(); }
	inline bool isUInt() const& { return _value->is_number_unsigned(); }
	inline bool isFloat() const& { return _value->is_number_float(); }
	inline bool isString() const& { return _value->is_string(); }
	
	/**
	 * @return true if the value type is null, false otherwise.
	 */
	inline auto isNull() const& -> bool { return _value->is_null(); }
	
	/**
	 * @return Whether the container is empty or not.
	 */
	inline auto empty() const& -> bool { return _value->empty(); }
	
	/**
	 * @return The number of contained elements.
	 */
	inline auto size() const -> std::size_t { return _value->size(); };
	
	inline auto dump() const -> std::string { return _value->dump(); };
	
	inline auto dump_pretty() const -> std::string { return _value->dump(2); };
	
	friend auto operator<<(streams<resource::json> auto& o, Property const& p) -> auto& {
		return o << *(p._value);
	}
	
	auto begin() const& noexcept -> PropertyIterator;
	auto end() const& noexcept -> PropertyIterator;
	
	auto items() const& noexcept -> PropertyObjectRange;
	
	void bind(GameDatabase const& database) & noexcept;
	
private:
	auto index(hash_t hash) const& -> Property;
	auto index(std::size_t index) const& -> Property;
	
	auto asString() const& noexcept -> std::string const&;
	auto asU8String() const& -> std::u8string;
	
	inline auto asCString() const& noexcept -> char const* {
		return asString().c_str();
	}
	
	Property toProperty(resource::json const& json) const&;
	
	static resource::json const null;
	friend struct PropertyIterator;
	friend struct detail::EntryTypeResolver;
	
	resource::json const* _value = &null;
	detail::EntryTypeResolver* _resolver = nullptr;
};

} // namespace sbg

template<>
struct std::hash<sbg::Property> {
	std::size_t operator()(sbg::Property const& property) const& noexcept {
		return std::hash<sbg::resource::json const*>{}(property._value);
	}
};
