#pragma once

#include "resourcehandle.h"
#include "property.h"
#include "resourcecache.h"
#include "gamedatabase.h"
#include "resourcetag.h"
#include "utility/trait.h"

#include "subgine/common/types.h"
#include "subgine/common/function_traits.h"
#include "subgine/common/hash.h"
#include "subgine/log/log.h"
#include "subgine/log/loginfo.h"
#include "subgine/log/logdecoration.h"

#include <string_view>

#include "subgine/common/define.h"

namespace sbg {
	
template<typename>
struct ResourceManagerService;

template<typename T>
struct ResourceManager {
private:
	using Result = typename T::Result;
	
public:
	explicit ResourceManager(T factory, ResourceCache& cache) :
		_factory{std::move(factory)},
		_cache{&cache} {}
	
	auto get(std::string_view name) const -> ResourceHandle<Result> {
		auto const hash = murmur64a(name);
		if (auto const resource = _cache->check<Result>(T::path, hash)) {
			return *resource;
		} else {
			auto l = Log::trace(SBG_LOG_INFO, "Creating resource", log::enquote(name), "from table", log::enquote(T::path));
			// We use name in create since it allows for better debuging
			return _cache->cache<Result>(T::path, hash, _factory.create(name));
		}
	}
	
	auto get(hash_t hash) const -> ResourceHandle<Result> {
		if (auto const resource = _cache->check<Result>(T::path, hash)) {
			return *resource;
		} else {
			auto const l = Log::trace(SBG_LOG_INFO, "Creating resource hash(", log::unpad(static_cast<std::size_t>(hash)), ") from table", log::enquote(T::path));
			return _cache->cache<Result>(T::path, hash, _factory.create(hash));
		}
	}
	
	auto get(resource_tag auto tag) const -> ResourceHandle<Result> {
		return get(tag.hash);
	}
	
private:
	NO_UNIQUE_ADDRESS
	T _factory;
	ResourceCache mutable* _cache;
	
	friend auto service_map(ResourceManager const&) -> ResourceManagerService<ResourceManager<T>>;
};

} // namespace sbg

#include "subgine/common/undef.h"
