#pragma once

#include "subgine/common/kangaru.h"

namespace sbg {

template<typename>
struct FactoryService;

template<typename C>
struct Factory final {
	Factory(kgr::invoker invoker, C& creator) : _invoker{invoker}, _creator{&creator} {}
	
	template<typename... Args>
	auto create(Args&&... args) -> decltype(std::declval<C&>().create(std::declval<kgr::invoker>(), std::declval<Args>()...)) {
		return _creator->create(_invoker, std::forward<Args>(args)...);
	}
	
	template<typename... Args>
	auto create(Args&&... args) const -> decltype(std::declval<C const&>().create(std::declval<kgr::invoker>(), std::declval<Args>()...)) {
		return std::as_const(*_creator).create(_invoker, std::forward<Args>(args)...);
	}
	
private:
	kgr::invoker _invoker;
	C* _creator;
	
	friend auto service_map(Factory const&) -> FactoryService<Factory<C>>;
};

} // namespace sbg
