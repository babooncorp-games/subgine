#pragma once

#include "../json.h"

#include <unordered_map>
#include <string_view>
#include <list>

namespace sbg {
	struct Property;
	struct GameDatabase;
	struct JsonManager;
}

namespace sbg::detail {

struct EntryTypeResolver {
	EntryTypeResolver(JsonManager& jsonManager, GameDatabase const* database) noexcept :
		_jsonManager{&jsonManager}, _database{database} {}
	
	[[nodiscard]]
	auto toProperty(std::string_view type, sbg::resource::json const& json) -> Property;
	
private:
	auto merge_patch_entry_type_aware(sbg::resource::json& target, sbg::resource::json const& patch) -> void;
	JsonManager* _jsonManager;
	GameDatabase const* _database;
	std::unordered_map<sbg::resource::json const*, sbg::resource::json> _composedJson;
};

} // namespace sbg::detail
