#include "entrytyperesolver.h"

#include "../property.h"
#include "../manager/jsonmanager.h"
#include "../gamedatabase.h"
#include "../utility/assetdirectory.h"

#include "subgine/log.h"

using namespace sbg::detail;

auto EntryTypeResolver::merge_patch_entry_type_aware(sbg::resource::json& target, sbg::resource::json const& patch) -> void {
	if (patch.is_object() and not patch.contains("$entry-type")) {
		if (not target.is_object()) {
			target = sbg::resource::json::object();
		} else if (auto const it = target.find("$entry-type"); it != target.end()) {
			auto const entryType = it->get<std::string_view>();
			target = *toProperty(entryType, target)._value;
		}
		
		for (auto const& [key, value] : patch.items()) {
			if (value.is_null()) {
				target.erase(key);
			} else {
				merge_patch_entry_type_aware(target[key], value);
			}
		}
	} else {
		target = patch;
	}
}

auto EntryTypeResolver::toProperty(std::string_view type, sbg::resource::json const& json) -> Property {
	auto l = Log::error([&]{ return !_database && type == "database"; },
		SBG_LOG_INFO, "Entry type database requested, but the property has no game database"
	);
	
	if (_database && type == "database") {
		auto const& table_entry = json["table"];
		auto const& name_entry = json["name"];
		
		if (!table_entry.is_string()) {
			Log::error(SBG_LOG_INFO, "Entry type database has an invalid table value. Expected a string, got:", table_entry);
			return Property{nullptr, nullptr};
		}
		
		if (!name_entry.is_string()) {
			Log::error(SBG_LOG_INFO, "Entry type database has an invalid name value. Expected a string, got:", name_entry);
			return Property{nullptr, nullptr};
		}
		
		auto const table = json["table"].get<std::string_view>();
		auto const name = json["name"].get<std::string_view>();
		
		return _database->get(table, name);
	} else if (type == "document") {
		auto const itPath = json.find("path");
		
		if (itPath != json.end() && !itPath->empty()) {
			return _jsonManager->get((asset_directory() / *itPath).string());
		}
		
		Log::error(SBG_LOG_INFO, "Missing path property for document entry type");
	} else if (type == "file") {
		auto const itPath = json.find("path");
		
		if (itPath != json.end() && !itPath->empty()) {
			return _jsonManager->getAsString((asset_directory() / *itPath).string());
		}
		
		Log::error(SBG_LOG_INFO, "Missing path property for file entry type");
	} else if (type == "compose") {
		auto const objects = json.find("objects");
		
		if (objects != json.end() && !objects->empty()) {
			auto composedJson = sbg::resource::json{};
			for (auto const& object : *objects) {
				if (not object.is_object()) {
					Log::error(SBG_LOG_INFO, "Composed object is not a json objectComposed entry type expects only objects, recieved:", log::enquote(object.dump()));
					continue;
				}
				
				merge_patch_entry_type_aware(composedJson, object);
			}
			
			auto const [iterator, inserted] = _composedJson.emplace(&json, composedJson);
			return Property{&iterator->second, this};
		}
		
		Log::error(SBG_LOG_INFO, "Missing path property for file entry type");
	}
	
	return Property{&json, this};
}
