#include "assetmapper.h"

using namespace sbg;

AssetMapper::AssetMapper(GameDatabase& database) noexcept : _database{&database} {}

auto AssetMapper::relative_from_name(std::string_view table_to, std::string_view table_from, std::string_view name, std::filesystem::path relative) const -> std::string {
	auto const pathMapping = _database->get("path-mapping", table_from);
	auto const base = std::filesystem::path{pathMapping[name]};
	
	return relative_from_path(table_to, base, relative);
}

auto AssetMapper::relative_from_path(std::string_view table, std::filesystem::path base, std::filesystem::path relative) const -> std::string {
	auto const assetMapping = _database->get("asset-mapping", table);
	auto const assetPath = to_asset_path(relative, base).generic_string();
	
	return std::string{assetMapping[assetPath]};
}

auto AssetMapper::to_asset_path(std::filesystem::path relativePath, std::filesystem::path base) -> std::filesystem::path {
	return (base.parent_path() / relativePath).lexically_normal();
}
