#pragma once

#include <utility>
#include <type_traits>
#include "subgine/common/dropping_invoke.h"

namespace sbg {

struct DefaultApply {
	template<typename I, typename B, typename... Args>
	auto operator()(I invoker, B& builder, Args&&... args) const -> dropping_invoke_result_t<I, B&, Args...> {
		return dropping_invoke(invoker, builder, std::forward<Args>(args)...);
	}
};

} // namespace sbg
