#pragma once

#include "property.h"
#include "detail/entrytyperesolver.h"

#include "subgine/common/traits.h"
#include "subgine/common/kangaru.h"
#include "subgine/common/hash.h"

#include "nlohmann/json.hpp"
#include "service/resourcecacheservice.h"

#include <filesystem>
#include <string_view>
#include <stdexcept>
#include <span>
#include <vector>

namespace sbg {

struct GameDatabaseService;

struct JsonManager;
struct FileManager;

struct GameDatabase {
	explicit GameDatabase(kgr::lazy<ResourceCacheService> cache, JsonManager& jsonManager, FileManager& fileManager) noexcept :
		_cache{cache},
		_resolver{jsonManager, this},
		_fileManager{fileManager} {}
	
	template<typename Arg, enable_if_i<std::is_convertible_v<Arg, std::span<std::filesystem::path const>>> = 0>
	inline void load(Arg&& arg) {
		load_impl(std::span<std::filesystem::path const>{arg});
	}
	
	template<typename... Args, enable_if_i<std::conjunction_v<std::is_convertible<Args, std::filesystem::path>...>> = 0>
	inline void load(Args&&... args) {
		auto const paths = std::array{std::filesystem::path{std::forward<Args>(args)}...};
		load_impl(paths);
	}
	
	void drop() noexcept;
	auto get(std::string_view table, std::string_view name) const -> Property;
	auto get(std::string_view table, hash_t hash) const -> Property;
	
private:
	friend Property;

	void load_impl(std::span<std::filesystem::path const> paths);
	
	kgr::lazy<ResourceCacheService> _cache;
	FileManager& _fileManager;
	resource::json _jsonTables = resource::json::object();
	mutable detail::EntryTypeResolver _resolver;
	
	friend auto service_map(GameDatabase const&) -> GameDatabaseService;
};

} // namespace sbg
