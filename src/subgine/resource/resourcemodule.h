#pragma once

namespace sbg {

struct ResourceModuleService;
struct ResourceCache;

struct ResourceModule {
	friend auto service_map(ResourceModule const&) -> ResourceModuleService;
};

} // namespace sbg
