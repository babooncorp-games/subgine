#pragma once

#include "abstractbuilder.h"

#include <tuple>

#include "subgine/common/define.h"

namespace sbg {

template<typename, typename, typename>
struct Builder;

template<typename B, typename R, typename... Args, typename Apply>
struct Builder<B, R(Args...), Apply> : AbstractBuilder<R(Args...), Apply> {
	explicit Builder(B builder) noexcept : _builder{std::move(builder)} {}
	
	auto create(kgr::invoker invoker, const Apply& apply, Args... args) const -> R override {
		return static_cast<R>(apply(invoker, _builder, std::forward<Args>(args)...));
	}
	
private:
	NO_UNIQUE_ADDRESS
	B _builder;
};

} // namespace sbg

#include "subgine/common/undef.h"
