#pragma once

#include <type_traits>
#include <utility>

namespace sbg {

// TODO: Add tests for OwnedResource
template<typename T>
struct OwnedResource {
	constexpr OwnedResource() = default;
	constexpr explicit OwnedResource(T&& r) noexcept : _resource{std::move(r)} {}
	constexpr explicit OwnedResource(T const& r) noexcept(std::is_nothrow_copy_constructible_v<T>) : _resource{r} {}
	
	constexpr OwnedResource(OwnedResource&& other) noexcept : _resource{std::exchange(other._resource, {})} {}
	
	OwnedResource(OwnedResource const&) = delete;
	auto operator=(OwnedResource const&) -> OwnedResource& = delete;
	
	constexpr auto operator=(OwnedResource&& other) & noexcept -> OwnedResource& {
		_resource = std::exchange(other.get(), T{});
		return *this;
	}
	
	constexpr ~OwnedResource() {
		_resource.free();
	}
	
	[[nodiscard]]
	constexpr auto release() & noexcept -> T {
		return std::exchange(_resource, T{});
	}
	
	[[nodiscard]]
	constexpr auto release() && noexcept -> T {
		return release();
	}
	
	[[nodiscard]]
	constexpr auto get() & noexcept -> T& {
		return _resource;
	}
	
	[[nodiscard]]
	constexpr auto get() const& noexcept -> T const& {
		return _resource;
	}
	
	[[nodiscard]]
	constexpr auto get() && noexcept -> T {
		return std::exchange(_resource, {});
	}
	
	constexpr auto operator->() & noexcept -> T* {
		return &_resource;
	}
	
	constexpr auto operator->() const& noexcept -> T const* {
		return &_resource;
	}
	
	constexpr auto operator*() & noexcept -> T& {
		return _resource;
	}
	
	constexpr auto operator*() const& noexcept -> T const& {
		return _resource;
	}
	
private:
	T _resource;
};

} // namespace sbg
