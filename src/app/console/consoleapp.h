#pragma once

#include "subgine/console/console.h"

namespace sbg {

struct ConsoleAppService;

struct ConsoleApp {
	struct DependentModule;
	
	ConsoleApp(DependentModule&, Console console);
	
	int run(int argc, char const** argv);
	
private:
	Console _console;
	
	friend auto service_map(ConsoleApp const&) -> ConsoleAppService;
};

} // namespace sbg
