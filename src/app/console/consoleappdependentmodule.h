#pragma once

#include "consoleapp.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleAppDependentModuleService;

struct ConsoleApp::DependentModule {
	DependentModule(kgr::container& container);
	friend auto service_map(ConsoleApp::DependentModule const&) -> ConsoleAppDependentModuleService;
};

} // namespace sbg

