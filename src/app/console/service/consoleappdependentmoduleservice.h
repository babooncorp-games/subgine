#pragma once

#include "../consoleappdependentmodule.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleAppDependentModuleService : kgr::single_service<ConsoleApp::DependentModule, kgr::autowire> {};

} // namespace sbg
