#pragma once

#include "../consoleapp.h"

#include "consoleappdependentmoduleservice.h"
#include "subgine/console/service/consoleservice.h"

#include "subgine/common/kangaru.h"

namespace sbg {

struct ConsoleAppService : kgr::single_service<ConsoleApp, kgr::autowire> {};

} // namespace sbg
