#include "consoleappdependentmodule.h"

#include "subgine/console/module.h"
#include "subgine/generator/module.h"
#include "subgine/log/module.h"

using namespace sbg;

ConsoleApp::DependentModule::DependentModule(kgr::container& container) {
	container.emplace<ConsoleModuleService>();
	container.emplace<GeneratorModuleService>();
	container.emplace<LogModuleService>();
}
