#include "service/consoleappservice.h"

#include "subgine/common/kangaru.h"

int main(int argc, char const** argv) {
	return kgr::container{}.service<sbg::ConsoleAppService>().run(argc, argv);
}
