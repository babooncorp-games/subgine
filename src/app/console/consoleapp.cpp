#include "consoleapp.h"

#include "subgine/log.h"
#include "subgine/log/logger/filelogger.h"
#include "cpplocate/cpplocate.h"

using namespace sbg;

ConsoleApp::ConsoleApp(ConsoleApp::DependentModule&, Console console) : _console{console} {
	static auto path = cpplocate::configDir("subgine") + "/subgine.log";
	FileLogger::file(path.c_str());
}

int ConsoleApp::run(int argc, char const** argv) {
	if (argc >= 2) {
		_console.execute(Console::default_context(argc, argv));
	}
	
	return 0;
}
