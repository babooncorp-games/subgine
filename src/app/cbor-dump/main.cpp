#include "nlohmann/json.hpp"

#include <iostream>
#include <filesystem>
#include <string_view>
#include <ranges>
#include <algorithm>
#include <fstream>

int main(int argc, char const** argv) {
	if (argc < 2) {
		std::cerr << "Usage: cbor-dump [-p] file-1 [file-n...]" << std::endl;
		return 1;
	}
	
	auto [pretty, starts_at] = /* immediate */ [&] {
		if (std::string_view{argv[1]}.starts_with("-")) {
			return std::pair{true, 2};
		} else {
			return std::pair{false, 1};
		}
	}();
	
	for (auto arg : std::ranges::subrange(argv, argv + argc) | std::views::drop(starts_at)) {
		auto const json = nlohmann::json::from_cbor(std::ifstream{arg});
		std::cout << (pretty ? json.dump(4) : json.dump()) << std::endl;
	}
	
	return 0;
}
