#include <nlohmann/json.hpp>

#include "subgine/state.h"

#include <iostream>
#include <fstream>
#include <filesystem>

auto not_token(std::string_view text) -> bool {
		return
		    text != "SOURCES"
		and text != "ASSET"
		and text != "TYPE"
		and text != "OPTIONS";
};

int main(int argc, char const** argv) {
	auto root = nlohmann::json{};
	auto& asset = root["asset"] = nlohmann::json::array();
	
	if (argc < 2) {
		std::cout << "Usage: generate-asset-metadata output.json [args1, [argsn...]]" << std::endl;
		return 1;
	}
	
	auto const output = std::filesystem::path{argv[1]};
	
	struct Initial {
		auto expects(std::string_view next) const {
			return next == "ASSET";
		}
	};
	
	struct BeginAsset {
		nlohmann::json& asset;
		
		auto expects(std::string_view next) const {
			return not_token(next);
		}
	};
	
	struct AssetFileRead {
		nlohmann::json& asset;
		
		auto expects(std::string_view next) const {
			return next == "TYPE";
		}
	};
	
	struct BeginType {
		nlohmann::json& asset;
		
		auto expects(std::string_view next) const {
			return not_token(next);
		}
	};
	
	struct TypeRead {
		nlohmann::json& asset;
		
		auto expects(std::string_view next) const {
			return next == "SOURCES" or next == "OPTIONS";
		}
	};
	
	struct SourcesBegin {
		nlohmann::json& asset;
		nlohmann::json& sources;
		
		auto expects(std::string_view next) const {
			return next != "SOURCES";
		}
	};
	
	struct SourcesReading {
		nlohmann::json& asset;
		nlohmann::json& sources;
		
		auto expects(std::string_view next) const {
			return next != "SOURCES" and next != "TYPE";
		}
	};
	
	struct OptionsBegin {
		nlohmann::json& asset;
		nlohmann::json& options;
		
		auto expects(std::string_view next) const {
			return next != "OPTIONS";
		}
	};
	
	struct OptionsReading {
		nlohmann::json& asset;
		nlohmann::json& options;
		
		auto expects(std::string_view next) const {
			return next != "OPTIONS" and next != "TYPE";
		}
	};
	
	auto state = sbg::StateMachine{Initial{}};
	
	for (auto arg = int{2} ; arg < argc ; ++arg) {
		auto const current = std::string_view{argv[arg]};
		
		auto okay = state.transit(
			[&](Initial state) -> std::optional<BeginAsset> {
				if (state.expects(current)) {
					asset.push_back(nlohmann::json::object());
					return BeginAsset{asset.back()};
				} else {
					return std::nullopt;
				}
			},
			[&](BeginAsset state) -> std::optional<AssetFileRead> {
				if (state.expects(current)) {
					state.asset["name"] = std::string{current};
					return AssetFileRead{state.asset};
				} else {
					return std::nullopt;
				}
			},
			[&](AssetFileRead state) -> std::optional<BeginType> {
				if (state.expects(current)) {
					return BeginType{state.asset};
				} else {
					return std::nullopt;
				}
			},
			[&](BeginType state) -> std::optional<TypeRead> {
				if (state.expects(current)) {
					state.asset["type"] = std::string{current};
					return TypeRead{state.asset};
				} else {
					return std::nullopt;
				}
			},
			[&](TypeRead state) -> std::variant<std::monostate, SourcesBegin, OptionsBegin> {
				if (state.expects(current)) {
					if (current == "SOURCES") {
						return SourcesBegin{state.asset, state.asset["files"] = nlohmann::json::array()};
					} else if (current == "OPTIONS") {
						return OptionsBegin{state.asset, state.asset["data"] = nlohmann::json::object()};
					}
				}
				
				return {};
			},
			[&](SourcesBegin state) -> std::variant<std::monostate, SourcesReading> {
				if (state.expects(current)) {
					state.sources.push_back(std::string{current});
					return SourcesReading{state.asset, state.sources};
				} else {
					return {};
				}
			},
			[&](SourcesReading state) -> std::variant<std::monostate, SourcesReading, OptionsBegin, BeginAsset> {
				if (state.expects(current)) {
					if (current == "OPTIONS") {
						return OptionsBegin{state.asset, state.asset["data"] = nlohmann::json::object()};
					} else if (current == "ASSET") {
						asset.push_back(nlohmann::json::object());
						return BeginAsset{asset.back()};
					}
					state.sources.push_back(std::string{current});
					return SourcesReading{state.asset, state.sources};
				} else {
					return {};
				}
			},
			[&](OptionsBegin state) -> std::variant<std::monostate, OptionsReading> {
				if (state.expects(current)) {
					state.options[std::string{"data"} + std::to_string(state.options.size())] = std::string{current};
					return OptionsReading{state.asset, state.options};
				} else {
					return std::monostate{};
				}
			},
			[&](OptionsReading state) -> std::variant<std::monostate, OptionsReading, SourcesBegin, BeginAsset> {
				if (state.expects(current)) {
					if (current == "SOURCES") {
						return SourcesBegin{state.asset, state.asset["files"] = nlohmann::json::array()};
					} else if (current == "ASSET") {
						asset.push_back(nlohmann::json::object());
						return BeginAsset{asset.back()};
					}
					state.options[std::string{"data"} + std::to_string(state.options.size())] = std::string{current};
					return OptionsReading{state.asset, state.options};
				} else {
					return std::monostate{};
				}
			}
		);
		
		if (not okay) {
			std::cerr << "Error: unexpected \"" << current << "\" at argument " << arg - 2 << std::endl;
			return 1;
		}
	}
	
	auto const json_str = std::string{root.dump(4)};
	auto output_file = std::ofstream{output, std::ios::out | std::ios::binary};
	output_file.write(json_str.data(), json_str.size());
}
