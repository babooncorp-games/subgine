#include "subgine/graphic/utility/stbi_deleter.h"

#include "stb/stb_image.h"
#include "stb/stb_image_write.h"

#include <algorithm>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <span>
#include <string_view>
#include <charconv>
#include <vector>

auto format_texture_tileset(unsigned char const* pixels, int const pixel_width, int const sizeX, int const sizeY, int const tile_sizeX, int const tile_sizeY) -> std::vector<unsigned char> {
	std::vector<unsigned char> formatted;
	
	formatted.resize((sizeX * sizeY + tile_sizeX * tile_sizeY) * pixel_width);
	
	auto nth = int{tile_sizeX * tile_sizeY};
	for (int k=0 ; k<(sizeX * sizeY) / (tile_sizeX * tile_sizeY) ; k++) {
		for (int j=0 ; j < tile_sizeY ; j++) {
			for (int i=0 ; i < tile_sizeX ; i++) {
				auto const tileset_width = (sizeX / tile_sizeX);
				auto const pixel = i + (j * tile_sizeX * tileset_width) +
					(k % tileset_width) * tile_sizeX + (k / tileset_width) * sizeX * tile_sizeY;
					
				std::copy(pixels + pixel * pixel_width, pixels + pixel * pixel_width + pixel_width, formatted.begin() + nth++ * pixel_width);
			}
		}
	}
	
	return formatted;
}

int main(int argc, char const** argv) {
	if (argc < 5) {
		std::cout << "Usage: compile-tileset 32 32 target-file.png input-image.png" << std::endl;
		return 1;
	}
	
	auto const argX = std::string_view{argv[1]};
	auto const argY = std::string_view{argv[2]};
	int tilesizeX{};
	int tilesizeY{};
	std::from_chars(argX.data(), argX.data() + argX.size(), tilesizeX);
	std::from_chars(argY.data(), argY.data() + argY.size(), tilesizeY);
	
	auto const output = std::filesystem::path{argv[3]};
	auto const input = std::filesystem::path{argv[4]};
	
	// std::cout << " -- Formatting image file " << input << std::endl;
	
	int sizeX;
	int sizeY;
	int channels;
	auto const pixels = std::unique_ptr<unsigned char, sbg::stbi_deleter>{stbi_load(input.string().c_str(), &sizeX, &sizeY, &channels, 0)};
	
	if (!pixels) {
		std::cerr << "STB image cannot load image at path " << input << std::endl;
		return 1;
	}
	
	auto formatted_pixels = format_texture_tileset(
		pixels.get(), channels,
		sizeX, sizeY, tilesizeX, tilesizeY
	);
	
	stbi_write_png(output.string().c_str(), tilesizeX + (sizeX * sizeY) / tilesizeY, tilesizeY, channels, formatted_pixels.data(), (tilesizeX + (sizeX * sizeY) / tilesizeY) * channels);
}
