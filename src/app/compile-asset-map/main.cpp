#include <nlohmann/json.hpp>

#include <iostream>
#include <filesystem>
#include <fstream>
#include <span>
#include <optional>

int main(int argc, char const** argv) {
	if (argc < 5) {
		std::cout << "Usage: compile-asset-map target-file.cbor module-path asset.json input-json-1.json [input-json-n.json...]" << std::endl;
		return 1;
	}
	
	auto const target = std::filesystem::path{argv[1]};
	auto const output = std::ofstream{target};
	
	auto const module_path = std::filesystem::path{argv[2]};
	
	auto const asset_json_path = std::filesystem::path{argv[3]};
	std::ifstream asset_file{asset_json_path};
	if (!asset_file.is_open()) {
		std::cerr << "Cannot open file " << asset_json_path << std::endl;
		return 1;
	}
	auto const asset_json = nlohmann::json::parse(std::move(asset_file))["asset"];
	
	auto const inputs = std::span{argv + 4, static_cast<std::size_t>(argc - 4)};
	
	auto result = nlohmann::json{};
	// std::cout << "Compiling resource map " << target.string() << " as cbor:\n" << std::flush;
	
	for (auto const input : inputs) {
		// std::cout << " -- Mapping json file " << input << "\n" << std::flush;
		
		std::ifstream f{input};
		
		if (!f.is_open()) {
			std::cerr << "Cannot open file " << input << std::endl;
			return 1;
		}
		
		auto const json_resource = nlohmann::json::parse(std::move(f));
		for (auto const& [resource_type, entries] : json_resource.items()) {
			auto assets = std::unordered_map<std::string, std::string>{};
			auto paths = std::unordered_map<std::string, std::string>{};
			
			for (auto const& [name, entry] : entries.items()) {
				auto const get_path = [](nlohmann::json entry) -> std::optional<std::filesystem::path> {
					if (auto const& data = entry["data"]; !data.is_null() && !data["$entry-type"].is_null()) {
						if (auto const path = data["path"]; path.is_string()) {
							return std::filesystem::path{path.get<std::string_view>()};
						}
					} else {
						if (auto const path = entry["path"]; path.is_string()) {
							return std::filesystem::path{path.get<std::string_view>()};
						}
					}
					
					return std::nullopt;
				};
				
				if (auto const entry_path = get_path(entry)) {
					auto const entry_name = std::filesystem::path{*entry_path}.replace_extension();
					
					auto it = std::find_if(
						asset_json.begin(), asset_json.end(),
						[&](auto const& asset) {
							return asset["name"] == entry_name.string();
						}
					);
					
					if (it != asset_json.end()) {
						auto const& found_entry = *it;
						for (auto const& file : found_entry["files"]) {
							auto const path_str = (module_path / std::filesystem::path{std::string{file}}).generic_string();
							assets[path_str] = name;
							paths[name] = path_str;
						}
					} else {
						std::cerr << "Didn't find resource " << *entry_path << std::endl;
					}
				} else {
					std::cerr << "Resource " << name << " doesn't contain a path" << std::endl;
					return 1;
				}
			}
			
			result["asset-mapping"][resource_type] = nlohmann::json(assets);
			result["path-mapping"][resource_type] = nlohmann::json(paths);
		}
	}
	
	std::cout << std::flush;
	
	auto const bytes = nlohmann::json::to_cbor(result);
	
	if (auto const parent = target.parent_path(); not parent.empty()) {
		std::filesystem::create_directories(parent);
	}
	auto destination = std::ofstream{target, std::ios::out | std::ios::binary};
	destination.write(reinterpret_cast<char const*>(bytes.data()), bytes.size());
}