#include "subgine/graphic/utility/stbi_deleter.h"

#include "stb/stb_image.h"
#include "stb/stb_image_write.h"

#include <algorithm>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <span>
#include <string_view>
#include <charconv>
#include <vector>

auto format_texture_spritesheet(unsigned char const* pixels, int const pixel_width, int const size_x, int const size_y, int const sprite_size_x, int const sprite_size_y) -> std::vector<unsigned char> {
	std::vector<unsigned char> formatted;
	
	formatted.resize(size_x * size_y * pixel_width);
	
	auto nth = int{0};
	for (int k=0 ; k<(size_x * size_y) / (sprite_size_x * sprite_size_y) ; k++) {
		for (int j=0 ; j < sprite_size_y ; j++) {
			for (int i=0 ; i < sprite_size_x ; i++) {
				auto const tileset_width = (size_x / sprite_size_x);
				auto const pixel = i + (j * sprite_size_x * tileset_width) +
					(k % tileset_width) * sprite_size_x + (k / tileset_width) * size_x * sprite_size_y;
					
				std::copy(pixels + pixel * pixel_width, pixels + pixel * pixel_width + pixel_width, formatted.begin() + nth++ * pixel_width);
			}
		}
	}
	
	return formatted;
}

int main(int argc, char const** argv) {
	if (argc < 5) {
		std::cout << "Usage: compile-spritesheet 32 32 target-file.png input-image.png" << std::endl;
		return 1;
	}
	
	auto const arg_x = std::string_view{argv[1]};
	auto const arg_y = std::string_view{argv[2]};
	int sprite_size_x{};
	int sprite_size_y{};
	std::from_chars(arg_x.data(), arg_x.data() + arg_x.size(), sprite_size_x);
	std::from_chars(arg_y.data(), arg_y.data() + arg_y.size(), sprite_size_y);
	
	auto const output = std::filesystem::path{argv[3]};
	auto const input = std::filesystem::path{argv[4]};
	
	// std::cout << " -- Formatting image file " << input << std::endl;

	int size_x;
	int size_y;
	int channels;
	auto const pixels = std::unique_ptr<unsigned char, sbg::stbi_deleter>{stbi_load(input.string().c_str(), &size_x, &size_y, &channels, 0)};
	
	if (!pixels) {
		std::cerr << "STB image cannot load image at path " << input << std::endl;
		return 1;
	}
	
	auto formatted_pixels = format_texture_spritesheet(
		pixels.get(), channels,
		size_x, size_y, sprite_size_x, sprite_size_y
	);
	
	stbi_write_png(output.string().c_str(), size_x, size_y, channels, formatted_pixels.data(), size_x * channels);
}
