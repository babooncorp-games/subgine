#include <nlohmann/json.hpp>

#include <iostream>
#include <filesystem>
#include <fstream>
#include <span>

int main(int argc, char const** argv) {
	if (argc < 3) {
		std::cout << "Usage: compile-json target-file.cbor input-json-1.json [input-json-n.json...]" << std::endl;
		return 1;
	}
	
	auto const target = std::filesystem::path{argv[1]};
	auto const output = std::ofstream{target};
	auto const inputs = std::span{argv + 2, static_cast<std::size_t>(argc - 2)};
	
	auto result = nlohmann::json{};
	// std::cout << "Compiling target \"" << target.string() << "\" as cbor:\n" << std::flush;
	for (auto const input : inputs) {
		// std::cout << " -- Merging json file \"" << input << "\"\n";
		
		std::ifstream f{input};
		
		if (!f.is_open()) {
			std::cerr << "Cannot open file \"" << input << std::endl;
			return 1;
		}
		
		try {
			result.merge_patch(nlohmann::json::parse(std::move(f)));
		} catch (nlohmann::json::parse_error const& ex) {
			auto location = ex.byte;
			auto f2 = std::fstream{input};
			
			auto lineNumber = 0;
			auto line = std::string{};
			while (std::getline(f2, line) and line.size() <= location) {
				location -= line.size();
				++lineNumber;
			}
			
			auto const path = std::filesystem::current_path() / input;
			std::cerr << path.string() << ":" << lineNumber << ":" << location << ": error: " << std::string_view{ex.what()}.substr(sizeof("[json.exception.parse_error.101]")) << std::endl;
		}
	}
	
	// std::cout << std::flush;
	
	auto const bytes = nlohmann::json::to_cbor(result);
	
	if (auto const parent = target.parent_path(); not parent.empty()) {
		std::filesystem::create_directories(parent);
	}
	auto destination = std::ofstream{target, std::ios::out | std::ios::binary};
	destination.write(reinterpret_cast<char const*>(bytes.data()), bytes.size());
}
